<?php
require('func.php');
require('stripe-php/init.php');

/* $stripesecretapikey and $stripewebhooksecret are set in connectdbinfo.php */
\Stripe\Stripe::setApiKey($stripesecretapikey);

$stripe = new \Stripe\StripeClient($stripesecretapikey);

/**
 *
 */
function stripe_webhook_php_get_account_id_from_customer($stripecustomerid)
{
	$query = sprintf('select id from accounts where stripecustomerid="%s" limit 1;',
			func_php_escape($stripecustomerid));
	$result = func_php_query($query);
	if (!mysqli_num_rows($result)) {
		return false;
	}
	$r = mysqli_fetch_assoc($result);
	return $r['id'];
}

/**
 *
 */
function stripe_webhook_php_invoice_paid($ev)
{
	global $stripe;

	$stripecustomerid = $ev->object->customer;
	$acctid = stripe_webhook_php_get_account_id_from_customer($stripecustomerid);
	if (!$acctid) {
		error_log('stripe-webook.php:'.__LINE__.' @ @ @ Error: No user with that customerid:'.$ev->object->customer);
		return false;
	}

	/* Update details on the payment method */
	$lastchargeid = $ev->object->charge;
	$paymentIntent = $ev->object->payment_intent;
	if ($lastchargeid && $paymentIntent) {
		$pi = $stripe->paymentIntents->retrieve($paymentIntent);
		$paymentMethodId = $pi->payment_method;
		$pm = $stripe->paymentMethods->retrieve($paymentMethodId);
		$last4 = $pm->card->last4;
		$brand = $pm->card->brand;
		$expiry = $pm->card->exp_month.'/'.$pm->card->exp_year;

		/* Remove any existing payment method */
		$query = sprintf('select stripepaymentmethodid from accounts'
				.' where accounts.id = %d limit 1;', /* ACCT-ID */
				$acctid);
		$result = func_php_query($query);
		$r = mysqli_fetch_assoc($result);
		$oldPaymentMethodId = $r['stripepaymentmethodid'];

		if ($oldPaymentMethodId && ($oldPaymentMethodId != $paymentMethodId)) {
			$pmdata = $stripe->paymentMethods->retrieve($oldPaymentMethodId);
			if ($pmdata->customer == $stripecustomerid) {
				$opm = $stripe->paymentMethods->detach($oldPaymentMethodId);
			}
		}

		$query = sprintf("update accounts"
				." set stripesubscriptionlastchargeid='%s'," /* CHARGE-ID */
				." stripepaymentmethodid='%s'," /* PAY-METHODID */
				." stripepaymentlast4='%s', stripepaymentbrand='%s'," /* LAST4, BRAND */
				." stripepaymentexpiry='%s'" /* EXPIRY */
				." where accounts.id=%d limit 1;", /* ACCT-ID */
				func_php_escape($lastchargeid),
				func_php_escape($paymentMethodId),
				func_php_escape($last4), func_php_escape($brand),
				func_php_escape($expiry), $acctid);
		func_php_query($query);

		/* Set the default payment method on the customer. */
		$custdata = $stripe->customers->update($stripecustomerid, [
			'invoice_settings' => [
				'default_payment_method' => $paymentMethodId,
			],
		]);
	}

	/* Check to issue refunds if in credit */
	$total = $ev->object->total;
	if ($total < 0) {
		$query = sprintf('select stripesubscriptionlastchargeid from accounts'
				.' where accounts.id = %d limit 1;', /* ACCT-ID */
				$acctid);
		$result = func_php_query($query);
		$r = mysqli_fetch_assoc($result);
		$lastchargeid = $r['stripesubscriptionlastchargeid'];
		$refundamount = -$total;
		$refund = $stripe->refunds->create([
				'charge' => $lastchargeid,
				'amount' => $refundamount
		]);

		$credit = $stripe->customers->createBalanceTransaction(
				$stripecustomerid,
				['amount' => $refundamount, 'currency' => 'aud']);
	}
}

/**
 * Payment Failed (such as out of balance credit card)
 * Send reminder to user before cancelling plan.
 */
function stripe_webhook_php_invoice_payment_failed($ev)
{
	global $stripe;

}

/**
 * Customer changed their plan.
 */
function stripe_webhook_php_customer_subscription_updated($ev)
{
	global $stripe;

	/* Check the status before updating anything. */
	$status = $ev->object->status;
	if ($status != 'active') {
		error_log('stripe-webook.php:'.__LINE__.' @ @ @ Error: Invalid stripe event status:'.$status);
		return false;
	}

	$acctid = stripe_webhook_php_get_account_id_from_customer($ev->object->customer);
	if (!$acctid) {
		error_log('stripe-webook.php:'.__LINE__.' @ @ @ Error: No user with that customerid:'.$ev->object->customer);
		return false;
	}

	$subscriptionid = $ev->object->id;
	$priceid = $ev->object->items->data[0]->price->id;
	$quantity = $ev->object->items->data[0]->quantity;
	$interval = $ev->object->items->data[0]->plan->interval;
	$currentperiodstart = $ev->object->current_period_start;
	$currentperiodend = $ev->object->current_period_end;

	$accounttype = '';
	$accountgb = $quantity * 10;
	if ($interval == 'year') {
		$accounttype = sprintf('%d GB Yearly', $accountgb);
	} else {
		$accounttype = sprintf('%d GB Monthly', $accountgb);
	}

	$query = sprintf("update accounts"
			.' set stripesubscriptionpriceid="%s", ' /* PRICE-ID */
			.' stripesubscriptionquantity=%d,' /* QUANTITY */
			.' accounttype="%s", stripesubscriptionid = "%s",' /* ACCT-TYPE, SUB-ID */
			.' stripesubscriptioncurrentperiodstart=FROM_UNIXTIME(%d),' /* DATE-START */
			.' stripesubscriptioncurrentperiodend=FROM_UNIXTIME(%d)' /* DATE-END */
			.' where accounts.id=%d limit 1;', /* ACCT-ID */
			func_php_escape($priceid), $quantity,
			$accounttype, func_php_escape($subscriptionid),
			$currentperiodstart, $currentperiodend, $acctid);
	func_php_query($query);

	func_php_setAccountType($acctid, $quantity);
}

/**
 * Customer closed their account.
 */
function stripe_webhook_php_customer_subscription_deleted($ev)
{
	global $stripe;

	$stripecustomerid = $ev->object->customer;
	$acctid = stripe_webhook_php_get_account_id_from_customer($stripecustomerid);
	if (!$acctid) {
		error_log('stripe-webook.php:'.__LINE__.' @ @ @ Error: No user with that customerid:'.$ev->object->customer);
		return false;
	}

	/* Refund everything they have paid for */
	$charges = $stripe->charges->all(['customer' => $stripecustomerid, 'limit' => 100]);
	$i = 0;
	$len = sizeof($charges->data);
	for ($i = 0; $i < $len; $i++) {
		$chargeid = $charges->data[$i]->id;
		$refunded = $charges->data[$i]->refunded;
		$paid = $charges->data[$i]->paid;
		if ($paid && !$refunded) {
			/* Try to refund */
			try {
				$refund = $stripe->refunds->create([
					'charge' => $chargeid
				]);
			} catch (Exception $e) {
				error_log('stripe-webhook.php:'.__LINE__.' Could not refund on account close! Caught Exception $e = '.$e);
				error_log('stripe-webhook.php:'.__LINE__.' $e->getMessage()='.$e->getMessage());
			}
		}
	}

	/* Reset paid fields in accounts table */
	$query = sprintf('update accounts'
			.' set accounttype = NULL,'
			.' stripesubscriptionid = NULL,'
			.' stripesubscriptioncurrentperiodstart = NULL,'
			.' stripesubscriptioncurrentperiodend = NULL,'
			.' stripesubscriptionpriceid = NULL,'
			.' stripesubscriptionquantity = NULL,'
			.' stripesubscriptionlastchargeid = NULL,'
			.' stripepaymentmethodid = NULL, stripepaymentlast4 = NULL,'
			.' stripepaymentbrand = NULL, stripepaymentexpiry = NULL'
			.' where accounts.id = %d limit 1;', /* ACCTID */
			$acctid);
	func_php_query($query);

	/* Setting GB in _setAccountType changes account to free quotas. */
	func_php_setAccountType($acctid, 0);
}

$payload = @file_get_contents('php://input');
$sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
$event = null;

try {
	$event = \Stripe\Webhook::constructEvent($payload,
			$sig_header, $stripewebhooksecret);
} catch(\UnexpectedValueException $e) {
	/* Invalid payload */
	http_response_code(400);
	exit();
} catch(\Stripe\Exception\SignatureVerificationException $e) {
	/* Invalid signature */
	http_response_code(400);
	exit();
}

switch ($event->type) {
case 'invoice.paid':
	stripe_webhook_php_invoice_paid($event->data);
	break;

case 'invoice.payment_failed':
	stripe_webhook_php_invoice_payment_failed($event->data);
	break;

case 'customer.subscription.updated':
	stripe_webhook_php_customer_subscription_updated($event->data);
	break;

case 'customer.subscription.deleted':
	stripe_webhook_php_customer_subscription_deleted($event->data);
	break;

default:
	error_log("Warning: Unprocessed Stripe webhook call: ".print_r($event, TRUE));
}

http_response_code(200);
?>
