<?php
require('func.php');

/* phpglobals */
$websock_host = 'localhost';
$websock_port = '8090';
$null = NULL; /* Needed for socket_select() as an empty destination array */
$sleeptime = 10000; /* Microseconds, not milliseconds */
$websock_clients = array();
$websock_masterSocket = null;

define("MSGTYPE", 1); /* Used for IPC with backend.php */

/**
 * Returns either an ID that could be zero, or boolean false.
 * Use strict comparitor (===) on return to make sure it is either
 * zero or false.
 */
function websock_php_getWebsockClientIdBySocket($socket)
{
	global $websock_clients;

	foreach ($websock_clients as $id => $client) {
		if ($client['socket'] == $socket) {
			return $id;
		}
	}

	error_log("%4d WARNING websock_php_getWebsockClientIdBySocket: No socket found, returning false;\n", __LINE__);
	return false; /* No matching socket found */
}

/**
 * Returns either an ID that could be zero, or boolean false.
 * Use strict comparitor (===) on return to make sure it is either
 * zero or false.
 */
function websock_php_getWebsockClientIdByUsername($username)
{
	global $websock_clients;

	foreach ($websock_clients as $id => $client) {
		if ($client['username'] == $username) {
			return $id;
		}
	}

	error_log("%4d WARNING: websock_php_getWebsockClientIdByUsername: No username for socket found, returning false;\n", __LINE__);
	return false; /* No matching socket found */
}

/**
 * $socket is the master socket in this function,
 * NOT the client socket, we'll get that one in here.
 */
function websock_php_connect($socket)
{
	global $clientSockets;
	global $websock_clients;
	global $websock_host, $websock_port;

	$newSocket = socket_accept($socket);
	$clientSockets[] = $newSocket;

	$websock_clients[] = array('id' => 0, 'username' => '',
			'websockid' => '', 'socket' => $newSocket);

	$header = socket_read($newSocket, 1024);
	websock_php_performHandshake($header, $newSocket, $websock_host, $websock_port);
}

/**
 *
 */
function websock_php_disconnect($socket)
{
	global $clientSockets;
	global $websock_clients;

	$wsclientid = websock_php_getWebsockClientIdBySocket($socket);
	if ($wsclientid !== false) {
		$username = $websock_clients[$wsclientid]['username'];
		$websockid = $websock_clients[$wsclientid]['websockid'];

		$query = sprintf('update accounts set websockid=NULL'
				.' where username="%s" and websockid="%s" limit 1;',
				func_php_escape($username), func_php_escape($websockid));
		$result = func_php_query($query);

		unset($websock_clients[$wsclientid]);
	}

	$i = array_search($socket, $clientSockets);
	unset($clientSockets[$i]);
	socket_close($socket);
}

/**
 *
 */
function websock_php_performHandshake($received_header, $socket, $host, $port)
{
	$headers = array();
	$lines = preg_split("/\r\n/", $received_header);
	foreach ($lines as $line) {
		$line = chop($line);
		if (preg_match('/\A(\S*): (.*)\z/', $line, $matches)) {
			$headers[$matches[1]] = $matches[2];
		}
	}

	$uuid = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11';
	$secKey = $headers['Sec-WebSocket-Key'];
	$secAccept = base64_encode(pack('H*', sha1($secKey.$uuid)));
	$upgrade = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n"
			."Upgrade: websocket\r\n"
			."Connection: Upgrade\r\n"
			."WebSocket-Origin: $host\r\n"
			."WebSocket-Location: ws://$host:$port/websock.php\r\n"
			."Sec-WebSocket-Accept:$secAccept\r\n\r\n";
	socket_write($socket, $upgrade, strlen($upgrade));
}

/**
 *
 */
function websock_php_unmask($text)
{
	$masks = '';
	$data = '';
	$length = ord($text[1]) & 127;
	if ($length == 126) {
		$masks = substr($text, 4, 4);
		$data = substr($text, 8);
	} elseif ($length == 127) {
		$masks = substr($text, 10, 4);
		$data = substr($text, 14);
	} else {
		$masks = substr($text, 2, 4);
		$data = substr($text, 6);
	}

	$text = '';
	for ($i = 0; $i < strlen($data); ++$i) {
		$text .= $data[$i] ^ $masks[$i % 4];
	}

	return $text;
}

/**
 *
 */
function websock_php_mask($text)
{
	$b1 = 0x80 | (0x1 & 0x0f);
	$length = strlen($text);
	if ($length <= 125) {
		$header = pack('CC', $b1, $length);
	} elseif (($length > 125) && ($length < 65536)) {
		$header = pack('CCn', $b1, 126, $length);
	} elseif ($length >= 65536) {
		$header = pack('CCNN', $b1, 127, $length);
	}

	return $header.$text;
}

/**
 *
 */
function websock_php_sendMessage($socket, $msg)
{
	@socket_write($socket, $msg, strlen($msg));

	return true;
}

/**
 * Assign the websock a unique ID and save to account database.
 */
function websock_php_wsopen($json, $socket)
{
	global $websock_clients;

	$wsclientid = websock_php_getWebsockClientIdBySocket($socket);
	if ($wsclientid === false) {
		error_log("%4d WARNING websock_php_open() no client socket found!\n", __LINE__);
		return false; /* No socket exists */
	}

	$query = sprintf('select id, username from accounts where username="%s" and sessionid="%s" limit 1;',
			func_php_escape($json['username']), func_php_escape($json['sessionid']));
	$result = func_php_query($query);
	if (!mysqli_num_rows($result)) {
		return false; /* No valid user with that username/ID */
	}

	$r = mysqli_fetch_assoc($result);
	$id = $r['id'];
	$username = $r['username'];
	$websockid = '';
	for ($i = 0; $i < 64; $i++) {
		$websockid .= substr('1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
			mt_rand(0, 61), 1);
	}
	$query = sprintf('update accounts set websockid="%s" where id=%d limit 1;',
			func_php_escape($websockid), $id);
	$result = func_php_query($query);

	$websock_clients[$wsclientid]['id'] = $id;
	$websock_clients[$wsclientid]['username'] = $username;
	$websock_clients[$wsclientid]['websockid'] = $websockid;
	//array('id' => $id, 'username' => $username,
	//		'websockid' => $websockid, 'socket' => $socket);

	/* DEBUGGING STUFF BELOW */
	$data = websock_php_mask(json_encode(array('type' => 'connected', 'websockid' => $websockid)));
	websock_php_sendMessage($socket, $data);
}

/**
 *
 */
function websock_php_handleWSCall($json, $socket)
{
	$jsontype = strtolower($json['type'] ?? '');
	switch ($jsontype) {
	case 'wsopen':
		websock_php_wsopen($json, $socket);
		break;
	}
}

/**
 *
 */
function DEBUG_message_everyone($msg)
{
	global $clientSockets;

	$clientcount = count($clientSockets);

	foreach ($clientSockets as $changed_socket) {
		@socket_write($changed_socket, $msg, strlen($msg));
	}
	return true;
}

/* Set up the master socket */
$websock_masterSocket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
socket_set_option($websock_masterSocket, SOL_SOCKET, SO_REUSEADDR, 1);
socket_bind($websock_masterSocket, 0, $websock_port);
socket_listen($websock_masterSocket);

$clientSockets = array($websock_masterSocket);
while (true) {
	usleep($sleeptime);

	$socketsread = $clientSockets;
	socket_select($socketsread, $null, $null, 0, 10);

	foreach ($socketsread as $socket) {
		if ($socket == $websock_masterSocket) {
			/* Handle incoming connections to the master socket */
			websock_php_connect($socket);
		} else {
			/* Handle incoming messages from client sockets */
			$size = @socket_recv($socket, $buffer, 1024, 0);
			$receivedSize = 0;
			if ($size > 0) {
				$receivedText = websock_php_unmask($buffer);
				$receivedSize = strlen($receivedText);
			}

			$json = false;
			if ($receivedSize) {
				$json = json_decode($receivedText, true);
			}

			if ($json) {
				websock_php_handleWSCall($json, $socket);
			} else {
				if ($size === 0) {
					websock_php_disconnect($socket);

				} else { // $size === false
					/* Handle error */
					$errCode = socket_last_error($socket);
					switch ($errCode) {
					case 102: // ENETRESET: Network dropped connection because of reset.
					case 103: // ECONNABORTED: Software caused connection abort.
					case 104: // ECONNRESET: Connection reset by peer.
					case 105: // ENOBUFS: No buffer space available.
					case 106: // EISCONN: Transport endpoint is already connected.
					case 107: // ENOTCONN: Transport endpoint is not connected.
					case 108: // ESHUTDOWN: Cannot send after transport endpoint shutdown.
					case 109: // ETOOMANYREFS: Too many references: cannot splice.
					case 110: // ETIMEDOUT: Connection timed out.
					case 111: // ECONNREFUSED: Connection refused.
					case 112: // EHOSTDOWN: Host is down.
					case 113: // EHOSTUNREACH: No route to host.
					case 121: // EREMOTEIO: Remote I/O error (failure their end).
					case 125: // ECANCELED: Operation Canceled.
						error_log("%4d websock.php ERROR code %d;\n", __LINE__, $errCode);
						socket_clear_error($socket);
						break;
					default:
						/* Some other error */
					}

					websock_php_disconnect($socket);
				}
			}
		}
	}

	/**
	 * START - IPC stuff
	 */

	/* Recieve any messages from backend.php */
	$backend_key = ftok('backend.php', 'A');
	$backend_queue = msg_get_queue($backend_key);
	while (msg_receive($backend_queue, MSGTYPE, $msgtype, 4096, $msg, false, MSG_IPC_NOWAIT)) {
		$m2 = unserialize($msg);

		/* Send a reply back to backend.php */
		$reply = md5($m2);
		$server_key = ftok('websock.php', 'A');
		$server_queue = msg_get_queue($server_key);
		msg_send($server_queue, MSGTYPE, $reply);

		$m = json_decode($m2);
		$messagearray['type'] = 'message_received';
		$messagearray['id'] = $m->postid;
		$messagearray['sender'] = $m->sender;

		foreach ($m->recipients as $recipient) {
			$recipientClientId = websock_php_getWebsockClientIdByUsername($recipient);
			if ($recipientClientId !== false) {
				$socket = $websock_clients[$recipientClientId]['socket'];
				$data = websock_php_mask(json_encode($messagearray));
				websock_php_sendMessage($socket, $data);
			}
		}
	}

	/**
	 * END - IPC stuff
	 */
}
?>
