<?php
/**
 * Looks for needle to get version with is numeric or decimal after needle text.
 * (e.g. Firefox/95.0)
 * - $n: needle to look for, including any spaces or slashes before the numbers.
 * - $f: flags to add to the regexp.
 * - $ua: useragent string.
 * Returns either full number (3 or 10, etc.) or subversion (3.5 or 10.1, etc.)
 *	or false on fail.
 */
function useragent_php_getVersion($n, $f, $ua)
{
	$ver = false;
	$needle = '/'.addcslashes($n, '/').'(\d+)[._](\d+)/'.$f;
	if (preg_match($needle, $ua, $m)) {
		$ver = $m[1];
		if (intval($m[2])) {
			$ver .= '.'.$m[2];
		}
	}

	return $ver;
}

function useragent_php_parse($ua)
{
	$br = false;
	$brver = false;
	$os = false;
	$osver = false;

	/**
	 * Browser matching
	 */
	if ((strpos($ua, 'Opera/') !== false)
			|| (strpos($ua, 'Opera ') !== false)
			|| (strpos($ua, ' OPR/') !== false)) {
		$br = 'Opera';

		if (preg_match('/OPR\/(\d+)/', $ua, $m)
				|| preg_match('/Opera (\d+)/', $ua, $m)
				|| preg_match('/Version\/(\d+)/', $ua, $m)) {
			$brver = $m[1];
		}
		goto getOS;
	}

	if (strpos($ua, 'AppleWebKit') !== false) {
		$br = 'WebKit';
	}

	if ((strpos($ua, 'FBAN/FBIOS') !== false)
			|| (strpos($ua, 'FB4A') !== false)) {
		$br = 'Facebook App';

		if (preg_match('/FBAV\/(\d+).(\d+)/', $ua, $m)
				|| preg_match('/FBSV\/(\d+).(\d+)/', $ua, $m)) {
			$brver = $m[1];
			if (intval($m[2])) {
				$brver .= '.'.$m[2];
			}
		}
		goto getOS;
	}

	if (strpos($ua, 'Instagram') !== false) {
		$br = 'Instagram App';
		$brver = useragent_php_getVersion('Instagram ', false, $ua);
		goto getOS;
	}

	if (strpos($ua, 'curl') !== false) {
		$br = 'cURL';
		$brver = useragent_php_getVersion('curl/', false, $ua);
		goto getOS;
	}

	if (strpos($ua, 'Wget') !== false) {
		$br = 'Wget';
		$brver = useragent_php_getVersion('Wget/', false, $ua);
		goto getOS;
	}

	if (strpos($ua, 'MicroMessenger') !== false) {
		$br = 'WeChat';
		$brver = useragent_php_getVersion('MicroMessenger/', false, $ua);
		goto getOS;
	}

	if (strpos($ua, 'PaleMoon') !== false) {
		$br = 'PaleMoon';
		$brver = useragent_php_getVersion('PaleMoon/', false, $ua);
		goto getOS;
	}

	if (strpos($ua, 'SamsungBrowser') !== false) {
		$br = 'Samsung Browser';
		$brver = useragent_php_getVersion('SamsungBrowser/', false, $ua);
		goto getOS;
	}

	if ((strpos($ua, 'Edge') !== false)
			|| (strpos($ua, 'Edg/') !== false)) {
		$br = 'Edge';

		if (preg_match('/Edge\/(\d+)\./', $ua, $m)
				|| preg_match('/Edg\/(\d+)\./', $ua, $m)) {
			$brver = $m[1];
		}
		goto getOS;
	}

	if (strpos($ua, 'Chromium') !== false) {
		$br = 'Chromium';
		$brver = useragent_php_getVersion('Chromium/', false, $ua);
		goto getOS;
	}

	if (strpos($ua, 'Chrome') !== false) {
		$br = 'Chrome';
		$brver = useragent_php_getVersion('Chrome/', false, $ua);
		goto getOS;
	}

	if (strpos($ua, 'Firefox') !== false) {
		$br = 'Firefox';
		$brver = useragent_php_getVersion('Firefox/', false, $ua);
		if (intval($brver) > 3) {
			$brver = intval($brver);
		}
		goto getOS;
	}

	if (strpos($ua, 'Lynx/') !== false) {
		$br = 'Lynx';
		$brver = useragent_php_getVersion('Lynx/', false, $ua);
		goto getOS;
	}

	if (strpos($ua, 'Minefield/') !== false) {
		$br = 'Minefield';
		$brver = useragent_php_getVersion('Minefield/', false, $ua);
		goto getOS;
	}

	if (strpos($ua, 'Safari') !== false) {
		$br = 'Safari';
		$brver = useragent_php_getVersion('Version/', false, $ua);
		if (!$brver) {
			$brver = useragent_php_getVersion('Safari/', false, $ua);
		}
		goto getOS;
	}

	if (strpos($ua, 'SamsungBrowser/') !== false) {
		$br = 'SamsungBrowser';
		$brver = useragent_php_getVersion('SamsungBrowser/', false, $ua);
		goto getOS;
	}

	if (strpos($ua, 'Thunderbird') !== false) {
		$br = 'Thunderbird';
		$brver = useragent_php_getVersion('Thunderbird/', false, $ua);
		goto getOS;
	}

	if (strpos($ua, 'MSIE') !== false) {
		$br = 'Internet Explorer';

		if (preg_match('/MSIE (\d+(.\d+))/', $ua, $m)) {
			echo "\nMSIE version = ".$m[1]."\n";
			switch($m[1]) {
			case '5.0':
			case '5.01': $brver = '5'; break;
			case '6.0': $brver = '6'; break;
			case '7.0': $brver = '7'; break;
			case '8.0': $brver = '8'; break;
			case '9.0': $brver = '9'; break;
			case '10.0': $brver = '10'; break;
			case '11': $brver = '11'; break;
			}
		}
	}

	if (strpos($ua, 'Trident/') !== false) {
		$br = 'Internet Explorer';

		if (preg_match('/Trident\/(\d+(.\d+))/', $ua, $m)) {
			switch ($m[1]) {
			case '4.0': $brver = '8'; break;
			case '5.0': $brver = '9'; break;
			case '6.0': $brver = '10'; break;
			case '7.0': $brver = '11'; break;
			}
		}
		goto getOS;
	}

	if (strpos($ua, 'Outlook') !== false) {
		$br = 'Outlook';
	}

	if (strpos($ua, 'YaBrowser') !== false) {
		$br = 'Yandex Browser';
		$brver = useragent_php_getVersion('YaBrowser/', false, $ua);
	}

	/**
	 * Bots
	 * Most of these don't need to report an OS.
	 */
	if (strpos($ua, 'AhrefsBot') !== false) {
		$br = 'AhrefsBot';
		$brver = useragent_php_getVersion('AhrefsBot/', false, $ua);
		goto sendNow;
	}

	if (strpos($ua, 'Applebot') !== false) {
		$br = 'Applebot';
		$brver = useragent_php_getVersion('Applebot/', false, $ua);
		goto sendNow;
	}

	if (strpos($ua, 'ia_archiver') !== false) {
		$br = 'Alexa crawler';
		goto sendNow;
	}

	if (strpos($ua, 'Baiduspider') !== false) {
		$br = 'Baiduspider';
		$brver = useragent_php_getVersion('Baiduspider/', false, $ua);
		goto sendNow;
	}

	if (stripos($ua, 'Bingbot') !== false) { /* Case insensitive */
		$br = 'Bingbot';
		$brver = useragent_php_getVersion('Bingbot/', 'i', $ua);
		goto sendNow;
	}

	if (strpos($ua, 'DuckDuckBot') !== false) {
		$br = 'DuckDuckBot';
		$brver = useragent_php_getVersion('DuckDuckBot/', false, $ua);
		goto sendNow;
	}

	if (strpos($ua, 'Exabot') !== false) {
		$br = 'Exabot';
		$brver = useragent_php_getVersion('Exabot/', false, $ua);
		goto sendNow;
	}

	if (strpos($ua, 'facebookexternalhit') !== false) {
		$br = 'Facebook external hit';
		$brver = useragent_php_getVersion('facebookexternalhit/', false, $ua);
		goto sendNow;
	}

	if (strpos($ua, 'Googlebot') !== false) {
		$br = 'Googlebot';
		$brver = useragent_php_getVersion('Googlebot/', false, $ua);
		goto sendNow;
	}

	if ((strpos($ua, 'Sogou') !== false)
			&& (stripos($ua, 'spider') !== false)) {
		$br = 'Sogou Spider';
		/* Pass case-insensitive flag ('i') below */
		$brver = useragent_php_getVersion('spider/', 'i', $ua);
		goto sendNow;
	}

	if (strpos($ua, 'Yahoo! Slurp') !== false) {
		$br = 'Yahoo! Slurp bot';
		goto sendNow;
	}

	if (strpos($ua, 'YandexBot') !== false) {
		$br = 'YandexBot';
		$brver = useragent_php_getVersion('YandexBot/', false, $ua);
		goto sendNow;
	}

	getOS:

	/**
	 * Operating system matching
	 */
	if ((strpos($ua, 'iOS') !== false)
			|| (strpos($ua, 'iPad') !== false)) {
		$os = 'iOS';
		goto sendNow;
	}

	if (strpos($ua, 'iPhone') !== false) {
		$os = 'iOS';
		$osver = useragent_php_getVersion('iPhone OS ', false, $ua);
		goto sendNow;
	}

	if (strpos($ua, 'Android') !== false) {
		$os = 'Android';
		$osver = useragent_php_getVersion('Android[- ]*', false, $ua);
		goto sendNow;
	}

	if (strpos($ua, 'CrOS') !== false) {
		$os = 'ChromeOS';
		goto sendNow;
	}

	if (strpos($ua, 'Ubuntu') !== false) {
		$os = 'Ubuntu';
		goto sendNow;
	}

	if ((strpos($ua, 'Linux') !== false)
			|| (strpos($ua, 'linux-') !== false)) {
		$os = 'Linux';
		goto sendNow;
	}

	if (strpos($ua, 'Mac OS X') !== false) {
		$os = 'macOS';
		$osver = useragent_php_getVersion('OS X ', false, $ua);
		goto sendNow;
	}

	if (strpos($ua, 'Windows') !== false) {
		$os = 'Windows';

		if (preg_match('/Windows NT (\d+.\d+)/', $ua, $m)) {
			switch($m[1]) {
			case '5.0': $osver = '2000'; break;
			case '5.1': $osver = 'XP'; break;
			case '6.0': $osver = 'Vista'; break;
			case '6.1': $osver = '7'; break;
			case '6.2': $osver = '8'; break;
			case '6.3': $osver = '8.1'; break;
			case '10.0': $osver = '10'; break;
			}
		}
		if (preg_match('/Windows (\d+)/', $ua, $m)) {
			switch($m[1]) {
			case '98': $osver = '98'; break;
			}
		}
	}

	sendNow:

	$arr = [
		'browser' => $br,
		'browserVersion' => $brver,
		'os' => $os,
		'osVersion' => $osver
	];
	return $arr;
}
?>
