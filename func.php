<?php
/**
 * Mirror Island - A small and efficient collaborative network.
 * Copyright (C) 2024
 * Sam and Janis Allen
 * samjanis@mirrorisland.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Inter-process communication message key (IPCMSGKEY).
 * IPCMSGKEY + 0 is the message queue key for index.php,
 * anything that is IPCMSGKEY + 1 or more is accountID */
define('IPCMSGKEY', 2**20);
define('IPCMSGTYPE', 1); /* Used for IPC with backend.php */

/* Connect to the database. */
require('connectdbinfo.php');
require('useragent.php');

/* Load markdown parser */
require('Parsedown.php');

$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname)
		or die("Could not connect: ".mysqli_connect_error());
$mysqli->set_charset('utf8mb4');

/* The following line is used to check if AWS has already been require()'d
 * to prevent "PHP Fatal error: Cannot redeclare Aws constantly()" */
$awsloaded = false;

/**
 * Set up a function to handle database calls.
 */
function func_php_query($query)
{
	global $mysqli;
	$result = mysqli_query($mysqli, $query) or die ("MariaDB/MySQL error ".mysqli_errno($mysqli)
			.": ".mysqli_error($mysqli)."\n<br>\n$query");

	return $result;
}

function func_php_escape($string)
{
	global $mysqli;

	return mysqli_real_escape_string($mysqli, $string);
}

/**
 * Clean text input from forms or other user input.
 * TODO: Remove leading/trailing <br> tags.
 */
function func_php_cleantext($string, $stringlength)
{
	if (!$string)
		return '';

	return substr(trim(stripslashes($string)), 0, $stringlength);
}

/**
 * Obtain integer values from a string.
 */
function func_php_inttext($string)
{
	if (!$string)
		return '';

	$inttext = preg_replace('/[^0-9\-]/', '', $string);
	$inttext = substr($inttext, 0, 12);
	$inttext = intval($inttext);
	return $inttext;
}

function func_php_stringtosearchcache($string)
{
	$str = preg_replace('/\<br\>/', ' ', $string); /* Convert <br>s to spaces. */
	$str = strip_tags($str);
	$str = preg_replace('/&amp;/', '&', $str); /* Remove HTML entity codes. */
	$str = preg_replace('/&[^&;]+;/', ' ', $str); /* Remove HTML entity codes. */
	$str = preg_replace('/[^0-9a-zA-Z\+\&\-\!\?\'\"\.\(\)\$\#\,\[\]]/', ' ', $str); /* Remove non-viewable characters. */
	$str = preg_replace('/\s+[\'\.]+\s+/', ' ', $str); /* Remove floating punctuation marks. */
	$str = preg_replace('/\s+/', ' ', $str); /* Collapse spaces. */
	$str = trim($str);

	return $str;
}

/**
 * Minimal function markdown formatter.
 * Accepts an HTML string and returns its markdown equivalent.
 */
function func_php_htmltomarkdown($string)
{
	$newstring = $string; /* Keep original */

	/* Convert divs to br - Part 1, Blank line divs */
	$match = '/<div>\s*?<br>\s*?<\/div>/i';
	$replace = "\n";
	$newstring = preg_replace($match, $replace, $newstring);

	/* Convert divs to br - Part 2, Regular divs */
	$match = '/<div>(.+?)<\/div>/i';
	$replace = "\${1}";
	$newstring = preg_replace($match, $replace, $newstring);

	/* Convert br to newlines and standardise other newlines */
	$newstring = str_replace(array('<br>', "\r\n", "\r"), "\n", $newstring);

	/* Remove specific HTML entities */
	$newstring = str_replace('&nbsp;', "\xc2\xa0", $newstring);
	$newstring = str_replace('&gt;', '>', $newstring);
	$newstring = str_replace('&lt;', '<', $newstring);
	$newstring = str_replace('&amp;', '&', $newstring);

	/* Convert links and images to their markdown equivelant.
	 * NOTE: Converted links can't contain line breaks
	 * (At least in this simplified version of markdown, anyway.) */
	$match = '/\<a href=\"([^"]+?)\"\>([^\<]+?)\<\/a\>/';
	$replace = '[${2}](${1})';
	$newstring = preg_replace($match, $replace, $newstring);

	return $newstring;
}

function func_php_markdowntohtml($text)
{
	/* Convert divs to br - Part 1, Blank line divs */
	$match = '/<div>\s*?<br>\s*?<\/div>/i';
	$replace = "\n";
	$text = preg_replace($match, $replace, $text);

	/* Convert divs to br - Part 2, Regular divs */
	$match = '/<div>(.+?)<\/div>/i';
	$replace = "\${1}";
	$text = preg_replace($match, $replace, $text);

	/* Convert br to newlines and standardise other newlines */
	$text = str_replace(array("<br>", "\r\n", "\r"), "\n", $text);

	/* Remove specific HTML entities */
	$text = str_replace('&nbsp;', "\xc2\xa0", $text);
	$text = str_replace('&amp;', '&', $text);
	$text = str_replace("\n&gt; ", "\n> ", $text);

	$pd = new Parsedown();
	$pd->setBreaksEnabled(true);
	$html = $pd->text($text);

	/* Is this a bug with Parsedown? <code> blocks end up with double-amp
	 * encoded symbols. Change it here. */
	$html = str_replace('&amp;gt;', '&gt;', $html);
	$html = str_replace('&amp;lt;', '&lt;', $html);

	return $html;
}

/**
 * Modified version of above but for lazy-loading images by swapping the
 * img src and lazy-src at page load, and swapping them back when in view.
 */
function func_php_markdowntohtml_lazyload($html)
{
	$html = func_php_markdowntohtml($html);

	$pattern = '/(<\s*?img)([^>]*)src="([^"]*)"([^>]*)lazy-src="([^"]*)"([^>]*?)(>)/si';
	$replace = '$1$2src="$5"$4lazy-src="$3"$6$7';
	$html = preg_replace($pattern, $replace, $html);

	return $html;
}

/**
 * Some characters, especially with emails, need to be changed
 * to their HTML counterparts.
 */
function func_php_plaintexttohtml($string)
{
	$string = str_replace('&', '&amp;', $string);
	$string = str_replace('<', '&lt;', $string);
	$string = str_replace('>', '&gt;', $string);
	$string = str_replace("\xc2\xa0", '&nbsp;', $string); /* Convert UTF-8 nbsp's */
	$string = str_replace(["\r\n", "\r", "\n"], '<br>', $string);

	return $string;
}

/**
 * Gets a MIME file type and return its associated extension with $pre prepended.
 * Returns FALSE if no match.
 */
function func_php_mimeext($pre, $mime)
{
	switch ($mime) {
	case 'image/gif':
		return $pre.'gif';

	case 'application/gzip':
		return $pre.'gz';

	case 'image/jpg':
	case 'image/jpeg':
		return $pre.'jpg';

	case 'image/png':
		return $pre.'png';

	case 'image/webp':
		return $pre.'webp';

	default:
		error_log('func_php_mimeext() Unknown mime: '.$mime);
		return FALSE;
	}
}

/**
 * Generates a GD Image from a given file path for further processing.
 * - $filepath: local file path to image resource (e.g. /tmp/hXtRdE)
 * - $mime: string to return MIME value, if valid.
 * Returns the GD Image resource.
 */
function func_php_filetogdimage($filepath, &$mime)
{
	$mime = mime_content_type($filepath);

	$gdimg = false;
	if (($mime === 'image/jpeg') || ($mime === 'image/jpg')) {
		$gdimg = imagecreatefromjpeg($filepath);
	} else if ($mime === 'image/png') {
		$gdimg = imagecreatefrompng($filepath);
	} else if ($mime === 'image/gif') {
		$gdimg = imagecreatefromgif($filepath);
	} else if ($mime === 'image/webp') {
		$gdimg = imagecreatefromwebp($filepath);
	}

	return $gdimg;
}

/**
 * Converts an SI-suffixed unit to bytes (eg. 1k to 1024, etc.)
 */
function func_php_bytes($bytes)
{
	$bytes = trim($bytes);
	$suffix = strtolower($bytes[strlen($bytes)-1]);
	$bytes = substr($bytes, 0, strlen($bytes)-1);
	switch ($suffix) {
	case 'g':
		$bytes *= 1024;
	case 'm':
		$bytes *= 1024;
	case 'k':
		$bytes *= 1024;
	}
	return func_php_inttext($bytes);
}

/**
 * Compares $username with $password.
 * Returns account.id as a result if successful, otherwise returns false.
 */
function func_php_verifypassword($username, $password)
{
	global $mysqli;

	/* First fetch the password - it contains the salt! */
	$query = sprintf('select accounts.id, passwords.password'
			.' from accounts inner join passwords on accounts.id = passwords.accountid'
			.' where accounts.username = "%s" limit 1;', func_php_escape($username));
	$result = func_php_query($query);
	if (mysqli_num_rows($result) != 1) {
		return false;
	}

	/* Now, salt the provided password and compare. */
	$r = mysqli_fetch_assoc($result);
	$savedpassword = $r['password'];
	$passwordhash = crypt($password, $savedpassword);
	if ($passwordhash == $savedpassword) {
		return $r['id'];
	}

	return false;
}

/**
 * Delete a file, checking if it was saved on S3 storage or the web host.
 * Requires a table row array from `files` itself for S3 checks.
 */
function func_php_deleteFile($r)
{
	global $awsloaded;

	if (!$r) {
		return false;
	}

	$fileindex = $r['fileindex'];
	if (!$fileindex) {
		error_log('func.php:'.__LINE__.' invalid file to delete $r='.print_r($r, TRUE));
		return false;
	}

	if ($r['s3profile'] && $r['s3endpoint']
			&& $r['s3region'] && $r['s3bucket']) {
		/* Delete from cloud storage */
		if (!$awsloaded) {
			require 'aws/aws-autoloader.php';
			$awsloaded = true;
		}

		$sdk = new Aws\Sdk([
			'profile' => $r['s3profile'],
			'endpoint' => $r['s3endpoint'],
			'region' => $r['s3region'],
			'version' => 'latest',
			'use_path_style_endpoint' => true,
		]);
		$s3 = $sdk->createS3();

		try {
			$s3->deleteObject([
				'Bucket' => $r['s3bucket'],
				'Key' => $fileindex,
			]);
		} catch (S3Exception $e) {
			error_log($e->getMessage());
		}
	} else {
		/* Delete from local server */
		unlink('files/'.$fileindex);
	}
}

/**
 * Set account file space, deleting files if necessary.
 */
function func_php_setAccountType($userid, $accountgb)
{
	/* Defaults; used for free plans. */
	$filespace = func_php_bytes('100m');
	$filesize = func_php_bytes('10m');
	$postsize = func_php_bytes('10m');

	/* Overwrite if paid plan active. */
	if ($accountgb > 0) {
		$fstxt = sprintf('%dg', $accountgb);
		$filespace = func_php_bytes($fstxt);
		$postsize = $filespace;
		$filesize = func_php_bytes('4g');
		if ($filesize > $filespace) {
			$filesize = $filespace;
		}
	}

	$query = sprintf('/* '.__LINE__. ' */ update accounts'
			.' set filespace=%d, filesize=%d, postsize=%d' /* SPACE, FILESIZE, POSTSIZE */
			.' where accounts.id = %d' /* USERID */
			.' limit 1',
			$filespace, $filesize, $postsize, $userid);
	func_php_query($query);

	/* Crop any files that exceed file space */
	$query = sprintf('select sum(filesize) as sumfilesize from files'
			.' where files.accountid = %d;', /* USERID */
			$userid);
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);
	$sumfilesize = $r['sumfilesize'];

	$numrows = 1;
	while (($sumfilesize > $filespace) && ($numrows)) {
		$query = sprintf('select id, filesize, fileindex from files'
				.' where files.accountid = %d' /* USERID */
				.' order by files.id asc limit 100;', $userid);
		$result = func_php_query($query);
		$numrows = mysqli_num_rows($result);
		if (($sumfilesize > $filespace) && ($numrows)) {
			$idremovearray = array();
			while (($sumfilesize > $filespace)
					&& ($r = mysqli_fetch_assoc($result))) {
				/* TODO:ERRORCHECK - only delete database entries
				 * on successful delete. */
				func_php_deleteFile($r);
				$idremovearray[] = $r['id'];
				$sumfilesize -= $r['filesize'];
			}

			if (count($idremovearray)) {
				$idlist = implode(' ,', $idremovearray);
				$query = sprintf('/* '.__LINE__.' */ delete from files'
						.' where files.accountid = %d' /* USERID */
						.' and files.id in (%s)', /* IDLIST */
						$userid, $idlist);
				func_php_query($query);
			}
		}
	}
}

/**
 * Set analytics data for a specific page (post id or profile page).
 * - $pageid: Required. Either numeric postid or username for analytics.
 * - $phpsessionid: Required. Usually from PHP's session_id().
 * - $httpuseragent: Optional. From $_SERVER['HTTP_USER_AGENT'].
 * - $remoteaddr: Optional. From $_SERVER['REMOTE_ADDR'].
 * - $referrer: Optional. From $_SERVER['HTTP_REFERER'] or document.referrer.
 * - $windowWidth: Optional. Also sets JS enabled for that visitor.
 * - $action: Optional. Used to sew what action was taken place, such as 'view',
 *	'edit', 'subscribe', or 'reply'. Defaults to 'view' if not set.
 * - $authorised: Required. Boolean value. True if view/edit/subscribe success.
 *	False if not permitted to view/edit/subscribe.
 *	NOTE: If $authorised is -1, it is from a javascript call
 *	and CAN NOT be used for an insert SQL statement.
 */
function func_php_setAnalytics($pageid, $phpsessionid, $httpuseragent, $remoteaddr,
		$referrer, $windowWidth, $action, $authorised)
{
	$browser = '';
	$browserVersion = '';
	$operatingSystem = '';
	$operatingSystemVersion = '';
	if ($httpuseragent) {
		$ua = useragent_php_parse($httpuseragent);
		$browser = $ua['browser'];
		$browserVersion = $ua['browserVersion'];
		$operatingSystem = $ua['os'];
		$operatingSystemVersion = $ua['osVersion'];
	}

	$countryCode = '';
	$countryName = '';
	if ($remoteaddr && preg_match('/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/', $remoteaddr, $addr)) {
		$addr1 = $addr[1];
		$addr2 = $addr[2];
		$addr3 = $addr[3];
		$addr4 = $addr[4];
		$addrInt = ($addr1 * 16777216) + ($addr2 * 65536) + ($addr3 * 256) + $addr4;

		$query = sprintf('select * from geoip'
				.' where %d between addrstart and addrend limit 1;', /* INT-IP */
				$addrInt);
		$result = func_php_query($query);
		if (mysqli_num_rows($result) == 1) {
			$r = mysqli_fetch_assoc($result);
			$countryCode = $r['countrycode'];
			// Currently unused: $countryName = $r['countryname'];
		}
	}

	switch ($action) {
	case 'view':
		break; /* Valid action. Keep this. */
	default:
		$action = 'view';
	}

	$query = sprintf('select id from analytics'
			.' where pageid="%s" and phpsessionid="%s"' /* PAGEID, SESSID */
			.' and added > NOW() - INTERVAL 2 HOUR limit 1;',
			func_php_escape($pageid), func_php_escape($phpsessionid));
	$result = func_php_query($query);
	/* Insert new analytic entry */
	if (mysqli_num_rows($result) === 0) {
		if ($authorised === -1) {
			return 'ERROR_ANALYTICS_INVALID_JS_CALL';
		}
		/* Country is NULL if not set, otherwise add quote marks for query below */
		$country = 'NULL';
		if ($countryCode) {
			$country = '"'.func_php_escape($countryCode).'"';
		}
		/* Get owner account id, if post id given */
		$acctid = 0;
		if (preg_match('/^[0-9]+$/', $pageid, $matches) === 1) {
			$postid = func_php_inttext($matches[0]);
			$query = sprintf('select accountid from posts'
					.' where posts.id = %d limit 1;', /* POSTID */
					$postid);
			$result = func_php_query($query);
			if (mysqli_num_rows($result)) {
				$r = mysqli_fetch_assoc($result);
				$acctid = $r['accountid'];
			}
		}

		$query = sprintf('insert into analytics (pageid, owneraccountid, phpsessionid,'
				.' added, weekday, referrer, country, browser, browserversion,'
				.' operatingsystem, operatingsystemversion,'
				.' action, authorised)'
				.' values ("%s", %d, "%s", NOW(), DAYOFWEEK(NOW()),' /* PAGEID, ACCTID, PHPSESSID */
				.' "%s", %s,' /* REFERRER, COUNTRY(is NULL or pre-quoted) */
				.' "%s", "%s",' /* BROWSER, BROWSER-VER */
				.' "%s", "%s", "%s", %d)', /* OS, OS-VER, ACTION, AUTHORISED */
				func_php_escape($pageid), $acctid, func_php_escape($phpsessionid),
				func_php_escape($referrer), $country, /* $country is 'NULL' or pre-quoted */
				func_php_escape($browser), func_php_escape($browserVersion),
				func_php_escape($operatingSystem), func_php_escape($operatingSystemVersion),
				func_php_escape($action), $authorised);
		func_php_query($query);
		return 'INFO_ANALYTICS_INSERTED';
	}

	/* Otherwise update analytic entry */
	$r = mysqli_fetch_assoc($result);
	$id = $r['id'];
	$windowWidth = func_php_inttext($windowWidth);

	if ($windowWidth) {
		$query = sprintf('update analytics set updated = NOW(),'
				.' windowwidth = %d, javascriptenabled = true' /* SCREENWIDTH */
				.' where id=%d limit 1;', /* ID */
				$windowWidth, $id);
	} else {
		$query = sprintf('update analytics set updated = NOW(),'
				.' javascriptenabled = false'
				.' where id=%d limit 1;', /* ID */
				$id);
	}
	func_php_query($query);

	/* Update the referrer fetched from javascript, if one given */
	if ($referrer) {
		$query = sprintf('select referrer from analytics'
				.' where id=%d limit 1', $id); /* ID */
		$result = func_php_query($query);
		$r = mysqli_fetch_assoc($result);
		if (!$r['referrer'] || ($r['referrer'] == '')) {
			$query = sprintf('update analytics set referrer = "%s"' /* REFERRER */
					.' where id = %d limit 1', /* $ID */
					func_php_escape($referrer), $id);
			func_php_query($query);
		}
	}
	return 'INFO_ANALYTICS_UPDATED';
}

/**
 * Takes a string and returns an array of lower-case words.
 * The string _should_ be pre-processed from func_php_stringtosearchcache.
 * If $removestopwords is true, common words will be left out of the array.
 */
function func_php_stringtokeywordarray($string, $removestopwords)
{
	/* Stopwords: A list of words to be (optionally) removed. */
	$stopwords = explode(' ', 'a an and are as at be by for from had has he in is'
			.' it its of on she that the to we were was were will with');

	$str = preg_replace('/[^0-9a-zA-Z\']/', ' ', $string); /* Remove all non-keyword characters. */
	$str = preg_replace('/\'/', '', $str); /* Trim some apostrophe marks. */
	$str = preg_replace('/\b.{1,2}\b/', ' ', $str); /* Remove words or numbers less than 3 numbers big. */
	$str = preg_replace('/\s+/', ' ', $str); /* Collapse spaces. */
	$str = explode(' ', mb_strtolower(trim($str)));
	$str = array_unique($str);

	if (!$removestopwords)
		return $str; /* Keep stopwords instead of removing them. */

	$arr = array_diff($str, $stopwords);

	return $arr;
}

/**
 * Sends a POST request to bgfunc.php without waiting for a response.
 * - $dataarray: an array that needs at least one entry;
 *	'f' => 'functionToRun'
 *	where functionToRun is the name of a function in bgfunc.php.
 *	The secret passcode 'p' is automatically added in here.
 *	Other array entries can be put in as needed, such as 'id' => for postid
 *	or 'deleteflag' => as 'true' or 'false'.
 */
function func_php_sendbgfunc($dataarray)
{
	global $bgfuncpass, $urlhome;

	/* Send $bgfuncpass as $_POST['p'] */
	$dataarray['p'] = $bgfuncpass;
	$postdata = http_build_query($dataarray);
	$options = array(
		'http' => array(
			'method' => 'POST',
			'header' => 'Content-type: application/x-www-form-urlencoded',
			'content' => $postdata,
			'timeout' => 1 // seconds
		)
	);
	$streamcontext = stream_context_create($options);
	/* Supress errors with @ symbol for next line */
	@file_get_contents($urlhome.'/bgfunc.php',
			false, $streamcontext);
}

/**
 * Returns string with a new RFC 4122 complant v4 UUID.
 * Thanks to https://www.uuidgenerator.net/dev-corner/php for this func.
 */
function func_newV4UUID()
{
	$data = random_bytes(16);
	assert(strlen($data) == 16);

	/* Set version to 0100 */
	$data[6] = chr(ord($data[6]) & 0x0f | 0x40);

	/* Set bits 6-7 to 10 */
	$data[8] = chr(ord($data[8]) & 0x3f | 0x80);

	/* Return the 36 character UUID */
	return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

/**
 * Returns fuzzy string describing bytes in SI-suffixed units (eg. 1024 returns 1kB, etc.)
 */
function func_php_fuzzyfilesize($bytes)
{
	if ($bytes < 1024) {
		$bytes = round($bytes, 2);
		return "{$bytes} bytes";
	}

	$bytes /= 1024;
	if ($bytes < 1024) {
		$bytes = round($bytes, 2);
		return "{$bytes} KiB";
	}

	$bytes /= 1024;
	if ($bytes < 1024) {
		$bytes = round($bytes, 2);
		return "{$bytes} MiB";
	}

	$bytes /= 1024;
	$bytes = round($bytes, 2);
	return "{$bytes} GiB";
}

/**
 * Accepts a Unix timestamp in seconds and returns fuzzy time string.
 * - $datediff: timedate; time to represent in fuzzy format.
 * - $abbreviate: boolean; Use initials instead of full words. (eg. '1h' instead of '1 hour')
 * - $moredetail: boolean; Show finer time (eg. '1d 4h' instead of just '1d')
 */
function func_php_fuzzyTimestamp($datediff, $abbreviate, $moredetail)
{
	if ($datediff < 0)
		$datediff = -$datediff; /* Force positive difference. */

	if ($datediff < 60) {
		return 'now';
	}

	$date = '';
	$years = floor($datediff / (3600 * 24 * 365));
	if ($years > 0) {
		if (($years != 1) || ($abbreviate))
			$date .= $years;

		if ($abbreviate) {
			$date .= 'Y';
		} else if ($years == 1) {
			$date .= 'a year';
		} else {
			$date .= ' years';
		}
		if (!$moredetail)
			return $date;
		$datediff = $datediff % (3600 * 24 * 365);
	}

	if ($datediff > 0) {
		$months = floor($datediff / (3600 * 24 * ceil(365 / 12)));
		if ($months > 0) {
			if ($date) {
				$date .= ($abbreviate ? ' ' : ' and ');
				$datediff = 0; /* Don't add other fuzzy dates */
			} else {
				$datediff = $datediff % (3600 * 24 * ceil(365 / 12));
			}

			if (($months != 1) || ($abbreviate))
				$date .= $months;

			if ($abbreviate) {
				$date .= 'M';
			} else if ($months == 1) {
				$date .= 'a month';
			} else {
				$date .= ' months';
			}
			if (!$moredetail)
				return $date;
		}
	}

	if ($datediff > 0) {
		$weeks = floor($datediff / (3600 * 24 * 7));
		if ($weeks > 0) {
			if ($date) {
				$date .= ($abbreviate ? ' ' : ' and ');
				$datediff = 0; /* Don't add other fuzzy dates */
			} else {
				$datediff = $datediff % (3600 * 24 * 7);
			}

			if (($weeks != 1) || ($abbreviate))
				$date .= $weeks;

			if ($abbreviate) {
				$date .= 'w';
			} else if ($weeks == 1) {
				$date .= 'a week';
			} else {
				$date .= ' weeks';
			}
			if (!$moredetail)
				return $date;
		} else if ($date) {
			$datediff = 0; /* Don't add other fuzzy dates */
		}
	}

	if ($datediff > 0) {
		$days = floor($datediff / (3600 * 24));
		if ($days > 0) {
			if ($date) {
				$date .= ($abbreviate ? ' ' : ' and ');
				$datediff = 0; /* Don't add other fuzzy dates */
			} else {
				$datediff = $datediff % (3600 * 24);
			}

			if (($days != 1) || ($abbreviate))
				$date .= $days;

			if ($abbreviate) {
				$date .= 'd';
			} else if ($days == 1) {
				$date .= 'a day';
			} else {
				$date .= ' days';
			}
			if (!$moredetail)
				return $date;
		} else if ($date) {
			$datediff = 0; /* Don't add other fuzzy dates */
		}
	}

	if ($datediff > 0) {
		$hours = floor($datediff / 3600);
		if ($hours > 0) {
			if ($date) {
				$date .= ($abbreviate ? ' ' : ' and ');
				$datediff = 0; /* Don't add other fuzzy dates */
			} else {
				$datediff = $datediff % 3600;
			}

			if (($hours != 1) || ($abbreviate))
				$date .= $hours;

			if ($abbreviate) {
				$date .= 'h';
			} else if ($hours == 1) {
				$date .= 'an hour';
			} else {
				$date .= ' hours';
			}
			if (!$moredetail)
				return $date;
		} else if ($date) {
			$datediff = 0; /* Don't add other fuzzy dates */
		}
	}

	if ($datediff > 0) {
		$minutes = floor($datediff / 60);
		if ($minutes > 0) {
			if ($date)
				$date .= ($abbreviate ? ' ' : ' and ');

			if (($minutes != 1) || ($abbreviate))
				$date .= $minutes;

			if ($abbreviate) {
				$date .= 'm';
			} else if ($minutes == 1) {
				$date .= 'a minute';
			} else {
				$date .= ' minutes';
			}
			if (!$moredetail)
				return $date;
		}
	}
	return $date;
}

/**
 * Returns fuzzy string describing difference between date string and current time.
 * - $datestring: timedate; time to compare difference from now().
 * - $abbreviate: boolean; Use initials instead of full words. (eg. '1h' instead of '1 hour')
 * - $moredetail: boolean; Show finer time difference (eg. '1d 4h' instead of just '1d')
 */
function func_php_fuzzyTimeAgo($datestring, $abbreviate, $moredetail)
{
	$datetimestamp = strtotime($datestring);
	$date = func_php_fuzzyTimestamp($datetimestamp - time(), $abbreviate, $moredetail);

	if (!$abbreviate && ($date != 'now'))
		$date .= ' ago';

	if (!$date)
		$date = 'Unknown';

	return $date;
}

/**
 * Returns the short time, depending on whether it was 24 hours ago,
 * or a specific hh:mm time today.
 * - $datestring: timedate, usually from a database call.
 * - $ampm: boolean, appends 'AM' or 'PM' if true, and possible.
 * Returns a string of either time (12:34 PM),
 * 3-word month & date (21 Mar) or 3-word month & year (Mar 2024)
 */
function func_php_shortTime($datestring, $ampm)
{
	$datetimestamp = strtotime($datestring);
	$datediff = time() - $datetimestamp;

	$timeformat = 'H:i'; /* Default 24-hour time */
	if ($datediff > (3600 * 24 * 365)) {
		$timeformat = 'M Y'; /* Short month, year */
	} else if ($datediff > 3600 * 24) {
		$timeformat = 'j M'; /* Date, short month */
	} else if ($ampm) {
		$timeformat = 'g:i a'; /* 12-hour time with am/pm */
	}
	$shorttime = date($timeformat, $datetimestamp);

	return $shorttime;
}

/**
 * Variables: $postid, $userid - Permission matching for user(id) with post(id).
 * Returns false on error or invalid postid, otherwise an array containing:
 *	username for the post author from accounts,
 *	confirmedage for the specified userid,
 * 	accountid, parentid, all 6 *permissions, viewmode, agerating from the post itself,
 *	istrustedmember - member of any of the post's parent page(s),
 *	istrustedcontact - trusted contact of post author,
 *	isrecipient - flags if recipient for sender-recipient addressed posts.
 *	parentaccountid, parentreplypermissions, parentapprovepermissions from parentpost
 *	(if available, otherwise NULL)
 *	canview, canedit, canreply, preapproved, canmoderate - permission flags.
 *	flagcount, flagreason, flagagerating, flagactive, flagupdated.
 */
function func_php_getUserPermissions($postid, $userid)
{
	$userpermissions = array();

	$query = sprintf('select (select id from accounts where id = %d limit 1) as testuserid,' /* USERID */
	.' (select username from accounts where accounts.id = testuserid limit 1) as testusername,'
	.' accounts.username,'
	.' (select confirmedage from accounts as useraccount '
	.' 	where useraccount.id = testuserid limit 1) as confirmedage,'
	.' posts.id, posts.parentid, posts.accountid, posts.added,'
	.' posts.agerating, posts.viewpermissions, posts.editpermissions,'
	.' posts.replypermissions, posts.approvepermissions,'
	.' posts.moderatepermissions, posts.tagpermissions,'
	.' posts.viewmode, posts.sortmode, posts.sortreverse,'
	.' flags.flagcount, flags.flagreason, flags.flagagerating,'
	.' flags.flagactive, flags.updated as flagupdated,'
	.' (select count(*) from membertree left join members on membertree.parentid = members.postid'
	.' 	where (membertree.postid = posts.id or members.postid = posts.id)'
	.' 	and members.accountid = testuserid and members.trusted = 1)'
	.' 	as istrustedmember,'
	.' (select count(trusted) from contacts'
	.' 	where (contacts.accountid = posts.accountid or contacts.accountid = pp.accountid)'
	.' 	and contacts.contactid = testuserid and trusted = 1) as istrustedcontact,'
	.' (select count(recipientaccountid) from recipients'
	.' 	where recipients.tablename = "posts" and recipients.tableid = posts.id'
	.' 	and recipients.recipientaccountid = testuserid) as isrecipient,'
	.' pp.accountid as parentaccountid, pp.replypermissions as parentreplypermissions,'
	.' pp.approvepermissions as parentapprovepermissions'
	.' from posts left join accounts on posts.accountid = accounts.id'
	.' left join posts as pp on posts.parentid = pp.id'
	.' left join flags on flags.tablename = "posts" and flags.tableid = posts.id'
	.' where posts.id = %d limit 1;', /* POSTID */
	$userid, $postid);

	$result = func_php_query($query);
	if (mysqli_num_rows($result) != 1) {
		return false;
	}

	$userpermissions = mysqli_fetch_assoc($result);

	$canview = false;
	switch ($userpermissions['viewpermissions']) {
	case 5: /* Unsigned and email users */
		$canview = true;
		break;
	case 4: /* Signed in users */
		if ($userid) {
			$canview = true;
			break;
		}
	case 3: /* Group members */
		if ($userpermissions['istrustedmember']) {
			$canview = true;
			break;
		}
	case 2: /* Starred contacts */
		if ($userpermissions['istrustedcontact']) {
			$canview = true;
			break;
		}
	case 1: /* Recipients */
		if ($userpermissions['isrecipient']) {
			$canview = true;
			break;
		}
	default: /* Assume 0 - Owner */
		if (($userid) && ($userpermissions['accountid'] == $userid)) {
			$canview = true;
		}
	}

	$canmoderate = false;
	switch ($userpermissions['moderatepermissions']) {
	case 5: /* Unsigned and email users */
		$canmoderate = true;
		break;
	case 4: /* Signed in users */
		if ($userid) {
			$canmoderate = true;
			break;
		}
	case 3: /* Group members */
		if ($userpermissions['istrustedmember']) {
			$canmoderate = true;
			break;
		}
	case 2: /* Starred contacts */
		if ($userpermissions['istrustedcontact']) {
			$canmoderate = true;
			break;
		}
	case 1: /* Recipients */
		if ($userpermissions['isrecipient']) {
			$canmoderate = true;
			break;
		}
	default: /* Assume 0 - Owner */
		if (($userid) && ($userpermissions['accountid'] == $userid)) {
			$canmoderate = true;
		}
	}
	$userpermissions['canmoderate'] = $canmoderate;

	$confirmedage = $userpermissions['confirmedage'];

	/* Restrict age-related material */
	if ($confirmedage < 3) $confirmedage = 3; /* For accounts with no age set */
	if (($confirmedage < $userpermissions['agerating'])
			&& ($userpermissions['accountid'] != $userid)) {
		$canview = false;
	}

	/* Restrict flagged material */
	if ($userpermissions['flagactive']) {
		$showflaggedpost = false;
		/* Display flagged posts to owner/moderator only. */
		if (($userpermissions['accountid'] == $userid)
				|| $userpermissions['canmoderate']) {
			/* Owner always sees posts. */
			$showflaggedpost = true;
		} else {
			/* Only show appropriate posts depending on flag reason */
			switch ($userpermissions['flagreason']) {
			case 'MATURE_CONTENT':
			case 'ADULT_CONTENT':
				if ($confirmedage >= $userpermissions['flagagerating']) {
					$showflaggedpost = true;
				}
				break;
			case 'COPYRIGHT_OR_TRADEMARK':
			case 'ILLEGAL_OR_VIOLATION':
				break;
			default:
				/* Always hide except from owner. */
			}
		}
		if (!$showflaggedpost) {
			$canview = false;
		}
	}

	$userpermissions['canview'] = $canview;

	$canedit = false;
	switch ($userpermissions['editpermissions']) {
	case 5: /* Unsigned and email users */
		$canedit = true;
		break;
	case 4: /* Signed in users */
		if ($userid != 0) {
			$canedit = true;
			break;
		}
	case 3: /* Group members */
		if ($userpermissions['istrustedmember']) {
			$canedit = true;
			break;
		}
	case 2: /* Starred contacts */
		if ($userpermissions['istrustedcontact']) {
			$canedit = true;
			break;
		}
	case 1: /* Recipients */
		if ($userpermissions['isrecipient']) {
			$canedit = true;
			break;
		}
	default: /* Assume 0 - Owner */
		if (($userid != 0) && ($userpermissions['accountid'] == $userid)) {
			$canedit = true;
		}
	}
	$userpermissions['canedit'] = $canedit;

	$canreply = false;
	switch ($userpermissions['replypermissions']) {
	case 5: /* Unsigned and email users */
		$canreply = true;
		break;
	case 4: /* Signed in users */
		if ($userid != 0) {
			$canreply = true;
			break;
		}
	case 3: /* Group members */
		if ($userpermissions['istrustedmember']) {
			$canreply = true;
			break;
		}
	case 2: /* Starred contacts */
		if ($userpermissions['istrustedcontact']) {
			$canreply = true;
			break;
		}
	case 1: /* Recipients */
		if ($userpermissions['isrecipient']) {
			$canreply = true;
			break;
		}
	default: /* Assume 0 - Owner */
		if (($userid != 0) && ($userpermissions['accountid'] == $userid)) {
			$canreply = true;
		}
	}
	$userpermissions['canreply'] = $canreply;

	$preapproved = false;
	switch ($userpermissions['approvepermissions']) {
	case 5: /* Unsigned and email users */
		$preapproved = true;
		break;
	case 4: /* Signed in users */
		if ($userid != 0) {
			$preapproved = true;
			break;
		}
	case 3: /* Group members */
		if ($userpermissions['istrustedmember']) {
			$preapproved = true;
			break;
		}
	case 2: /* Starred contacts */
		if ($userpermissions['istrustedcontact']) {
			$preapproved = true;
			break;
		}
	case 1: /* Recipients */
		if ($userpermissions['isrecipient']) {
			$preapproved = true;
			break;
		}
	default: /* Assume 0 - Owner */
		if (($userid != 0) && ($userpermissions['accountid'] == $userid)) {
			$preapproved = true;
		}
	}
	$userpermissions['preapproved'] = $preapproved;

	return $userpermissions;
}

/**
 * Get the permissions a post has to its parent (or parents).
 * such as if it can reply, is pre-approved or has been approved.
 * Returns:
 * - accountid (for current post)
 * - parentid, parentaccountid, parentreplypermissions, parentapprovepermissions
 * - istrustedmember (of parent or ancestors)
 * - istrustedcontact (of parent accountid)
 * - isrecipient (of parent)
 * - preapproved (always true for posts without parents)
 * - approvalstrusted
 * - approvalshidden
 * - canreply
 * (Permissions TODO)
 * x isflagged
 * x ispinned
 */
function func_php_getPostPermissions($postid)
{
	$query = sprintf('select posts.accountid, pp.id as parentid, pp.accountid as parentaccountid,'
			.' pp.replypermissions as parentreplypermissions,'
			.' pp.approvepermissions as parentapprovepermissions,'
			.' (select count(*) from membertree left join members on membertree.parentid = members.postid'
			.' 	where (membertree.postid = posts.id or members.postid = posts.id)'
			.' 	and members.accountid = posts.accountid and members.trusted = 1) as istrustedmember,'
			.' (select count(trusted) from contacts'
			.' 	where (contacts.accountid = posts.accountid or contacts.accountid = pp.accountid)'
			.' 	and contacts.contactid = posts.accountid and trusted = 1) as istrustedcontact,'
			.' (select count(recipientaccountid) from recipients'
			.' 	where recipients.tablename = "posts" and recipients.tableid = pp.id'
			.' 	and recipients.recipientaccountid = posts.accountid) as isrecipient,'
			.' flags.flagcount, flags.flagactive, flags.updated as flagsupdated, pins.pinstate,'
			.' approvals.trusted as approvalstrusted, approvals.hidden as approvalshidden'
			.' from posts left join posts as pp on posts.parentid = pp.id'
			.' left join flags on flags.tablename = "posts" and flags.tableid = posts.id'
			.' left join approvals on approvals.tablename = "posts" and approvals.tableid = posts.id'
			.' left join pins on pins.postid = posts.id'
			.' where posts.id = %d limit 1;', /* POSTID */
			$postid);

	$result = func_php_query($query);
	if (mysqli_num_rows($result) != 1) {
		return false;
	}
	$permissionsarray = mysqli_fetch_assoc($result);

	$canreply = false;
	switch ($permissionsarray['parentreplypermissions']) {
	case null:
	case 5:
		$canreply = true;
		break;
	case 4:
		if ($permissionsarray['accountid'] != 0) {
			$canreply = true;
			break;
		}
	case 3:
		if ($permissionsarray['istrustedmember'] != 0) {
			$canreply = true;
			break;
		}
	case 2:
		if ($permissionsarray['istrustedcontact'] != 0) {
			$canreply = true;
			break;
		}
	case 1:
		if ($permissionsarray['isrecipient'] != 0) {
			$canreply = true;
			break;
		}
	default: /* Assume owner */
		if (($permissionsarray['accountid'] == $permissionsarray['parentaccountid'])
				&& ($permissionsarray['accountid'] != 0)) {
			$canreply = true;
			break;
		}
	}
	$permissionsarray['canreply'] = $canreply;

	$preapproved = false;
	switch ($permissionsarray['parentapprovepermissions']) {
	case null:
	case 5:
		$preapproved = true;
		break;
	case 4:
		if ($permissionsarray['accountid'] != 0) {
			$preapproved = true;
			break;
		}
	case 3:
		if ($permissionsarray['istrustedmember'] != 0) {
			$preapproved = true;
			break;
		}
	case 2:
		if ($permissionsarray['istrustedcontact'] != 0) {
			$preapproved = true;
			break;
		}
	case 1:
		if ($permissionsarray['isrecipient'] != 0) {
			$preapproved = true;
			break;
		}
	default: /* Assume owner */
		if (($permissionsarray['accountid'] == $permissionsarray['parentaccountid'])
				&& ($permissionsarray['accountid'] != 0)) {
			$preapproved = true;
			break;
		}
	}
	$permissionsarray['preapproved'] = $preapproved;

	return $permissionsarray;
}

/**
 * Return an array with links to attachments.
 */
function func_php_fileAttachmentArray($phylum, $postid)
{
	$query = sprintf('/* '.__LINE__.' */ select * from files where files.postid=%d' /* POSTID */
			.' and files.filemime not in ("image/gif", "image/jpeg", "image/png")'
			.' order by files.added desc;', $postid);
	$filesresult = func_php_query($query);
	if (mysqli_num_rows($filesresult) < 1) {
		return false;
	}

	$attachmentarray = array();
	$ct = 0;
	while (($ct < 100) && ($file = mysqli_fetch_assoc($filesresult))) {
		$fileid = $file['id'];
		$filename = $file['filename'];
		$filesize = $file['filesize'];
		$fuzzyfilesize = func_php_fuzzyfilesize($filesize);

		$id = "{$phylum}_{$postid}_attachment_{$fileid}";
		$innerHTML = "<span class=\"tooltiphost\">"
				."<a href=\"/index.php?fileid={$fileid}&filename={$filename}\">$filename</a>"
				."<span class=\"tooltiptext tooltiptexttop\">Download file</span></span> ($fuzzyfilesize)";
		$attachmentarray[] = array('id' => $id, 'innerHTML' => $innerHTML);
	}

	return $attachmentarray;
}

/**
 * Inserts a button link with bubble-style popup. Used mainly for linking to
 * pages and user profiles with additional button controls.
 * - $elementid: String used to identify the link.
 * - $buttonhtml: HTML to use for button label.
 * - $popuphtml: HTML for inside the popup.
 * - $is_inline: boolean false for sidepanel entry, true to display normally.
 * - $is_groupmember: boolean true to hilite inline member link.
 * - $username: (optional) value for doubleclick action.
 * - $postid: (optional) value for doubleclick action.
 * Returns HTML for the entire popup, including the parent div or span
 * containing the button and popup.
 */
function func_php_popupLinkHTML($elementid, $buttonhtml, $popuphtml,
		$is_inline, $is_groupmember, $username, $postid)
{
	$html = '';
	$popupid = uniqid("{$elementid}_");

	/* Set up the popup link's "parent" container */
	if ($is_inline) {
		if ($is_groupmember) {
			$html = "<span class=\"memberlink groupmemberlink\" id=\"{$elementid}\">";
		} else {
			$html = "<span class=\"memberlink\" id=\"{$elementid}\"> ";
		}
	} else {
		$html = "<div class=\"memberlink memberlinkuserpanel\" id=\"{$elementid}\">";
	}

	/* Set up the popup link's button */
	if ($is_inline) {
		$html .= "<button class=\"memberlinktitle\" onclick=\"index_js_popupDiv('$popupid');\">";
		$html .= '<span class="memberlinknowrap">';
	} else {
		$html .= "<button class=\"memberlinknowrap memberlinkuserpanelbutton\""
				." onclick=\"index_js_showContactListPopup(this, '{$popupid}');\""
				." ondblclick=\"index_js_message('{$username}', $postid);\">";
		$html .= '<div class="memberlinknowrap memberlinkblock">';
	}
	$html .= "{$buttonhtml}</span></button>";

	/* Set up the popup link's popup HTML contents */
	if ($is_inline) {
		$html .= "<div id=\"{$popupid}\" class=\"memberlinkdetails memberlinkdetailsinline\">";
	} else {
		$html .= "<div id=\"{$popupid}\" class=\"memberlinkdetails\">";
	}
	$html .= $popuphtml.'</div>';

	/* Close the popup link's "parent" container */
	if ($is_inline) {
		$html .= '</span>';
	} else {
		$html .= '</div>';
	}

	return $html;
}

/**
 * $username: if $postid is false then assume username of this server, otherwise
 * 	if $postid is set an email address was given (not username of this server)
 * $is_inline: set false for side bar, otherwise true for text body or title bars.
 * $elementid: sets element id of member link generated in this function.
 * $show_status: display status icon beside display name.
 * $groupid: post id of group page, if a trusted or requesting member.
 *	Also, only owner and moderators can see pending members.
 *	Memberlink returns NULL if member pending and activeuser is not owner/moderator.
 * $postid: for guest or email posts where the only identifier is the original post.
 */
function func_php_insertMemberLink($username, $is_inline, $show_status, $elementid,
		$groupid, $phylum, $postid, $activeuserid)
{
	global $urlhome, $servername;

	$groupmembertrusted = 0;
	$groupmemberownerid = 0;
	$statusimgsrc = '';
	$fullname = '';
	$emailaddress = '';
	$contactid = 0;
	$memberlinkhref = '';
	$pagelinktext = 'View Profile';
	$popupid = '';
	$buttonhtml = '';
	$popuphtml = '';
	$is_groupmember = false;
	$popuppostid = 0;

	if ($username) {
		$query = sprintf('/* '.__LINE__.' */ select accounts.*, profileimage.filename as profileimagefilename'
				.' from accounts left join files as profileimage on profileimage.id = accounts.profileimagefileid'
				.' where username="%s" limit 1;', func_php_escape($username)); /* USERNAME */
		$result = func_php_query($query);
		if (mysqli_num_rows($result) != 1) {
			return 'ERROR: '.$query; /* ERROR FOUND! What are we meant to do here? */
		}

		$r = mysqli_fetch_assoc($result);
		$contactid = $r['id'];
		$fullname = $r['fullname'];
		$statusimgsrc = "/img/status-offline12.png";
		$emailaddress = "{$r['username']}@{$servername}";
		if ($r['profileimagefilename'] && $r['profileimagefileid']) {
			$contactimgsrc = "/index.php?fileid={$r['profileimagefileid']}&filename={$r['profileimagefilename']}";
		} else {
			$contactimgsrc = '/img/defaultprofile.jpg';
		}

		$memberlinkhref = "{$urlhome}/{$username}";
	} else if ($postid) {
		$query = sprintf('select searchcache, emaildisplayname, emailusername, emailserver'
				.' from posts where posts.id = %d limit 1;', $postid);
		$result = func_php_query($query);
		if (mysqli_num_rows($result) != 1) {
			return 'ERROR: '.$query; /* ERROR FOUND! What are we meant to do here? */
		}

		$memberlinkhref = "{$urlhome}/{$postid}";
		if (!$groupid) /* Set $postid as fallback if no username/postid set later on */
			$popuppostid = $postid;

		$r = mysqli_fetch_assoc($result);
		$emailaddress = '';
		$contactimgsrc = '';
		$statusimgsrc = '';
		if ($r['emailusername'] && $r['emailserver']) {
			$emailaddress = $r['emailusername'].'@'.$r['emailserver'];
			$contactimgsrc = "/img/emailprofile.jpg";
			$statusimgsrc = "/img/status-email12.png";
			$username = $emailaddress;
			$pagelinktext = 'Show Emails';

			/* Fall back to email address if no display name given */
			$fullname = $r['emaildisplayname'];
			if (!$fullname) {
				$fullname = $emailaddress;
			}
		} else {
			$contactimgsrc = "/img/unknownprofile.jpg";
			$statusimgsrc = "/img/status-offline12.png";
			if ($is_inline) {
				$fullname = '[Unsigned Author]';
			} else if ($r['searchcache']) {
				$fullname = '<i>&quot;'.substr($r['searchcache'], 0, 80).'&quot;</i>';
			} else {
				$fullname = '<i>(Untitled Post)</i>';
			}
		}

	} else {
		$fullname = '<i>User Unknown</i>';
		$contactimgsrc = "/img/unknownprofile.jpg";
		$statusimgsrc = "/img/status-offline12.png";
	}

	$activeusername = '';
	if ($activeuserid) {
		$query = sprintf('select username from accounts where id = %d limit 1;', $activeuserid);
		$result = func_php_query($query);
		$r = mysqli_fetch_assoc($result);
		$activeusername = $r['username'];
	}

	/* Preprocess group member details here. */
	if (($groupid > 0) && ($username)) {
		$query = sprintf('select members.*, posts.accountid as ownerid from members, posts where members.postid = %d and members.accountid = %d'
				.' and posts.id = members.postid limit 1;', $groupid, $contactid);
		$groupmemberresult = func_php_query($query);
		if (mysqli_num_rows($groupmemberresult) == 1) {
			$groupmember = mysqli_fetch_assoc($groupmemberresult);
			$groupmembertrusted = $groupmember['trusted'];
			$groupmemberownerid = $groupmember['ownerid'];

			if ($contactid == $groupmemberownerid) {
				$statusimgsrc = "{$urlhome}/img/status-owner12.png";
			} else if ($groupmembertrusted == 1) {
				$statusimgsrc = "{$urlhome}/img/status-member12.png";
			} else {
				$permissions = func_php_getUserPermissions($groupid, $activeuserid);
				/* Can't display pending member to non-moderators. */
				if (!$permissions['canedit'] && !$permissions['canmoderate']
						&& ($activeusername != $username)) {
					return NULL;
				}
				$statusimgsrc = "{$urlhome}/img/status-pending12.png";
			}
		} else {
			$groupid = 0;
		}
	}

	/* Start building the actual member link here... */
	if ($is_inline) {
		if ($phylum == 'message') {
			$memberlink = " <span class=\"messagecredits\">$fullname</span> ";
			return $memberlink; /* Only need full name with messages. */
		} else if ($groupid > 0) {
			$is_groupmember = true;
		}
	}

	$popupid = 'memberlinkdetails_';
	if ($username) {
		$popupid .= $username;
	} else if ($postid) {
		$popupid .= $postid;
	}
	$popupid .= '_'.uniqid(); /* Assign random value as a fallback */

	if ($statusimgsrc && (!$is_inline || ($groupid > 0) || $show_status)) {
		$buttonhtml .= "<img id=\"{$elementid}_statusimg\" src=\"{$statusimgsrc}\" width=\"12\" height=\"12\">";
	}

	$buttonhtml .= $fullname;
	$popuphtml .= "<img src=\"$contactimgsrc\">";

	$popuphtml .= '<div class="memberlinkbuttons">';
	if ($username) {
		$popuphtml .= " <div><a class=\"greenbutton\" href=\"{$urlhome}/{$username}/\">{$pagelinktext}</a>";
		$popuphtml .= " <a class=\"silverbutton\" onclick=\"index_js_message('$username', $postid);\">Message</a></div>";
	} else if ($postid) {
		$popuphtml .= " <div><a class=\"greenbutton\" href=\"{$urlhome}/{$postid}\">View Origin Post</a></div>";
	}

	/* Add Contact/Friend/Starred links. */
	if (($activeusername) && ($username)
			&& (strtolower($username) != strtolower($activeusername))) {
		$query = sprintf('select trusted from contacts where accountid=%d'
				.' and contactid=%d limit 1;', $activeuserid, $contactid);
		$result = func_php_query($query);
		$association = 0; /* Neither trusted or contact. */
		if (mysqli_num_rows($result) == 1) {
			$r = mysqli_fetch_assoc($result);
			if ($r['trusted'] == 0)
				$association = 1;
			else
				$association = 2;
		}

		/* Suggest to add contact. */
		if ($association == 0) {
			$popuphtml .= "<a id='memberlinkaddremove_$username' class=\"silverbutton\" onclick=\"index_js_addContact('$username', false);\">Add Contact</a>";
		} else {
			$popuphtml .= "<a id='memberlinkaddremove_$username' class=\"silverbutton\" onclick=\"index_js_removeContact('$username');\">Remove</a>";
		}

		/* Suggest to add as trusted. */
		if ($association == 2) {
			$popuphtml .= " <a id='memberlinktrust_$username' class=\"silverbutton\" onclick=\"index_js_addContact('$username', false);\">Unstar</a>";
		} else {
			$popuphtml .= " <a id='memberlinktrust_$username' class=\"silverbutton\" onclick=\"index_js_addContact('$username', true);\">Star</a>";
		}

	}

	/* Add Group Member controls (if requested) */
	if ($groupid > 0) {
		$popuppostid = $groupid;
		$joinrequest = 0;
		$confirmtype = 0;
		$linktext = false;
		$showconfirmjoin = 0;
		if ($contactid == $groupmemberownerid) {
			/* Owner - Nothing To Do */
		} else if (($groupmembertrusted == 1) && ($contactid == $activeuserid)) {
			$confirmtype = 1;
			$linktext = "Leave Group";
		} else if (($groupmembertrusted == 1) && ($groupmemberownerid == $activeuserid)) {
			$linktext .= "Remove Member";
		} else if (($groupmemberownerid != $activeuserid) && ($contactid == $activeuserid)) {
			$linktext .= "Cancel Join Request";
		} else if ($groupmemberownerid == $activeuserid) {
			$linktext = "Cancel Join Request";
			$showconfirmjoin = 1;
		} else {
			/* NO PERMISSIONS */
		}

		if ($linktext) {
			$popuphtml .= "<div><a class=\"silverbutton\" onclick=\"index_js_joinGroup('{$phylum}', "
					."{$groupid}, {$contactid}, 0, 1);\">{$linktext}</a>";

			if ($showconfirmjoin) {
				$popuphtml .= " <a class=\"silverbutton\" onclick=\"index_js_joinGroup('{$phylum}', "
						."{$groupid}, {$contactid}, 1, 0);\">Confirm Join</a>";
			}

			$popuphtml .= "</div>";
		}
	}
	$popuphtml .= '</div>';

	$popuphtml .= " <div class=\"memberlinkemailaddress\">$emailaddress</div>";

	$memberlink = func_php_popupLinkHTML($elementid, $buttonhtml, $popuphtml,
			$is_inline, $is_groupmember, $username, $popuppostid);

	return $memberlink;
}

/**
 * - $phylum: either 'post' or 'message'.
 * - $postid: numerid ID.
 * - $is_inline: boolean false for sidepanel entry, true to display normally.
 * - $elementid: sets element id of new link generated in this function.
 * - $label: Optional text label, or False for substr(80) of post.
 * - $activeuserid: user to show this link to, changes depending on permissions.
 */
function func_php_insertPostLink($phylum, $postid, $is_inline, $elementid, $label, $activeuserid)
{
	global $urlhome, $servername;

	$invalidpostlink = false;

	/* Check for age-limited post link */
	$userpermissions = func_php_getUserPermissions($postid, $activeuserid);
	if (!$userpermissions) {
		$invalidpostlink = true;
	} else {
		if (!$userpermissions['canview']) {
			$invalidpostlink = true;
		}
	}

	$query = sprintf('select posts.id, posts.viewpermissions, owneraccount.username, owneraccount.fullname,'
			.' posts.accountid, posts.searchcache, %d as userid,' /* USERID */
			.' (select members.accountid from members'
			.' 	where members.postid = posts.id and members.accountid = userid limit 1) as memberid,'
			.' (select members.trusted from members'
			.' 	where members.postid = posts.id and members.accountid = userid) as trusted'
			.' from posts left join accounts as owneraccount on owneraccount.id = posts.accountid'
			.' where posts.id=%d limit 1;', $activeuserid, $postid); /* POSTID */
	$result = func_php_query($query);
	if (mysqli_num_rows($result) != 1) {
		$invalidpostlink = true;
	}

	if ($invalidpostlink) {
		if ($label) {
			return $label.'&nbsp;<i>(Missing&nbsp;or&nbsp;Invalid)</i>';
		} else {
			return '<i>(Missing&nbsp;or&nbsp;Invalid&nbsp;Post)</i>';
		}
	}

	$r = mysqli_fetch_assoc($result);
	$postid = $r['id'];
	$viewpermissions = $r['viewpermissions'];
	$accountid = $r['accountid'];
	$searchcache = $r['searchcache'];
	$memberid = $r['memberid'];
	$trusted = $r['trusted'];
	$username = $r['username'];
	$fullname = $r['fullname'];
	$popupid = '';
	$buttonhtml = '';
	$popuphtml = '';
	$is_groupmember = false;
	$searchcachesnippet = func_php_cleantext($searchcache, 80);

	if ($is_inline) {
		$buttonhtml = "<img id=\"{$elementid}_statusimg\" src=\"/img/icon-post.png\" width=\"12\" height=\"12\">";
	} else {
		$imgsrc = '';
		if ($accountid == $memberid)
			$imgsrc = '/img/status-owner12.png';
		else if ($trusted == 1)
			$imgsrc = '/img/status-member12.png';
		else if ($memberid > 0)
			$imgsrc = '/img/status-pending12.png';
		else
			$imgsrc = '/img/icon-post.png';

		$buttonhtml = "<img id=\"{$elementid}_statusimg\" src=\"$imgsrc\" width=\"12\" height=\"12\">";
		if ($memberid)
			$is_groupmember = true;
	}

	if (!$label) {
		$label = $searchcachesnippet;
	}

	$buttonhtml .= "<span>{$label}</span>";

	$popuphtml .= "<div><a href=\"/$username\"><strong>$fullname</strong></a></div>";
	$popuphtml .= "<div>$servername/$postid</div>";

	$popuphtml .= '<div class="memberlinkbuttons">';
	$popuphtml .= "<a href=\"$urlhome/{$postid}\" class=\"greenbutton\">View Page</a>";
	if ($memberid) {
		if ($activeuserid == $accountid) {
			$popuphtml .= ' <span class="greentext">Group Owner</span>';
		} else if (($trusted == 1) && ($memberid == $activeuserid)) {
			$popuphtml .= " <a class=\"silverbutton\" onclick=\"index_js_joinGroup('$phylum', $postid, $memberid, 0, 1);\">Leave Group</a>";
		} else if (($accountid != $activeuserid) && ($memberid == $activeuserid)) {
			$popuphtml .= " <a class=\"silverbutton\" onclick=\"index_js_joinGroup('$phylum', $postid, $memberid, 0, 0);\">Cancel Join Request</a>";
		} else {
			// $popuphtml .= "<div># NO PERMISSIONS #</div>";
		}
	}
	$popuphtml .= '</div>';
	$popuphtml .= $searchcachesnippet;

	$postlink = func_php_popupLinkHTML($elementid, $buttonhtml, $popuphtml,
			$is_inline, $is_groupmember, '', $postid);

	return $postlink;
}

/**
 * Generate the permissions popup and date link used in post credits.
 */
function func_php_insertPostCreditPopups($phylum, $postid, $permissions, $added, $viewmode, $moredetail)
{
	$permissionpopup = "<span id=\"{$phylum}creditdate_{$postid}\" class=\"postcreditspermissions tooltiphost\">";
	/* Add post permissions here. */
	for ($i = 0; $i <= 5; $i++) {
		$permission = $permissions[$i];
		$imgsrc = '';
		$imgalt = '';
		$imgdescription = '';

		/* Set image icon for permission. */
		switch ($permission) {
		case 5: /* Open (allow guest interactions.) */
			$imgsrc = '/img/icon-permissions-open.png';
			$imgdescription = 'Anyone';
			break;
		case 4: /* All signed-in members of the site, without being in a group. */
			$imgsrc = '/img/icon-permissions-all.png';
			$imgdescription = 'Users on this site';
			break;
		case 3: /* Members - Other, trusted and pending as well. */
			$imgsrc = '/img/icon-permissions-members.png';
			$imgdescription = 'Group members';
			break;
		case 2: /* Contacts - Other, trusted */
			$imgsrc = '/img/icon-permissions-contacts.png';
			$imgdescription = 'Starred contacts';
			break;
		case 1: /* Recipients - Other and sender/recipient. */
			$imgsrc = '/img/icon-permissions-recipient.png';
			$imgdescription = 'The sender and recipient(s)';
			break;
		default: /* Owner */
			$imgsrc = '/img/icon-permissions-owner.png';
			$imgdescription = 'Only the owner';
		}

		/* Set image icon title. */
		if ($i == 0) {
			$imgalt = 'View Permissions';
			$imgdescription .= ' can see this.';
		} else if ($i == 1) {
			$imgalt = 'Edit Permissions';
			$imgdescription .= ' can edit this.';
		} else if ($i == 2) {
			$imgalt = 'Reply Permissions';
			$imgdescription .= ' can reply.';
		} else if ($i == 3) {
			$imgalt = 'Approved Replies Permissions';
			$imgdescription .= ' have replies pre-approved.';
		} else if ($i == 4) {
			$imgalt = 'Who can delete replies and approve new members';
			$imgdescription .= ' can moderate users and replies.';
		} else if ($i == 5) {
			$imgalt = 'Tags from other users';
			$imgdescription .= ' can add tags.';
		}

		if ($i == 0) {
			$permissionpopup .= "<img alt=\"$imgalt\" src=\"$imgsrc\"><span class=\"tooltiptext tooltiptextleftedge\" style=\"line-height:22px;\">";
		}
		if ($i > 0)
			$permissionpopup .= '<br>';
		$permissionpopup .= "<img alt=\"$imgalt\" src=\"$imgsrc\" style=\"vertical-align:-2px;margin-right:2px;\">$imgdescription";
	}
	$permissionpopup .= '</span></span>';
	/* Reformat any strings. */
	$addedmoredetail = true;
	if (($viewmode == 'thumbnail') || ($viewmode == 'list'))
		$addedmoredetail = false;
	$addedtime = strtotime($added);
	$addedstring = '<span class="tooltiphost"><span class="fadetext">'.func_php_fuzzyTimeAgo($added, true, $addedmoredetail).'</span>'
			.'<span class="tooltiptext tooltiptextleft">'
			.func_php_fuzzyTimeAgo($added, false, true).'<br>'
			.date('l g:i a\<\b\r\>jS F Y', $addedtime)
			.'</span></span>';

	$postcreditspopups = "<a href=\"/$postid\">{$permissionpopup}&nbsp;{$addedstring}</a> ";

	/* Exit early if post... */
	if ($phylum == 'post') {
		return $postcreditspopups;
	}

	/* ...otherwise add message specific details. */
	$query = sprintf('select'
			.' (select count(distinct concat_ws("", accounts.username, recipients.emailusername,'
			.' 	recipients.emailserver))'
			.' 	from recipients left join accounts on accounts.id = recipients.recipientaccountid'
			.' 	where recipients.tablename = "posts" and recipients.tableid = posts.id'
			.'	and (recipients.senderaccountid != recipients.recipientaccountid'
			.' 	or recipients.recipientaccountid is NULL)) as recipientcount,'
			.' (select count(*) from members where members.postid = posts.id'
			.' 	and members.trusted = 1) as trustedmembercount,'
			.' (select count(*) from members where members.postid = posts.id'
			.' 	and members.trusted != 1) as pendingmembercount'
			.' from posts where posts.id = %d;', $postid);
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);
	$recipientcount = $r['recipientcount'];
	$trustedmembercount = $r['trustedmembercount'];
	$pendingmembercount = $r['pendingmembercount'];

	/* Only need to show if other recipients present in IM */
	if ($recipientcount > 1) {
		$query = sprintf('select distinct concat_ws("", accounts.fullname, recipients.emailusername,'
				.' 	if(recipients.emailserver is not NULL, "@", ""), recipients.emailserver) as recipient'
				.' 	from recipients left join accounts on recipients.recipientaccountid = accounts.id'
				.' 	where recipients.tablename = "posts" and recipients.tableid = %d;', $postid); /* POSTID */
		$result = func_php_query($query);
		$recipientlist = '';
		while ($r = mysqli_fetch_assoc($result)) {
			$recipientlist .= "<div>{$r['recipient']}</div>";
		}

		$recipientcountspan = ' <span class="tooltiphost postcreditsmessageinfo" style="border:solid 1px #cce; background:#edf;">'
				.'<img src="/img/icon-permissions-recipient.png" width="12" height="12">'.$recipientcount
				.'<span class="tooltiptext tooltiptextleft">';
		$recipientcountspan .= $recipientlist;
		$recipientcountspan .= '</span></span>';
		$postcreditspopups .= $recipientcountspan;
	}

	/* Display group member count for each message, if applicable */
	if ($trustedmembercount || $pendingmembercount) {
		$trustedmembercountspan = ' <span class="tooltiphost postcreditsmessageinfo" style="border:solid 1px #cec; background:#ded;">'
				.'<img src="/img/icon-permissions-members.png" width="12" height="12">'.$trustedmembercount
				.'<span class="tooltiptext tooltiptextleft">';
		if ($trustedmembercount > 1) {
			$trustedmembercountspan .= "This&nbsp;post&nbsp;has {$trustedmembercount}&nbsp;members.";
		} else if ($trustedmembercount == 1) {
			$trustedmembercountspan .= 'This&nbsp;post&nbsp;has only&nbsp;1&nbsp;member.';
		} else {
			$trustedmembercountspan .= 'This&nbsp;post&nbsp;has&nbsp;no approved&nbsp;members.';
		}
		$trustedmembercountspan .= '</span></span>';

		$postcreditspopups .= $trustedmembercountspan;

		/* Conditionally display group members waiting for approval. */
		if ($pendingmembercount) {
			$pendingmembercountspan = ' <span class="tooltiphost postcreditsmessageinfo" style="border:solid 1px #ece; background:#fdf;">'
					.'<img src="/img/status-pending12.png" width="12" height="12\>'.$pendingmembercount
					.'<span class="tooltiptext tooltiptextleft">';
			if ($pendingmembercount == 1) {
				$pendingmembercountspan .= 'This&nbsp;post&nbsp;has 1&nbsp;member waiting&nbsp;approval.';
			} else {
				$pendingmembercountspan .= "This&nbsp;post&nbsp;has {$pendingmembercount}&nbsp;members waiting&nbsp;approval.";
			}
			$pendingmembercountspan .= '</span></span> ';

			$postcreditspopups .= $pendingmembercountspan;
		}
	}

	return $postcreditspopups;
}

/**
 * Adds both timestamp and user popup. Returns as an HTML string.
 * - $phylum: string; either 'post' or 'message'.
 * - $postid: int; for non-member posts.
 * - $showparent: int for parent post id; Adds link to parent post, if available.
 * - $moredetail: boolean; Set false for compact form.
 */
function func_php_insertPostCredits($phylum, $postid, $showparent, $viewmode,
		$moredetail, $activeuserid)
{
	global $urlhome;

	$query = sprintf('/* func.php:'.__LINE__.' */ select %d as activeuserid, posts.*,' /* USERID */
			.' accounts.username as accountsusername, accounts.fullname as accountsfullname,'
			.' (select count(members.trusted) from members'
			.' 	left join membertree on members.postid = membertree.parentid'
			.' 	where (members.postid = posts.id or membertree.postid = posts.id)'
			.' 	and members.accountid = activeuserid and members.trusted != 1) as activeuserispendingmember,'
			.' ((select count(*) from members left join membertree on members.postid = membertree.parentid'
			.' 	where members.accountid = activeuserid and membertree.postid = posts.id'
			.' 	and members.trusted = 1 and members.accountid != posts.accountid)'
			.' 	+ (select count(*) from members where members.accountid = activeuserid'
			.' 	and members.postid = posts.id and members.trusted = 1 and members.accountid != posts.accountid))'
			.' 	as activeuseristrustedmember,'
			.' (select count(recipientaccountid) from recipients where recipients.tablename = "posts"'
			.' 	and recipients.tableid = posts.id'
			.' 	and recipients.recipientaccountid = activeuserid) as activeuserisrecipient,'
			.' (select count(trusted) from contacts where contacts.accountid = posts.accountid'
			.' 	and contacts.contactid = activeuserid and contacts.trusted = 1) as activeuseristrustedcontact,'
			.' concat(posts.viewpermissions, posts.editpermissions, posts.replypermissions,'
			.' 	posts.approvepermissions, posts.moderatepermissions, posts.tagpermissions) as permissions'
			.' from posts left join accounts on posts.accountid = accounts.id'
			.' where posts.id = %d limit 1;', /* POSTID */
			$activeuserid, $postid);
	$result = func_php_query($query);
	if (mysqli_num_rows($result) != 1) {
		return $query; /* ERROR FOUND! What are we meant to do here? */
	}

	$r = mysqli_fetch_assoc($result);
	$postid = $r['id'];
	$parentid = $r['parentid'];
	$accountid = $r['accountid'];
	$searchcache = $r['searchcache'];
	$authorusername = $r['accountsusername'];
	$authorfullname = $r['accountsfullname'];
	$permissions = $r['permissions'];
	$added = $r['added'];
	$domainname = $r['domainname'];

	$userpermissions = func_php_getUserPermissions($postid, $activeuserid);

	/* Remove postid reference if member of this server. */
	$memberlinkpostid = $postid;
	$emailaddress = '';
	if ($accountid) {
		/* Remove postid reference if member of this server. */
		$memberlinkpostid = 0;
	} else if ($r['emailusername'] && $r['emailserver']) {
		/* Add email display names if available */
		$emailaddress = $r['emailusername'].'@'.$r['emailserver'];
		$authorfullname = $r['emaildisplayname'];
		if (!$authorfullname) {
			$authorfullname = $emailaddress;
		}
	}

	/* Messages have a basic credits popup */
	if ($phylum === 'message') {
		$addedtime = strtotime($added);
		if ($emailaddress) {
			$authorfullname = "<span class=\"tooltiphost\">{$authorfullname}"
					."<span class=\"tooltiptext\">{$emailaddress}</span></span>";
		}
		$postcredits = "<strong>$authorfullname</strong>"
				.func_php_shortTime($added, true);
		return $postcredits; /* Messages don't need further details. */
	}

	/* Opening tag for postcredits */
	$postcredits = '';
	if (($viewmode == 'full') || ($viewmode == 'thumbnail')) {
		$postcredits = '<div>';
	}

	$postcredits .= func_php_insertPostCreditPopups($phylum, $postid,
			$permissions, $added, $viewmode, $moredetail);

	if (($viewmode == 'full') || ($viewmode == 'thumbnail')) {
		$postcredits .= ' &#183; ';
	}

	$postcredits .= func_php_insertMemberLink($authorusername, true, false,
			0, 0, $phylum, $memberlinkpostid, $activeuserid);

	if (($parentid > 0) && ($showparent)) {
		$postcredits .= func_php_insertPostLink($phylum, $parentid, True,
				"parentpost_{$parentid}", 'Parent', $activeuserid);
	}

	$domainnamelink = '';
	if ($domainname) {
		$domainnamelink = " &#183; <a href=\"http://{$domainname}\">"
				."<img src=\"/img/icon-domainname.png\" style=\"vertical-align:-2px;\">&nbsp;{$domainname}</a>"
				."<span class=\"tooltiptext tooltiptextleft\">This post has a custom domain</span>";
	}
	$postcredits .= "<span id=\"{$phylum}creditsdomainname_{$postid}\" class=\"fadetext tooltiphost\">{$domainnamelink}</span>";
	if (($viewmode == 'full') || ($viewmode == 'thumbnail')) {
		$postcredits .= '</div>'; /* Closing tag for postcredits */
	}

	return $postcredits;
}

/**
 * Generate popup edit lists for posts.
 * $phylum is either the strings 'message' or 'post'.
 * $showparent is used in editpost option to display parent link in post credits.
 * $posttype describes the position of the post on a page, including:
 *  'title', 'post', 'reply' or 'message', since features differ between types.
 * $pagetype usually passes on $index_page - the value we're looking for is
 *  either 'page' or 'profile' to show pin menu item.
 * $viewmode changes which list entries are shown. values are either 'list',
 *  'thumbnail', or 'full'. Blank or anything else defaults to 'full'.
 * $activeuserid is id of user in accounts to build menu for.
 */
function func_php_insertPostPopupList($phylum, $postid, $showparent, $posttype,
		$pagetype, $viewmode, $activeuserid)
{
	global $urlhome;

	switch ($viewmode) {
	case 'thumbnail':
	case 'list':
	case 'full':
		break;
	default:
		$viewmode = 'full';
	}

	$activeusername = '';
	$query = sprintf('select username, accounttype from accounts'
			.' where id = %d limit 1;', $activeuserid);
	$result = func_php_query($query);
	if (mysqli_num_rows($result)) {
		$r = mysqli_fetch_assoc($result);
		$activeusername = $r['username'];
		$activeuseraccounttype = $r['accounttype'];
	}

	/* Fetch post data for menu items */
	$query = sprintf('select %d as activeuserid, accounts.username as authorusername,' /* USERID */
			.' posts.parentid, posts.accountid, posts.editpermissions, posts.domainname,'
			.' parentpost.accountid as parentaccountid,'
			.' parentpost.replypermissions as parentreplypermissions,'
			.' parentpost.approvepermissions as parentapprovepermissions,'
			.' ((select count(*) from members left join membertree on members.postid = membertree.parentid'
			.' 	where members.accountid = posts.accountid and membertree.postid = posts.id'
			.' 	and members.trusted = 1 /* and posts.accountid != members.accountid */ )'
			.' 	+ (select count(*) from members where members.accountid = posts.accountid'
			.' 	and members.postid = posts.id and members.trusted = 1 /* and posts.accountid != members.accountid */ ))'
			.' 	as postuseristrustedmember,'
			.' (select count(recipientaccountid) from recipients'
			.' 	where recipients.tablename = "posts" and recipients.tableid = posts.id'
			.' 	and recipients.recipientaccountid = posts.accountid) as postuserisrecipient,'
			.' (select count(recipientaccountid) from recipients'
			.' 	where recipients.tablename = "posts" and recipients.tableid = posts.id'
			.' 	and recipients.recipientaccountid = %d) as activeuserisrecipient,' /* USERID */
			.' (select count(trusted) from contacts where contacts.accountid = posts.accountid'
			.' 	and contacts.contactid = posts.accountid and contacts.trusted = 1) as postuseristrustedcontact,'
			.' approvals.trusted as approved, approvals.hidden as hidden,'
			.' flags.flagcount, flags.flagactive, flags.updated as flagupdated, pins.pinstate'
			.' from posts left join accounts on posts.accountid = accounts.id'
			.' left join posts as parentpost on posts.parentid = parentpost.id'
			.' left join flags on flags.tablename = "posts" and flags.tableid = posts.id'
			.' left join approvals on posts.id = approvals.tableid and approvals.tablename = "posts"'
			.' left join pins on pins.postid = posts.id'
			.' where posts.id = %d limit 1;', /* POSTID */
			$activeuserid, $activeuserid, $postid);
	$result = func_php_query($query);
	if (mysqli_num_rows($result) != 1) {
		return false;
	}
	$r = mysqli_fetch_assoc($result);
	$accountid = $r['accountid'];
	$domainname = $r['domainname'];

	/* Get edit permissions for current post. */
	$permissions = func_php_getPostPermissions($postid);
	$userpermissions = func_php_getUserPermissions($postid, $activeuserid);
	$posteditcontrols = 0;
	if ($userpermissions['canedit']) {
		if ($activeusername && $r['authorusername']
				&& (strtolower($activeusername) === strtolower($r['authorusername']))) {
			$posteditcontrols = 2; /* Only the owner has full control. */
		} else {
			$posteditcontrols = 1; /* Allow others with edit permissions... */
		}
	}

	/* Get edit permissions for parent post, usually to see
	 * if we can pin posts or moderate them. */
	$parentpermissions = false;
	$parentuserpermissions = false;
	$parentid = $r['parentid'];
	if ($parentid != 0) {
		$parentpermissions = func_php_getPostPermissions($parentid);
		$parentuserpermissions = func_php_getUserPermissions($parentid, $activeuserid);
	}

	$postmoderatecontrols = false;
	if ($parentuserpermissions && ($parentuserpermissions['canmoderate'])) {
		$postmoderatecontrols = true;
	}

	if ($phylum === 'post') {
		$approveicon = '';
		$approvelinktext = false;
		$approvevalue = 0;
		$hidelinktext = false;
		$hidevalue = 0;
		if ($r['approved'] && !$parentpermissions['preapproved']
				&& ($parentpermissions['accountid'] != $permissions['accountid'])) {
			$approveicon = '/img/icon-post-approved.png';
			if ($postmoderatecontrols) {
				$approvelinktext = 'Undo Approve';
				$approvevalue = 0;
			}
		} else if ($permissions && !$permissions['preapproved']
				&& ($parentpermissions['accountid'] != $permissions['accountid'])) {
			$approveicon = '/img/icon-post-pending.png';
			if ($parentuserpermissions['canmoderate']) {
				$approvelinktext = 'Approve Post';
				$approvevalue = 1;
			}
		}

		/* Hide link is always visible to parent post owner/moderator. */
		if ($r['hidden'] != 1) {
			$hidelinktext = 'Hide Post';
			$hidevalue = -1;
		} else {
			$hidelinktext = 'Undo Hide';
			$hidevalue = 0;
			$approveicon = '/img/icon-post-blocked.png';
		}

		$pinstateicon = '';
		if ($r['pinstate'] == 1) {
			$pinstateicon = '/img/icon-post-pinned.png';
		}

		/* Flag request is hide by default, withdraw if hidden and viewed by owner. */
		$flagrequest = 1;
		$flagtext = 'Flag Post';
		if (($r['flagcount'] > 0) && ($r['flagactive'])) {
			$approveicon = '/img/icon-post-flagged.png';
			if ((strtolower($activeusername) == strtolower($r['authorusername']))
					&& ($activeuserid != 0)) {
				$flagrequest = 0;
				$flagtext = 'Remove Flag';
			}
		}

		$setgrouptext = '';
		$setgroup = 0;
		$confirmtype = 0;
		if (($activeuserid > 0) && ($activeuserid == $accountid)) {
			/* Set up / delete group link. */
			$setgroup = 1;
			$setgrouptext = 'Set As Group';
			$query = sprintf('select %d as pid, (select count(*) from members where postid = pid) as membercount,' /* POSTID */
					.'(select count(*) from members where accountid = %d and postid = pid limit 1) as ownercount;', /* ACCTID */
					$postid, $activeuserid);
			$result = func_php_query($query);
			$r = mysqli_fetch_assoc($result);
			if ($r['ownercount'] == 1) {
				$setgroup = 0;
				$setgrouptext = 'Close Group';

				if ($r['membercount'] > 1) {
					$confirmtype = 2; /* Owner must confirm close when members present. */
				}
			}
		}
	} /* End if ($phylum === 'post') */

	if ($posteditcontrols > 0) {
		$menuicon = "/img/postmenu-edit.png";
	} else {
		$menuicon = "/img/postmenu.png";
	}

	$popuplist = '';

	if (($viewmode !== 'list') && ($phylum !== 'message')) {
		$url = rawurlencode("{$urlhome}/{$postid}");
		$sharemenudivid = "{$phylum}sharemenu_{$postid}";
		$sharemenubuttonid = "{$phylum}sharebutton_{$postid}";
		$sharemenulistid = "{$phylum}sharelist_{$postid}";
		$popuplist .= <<<POSTSHARELIST
<div id="$sharemenudivid" class="popuplist posteditlist">
<button id="$sharemenubuttonid" class="popuplistbutton"
	onclick="index_js_popupDiv('$sharemenulistid');index_js_movePopupMenu('$sharemenubuttonid', '$sharemenulistid');"><img src="/img/postshare.png"> Share...</button>
<div id="$sharemenulistid">
<ul>
    <li><a onclick="index_js_shareByEmail($postid);">Send by Email</a></li>
    <li><a href="https://facebook.com/sharer/sharer.php?u=$url" target="_blank">Facebook</a></li>
    <li><a href="https://twitter.com/intent/tweet?url=$url" target="_blank">Twitter</a></li>
    <li><a href="https://pinterest.com/pin/create/link/?url=$url" target="_blank">Pinterest</a></li>
    <li><a href="https://reddit.com/submit?url=$url" target="_blank">Reddit</a></li>
    <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=$url" target="_blank">LinkedIn</a></li>
</ul></div>
</div>
POSTSHARELIST;
	}


	$popuplistclass = 'popuplist posteditlist';
	$popupmenubuttonid = "{$phylum}popupmenubutton_{$postid}";
	$popuplistdivid = "{$phylum}popuplist_{$postid}";
	$popupiconimgid = "{$phylum}popupicon_{$postid}";
	$popupmenudivid = "{$phylum}popupmenu_{$postid}";
	$popuplist .= <<<POSTPOPUPLISTSTART01
<!-- Start of Post Popup List -->
<div id="$popuplistdivid" class="$popuplistclass">
<button id="$popupmenubuttonid" class="popuplistbutton"
		onclick="index_js_popupDiv('$popupmenudivid');index_js_movePopupMenu('$popupmenubuttonid', '$popupmenudivid');">
POSTPOPUPLISTSTART01;

	if ($pinstateicon ?? false) {
		$popuplist .= "<img id=\"postpinnedicon_$postid\" src=\"$pinstateicon\">";
	}
	if ($approveicon ?? false) {
		$popuplist .= "<img id=\"postapproveicon_$postid\" src=\"$approveicon\">";
	}

	$popuplist .= <<<POSTPOPUPLISTSTART02
<img id="$popupiconimgid" src="$menuicon">
</button>
<div id="$popupmenudivid">
<ul class="accountlist">
POSTPOPUPLISTSTART02;

	if ($phylum === 'post') {
		// TODO $popuplist .= "<li><a onclick=\"javascript:alert('Age rating? rename table column also');\">"
		// TODO		."Age (rating?)</a></li>";
		$popuplist .= "<li><a id=\"{$phylum}flag_{$postid}\" onclick=\"index_js_flagPost('$phylum', $postid, $flagrequest);\">$flagtext</a></li>";
		// TODO $popuplist .= "<li><a onclick=\"index_js_subscribeByEmail($postid);\">Subscribe or Unsubscribe</a></li>";

		if ($postmoderatecontrols) {
			if ($approvelinktext) {
				$popuplist .= "<li><a id=\"{$phylum}approve_{$postid}\" onclick=\"index_js_approvePost('$phylum', $postid, $approvevalue);\">$approvelinktext</a></li>";
			}
			if ($hidelinktext) {
				$popuplist .= "<li><a id=\"{$phylum}hide_".$postid."\" onclick=\"index_js_approvePost('$phylum', $postid, $hidevalue);\">$hidelinktext</a></li>";
			}
		}

		/* Only posts can be pinned to the start of post stream. */
		if (($phylum == 'post') && (($posttype == 'post') || ($posttype == 'title'))
				&& ((($pagetype == 'profile') && ($activeuserid == $accountid))
				|| (($pagetype == 'page') &&
				(($parentuserpermissions['canedit'] ?? false)
				|| $postmoderatecontrols)))) {
			if (!$pinstateicon) {
				$popuplist .= "<li><a id=\"postpinpost_$postid\""
						." onclick=\"index_js_pinPost($postid, 1, '$posttype', '$pagetype');\">Pin to Top</a></li>";
			} else {
				$popuplist .= "<li><a id=\"postpinpost_$postid\""
						." onclick=\"index_js_pinPost($postid, 0, '$posttype', '$pagetype');\">Remove Pin</a></li>";
			}
		}

		if ($activeuserid) {
			if ($setgrouptext) {
				$popuplist .= "<li><a id=\"{$phylum}joingrouplink_$postid\""
						." onclick=\"index_js_joinGroup('$phylum', $postid, $activeuserid, $setgroup, $confirmtype);\">$setgrouptext</a></li>";
			}
			// TODO $popuplist .= "<li><a onclick=\"javascript:alert('TODO: Copy!');\">Copy</a></li>";
		}
	} else { /* if ($phylum === 'message') { */
		$popuplist .= "<li><a href=\"/{$postid}\">View Full</a>";
	} /* End if ($phylum === 'post') */

	if ($posteditcontrols > 0) {
		if ($showparent) {
			$showparent = 'true';
		} else {
			$showparent = 'false';
		}

		if ($phylum !== 'message') {
			// TODO $popuplist .= "<li><a onclick=\"javascript:alert('TODO: Posts can only be moved to another location the editor can edit!');\">Move</a></li>";
		}

		/* Should only edit when full view. Things get squashed otherwise. */
		if ($viewmode === 'full') {
			$popuplist .= "<li><a onclick=\"index_js_editPost('$phylum', $postid, '$showparent', '$posttype', '$viewmode');\">Edit</a></li>";
		} else {
			$popuplist .= '<li><a class="tooltiphost" onclick="alert(\'Can not edit posts displayed as thumbnails or list items.\nChange [View As] to [Full Post] to edit these posts.\');">'
					.'Edit&nbsp;<span class="emoji">⚠️</span><span class="tooltiptext">Can not edit as thumbnail or list item.</span></a></li>';
		}

		$popuplist .= "<li><a onclick=\"index_js_deletePost('$phylum', $postid);\">Delete</a></li>";
		if (($posteditcontrols == 2) && ($phylum !== 'message')) {
			// TODO $popuplist .= "<li><a onclick=\"javascript:alert('TODO: Change owner');\">ChOwn</a></li>";
		}
	}

	if ($phylum === 'post') {
		/* Only show analytics link for owner, editors, or moderators */
		if ($posteditcontrols || $postmoderatecontrols) {
			$popuplist .= '<li><a href="/'.$postid.'/analytics/">View Post Stats...</a></li>';
		}

		/* Show domain name controls */
		if (($activeuserid === $accountid) && ($activeuseraccounttype !== NULL)) {
			$popuplist .= "<li><a id=\"{$phylum}setdomainnamemenuitem_{$postid}\" onclick=\"index_js_setPostDomainName('{$phylum}', {$postid}, '{$domainname}');\">"
					."Set domain name...</a></li>";
		}
	}

	$popuplist .= <<<POSTPOPUPLISTEND
</ul>
</div>
</div>
POSTPOPUPLISTEND;

	return $popuplist;
}

/**
 * Generate HTML to be shown in a popup messager.
 */
function func_php_postToMessageHTML($id, $userid)
{
	$query = sprintf('select posts.accountid, posts.description,'
			.' concat(posts.viewpermissions, posts.editpermissions, posts.replypermissions, posts.approvepermissions,'
			.' posts.moderatepermissions, posts.tagpermissions) as permissions, posts.agerating,'
			.' (select group_concat(distinct concat_ws("", accounts.username, recipients.emailusername,'
			.'		if (recipients.emailserver is not NULL, "@", ""),'
			.' 		recipients.emailserver) separator " ")'
			.' 	from recipients left join accounts on accounts.id = recipients.recipientaccountid'
			.' 	where recipients.tablename = "posts"'
			.'	and recipients.senderaccountid != recipients.recipientaccountid'
			.' 	and recipients.tableid = posts.id) as recipients'
			.' from posts'
			.' where posts.id = %d limit 1', $id);
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);

	$html = '';

	$popuplist = func_php_insertPostPopupList('message',
			$id, false, 'message', false, 'full', $userid);
	$postcredits = func_php_insertPostCredits('message',
			$id, false, 'full', false, $userid);
	$description = func_php_markdowntohtml($r['description']);
	$permissions = $r['permissions'];
	$recipients = $r['recipients'];

	$messageentryclasses = 'messageentry';
	if ($r['accountid'] === $userid) {
		$messageentryclasses .= ' messageentrysent';
	}

	$html = <<<HTML
<div id="message_$id" class="$messageentryclasses" postpermissions="$permissions" postrecipients="$recipients">
	<div class="messagecredits">$postcredits
		<div id="messagemenudiv_$id">
			$popuplist
		</div>
	</div>
	<div id="messagedescription_$id" class="messagedescription">$description</div>
HTML;

	/* Return attachments */
	if ($filesarray = func_php_fileAttachmentArray('message', $id)) {
		foreach ($filesarray as $attachment) {
			$html .= "\n<div id=\"{$attachment['id']}\" class=\"messageattachment\">{$attachment['innerHTML']}</div>";
		}
	}

	$html .= '</div>';

	return $html;
}

/**
 * Returns a base64 encoded string of data for a specific file,
 * or boolean false if no file exists.
 */
function func_php_base64file($fileid, $filename)
{
	$query = sprintf('/* '.__LINE__.' */ select * from files'
			.' where id=%d and filename="%s" limit 1;', /* FILEID, FILENAME */
			$fileid, func_php_escape($filename));
	$result = func_php_query($query);

	if (!mysqli_num_rows($result)) {
		return false; /* Invalid file ID or file not exists */
	}

	if (mysqli_num_rows($result) == 1) {
		$r = mysqli_fetch_assoc($result);
		$filemime = $r['filemime'];
		$filename = $r['filename'];
		$fileindex = $r['fileindex'];
		$s3profile = $r['s3profile'];
		$s3endpoint = $r['s3endpoint'];
		$s3region = $r['s3region'];
		$s3bucket = $r['s3bucket'];

		/* Check if using an S3 object source for file data */
		if ($s3profile && $s3endpoint && $s3region && $s3bucket) {
			require 'aws/aws-autoloader.php';

			$sdk = new Aws\Sdk([
				'profile' => $r['s3profile'],
				'endpoint' => $r['s3endpoint'],
				'region' => $r['s3region'],
				'version' => 'latest',
				'use_path_style_endpoint' => true,
			]);
			$s3 = $sdk->createS3();
			$s3->registerStreamWrapper();

			$s3uri = 's3://'.$r['s3bucket'].'/'.$fileindex;

			$stream = fopen($s3uri, 'r');
		} else {
			$stream = fopen("/files/{$fileindex}");
		}
	}

	if (!$stream)
		return false; /* Could not open file or stream */

	$base64data = '';
	while (!feof($stream)) {
		/* 8151 is divisble by 3 to byte align base64 encoding as well as
		 * 57 for base-64 encoded data to split lines at 76 length. */
		$data = fread($stream, 8151);
		$base64data .= chunk_split(base64_encode($data), 76, "\r\n");
	}
	fclose($stream);

	return $base64data;
}

/**
 * The following functions work together to send emails through an SMTP server:
 * - func_php_getSocketResponse($socket)
 * - func_php_sendSocketCommand($socket, $command)
 * - func_php_mailAddressHeaderArray($addresses)
 * - func_php_sendmail($message, $headers)
 */
function func_php_getSocketResponse($socket)
{
	$response = '';
	stream_set_timeout($socket, 30);
	while (($line = fgets($socket, 1024)) !== false) {
		$response .= trim($line)."\n";
		if (substr($line, 3, 1) == ' ')
			break;
	}

	return $response;
}

function func_php_sendSocketCommand($socket, $command)
{
	fputs($socket, trim($command)."\r\n");

	return func_php_getSocketResponse($socket);
}


/**
 * Process header entries with email addresses, then return an array of recipients.
 */
function func_php_mailAddressHeaderArray($addresses)
{
	$addrlist = explode(',', $addresses);
	if (!$addrlist || !count($addrlist)) {
		return 0;
	}

	$addrarray = [];
	$i = 0;
	foreach ($addrlist as $addr) {
		$addr = func_php_cleantext($addr, 1024);
		if (preg_match('/^"?([^<"]*)"?\s*?<([^@]*)@([^>]*)>$/', $addr, $matches)) {
			/* Handle emails WITH display name. */
			$displayname = $matches[1];
			$username = strtolower($matches[2]);
			$server = strtolower($matches[3]);
			$addrarray[] = array("{$username}@{$server}", $displayname);
		} else if (preg_match('/^([^<@"]*)@([^>]*)$/', $addr, $matches)) {
			/* Handle emails without display names. */
			$username = strtolower($matches[1]);
			$server = strtolower($matches[2]);
			$addrarray[] = array("{$username}@{$server}");
		} else {
			continue;
		}

		$i++;
	}

	return $addrarray;
}

function func_php_sendmail($message, $headers)
{
	global $mailerhostname, $mailerport;
	global $mailerusername, $mailerpassword;

	$responseTimeout = 10;
	$connectionTimeout = 32;

	$fromarray = [];
	$toarray = [];
	$ccarray = [];
	$bccarray = [];
	$dbg = [];

	/* Header processing loop. During this time certain headers will be removed,
	 * currently the "Bcc" header but still retaining the recipients. */
	$lines = explode("\n", $headers);
	$linecount = count($lines);
	$currentline = 0;
	$headers = '';
	$subject = false;
	for ($currentline = 0; $currentline < $linecount; $currentline++) {
		$line = func_php_cleantext($lines[$currentline], 1024);
		$headerline = $line."\r\n";
		if (preg_match('/;$/', $line)) {
			/* Keep appending header lines ending with a semicolon (;)
			 * These are ususally meant to be processed together */
			while (preg_match('/;$/', $line)) {
				$currentline++;
				$nextline = func_php_cleantext($lines[$currentline], 1024);
				$line .= ' '.$nextline;
				$headerline .= "\t{$nextline}\r\n";
			}
		}

		/* Look for any headers, before reading data */
		if (preg_match('/^(.+):\s*(.+)$/', $line, $matches)) {
			$key = mb_strtolower($matches[1]);
			$value = $matches[2];
			switch ($key) {
			case 'from':
				$fromarray = func_php_mailAddressHeaderArray($value);
				break;
			case 'to':
				$toarray = func_php_mailAddressHeaderArray($value);
				break;
			case 'cc':
				$ccarray = func_php_mailAddressHeaderArray($value);
				break;
			case 'bcc':
				$bccarray = func_php_mailAddressHeaderArray($value);
				$headerline = false; /* Don't preserve 'Bcc:' header */
				break;
			case 'subject':
				$subject = $value;
				break;
			}
		}

		/* Re-attach header line if it doesn't need to be removed */
		if ($headerline) {
			$headers .= $headerline;
		}
	}

	if ((count($fromarray) == 0) || (count($toarray) == 0)) {
		return 0;
	}

	/* Now, send the actual message */
	$server = "tls://{$mailerhostname}";
	$socket = fsockopen($server, $mailerport, $errorCode, $errorMessage, $connectionTimeout);

	if (!$socket)
		return 0;

	$dbg[] = __LINE__.'::fsockopen::'.func_php_getSocketResponse($socket);
	$dbg[] = func_php_sendSocketCommand($socket, "EHLO {$mailerhostname}");

	/* Commented block. Is this even needed anymore? */
	//if ($tls) {
	//	$dbg[] = __LINE__.'::startTLS::'.func_php_sendSocketCommand($socket, 'STARTTLS');
	//	stream_socket_enable_crypto($socket, true, STREAM_CRYPTO_METHOD_TLS_CLIENT);
	//	$dbg[] = __LINE__.'::helloTLS::'.func_php_sendSocketCommand($socket, "EHLO {$mailerhostname}");
	//}

	$dbg[] = __LINE__.'::auth::'.func_php_sendSocketCommand($socket, 'AUTH LOGIN');
	$dbg[] = __LINE__.'::username::'.func_php_sendSocketCommand($socket, base64_encode($mailerusername));
	$dbg[] = __LINE__.'::password::'.func_php_sendSocketCommand($socket, base64_encode($mailerpassword));
	$dbg[] = __LINE__.'::mailfrom::'.func_php_sendSocketCommand($socket, "MAIL FROM: <{$fromarray[0][0]}>");

	$recipients = array_merge($toarray, $ccarray, $bccarray);
	foreach ($recipients as $recipient) {
		$dbg[] = __LINE__.'::mailto::'.func_php_sendSocketCommand($socket, "RCPT TO: <{$recipient[0]}>");
	}

	$data = "{$headers}\r\n{$message}\r\n."; /* Emails must end with a dot on its own line */
	$dbg[] = __LINE__.'::emaildata::'.$data;
	$dbg[] = __LINE__.'::data1::'.func_php_sendSocketCommand($socket, 'DATA');
	$dbg[] = __LINE__.'::data2::'.func_php_sendSocketCommand($socket, $data);
	$dbg[] = __LINE__.'::quit::'.func_php_sendSocketCommand($socket, 'QUIT');

	return $dbg;
}
?>
