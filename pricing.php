<div class="debugpostclass debugpostborderclass" style="margin-left:8px;">
<div class="postdescription" style="padding:8px;">
<h1>Plans and Pricing</h1>
<br>

<style>
.pricingtogglecontainer {
	text-align:center;
	font-size:24px; /* Also toggle height */
	height:100%;
	width:100%;
	text-align: center;
	vertical-align: middle;
	box-sizing:border-box;
	white-space:nowrap;
	margin-top:10px;
}

.pricingtogglelabel {
	text-align: left;
	width: 2em;
	height: 1em;
	border-radius: 0.8em;
	background-color:#eee;
	display: inline-block;
	position: relative;
	cursor: pointer;
	vertical-align: -0.133em;
	box-shadow:inset 0 0 3px rgba(0, 0, 0, 0.25);
}

.pricingToggleCheckbox {
	opacity: 0;
	position: absolute;
}

.pricingtogglespan {
	display: block;
	width: 100%;
	height: 100%;
	border-radius: 0.8em;
}

.pricingtogglespan:after {
	content: "";
	cursor: pointer;
	position: absolute;
	top: 50%;
	left: 0;
	z-index: 3;
	transition: transform 0.25s cubic-bezier(0.5, 0.5, 0.0, 1.5);
	width: 0.8em;
	height: 0.8em;
	transform: translate3d(0, -50%, 0);
	background-color: #fff;
	border-radius: 100%;
	border:solid 3px #9c5;
}

.pricingToggleCheckbox:active ~ .pricingtogglespan:after,
.pricingToggleCheckbox:focus ~ .pricingtogglespan:after {
	background-color:#dfd;
}

.pricingToggleCheckbox:checked ~ .pricingtogglespan:after {
	transform: translate3d(1em, -50%, 0);
}

.pricingswitchlabel {
	cursor:default;
	cursor:pointer;
	transition:0.5s;
	color:#ccc;
	margin:0.1em;
}

.pricingswitchlabel-active {
	cursor:default;
	color:#090;
}

.pricingbuttondiv {
	padding-top:8px;
	padding-bottom:8px;
}

.pricingbuttondiv big {
	font-size:20px;
	font-weight:bold;
}

.pricingbuttonwhite,
.pricingbutton {
	display:block;
	width:10em;
	padding:4px 12px;
	margin-left:auto;
	margin-right:auto;
}

.pricingbuttonwhite {
	background:none;
	text-shadow:none;
	color:#090
}

.pricingplanprice {
	text-align:center;
	margin:4px;
	transition:0.5s;
}
.pricingplanprice sup {
	font-size:16px;
	font-weight:bold;
	position:relative;
	top:-4px;
}
.pricingplanprice big {
	font-weight:normal;
	font-size:32px;
}
.pricingplanprice sub {
	font-size:16px;
	font-weight:normal;
	position:relative;
	bottom:3px;
}

/* Plan feature table */
.planfeaturetable {
	margin:0 auto;
	border-spacing:0;
}

.planfeatureheading {
	border-bottom:solid 1px #ddd;
	padding-top:1em;
	padding-left:0.3em;
	font-size:1.5em;
	text-align:left;
	font-weight:normal;
	vertical-align:bottom;
}

.planfeatureheader {
	border-bottom:solid 1px #ddd;
	text-align:center;
	width:12em;
	vertical-align:bottom;
}

.planfeatureheaderstart {
	border-bottom:solid 1px #ddd;
}

.planfeatureitem {
	border-bottom:solid 1px #ddd;
}

.planfeatureitemupcoming {
	border-bottom:solid 1px #ddd;
	text-decoration:line-through;
	color:#aaa;
}

.planfeatureitemupcoming ~ .planfeaturevalue {
	color:#aaa;
}

.planfeaturevalue {
	border-bottom:solid 1px #ddd;
	text-align:center;
	vertical-align:middle;
}

/* Generic override styles */
.centered {
	text-align:center;
}

.smalllink {
	text-decoration:none;
	font-size:14px;
	font-weight:normal;
	color:#57f;
}

.signinorupdiv {
	background:#eee;
	padding:1em;
	margin:1em auto;
	max-width:30em;
	border-radius:4px;
}

.signinoruplabel {
	display:block;
	font-weight:bold;
	font-size:1em;
	color:#999;
	cursor:default; /* Force cross-compatibility */
	cursor:pointer;
}

.signinoruplabel:before {
	content:'';
	border:solid 2px #3a6;
	display:inline-block;
	width:1em;
	height:1em;
	border-radius:100%;
	margin-right:0.5em;
	vertical-align:text-top;
	background:#fff;
}

.signinorupform {
	max-height:0;
	transition:all 0.5s;
	overflow:hidden;
	padding:0 2.0em;
}

.signinorupformdiv {
	margin-top:1em;
}

.signinorupradio {
	display:none;
}

.signinorupradio:checked ~ .signinorupform {
	max-height:30em;
	overflow:unset;
}

.signinorupradio ~ .signinoruplabel:hover:before,
.signinorupradio:checked ~ .signinoruplabel:before {
	background:#cec;
}

.signinorupradio:checked ~ .signinoruplabel,
.signinorupradio:checked ~ .signinoruplabel:hover {
	color:#090;
}

.signinorupradio:checked ~ .signinoruplabel:before,
.signinorupradio:checked ~ .signinoruplabel:hover:before {
	background:#3a6 url('/radiocheck.png') 50% 50% no-repeat;
}

.signinorupinputdiv {
	position:relative;
	border:solid 1px #abc;
	border-radius:4px;
	padding:3px;
	background-color:#fff;
	box-shadow:inset 3px 3px 9px #ccc;
}

.signinorupinputdiv input[type=text],
.signinorupinputdiv input[type=password] {
	border:none;
	box-sizing:border-box;
	width:100%;
	background:none;
	padding:4px;
}

/* Copy of signuppopup */
.signinoruppopup {
	display:none;
	position:absolute;
	left:20%;
	right:0;
	top:100%;
	background:#ff9 -moz-linear-gradient(bottom, rgba(255,255,255,0.8), rgba(255,255,255,0.0)); /* Firefox 3.6 to 15 */
	background:#ff9 -o-linear-gradient(bottom, rgba(255,255,255,0.8), rgba(255,255,255,0.0)); /* Opera 11.1 to 12.0 */
	background:#ff9 -webkit-linear-gradient(bottom, rgba(255,255,255,0.8), rgba(255,255,255,0.0)); /* Safari */
	background:#ff9 linear-gradient(to bottom, rgba(255,255,255,0.8), rgba(255,255,255,0.0)); /* Standard syntax (must be last) */
	color:inherit;
	box-shadow:4px 4px 30px 8px rgba(0, 0, 0, 0.5);
	border:solid 1px #abc;
	border-radius:4px;
	padding:0.1em 0.2em;
	z-index:8999 !important;
	box-sizing:border-box;
}

.signinorupinput:focus ~ .signinoruppopup {
	display:block;
}

.silvertext {
	color:#ccc;
}

.purchasedetails {
	width:100%;
}

caption, th, td {
	vertical-align:top;
	padding:0.5em;
}

th, td {
	text-align:left;
}

th {
	font-weight:bold;
}

caption {
	font-size:22px;
	color:#6a9;
	text-align:center;
	/* border-bottom:solid 1px #6a9; */
}

</style>

<?php
	if ($index_activeuserid && isset($index_activeuser['accounttype'])) {
?>
<p class="information" style="max-width:46em;margin:0 auto;padding:3pt 1em;">You are currently subscribed to <strong><?=$index_activeuser['accounttype']?></strong><br>
	Choosing another plan will replace it.
	<br><a class="boldlink" href="<?=$urlhome?>/billing">Manage</a></p>
<?php
	}
?>

<div style="font-size:24px;text-align:center;line-height:48px;">
	Simple Monthly Pricing.<br>
	Get all features, only pay for storage.<br>
	AU$1 per 10GB. Pay yearly and save 25%.
</div>

<form action="/billing" method="get" style="margin:0 auto 1em auto;">

	<table class="planfeaturetable">
	<tr>
	<td>
		<div class="pricingtogglecontainer">
			<label id="leftLabelTop" class="pricingswitchlabel leftLabel pricingswitchlabel-active">Monthly</label>
			<label class="pricingtogglelabel">
			<input id="pricingToggleCheckboxTop" name="yearly" type="checkbox" class="pricingToggleCheckbox">
			<span class="pricingtogglespan"></span>
			</label>
			<label id="rightLabelTop" class="pricingswitchlabel rightLabel">Yearly</label>
		</div>
	</td>
	<td class="centered">
		<div class="pricingbuttondiv">
			<a class="greenbutton pricingbuttonwhite" href="/#rightpanel">Sign Up for Free</a>
			<big>100 MB</big>
		</div>
		<div>No credit card required</div>
	</td>
	<td class="centered">
		<div class="pricingbuttondiv">
			<button class="greenbutton pricingbutton" name="qty" value="10">Buy Personal</button>
			<big>100 GB</big>
		</div>
		<div id="pricePersonalMonthTop" class="pricingplanprice">
			<sup>$</sup><big>10</big><sub>/month</sub>
		</div>
		<div id="pricePersonalYearTop" class="pricingplanprice">
			<sup>$</sup><big>90</big><sub>/year</sub>
		</div>
	</td>
	<td class="centered">
		<div class="pricingbuttondiv">
			<button class="greenbutton pricingbutton" name="qty" value="50">Buy Advanced</button>
			<big>500 GB</big>
		</div>
		<div id="priceAdvancedMonthTop" class="pricingplanprice">
			<sup>$</sup><big>50</big><sub>/month</sub>
		</div>
		<div id="priceAdvancedYearTop" class="pricingplanprice">
			<sup>$</sup><big>450</big><sub>/year</sub>
		</div>
	</td>
	<td class="centered">
		<div class="pricingbuttondiv">
			<button class="greenbutton pricingbutton" name="qty" value="200">Buy Professional</button>
			<big>2,000 GB</big>
		</div>
		<div id="priceProfessionalMonthTop" class="pricingplanprice">
			<sup>$</sup><big>200</big><sub>/month</sub>
		</div>
		<div id="priceProfessionalYearTop" class="pricingplanprice">
			<sup>$</sup><big>1,800</big><sub>/year</sub>
		</div>
	</td>
	</tr>
	<tr>
	<th class="planfeatureheading">Features</th>
	<th class="planfeatureheader">Free</th>
	<th class="planfeatureheader">Personal</th>
	<th class="planfeatureheader">Advanced</th>
	<th class="planfeatureheader">Profesional</th>
	</tr>
	<tr>
	<td class="planfeatureitem">Storage space</td>
	<td class="planfeaturevalue">100 MB</td>
	<td class="planfeaturevalue">100 GB</td>
	<td class="planfeaturevalue">500 GB</td>
	<td class="planfeaturevalue">2,000 GB</td>
	</tr>
	<tr>
	<td class="planfeatureitem">Transfers</td>
	<td class="planfeaturevalue">250 MB /month</td>
	<td class="planfeaturevalue">100 GB /month</td>
	<td class="planfeaturevalue">500 GB /month</td>
	<td class="planfeaturevalue">2,000 GB /month</td>
	</tr>
	<tr>
	<td class="planfeatureitem">Maximum upload size</td>
	<td class="planfeaturevalue">4 MB</td>
	<td class="planfeaturevalue">2 GB</td>
	<td class="planfeaturevalue">2 GB</td>
	<td class="planfeaturevalue">2 GB</td>
	</tr>
	<tr>
	<td class="planfeatureitem">Send and receive emails</td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	</tr>
	<tr>
	<td class="planfeatureitem">Upload images, videos and attachments</td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	</tr>
	<tr>
	<td class="planfeatureitem">Embed Videos</td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	</tr>
	<tr>
	<td class="planfeatureitem">Edit HTML</td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	</tr>
	<tr>
	<td class="planfeatureitem">Analytics</td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	</tr>
	<tr>
	<td class="planfeatureitem">Hotlinking</td>
	<td class="planfeaturevalue"><img src="/img/greycrossout.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	</tr>
	<tr>
	<th class="planfeatureheading">Support</th>
	<th class="planfeatureheader">Free</th>
	<th class="planfeatureheader">Personal</th>
	<th class="planfeatureheader">Advanced</th>
	<th class="planfeatureheader">Profesional</th>
	</tr>
	<tr>
	<td class="planfeatureitem">Community forums</td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	</tr>
	<tr>
	<td class="planfeatureitem">Priority email support</td>
	<td class="planfeaturevalue"><img src="/img/greycrossout.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	<td class="planfeaturevalue"><img src="/img/greencheck.png"></td>
	</tr>
	<tr>
	<th class="planfeatureheading">Upcoming features</th>
	<th class="planfeatureheader">Free</th>
	<th class="planfeatureheader">Personal</th>
	<th class="planfeatureheader">Advanced</th>
	<th class="planfeatureheader">Profesional</th>
	</tr>
	<tr>
	<td class="planfeatureitemupcoming">Connect a custom domain</td>
	<td class="planfeaturevalue">&mdash;</td>
	<td class="planfeaturevalue">&mdash;</td>
	<td class="planfeaturevalue">&mdash;</td>
	<td class="planfeaturevalue">&mdash;</td>
	</tr>
	<tr>
	<td class="planfeatureitemupcoming">Own branding</td>
	<td class="planfeaturevalue">&mdash;</td>
	<td class="planfeaturevalue">&mdash;</td>
	<td class="planfeaturevalue">&mdash;</td>
	<td class="planfeaturevalue">&mdash;</td>
	</tr>
	<tr>
	<td class="planfeatureitemupcoming">Manage email lists</td>
	<td class="planfeaturevalue">&mdash;</td>
	<td class="planfeaturevalue">&mdash;</td>
	<td class="planfeaturevalue">&mdash;</td>
	<td class="planfeaturevalue">&mdash;</td>
	</tr>
	<tr>
	<td class="planfeatureitemupcoming">Scheduled posts</td>
	<td class="planfeaturevalue">&mdash;</td>
	<td class="planfeaturevalue">&mdash;</td>
	<td class="planfeaturevalue">&mdash;</td>
	<td class="planfeaturevalue">&mdash;</td>
	</tr>
	<tr>
	<td class="planfeatureitemupcoming">Custom forms</td>
	<td class="planfeaturevalue">&mdash;</td>
	<td class="planfeaturevalue">&mdash;</td>
	<td class="planfeaturevalue">&mdash;</td>
	<td class="planfeaturevalue">&mdash;</td>
	</tr>
	<tr>
	<td colspan="5">
	</td>
	</tr>
	<tr>
	<td>
		<div class="pricingtogglecontainer">
			<label id="leftLabelBottom" class="pricingswitchlabel leftLabel pricingswitchlabel-active">Monthly</label>
			<label class="pricingtogglelabel">
			<input id="pricingToggleCheckboxBottom" type="checkbox" class="pricingToggleCheckbox">
			<span class="pricingtogglespan"></span>
			</label>
			<label id="rightLabelBottom" class="pricingswitchlabel rightLabel">Yearly</label>
		</div>
	</td>
	<td class="centered">
		<div class="pricingbuttondiv">
			<a class="greenbutton pricingbuttonwhite" href="/#rightpanel">Sign Up for Free</a>
			<big>100 MB</big>
		</div>
		<br>
		<div>No credit card required</div>
	</td>
	<td class="centered">
		<div class="pricingbuttondiv">
			<button class="greenbutton pricingbutton" name="qty" value="10">Buy Personal</button>
			<big>100 GB</big>
		</div>
		<div id="pricePersonalMonthBottom" class="pricingplanprice">
			<sup>$</sup><big>10</big><sub>/month</sub>
		</div>
		<div id="pricePersonalYearBottom" class="pricingplanprice">
			<sup>$</sup><big>90</big><sub>/year</sub>
		</div>
	</td>
	<td class="centered">
		<div class="pricingbuttondiv">
			<button class="greenbutton pricingbutton" name="qty" value="50">Buy Advanced</button>
			<big>500 GB</big>
		</div>
		<div id="priceAdvancedMonthBottom" class="pricingplanprice">
			<sup>$</sup><big>50</big><sub>/month</sub>
		</div>
		<div id="priceAdvancedYearBottom" class="pricingplanprice">
			<sup>$</sup><big>450</big><sub>/year</sub>
		</div>
	</td>
	<td class="centered">
		<div class="pricingbuttondiv">
			<button class="greenbutton pricingbutton" name="qty" value="200">Buy Professional</button>
			<big>2,000 GB</big>
		</div>
		<div id="priceProfessionalMonthBottom" class="pricingplanprice">
			<sup>$</sup><big>200</big><sub>/month</sub>
		</div>
		<div id="priceProfessionalYearBottom" class="pricingplanprice">
			<sup>$</sup><big>1,800</big><sub>/year</sub>
		</div>
	</td>
	</tr>
	</table>
</form>

<div style="text-align:center;">
	<i>If changing your plan, you will be able to see cost differences before being refunded or charged extra.</i>
	</div>

	<div class="postdescription maxwidth40em">
	<div class="centered">
	<h2>FAQs</h2>
	<p class="greytext">Frequently Asked Questions</p>
	</div>

	<br>

	<h3>Do notes, posts and emails use storage space?</h3>
	<p>Yes. However, emails you send and receive, and notes and posts you edit on here don't use up much storage space. Posts, notes, and emails would have to be in the <em>thousands</em> before they would take up any significant storage.</p>

	<br>

	<h3>Do you have a free plan?</h3>
	<p>Yes we do! You can try out Mirror Island free, forever. However, storage is limited to 100MB per account.</p>

	<br>

	<h3>Can I get a refund?</h3>
	<p>Yes. All plans come with a 30-day guarantee. If you decide that it doesn't fit your needs before the 30 days are over, you can get a complete refund during that 30-day guarantee period; no questions asked. After the 30 day period, refunds are handled &quot;pro-rata&quot;, where you will be refunded for the remaining time left on the plan.</p>

	<br>

	<h3>Can I use this for a business?</h3>
	<p>Yes! All plans can be used by anyone, including for commercial purposes.</p>

	<br>

	<h3>Are these prices per user?</h3>
	<p>No, the quotas above apply <strong>per account</strong>. You can share the account between multiple people if you wanted to.</p>

	<br>

	<h3>What happens if I run out of storage space?</h3>
	<p>You can keep uploading files as long as there is available space for it. If the uploaded file is larger than the available space it won't be saved. Here you have two choices; you can delete old files you don't need, or purchase more storage space.</p>

	<br>

	<h3>Can I upgrade or downgrade at any time?</h3>
	<p>Yes you can! However when an account is downgraded, the system will automatically remove older files if you have used up more space than you have been given with the downgraded account. You can avoid this by clearing or backing up some files yourself before downgrading.</p>

	<br>

</div>

<script>
function showMonthly()
{
	$('pricingToggleCheckboxTop').checked = false;
	$('leftLabelTop').classList.add("pricingswitchlabel-active");
	$('rightLabelTop').classList.remove("pricingswitchlabel-active");
	$('pricePersonalMonthTop').classList.remove('silvertext');
	$('pricePersonalYearTop').classList.add('silvertext');
	$('priceAdvancedMonthTop').classList.remove('silvertext');
	$('priceAdvancedYearTop').classList.add('silvertext');
	$('priceProfessionalMonthTop').classList.remove('silvertext');
	$('priceProfessionalYearTop').classList.add('silvertext');

	$('pricingToggleCheckboxBottom').checked = false;
	$('leftLabelBottom').classList.add("pricingswitchlabel-active");
	$('rightLabelBottom').classList.remove("pricingswitchlabel-active");
	$('pricePersonalMonthBottom').classList.remove('silvertext');
	$('pricePersonalYearBottom').classList.add('silvertext');
	$('priceAdvancedMonthBottom').classList.remove('silvertext');
	$('priceAdvancedYearBottom').classList.add('silvertext');
	$('priceProfessionalMonthBottom').classList.remove('silvertext');
	$('priceProfessionalYearBottom').classList.add('silvertext');
}

function showYearly()
{
	$('pricingToggleCheckboxTop').checked = true;
	$('rightLabelTop').classList.add("pricingswitchlabel-active");
	$('leftLabelTop').classList.remove("pricingswitchlabel-active");
	$('pricePersonalMonthTop').classList.add('silvertext');
	$('pricePersonalYearTop').classList.remove('silvertext');
	$('priceAdvancedMonthTop').classList.add('silvertext');
	$('priceAdvancedYearTop').classList.remove('silvertext');
	$('priceProfessionalMonthTop').classList.add('silvertext');
	$('priceProfessionalYearTop').classList.remove('silvertext');

	$('pricingToggleCheckboxBottom').checked = true;
	$('rightLabelBottom').classList.add("pricingswitchlabel-active");
	$('leftLabelBottom').classList.remove("pricingswitchlabel-active");
	$('pricePersonalMonthBottom').classList.add('silvertext');
	$('pricePersonalYearBottom').classList.remove('silvertext');
	$('priceAdvancedMonthBottom').classList.add('silvertext');
	$('priceAdvancedYearBottom').classList.remove('silvertext');
	$('priceProfessionalMonthBottom').classList.add('silvertext');
	$('priceProfessionalYearBottom').classList.remove('silvertext');
}

leftLabelTop.addEventListener("click", showMonthly);
rightLabelTop.addEventListener("click", showYearly);
pricingToggleCheckboxTop.addEventListener("click", function(){
	if (pricingToggleCheckboxTop.checked) {
		showYearly();
	} else {
		showMonthly();
	}
})

leftLabelBottom.addEventListener("click", showMonthly);
rightLabelBottom.addEventListener("click", showYearly);
pricingToggleCheckboxBottom.addEventListener("click", function(){
	if (pricingToggleCheckboxBottom.checked) {
		showYearly();
	} else {
		showMonthly();
	}
})

/* Update labels with checkmark
 * (some browsers keep check state between page loads) */
if (pricingToggleCheckboxTop.checked) {
	showYearly();
} else {
	showMonthly();
}

</script>

</div>
</div>
