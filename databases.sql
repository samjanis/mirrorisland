-- --------------------------------------------------------

--
-- Database structures for Mirror Island.
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `fullname` tinytext,
  `username` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `sessionid` tinytext,
  `added` datetime DEFAULT NULL,
  `accessed` datetime DEFAULT NULL,
  `viewcount` int(11) DEFAULT NULL,
  `address` tinytext,
  `address2` tinytext,
  `town` tinytext,
  `state` tinytext,
  `postcode` tinytext,
  `country` tinytext,
  `email` tinytext,
  `website` tinytext,
  `interests` mediumtext,
  `about` mediumtext,
  `profileimagefileid` int(11) DEFAULT NULL,
  `backgroundimagefileid` int(11) DEFAULT NULL,
  `viewpermissions` int(11) DEFAULT NULL,
  `editpermissions` int(11) DEFAULT NULL,
  `replypermissions` int(11) DEFAULT NULL,
  `approvepermissions` int(11) DEFAULT NULL,
  `moderatepermissions` int(11) DEFAULT NULL,
  `tagpermissions` int(11) DEFAULT NULL,
  `indexforsearch` tinyint(1) DEFAULT NULL,
  `indexnewposts` tinyint(1) DEFAULT NULL,
  `license` tinytext,
  `licenselaterversions` tinyint(1) DEFAULT NULL,
  `confirmedage` int(11) DEFAULT NULL,
  `termsofuse` mediumtext,
  `termsofusedate` datetime DEFAULT NULL,
  `messagesendonenterkey` tinyint(1) DEFAULT NULL,
  `messagecloseonsend` tinyint(1) DEFAULT NULL,
  `messagepopuponreceive` tinyint(1) DEFAULT NULL,
  `messagesoundonreceive` tinyint(1) DEFAULT NULL,
  `messagetypingstatustype` tinytext,
  `onlinestatus` tinyint,
  `onlinestatusmessage` tinytext,
  `sseconnectionid` tinytext,
  `sidepanelvisible` tinyint(1),
  `accounttype` tinytext,
  `filespace` bigint,
  `filesize` int,
  `postsize` int,
  `stripecustomerid` tinytext,
  `stripesubscriptionid` tinytext,
  `stripesubscriptionlatestinvoice` tinytext,
  `stripesubscriptionupcominginvoice` tinytext,
  `stripesubscriptionstatus` tinytext,
  `stripesubscriptioncurrentperiodstart` datetime,
  `stripesubscriptioncurrentperiodend` datetime,
  `stripesubscriptionpriceid` tinytext,
  `stripesubscriptionquantity` int(11),
  `stripesubscriptionlastchargeid` tinytext,
  `stripepaymentmethodid` tinytext,
  `stripepaymentlast4` tinytext,
  `stripepaymentbrand` tinytext,
  `stripepaymentexpiry` tinytext,
  `accountemail` text,
  `resetlinkid` text,
  `resetlinkadded` datetime
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `analytics`
--

CREATE TABLE `analytics` (
  `id` bigint AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `pageid` text,
  `owneraccountid` bigint,
  `phpsessionid` tinytext,
  `added` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `weekday` tinyint DEFAULT NULL,
  `referrer` text,
  `country` tinytext,
  `state` tinytext,
  `city` tinytext,
  `browser` tinytext,
  `browserversion` tinytext,
  `javascriptenabled` boolean,
  `operatingsystem` tinytext,
  `operatingsystemversion` tinytext,
  `windowwidth` int,
  `action` tinytext,
  `authorised` boolean
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `approvals`
--

CREATE TABLE `approvals` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `tablename` tinytext,
  `tableid` int(11) DEFAULT NULL,
  `accountid` int(11) DEFAULT NULL,
  `trusted` tinyint(1) DEFAULT NULL,
  `hidden` tinyint(1) DEFAULT NULL,
  `added` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bookmarks`
--

CREATE TABLE `bookmarks` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `accountid` int(11) DEFAULT NULL,
  `postid` int(11) DEFAULT NULL,
  `subscription` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `accountid` int(11) DEFAULT NULL,
  `contactid` int(11) DEFAULT NULL,
  `viewpermissions` int(11) DEFAULT '0',
  `editpermissions` int(11) DEFAULT '0',
  `replypermissions` int(11) DEFAULT '0',
  `approvepermissions` int(11) DEFAULT '0',
  `moderatepermissions` int(11) DEFAULT '0',
  `tagpermissions` int(11) DEFAULT NULL,
  `trusted` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `fileindex` tinytext,
  `accountid` int(11) DEFAULT NULL,
  `newpostsessionid` int(11) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `viewcount` int(11) DEFAULT NULL,
  `postid` int(11) DEFAULT NULL,
  `viewpermissions` int(11) DEFAULT NULL,
  `editpermissions` int(11) DEFAULT NULL,
  `replypermissions` int(11) DEFAULT NULL,
  `approvepermissions` int(11) DEFAULT NULL,
  `moderatepermissions` int(11) DEFAULT NULL,
  `tagpermissions` int(11) DEFAULT NULL,
  `indexforsearch` tinyint(1) DEFAULT NULL,
  `filename` tinytext,
  `filesize` int(11) DEFAULT NULL,
  `filewidth` int(11) DEFAULT NULL,
  `fileheight` int(11) DEFAULT NULL,
  `filemime` tinytext,
  `s3profile` tinytext,
  `s3endpoint` tinytext,
  `s3region` tinytext,
  `s3bucket` tinytext,
  `tinyimg` text,
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `flags`
--

CREATE TABLE `flags` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `tablename` tinytext,
  `tableid` int(11) DEFAULT NULL,
  `flagcount` int(11) DEFAULT NULL,
  `flagreason` tinytext DEFAULT NULL,
  `flagagerating` int(11) DEFAULT NULL,
  `flagactive` tinyint(1) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `accountid` int(11) DEFAULT NULL,
  `postid` int(11) DEFAULT NULL,
  `trusted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `membertree`
--

CREATE TABLE `membertree` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `postid` int(11) DEFAULT NULL,
  `parentid` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `microdata`
--

CREATE table `microdata` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `postid` int(11) DEFAULT NULL,
  `accountid` int(11) NOT NULL,
  `itemprop` tinytext DEFAULT NULL,
  `strval` tinytext DEFAULT NULL,
  `intval` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `passwords`
--

CREATE TABLE `passwords` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `accountid` int(11) DEFAULT NULL,
  `password` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pins`
--

CREATE TABLE `pins` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `postid` int(11) DEFAULT NULL,
  `parentpostid` int DEFAULT NULL,
  `pinstate` tinyint(1) DEFAULT NULL,
  `added` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `parentid` int(11) DEFAULT NULL,
  `accountid` int(11) DEFAULT NULL,
  `emaildisplayname` tinytext,
  `emailusername` tinytext,
  `emailserver` tinytext,
  `emailinbox` tinytext,
  `added` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `indexed` datetime DEFAULT NULL,
  `viewcount` int(11) DEFAULT '0',
  `viewpermissions` int(11) DEFAULT '0',
  `editpermissions` int(11) DEFAULT '0',
  `replypermissions` int(11) DEFAULT '0',
  `approvepermissions` int(11) DEFAULT '0',
  `moderatepermissions` int(11) DEFAULT '0',
  `tagpermissions` int(11) DEFAULT NULL,
  `viewmode` text DEFAULT NULL,
  `sortmode` text DEFAULT NULL,
  `sortreverse` boolean DEFAULT FALSE,
  `indexforsearch` tinyint(1) DEFAULT NULL,
  `agerating` int(11) DEFAULT NULL,
  `istemplate` tinyint(1) DEFAULT NULL,
  `istitlepage` tinyint(1) DEFAULT NULL,
  `isdirectory` tinyint(1) DEFAULT NULL,
  `bannerfileid` int(11) DEFAULT NULL,
  `description` longtext,
  `title` tinytext DEFAULT NULL,
  `image` tinytext DEFAULT NULL,
  `searchcache` longtext
) ENGINE=InnoDB AUTO_INCREMENT=1200 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recipients`
--

CREATE TABLE `recipients` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `tablename` tinytext,
  `tableid` int(11) DEFAULT NULL,
  `senderaccountid` int(11) DEFAULT NULL,
  `recipientaccountid` int(11) DEFAULT NULL,
  `emaildisplayname` tinytext,
  `emailusername` tinytext,
  `emailserver` tinytext,
  `added` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `searchindex`
--

CREATE TABLE `searchindex` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `tablename` tinytext,
  `tableid` int(11) DEFAULT NULL,
  `keywordid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `searchkeywords`
--

CREATE TABLE `searchkeywords` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `keyword` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE table `templates` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `accountid` int(11) NOT NULL,
  `indexfileid` int(11),
  `useindex` boolean
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD UNIQUE KEY `username` (`username`);

