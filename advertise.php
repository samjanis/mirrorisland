<!DOCTYPE html>
<html>
<head>
<title>Advertising on Mirror Island</title>
<style type="text/css">
body { font:normal 16px 'sans-serif'; font-weight:semi-bold; color:#567; background:#f0f0f3; padding:0px; margin:0px; line-height:24px; }
h1, h2, h3 { font-weight:bold; font-face:'sans-serif'; color:#345; }
h1 { font-size:18px; text-align:center; text-transform:uppercase; }
h2, h3 { font-size:16px; text-align:left; }
h2 { font-weight:bold; text-transform:uppercase; }
h3 { letter-spacing:1px; }
p { margin:16px 0 0 0; }
.maindiv { background:#fafafe; max-width:600px; padding:15px 25px; margin:10px auto 10px auto; border:solid 1px #dde; border-radius:8px; }
#footer { color:#79a; padding:10px 20px; text-align:center; margin:30px 0 0 0; border-top:solid 1px #cde; }
#footer div { margin:10px; }
#footer a { background:none; color:#57f; text-decoration:none; margin:auto 4px; }
#footer a:hover { text-decoration:underline; }
</style>
</head>

<body>

<div class="maindiv">

<center>
<h1>Advertising on Mirror Island</h1>
<a href="/"><img src="/milogo.png"></a>
<p>June 3, 2018</p>
</center>

<!-- Hey there! This is planned...

<h2>Acceptable Ads Policy</h2>

<p>Only text links with an accompanying short description are displayed. The URL must point to top-level domains with optional subfolders.</p>

<h3>Advertiser Conduct</h3>

<p>All ads must accept all messages, even from non-members regarding the suitability of their ad.</p>

<h2>Relevance</h2>

<p>Ads are relevant based on the currently displayed content. No history is recorded of who has seen advertisements or which page it was displayed on.</p>

<p>It is possible to display ads both with targeted keywords, and at random without any relevance to the content being displayed.</p>

<h2>Campaign Features</h2>

<p>During advertising campaigns, you will be provided with tools allowing you to monitor the performance of your advertisements. Data provided includes the time and date of each click, and when your advertisement is displayed with targeted keywords.</p>

<p>Throughout the campaign, you can change which keywords to display the ad with, and to pause and resume the campaign.</p>

<h2>Rates</h2>

<p>Advertising campaigns can be one of three methods; time-based, impression-based and click-through. For impression based an clickthrough rates, you can choose to pause and resume your advertising campaign at any time to extend its duration.</p>

<h3>Time based rates</h3>

<p>$10 AUD per 24-hour block. Time-based ads have unlimited impression and click-through quantities.<p>

<h3>Impression based rates</h3>

<p>$10 AUD per 10,000 impressions. Impression-based ads have unlimited click-through quantities, and will be continuously displayed until the requested amount of impressions have been reached.<p>

<h3>Clickthrough Rates</h3>

<p>$10 AUD per 100 clicks. Your advertisemnt will be displayed continuously at no cost until somebody clicks on it - no click, no fee.</p>

<h3>Ad Rotation</h3>

<p>All advertisiments have been shown evenly for the duration of each advertiser&rsquo;s campaign. If you would like your ad to be shown more you can change the target keywords and monitor its display count.</p>

<p>You can monitor the performance of your advertising campaign by the amount of times it has been displayed to adjust which keywords are targeted.</p>

// Well, that's the proposed advertising features. -->

<p>Advertisements on this site are currently not implemented although it is a feature planned for future releases.</p>

<p>Until then, you can play around with the ad preview tool below.</p>

<h3>Preview</h3>

<div style="margin-bottom:10px;">
	<input type="text" placeholder="Your Company Name" style="width:160px; margin-right:10px;" onkeyup="document.getElementById('desktopadheader').innerHTML=(this.value.trim()?this.value.trim():this.placeholder);"><input type="text" style="width:160px;" placeholder="www.YourWebsite.com" onkeyup="document.getElementById('desktopadurl').innerHTML=(this.value.trim()?this.value.trim():this.placeholder);">
</div>
<div style="margin-bottom:10px;">
	<textarea style="font:inherit;font-size:14px;width:330px;height:44px;padding:0 4px;resize:none;line-height:20px;"
			placeholder="A short text describing the products or services you would like to advertise."
			onkeyup="document.getElementById('desktopaddescription').innerHTML=(this.value.trim()?this.value.trim():this.placeholder);"></textarea>
</div>

<div style="width:100%;border:solid 1px #69a; border-radius:8px; font:normal 14px 'sans-serif';line-height:20px;padding:11px 15px;box-sizing:border-box; background:#fff; ">
	<div style="white-space:nowrap;overflow:hidden;text-overflow:ellipsis;color:#00c;">
		<span style="border:solid 1px #00c;border-radius:4px;background:#fff;color:inherit;font-size:10px;padding:1px 3px;vertical-align:bottom;margin-right:10px;">AD</span><span id="desktopadheader" style="font-weight:bold;">Your Company Name</span></div>
	<div id="desktopaddescription" style="max-height:40px;overflow:hidden;letter-spacing:0.2pt;">A short text describing the products or services you would like to advertise.</div>
	<div id="desktopadurl" style="color:#171;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;">www.YourWebsite.com</div>
</div>

<div id="footer">
	<div>
		<a href="/"><strong>Mirror&nbsp;Island</strong></a>
		<a href="/about.php">About</a>
		<a href="/contact.php">Contact Us</a>
		<a href="/terms.php">Terms of Use</a>
		<a href="/manifesto.php">Manifesto</a>
		<a href="/quickstart.php">Quickstart Guide</a>
		<a href="/features.php">Feature Tour</a>
		<a href="/advertise.php">Advertise</a>
		<a href="/reportabug.php">Report a Bug</a>
	</div>
</div>

</div>

</body>
</html>
