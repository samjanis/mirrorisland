<html>
<head>
<title>Contact Us on Mirror Island</title>
<style type="text/css">
body { font:normal 16px Arial,Helvetica,sans-serif; font-weight:semi-bold; color:#567; background:#f0f0f3; padding:0px; margin:0px; line-height:24px; }
h1, h2, h3 { font-weight:bold; font-face:'sans-serif'; color:#345; }
h1 { font-size:18px; text-align:center; text-transform:uppercase; }
h2, h3 { font-size:16px; text-align:left; }
h2 { font-weight:bold; text-transform:uppercase; }
h3 { letter-spacing:1px; }
p { margin:16px 0 0 0; }
.maindiv { background:#fafafe; max-width:600px; padding:15px 25px; margin:10px auto 10px auto; border:solid 1px #dde; border-radius:8px; }
#footer { color:#79a; padding:10px 20px; text-align:center; margin:30px 0 0 0; border-top:solid 1px #cde; } 
#footer div { margin:10px; }
#footer a { background:none; color:#57f; text-decoration:none; margin:auto 4px; }
#footer a:hover { text-decoration:underline; } 
</style>
</head>

<body>

<div class="maindiv">

<center>
<h1>Contact Us on Mirror Island</h1>
<a href="/"><img src="/milogo.png"></a>
<p>December 31, 2017</p> 
</center> 

<p>Send all email to:</p>

<div style="text-align:center;"><big>samjanis@mirrorisland.com</big></div>

<p>and we will get in contact with you as soon as possible (usually within a day).</p>

<div id="footer">
	<div>
		<a href="/"><strong>Mirror&nbsp;Island</strong></a>
		<a href="/about.php">About</a>
		<a href="/contact.php">Contact Us</a>
		<a href="/terms.php">Terms of Use</a>
		<a href="/manifesto.php">Manifesto</a>
		<a href="/quickstart.php">Quickstart Guide</a>
		<a href="/features.php">Feature Tour</a>
		<a href="/advertise.php">Advertise</a>
		<a href="/reportabug.php">Report a Bug</a>
	</div>
</div>

</div>

</body>
</html>
