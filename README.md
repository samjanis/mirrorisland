# Mirror Island

Copyright (C) 2024 Sam and Janis Allen
samjanis@mirrorisland.com

Mirror Island is a multipurpose web interface that changes its behaviour depending on the content being shown. It takes advantage of various web standards and internet based services to make this possible.

Its functionality is somewhere between a traditional CMS, and that of a social network website.

It is currently under development with the primary focus being data input and usability.

At the time of writing, the latest working version can be accessed on:
https://www.mirrorisland.com/

Geolocating for analytics uses IP2Location LITE data available from: https://lite.ip2location.com

>
> This README is brief and by no means is concise, however new information is being added over time to describe the installation process in more detail.
>
> It currently assumes that the reader has **some** knowledge of MySQL and Apache (or compatible server software, such as MariaDB or LiteSpeed).
>

## System Requirements

Any device capable of connecting to a network, whether it is local or wide, and capable of running the software required below is capable of running the Mirror Island web interface. Even though the main executable is roughly half a megabyte, the web interface relies heavily on database access and is a task that relies on a generous amount of free memory.


## Hardware Requirements

- 200Mhz CPU of better (1Ghz or more recommended)
- 256MB RAM (2GB DDR2 or better recommended)
- 10MB free disk space. (Depending on usage, this can easily reach gigabytes)
- IPv4/IPv6 capable network connection. (Can run on localhost only if needed)


## Software Requirements

- Any operating system with network access or localhost server.
- Apache or Litespeed, or any server compatible with .HTACCESS and mod_rewrite.
- PHP 5 or 7, or later.
- MySQL or MariaDB.
- PHP5-GD or any compatible version for image processing.
- sendmail
- mailparse (php package)
- mbstring (php package)

### Additional Extras

- xsendfile if using Apache.
- Access to an SMTP server for sending emails.
- (Optional) imgzoom.js for image viewing. Download `imgzoom.js' from https://gitlab.com/samjanis/imgzoom.js and copy to the same directory as `index.php`
- (Optional) Amazon AWS S3 or compatible (including Wasabi S3) for cloud storage.
- (Optional) If using S3 above, you will also need the Amazon AWS SDK for PHP.
- (Optional) Parsedown if using markdown parser. Download `Parsedown.php` from https://parsedown.org and copy the PHP file to the same directory as `index.php`
- (Optional) sysvmsg for PHP if using websockets or Server-Sent Events (SSE), used for interprocess communication.


## Terminology

**Server** describes the computer running Apache, MySQL and PHP, whether it is connected to a network or not. The actual software can vary from one server to another, such as Apache can be replaced by Litespeed, and MySQL can be replaced by MariaDB.

**public_html** is the assumed root folder of the web server's storage location. In some setups, especially Linux or Unix compatible this can be `/var/www`, although for simplicity this location will be referred to as `public_html` for the rest of this documentation.

**S3 storage** refers to Amazon AWS (Amazon Web Services) S3 (Simple Storage Solutions) or any S3 compatible service, including Wasabi hot cloud storage.


## Installation Process

### Set Up A Clean AMP Environment

The installation procedure can vary between operating systems so you will need to refer to other documents for setting up the web server. Some keywords that might help you if you choose to search on the internet or if asking for support from others include; WAMP for Microsoft Windows, or LAMP for GNU/Linux.

Alternatively, you can purchase hosting online which will provide you with a clean AMP environment from the start.

### Copy Files To The Server

If you downloaded all files in an archive, unpack the archive in the base directory of the AMP server. This should be a folder named `public_html`

Alternatively, the latest version of all files can be copied from the GitLab repository, using the command `git clone https://gitlab.com/samjanis/mirrorisland.git` which will then have all files in a newly downloaded `mirrorisland` directory. The files inside the `mirrorisland` directory can then be copied to the base directory of the AMP server, which should be `public_html`.

1. Create a new directory inside `public_html` directory, named `files`. This directory is needed in the same directory as `index.php` to host uploaded photos and attachment, and needs read, write and execute permissions for the owner, but only execute permissions for group and world. This can be done in a few ways:
	1. Depending on your web host set up you should be able to set those permissions with the file manager of the web console, such as cPanel.
	2. If you have access to a terminal or shell, run the commands `chmod 0711 files`
	3. Otherwise, you can set the permissions to be 711 or rwx--x--x in your preferred file management utility or FTP client.
2. Inside the `files` directory, create a new file named `.htaccess` with the following line to restrict unauthorised access to the files within: `Deny from all`

At this point, the first stage is complete. The next steps are to set up the database and then add the configuration files.


## Database Setup

You will need to choose a username and password for the database user, as well as the name of the database itself as this will need to be entered in a configuration file later on.

Create a new database and add a database user for the database with `INSERT`, `SELECT`, `UPDATE` and `DELETE` permissions. This can be done in a number of ways; using CPanel or the MySQL Monitor from the terminal are two methods, and are described below.

### Method 1 - CPanel Method

1. After signing in, in the section **Databases** click on **MySQL Databases**.
2. In the section **Create New Database** enter the database name and click **Create Database**. *NOTE: Watch out for any prepended names, such as `webhost_` since this will also need to be added in the configuration file below.*
3. After creating the database, go back to the *MySQL Databases* page and scroll down to the **MySQL Users - Add New User**.
4. Enter the username and password in the required fields. *Just like the database name any prepended names, such as `webhost_` will also need to be added in the configuration file below.*
5. After creating the database user, go back to the *MySQL Databases* page and scroll down to the **Add User To Database** section.
6. Set **User** to the database user made in step 4, and **Database** to the database name entered in step 2, then click **Add**.
7. In the following page under **Manage User Privileges** only check the boxes **DELETE**, **INSERT**, **SELECT**, and **UPDATE**. All other checkboxes must be unchecked.
8. Click **Make Changes** at the bottom of the page, then **Go Back**.
9. Click on the logo on the top-left to go to the cPanel home page, then in the section **Databases** click on **phpMyAdmin**.
10. In the new window that pops up, on the left side click on the database name you entered in step 2.
11. Click on **Operations** on the top of the page, then in the section **Collation** click on the popup list and choose `utf8mb4_unicode_ci`, then click **Go** directly underneath it.
12. Click on **Import** on the top of the page.
13. In the section **File to import:** click on the *Browse* button (this button can vary between different computers), and select the file `databases.sql` provided in the source folder of this website, then click **Go** on the bottom of the page.
14. Setting up the database is now complete.

### Method 2 - MySQL Monitor Method

1. Open a terminal and navigate to the root folder of the website, `public_html`. You can do this through the terminal with the `cd` command or open a terminal from the location with a file manager.
2. Log in to the MySQL with the command `mysql -u root -p` and enter your administrator password.
3. At the **MySQL** or **MariaDB** prompt, enter the following commands, replacing *database_name*, *database_username*, and *database_password* with the required information:
	1. `create database database_name character set utf8mb4 collate utf8mb4_unicode_ci;`
	2. `grant select, insert, update, delete on *.* to 'database_username'@'localhost' identified by 'database_password' with grant option;`
	3. `use database_name;`
4. With the empty database selected, run this command to set up the required database tables: `\. ./databases.sql`
5. Setting up the database is now complete. To exit the Mysql terminal, enter `\q`


## Configuration

>
> An additional file will need to be added.
>
> **connectdbinfo** contains basic settings and passwords. This file can not be accessed by the public to keep passwords safe.
>

### connectdbinfo.php

To connect the website to the database, a file named **connectdbinfo.php** needs to be added in the same folder with "index.php" in the root folder of the web server.

The contents of **connectdbinfo.php** you will need to enter starts
with `<?php` and end with `?>`, and will have lines similar to:

```
<?php
$dbhost = "localhost";
$dbuser = "username";
$dbpass = "password";
$dbname = "database_name";
$bgfuncpass = "bgfunc_password";
$servertitle = "Server Title";
$servername = "servername.com";
$urlhome = "https://www.servername.com/";
$copyrightfootertext = "Copyright &copy; 2018 Website Author";
$websockuri = "https://www.servername.com:8090/websock.php";
$homepageusername = '';
$homepageviewcount = 0;
$stripepublicapikey = 'pk_9Eiip0CLdDHzWE5aL9oMk4wx';
$stripesecretapikey = 'sk_nahVvcEL5TvLt9l3Rx1bSb7b';
$stripewebhooksecret = 'whsec_L95pMDe2qEg4GCLt9lg9IlOMSbSQuQ6V';
$mailerusername = 's33.mailhost.net';
$mailerusername = 'no-reply@mailhost.net';
$mailerpassword = 'ThePasswordForMailerUsername';
$mailerport = 465;
$headtags = <<<HEADERS
<script src="/imgzoom.js"></script>
<script>
	const imgzoom_className = "postimg";
</script>
HEADERS;
?>
```

You well need to replace the "localhost", "username", "password" and "database_name" fields with the required information associated with the database connection.

The other fields that need to replaced, depending on your situation are:

- **bgfuncpass** : REQUIRED; A secret passphrase that allows internal functions to talk to eachother. It doesn't matter what you put in here, as long as it's a valid PHP string, is less than 250 characters, and would be hard to guess.

- **servertitle** : The name of the website. This can be the name of a person or a two to three word description of what the website is about, such as "The Card House" or "Elva Sanvik".

- **servername** : A short hand human-readable version of the website URL this website is hosted on. In this example it is "servername.com"

- **urlhome** : The full URL of the website without the trailing slash. In this example it is "https://www.servername.com"

- **copyrightfootertext** : A short description stating the copyright status, year and holder of the content added by the owner of this site. The copyright status only applies to non user-submitted data and multimedia that would of been added by the website's maintaner.

- **websockuri** : The uri of the websock server that comes included with this project. Usually it follows `https://www.servername.com:8090/websock.php` where `www.servername.com` is the same as the variable `urlhome` above.

- **homepageusername** : Identifies who's posts are shown on the home page. Replies to any post are unaffected. Usernames specified here are also prevented from being flagged. Values are:

	- blank (as shown above with '') or not setting this value at all allows anybody to post on the home page stream, whether they are a member of this website or not.
	- an asterisk (if the value is '*') only allows members of this website to post while they are signed in.
	- specifying a username or list of comma-separated usernames (such as 'samjanis1' or 'samallen1, janisallen1') will only allow display that user's (or users') posts on the home page.

- **homepageviewcount** : Posts with the required number of view counts will also be displayed on the home page.
- **stripepublicapikey** : Used for purchases when using Stripe. You can get this key from the developer's console on your Stripe account.
- **stripesecretapikey** : Used for purchases when using Stripe. You can get this key from the developer's console on your Stripe account.
- **stripewebhooksecret** : Used for purchases when using Stripe. You can get this key from the developer's console on your Stripe account.

- **$mailerusername** : The URI of your chosen SMTP server.
- **$mailerusername** and **$mailerpassword** : The username and password of the email account to send from. Note that the actual "From" account will be different depending on which account is being used on the hosted Mirror Island instance.
- **$mailerport** : By default it should be **465** unless your chosen STMP server is different.

- **$headtags** : Additional HTML to be added before the closing `</head>` tag. Can be a simple string, or a PHP "heredoc" to use a multiline string.

>
> As a security note, this file is secured by the <FilesMatch> directive in the ".htaccess" file.
>
> Just make sure that the server this website is hosted on supports .htaccess files for the security feature to work.
>


## Receiving Emails

To process emails, they will need to be piped to the `emailhandler.php` script.

To do this in cPanel:

1. From the cPanel home page scroll down to **Email** then select **Default Address**.
2. Under "*Send all unrouted email for the following domain*" select the domain you want to set up.
3. Click on **Advanced Options** to reveal more options if it hasn't been expanded yet.
4. Select **Pipe to a Program** option.
5. Under the selected option enter the path to the script after the "Home" icon, such as: `public_html/emailhandler.php` but this can vary between servers or cPanel versions, so check the documentation for your version of cPanel and possibly your website host on entering the appropriate file path.

## Using SSL

If your server has been configured with a secure connection (SSL), an option in the .htaccess file can redirect all traffic through the secure connection. Under the heading "Force HTTPS:" remove the comment marks (#) at the start of the two lines under that heading, so it should end up looking like this:

```
# Force HTTPS: (Uncomment only if available)
RewriteCond %{HTTPS} off
RewriteRule .* https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
```


## Using Apache VirtualHost

When running your own Apache server (from home or on a private network), sometimes it is easier to run an instance of Mirror Island from the "public_html" folder in your home folder. To do this, a ".conf" file for each website instance will have to be created. In Ubuntu/Debian (and possibly other Linux variants) this file is located in /etc/apache2/sites-available.

An example .conf file is shown below saved with the filename `demosite.conf` (although you might need to replace the name `demosite` with your chosen domain name):

```
NameVirtualHost *:80

<VirtualHost *:80>
	ServerName demosite
	ServerAlias demosite *.demosite
	DocumentRoot /home/username/public_html/demosite/

	<Directory /home/username/public_html/demosite/>
		Options Indexes FollowSymLinks
		AllowOverride All
		Order allow,deny
		allow from all
	</Directory>
</VirtualHost>
```

Replace the above words `demosite` with your chosen domain name (on the local server) as well as `/home/username/public_html/demosite` with the correct file path to the server's root folder, then enable the site in Apache with `a2ensite demosite`, again replacing `demosite` with your chosen domain name.

Lastly, to allow web browsers access to the working website, add the following line to /etc/hosts (replacing `demosite` with your chosen domain name):

```
127.0.0.1 demosite
```

You can now access the local website in your browser by typing `http://demosite/` (replacing `demosite` with your chosen domain name).

### Troubleshooting VirtualHosts

If the PHP code is displayed rather than being executed, try adding the following line

```
		php_admin_flag engin on
```

between the `<Directory ...>` tag and above the line `Options Indexes FollowSymLinks` so it looks like this:

```
	<Directory /home/username/public_html/demosite/>
		php_admin_flag engin on
		Options Indexes FollowSymLinks
		AllowOverride All
		...
```

## Using S3 Storage (Optional)

If the total uploaded files are likely to exceed the server's disk capacity, Mirror Island is able to automatically use S3 storage if given.

Before setting up your instance of Mirror Island to connect with an S3 provider, you will need to download and install the AWS SDK, and unpack it in a folder named `aws` in the same folder as `index.php`.

To enable S3 storage, 4 additional lines are needed in `connectdbinfo.php`, as well as an additional file located in `/.aws/credentials`.

Create a new folder in the root folder of your server named `/.aws` and inside that create a text file named `credentials` with the following lines:

```
[default]
aws_access_key_id = YOURKEYID
aws_secret_aceess_key = YourSecretAccessKey

[profilename]
aws_access_key_id = YOURKEYID
aws_secret_aceess_key = YourSecretAccessKey
```

You will need to replace the above values with the keys obtained for your S3 service:

- **profilename** : Names the key id and secret access key pair for later use. The **default** key/secret key pair is usually a copy of the most used keys.
- **YOURKEYID** and **YourSecretAccessKey** : The key/secret key pair are obtained from your S3 service provider.


In your `connectdbinfo.php` file, add the following lines:

```
$s3profile = 'profilename';
$s3endpoint = 'endpoint';
$s3region = 'region';
$s3bucket = 'bucketname';
```

You will need then to replace the above values with the required information to use your S3 service for cloud storage:

- **profilename** : Name of profile to use in `/.aws/credentials` for key/secret key pair.
- **endpoint** : URI of S3 server. Usually in the form of `https://s3.region.server.com/`.
- **region** : S3 region to use. e.g.: `us-west-1` or `ap-southeast-1`.
- **bucketname** : Name of bucket to use.


## Troubleshooting Problems

### AWS Can not read credentials error

When using the AWS SDK for S3 storage, you might encounter this error in Apache's `error_log` when files aren't being transferred properly:

```
PHP Fatal error:  Uncaught Aws\Exception\CredentialsException: Cannot read credentials from /.aws/credentials...
```

By setting the `HOME` env, this should be resolved. To do this you will need to find out your home path, which is usually something like `/home/yourusername`, and change the example HOME path to match yours in the top of the .htaccess file where it says:

```
# Set default home folder. You will need to edit this to suit your
# deployment environment before uncommenting, if needed.
#SetEnv HOME /home/mirrorisland
```

Then uncomment the line by removing the # symbol in front of the **SetEnv HOME** line for the changes to be applied immediately.

EOF
