<?php
/* Get image size */
$d = strtolower(substr($_GET['d'], 0, 20));
$wd = 0;
$ht = 0;
$rgb = '';
if (preg_match('/^(\d+)x(\d+)-([a-f0-9]{6,6})$/', $d, $matches)) {
	$wd = intval($matches[1]);
	$ht = intval($matches[2]);
	$rgb = $matches[3];
} else {
	exit();
}

if (($wd <= 0) || ($ht <= 0) || !$rgb) {
	exit();
}

if ($wd > 4096) $wd = 4096;
if ($ht > 4096) $ht = 4096;

/* Get image colour */
$rgbarr = sscanf($rgb, '%02x%02x%02x');
if (!$rgbarr) {
	exit();
}
list($r, $g, $b) = $rgbarr;

/* Get data if drawing a sparkline */
$sl = false;
$slrgb = '';
$smin = 0;
$smax = 0;
$s = substr($_GET['s'] ?? '', 0, 8192);
if (preg_match('/^([a-f0-9]{6,6}),([\d\-.,]+)$/', $s, $matches)) {
	$slrgb = $matches[1];
	$sl = explode(',', $matches[2]);

	/* At least 2 points needed for sparkline */
	if (count($sl) < 2) {
		$sl = false;
	} else {
		/* Get min and max values */
		$slsorted = array_filter($sl, 'is_numeric'); /* Remove any NULL entries */
		sort($slsorted);
		$smin = $slsorted[0];
		$i = count($slsorted);
		$smax = $slsorted[$i-1];

		/* Force middle-line if all list values are same
		 * e.g. all zeros or all 100's */
		if ($smin === $smax) {
			$smin--;
			$smax++;
		}
	}
}

/* Create the image */
header('Content-type: image/png');
$im = @imagecreatetruecolor($wd, $ht);
$colour = imagecolorallocate($im, $r, $g, $b);
imagefill($im, 0, 0, $colour);

/* Check if drawing a sparkline */
if ($sl) {
	$rgbarr = sscanf($slrgb, '%02x%02x%02x');
	if (!$rgbarr) {
		exit();
	}
	list($r, $g, $b) = $rgbarr;
	$colour = imagecolorallocate($im, $r, $g, $b);

	imagesavealpha($im, TRUE);
	imagealphablending($im, TRUE);
	imageantialias($im, TRUE);
	imagesetthickness($im, 1);
	$range = $smax - $smin;
	$x = $y = $xp = $yp = 0;
	$sct = count($sl) - 1;
	foreach ($sl as $i => $v) {
		if (is_numeric($v)) {
			$y = ($ht - 2) - floor((($v - $smin) / $range) * ($ht - 2));
		}
		if ($i !== 0) {
			$x = ceil(($i  / $sct) * ($wd - 1));
			if (is_numeric($v)) {
				imageline($im, $xp, $yp, $x, $y, $colour);
			}
		}
		$xp = $x;
		if (is_numeric($v)) {
			$yp = $y;
		}
	}
	imagefilter($im, IMG_FILTER_SMOOTH, 15);
}

imagepng($im);
?>
