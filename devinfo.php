<!DOCTYPE html>
<html>
<head>
<title>Developer Information for Mirror Island</title>
<style type="text/css">
body { font:normal 16px Arial,Helvetica,sans-serif; font-weight:semi-bold; color:#567; background:#f0f0f3; padding:0px; margin:0px; line-height:24px; }
h1, h2, h3 { font-weight:bold; font-face:'sans-serif'; color:#345; }
h1 { font-size:18px; text-align:center; text-transform:uppercase; }
h2, h3 { font-size:16px; text-align:left; }
h2 { font-weight:bold; text-transform:uppercase; }
h3 { letter-spacing:1px; }
p { margin:16px 0 0 0; }
.maindiv { background:#fafafe; max-width:600px; padding:15px 25px; margin:10px auto 10px auto; border:solid 1px #dde; border-radius:8px; }
#footer { color:#79a; padding:10px 20px; text-align:center; margin:30px 0 0 0; border-top:solid 1px #cde; }
#footer div { margin:10px; }
#footer a { background:none; color:#57f; text-decoration:none; margin:auto 4px; }
#footer a:hover { text-decoration:underline; }
</style>
</head>

<body>

<div class="maindiv">

<center>
<h1>Developer Information</h1>
<a href="/"><img src="/milogo.png"></a>
<p>May 7, 2019</p>
</center>

<p>If you are interested in obtaining the source code, the official git repository is publicly available at:</p>

<p><a href="https://gitlab.com/samjanis/mirrorisland/">https://gitlab.com/samjanis/mirrorisland</a></p>

<div id="footer">
	<div>
		<a href="/"><strong>Mirror&nbsp;Island</strong></a>
		<a href="/about.php">About</a>
		<a href="/contact.php">Contact Us</a>
		<a href="/terms.php">Terms of Use</a>
		<a href="/manifesto.php">Manifesto</a>
		<a href="/quickstart.php">Quickstart Guide</a>
		<a href="/features.php">Feature Tour</a>
		<a href="/advertise.php">Advertise</a>
		<a href="/reportabug.php">Report a Bug</a>
	</div>
</div>

</div>

</body>
</html>
