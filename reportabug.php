<!DOCTYPE html>
<html>
<head>
<title>Reporting A Bug on Mirror Island</title>
<style type="text/css">
body { font:normal 16px Arial,Helvetica,sans-serif; font-weight:semi-bold; color:#567; background:#f0f0f3; padding:0px; margin:0px; line-height:24px; }
h1, h2, h3 { font-weight:bold; font-face:'sans-serif'; color:#345; }
h1 { font-size:18px; text-align:center; text-transform:uppercase; }
h2, h3 { font-size:16px; text-align:left; }
h2 { font-weight:bold; text-transform:uppercase; }
h3 { letter-spacing:1px; }
p { margin:16px 0 0 0; }
li { margin-bottom:1em; }
.maindiv { background:#fafafe; max-width:600px; padding:15px 25px; margin:10px auto 10px auto; border:solid 1px #dde; border-radius:8px; }
#footer { color:#79a; padding:10px 20px; text-align:center; margin:30px 0 0 0; border-top:solid 1px #cde; }
#footer div { margin:10px; }
#footer a { background:none; color:#57f; text-decoration:none; margin:auto 4px; }
#footer a:hover { text-decoration:underline; }
</style>
</head>

<body>

<div class="maindiv">

<center>
<h1>Report A Bug</h1>
<a href="/"><img src="/milogo.png"></a>
<p>May 7, 2019</p>
</center>

<p><em>This page list suggestions on how to report a bug. While they improve the response time in finding and fixing errors with this project, reporting bugs in any way you can is greatly appreciated.</em></p>

<p><em>There are times when using few words to describe what didn't work is just as effective as writing a detailed report.</em></p>

<center>
<p>Send all bug reports to:<br><strong>samjanis@mirrorisland.com</strong></p>
</center>

<p>Bug reporting is an important part of building and improving on this project. Mirror Island aims to be a community driven project that listens as much as possible towards its users.</p>

<p>First of all, bugs can come in many forms. Even if there isn't an actual software bug found but rather a user wasn't sure how to do something, that <em>is</em> important for us to know.</p>

<p>Secondly, bug reports don't have to be formal as long as they are informative. The quicker we can read bug reports the quicker we can fix any errors.</p>

<p>Since development on this website and related projects is done in a collaborative way, here are a few tips that can speed things up:</p>

<ul>
<li>Do not include any personally identifying information that is not public information. A personal page such as a blog or gallery that can be accessed without needing any special priveleges is fine, however as it is publicly available anyway.</li>
<li>Be specific. Instead of listing multiple bugs in one report, write short but specific reports for each one. (For example, images disappearing during editing, and not being able to save are two different problems.)</li>
<li>Avoid including links to copies of error messages, screenshots, or file attachments on other hosting services. Instead, include all of this information in an email since the developers will have all information needed in a single message without having to navigate to other locations on the internet for more information.</li>
<li>If the problem is with the website, try to repeat the bug before and after a browser refresh. If you notice a difference include it with the report.</li>
<li>Some bugs may take longer or need a higher priority than others to be fixed, meaning a recently reported bug may take a while to fix.</li>
</ul>

<p>When reporting a bug, break it up in 4 parts:</p>

<ol>
<li>Describe what browser software and operating system you are using, and (if possible) the software version would be very helpful. It doesn't have to be specific and a description such as "I'm using Firefox on Android" would be sufficient.</li>
<li>Describe what you thought was incorrect, and what should of happened.</li>
<li>Describe the steps needed to repeat the bug. If you can't repeat the bug, at least describe what you did to the best of your recollection. Also describe how often it happens whether it always happens or only occasionally.</li>
<li>A complete and identical copy of any error messages that may of been displayed. Take screenshots, if possible.</li>
</ol>

<p>Once you have written out your bug report, you can send a copy of it by email to the address located on our <a href="/contact.php">Contact Us</a> page or the email address at the top of this page.</p>

<p>Displayed below is an example bug report describing a fictitious error that occurs when a popup menu is displayed.</p>

<h2>Sample Bug Report</h2>

<pre style="white-space:pre-wrap;word-wrap:break-word;">
I'm using Chromium on Debian, Windows 10, and macOS.

When I click on the menu icon the menu itself appears near the bottom of the screen instead of under the menu icon. This is what happened:

Start a new post and add anything to it. Photos, files, whatever.

Save the post.

Before refreshing the page, hover the mouse on the edit icon. The menu will popup near the bottom of the screen and it disappears before I can click on it.

This always happens no matter what OS I'm using, but only if I don't reload the page. Otherwise it goes away when the page loads again.

I can be contacted at computercat@example.com

Regards,
Felix
</pre>

<div id="footer">
	<div>
		<a href="/"><strong>Mirror&nbsp;Island</strong></a>
		<a href="/about.php">About</a>
		<a href="/contact.php">Contact Us</a>
		<a href="/terms.php">Terms of Use</a>
		<a href="/manifesto.php">Manifesto</a>
		<a href="/quickstart.php">Quickstart Guide</a>
		<a href="/features.php">Feature Tour</a>
		<a href="/advertise.php">Advertise</a>
		<a href="/reportabug.php">Report a Bug</a>
	</div>
</div>

</div>

</body>
</html>
