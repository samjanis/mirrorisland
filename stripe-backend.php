<?php
require('func.php');
require('stripe-php/init.php');

$stripe = new \Stripe\StripeClient($stripesecretapikey); /* set in connectdbinfo.php */

/**
 *
 */
function backend_php_create_customer()
{
	/* Function still in payment.php from \Stripe\Customer::create(...) */
}

/**
 *
 */
function backend_php_create_subscription_update_existing($username, $customerId,
		$subscription, $priceId, $quantity)
{
	global $stripe;

	if (!isset($_POST['password'])) {
		http_response_code(401);
		echo json_encode(['error' => ['message' => 'Incorrect password. Try again.']]);
		exit();
	}
	$password = func_php_cleantext($_POST['password'], 1024);
	$userid = func_php_verifypassword($username, $password);
	if (!$userid) {
		/* Setting error in the JSON object will trigger the
		 * ".catch(error)" feature from the fetch request. */
		http_response_code(401);
		echo json_encode(['error' => ['message' => 'Incorrect password. Try again.']]);
		exit();
	}

	/* Get details to refund the last charge. */
	$query = sprintf('select stripesubscriptionlastchargeid'
			.' from accounts where accounts.id = %d limit 1', /* USERID */
			$userid);
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);
	$chargeid = $r['stripesubscriptionlastchargeid'];

	/* Update current plan, and request partial payment or refund if needed. */
	try {
		$updatedSubscription = $stripe->subscriptions->update($subscription->id, [
			'items' => [
				['id' => $subscription->items->data[0]->id,
				'price' => $priceId, 'quantity' => $quantity]
			],
			'proration_behavior' => 'create_prorations',
			'billing_cycle_anchor' => 'now'
		]);
	} catch (Exception $e) {
		error_log(__FILE__.'-'.__LINE__.' Caught Exception $e = '.$e);
		error_log(__FILE__.'-'.__LINE__.' $e->getMessage()='.$e->getMessage());
		/* Return now. Response given. */
		http_response_code(402);
		/* Setting error in the JSON object will trigger the
		 * ".catch(error)" feature from the fetch request. */
		echo json_encode(['error' => ['message' => $e->getMessage()]]);
		exit();
	}
	$status = $updatedSubscription->status;

	return ['status' => $status];
}

/**
 *
 */
function backend_php_create_subscription()
{
	global $stripe;

	$customerId = func_php_cleantext($_POST['customerId'], 256);
	$priceId = func_php_cleantext($_POST['priceId'], 256);
	$quantity = func_php_inttext($_POST['quantity']);

	/* Check if an existing subscription is already in place */
	$query = sprintf('select * from accounts'
			.' where accounts.stripecustomerid = "%s"' /* CUST-ID */
			.' limit 1;',
			func_php_escape($customerId));
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);
	$subscriptionid = $r['stripesubscriptionid'];
	$userid = $r['id'];
	$username = $r['username'];

	if ($subscriptionid) {
		try {
			$subscription = $stripe->subscriptions->retrieve($subscriptionid);
		} catch (Exception $e) {
			error_log(__FILE__.'-'.__LINE__.' Caught Exception $e = '.$e);
			error_log(__FILE__.'-'.__LINE__.' $e->getMessage()='.$e->getMessage());
			/* Return now. Response given. */
			http_response_code(402);
			/* Setting error in the JSON object will trigger the
			 * ".catch(error)" feature from the fetch request. */
			echo json_encode(['error' => ['message' => $e->getMessage()]]);
			exit();
		}
		if ($subscription->status == 'active') {
			return backend_php_create_subscription_update_existing(
					$username, $customerId, $subscription, $priceId, $quantity);
		}
	}

	/* Create the subscription. */
	$subscription = $stripe->subscriptions->create([
			'customer' => $customerId,
			'items' => [[
				'price' => $priceId,
				'quantity' => $quantity,
			]],
			'payment_behavior' => 'default_incomplete',
			'expand' => ['latest_invoice.payment_intent'],
	]);

	$price = $subscription->latest_invoice->amount_due;
	$interval = $subscription->plan->interval;
	$displayedPrice = sprintf("AU$%01.2f", $price / 100);
	$displayedCharge = "You will be immediately charged {$displayedPrice}.";
	if ($interval === 'month') {
		$displayedCharge .= '<br>This will be done every month.';
	} else if ($interval === 'year') {
		$displayedCharge .= '<br>This will be done every year.';
	}
	$status = $subscription->status;
	return ['status' => $status, 'subscription' => $subscription,
			'price' => $price, 'displayedCharge' => $displayedCharge,
			'subscriptionId' => $subscription->id,
			'clientSecret' => $subscription->latest_invoice->payment_intent->client_secret];
}

/**
 *
 */
function backend_php_retry_invoice()
{
	global $stripe;

	$customerId = func_php_cleantext($_POST['customerId'], 256);
	$paymentMethodId = func_php_cleantext($_POST['paymentMethodId'], 256);
	$invoiceId = func_php_cleantext($_POST['invoiceId'], 256);

	try {
		$payment_method = $stripe->paymentMethods->retrieve($paymentMethodId);
		$payment_method->attach([
			'customer' => $customerId,
		]);
	} catch (Exception $e) {
		error_log(__FILE__.'-'.__LINE__.' Caught Exception $e = '.$e);
		error_log(__FILE__.'-'.__LINE__.' $e->getMessage()='.$e->getMessage());
		/* Return now. Response given. */
		http_response_code(402);
		/* Setting error in the JSON object will trigger the
		 * ".catch(error)" feature from the fetch request. */
		echo json_encode(['error' => ['message' => $e->getMessage()]]);
		exit();
	}

	$invoice = $stripe->invoices->retrieve($invoiceId, [
		'expand' => ['payment_intent', 'subscription'],
	]);

	$status = $invoice->payment_intent->status;
	if ($status != 'succeeded') {
		$clientsecret = $invoice->payment_intent->client_secret;
		$latestinvoiceid = $invoice->id;
		$paymentintentstatus = $invoice->payment_intent->status;
		return ['status' => $status,
				'client_secret' => $clientsecret,
				'latest_invoice_id' => $latestinvoiceid,
				'latest_invoice_payment_intent_status' => $paymentintentstatus];
	}

	return ['status' => $status];
}

/**
 *
 */
function backend_php_retrieve_upcoming_invoice()
{
	global $stripe;

	$username = func_php_cleantext($_POST['username'], 256);
	$query = sprintf('select stripecustomerid, stripesubscriptionid from accounts'
			.' where accounts.username = "%s" limit 1;', /* USERNAME */
			func_php_escape($username));
	$result = func_php_query($query);
	if (mysqli_num_rows($result) != 1) {
		http_response_code(402);
		/* Setting error in the JSON object will trigger the
		 * ".catch(error)" feature from the fetch request. */
		echo json_encode(['error' => ['message' => 'invalid username']]);
		exit();
	}

	$r = mysqli_fetch_assoc($result);
	$customerId = $r['stripecustomerid'];
	$subscriptionId = $r['stripesubscriptionid'];
	if (!$subscriptionId) {
		return ['status' => 'success', 'newPrice' => 'none'];
	}

	try {
		$subscription = $stripe->subscriptions->retrieve($subscriptionId);
	} catch (Exception $e) {
		error_log(__FILE__.'-'.__LINE__.' Caught Exception $e = '.$e);
		error_log(__FILE__.'-'.__LINE__.' $e->getMessage()='.$e->getMessage());
		/* Return now. Response given. */
		http_response_code(402);
		/* Setting error in the JSON object will trigger the
		 * ".catch(error)" feature from the fetch request. */
		echo json_encode(['error' => ['message' => $e->getMessage()]]);
		exit();
	}

	$newPriceId = func_php_cleantext($_POST['newPriceId'], 256);
	$newQuantity = func_php_inttext($_POST['newQuantity']);
	$invoice = $stripe->invoices->upcoming([
		'customer' => $customerId,
		'subscription_proration_behavior' => 'create_prorations',
		'subscription_billing_cycle_anchor' => 'now',
		'subscription' => $subscriptionId,
		'subscription_items' => [
			[
				'id' => $subscription->items->data[0]->id,
				'price' => $newPriceId,
				'quantity' => $newQuantity,
			]
		]
	]);

	$newPrice = 'You will not be charged for this plan change.';
	$total = $invoice->total;
	$totalText = sprintf("AU$%01.2f", abs($total / 100));
	if ($total > 0) {
		$newPrice = "You will be charged a difference of {$totalText}.";
	} else if ($total < 0) {
		$newPrice = "You will be refunded the difference of {$totalText}.";
	}
	$newPrice .= "<br>This amount might vary, depending on when you change plans.";

	return ['status' => 'success', 'newPrice' => $newPrice, 'invoice-data' => $invoice];
}

/**
 *
 */
function backend_php_cancel_subscription()
{
	global $stripe;

	if (!isset($_POST['password'])) {
		/* Setting error in the JSON object will trigger the
		 * ".catch(error)" feature from the fetch request. */
		http_response_code(401);
		echo json_encode(['error' => ['message' => 'Invalid password. Try again.']]);
		exit();
	}

	$username = func_php_cleantext($_POST['username'], 1024);
	$password = func_php_cleantext($_POST['password'], 1024);
	$userid = func_php_verifypassword($username, $password);
	if (!$userid) {
		/* Setting error in the JSON object will trigger the
		 * ".catch(error)" feature from the fetch request. */
		http_response_code(401);
		echo json_encode(['error' => ['message' => 'Incorrect password. Try again.']]);
		exit();
	}

	/* Get details to refund the last charge and cancel subscription. */
	$query = sprintf('select stripesubscriptionid,'
			.' stripepaymentmethodid, stripesubscriptionlastchargeid'
			.' from accounts where accounts.id = %d limit 1', /* USERID */
			$userid);
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);
	$subscriptionid = $r['stripesubscriptionid'];
	$paymentMethodId = $r['stripepaymentmethodid'];
	$chargeid = $r['stripesubscriptionlastchargeid'];

	if ($chargeid) {
		$lastCharge = $stripe->charges->retrieve($chargeid, []);
		if ($lastCharge->refunded === false) {
			/* Refund the last charge */
			try {
				$refund = $stripe->refunds->create([
					'charge' => $chargeid
				]);
			} catch (Exception $e) {
				error_log('backend.php:'.__LINE__.' Caught Exception $e = '.$e);
				error_log('backend.php:'.__LINE__.' $e->getMessage()='.$e->getMessage());
				/* Return now. Response given. */
				http_response_code(402);
				/* Setting error in the JSON object will trigger the
				 * ".catch(error)" feature from the fetch request. */
				echo json_encode(['error' => ['message' => $e->getMessage()]]);
				exit();
			}
		}
	}

	/* Remove paymentmethod, if any. */
	if ($paymentMethodId) {
		$pmdata = $stripe->paymentMethods->retrieve($paymentMethodId);
		if ($pmdata->customer) {
			$pm = $stripe->paymentMethods->detach($paymentMethodId);
		}
	}

	/* Cancel the subscription */
	$subscription = $stripe->subscriptions->retrieve($subscriptionid);
	try {
		$subscription->delete();
	} catch (Exception $e) {
		error_log(__LINE__.' Caught Exception $e = '.$e);
		error_log(__LINE__.' $e->getMessage()='.$e->getMessage());
		/* Return now. Response given. */
		http_response_code(402);
		/* Setting error in the JSON object will trigger the
		 * ".catch(error)" feature from the fetch request. */
		echo json_encode(['error' => ['message' => $e->getMessage()]]);
		exit();
	}
	$status = $subscription->status;

	return ['status' => $status];
}

$backend_array = array();
$command = (isset($_POST['command']) ? func_php_cleantext($_POST['command'], 64) : '');
switch ($command) {
case 'create-customer':
	$backend_array = backend_php_create_customer();
	break;
case 'create-subscription':
	$backend_array = backend_php_create_subscription();
	break;
case 'retry-invoice':
	$backend_array = backend_php_retry_invoice();
	break;
case 'get-invoice':
case 'retrieve-upcoming-invoice':
	$backend_array = backend_php_retrieve_upcoming_invoice();
	break;
case 'cancel-subscription':
	$backend_array = backend_php_cancel_subscription();
	break;
default:
	$backend_array['error'] = "No function";
	$backend_array['command'] = $command;
}

echo json_encode($backend_array, JSON_PRETTY_PRINT);
?>
