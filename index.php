<?php
/**
 * Mirror Island - A small and efficient content delivery/distribution system (CDS).
 * Copyright (C) 2018, 2019, 2020, 2021, 2022
 * Sam and Janis Allen
 * samjanis@mirrorisland.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$siteoperators = ''; /* If set, these users can not be flagged. */
$helppage = false; /* If set, shows 'Help' link at top of the page */

require('func.php');

/**
 * Change an integer in to an alphanumeric representation, similar to base64.
 */
function index_php_alphanumericid($int)
{
	$charset = 'abcdefghijklmnopqrstuvwqyzABCDEFGHIJKLMNOPQRSTUVWQYZ1234567890-_';
	$string = '';
	while ($int > 0) {
		$remainder = $int % 64;
		$int = ($int - $remainder) / 64;
		$string = $charset[$remainder].$string;
	}
	return $string;
}

/**
 *
 */
function index_php_dashifempty($string)
{
	return ($string ? $string : '-');
}

/**
 * Restrict a numeric value between min and max values.
 */
function index_php_minmax($number, $min, $max)
{
	if ($number < $min)
		return $min;

	if ($number > $max)
		return $max;

	return $number;
}

function index_php_getflagreasondescription($postflagreason)
{
	switch ($postflagreason) {
	case 'MATURE_CONTENT':
		$reason = 'Possible mature content';
		break;
	case 'ADULT_CONTENT':
		$reason = 'Possible adult content';
		break;
	case 'COPYRIGHT_OR_TRADEMARK':
		$reason = 'Possible copyright or trademark infringement';
		break;
	case 'ILLEGAL_OR_VIOLATION':
		$reason = 'Possible violation of terms of service';
		break;
	default:
		$reason = "Other ($postflagreason)";
	}

	return $reason;
}

/**
 * Compares $username with $password.
 * Returns account.id as a result if successfull, otherwise 0 on fail.
 */
function index_php_verifypassword($username, $password)
{
	global $mysqli;

	/* First fetch the password - it contains the salt! */
	$query = sprintf('select accounts.id, passwords.password'
			.' from accounts inner join passwords on accounts.id = passwords.accountid'
			.' where accounts.username = "%s" limit 1;', func_php_escape($username));
	$result = func_php_query($query);
	if (mysqli_num_rows($result) != 1) {
		return false;
	}

	/* Now, salt the provided password and compare. */
	$r = mysqli_fetch_assoc($result);
	$savedpassword = $r['password'];
	$passwordhash = crypt($password, $savedpassword);
	if ($passwordhash == $savedpassword) {
		return $r['id'];
	}

	return false;
}

function index_php_recipientstringtoidarray($recipients)
{
	if (!$recipients)
		return '';

	/* Escape string now, otherwise comma-separate list doesn't work. */
	$recipients = func_php_escape(preg_replace('/\s+/', ' ', $recipients));
	$recipients = explode(' ', $recipients);
	$recipients = array_unique($recipients);
	$recipientlist = "'".implode("', '", $recipients)."'";
	$query = sprintf('select distinct accounts.id from accounts where accounts.username in (%s)',
			$recipientlist); /* String escaped in above code. */
	$result = func_php_query($query);
	while ($r = mysqli_fetch_assoc($result)) {
		$recipientidarray[] = $r['id'];
	}

	return $recipientidarray;
}

/* Constants */
define('mibibyte', 1024*1024); /* Mibibyte */

/* Global PHP variables (phpglobal) */
$dbg = array();
session_start();
$_SESSION['start'] = 'started';
$index_showprofile = false;
$index_newpostlabel = false;
$index_newpostrecipients = '';
$index_showposts = true;
$index_showanalytics = false;
$index_showsettings = false;
$index_showflaggedposts = false; /* Can be over-ridden by moderators */
$index_errorlist = array();
$index_infolist = array();
$index_activeuser = [];
$index_activeusername = '';
$index_activeuserid = 0;
$index_activesessionid = '';
$index_activefullname = '';
$index_activeprofile = null;
$index_activeuserconfirmedage = 3;
$index_activeuserfilespace = 0; /* 104857600: 1024*1024*100 bytes, Default available space = 100MiB */
$index_activeuserfilesize = 0; /* 10485760: 1024*1024*10 bytes, Default maxiumum single file size = 10MiB */
$index_activeuserpostsize = 0; /* 10485760: 1024*1024*10 bytes, Default maxiumum combined post size = 10MiB */
$index_activeuserfileuse = 0; /* Total file use */
/* $servername and $urlhome are set in connectdbinfo.php ...
 * - They need to be dynamically set with auto detection. */
$index_page = '';
$index_page_id = 0;
$index_page_userpermissions = false;
$index_newpostparentid = 0;
$index_newpostpermissions = '000000';
$index_title = '';
$index_emailusername = ''; /* Used to list emails. */
$index_emailserver = ''; /* Used to list emails. */
$index_viewmode = '';
$index_sortmode = '';
$index_sortreverse = false;
$index_passwordresetid = '';

$index_page_offset = 0; /* Starting post; offset param in select limit. */
$index_page_count = 20; /* Number of replies/posts shown. This will soon be user set */
$index_page_number = 1;

$index_post_btn = (isset($_POST['btn']) ? func_php_cleantext($_POST['btn'], 64) : '');
$index_get_page = (isset($_GET['page']) ? func_php_cleantext($_GET['page'], 1024) : '');
$index_get_option = (isset($_GET['option']) ? func_php_cleantext($_GET['option'], 255) : '');

$index_subdomain = '';
if (preg_match('/^([^.]*)\.'.$servername.'/', $_SERVER['HTTP_HOST'], $matches)) {
	$index_subdomain = $matches[1];
	$urlhome = 'http://'.$_SERVER['HTTP_HOST'];
	if (!$index_get_page) {
		$index_get_page = $index_subdomain;
	}
}

/* Check and set custom domain here */
$httphost = func_php_cleantext($_SERVER['HTTP_HOST'], 1024);
if (strpos($httphost, $servername) === false) {
	$query = sprintf('select posts.id, posts.domainname from posts, accounts'
			.' where posts.domainname = "%s"' /* HTTPHOST */
			.' and accounts.id = posts.accountid and accounts.accounttype is not NULL'
			.' limit 1',
			func_php_escape($httphost));
	$result = func_php_query($query);
	if (mysqli_num_rows($result)) {
		$r = mysqli_fetch_assoc($result);
		$servername = $r['domainname'];
		$urlhome = 'http://'.$servername;
		if (!$index_get_page) {
			$index_get_page = $r['id'];
		}
	} else {
		$index_get_page = 'INVALID';
	}
}

$index_querystring = '';
$index_analyticspageid = '';
$index_analyticsphpsessionid = '';

$index_get_viewas = (isset($_GET['viewas']) ? func_php_cleantext($_GET['viewas'], 255) : '');
switch ($index_get_viewas) {
case 'full':
case 'thumbnail':
case 'list':
	/* Keep these values */
	$index_viewmode = $index_get_viewas;
	$index_querystring .= ($index_querystring ? '&' : '?');
	$index_querystring .= "viewas={$index_viewmode}";
	break;
default:
	$index_get_viewas = '';
	$index_viewmode = 'full';
}

$index_get_sort = func_php_cleantext($_GET['sort'] ?? '', 255);
switch ($index_get_sort) {
case 'date':
case 'title':
	/* Keep these values */
	$index_sortmode = $index_get_sort;
	$index_querystring .= ($index_querystring ? '&' : '?');
	$index_querystring .= "sortmode={$index_sortmode}";
	break;
default:
	$index_get_sort = '';
	$index_sortmode = 'date';
}

$index_get_order = func_php_cleantext($_GET['order'] ?? '', 64);
$index_querystring .= ($index_querystring ? '&' : '?');
if ($index_get_order === 'reverse') {
	$index_sortreverse = true;
	$index_querystring .= ($index_querystring ? '&' : '?').'order=reverse';
} else {
	$index_sortreverse = false;
}

/* Check for password reset links */
if (isset($_GET['reset'])) {
	$resetlinkid = func_php_cleantext($_GET['reset'], 255);
	$query = sprintf('select id, username from accounts'
			.' where resetlinkid="%s" and now() < date_add(resetlinkadded, interval 1 hour)'
			.' limit 1',
			func_php_escape($resetlinkid));
	$result = func_php_query($query);
	if (mysqli_num_rows($result)) {
		$r = mysqli_fetch_assoc($result);
		$userid = $r['id'];
		$username = $r['username'];

		if (!$index_get_page) {
			/* Sign in if no username given */
			$sessionid = '';
			for ($i = 0; $i < 64; $i++) {
				$sessionid .= substr('1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
					mt_rand(0, 61), 1);
			}
			$query = sprintf('update accounts set sessionid="%s" where id=%d limit 1;',
					func_php_escape($sessionid), $userid);
			$result = func_php_query($query);

			$cookietimelength = time()+(60*60*24*90); /* 90 days. */
			setcookie('activesessionid', $sessionid, $cookietimelength, '/');
			setcookie('activeusername', $username, $cookietimelength, '/');
			setcookie('activeuserid', $userid, $cookietimelength, '/');
			header("Location: {$urlhome}/{$username}/password/?reset={$resetlinkid}");
			exit();
		} else if ($index_get_page === $username) {
			/* Otherwise, save reset id for password form */
			$index_passwordresetid = $resetlinkid;
		}
	} else {
		$index_page = 'invalidresetlink';
		$index_showposts = false;
	}
}

/* Check for a currently signed in user. */
if (isset($_COOKIE['activeusername']) && isset($_COOKIE['activesessionid'])) {
	$activesessionid = func_php_cleantext($_COOKIE['activesessionid'], 64);
	$activeusername = func_php_cleantext($_COOKIE['activeusername'], 255);
	$index_activesessionid = '';
	$index_activeusername = '';
	$index_activeuserid = 0;
	$query = sprintf('select * from accounts where username="%s" and sessionid="%s" limit 1',
			func_php_escape($activeusername),
			func_php_escape($activesessionid));
	$result = func_php_query($query);
	if (mysqli_num_rows($result) != 1) {
		/* No valid member? Abort! */
		setcookie('activesessionid', $activesessionid, 1);
		setcookie('activeusername', $activeusername, 1);
		$index_activesessionid = '';
		$index_activeusername = '';
		$index_activeuserid = 0;
		$activeusername = '';
		$activesessionid = '';
	} else {
		$index_activeuser = mysqli_fetch_assoc($result);
		$index_activeuser['termsofuse'] = ''; /* Takes up too much space */
		$index_activeuserid = $index_activeuser['id'];
		$index_activeusername = $index_activeuser['username'];
		$index_activefullname = $index_activeuser['fullname'];
		$index_activesessionid = $index_activeuser['sessionid'];
		$index_activeuserconfirmedage = $index_activeuser['confirmedage'];
		if (!$index_activeuserconfirmedage)
			$index_activeuserconfirmedage = 3;
		/* Refresh the cookies so they don't time out. */
		$cookietimelength = time()+(60*60*24*90); /* 90 days. */
		setcookie('activesessionid', $index_activesessionid, $cookietimelength, '/');
		setcookie('activeusername', $index_activeusername, $cookietimelength, '/');
	}
}

/* Set storage space and usage. Use defaults if no specific size set. */
if ($index_activeuserid) {
	if ($index_activeuser['filespace']) {
		$index_activeuserfilespace = $index_activeuser['filespace'];
	} else {
		$index_activeuserfilespace = mibibyte * 100;
	}

	if ($index_activeuser['filesize']) {
		$index_activeuserfilesize = $index_activeuser['filesize'];
	} else {
		$index_activeuserfilesize = mibibyte * 10;
	}

	if ($index_activeuser['postsize']) {
		$index_activeuserpostsize = $index_activeuser['postsize'];
	} else {
		$index_activeuserpostsize = mibibyte * 10;
	}

	$query = sprintf('select IFNULL(SUM(filesize), 0) as fileuse from files'
			.' where files.accountid = %d;', $index_activeuserid);
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);
	$index_activeuserfileuse = $r['fileuse'];
}

/* Functions for use with Thoth and compatible clients. */
$pageopt = '';
if (isset($_POST['pageopt'])) {
	$pageopt = func_php_cleantext($_POST['pageopt'], 255);
}
if (($pageopt == 'md') || ($pageopt == 'save')) {
	$userid = 0;
	$username = '';
	if (isset($_POST['username']) && isset($_POST['password'])) {
		$username = func_php_cleantext($_POST['username'], 255);
		$password = func_php_cleantext($_POST['password'], 255);
		$userid = index_php_verifypassword($username, $password);
		if ($userid) {
			$index_activeuserid = $userid;
			$index_activeusername = $username;
		}
	}
	if ($pageopt == 'save') {
		$id = 0;
		if (preg_match('/^[0-9]+$/', $index_get_page) == 1) {
			/* First check if post exists and the user has permission to edit. */
			$permissions = func_php_getUserPermissions($index_get_page, $index_activeuserid);
			if (!$permissions['canedit']) {
				echo "Can't edit!\n";
				exit(); /* Can't edit this post! */
			}
			echo "Going to edit!\n";
			$id = $index_get_page;
		} else if (!$index_get_page) {
			/* Create a new post for the uploaded note */
			$query = sprintf('/* '.__LINE__.' */ insert into posts (parentid, accountid, added, viewcount,'
					.' viewpermissions, editpermissions, replypermissions, approvepermissions,'
					.' moderatepermissions, tagpermissions, agerating)'
					.' values (0, %d, NOW(), 0, 0, 0, 0, 0, 0, 0, 3)', $userid); /* USERID */
			$result = func_php_query($query);
			$id = mysqli_insert_id($mysqli);
		} else {
			echo "Invalid Thoth action!\n";
			exit();
		}

		/* Update the description and searchcache for the new or existing post. */
		if ($id) {
			$description = func_php_cleantext($_POST['description'], 4000000);

			/* Some characters need to be escaped */
			$description = func_php_plaintexttohtml($description);

			$searchcache = func_php_stringtosearchcache($description);
			$query = sprintf('/* '.__LINE__.' */ update posts set updated=NOW(),'
					.' description="%s", searchcache="%s"' /* DESCRIPTION, SEARCHCACHE */
					.' where posts.id = %d limit 1;', /* POSTID */
					func_php_escape($description), func_php_escape($searchcache), $id);
			$result = func_php_query($query);

			$data = array('f' => 'indexpostforsearch', 'id' => $id, 'deleteflag' => 'true');
			func_php_sendbgfunc($data);
			$windowlocationhref = $urlhome.'/'.$id;
			header('Location: '.$windowlocationhref);
		}
		exit();
	}
	if ($pageopt == 'save') {
		exit(); /* No need to output anything else. */
	}
	$index_get_option = $pageopt;
}

/**
 * Handle authenticated file downloads.
 */
$index_fileid = '';
if (isset($_GET['fileid']))
	$index_fileid = func_php_inttext($_GET['fileid']);

$index_filename = '';
if (isset($_GET['filename']))
	$index_filename = func_php_cleantext($_GET['filename'], 65535);

if (($index_fileid) && ($index_filename)) {
	$query = sprintf('/* '.__LINE__.' */ select * from files'
			.' where id=%d and filename="%s" limit 1;', /* FILEID, FILENAME */
			$index_fileid, func_php_escape($index_filename));
	$result = func_php_query($query);

	if (mysqli_num_rows($result) == 1) {
		$r = mysqli_fetch_assoc($result);
		$filemime = $r['filemime'];
		$filename = $r['filename'];
		$fileindex = $r['fileindex'];
		$s3profile = $r['s3profile'];
		$s3endpoint = $r['s3endpoint'];
		$s3region = $r['s3region'];
		$s3bucket = $r['s3bucket'];

		$accountid = $r['accountid'];
		$postid = $r['postid'];
		if ($postid) {
			$permissions = func_php_getUserPermissions($postid,
					$index_activeuserid);
			if (!$permissions || !$permissions['canview']) {
				exit(); /* Can't access file. */
			}
		} else if ($accountid != $index_activeuserid) {
			/* If not owner of image, check if profile image
			 * and able to access */
			$query = sprintf('select viewpermissions from accounts'
					.' where profileimagefileid=%d limit 1', $index_fileid);
			$result = func_php_query($query);
			if (mysqli_num_rows($result) != 1) {
				exit();
			}
			$r = mysqli_fetch_assoc($result);
			$viewpermissions = $r['viewpermissions'];
			if ($viewpermissions != 5) {
				exit();
			}
		}

		/* Defaults set here - can change below. */
		$contentdisposition = 'attachment'; /* Force "Save As..." dialogue for downloading. */

		if (($filemime == 'image/gif')|| ($filemime == 'image/jpeg')
				|| ($filemime == 'image/png')) {
			$contentdisposition = 'inline'; /* Display file in browser. */
		} else {
			$filemime = 'application/octet-stream';
		}

		header("Content-Disposition: $contentdisposition; filename=\"{$filename}\"");
		header("Content-Type: $filemime");

		/* Check if using an S3 object source for file data */
		if ($s3profile && $s3endpoint && $s3region && $s3bucket) {
			require 'aws/aws-autoloader.php';

			$sdk = new Aws\Sdk([
				'profile' => $r['s3profile'],
				'endpoint' => $r['s3endpoint'],
				'region' => $r['s3region'],
				'version' => 'latest',
				'use_path_style_endpoint' => true,
			]);
			$s3 = $sdk->createS3();
			$s3->registerStreamWrapper();

			$s3uri = 's3://'.$r['s3bucket'].'/'.$fileindex;

			if ($stream = fopen($s3uri, 'r')) {
				while (!feof($stream)) {
					echo fread($stream, 8192);
				}
				fclose($stream);
			}
			exit();
		}

		$ss = $_SERVER['SERVER_SOFTWARE'];
		if (preg_match('/LiteSpeed/i', $ss)) {
			header("X-LiteSpeed-Location: /files/{$fileindex}");
			exit();
		}

		/* Assume Apache */
		header("X-Sendfile: files/{$fileindex}");
		exit();
	}
	exit();
}

/**
 * Returns bytes used by the current active user for a specific post
 * or for all files they own, depending on what values are given.
 * - If $postid is specified, the sum file size for that post is returned.
 * - If $newpostsessionid is specified, the sum file size for that is returned.
 * - If both are FALSE (or 0) then the sum of all files the current user has
 *   is returned.
 */
function index_php_getuserfileuse($postid, $newpostsessionid)
{
	global $index_activeuserid;

	if (!$index_activeuserid) {
		return 0;
	}
	$query = sprintf('select IFNULL(SUM(filesize), 0) as fileuse from files'
			.' where files.accountid = %d', $index_activeuserid);
	if ($postid) {
		$query .= sprintf(' and postid = %d;', $postid);
	} else if ($newpostsessionid) {
		$query .= sprintf(' and newpostsessionid = %d;', $newpostsessionid);
	}
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);
	return $r['fileuse'];
}

/**
 * Add (or update) a profile's search index entries.
 * $id: table entry from accounts.
 * $removekeys: Remove keywords from the search cache.
 *		Setting this to false can speed things up.
 * TODO:OPTIMISE - This has duplicate code shared wth other index_php_index* functions.
 */
function index_php_indexprofileforsearch($id, $removekeywords)
{
	global $mysqli;

	$query = sprintf('select concat(accounts.username, " ", accounts.fullname) as description'
			.' from accounts where accounts.id=%d limit 1;', $id);
	$result = func_php_query($query);
	if (!mysqli_num_rows($result))
		return; /* No valid account, move along people... */

	$r = mysqli_fetch_assoc($result);
	$description = $r['description'];
	$keywordarray = false;
	$keywordlist = '';

	if ($description) {
		/* Generate a stripped-down copy of description for searchcache. */
		$searchcache = func_php_stringtosearchcache(func_php_markdowntohtml($description));

		/* Tokenize and remove some stop words. */
		$keywordarray = func_php_stringtokeywordarray($searchcache, true);

		/* Generate a comma-separated single-quoted string of keywords for use in sql below. */
		foreach ($keywordarray as $keyword) {
			if ($keywordlist)
				$keywordlist .= ', ';
			$keywordlist .= '"'.func_php_escape($keyword).'"';
		}
	}

	if ($removekeywords) {
		/* Remove any keywords not use by any other searchindex entry. */
		$query = sprintf('delete from searchkeywords where id in (select keywordid from'
				.' (select searchindex.*, searchkeywords.keyword, count(*) as keywordcount'
				.' from searchindex'
				.' inner join searchkeywords on searchindex.keywordid = searchkeywords.id'
				.' left join searchindex as si on si.keywordid = searchindex.keywordid'
				.' where searchindex.tablename="accounts" and searchindex.tableid=%d' /* ACCTID */
				.' group by keywordid) as searchkeywordstodelete'
				.' where keywordcount <= 1', $id);

		if ($keywordlist) {
			$query .= sprintf(' and keyword not in (%s)', $keywordlist);
		}

		$query .= ');';
		func_php_query($query);

		/* Remove searchindex entries not in keyword list. */
		$query = '/* '.__LINE__.' */ '.sprintf('delete from searchindex where id in'
				.'(select id from (select searchindex.id from searchindex'
				.' inner join searchkeywords on searchindex.keywordid = searchkeywords.id'
				.' where searchindex.tablename="accounts" and tableid=%d', $id);

		if ($keywordlist) {
			$query .= sprintf(' and keyword not in (%s)', $keywordlist);
		}

		$query .= ') as searchindexentriestodelete);';
		func_php_query($query);
	}

	if (!$keywordarray)
		return; /* Nothing new to insert, leave now. */

	/* Only add keywords that don't exist yet. TODO:OPTIMISE */
	foreach ($keywordarray as $keyword) {
		$query = sprintf('select * from searchkeywords where keyword="%s" limit 1;',
				func_php_escape($keyword));
		$result = func_php_query($query);

		/* Fetch an existing keyword id or save a new one. */
		$keywordid = 0;
		if (mysqli_num_rows($result) == 1) {
			$r = mysqli_fetch_assoc($result);
			$keywordid = $r['id'];
		} else {
			$query = sprintf('insert into searchkeywords (keyword) values ("%s");',
					func_php_escape($keyword));
			$result = func_php_query($query);
			$keywordid = mysqli_insert_id($mysqli);
		}

		/* See if an existing searchindex entry exists for accounts. */
		$query = sprintf('insert into searchindex (tablename, tableid, keywordid)'
				.' select * from (select "accounts", %d as tid, %d as kwid) as newentry' /* ACCTID, KEYWORDID */
				.' where not exists (select tablename, tableid, keywordid from searchindex'
				.' where tablename="accounts" and tableid=%d and keywordid=%d limit 1)' /* ACCTID, KEYWORDID */
				.' limit 1;', $id, $keywordid, $id, $keywordid);
		func_php_query($query);
	}

	return;
}

/**
 * TODO:OPTIMISE - This has duplicate cade hared wth other index_php_index* functions.
 */
function index_php_indexfileforsearch($id, $removekeywords)
{
	global $mysqli;

	$query = sprintf('select files.filename as description'
			.' from files where files.id=%d limit 1;', $id);
	$result = func_php_query($query);
	if (mysqli_num_rows($result) == 0)
		return; /* No valid file, move along people... */

	$r = mysqli_fetch_assoc($result);
	$description = $r['description'];
	$keywordarray = false;
	$keywordlist = '';

	if ($description) {
		/* Generate a stripped-down copy of description for searchcache. */
		$searchcache = func_php_stringtosearchcache(func_php_markdowntohtml($description));

		/* Tokenize and remove some stop words. */
		$keywordarray = func_php_stringtokeywordarray($searchcache, true);

		/* Generate a comma-separated single-quoted string of keywords for use in sql below. */
		foreach ($keywordarray as $keyword) {
			if ($keywordlist)
				$keywordlist .= ', ';
			$keywordlist .= '"'.func_php_escape($keyword).'"';
		}
	}

	if ($removekeywords) {
		/* Remove any keywords not use by any other searchindex entry. */
		$query = sprintf('delete from searchkeywords where id in (select keywordid from'
				.' (select searchindex.*, searchkeywords.keyword, count(*) as keywordcount'
				.' from searchindex'
				.' inner join searchkeywords on searchindex.keywordid = searchkeywords.id'
				.' left join searchindex as si on si.keywordid = searchindex.keywordid'
				.' where searchindex.tablename="files" and searchindex.tableid=%d' /* ACCTID */
				.' group by keywordid) as searchkeywordstodelete'
				.' where keywordcount <= 1', $id);

		if ($keywordlist) {
			$query .= sprintf(' and keyword not in (%s)', $keywordlist);
		}

		$query .= ');';
		func_php_query($query);

		/* Remove searchindex entries not in keyword list. */
		$query = '/* '.__LINE__.' */ '.sprintf('delete from searchindex where id in'
				.'(select id from (select searchindex.id from searchindex'
				.' inner join searchkeywords on searchindex.keywordid = searchkeywords.id'
				.' where searchindex.tablename="files" and tableid=%d', $id);

		if ($keywordlist) {
			$query .= sprintf(' and keyword not in (%s)', $keywordlist);
		}

		$query .= ') as searchindexentriestodelete);';
		func_php_query($query);
	}

	if (!$keywordarray)
		return; /* Nothing new to insert, leave now. */

	/* Only add keywords that don't exist yet. TODO:OPTIMISE */
	foreach ($keywordarray as $keyword) {
		$query = sprintf('select * from searchkeywords where keyword="%s" limit 1;',
				func_php_escape($keyword));
		$result = func_php_query($query);

		/* Fetch an existing keyword id or save a new one. */
		$keywordid = 0;
		if (mysqli_num_rows($result) == 1) {
			$r = mysqli_fetch_assoc($result);
			$keywordid = $r['id'];
		} else {
			$query = sprintf('insert into searchkeywords (keyword) values ("%s");',
					func_php_escape($keyword));
			$result = func_php_query($query);
			$keywordid = mysqli_insert_id($mysqli);
		}

		/* See if an existing searchindex entry exists for files. */
		$query = sprintf('insert into searchindex (tablename, tableid, keywordid)'
				.' select * from (select "files", %d as tid, %d as kwid) as newentry' /* FILEID, KEYWORDID */
				.' where not exists (select tablename, tableid, keywordid from searchindex'
				.' where tablename="files" and tableid=%d and keywordid=%d limit 1)' /* FILEID, KEYWORDID */
				.' limit 1;', $id, $keywordid, $id, $keywordid);
		func_php_query($query);
	}

	return;
}

/**
 * Returns the innerHTML of a post members div.
 */
function index_php_insertPostMembers($phylum, $postid)
{
	global $index_activeuserid;

	$query = sprintf('/* '.__LINE__.' */ select %d as activeuserid, posts.*,' /* USERID */
			.' (select count(*) from members where members.postid = posts.id) as membercount,'
			.' (select count(*) from members where members.postid = posts.id'
			.' 	and members.trusted = 1) as trustedmembercount'
			.' from posts left join accounts on posts.accountid = accounts.id'
			.' where posts.id = %d limit 1;', /* POSTID */
			$index_activeuserid, $postid);
	$result = func_php_query($query);
	if (mysqli_num_rows($result) != 1) {
		return $query; /* ERROR FOUND! What are we meant to do here? */
	}
	$r = mysqli_fetch_assoc($result);

	$accountid = $r['accountid'];
	$membercount = $r['membercount'];
	$trustedmembercount = $r['trustedmembercount'];

	$postmembers = '';
	$jointext = '';
	$membertitle = '';
	if ($trustedmembercount > 0) {
		$membertitle = "Members ($trustedmembercount) ";

		/* Set up group join/cancel/leave link. */
		if (($index_activeuserid > 0) && ($index_activeuserid != $accountid)) {
			$joinrequest = 1;
			$jointext = 'Join Group';
			$query = sprintf('select * from members where accountid = %d and postid = %d limit 1;',
					$index_activeuserid, $postid);
			$joinresult = func_php_query($query);
			if (mysqli_num_rows($joinresult) > 0) {
				$joinrequest = 0;
				$joinresultrow = mysqli_fetch_assoc($joinresult);
				if ($joinresultrow['trusted'] == 1)
					$jointext = 'Leave Group';
				else
					$jointext = 'Cancel Join Request';
			}
		}
	}

	$postmembers .= "<span id=\"{$phylum}membercount_{$postid}\" class=\"fadetext\">$membertitle</span>";
	if ($jointext) {
		$joinconfirm = ($joinrequest ? 0 : 1); /* Confirm if leaving group */
		$postmembers .= "<input type=\"button\" id=\"{$phylum}joingroupbutton_$postid\""
				." class=\"silverbutton postmembersbutton\" value=\"$jointext\" onClick=\"index_js_joinGroup('$phylum', $postid, $index_activeuserid, $joinrequest, $joinconfirm);\">";
	}
	$pendingtext = '';
	if (($index_activeuserid == $accountid) && $membercount) {
		$pendingcount = $membercount - $trustedmembercount;
		if ($pendingcount) {
			$pendingtext = "&bull; {$pendingcount} pending";
		}
	}
	$postmembers .= "<span id=\"{$phylum}memberspending_{$postid}\" class=\"fadetext\">{$pendingtext}</span>";

	/* Add the list of group members here. */
	$postmembers .= "<ul id=\"{$phylum}membersUL_{$postid}\" class=\"inlinelist\">";
	if ($membercount > 0) {
		$query = sprintf('select members.*, accounts.fullname, accounts.username from members, accounts'
				.' where members.postid = %d and accounts.id = members.accountid;', $postid);
		$membersresult = func_php_query($query);
		while ($member = mysqli_fetch_assoc($membersresult)) {
			$userid = $member['accountid'];
			$postmember = func_php_insertMemberLink($member['username'], true, false,
					"{$phylum}memberentrylink_{$postid}_{$userid}", $postid, $phylum, 0, $index_activeuserid);
			if ($postmember) {
				$postmembers .= "<li id=\"{$phylum}memberentry_{$postid}_{$userid}\">{$postmember}</li>";
			}
		}
	}
	$postmembers .= '</ul>';

	return $postmembers;
}

/**
 * Returns the innerHTML of a post recipients div.
 */
function index_php_insertPostRecipients($phylum, $postid)
{
	$query = sprintf('/* '.__LINE__.' */ select distinct concat_ws("", accounts.username, recipients.emailusername,'
			.' 	if (recipients.emailserver is not NULL, "@", ""), recipients.emailserver) as recipient'
			.' from recipients left join accounts on accounts.id = recipients.recipientaccountid'
			.' where recipients.tablename = "posts" and recipients.tableid = %d', /* POSTID */
			$postid);
	$result = func_php_query($query);
	$postrecipients = '';
	$numrows = mysqli_num_rows($result);
	if ($numrows) {
		$postrecipients .= "<span class=\"fadetext\">Recipients ({$numrows}) </span>";
	}
	$postrecipients .= "<ul id=\"{$phylum}recipientsUL_{$postid}\" class=\"inlinelist\">";
	while ($r = mysqli_fetch_assoc($result)) {
		$recipient = $r['recipient'];
		$postrecipients .= "<li class=\"recipientsLI\">{$recipient}</li>";
	}
	$postrecipients .= '</ul>';

	return $postrecipients;
}

/**
 * Returns either HTML for a clear flag button,
 * or text when to say the flag can be cleared.
 */
function index_php_getClearFlagButton($phylum, $postid, $flagupdated, $flagcount)
{
	$timestamp = strtotime($flagupdated);
	/* timeoffset grows exponentially with flag count. When the owner clears the
	 * flag, and post gets reflagged, the owner has to wait longer to clear the flag. eg:
	 * 1 flag=1m, 2=2H 8m, 3=1d 12H, 4=1w 4d, 5=1M 3W, 6=6M 1W, 7=1Y 6M, etc. */
	$timeoffset = 60 * (pow($flagcount, 7));
	$timestamp += $timeoffset;
	$timedifference = $timestamp - time();
	$fuzzytime = func_php_fuzzyTimestamp($timedifference, false, true);
	if ($fuzzytime == 'now') {
		$fuzzytime = 'a few seconds';
	}

	if ($timedifference > 0) {
		return "You can clear the flag in {$fuzzytime}.";
	}

	return "<a class=\"silverbutton\" onClick=\"index_js_flagPost('{$phylum}', {$postid}, 0);\">Remove Flag</a>";
}

/**
 * Return an array with strings of microdata from a specified post.
 */
function index_php_microdataArray($postid)
{
	$query = sprintf('/* '.__LINE__.' */ select * from microdata'
			.' where microdata.postid = %d limit 20;', /* POSTID */
			$postid);
	$result = func_php_query($query);

	if (mysqli_num_rows($result) < 1) {
		return false;
	}

	$microdataarray = array();
	$ct = 0;
	while (($ct < 20) && ($data = mysqli_fetch_assoc($result))) {
		$itemprop = $data['itemprop'];
		$strval = $data['strval'];
		$intval = $data['intval'];

		/* Format item value by its property */
		$str = '';
		switch ($itemprop) {
		case 'price':
			$val = sprintf('AU$%0.2f', $intval / 100);
			$str = "<span itemprop=\"price\" content=\"{$val}\">"
					."<meta itemprop=\"priceCurrency\" content=\"AUD\">"
					."{$val}</span>";
			break;
		case 'size':
			/* Size is currently assumed to be quantity */
			$str = "<span itemprop=\"size\">{$intval}</span> available.";
			break;
		}

		$microdataarray[] = array('str' => $str);
	}

	return $microdataarray;
}

/**
 * Send an email to a list of recipients.
 * Can either send a plain text version, or HTML message with attachments.
 * - $postid is self explanatory.
 * - $emailfrom is the standard form of the "From:" header.
 * - $emailrecipients is a comma separated string of email addresses.
 * - $subjectprefix is for specific titles, such as [EDITED].
 * Returns: True if sent, False on error or if no view permissions.
 */
function index_php_mail($postid, $emailfrom, $emailrecipients, $subjectprefix)
{
	global $index_activeuserid;

	$permissions = func_php_getUserPermissions($postid, $index_activeuserid);
	if (!$permissions['canview'])
		return false;

	$query = sprintf('select * from posts where posts.id = %d limit 1;', $postid);
	$result = func_php_query($query);
	if (mysqli_num_rows($result) != 1)
		return false;

	$r = mysqli_fetch_assoc($result);

	/* Generate a stripped-down version of the post suitable for plain-text email. */
	$description = func_php_htmltomarkdown($r['description']);
	$description = strip_tags($description);
	$subject = $subjectprefix;
	$subject .= substr($description, 0, 70);
	if (strlen($description) > 70)
		$subject .= '...';
	$subject = str_replace(array("\r", "\n"), '', $subject);

	$headers = "From: $emailfrom\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "X-Mailer: PHP/".phpversion()."\r\n";
	$headers .= "To: {$emailrecipients}\r\n";
	$headers .= "Subject: {$subject}\r\n";

	/* Build attachments (if any). This will determine if we use multipart. */
	$query = sprintf('select * from files where postid=%d', $postid);
	$result = func_php_query($query);

	$boundary = '';
	if (mysqli_num_rows($result) == 0) {
		$headers .= "Content-type: text/plain; charset=UTF-8\r\n";
		$headers .= "Content-Transfer-Encoding: quoted-printable\r\n";
		$message = quoted_printable_encode($description);
	} else {
		$boundary = uniqid('boundary_');
		$headers .= "Content-Type: multipart/mixed; boundary={$boundary}\r\n\r\n";
		$message = "--{$boundary}\r\n";
		$message .= "Content-Type: text/plain; charset=UTF-8\r\n";
		$message .= "Content-Disposition: inline\r\n";
		$message .= "Content-Transfer-Encoding: quoted-printable\r\n\r\n";
		$message .= quoted_printable_encode($description)."\r\n\r\n";

		/* Add attachments */
		while ($r = mysqli_fetch_assoc($result)) {
			$filename = mb_encode_mimeheader($r['filename'], "UTF-8", "Q");
			$filedata = func_php_base64file($r['id'], $r['filename']);
			$mime = $r['mime'];
			if (!preg_match('/^image.(gif|jpeg|jpg|png)$/', $mime)) {
				$mime = 'application/octet-stream';
			}
			$message .= "--{$boundary}\r\n";
			$message .= "Content-Type: {$mime}; name=\"{$filename}\"\r\n";
			$message .= "Content-Disposition: attachment; filename=\"{$filename}\"\r\n";
			$message .= "Content-Transfer-Encoding: base64\r\n\r\n";
			$message .= "{$filedata}\r\n\r\n";
		}
		$message .= "--{$boundary}--\r\n";
	}

	$log = func_php_sendmail($message, $headers);
	//error_log('index.php:'.__LINE__.' - '.print_r($log, true));

	return true;
}

function index_php_deletetemporaryfiles()
{
	global $index_activeuserid;
	global $s3profile, $s3endpoint, $s3region, $s3bucket;

	/* Temporarily disabled - forcing delete.
	$query = sprintf('select * from files where accountid = %d and updated is NULL'
			.' and added < DATE_SUB(NOW(), INTERVAL 1 DAY);',
			$index_activeuserid);
	// */
	$query = sprintf('select * from files where accountid = %d and updated is NULL;', $index_activeuserid);
	$ajaxarray['query'] = $query;
	$result = func_php_query($query);
	if (mysqli_num_rows($result)) {
		while ($r = mysqli_fetch_assoc($result)) {
			/* TODO:ERRORCHECK - only delete database entries on successful delete/unlink. */
			func_php_deleteFile($r);
			$query = sprintf('delete from files where id=%d;', $r['id']);
			func_php_query($query);
		}
	}
}

/**
 * Handle XMLHTTP (AJAX) requests.
 */

/**
 *
 */
function index_php_ajax_username_available()
{
	global $mysqli;

	$ajaxarray = array();
	$username = func_php_cleantext($_POST['username'], 64);
	$fullname = func_php_cleantext($_POST['fullname'] ?? '', 255);
	$suggestedusername = '';
	$ajaxarray['post'] = print_r($_POST, true);

	if ((strlen($username) > 64)
			|| (preg_match('/^[A-Za-z0-9]{5,}[0-9]+$/', $username) !== 1)
			|| (preg_match('/[A-Za-z]+/', $username) !== 1)) {
		$ajaxarray['error'] = 'ERROR_SIGNUP_USERNAME_INVALID';
		$username = '';
	}

	if ($username) {
		$query = sprintf('select count(accounts.id) from accounts where username="%s" limit 1;',
				func_php_escape($username));
		$result = func_php_query($query);
		$r = mysqli_fetch_assoc($result);
		if ($r['count(accounts.id)'] == 0) {
			$ajaxarray['error'] = 'INFO_USERNAME_AVAILABLE';
		} else {
			$username = '';
		}
	}

	if (!$username) {
		$ajaxarray['error'] = 'ERROR_USERNAME_UNAVAILABLE';
	}

	/* Username not available or invalid - generate suggested usernames */
	$found = 1;
	$loopct = 0;
	while ($found != 0) {
		$suggestedusername = strtolower(preg_replace('/[^a-zA-Z0-9]/', '', $fullname));
		if ($loopct < 10) {
			$suggestedusername .= str_repeat(rand(1, 9), rand(1, 3));
		} else {
			$suggestedusername .= rand(11, 999999);
		}
		$query = sprintf('select count(accounts.id) from accounts where username="%s" limit 1;',
				func_php_escape($suggestedusername));
		$result = func_php_query($query);
		$r = mysqli_fetch_assoc($result);
		$found = $r['count(accounts.id)'];
		$loopct++;
	}
	$ajaxarray['suggestedusername'] = $suggestedusername;

	return $ajaxarray;
}

/**
 * Called from index_php_ajax_save_profile_information()
 */
function index_php_ajax_save_profile_information_photo($ajaxarray)
{
	global $mysqli;
	global $index_activeusername, $index_activeuserid;
	$pimgarray = array();

	$filename = func_php_cleantext($_FILES['profileimgfileinput']['name'], 65535);
	$filetmpname = func_php_cleantext($_FILES['profileimgfileinput']['tmp_name'], 65535);
	$filesize = func_php_inttext($_FILES['profileimgfileinput']['size']);

	/* TODO: Change the following lines to a function.
	 * They're reused in index_php_ajax_upload_files() */
	/* Using getimagesize to determine the MIME type is a work around
	 * for when the exif_imagetype() function is not available. */
	ini_set('user_agent', 'Mozilla/4.0 (compatible; MirrorIsland Previewer)');
	$imagesizearray = getimagesize($filetmpname);
	$width = $imagesizearray[0];
	$height = $imagesizearray[1];
	$mime = $imagesizearray['mime'];

	/* Check to see if the uploaded file is a valid image. */
	$filemime = '';
	$pimg_src = func_php_filetogdimage($filetmpname, $filemime);
	$pimgarray['profileimgfileinput']['mime'] = $filemime;

	/* Return early on invalid image */
	if (!$pimg_src) {
		return $pimgarray;
	}

	/* Save a unique ID to database. */
	$query = sprintf('insert into files(accountid, added, filename) values(%d, NOW(), "%s")',
			$index_activeuserid, func_php_escape($filename));
	func_php_query($query);

	$fileid = mysqli_insert_id($mysqli);
	$fileindex = "{$fileid}.jpg";
	$filedest = "files/{$fileindex}";

	$pimgarray['profileimgfilename'] = $filename;
	$pimgarray['profileimgfileid'] = $fileid;

	/* Generate the cropped image and save. */
	$pimg_new = imagecreatetruecolor(256, 256);
	$pimg_width = func_php_inttext($_POST['profilephotowidth']);
	$pimg_height = func_php_inttext($_POST['profilephotoheight']);
	$pimg_left = func_php_inttext($_POST['profilephotoleft']);
	$pimg_top = func_php_inttext($_POST['profilephototop']);
	imagecopyresampled($pimg_new, $pimg_src, 0, 0, $pimg_left, $pimg_top,
			256, 256, $pimg_width, $pimg_height);
	imagejpeg($pimg_new, $filedest, 90);
	chmod($filedest, 0666);
	$pimgarray['newfile'] = $filedest;

	$filesize = filesize($filedest);
	$query = sprintf('update files set filesize=%d, filemime="%s", fileindex="%s",'
			.' filewidth=256, fileheight=256, updated=NOW() ' /* Hardcoded image sizes; change if needed */
			.' where id=%d limit 1;', $filesize,
			func_php_escape($mime), func_php_escape($fileindex), $fileid);
	func_php_query($query);
	$query = sprintf('update accounts set profileimagefileid = %d where id=%d limit 1;',
			$fileid, $index_activeuserid);
	func_php_query($query);

	return $pimgarray;
}

/**
 *
 */
function index_php_ajax_save_profile_information()
{
	global $mysqli;
	global $index_activeuserid, $index_activeusername, $index_activesessionid;
	$ajaxarray = array();

	/* Save profile information data. */
	$profilefullname = func_php_cleantext($_POST['profilefullname'], 255);
	$profileemail = func_php_cleantext($_POST['profileemail'], 255);
	$profilewebsite = func_php_cleantext($_POST['profilewebsite'], 255);
	$profileaddress = func_php_cleantext($_POST['profileaddress'], 255);
	$profileaddress2 = func_php_cleantext($_POST['profileaddress2'], 255);
	$profiletown = func_php_cleantext($_POST['profiletown'], 255);
	$profilestate = func_php_cleantext($_POST['profilestate'], 255);
	$profilepostcode = func_php_cleantext($_POST['profilepostcode'], 255);
	$profilecountry = func_php_cleantext($_POST['profilecountry'], 255);
	$profileinterests = func_php_cleantext($_POST['profileinterests'], 65535);
	$profileabout = func_php_cleantext($_POST['profileabout'], 65535);
	$profileimagefileid = func_php_inttext($_POST['profileeditimagefileid']);

	$profileinterests = str_replace('&', '&amp;', $profileinterests);
	$profileinterests = str_replace('\\', '&slash;', $profileinterests);
	$profileinterests = str_replace("\r\n", '<br>', $profileinterests);
	$profileabout = str_replace('&', '&amp;', $profileabout);
	$profileabout = str_replace('\\', '&slash;', $profileabout);
	$profileabout = str_replace("\r\n", '<br>', $profileabout);

	$query = sprintf('update accounts set fullname="%s", email="%s", website="%s",'
			.' address="%s", address2="%s", town="%s", state="%s",'
			.' postcode="%s", country="%s", interests="%s", about="%s"'
			.' where id=%d and sessionid="%s" limit 1',
			func_php_escape($profilefullname), func_php_escape($profileemail),
			func_php_escape($profilewebsite), func_php_escape($profileaddress),
			func_php_escape($profileaddress2), func_php_escape($profiletown),
			func_php_escape($profilestate), func_php_escape($profilepostcode),
			func_php_escape($profilecountry), func_php_escape($profileinterests),
			func_php_escape($profileabout),
			$index_activeuserid, func_php_escape($index_activesessionid));
	func_php_query($query);
	index_php_indexprofileforsearch($index_activeuserid, TRUE);

	/* Delete image if no profileimagefileid saved. */
	if ($profileimagefileid == 0) {
		$query = sprintf('select accounts.profileimagefileid, files.filename as profileimagefilename,'
				.' files.fileindex as profileimagefileindex'
				.' from accounts left join files on files.id = accounts.profileimagefileid'
				.' where accounts.id = %d limit 1;', $index_activeuserid);
		$result = func_php_query($query);
		$r = mysqli_fetch_assoc($result);
		if (($r['profileimagefileid'] > 0) && $r['profileimagefileindex']) {
			unlink('files/'.$r['profileimagefileindex']);
			$query = sprintf('delete from files where id = %d limit 1;', /* FILEID */
					$r['profileimagefileid']);
			func_php_query($query);

			$query = sprintf('delete from searchindex where tablename = "files"'
					.' and tableid = %d limit 1;', $r['profileimagefileid']); /* FILEID */
			func_php_query($query);
		}
	}

	/* Upload and crop profile img, if given. */
	$ajaxarray['files'] = print_r($_FILES, true);
	if (isset($_FILES['profileimgfileinput']['error'])
			&& (func_php_inttext($_FILES['profileimgfileinput']['error']) == 0)) {
		$pimgarray = index_php_ajax_save_profile_information_photo(NULL);
		$ajaxarray += $pimgarray;
	}

	/* Return with any extra data. */
	$ajaxarray['error'] = 'INFO_PROFILE_INFORMATION_SAVED';
	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_save_profile_settings()
{
	global $mysqli;
	global $index_activeuserid, $index_activeusername, $index_activesessionid;
	$ajaxarray = array();

	/* Save profile information data. */
	$profileviewpermissions = func_php_inttext($_POST['profilesettingsviewpermissions']);
	$profileeditpermissions = func_php_inttext($_POST['profilesettingseditpermissions']);
	$profilereplypermissions = func_php_inttext($_POST['profilesettingsreplypermissions']);
	$profileapprovepermissions = func_php_inttext($_POST['profilesettingsapprovepermissions']);
	$profilemoderatepermissions = func_php_inttext($_POST['profilesettingsmoderatepermissions']);
	$profiletagpermissions = func_php_inttext($_POST['profilesettingstagpermissions']);
	$profilelicense = func_php_cleantext($_POST['profilesettingslicense'], 256);
	$profileconfirmedage = func_php_inttext($_POST['profilesettingsconfirmedage']);
	$profileaccountemail = func_php_cleantext($_POST['profilesettingsaccountemail'], 256);

	/* TODO: Find a more secure way of doing this. */
	if (func_php_cleantext($_POST['profilesettingslicenselaterversions'], 256) == 'on')
		$profilelicenselaterversions = 1;
	else
		$profilelicenselaterversions = 0;

	$profileviewpermissions = index_php_minmax($profileviewpermissions, 0, 5);
	$profileeditpermissions = index_php_minmax($profileeditpermissions, 0, 5);
	$profilereplypermissions = index_php_minmax($profilereplypermissions, 0, 5);
	$profileapprovepermissions = index_php_minmax($profileapprovepermissions, 0, 5);
	$profilemoderatepermissions = index_php_minmax($profilemoderatepermissions, 0, 5);
	$profiletagpermissions = index_php_minmax($profiletagpermissions, 0, 5);

	$query = sprintf('update accounts set viewpermissions=%d, editpermissions=%d, replypermissions=%d,'
			.' approvepermissions=%d, moderatepermissions=%d, tagpermissions=%d,'
			.' license="%s", licenselaterversions=%d,'
			.' confirmedage=%d, accountemail="%s"'
			.' where id=%d and sessionid="%s" limit 1',
			$profileviewpermissions, $profileeditpermissions, $profilereplypermissions,
			$profileapprovepermissions, $profilemoderatepermissions, $profiletagpermissions,
			func_php_escape($profilelicense), $profilelicenselaterversions,
			$profileconfirmedage, func_php_escape($profileaccountemail),
			$index_activeuserid, func_php_escape($index_activesessionid));
	func_php_query($query);
	$ajaxarray['updatequery'] = $query;

	/* Return with any extra data. */
	$ajaxarray['error'] = 'INFO_PROFILE_SETTINGS_SAVED';
	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_delete_account()
{
	global $mysqli;
	global $index_activeusername, $index_activeuserid;
	$ajaxarray = array();

	$ajaxarray['post'] = print_r($_POST, true);
	/* Check current username and password. */
	$profileusername = func_php_cleantext($_POST['profileusername'], 255);
	if (strtolower($profileusername) != strtolower($index_activeusername)) {
		$ajaxarray['message'] = 'Incorrect login details.';
		$ajaxarray['error'] = 'ERROR_SIGN_IN_INVALID_USERNAME_OR_PASSWORD';
		return $ajaxarray;
	}

	$profilepassword = func_php_cleantext($_POST['profilepassword'], 255);
	$accountid = index_php_verifypassword($index_activeusername, $profilepassword);
	if (!$accountid) {
		$ajaxarray['message'] = 'Incorrect login details.';
		$ajaxarray['error'] = 'ERROR_SIGN_IN_INVALID_USERNAME_OR_PASSWORD';
		return $ajaxarray;
	}

	$messages = '';

	/* Remove search data and clear searchable flag where possible. */
	$query = sprintf('delete searchindex.* from searchindex where searchindex.tablename = "accounts"'
			.' and searchindex.tableid = %d;', $accountid); /* ACCTID */
	func_php_query($query);

	$query = sprintf('delete searchindex.* from searchindex'
			.' inner join files on searchindex.tableid = files.id and searchindex.tablename = "files"'
			.' where files.accountid = %d;', $accountid); /* ACCTID */
	func_php_query($query);

	/* Remove search entries for posts, before removing posts themselves. */
	$query = sprintf('delete searchindex.* from searchindex'
			.' inner join posts on searchindex.tableid = posts.id and searchindex.tablename = "posts"'
			.' where posts.accountid = %d;', $accountid); /* ACCTID */
	func_php_query($query);

	/* Start to remove posts and non-search data from here on... */

	$query = sprintf('delete from bookmarks where accountid=%d;', $accountid); /* ACCTID */
	func_php_query($query);

	$query = sprintf('delete from contacts where accountid = %d;', $accountid); /* ACCTID */
	func_php_query($query);

	$query = sprintf('delete from members where accountid=%d;', $accountid); /* ACCTID */
	func_php_query($query);

	$query = sprintf('delete from microdata where accountid=%d', $accountid);
	func_php_query($query);

	$query = sprintf('delete membertree.* from membertree left join posts as p on p.id = membertree.postid'
			.' left join posts as pp on pp.id = membertree.parentid'
			.' where p.accountid = %d or pp.accountid = %d;', $accountid, $accountid); /* ACCTID, ACCTID */
	func_php_query($query);

	$query = sprintf('delete approvals.* from approvals'
			.' left join posts on posts.id = approvals.tableid and approvals.tablename = "posts"'
			.' left join files on files.id = approvals.tableid and approvals.tablename = "files"'
			.' where posts.accountid = %d or files.accountid = %d' /* ACCTID, ACCTID */
			.' or approvals.accountid = %d;', $accountid, $accountid, $accountid); /* ACCTID */
	func_php_query($query);

	$query = sprintf('delete recipients.* from recipients'
			.' left join posts on posts.id = recipients.tableid and recipients.tablename = "posts"'
			.' left join files on files.id = recipients.tableid and recipients.tablename = "files"'
			.' where posts.accountid = %d or files.accountid = %d' /* ACCTID, ACCTID */
			.' or recipients.senderaccountid = %d;', $accountid, $accountid, $accountid); /* ACCTID */
	func_php_query($query);

	/* Remove all profile-related personally identifiable information. */
	$query = sprintf('select accounts.profileimagefileid, accounts.backgroundimagefileid,'
			.' pi.filename as profileimagefilename, pi.fileindex as profileimagefileindex,'
			.' bg.filename as backgroundimagefilename, bg.fileindex as backgroundimagefileindex'
			.' from accounts left join files as pi on accounts.profileimagefileid = pi.id'
			.' left join files as bg on accounts.backgroundimagefileid = bg.id'
			.' where accounts.id = %d limit 1;', $accountid); /* ACCTID */
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);

	if (($r['profileimagefileid'] > 0) && $r['profileimagefileindex']) {
		unlink('files/'.$r['profileimagefileindex']);
		$query = sprintf('delete from files where id = %d limit 1;', /* FILEID */
				$r['profileimagefileid']);
		func_php_query($query);

		$query = sprintf('delete from searchindex where tablename = "files"'
				.' and tableid = %d limit 1;', $r['profileimagefileid']); /* FILEID */
		func_php_query($query);
	}

	if (($r['backgroundimagefileid'] > 0) && $r['backgroundimagefileindex']) {
		unlink('files/'.$r['backgroundimagefileindex']);
		$query = sprintf('delete from files where id = %d limit 1;', /* FILEID */
				$r['backgroundimagefileid']);
		func_php_query($query);
	}

	/* Delete files associated with the account. */
	$numrows = 1;
	while ($numrows > 0) {
		$query = sprintf('select fileindex from files where files.accountid = %d' /* ACCTID */
				.' order by id limit 100;', $accountid);
		$result = func_php_query($query);
		$numrows = mysqli_num_rows($result);
		if ($numrows > 0) {
			while ($r = mysqli_fetch_assoc($result)) {
				/* TODO:ERRORCHECK - only delete database entries on successful unlink. */
				func_php_deleteFile($r);
			}
			$query = sprintf('delete from files where files.accountid = %d', $accountid);
			func_php_query($query);
		}
	}

	/* Remove post pin states. */
	$query = sprintf('delete from pins where pins.postid in (select posts.id from posts'
			.' 	where accountid=%d);', $accountid); /* ACCTID */
	func_php_query($query);

	/* Post have to be removed pretty much last; other tables might depend on the data. */
	$query = sprintf('delete from posts where accountid=%d;', $accountid); /* ACCTID */
	func_php_query($query);

	/* Clean up any unused search keywords. */
	$query = sprintf('delete searchkeywords.* from searchkeywords'
			.' left join searchindex on searchindex.keywordid = searchkeywords.id'
			.' where searchindex.keywordid is NULL;');
	error_log('index.php:'.__LINE__.' index_php_ajax_delete_account() skipping $query='.$query);
	/**
	 * Running this query has been commented out since it takes a LOT of
	 * time to process and causes the "Delete Account" form to time out.
	 * Also, the query is generic and doesn't target any user specifically,
	 * so it can be called anytime periodically to clean up the database.
	 */
	//func_php_query($query);

	/* Remove entire account. */
	$query = sprintf('delete from passwords where accountid = %d limit 1;', $accountid); /* ACCTID */
	func_php_query($query);

	$query = sprintf('delete from accounts where id = %d limit 1;', $accountid); /* ACCTID */
	func_php_query($query);

	/* Add a blank entry for the username - prevents username hijacking. */
	$query = sprintf('insert into accounts (id, username) values (%d, "%s");',
			$accountid, func_php_escape($profileusername));
	func_php_query($query);

	$ajaxarray['message'] = 'Account deleted.';

	$ajaxarray['error'] = 'INFO_ACCOUNT_DELETED';
	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_change_password()
{
	global $mysqli;
	global $index_activeusername, $index_activeuserid;
	$ajaxarray = array();

	$ajaxarray['POST'] = print_r($_POST, true);
	$newpassword = func_php_cleantext($_POST['newpassword'], 255);
	$newpasswordcheck = func_php_cleantext($_POST['newpasswordcheck'], 255);
	if (($newpassword != $newpasswordcheck)
			|| (strlen($newpassword) < 6)) {
		$ajaxarray['error'] = 'ERROR_NEW_PASSWORDS_DO_NOT_MATCH_OR_INVALID';
		return $ajaxarray;
	}

	/* Check for reset link id */
	$linkid = '';
	$passwordaccountid = 0;
	if (isset($_POST['resetlinkid'])) {
		$linkid = func_php_cleantext($_POST['resetlinkid'], 255);
		$query = sprintf('select id from accounts'
				.' where username="%s" and resetlinkid="%s"' /* USERNAME, LINKID */
				.' and now() < date_add(resetlinkadded, interval 1 hour)'
				.' limit 1',
				func_php_escape($index_activeusername), func_php_escape($linkid));
		$result = func_php_query($query);
		if (mysqli_num_rows($result) !== 1) {
			$ajaxarray['error'] = 'ERROR_CHANGE_PASSWORD_INVALID_RESET_LINK';
			return $ajaxarray;
		}
		$r = mysqli_fetch_assoc($result);
		$passwordaccountid = $r['id'];
		$ajaxarray['resetlinkid'] = $linkid;
		$ajaxarray['windowlocationhref'] = "/{$index_activeusername}/";
		$query = sprintf('update accounts'
				.' set resetlinkid = NULL, resetlinkadded = NULL'
				.' where id = %d limit 1', /* USERID */
				$passwordaccountid);
		func_php_query($query);
	} else {
		/* Check current password. */
		$profilepassword = func_php_cleantext($_POST['profilepassword'], 255);
		$passwordaccountid = index_php_verifypassword($index_activeusername, $profilepassword);
		if (!$passwordaccountid) {
			$ajaxarray['error'] = 'ERROR_CHANGE_PASSWORD_INVALID_CURRENT_PASSWORD';
			return $ajaxarray;
		}
	}

	/* Update new password. */
	$passwordsalt = "$2y$13$";
	for ($i = 0; $i < 22; $i++) {
		$passwordsalt .= substr('1234567890/abcdefghijklmnopqrstuvwxyz.ABCDEFGHIJKLMNOPQRSTUVWXYZ',
				mt_rand(0, 63), 1);
	}
	$passwordhash = crypt($newpassword, $passwordsalt);
	$query = sprintf('update passwords set password="%s" where accountid=%d limit 1',
			func_php_escape($passwordhash), $passwordaccountid);
	$ajaxarray['updatequery'] = $query;
	func_php_query($query);
	$ajaxarray['error'] = 'INFO_PASSWORD_CHANGED';

	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_save_post($ajax_command)
{
	global $mysqli;
	global $index_activeuserid, $index_activeusername, $index_activefullname;
	global $servername, $servertitle, $urlhome;
	global $index_activeuserfileuse, $index_activeuserfilespace;

	$ajaxarray = array('error' => 'ERROR_EDIT_POST');
	$newpostparentpermissions = false;
	$originalaccountid = 0; /* Set correct senderaccountid with recipients */
	$emailsubject = '';
	$awsloaded = false;

	$id = func_php_inttext($_POST['editpostid'], 255);
	$added = 0;
	$username = '';
	$parentid = func_php_inttext($_POST['newpostparentid']);
	$postid = func_php_inttext($_POST['newpostpostid']); /* Used for identifying replies to guest posts. */
	$viewmode = func_php_cleantext($_POST['newpostviewmode'], 64);
	$inlinereply = func_php_inttext($_POST['inlinereply'] ?? 0);
	$showparent = false;
	$parentpermissions = false;
	if ($parentid != 0) {
		$parentpermissions = func_php_getUserPermissions($parentid, $index_activeuserid);
		if (!$parentpermissions)
			$parentid = 0;
	}

	/* Branch - Editing an existing post or adding a new one?
	 * If editing a new one (handled a few lines down) generate
	 * a blank post and used its ID to save post data to. */
	if ((($ajax_command == 'edit_post') || ($ajax_command == 'edit_message'))
			&& ($id > 0)) {
		/* First, make sure we're in full view mode */
		if ($viewmode != 'full') {
			$ajaxarray['error'] = 'ERROR_EDIT_POST_INVALID_VIEW_MODE';
			return $ajaxarray;
		}

		/* Next check if post exists and the user has permission to edit. */
		$permissions = func_php_getUserPermissions($id, $index_activeuserid);
		if (!$permissions['canedit']) {
			$ajaxarray['error'] = 'ERROR_EDIT_POST_INVALID_POST_OR_USER_ID';
			return $ajaxarray;
		}
		$added = $permissions['added'];
		$authorusername = $permissions['username'];
		$originalaccountid = $permissions['accountid'];

		/* Showparent is used later for _insertPostCredits and _insertPopupMenu functions. */
		if (($ajax_command == 'edit_post') && (isset($_POST['editpostshowparent']))) {
			$showparent = func_php_cleantext($_POST['editpostshowparent'], 64);
			if ($showparent == 'true') {
				$showparent = true;
			} else {
				$showparent = false;
			}
		}
	} else if (($ajax_command == 'new_post') || ($ajax_command == 'new_postreply')
			|| ($ajax_command == 'new_message')) {
		if ($parentpermissions && !$parentpermissions['canreply']) {
			$ajaxarray['error'] = 'ERROR_NEW_POST_CAN_NOT_REPLY';
			$ajaxarray['parentid'] = __LINE__.' '.$parentid;
			return $ajaxarray;
		}

		$preapproved = 0;
		$postreplystyle = '';
		if ((($viewmode == 'full') || ($viewmode == 'thumbnail'))
				&& !$inlinereply) {
			$postreplystyle .= ' debugreplyborderclass';
		}

		if ($viewmode == 'thumbnail') {
			$postreplystyle .= ' debugpostthumbnail';
		} else if ($viewmode == 'list') {
			$postreplystyle .= ' debugpostlist';
		}

		if ($parentid > 0) {
			$postreplystyle .= ' postapprovalnormal';
			if ($parentpermissions['preapproved']) {
				/* Pre-approve post */
				$preapproved = 1;
			} else {
				$postreplystyle .= ' postapprovalpending';
				$ajaxarray['poststatus'] = 'This post is pending approval.';
			}
		}
		$ajaxarray['postreplystyle'] = $postreplystyle;

		/* New post or reply - generate a new post id to save to */
		$query = sprintf('/* '.__LINE__.' */ insert into posts (parentid, accountid, added)'
				.' values (%d, %d, NOW());', $parentid, $index_activeuserid);
		$ajaxarray['insertquery'] = $query;
		func_php_query($query);

		$id = mysqli_insert_id($mysqli);
		$query = sprintf('select posts.added from posts'
				.' where posts.id=%d limit 1;', $id);
		$result = func_php_query($query);
		if (mysqli_num_rows($result) != 1) {
			$ajaxarray['error'] = 'ERROR_NEW_POST_NOT_SAVED';
			$ajaxarray['query'] = $query;
			return $ajaxarray;
		}

		$r = mysqli_fetch_assoc($result);
		$added = $r['added'];
		$authorusername = $index_activeusername;
		$originalaccountid = $index_activeuserid;

		/* Add membertree details if a parentid has been given. */
		if ($parentid > 0) {
			$membertreequery = sprintf('/* '.__LINE__.' */ insert into membertree '
					.'(postid, parentid, level) select %d, %d, 1'
					.' UNION select %d, newbranch.parentid, newbranch.level+1'
					.' from membertree as newbranch'
					.' where newbranch.postid = %d;',
					$id, $parentid, $id, $parentid);
			$ajaxarray['membertreequery'] = $membertreequery;
			func_php_query($membertreequery);
		}

		/* Add an approval entry, where pending or pre-approved (if applicable.) */
		if ($preapproved != 1) {
			$preapproved = 'NULL';
		}
		$query = sprintf('insert into approvals (tablename, tableid, accountid, trusted, hidden, added)'
				.' values ("posts", %d, %d, %s, NULL, NOW());', /* POSTID, ACCTID, TRUSTED */
				$id, $index_activeuserid, $preapproved);
		func_php_query($query);
	} else {
		/* ERROR! We shouldn't reach here! */
		$ajaxarray['error'] = 'ERROR_POST_EDITOR_INVALID';
		return $ajaxarray;
	}

	/* Fetch and correct permissions and ratings */
	$viewpermissions = func_php_inttext($_POST['newpostviewpermissions']);
	$editpermissions = func_php_inttext($_POST['newposteditpermissions']);
	$replypermissions = func_php_inttext($_POST['newpostreplypermissions']);
	$approvepermissions = func_php_inttext($_POST['newpostapprovepermissions']);
	$moderatepermissions = func_php_inttext($_POST['newpostmoderatepermissions']);
	$tagpermissions = func_php_inttext($_POST['newposttagpermissions']);
	$defaultviewmode = func_php_cleantext($_POST['newpostdefaultviewmode'], 64);
	$defaultsortmode = func_php_cleantext($_POST['newpostdefaultsortmode'], 64);
	$defaultsortreverse = func_php_cleantext($_POST['newpostdefaultsortreverse'], 64);
	$recipients = func_php_cleantext($_POST['newpostrecipients'], 4096);
	$agerating = func_php_inttext($_POST['newpostagerating'] ?? 3);
	if (($viewpermissions < 0) || ($viewpermissions > 5)) $viewpermissions = 0;
	if (($editpermissions < 0) || ($editpermissions > 5)) $editpermissions = 0;
	if (($replypermissions < 0) || ($replypermissions > 5)) $replypermissions = 0;
	if (($approvepermissions < 0) || ($approvepermissions > 5)) $approvepermissions = 0;
	if (($moderatepermissions < 0) || ($moderatepermissions > 5)) $moderatepermissions = 0;
	if (($tagpermissions < 0) || ($tagpermissions > 5)) $tagpermissions = 0;
	if ($agerating < 3)
		$agerating = 3;
	else if ($agerating > 100)
		$agerating = 100;

	/* Adjust any permissions for guest accounts */
	if (!$index_activeuserid) {
		if (($viewpermissions != 5) && ($viewpermissions != 4)
				&& ($viewpermissions != 1)) {
			$viewpermissions = 5;
		}

		if (($editpermissions != 5) && ($editpermissions != 4)
				&& ($editpermissions != 1)) {
			$editpermissions = 5;
		}

		if (($replypermissions != 5) && ($replypermissions != 4)
				&& ($replypermissions != 1)) {
			$replypermissions = 5;
		}

		if (($approvepermissions != 5) && ($approvepermissions != 4)
				&& ($approvepermissions != 1)) {
			$approvepermissions = 5;
		}

		if (($moderatepermissions != 5) && ($moderatepermissions != 4)
				&& ($moderatepermissions != 1)) {
			$moderatepermissions = 5;
		}

		if (($tagpermissions != 5) && ($tagpermissions != 4)
				&& ($tagpermissions != 1)) {
			$tagpermissions = 5;
		}
	}

	/* Handle file attachments and images, if given. */
	$i = 0;
	if ($index_activeuserid && isset($_POST['newpostfileid'])) {
		foreach ($_POST['newpostfileid'] as $newpostfileid) { /* Sanitised immediately below */
			$fileid = func_php_inttext($newpostfileid);
			$filename = func_php_cleantext($_POST['newpostfilename'][$i], 256);
			$filewasuploaded = func_php_inttext($_POST['newpostfilewasuploaded'][$i]);
			$deletefile = func_php_inttext($_POST['newpostdeletefile'][$i]);
			$query = sprintf('/* '.__LINE__.' */ select * from files'
					.' where id=%d and filename="%s"' /* FILEID, FILENAME */
					.' and accountid=%d limit 1;', /* ACCTID */
					$fileid, func_php_escape($filename), $index_activeuserid);
			$ajaxarray['selectcountfromfiles'][$i] = $query;
			$result = func_php_query($query);
			if (mysqli_num_rows($result) == 1) {
				$r = mysqli_fetch_assoc($result);
				$fileid = $r['id'];
				$fileindex = $r['fileindex'] ?? false;
				$ajaxarray['files'][$i]['fileid'] = $fileid;
				$ajaxarray['files'][$i]['filename'] = $filename;
				$ajaxarray['files'][$i]['deletefile'] = $deletefile;
				if (!$deletefile) {
					$ajaxarray['filelinkerror'][$i] = 'INFO_UPDATING_FILE_MATCH';
					$query = sprintf('/* '.__LINE__.' */ update files set newpostsessionid=NULL,'
							.' updated=NOW(), postid=%d,' /* POSTID */
							.' viewpermissions=%d, editpermissions=%d,' /* PERMISSION, PERMISSION */
							.' replypermissions=%d, approvepermissions=%d,' /* PERMISSION, PERMISSION */
							.' moderatepermissions=%d, tagpermissions=%d' /* PERMISSION, PERMISSION */
							.' where files.id = %d limit 1', $id, $viewpermissions, $editpermissions, /* FILEID */
							$replypermissions, $approvepermissions, $moderatepermissions, $tagpermissions, $fileid);
					$ajaxarray['fileupdatequery'][$i] = $query;
					func_php_query($query);
				} else { /* File was marked for deletion. */
					func_php_deleteFile($r);
					$query = sprintf('delete from files where id=%d;', $fileid); /* FILEID */
					func_php_query($query);
					$ajaxarray['filedeletequery'][$i] = $query;
				}
			} else {
				$ajaxarray['filelinkerror'][$i] = 'ERROR_FILE_NO_MATCH';
			}
			$i++;
		}
	}

	/* Delete any images no longer in the description */
	$postfilelist = array();

	$description = func_php_cleantext($_POST['newpostdescription'], 4000000);

	/* Post-process formatting */
	$description = str_replace('&nbsp;', ' ', $description);

	/* Extract metadata and image tags from HTML */
	$domdoc = new DOMDocument();
	if ($description) { /* Crashes on empty description */
		$domdoc->loadHTML($description);
	}

	/* Get semantic microdata */
	$spantags = $domdoc->getElementsByTagName('span');
	$insertquery = '';
	foreach ($spantags as $span) {
		$type = 'string';
		$val = false;
		$itemprop = false;
		if ($span->hasAttribute('itemprop')) {
			$itemprop = $span->getAttribute('itemprop');
			$textContent = $span->textContent;
			$val = func_php_cleantext($textContent, 255);
			if (preg_match('!\d+\.*\d*!', $textContent, $nums)) {
				$val = $nums[0];
				$type = 'decimal';
			}
		}
		if ($itemprop && $insertquery) {
			$insertquery .= ', ';
		}
		if ($itemprop && ($type == 'decimal')) {
			if ($itemprop == 'price')
				$val *= 100;

			$insertquery .= sprintf('(%d, %d, "%s", NULL, %d)',
					$id, $index_activeuserid,
					$itemprop, $val);
		} else {
			$insertquery .= sprintf('(%d, %d, "%s", "%s", NULL)',
					$id, $index_activeuserid,
					func_php_escape($itemprop), $val);
		}
	}

	/* Remove existing microdata, then insert any newly fetched data. */
	$query = sprintf('delete from microdata where postid = %d', $id);
	func_php_query($query);

	if ($insertquery) {
		$query = 'insert into microdata'
				.' (postid, accountid, itemprop, strval, intval) values '
				.$insertquery;
		func_php_query($query);
	}

	/* Build a list of images that are in the description... */
	$imgtags = $domdoc->getElementsByTagName('img');
	$postimage = '';
	$home = str_replace([':', '/'], ['\:', '\/'], $urlhome);
	foreach ($imgtags as $tag) {
		$src = $tag->getAttribute('src');
		if (preg_match('/^'.$home.'\/?index\.php\?fileid=([0-9]*)\&filename=(.*)$/', $src, $matches)
			|| preg_match('/^\/?index\.php\?fileid=([0-9]*)\&filename=(.*)$/', $src, $matches)) {
			$postfilelist[] = $matches[1];
		}

		/* First found image is used for thumbnail/OpenGraph image, if any. */
		if (!$postimage) {
			$postimage = $src;
		}
	}

	/* ...and compare with currently saved files, removing any that aren't used anymore. */
	$query = sprintf('select * from files where postid=%d'
			.' and filemime in ("image/gif", "image/jpeg", "image/png");',
			$id);
	$result = func_php_query($query);
	while ($r = mysqli_fetch_assoc($result)) {
		$postfile = $r['id'];
		if (!in_array($postfile, $postfilelist)) {
			func_php_deleteFile($r);
			$query = sprintf('delete from files where id=%d and filename="%s" limit 1;',
					$r['id'], func_php_escape($r['filename']));
			func_php_query($query);
		}
	}

	/* Mark remaining new files as published by setting "updated" column. */
	$query = sprintf('update files set newpostsessionid=NULL, files.updated=NOW()'
			.' where files.postid=%d;', $id); /* POSTID */
	func_php_query($query);

	/* Get used and free disk space */
	$index_activeuserfileuse = index_php_getuserfileuse(FALSE, FALSE);
	$filespace = $index_activeuserfilespace - $index_activeuserfileuse;
	$ajaxarray['savepostfilespace'] = $filespace;
	$ajaxarray['savepostfilespacetext'] = func_php_fuzzyfilesize($filespace).' available';

	/* Save first image URI. */
	$query = sprintf('update posts set image="%s"' /* IMAGE */
			.' where posts.id=%d limit 1;', /* POSTID */
			func_php_escape($postimage), $id);
	func_php_query($query);

	/* $phylum is used to distinguish posts from messages.
	 * $posttype identifies how and which posts can be pinned.
	 * $pagetype identifies profile and page posts for pinning */
	$phylum = 'post';
	$posttype = '';
	$pagetype = '';
	if (($ajax_command == 'new_message') || ($ajax_command == 'edit_message')) {
		$phylum = 'message';
		$posttype = 'message';
	} else if ($ajax_command == 'new_post') {
		$posttype = 'post';
		$pagetype = func_php_cleantext($_POST['newpostpagetype'], 64);
	} else if ($ajax_command == 'new_postreply') {
		$posttype = 'reply';
	}

	$query = sprintf('select posts.added, accounts.username, accounts.fullname'
			.' from posts inner join accounts on posts.accountid = accounts.id'
			.' where posts.id=%d limit 1;', $id);
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);
	$username = $r['username'];
	$ajaxarray['username'] = $username;
	$ajaxarray['fullname'] = $r['fullname'];

	/* Some of these might need to be moved down this function to get updated data. */
	$ajaxarray['newpostid'] = $id;
	$ajaxarray['parentid'] = $parentid;
	$ajaxarray['added'] = $added;

	if (($defaultviewmode != 'full') && ($defaultviewmode != 'thumbnail')
			&& ($defaultviewmode != 'list')) {
		$defaultviewmode = 'full';
	}

	if (($defaultsortmode != 'date') && ($defaultsortmode != 'title')) {
		$defaultsortmode = 'date';
	}

	if ($defaultsortreverse == 'true') {
		$defaultsortreverse = true;
	} else {
		$defaultsortreverse = false;
	}

	/* Fetch title (as well as other text as short description) if given */
	$title = '';
	$shortdesc = '';
	if (preg_match('/^#\s(.+?)(<br>)(.*)?$/', $description, $matches)) {
		$title = func_php_cleantext(func_php_stringtosearchcache($matches[1]), 255);
		if (count($matches) >= 4) {
			$shortdesc = func_php_cleantext(func_php_stringtosearchcache($matches[3]), 1024);
		}
	} else if (preg_match('/^#\s(.+?)(<br>)?/', $description, $matches)) {
		$title = func_php_cleantext(func_php_stringtosearchcache($matches[1]), 255);
	} else {
		$shortdesc = func_php_cleantext(func_php_stringtosearchcache($description), 1024);
	}

	$query = sprintf('/* '.__LINE__.' */ update posts set description="%s",'
			.' viewpermissions=%d, editpermissions=%d, replypermissions=%d,'
			.' approvepermissions=%d, moderatepermissions=%d,'
			.' tagpermissions=%d, viewmode="%s", agerating=%d,'
			.' sortmode="%s", sortreverse=%d,'
			.' title="%s", shortdesc="%s", updated=NOW()'
			.' where posts.id = %d limit 1', func_php_escape($description),
			$viewpermissions, $editpermissions, $replypermissions,
			$approvepermissions, $moderatepermissions, $tagpermissions,
			func_php_escape($defaultviewmode), $agerating,
			func_php_escape($defaultsortmode), $defaultsortreverse,
			func_php_escape($title), func_php_escape($shortdesc), $id);
	$result = func_php_query($query);
	$ajaxarray['postprocess'] = 'indexpostforsearch';
	$ajaxarray['postpopuplist'] = func_php_insertPostPopupList($phylum, $id,
			$showparent, $posttype, $pagetype, $viewmode, $index_activeuserid);
	$ajaxarray['memberlink'] = func_php_insertMemberLink($username, true, false, 0, 0, $phylum, $id, $index_activeuserid);

	$phylum = '';
	$editordescription = '';
	if (($ajax_command == 'new_post') || ($ajax_command == 'new_postreply')) {
		$phylum = 'post';
		$ajaxarray['error'] = 'INFO_NEW_POST_SAVED';
	} else if ($ajax_command == 'edit_post') {
		$phylum = 'post';
		$ajaxarray['error'] = 'INFO_EDIT_POST_SAVED';
		$markdown = func_php_markdowntohtml($description);
		$editordescription = $markdown;
		$searchcache = func_php_stringtosearchcache($markdown);
		$ajaxarray['title'] = substr($searchcache, 0, 140) .' - '.$servertitle;
	} else if ($ajax_command == 'new_message') {
		/* Save settings that get sent with the popup editor. */
		$messagesendonenterkey = 0;
		$messagecloseonsend = 0;

		if (func_php_cleantext($_POST['messagesendonenterkey'], 255) == 'true')
			$messagesendonenterkey = 1;

		if (func_php_cleantext($_POST['messagecloseonsend'], 255) == 'true')
			$messagecloseonsend = 1;

		$query = sprintf('update accounts set messagesendonenterkey=%d,'
				.' messagecloseonsend=%d where id=%d limit 1',
				$messagesendonenterkey, $messagecloseonsend, $index_activeuserid);
		$result = func_php_query($query);

		$phylum = 'message';
		$ajaxarray['error'] = 'INFO_NEW_MESSAGE_SAVED';
	} else if ($ajax_command == 'edit_message') {
		$phylum = 'message';
		$ajaxarray['error'] = 'INFO_EDIT_MESSAGE_SAVED';
	} else {
		$ajaxarray['error'] = 'ERROR_INCOMPLETE_POST_FUNC';
	}
	$permissions = $viewpermissions.$editpermissions.$replypermissions.$approvepermissions.$moderatepermissions.$tagpermissions;
	$ajaxarray['phylum'] = $phylum;
	$ajaxarray['posttype'] = $posttype;
	$ajaxarray['permissions'] = $permissions;
	$ajaxarray['defaultviewmode'] = $defaultviewmode;
	$ajaxarray['defaultsortmode'] = $defaultsortmode;
	$ajaxarray['defaultsortreverse'] = ($defaultsortreverse ? 1 : 0);
	$ajaxarray['agerating'] = $agerating;
	$ajaxarray['ajax_command'] = $ajax_command;
	$ajaxarray['inlinereply'] = $inlinereply;
	$ajaxarray['viewmode'] = $viewmode;

	/* Generate description data to display in postdescription div here. */
	$postdescription = '';
	if ($viewmode == 'thumbnail') {
		if (!$postimage) {
			$postimage = '/img/default-thumbnail.jpg';
		}
		$postdescription = "<div class=\"squareimagediv\"><a href=\"/{$id}\"><img src=\"{$postimage}\"></a></div>";

		$postdescription .= '<div class="postdescriptionpadding">';
		if ($title) {
			$postdescription .= "<h1><a href=\"/{$id}\">{$title}</a></h1>";
		}

		if ($microdataarray = index_php_microdataArray($id)) {
			foreach ($microdataarray as $microdata) {
				$postdescription .= "<div>{$microdata['str']}</div>";
			}
		} else {
			$postdescription .= trim(substr($shortdesc, 0, 40));
			if (strlen($shortdesc) > 40)
				$postdescription .= '...';
		}
		$postdescription .= '</div>';
	} else if ($viewmode == 'list') {
		$postdescription = "&nbsp;&#183;&nbsp;<a href=\"/{$postid}\">";
		if ($title) {
			$postdescription .= $title;
		}

		if ($title && $shortdesc) {
			$postdescription .= ' &#183; ';
		}

		$subshortdesc = trim(substr($shortdesc, 0, 80));
		if ($subshortdesc) {
			$postdescription .= "<span style=\"color:#888;\">{$subshortdesc}</span>";
		}

		if (!$title && !$subshortdesc) {
			$postdescription .= '(No subject)';
		}
		$postdescription .= '</a>';
	} else {
		/* Show full post */
		$postdescription = func_php_markdowntohtml($description);
	}
	$ajaxarray['description'] = $postdescription;

	/* Separate local and remote users, verifying local ones */
	$emailrecipientarray = array();
	$localrecipientarray = array();
	$localrecipientidarray = array();
	$recipientarray = array();
	$emailrecipients = ''; /* Comma-separated string used later for sending emails */
	if ($recipients && ($ajax_command !== 'edit_message')) {
		$recipients = func_php_escape(preg_replace('/\s+/', ' ', $recipients));
		$recipients = strtolower($recipients);
		$recipientarray = explode(' ', $recipients);
		$recipientarray = array_unique($recipientarray);
		foreach ($recipientarray as $key => $recipient) {
			$username = false;
			$server = false;
			/* Separate local and remote email addresses */
			if (preg_match('/^([^@]+)@(.+)$/', $recipient, $matches)) {
				$username = $matches[1];
				$server = $matches[2];
				if ($server == $servername) {
					/* Local recipient */
					$query = sprintf('select count(*) from accounts'
							.' where username="%s" limit 1', /* USERNAME */
							func_php_escape($username));
					$result = func_php_query($query);
					$r = mysqli_fetch_assoc($result);
					if ($r['count(*)'] == 1) {
						$recipient = $username;
						$server = false;
					}
				}
				if ($server) {
					/* Add email recipients if not already exist. The insert query checks
					 * if a row already exists and doesn't insert if one is found. */
					$query = sprintf('insert into recipients'
							.' (tablename, tableid, senderaccountid, recipientaccountid,'
							.' emailusername, emailserver, added)'
							.' select "posts", %d, %d, NULL,' /* POSTID, ACCTID */
							.' "%s", "%s", NOW() from recipients' /* EMAILUSER, EMAILSERVER */
							.' where recipients.tablename = "posts" and recipients.tableid = %d' /* POSTID */
							.' and recipients.senderaccountid = %d' /* ACCTID */
							.' and emailusername = "%s" and emailserver = "%s"' /* EMAILUSER, EMAILSERVER */
							.' having count(*) = 0 limit 1;',
							$id, $originalaccountid, func_php_escape($username), func_php_escape($server),
							$id, $originalaccountid, func_php_escape($username), func_php_escape($server));
					func_php_query($query);

					if ($emailrecipients)
						$emailrecipients .= ', ';
					$emailrecipients .= $recipient;
					$emailrecipientarray[] = $recipient;
				}
			}

			/* Check for valid local recipient */
			if ($recipient && !$server) {
				$recipientid = 0;
				$query = sprintf('select id, username from accounts where username = "%s" limit 1;',
						func_php_escape($recipient));
				$result = func_php_query($query);
				if (mysqli_num_rows($result)) {
					$r = mysqli_fetch_assoc($result);
					$recipientid = $r['id'];
					$recipientusername = $r['username'];
					if ($recipientid == $originalaccountid) {
						$recipientid = 0;
					}
				}

				if ($recipientid) {
					/* Add local recipients only if it doesn't already exist for that post.
					 * The insert query checks for an already exsting row for the specified post
					 * and doesn't insert if one is found. */
					$query = sprintf('insert into recipients'
							.' (tablename, tableid, senderaccountid, recipientaccountid, added)'
							.' select "posts", %d, %d, %d, NOW() from recipients' /* POSTID, ACCTID, RECPID */
							.' where recipients.tablename = "posts" and recipients.tableid = %d' /* POSTID */
							.' and recipients.senderaccountid = %d' /* ACCTID */
							.' and recipients.recipientaccountid = %d' /* RECPID */
							.' having count(*) = 0 limit 1;',
							$id, $originalaccountid, $recipientid,
							$id, $originalaccountid, $recipientid);
					func_php_query($query);
					$localrecipientarray[] = $recipientusername;
					$localrecipientidarray[] = $recipientid;
				} else {
					unset($recipientarray[$key]);
				}
			}
		}
		$recipients = implode(' ', $recipientarray);
	}

	if ($ajax_command !== 'edit_message') {
		$query = sprintf('/* '.__LINE__.' */ select recipients.*, accounts.username'
				.' from recipients left join accounts on recipients.recipientaccountid = accounts.id'
				.' where recipients.tablename = "posts" and recipients.tableid = %d', /* POSTID */
				$id);
		$result = func_php_query($query);

		/* Remove any already existing recipients that aren't specified by the user in this edit.
		 * Already existing recipients that aren't removed between edits won't be removed from the post but will be
		 * removed from the new recipient array to be added. */
		$idremovearray = array();
		while ($r = mysqli_fetch_assoc($result)) {
			$username = $r['username'];
			$emailusername = $r['emailusername'];
			$emailserver = $r['emailserver'];
			$email = "{$emailusername}@{$emailserver}";

			if (!in_array($username, $localrecipientarray) && !in_array($email, $emailrecipientarray)) {
				/* Remove already existing recipients that aren't specified in this edit */
				$idremovearray[] = $r['id'];
			} else {
				/* Clear preserved existing recipients, so they aren't added as recipients again */
				$key = array_search($username, $localrecipientarray);
				if ($key) {
					/* Both arrays are generated at exactly the same time,
					 * so the same key can be used for both arrays */
					unset($localrecipientarray[$key]);
					unset($localrecipientidarray[$key]);
				}

				$key = array_search($email, $emailrecipientarray);
				if ($key) {
					unset($emailrecipientarray[$key]);
				}
			}
		}

		if (count($idremovearray)) {
			$idlist = implode(' ,', $idremovearray);
			$query = sprintf('delete from recipients where recipients.tablename = "posts"'
					.' and recipients.tableid = %d and recipients.senderaccountid = %d' /* POSTID, ACCTID */
					.' and recipients.id in (%s);', /* IDLIST */
					$id, $originalaccountid, $idlist);
			$result = func_php_query($query);
		}

		/* Get current recipients after (conditionally) removing/adding recipients. */
		$recipients = '';
		if (count($localrecipientarray) || count($emailrecipientarray)) {
			$query = sprintf('/* '.__LINE__.' */ select (select group_concat(distinct concat_ws("",'
					.' 	accounts.username, recipients.emailusername,'
					.'	if (recipients.emailserver is not NULL, "@", ""), recipients.emailserver)'
					.' separator " ")'
					.' from recipients left join accounts on accounts.id = recipients.recipientaccountid'
					.' where recipients.tablename = "posts" and recipients.tableid = %d)'
					.' as recipients;', $id); /* POSTID */
			$result = func_php_query($query);
			$r = mysqli_fetch_assoc($result);
			$recipients = $r['recipients'];

			/* TODO - START move this block to backend.php...
			 * Other functions also call to use it. */
			foreach ($localrecipientidarray as $recipientid) {
				/* Send the message to any open sseserver.php
				 * instances specified by userID */
				/* TODO: Can msg_send/msg_receive calls be moved to a function?
				 * TODO: They are also used to change online/offline statuses. */
				$backend_key = IPCMSGKEY + $recipientid;
				$confirmation_key = (IPCMSGKEY * 2) + $recipientid;
				if (msg_queue_exists($backend_key)
						&& msg_queue_exists($confirmation_key)) {
					$backend_queue = msg_get_queue($backend_key);
					$confirmation_queue = msg_get_queue($confirmation_key);
					$json = json_encode(array('type' => 'message_received', 'postid' => $id,
							'sender' => $index_activeusername, 'recipient' => $recipientid));

					/* Empty the confirmation queue */
					while (msg_receive($confirmation_queue, IPCMSGTYPE, $msgtype, 4096,
							$reply, false, MSG_IPC_NOWAIT)) { /* Do nothing */ }

					$attempts = 5;
					$md5 = md5($json);
					$replymd5 = '';
					do {
						msg_send($backend_queue, IPCMSGTYPE, $json);
						time_nanosleep(0, 100000000); /* 0.1 second delay */

						/* Get a confirmation from the SSE backend */
						$reply = false;
						if (msg_receive($confirmation_queue, IPCMSGTYPE, $msgtype, 4096,
								$reply, false, MSG_IPC_NOWAIT)) {
							$replymd5 = unserialize($reply);
						}
						$attempts--;
					} while (($attempts > 0) && ($replymd5 !== $md5));
				}
				/* TODO - END move this block to backend.php... */
			}
		}
	}

	$moredetail = true;
	if ($viewmode == 'list')
		$moredetail = false;

	$ajaxarray['recipients'] = $recipients;
	$ajaxarray['postcredits'] = func_php_insertPostCredits($phylum, $id, $showparent, $viewmode,
			$moredetail, $index_activeuserid);
	$ajaxarray['postmembers'] = index_php_insertPostMembers($phylum, $id);
	$ajaxarray['postrecipients'] = index_php_insertPostRecipients($phylum, $id);

	$ajaxarray['editlinktext'] = func_php_cleantext($_POST['editlinktext'], 255);

	/* Send emails where addresses given */
	if ($emailrecipients) {
		$emailfrom = "$index_activefullname <{$index_activeusername}@{$servername}>";
		$subject = '';
		if (($ajax_command == 'edit_post') || ($ajax_command == 'edit_message')) {
			$subject = '[EDITED] ';
		}
		index_php_mail($id, $emailfrom, $emailrecipients, $subject);
	}

	/* Add message HTML */
	if ($ajax_command == 'new_message') {
		$ajaxarray['messageHTML'] = func_php_postToMessageHTML($id, $index_activeuserid);
	}

	return $ajaxarray;
}

/**
 * Setup the editor for a new post.
 */
function index_php_ajax_setup_editor()
{
	global $index_activeusername, $index_activeuserid;
	global $index_activeuserfileuse, $index_activeuserfilespace;
	$ajaxarray = array();

	$parentid = func_php_inttext($_POST['parentid']);
	if ($parentid) {
		$userpermissions = func_php_getUserPermissions($parentid, $index_activeuserid);
		if (!$userpermissions['canreply']) {
			$ajaxarray['error'] = 'ERROR_SETUP_EDITOR_CAN_NOT_REPLY_TO_POST';
			return $ajaxarray;
		}
	}

	index_php_deletetemporaryfiles();

	/* Get used and free disk space */
	$index_activeuserfileuse = index_php_getuserfileuse(FALSE, FALSE);
	$filespace = $index_activeuserfilespace - $index_activeuserfileuse;
	$ajaxarray['setupeditorfilespace'] = $filespace;
	$ajaxarray['setupeditorfilespacetext'] = func_php_fuzzyfilesize($filespace).' available';
	$ajaxarray['setupposteditorviewmode'] = func_php_cleantext($_POST['setupposteditorviewmode'], 64);

	$ajaxarray['newpostsessionid'] = mt_rand(0, pow(2, 16));
	$ajaxarray['error'] = 'INFO_SETUP_POST_PROCEED';
	return $ajaxarray;
}

/**
 * Edit an existing post or message.
 */
function index_php_ajax_init_editor()
{
	global $index_activeusername, $index_activeuserid;
	global $index_activeuserfilespace, $index_activeuserfileuse;
	$ajaxarray = array('error' => 'ERROR_CAN_NOT_EDIT');
	$ajaxarray['post'] = print_r($_POST, TRUE);

	$viewmode = func_php_cleantext($_POST['editpostviewmode'], 64);
	/* First, make sure we're in full view mode */
	if ($viewmode != 'full') {
		$ajaxarray['error'] = 'ERROR_INIT_EDITOR_INVALID_VIEW_MODE';
		return $ajaxarray;
	}
	$ajaxarray['editpostviewmode'] = $viewmode;

	$phylum = func_php_cleantext($_POST['editpostphylum'], 64);
	if (($phylum != 'post') && ($phylum != 'message')) {
		$ajaxarray['error'] = 'ERROR_EDIT_POST_INVALID_PHYLUM';
		return $ajaxarray;
	}
	$ajaxarray['phylum'] = $phylum;

	$showparent = func_php_cleantext($_POST['editpostshowparent'], 64);
	if ($showparent != 'true')
		$showparent = 'false';
	$ajaxarray['showparent'] = $showparent;

	$postid = func_php_inttext($_POST['editpostid']);
	$userpermissions = func_php_getUserPermissions($postid, $index_activeuserid);
	if (!$userpermissions['canedit']) {
		$ajaxarray['error'] = 'ERROR_INIT_EDIT_POST_INVALID_POST_OR_USER_ID';
		return $ajaxarray;
	}

	index_php_deletetemporaryfiles();
	$index_activeuserfileuse = index_php_getuserfileuse(FALSE, FALSE);
	$sumfilesize = index_php_getuserfileuse($postid, FALSE);
	$filespace = $index_activeuserfilespace - $index_activeuserfileuse;

	$ajaxarray['editpostfilespace'] = $filespace;
	$ajaxarray['editpostfilespacetext'] = func_php_fuzzyfilesize($filespace).' available';

	$query = sprintf('select id, description from posts where posts.id = %d limit 1;', $postid);
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);
	$ajaxarray['id'] = $r['id'];
	$ajaxarray['description'] = $r['description'];

	$query = sprintf('/* '.__LINE__.' */ select * from files where files.postid=%d' /* POSTID */
			.' and files.filemime not in ("image/gif", "image/jpeg", "image/png");',
			$postid);
	$result = func_php_query($query);
	if (mysqli_num_rows($result) > 0) {
		$i = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			$ajaxarray['files'][$i] = $r;
			$i++;
		}
	}

	$query = sprintf('select distinct concat_ws("", accounts.username, recipients.emailusername,'
			.' if (recipients.emailserver is not NULL, "@", ""), recipients.emailserver) as recipient'
			.' from recipients left join accounts on accounts.id = recipients.recipientaccountid'
			.' where recipients.tablename = "posts" and recipients.tableid = %d', /* POSTID */
			$postid);
	$result = func_php_query($query);
	if (mysqli_num_rows($result)) {
		$i = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			$ajaxarray['recipients'][$i] = $r['recipient'];
			$i++;
		}
	}

	$ajaxarray['error'] = 'INFO_EDIT_POST_PROCEED';
	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_cancel_edit_post()
{
	global $index_activeuserid;

	$ajaxarray['error'] = 'ERROR_CANCEL_EDIT_POST';
	$ajaxarray['post'] = print_r($_POST, true);

	$postid = func_php_inttext($_POST['canceleditpostid']);
	if ($postid > 0) {
		$userpermissions = func_php_getUserPermissions($postid, $index_activeuserid);
		if (!$userpermissions) {
			$ajaxarray['error'] = 'ERROR_CANCEL_EDIT_INVALID_POST_ID';
			return $ajaxarray;
		}
	}
	$postdescriptionid = func_php_cleantext($_POST['canceleditdescriptionid'], 255);

	if (isset($_POST['canceleditpostfileid'])) {
		$i = 0;
		foreach ($_POST['canceleditpostfileid'] as $canceleditpostfileid) { /* Sanitised immediately below */
			$fileid = func_php_inttext($canceleditpostfileid);
			$filename = func_php_cleantext($_POST['canceleditpostfilename'][$i], 4096);
			$query = sprintf('select * from files where id=%d and filename="%s" and added is null limit 1;', /* FILEID, FILENAME */
					$fileid, $filename);
			$ajaxarray['selectfromfiles'][$i] = $query;
			$result = func_php_query($query);
			if (mysqli_num_rows($result) == 1) {
				$r = mysqli_fetch_assoc($result);
				func_php_deleteFile($r);
				$query = sprintf('delete from files where id=%d limit 1;', $r['id']); /* FILEID */
				func_php_query($query);
			}

			$i++;
		}
	}

	$ajaxarray['canceleditdescriptionid'] = $postdescriptionid;
	$ajaxarray['error'] = 'INFO_CANCEL_EDIT_POST_SUCCESS';
	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_delete_post()
{
	global $index_activeusername, $index_activeuserid;
	global $index_activeuserfileuse, $index_activeuserfilespace;

	$ajaxarray = array('error' => 'ERROR_DELETE_POST');
	$phylum = func_php_cleantext($_POST['deletepostphylum'], 64);
	if (($phylum != 'post') && ($phylum != 'message')) {
		return $ajaxarray;
	}

	$postid = func_php_cleantext($_POST['deletepostid'], 255);
	// XXX Is this needed? $username = func_php_cleantext($_POST['deletepostusername'], 255);
	$userpermissions = func_php_getUserPermissions($postid, $index_activeuserid);
	if (!$userpermissions['canedit']) {
		$ajaxarray['error'] = 'ERROR_DELETE_POST_INVALID_POST_OR_USER_ID';
		return $ajaxarray;
	}

	/* Delete recipients. */
	$query = sprintf('delete from recipients where recipients.tablename = "posts" and recipients.tableid = %d', $postid);
	func_php_query($query);

	/* Delete members */
	$query = sprintf('delete from members where members.postid = %d;', $postid);
	func_php_query($query);

	/* Delete membertree paths. */
	$query = sprintf('delete from membertree where membertree.postid = %d or membertree.parentid = %d;', $postid, $postid);
	func_php_query($query);

	/* Delete microdata */
	$query = sprintf('delete from microdata where postid = %d', $postid);
	func_php_query($query);

	/* Delete any resources attached to the page. (eg. images, audio, video, archive files, etc.) */
	$query = sprintf('select * from files where postid=%d;', $postid); /* POSTID */
	$result = func_php_query($query);
	if (mysqli_num_rows($result) > 0) {
		while ($r = mysqli_fetch_assoc($result)) {
			func_php_deleteFile($r);
		}
	}
	$query = sprintf('delete from files where postid=%d;', $postid); /* POSTID */
	func_php_query($query);

	/* Get used and free disk space */
	$index_activeuserfileuse = index_php_getuserfileuse(FALSE, FALSE);
	$filespace = $index_activeuserfilespace - $index_activeuserfileuse;
	$ajaxarray['deletepostfilespace'] = $filespace;
	$ajaxarray['deletepostfilespacetext'] = func_php_fuzzyfilesize($filespace).' available';

	$query = sprintf('delete from pins where pins.postid = %d', /* POSTID */
			$postid);
	$result = func_php_query($query);

	/* Delete the post itself. */
	$query = sprintf('delete from posts where posts.id = %d limit 1', $postid); /* POSTID */
	$result = func_php_query($query);
	$ajaxarray['error'] = 'INFO_DELETE_POST_SUCCESS';
	$ajaxarray['phylum'] = $phylum;
	$ajaxarray['postid'] = $postid;

	$data = array('f' => 'indexpostforsearch', 'id' => $postid, 'deleteflag' => 'true');
	func_php_sendbgfunc($data);

	return $ajaxarray;
}

/**
 * Sets a post's custom domain name.
 * Checks if the user is a premium account before setting post domain name.
 */
function index_php_ajax_set_post_domain_name()
{
	global $index_activeuser;

	$ajaxarray = array('error' => 'ERROR_SET_POST_DOMAIN_NAME');

	$postid = func_php_inttext($_POST['setpostdomainnameid']);
	$query = sprintf('select id from posts'
			.' where posts.id = %d and posts.accountid = %d' /* POSTID, USERID */
			.' limit 1;', $postid, $index_activeuser['id']);
	$ajaxarray['query'] = $query;
	$result = func_php_query($query);
	if (mysqli_num_rows($result) !== 1) {
		$ajaxarray['error'] = 'ERROR_SET_POST_DOMAIN_NAME_INVALID_USER';
		return $ajaxarray;
	}

	$domainname = func_php_cleantext($_POST['domainname'], 1024);

	if ($domainname) {
		/* Can these preg_matches be broken off as a function? */
		$charcheck = preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", /* Character check */
				$domainname);
		if (!$charcheck) {
			$ajaxarray['error'] = 'ERROR_SET_POST_DOMAIN_NAME_INVALID_CHARACTERS';
			return $ajaxarray;
		}

		$lengthcheck = preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", /* Segment length check */
				$domainname);
		if (!$lengthcheck) {
			$ajaxarray['error'] = 'ERROR_SET_POST_DOMAIN_NAME_INVALID_LENGTH';
			return $ajaxarray;
		}

		$query = sprintf('update posts set domainname = "%s"' /* DOMAINNAME */
				.' where posts.id = %d limit 1;', /* POSTID */
				func_php_escape($domainname), $postid);
		func_php_query($query);
	} else {
		/* Remove domain name data */
		$query = sprintf('update posts set domainname = NULL'
				.' where posts.id = %d limit 1;', /* POSTID */
				$postid);
		func_php_query($query);
	}

	$domainnamelink = '';
	if ($domainname) {
		$domainnamelink = " &#183; <a href=\"http://{$domainname}\">"
				."<img src=\"/img/icon-domainname.png\" style=\"vertical-align:-2px;\">&nbsp;{$domainname}</a>"
				."<span class=\"tooltiptext tooltiptextleft\">This post has a custom domain</span>";
	}
	$ajaxarray['domainnamelink'] = $domainnamelink;

	$ajaxarray['id'] = $postid;
	$ajaxarray['phylum'] = func_php_cleantext($_POST['setpostdomainnamephylum'], 32);
	$ajaxarray['domainname'] = $domainname;

	$ajaxarray['error'] = 'INFO_SET_POST_DOMAIN_NAME_SUCCESS';

	return $ajaxarray;
}

/**
 * To pin a post, the user must have edit permissions for both this post
 * and it's parent. Later we'll test if the user is on their profile page.
 */
function index_php_ajax_pin_post()
{
	global $index_activeusername, $index_activeuserid;
	$ajaxarray = array('error' => 'ERROR_PIN_POST_SETTING_STATE');

	$postid = func_php_inttext($_POST['pinpostid']);
	$posttype = func_php_cleantext($_POST['pinposttype'], 64);
	$pagetype = func_php_cleantext($_POST['pinpagetype'], 64);
	if (($pagetype != 'page') && ($pagetype != 'profile')) {
		$ajaxarray['error'] = 'ERROR_PIN_POST_INVALID_PAGETYPE';
		return $ajaxarray;
	}

	$parentid = 0; /* 0 parentid is profile pin; NULL would be invalid. */
	if ($pagetype == 'page') {
		$query = sprintf('select accountid, parentid from posts where posts.id = %d limit 1;', /* POSTID */
				$postid);
		$result = func_php_query($query);
		if (mysqli_num_rows($result) != 1) {
			$ajaxarray['error'] = 'ERROR_PIN_POST_INVALID_POST';
			return $ajaxarray;
		}
		$r = mysqli_fetch_assoc($result);
		$parentid = $r['parentid'];

		/* We can only pin replies to parent posts we can edit. */
		$parentuserpermissions = func_php_getUserPermissions($r['parentid'], $index_activeuserid);
		if (!$parentuserpermissions['canedit'] && ($r['accountid'] != $index_activeuserid)) {
			$ajaxarray['error'] = 'ERROR_PIN_POST_INVALID_PARENT_POST_OR_USER_ID';
			return $ajaxarray;
		}
	}

	$query = sprintf('select count(*) as pincount from pins'
			.' where pins.postid = %d and pins.parentpostid = %d;', /* POSTID */
			$postid, $parentid);
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);
	$pincount = $r['pincount'];
	$pinpoststate = func_php_inttext($_POST['pinpoststate']);
	$ajaxarray['postid'] = $postid;
	$ajaxarray['posttype'] = $posttype;
	$ajaxarray['pagetype'] = $pagetype;

	/* Update the post's pin status. */
	if ($pinpoststate == 1) {
		/* Try to set the post's pin state. */
		if ($pincount != 0) {
			$ajaxarray['error'] = 'WARNING_POST_ALREADY_PINNED';
			$ajaxarray['pinpostlinktext'] = 'Remove Pin';
			$ajaxarray['newpinpostvalue'] = 0;
			return $ajaxarray;
		}

		$query = sprintf('insert into pins (postid, parentpostid, pinstate, added)'
				.' values (%d, %d, TRUE, NOW());', /* POSTID, PARENTID */
				$postid, $parentid);
		func_php_query($query);
		$ajaxarray['pinpostlinktext'] = 'Remove Pin';
		$ajaxarray['newpinpostvalue'] = 0;
		$ajaxarray['error'] = 'INFO_PIN_POST_SUCCESS';
		return $ajaxarray;
	} else {
		/* Clear the post's pin state. */
		if ($pincount == 0) {
			$ajaxarray['error'] = 'ERROR_NO_POST_PIN_TO_CLEAR';
			return $ajaxarray;
		}

		$query = sprintf('delete from pins where postid = %d and parentpostid = %d', /* POSTID, PARENTID */
				$postid, $parentid);
		func_php_query($query);
		$ajaxarray['pinpostlinktext'] = 'Pin to Top';
		$ajaxarray['newpinpostvalue'] = 1;
		$ajaxarray['error'] = 'INFO_REMOVE_POST_PIN_SUCCESS';
		return $ajaxarray;
	}
}

/**
 *
 */
function index_php_ajax_approve_post()
{
	global $index_activeusername, $index_activeuserid;
	$ajaxarray = array();

	$ajaxarray['post'] = print_r($_POST, true);
	$phylum = func_php_cleantext($_POST['phylum'], 64);
	$postid = func_php_inttext($_POST['postid']);
	$ajaxarray['phylum'] = $phylum;
	$ajaxarray['postid'] = $postid;
	$postapproval = func_php_inttext($_POST['approval']); /* 1:Approve, 0:Unapprove(Neutral), -1:Hide */
	$ajaxarray['postapproval'] = $postapproval;

	/* Check post permissions. */
	$postpermissions = func_php_getPostPermissions($postid);
	if (!$postpermissions || ($postpermissions['parentid'] == NULL)) {
		$ajaxarray['error'] = 'ERROR_APPROVE_POST_INVALID_POST';
		return $ajaxarray;
	}

	$parentpermissions = func_php_getPostPermissions($postpermissions['parentid']);

	$parentuserpermissions = func_php_getUserPermissions($postpermissions['parentid'], $index_activeuserid);
	if (!$parentuserpermissions['canmoderate']) {
		$ajaxarray['error'] = 'ERROR_APPROVE_POST_CAN_NOT_MODERATE';
		return $ajaxarray;
	}

	$query = sprintf('select approvals.* from approvals where approvals.tablename = "posts"'
			.' and approvals.tableid = %d', $postid); /* POSTID */
	$result = func_php_query($query);
	$ajaxarray['selectapprovals'] = $query;
	$approvalid = false;
	if (mysqli_num_rows($result) == 1) {
		$r = mysqli_fetch_assoc($result);
		$approvalid = $r['id'];
	} else {
		$query = sprintf('insert into approvals (tablename, tableid, accountid, trusted, hidden, added)'
				.' values ("posts", %d, %d, NULL, NULL, NOW());', $postid, $index_activeuserid);
		func_php_query($query);
	}

	$ajaxarray['approveicon'] = '/img/icon-post-pending.png';
	$ajaxarray['approvelinktext'] = 'Approve Post';
	$ajaxarray['newapprovalvalue'] = 1;
	$ajaxarray['hidelinktext'] = 'Hide Post';
	$ajaxarray['newhidevalue'] = -1;
	if ($postapproval == 1) {
		$query = '';
		if (($approvalid > 0) && ($r['trusted'] == 1)) {
			$ajaxarray['error'] = 'ERROR_POST_ALREADY_APPROVED';
			return $ajaxarray;
		} else if ($approvalid > 0) {
			$query = sprintf('update approvals set approvals.trusted = 1, approvals.hidden = NULL,'
					.' added = NOW(), accountid = %d' /* USERID */
					.' where approvals.id = %d and approvals.tablename = "posts"' /* APRVID */
					.' and approvals.tableid = %d limit 1;', /* POSTID */
					$index_activeuserid, $approvalid, $postid);
		}
		func_php_query($query);
		$ajaxarray['approvelinktext'] = 'Undo Approve';
		$ajaxarray['newapprovalvalue'] = 0;
		$ajaxarray['approveicon'] = '/img/icon-post-approved.png';
		$ajaxarray['error'] = 'INFO_APPROVE_POST_SUCCESS';
		return $ajaxarray;
	} else if ($postapproval == 0) {
		$query = '';
		if (($approvalid > 0) && ($r['trusted'] == NULL) && ($r['hidden'] == NULL)) {
			$ajaxarray['error'] = 'ERROR_APPROVE_POST_ALREADY_RESET';
			return $ajaxarray;
		} else if ($approvalid > 0) {
			$query = sprintf('update approvals set approvals.trusted = NULL,'
					.' approvals.hidden = NULL, added = NOW(), accountid = %d' /* USERID */
					.' where approvals.id = %d and approvals.tableid = %d' /* APRVID, POSTID */
					.' and approvals.tablename = "posts"',
					$index_activeuserid, $approvalid, $postid);
		}
		func_php_query($query);

		/* Do not display pending for pre-approved posts. */
		if ($postpermissions['preapproved']) {
			$ajaxarray['approveicon'] = '';
		}
		$ajaxarray['error'] = 'INFO_APPROVE_POST_RESET';
		return $ajaxarray;
	} else if ($postapproval == -1) {
		$query = '';
		if (($approvalid > 0) && ($r['hidden'] == 1)) {
			$ajaxarray['error'] = 'ERROR_POST_ALREADY_HIDDEN';
			return $ajaxarray;
		} else if ($approvalid > 0) {
			$query = sprintf('update approvals set approvals.trusted = NULL,'
					.' approvals.hidden = 1, accountid = %d, added = NOW()' /* USERID */
					.' where approvals.id = %d and approvals.tablename = "posts"'
					.' and approvals.tableid = %d limit 1;', /* APRVID, POSTID */
					$index_activeuserid, $approvalid, $postid);
		}
		func_php_query($query);
		$ajaxarray['hidelinktext'] = 'Undo Hide';
		$ajaxarray['newhidevalue'] = 0;
		$ajaxarray['approveicon'] = '/img/icon-post-blocked.png';
		$ajaxarray['error'] = 'INFO_APPROVE_POST_HIDDEN';
		return $ajaxarray;
	}

	$ajaxarray['error'] = 'INFO_APPROVE_POST_UNFINISHED_FUNCTION';
	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_flag_post()
{
	global $index_activeusername, $index_activeuserid;
	global $index_activeuserconfirmedage;
	global $siteoperators;
	$ajaxarray = array();

	$postid = func_php_inttext($_POST['postid']);

	/* Check if current user has view permissions. */
	$permissions = func_php_getUserPermissions($postid, $index_activeuserid);
	$ajaxarray['permissions'] = print_r($permissions, true);
	if (!$permissions) {
		$ajaxarray['error'] = 'ERROR_FLAG_POST_INVALID_POST';
		return $ajaxarray;
	}

	if (!$permissions['canview']) {
		$ajaxarray['error'] = 'ERROR_FLAG_POST_INVALID_PERMISSIONS';
		return $ajaxarray;
	}

	/* Check for users that can't be flagged */
	$usernames = preg_split("/[\s,]+/", mb_strtolower($siteoperators));
	$usernames = array_unique($usernames);
	$testusername = mb_strtolower($permissions['username']);
	if (in_array($testusername, $usernames)) {
		$ajaxarray['error'] = 'ERROR_FLAG_POST_NOT_PRIVELEGED';
		return $ajaxarray;
	}

	$phylum = func_php_cleantext($_POST['phylum'], 64);
	$flagrequest = func_php_inttext($_POST['flagrequest']);
	$ajaxarray['phylum'] = $phylum;
	$ajaxarray['postid'] = $postid;
	$ajaxarray['flagrequest'] = $flagrequest;
	$ajaxarray['postaccountid'] = $permissions['accountid'];
	$ajaxarray['canmoderate'] = $permissions['canmoderate'];

	$query = sprintf('select flags.*, posts.accountid from flags'
			.' left join posts on posts.id = flags.tableid'
			.' where flags.tablename="posts" and flags.tableid=%d' /* POSTID */
			.' limit 1', $postid);
	$result = func_php_query($query);

	$flagactive = false;
	$flagcount = 0;
	if (mysqli_num_rows($result) == 1) {
		$r = mysqli_fetch_assoc($result);
		$flagcount = $r['flagcount'];
		$flagactive = $r['flagactive'];
		$postaccountid = $r['accountid'];
		$flagupdated = $r['updated'];
	}

	/* Check if clearing flag */
	if ($flagrequest == 0) {
		if ((($index_activeuserid == $postaccountid) && ($index_activeuserid > 0)) == false) {
			$ajaxarray['error'] = 'ERROR_ONLY_OWNER_CAN_CLEAR_FLAG';
			return $ajaxarray;
		} else if ($flagactive == 0) {
			$ajaxarray['error'] = 'ERROR_NO_FLAG_TO_CLEAR';
			return $ajaxarray;
		}

		/* Check if enough time has passed since last flag. */
		$timestamp = strtotime($flagupdated);
		$timeoffset = 60 * (pow($flagcount, 7));
		$timestamp += $timeoffset;
		if ($timestamp > time()) {
			$ajaxarray['error'] = 'ERROR_CLEAR_FLAG_NOT_TIME_PASSED';
			return $ajaxarray;
		}

		/* Clear the flag if we got this far. */
		$query = sprintf('update flags set flagactive = 0, updated = NOW()'
				.' where flags.tablename = "posts" and flags.tableid = %d;', $postid); /* POSTID */
		func_php_query($query);
		$ajaxarray['flagpostlinktext'] = 'Flag Post';
		$ajaxarray['newflagpostvalue'] = 1;
		$ajaxarray['error'] = 'INFO_FLAG_CLEAR_SUCCESS';
		return $ajaxarray;
	}

	/* Fall through if flagging post. */
	$agerating = false;
	$flagreason = strtoupper(func_php_cleantext($_POST['flagreason'], 255));
	switch ($flagreason) {
	case 'MATURE_CONTENT':
		$agerating = 15;
		break;
	case 'ADULT_CONTENT':
		$agerating = 18;
		break;
	case 'COPYRIGHT_OR_TRADEMARK':
		break;
	case 'ILLEGAL_OR_VIOLATION':
		break;
	default:
		$ajaxarray['error'] = 'ERROR_FLAG_POST_INVALID_REASON';
		return $ajaxarray;
	}

	$ajaxarray['flagpostagerating'] = $agerating;

	$flagupdated = false;
	if (!$flagcount) {
		$query = sprintf('insert into flags (tablename, tableid, flagcount, flagreason,'
				.' flagagerating, flagactive, added, updated)'
				.' values ("posts", %d, 1, "%s",' /* POSTID, FLAGREASON */
				.' %d, TRUE, NOW(), NOW());', /* AGERATING */
				$postid, $flagreason, $agerating);
		func_php_query($query);
	} else if (!$flagactive) {
		$query = sprintf('update flags set flagcount = flagcount+1, flagactive = TRUE, updated = NOW()'
				.' where tablename="posts" and tableid=%d limit 1;', /* POSTID */
				$postid);
		func_php_query($query);
	} else {
		$ajaxarray['error'] = 'ERROR_POST_ALREADY_FLAGGED';
		return $ajaxarray;
	}
	$flagcount++;

	/* Only show flagged post if owner or moderator,
	 * or current user's age meets flagged age for mature/adult content */
	if ($permissions['canedit'] || $permissions['canmoderate']
			|| ($agerating && ($index_activeuserconfirmedage >= $agerating))) {
		$ajaxarray['showflagged'] = true;
	} else {
		$ajaxarray['showflagged'] = false;
		$ajaxarray['error'] = 'INFO_FLAG_POST_SUCCESS';
		return $ajaxarray;
	}

	/* Add flag div HTML if still able to view flagged post */
	$query = sprintf('select updated from flags'
			.' where tablename="posts" and tableid=%d limit 1;', /* POSTID */
			$postid);
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);
	$flagupdated = $r['updated'];
	$fuzzytime = func_php_fuzzyTimeAgo($flagupdated, false, false);
	if ($fuzzytime == 'now') {
		$fuzzytime = 'a minute ago';
	}
	$reason = index_php_getflagreasondescription($flagreason);
	$fulldate = date('l g:i a\<\b\r\>jS F Y', strtotime($flagupdated));
	$flagdivinnerHTML = "This post was flagged <span class=\"tooltiphost poststatusflaggedunderline\">{$fuzzytime}"
			."<span class=\"tooltiptext tooltiptextleft tooltiptexttop\">{$fulldate}</span></span>."
			."<br>Reason: {$reason}";

	/* Add [Remove Flag] button for owner/editor/moderator */
	if ($permissions['canedit'] || $permissions['canmoderate']) {
		$flagdivinnerHTML .= '<br>'.index_php_getClearFlagButton($phylum, $postid,
				$flagupdated, $flagcount);
	}
	$ajaxarray['flagdivinnerHTML'] = $flagdivinnerHTML;
	$ajaxarray['flagpostlinktext'] = 'Remove Flag';
	$ajaxarray['newflagpostvalue'] = 0;

	$ajaxarray['error'] = 'INFO_FLAG_POST_SUCCESS';
	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_join_group()
{
	global $index_activeusername, $index_activeuserid;
	$ajaxarray = array();
	$membercount = 0;
	$trusted = 0;

	$ajaxarray['post'] = print_r($_POST, true);
	$phylum = func_php_cleantext($_POST['joingroupphylum'], 64);
	$postid = func_php_inttext($_POST['joingrouppostid']);
	$userid = func_php_inttext($_POST['joingroupuserid']);
	$joinrequest = func_php_inttext($_POST['joinrequest']);

	/* TODO: A username/ID fetching function that doubles as an account verifier. */
	$query = sprintf('select accounts.username from accounts where accounts.id = %d limit 1;', $userid);
	$result = func_php_query($query);
	if (mysqli_num_rows($result) != 1) {
		$ajaxarray['error'] = 'ERROR_JOIN_GROUP_INVALID_USERID';
		return $ajaxarray;
	}
	$r = mysqli_fetch_assoc($result);
	$username = $r['username'];

	/* Make sure the post exists AND EITHER the owner is already a group member,
	 * OR the member joining IS the owner. */
	$query = sprintf('select posts.accountid, members.trusted from posts'
			.' left join members on members.postid = posts.id and members.accountid = posts.accountid'
			.' where posts.id = %d limit 1;',
			$postid);
	$result = func_php_query($query);
	if (mysqli_num_rows($result) == 0) {
		$ajaxarray['query'] = $query;
		$ajaxarray['error'] = 'ERROR_JOIN_GROUP_INVALID_POST_ID';
		return $ajaxarray;
	}

	$r = mysqli_fetch_assoc($result);
	$ownerid = $r['accountid'];

	/* Try to set up group if it hasn't been. */
	if ($r['trusted'] == null) {
		if ($ownerid != $index_activeuserid) {
			$ajaxarray['error'] = 'ERROR_JOIN_GROUP_INVALID_GROUP';
			return $ajaxarray;
		}
		if ($joinrequest != 1) {
			$ajaxarray['error'] = 'ERROR_JOIN_GROUP_NOT_SET_UP';
			return $ajaxarray;
		}
	}

	$ajaxarray['ownerid'] = $ownerid;

	$query = sprintf('select members.trusted from members where members.accountid = %d and members.postid = %d limit 1;',
			$userid, $postid);
	$ajaxarray['checkquery'] = $query;
	$result = func_php_query($query);
	if (mysqli_num_rows($result) == 1) {
		$r = mysqli_fetch_assoc($result);
		$membercount = 1;
		$trusted = $r['trusted'];
	}

	$ajaxarray['variables'] = "userid=$userid ownerid=$ownerid postid=$postid joinrequest=$joinrequest trusted=$trusted;";
	$ajaxarray['phylum'] = $phylum;
	$ajaxarray['postid'] = $postid;
	$ajaxarray['userid'] = $userid;
	$ajaxarray['joinrequest'] = $joinrequest;
	/* Update pending join request as trusted, or if owner add user as member. */
	if (($joinrequest == 1) && ($ownerid == $index_activeuserid) && ($trusted == 0)) {
		$ajaxarray['checkblock'] = "Owner($ownerid) granted user($userid) join request($joinrequest).";
		$trusted = 1; /* Update trusted value */
	/* Record non-member sending join request. */
	} else if (($joinrequest == 1) && ($userid == $index_activeuserid) && ($membercount == 0)) {
		$ajaxarray['checkblock'] = "User($userid) with membercount($membercount) sent join request($joinrequest).";
	/* Remove member regardless of trusted flag. */
	} else if ((joinrequest == 0) // Force invalid member removal - && ($membercount == 1)
			&& ((($userid == $index_activeuserid) && ($userid != $ownerid)) /* Non-owner member leaving */
			|| (($ownerid == $index_activeuserid) && ($userid != $ownerid)))) { /* Owner kicking member out. */
		$ajaxarray['checkblock'] = "User($userid) left group($postid) with joinrequest($joinrequest) ownerid($ownerid)";
		$query = sprintf('delete from members where accountid = %d and postid = %d limit 1;', $userid, $postid);
		$ajaxarray['deletequery'] = $query;
		$result = func_php_query($query);
		$ajaxarray['joinrequest'] = 1;
		$ajaxarray['linktext'] = 'Join Group';
		$ajaxarray['error'] = 'INFO_LEAVE_GROUP_COMPLETED';
	/* Close group if owner is leaving. */
	} else if ((joinrequest == 0) && ($membercount == 1) && ($userid == $index_activeuserid) && ($userid == $ownerid)) {
		$query = sprintf('delete from members where postid = %d;', $postid);
		func_php_query($query);
		$ajaxarray['joinrequest'] = 1;
		$ajaxarray['linktext'] = 'Set As Group';
		$ajaxarray['error'] = 'INFO_CLOSE_GROUP_COMPLETED';
		return $ajaxarray;
	} else {
		$ajaxarray['error'] = 'ERROR_JOIN_GROUP_INVALID_COMBINATION_OR_ALREADY_MEMBER';
		return $ajaxarray;
	}

	if ($membercount == 1) {
		$query = sprintf('update members set trusted = %d where postid = %d and accountid = %d limit 1;',
				$trusted, $postid, $userid);
	} else {
		$query = sprintf('insert into members (accountid, postid, trusted) values (%d, %d, %d);',
				$userid, $postid, $trusted);
	}
	$result = func_php_query($query);

	/* Get updated member/pending count. */
	$query = sprintf('select (select count(*) from members where members.postid = posts.id'
			.' and members.trusted = 1) as trustedmembercount,'
			.' (select count(*) from members where members.postid = posts.id'
			.' and members.trusted != 1) as pendingmembercount'
			.' from posts where posts.id=%d;', $postid);
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);
	$trustedmembercount = $r['trustedmembercount'];
	$pendingmembercount = $r['pendingmembercount'];
	$ajaxarray['trustedmembercount'] = $trustedmembercount;

	$ajaxarray['membercounttext'] = '';
	if ($trustedmembercount > 0) {
		$ajaxarray['membercounttext'] = "Members: ($trustedmembercount)";
	}

	/* Only the owner/moderator(s) can see pending member count */
	if ($index_activeuserid == $ownerid) {
		$ajaxarray['pendingmembercount'] = $pendingmembercount;
		$memberspendingtext = '';
		if ($pendingmembercount) {
			$memberspendingtext = "&bull; {$pendingmembercount} pending ";
		}
		$ajaxarray['memberspendingtext'] = $memberspendingtext;
	}

	if ($ajaxarray['error'] == 'INFO_LEAVE_GROUP_COMPLETED')
		return $ajaxarray;

	$ajaxarray['grouplistentry'] = func_php_insertPostLink($phylum, $postid, false,
			"grouplistentry_{$postid}", false, $index_activeuserid);

	$postmember = func_php_insertMemberLink($username, true, false,
			"{$phylum}memberentrylink_{$postid}_{$userid}",
			$postid, $phylum, 0, $index_activeuserid);
	if ($postmember) {
		$ajaxarray['groupmembersentry'] = $postmember;
	}
	$ajaxarray['joinrequest'] = 0;
	if ($trusted) {
		if ($userid == $ownerid) {
			$ajaxarray['linktext'] = 'Close Group';
		} else if ($index_activeuserid != $ownerid) {
			$ajaxarray['linktext'] = 'Leave Group';
		} else {
			$ajaxarray['linktext'] = '';
		}
		$ajaxarray['error'] = 'INFO_JOIN_GROUP_REQUEST_APPROVED';
	} else {
		$ajaxarray['linktext'] = 'Cancel Join Request';
		$ajaxarray['error'] = 'INFO_JOIN_GROUP_REQUEST_SENT';
	}

	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_upload_files()
{
	global $mysqli;
	global $index_activeusername, $index_activeuserid;
	global $index_activeuserfileuse, $index_activeuserfilespace;
	global $index_activeuserfilesize, $index_activeuserpostsize;
	global $s3profile, $s3endpoint, $s3region, $s3bucket;

	$ajaxarray = array();
	$ajaxarray['post-and-files'] = print_r($_POST, true)." * * * ".print_r($_FILES, true);
	$ajaxarray['error'] = 'INFO_IMAGE_UPLOAD_SUCCESS'; /* Will be overwritten by errors. */

	/* Phylum and postid are used to identify with div to attach to,
	 * in case a post and a message with the same postid are visible.
	 * Uplodimageposteditor is used to distinguish between the messaging
	 * popup editor or the post editor. */
	$phylum = func_php_cleantext($_POST['uploadimagephylum'], 64);
	$postidtext = func_php_cleantext($_POST['uploadimagepostid'], 64);
	$postid = 0;
	if ($postidtext != 'NEWPOST') {
		$postid = func_php_inttext($_POST['uploadimagepostid']);
	}
	$posteditor = func_php_cleantext($_POST['uploadimageposteditor'], 64);
	$uploadmode = func_php_cleantext($_POST['uploadimagefileuploadmode'], 64);
	$newpostsessionid = 0;
	if ($postid > 0) {
		$permissions = func_php_getUserPermissions($postid, $index_activeuserid);
		if (!$permissions) {
			$ajaxarray['error'] = 'ERROR_IMAGE_UPLOAD_INVALID_POST:'
					."postid={$postid},index_activeuserid={$index_activeuserid};";
			return $ajaxarray;
		} else if (!$permissions['canview']) {
			$ajaxarray['error'] = 'ERROR_IMAGE_UPLOAD_POST_PERMISSIONS';
			return $ajaxarray;
		}
	} else if (isset($_POST['uploadimagenewpostsessionid'])) {
		$newpostsessionid = func_php_inttext($_POST['uploadimagenewpostsessionid']);
	}

	$index_activeuserfileuse = index_php_getuserfileuse(FALSE, FALSE);
	$i = 0;
	$awsloaded = false;
	foreach ($_FILES['uploadfile']['error'] as $fileerror) {
		$sumfilesize = index_php_getuserfileuse($postid, $newpostsessionid);

		$uploadfilename = func_php_cleantext($_FILES['uploadfile']['name'][$i], 65535);
		$uploadfilesize = func_php_cleantext($_FILES['uploadfile']['size'][$i], 256);
		$uploadfiletmpname = func_php_cleantext($_FILES['uploadfile']['tmp_name'][$i], 65535);
		$uploadfiletempid = func_php_cleantext($_POST['uploadfiletempid'][$i], 256);

		/* Limit file upload sizes */
		if ($uploadfilesize > $index_activeuserfilesize) {
			$fileerror = 1;
		}

		/* Limit post uploads size */
		if (($sumfilesize + $uploadfilesize) > $index_activeuserpostsize) {
			$fileerror = 2;
		}

		/* Check for available free space */
		if (($index_activeuserfileuse + $uploadfilesize) > $index_activeuserfilespace) {
			$fileerror = 3;
		}

		$ajaxarray['uploadedfile']['name'][$i] = $uploadfilename;
		$ajaxarray['uploadedfile']['urlname'][$i] = rawurlencode($uploadfilename);
		$ajaxarray['uploadedfile']['error'][$i] = $fileerror;
		$ajaxarray['uploadedfile']['size'][$i] = func_php_fuzzyfilesize($uploadfilesize);
		$ajaxarray['uploadedfile']['tempid'][$i] = $uploadfiletempid;

		if ($fileerror) {
			$i++;
			continue;
		}

		/* TODO: Change the following lines to a function. They're reused in index_php_ajax_save_profile_information() */
		$query = sprintf('insert into files(accountid, filename, added) values (%d, "%s", NOW());',
				$index_activeuserid, func_php_escape($uploadfilename));
		func_php_query($query);
		$ajaxarray['filesinsertquery'] = $query;
		$fileid = mysqli_insert_id($mysqli);

		/* TODO: Change the following lines to a function.
		 * They're reused in index_php_ajax_save_profile_information_photo() */
		/* Using getimagesize to determine the MIME type is a work around
		 * for when the exif_imagetype() function is not available. */
		ini_set('user_agent', 'Mozilla/4.0 (compatible; MirrorIsland Previewer)');
		$imagesizearray = getimagesize($uploadfiletmpname);
		$width = $imagesizearray[0];
		$height = $imagesizearray[1];
		$mime = $imagesizearray['mime'];

		$filesize = filesize($uploadfiletmpname);

		/* TODO: Generate a thumbnail? */

		$filemime = '';
		$tinyimgdataURI = '';
		$tinyimgsrc = func_php_filetogdimage($uploadfiletmpname, $filemime);

		/* Generate a tiny image for lazy loading */
		if ($tinyimgsrc) {
			$tinywd = 8;
			$tinyht = 8;
			if ($width > $height) {
				$tinyht = ceil(($height / $width) * 8);
			} else {
				$tinywd = ceil(($width / $height) * 8);
			}
			$tinyimg = imagecreatetruecolor($tinywd, $tinyht);
			imagecopyresampled($tinyimg, $tinyimgsrc, 0, 0, 0, 0, $tinywd, $tinyht, $width, $height);
			imagetruecolortopalette($tinyimg, false, 16);

			$tinyimgdata = '';
			ob_start();
			imagepng($tinyimg, null, 9, PNG_NO_FILTER);
			$tinyimgdata = ob_get_contents();
			ob_end_clean();

			$tinyimgdataURI = 'data:image/png;base64,'.base64_encode($tinyimgdata);
		}

		$fileext = func_php_mimeext('.', $filemime);
		$fileindex = $fileid.$fileext;

		/* Find out if saving to S3 bucket, or locally otherwise */
		$uploadsuccess = false;
		if ($s3profile && $s3endpoint && $s3region && $s3bucket) {
			/* Use cloud storage (S3 bucket) */
			if (!$awsloaded) {
				require 'aws/aws-autoloader.php';
				$awsloaded = true;
			}

			$sdk = new Aws\Sdk([
				'profile' => $s3profile,
				'endpoint' => $s3endpoint,
				'region' => $s3region,
				'version' => 'latest',
				'use_path_style_endpoint' => true,
			]);
			$s3 = $sdk->createS3();

			try {
				$result = $s3->putObject([
					'Bucket' => $s3bucket,
					'Key' => $fileindex,
					'SourceFile' => $uploadfiletmpname,
				]);
				$uploadsuccess = true;
				$query = sprintf('update files'
						.' set s3profile="%s", s3endpoint="%s",' /* s3profile, s3endpoint */
						.' s3region="%s", s3bucket="%s"' /* s3region, s3bucket */
						.' where id=%d limit 1;', /* id */
						$s3profile, $s3endpoint, $s3region, $s3bucket, $fileid);
				func_php_query($query);
			} catch (S3Exception $e) {
				error_log($e->getMessage());
			}
		} else {
			/* Save on web host storage */
			$uploadsuccess = move_uploaded_file($uploadfiletmpname,
					'files/'.$fileindex);
			if ($uploadsuccess) {
				chmod('files/'.$fileindex, 0666);
			}
		}

		if ($uploadsuccess) {
			$query = sprintf('update files set filesize=%d, filewidth=%d, fileheight=%d,'
					.' filemime="%s", fileindex="%s", tinyimg="%s",'
					.' newpostsessionid=%d'
					.' where id=%d limit 1;', $filesize, $width, $height,
					func_php_escape($filemime), func_php_escape($fileindex),
					func_php_escape($tinyimgdataURI), $newpostsessionid, $fileid);
			func_php_query($query);

			index_php_indexfileforsearch($fileid, true);

			$ajaxarray['uploadedfile']['fileid'][$i] = $fileid;
			$ajaxarray['uploadedfile']['fileindex'][$i] = $fileindex;
			$ajaxarray['uploadedfile']['mime'][$i] = $filemime;
			$ajaxarray['uploadedfile']['width'][$i] = $width;
			$ajaxarray['uploadedfile']['height'][$i] = $height;
			$ajaxarray['uploadedfile']['tinyimg'][$i] = $tinyimgdataURI;
		} else {
			$query = sprintf('delete from files where id = %d limit 1;',
					$fileid);
			func_php_query($query);
		}

		$i++;
	}
	$index_activeuserfileuse = index_php_getuserfileuse(FALSE, FALSE);
	$sumfilesize = index_php_getuserfileuse($postid, $newpostsessionid);
	$filespace = $index_activeuserfilespace - $index_activeuserfileuse;

	if ($postidtext == 'NEWPOST')
		$postid = $postidtext;

	$ajaxarray['uploadimagefilespace'] = $filespace;
	$ajaxarray['uploadimagefilespacetext'] = func_php_fuzzyfilesize($filespace).' available';
	$ajaxarray['uploadimagefileuse'] = $index_activeuserfilesize;
	$ajaxarray['uploadimagepostsize'] = $sumfilesize;
	$ajaxarray['uploadimagephylum'] = $phylum;
	$ajaxarray['uploadimagepostid'] = $postid;
	$ajaxarray['uploadimageposteditor'] = $posteditor;
	$ajaxarray['uploadimagefileuploadmode'] = $uploadmode;

	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_get_older_posts()
{
	$ajaxarray = array();

	$earliestpostdate = func_php_cleantext($_POST['earliestpostdate'], 255);
	$parentpostid = func_php_cleantext($_POST['postid'], 255);
	$query = 'select * from posts';
	//* TODO: Fix invalid date-checking.
	//if (date_create_from_format('Y-m-d H:i:s', $latestpostdate))
		$query .= ' where added < "$earliestpostdate"';
		// */
	$query .= ' order by added desc';
	$result = func_php_query($query);
	if (mysqli_num_rows($result) > 0) {
		$ajaxarray['posts'] = array();
		$i = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			$r['description'] .= " $query".print_r($_POST, True);;
			$ajaxarray['posts'][$i] = $r;
			$i++;
		}
		$ajaxarray['error'] = 'INFO_EARLIER_POSTS_FOUND';
	} else {
		$ajaxarray['error'] = 'INFO_NO_EARLIER_POSTS_FOUND';
	}

	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_get_newer_posts()
{
	$ajaxarray = array();

	$latestpostdate = func_php_cleantext($_POST['latestpostdate'], 255);
	$parentpostid = func_php_cleantext($_POST['postid'], 255);
	$query = 'select * from posts';
	//* TODO: Fix invalid date checking.
	//if (date_create_from_format('Y-m-d H:i:s', $latestpostdate))
		$query .= ' where added > "$latestpostdate"';
		// */
	$query .= ' order by added desc';
	$result = func_php_query($query);
	if (mysqli_num_rows($result) > 0) {
		$ajaxarray['posts'] = array();
		$i = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			$r['description'] .= ' $query'.print_r($_POST, True);;
			$ajaxarray['posts'][$i] = $r;
			$i++;
		}
		$ajaxarray['error'] = 'INFO_NEWER_POSTS_FOUND';
	} else {
		$ajaxarray['error'] = 'INFO_NO_NEWER_POSTS_FOUND';
	}

	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_forgot_password()
{
	global $mysqli;
	global $servertitle, $servername, $urlhome;
	$ajaxarray = array();

	$ajaxarray['error'] = 'ERROR_FORGOT_PASSWORD_INCOMPLETE';
	$username = func_php_cleantext($_POST['forgotpasswordusername'], 255);

	$query = sprintf('select id, username, fullname, accountemail from accounts'
			.' where username="%s" limit 1', /* USERNAME */
			func_php_escape($username));
	$result = func_php_query($query);
	if (!mysqli_num_rows($result)) {
		$ajaxarray['error'] = 'ERROR_FORGOT_PASSWORD_INVALID_USERNAME';
		return $ajaxarray;
	}
	$r = mysqli_fetch_assoc($result);
	$accountemail = $r['accountemail'] ?? '';
	if (!$accountemail) {
		$ajaxarray['error'] = 'ERROR_FORGOT_PASSWORD_NO_ACCOUNT_EMAIL';
		return $ajaxarray;
	}

	$id = $r['id'];
	$username = $r['username'];
	$fullname = $r['fullname'] ?? $username;

	$resetid = '';
	for ($i = 0; $i < 64; $i++) {
		$resetid .= substr('1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
			mt_rand(0, 61), 1);
	}
	$query = sprintf('update accounts'
			.' set resetlinkid="%s", resetlinkadded=NOW()' /* RESETID */
			.' where id=%d limit 1;', /* USERID */
			func_php_escape($resetid), $id);
	$result = func_php_query($query);

	/* Generate the plaintext and HTML password reset email messages. */
	$resetlink = "{$urlhome}?reset={$resetid}";

	$plaintext = <<<PLAINTEXT
$servertitle;

Somebody has asked to reset the password for the account
$fullname ($username) on $servertitle.

To change it, visit:
$resetlink

Your password has not been changed yet.

If you do not need to change your password, you can ignore this message.
PLAINTEXT;

	$html = <<<HTML
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>$servertitle - Password Reset</title>
</head>
<body>
<p>Somebody has asked to reset the password for the account:
	<br>$fullname ($username) on $servertitle.</p>
<p><a href="$resetlink">Reset Password</a></p>
<p>Your password has not been changed yet.</p>
<p>If you do not need to change your password, you can ignore this message.</p>
</body>
</html>
HTML;

	$from = "{$servername} <no-reply@{$servername}>"; /* "no-reply" can be changed in the future */
	$to = $accountemail;
	$subject = "{$servertitle} - Password reset";
	$boundary = uniqid('boundary_');

	$headers = "From: {$from}\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: multipart/alternative;\r\n";
	$headers .= " boundary=\"{$boundary}\";\r\n";
	$headers .= " charset=UTF-8\r\n";
	$headers .= "X-Mailer: PHP/".phpversion()."\r\n";

	$message = "--{$boundary}\r\n";
	$message .= "Content-Transfer-Encoding: base64\r\n";
	$message .= "Content-type: text/plain; charset=UTF-8\r\n";
	$message .= "\r\n";
	$message .= chunk_split(base64_encode($plaintext));
	$message .= "\r\n\r\n";

	$message .= "--{$boundary}\r\n";
	$message .= "Content-Transfer-Encoding: base64\r\n";
	$message .= "Content-type: text/html; charset=UTF-8\r\n";
	$message .= "\r\n";
	$message .= chunk_split(base64_encode($html));
	$message .= "\r\n\r\n";

	$message .= "--{$boundary}--\r\n"; /* Append '--' for closing boundary */
	/* $message = quoted_printable_encode($message); */

	if (mail($to, $subject, $message, $headers)) {
		$ajaxarray['error'] = "INFO_FORGOT_PASSWORD_EMAIL_SENT";
	} else {
		$ajaxarray['error'] = "ERROR_FORGOT_PASSWORD_EMAIL_NOT_SENT";
	}

	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_sign_in()
{
	global $mysqli;
	$ajaxarray = array();

	$username = func_php_cleantext($_POST['signinusername'], 255);
	$password = func_php_cleantext($_POST['signinpassword'], 255);

	$ajaxarray['post'] = print_r($_POST, true);
	$ajaxarray['username'] = $username;

	$query = sprintf('select id from accounts where username="%s" limit 1',
			func_php_escape($username));
	$result = func_php_query($query);
	if (mysqli_num_rows($result) != 1) {
		$ajaxarray['error'] = 'ERROR_SIGN_IN_INVALID_USERNAME';
		return $ajaxarray;
	}

	$userid = index_php_verifypassword($username, $password);
	if (!$userid) {
		$ajaxarray['error'] = 'ERROR_SIGN_IN_INVALID_PASSWORD';
		return $ajaxarray;
	}

	$responsearray = array();
	$sessionid = '';
	for ($i = 0; $i < 64; $i++) {
		$sessionid .= substr('1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
			mt_rand(0, 61), 1);
	}
	$query = sprintf('update accounts set sessionid="%s" where username="%s" limit 1;',
			func_php_escape($sessionid), func_php_escape($username));
	$result = func_php_query($query);

	/* Return location on success */
	$ref = func_php_cleantext($_POST['ref'], 255);

	/* echo "I_SIGN_IN"; */
	$ajaxarray['linenumber'] = __LINE__;
	$ajaxarray['sessionid'] = $sessionid;
	$ajaxarray['username'] = $username;
	$ajaxarray['userid'] = $userid;
	$ajaxarray['windowlocationhref'] = $ref;
	$ajaxarray['error'] = 'INFO_SIGN_IN_SUCCESS';
	$ajaxarray['server'] = print_r($_SERVER, true);

	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_sign_up()
{
	global $mysqli;
	$ajaxarray = array();

	$username = func_php_cleantext($_POST['signupusername'] ?? '', 255);
	$password = func_php_cleantext($_POST['signuppassword'] ?? '', 255);
	$email = func_php_cleantext($_POST['signupemail'] ?? '', 255);
	$fullname = func_php_cleantext($_POST['signupfullname'] ?? '', 255);

	/* Error-check and format input. */
	if (!$username || !$password) {
		$ajaxarray['error'] = 'ERROR_SIGNUP_FORM_INCOMPLETE';
		return $ajaxarray;
	}

	if (strlen($password) < 6) {
		$ajaxarray['error'] = 'ERROR_SIGNUP_INVALID_PASSWORD';
		return $ajaxarray;
	}

	if ((preg_match('/^[A-Za-z0-9]{6,64}$/', $username) != 1)
			|| (preg_match('/[A-Za-z]+/', $username) == 0)) {
		$ajaxarray['error'] = 'ERROR_SIGNUP_USERNAME_INVALID';
		return $ajaxarray;
	}

	$query = sprintf('select count(accounts.username) from accounts'
			.' where accounts.username="%s" limit 1;', func_php_escape($username));
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);
	if ($r['count(accounts.username)'] != 0) {
		$ajaxarray['selectcountusername'] = $query;
		$ajaxarray['error'] = 'ERROR_SIGNUP_USERNAME_UNAVAILABLE';
		return $ajaxarray;
	}

	/* Only proceed if all fields have been successfully entered. */
	$terms = file_get_contents('./terms.md');
	if ($terms === FALSE) {
		$terms = 'ERROR:UNABLE TO SAVE TERMS';
	}

	$sessionid = '';
	for ($i = 0; $i < 64; $i++) {
		$sessionid .= substr('1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
			mt_rand(0, 61), 1);
	}

	$filespace = mibibyte * 100;
	$filesize = mibibyte * 10; /* Also used for post size */
	$query = sprintf('insert into accounts '
			.' (fullname, username, termsofuse, termsofusedate,'
			.' viewpermissions, editpermissions, replypermissions,'
			.' sessionid, added, filespace, filesize, postsize,'
			.' sidepanelvisible, onlinestatus)'
			.' values ("%s", "%s", "%s", now(), 5, 0, 4,'
			.' "%s", now(), %d, %d, %d, 1, 12);',
			func_php_escape($fullname), func_php_escape($username),
			func_php_escape($terms), func_php_escape($sessionid),
			$filespace, $filesize, $filesize);
	$result = func_php_query($query);

	/* Preserve account id for next few queries. */
	$lastinsertid = mysqli_insert_id($mysqli);

	/* Add the account management email, if one given */
	if ($email) {
		$query = sprintf('update accounts set accountemail="%s"' /* EMAIL */
				.' where id=%d limit 1', /* ACCTID */
				func_php_escape($email), $lastinsertid);
		func_php_query($query);
	}

	index_php_indexprofileforsearch($lastinsertid, FALSE);

	/* The password belongs in a separate table. */
	$passwordsalt = "$2y$13$";
	for ($i = 0; $i < 22; $i++) {
		$passwordsalt .= substr('1234567890/abcdefghijklmnopqrstuvwxyz.ABCDEFGHIJKLMNOPQRSTUVWXYZ',
				mt_rand(0, 63), 1);
	}
	$passwordhash = crypt($password, $passwordsalt);
	$query = sprintf('insert into passwords (accountid, password) values (%d, "%s");',
			$lastinsertid, func_php_escape($passwordhash));
	$result = func_php_query($query);

	/* Return location on success */
	$ref = func_php_cleantext($_POST['ref'], 255);

	$ajaxarray['fullname'] = $fullname;
	$ajaxarray['username'] = $username;
	$ajaxarray['sessionid'] = $sessionid;
	$ajaxarray['windowlocationhref'] = $ref;
	$ajaxarray['error'] = 'INFO_SIGNED_UP';
	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_sign_out()
{
	global $mysqli;
	$ajaxarray = array();

	$username = func_php_cleantext($_POST['signoutusername'], 255);
	$sessionid = func_php_cleantext($_POST['signoutsessionid'], 255);

	/* Check if we're dealing with an actual member! */
	$query = sprintf('select count(*) from accounts where username="%s" '
			.'and sessionid="%s" limit 1;', func_php_escape($username),
			func_php_escape($sessionid));
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);
	if ($r['count(*)'] != 1) {
		$ajaxarray['error'] = 'ERROR_INVALID_SIGN_OUT_DETAILS';
		$ajaxarray['query'] = $query;
		return $ajaxarray;
	}

	$query = sprintf('update accounts set sessionid=NULL '
			.'where sessionid="%s" and username="%s" limit 1;',
			func_php_escape($sessionid), func_php_escape($username));
	$result = func_php_query($query);
	$ajaxarray['windowlocationhref'] = $_SERVER['HTTP_REFERER'];
	$ajaxarray['error'] = 'INFO_SIGN_OUT_SUCCESS';

	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_add_contact()
{
	global $mysqli;
	global $index_activeusername, $index_activeuserid;
	$ajaxarray = array();
	$username = func_php_cleantext($_POST['username'], 255);
	$trusted = func_php_inttext($_POST['trusted']);
	$ajaxarray['post'] = print_r($_POST, true);

	if (strtolower($index_activeusername) == strtolower($username)) {
		$ajaxarray['error'] = 'ERROR_CAN_NOT_ADD_OWN_USERNAME_TO_CONTACTS';
		return $ajaxarray;
	}

	$query = sprintf('select id from accounts where username="%s" limit 1',
			func_php_escape($username));
	$result = func_php_query($query);
	if (mysqli_num_rows($result) != 1) {
		$ajaxarray['error'] = 'ERROR_ADD_CONTACT_USERNAME_INVALID';
		return $ajaxarray;
	}

	$r = mysqli_fetch_assoc($result);
	$contactid = $r['id'];
	$query = sprintf('select * from contacts where accountid=%d'
			.' and contactid=%d limit 1;', $index_activeuserid, $contactid);
	$result = func_php_query($query);
	if (mysqli_num_rows($result) == 1) {
		$query = sprintf('update contacts set trusted = %d where accountid = %d'
				.' and contactid=%d limit 1;', $trusted, $index_activeuserid, $contactid);
		$ajaxarray['updatequery'] = $query;
		$result = func_php_query($query);
		$ajaxarray['error'] = 'INFO_CONTACT_UPDATED';
		return $ajaxarray;
	}

	$query = sprintf('insert into contacts (accountid, contactid, trusted) values (%d, %d, %d);',
			$index_activeuserid, $contactid, $trusted);
	$ajaxarray['insertquery'] = $query;
	$result = func_php_query($query);

	$ajaxarray['contactlistentry'] = func_php_insertMemberLink($username, false, true,
			"contactlistentry_{$username}", 0, '', 0, $index_activeuserid);
	$ajaxarray['error'] = 'INFO_CONTACT_ADDED';

	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_remove_contact()
{
	global $mysqli;
	global $index_activeusername, $index_activeuserid;
	$ajaxarray = array();
	$username = func_php_cleantext($_POST['username'], 255);

	/* XXX Should this be kept? Things /can/ go strange...
	if (strtolower($index_activeusername) == strtolower($username)) {
		$ajaxarray['error'] = 'ERROR_REMOVE_CONTACT_USERNAME_INVALID';
		return $ajaxarray;
	}
	// */

	$query = sprintf('select id from accounts where username="%s" limit 1',
			func_php_escape($username));
	$result = func_php_query($query);
	if (mysqli_num_rows($result) != 1) {
		$ajaxarray['error'] = 'ERROR_REMOVE_CONTACT_USERNAME_INVALID';
		return $ajaxarray;
	}

	$r = mysqli_fetch_assoc($result);
	$contactid = $r['id'];
	$query = sprintf('select * from contacts where contactid=%d and accountid=%d limit 1;',
			$contactid, $index_activeuserid);
	$result = func_php_query($query);
	if (mysqli_num_rows($result) == 0) {
		$ajaxarray['error'] = 'ERROR_USERNAME_NOT_IN_CONTACT_LIST';
		return $ajaxarray;
	}

	$query = sprintf('delete from contacts where accountid=%d'
			.' and contactid=%d limit 1', $index_activeuserid, $contactid);
	$ajaxarray['removequery'] = $query;
	$result = func_php_query($query);

	$ajaxarray['username'] = $index_activeusername;
	$ajaxarray['contactusername'] = $username;
	$ajaxarray['error'] = 'INFO_CONTACT_REMOVED';

	return $ajaxarray;
}

function index_php_ajax_get_updates()
{
	global $index_activeusername, $index_activeuserid;
	$ajaxarray = array();
	$ajaxarray['error'] = 'INFO_NO_UPDATES_AVAILABLE';

	$query = sprintf('/* '.__LINE__.' */ select accounts.username, accounts.fullname, contacts.trusted,'
			.' concat(contacts.viewpermissions, contacts.editpermissions,'
			.' contacts.replypermissions, contacts.approvepermissions,'
			.' contacts.moderatepermissions, contacts.tagpermissions) as permissions'
			.' from contacts, accounts'
			.' where contacts.accountid=%d and accounts.id=contacts.contactid;',
			$index_activeuserid);
	$ajaxarray['contactsquery'] = $query;
	$result = func_php_query($query);
	$onlineusercount = 0; /* Later, represent online user count. */
	if (mysqli_num_rows($result) > 0) {
		$ajaxarray['error'] = 'INFO_UPDATES_AVAILABLE';
		$i = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			//$onlineusercount++; /* Later, represent actual available online status. */
			$ajaxarray['contact'][$i]['username'] = $r['username'];
			$ajaxarray['contact'][$i]['fullname'] = $r['fullname'];
			$ajaxarray['contact'][$i]['permissions'] = $r['permissions'];
			$ajaxarray['contact'][$i]['trusted'] = $r['trusted'];
			$ajaxarray['contact'][$i]['contactlistentry'] = func_php_insertMemberLink($r['username'],
					false, true, "contactlistentry_{$r['username']}", 0, '', 0, $index_activeuserid);
			$i++;
		}
	}
	$ajaxarray['onlineusercount'] = $onlineusercount;

	/* XXX: Can this be merged with index_php_ajax_get_messages() ? */

	$query = sprintf('/* '.__LINE__.' */ select (select posts.id from posts'
			.' inner join recipients as postrecipient on postrecipient.tableid = posts.id'
			.' where postrecipient.tablename = "posts"'
			.' and postrecipient.recipientaccountid = recipients.recipientaccountid'
			.' and postrecipient.senderaccountid = recipients.senderaccountid'
			.' order by posts.added asc limit 1) as id,'
			.' sa.username, NULL as postid, lower(sa.username) as sender,'
			.' (select posts.description from posts'
			.' 	inner join recipients as postrecipient on postrecipient.tableid = posts.id'
			.' 	where postrecipient.tablename = "posts"'
			.' 	and postrecipient.recipientaccountid = recipients.recipientaccountid'
			.' 	and postrecipient.senderaccountid = recipients.senderaccountid'
			.' 	order by posts.added asc limit 1) as description,'
			.' (select count(*) from posts as ucp inner join recipients as ucr on ucp.id = ucr.tableid'
			.' 	where ucr.tablename = "posts" and ucr.senderaccountid = recipients.senderaccountid'
			.' 	and ucr.recipientaccountid = recipients.recipientaccountid'
			.' 	and ucp.viewcount = 0) as unreadcount'
			.' from recipients inner join accounts ra on recipients.recipientaccountid = ra.id'
			.' inner join accounts sa on recipients.senderaccountid = sa.id where ra.username = "%s"' /* USERNAME */
			.' UNION select p.id, NULL as username, p.id as postid,'
			.' if(p.emailusername is NULL or p.emailserver is NULL, p.id, lower(concat(p.emailusername, "@", p.emailserver))) as sender,'
			.' p.description,'
			.' (select count(*) from posts as ucp inner join recipients as ucr on ucp.id = ucr.tableid'
			.' 	where ucr.tablename = "posts" and ucr.recipientaccountid = a.id and ucp.viewcount = 0'
			.' 	and ((p.accountid = ucr.senderaccountid and p.accountid != 0)'
			.' 	or (p.emailusername = ucp.emailusername and p.emailserver = ucp.emailserver)'
			.' 	or (ucp.accountid = 0 and ucp.emailserver is NULL and ucp.id = p.id))) as unreadcount'
			.' from recipients inner join accounts a on recipients.recipientaccountid = a.id'
			.' inner join posts p on recipients.tableid = p.id'
			.' where a.username = "%s" and recipients.tablename = "posts"' /* USERNAME */
			.' and recipients.senderaccountid = 0 group by sender;',
			$index_activeusername, $index_activeusername);
	$ajaxarray['unreadquery'] = $query;
	$result = func_php_query($query);
	$unreadcount = 0; /* Unread message count. */
	if (mysqli_num_rows($result) > 0) {
		$ajaxarray['error'] = 'INFO_UPDATES_AVAILABLE';
		//$ajaxarray['message'] = array();
		$i = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			$unreadcount += intval($r['unreadcount']);
			$elementid = "contactlistentry_{$r['sender']}";

			$ajaxarray['unread'][$i] = $r;
			$ajaxarray['unread'][$i]['contactlistentry'] = func_php_insertMemberLink($r['username'],
					false, true, $elementid, 0, '', $r['postid'], $index_activeuserid);
			$i++;
		}
	}
	$ajaxarray['unreadcount'] = $unreadcount;

	/* Get groups */
	$query = sprintf('select posts.id, members.trusted, posts.accountid as ownerid,'
			.' substring(posts.searchcache, 1, 250) as searchcache'
			.' from members left join posts on members.postid = posts.id'
			.' where members.accountid = %d', /* USERID */
			$index_activeuserid);
	$result = func_php_query($query);
	if (mysqli_num_rows($result) > 0) {
		$ajaxarray['error'] = 'INFO_UPDATES_AVAILABLE';
		$i = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			$postid = $r['id'];
			$label = func_php_cleantext($r['searchcache'], 80);

			$ajaxarray['group'][$i] = $r;
			$ajaxarray['group'][$i]['grouplistentry'] = func_php_insertPostLink('post', $postid, false,
					"grouplistentry_{$postid}", false, $index_activeuserid);
			$i++;
		}
	}

	return $ajaxarray;
}

function index_php_ajax_get_messages()
{
	global $mysqli;
	global $index_activeusername, $index_activeuserid;
	global $servername, $urlhome;
	$ajaxarray = array();
	$ajaxarray['error'] = 'ERROR_MESSAGING_UNAVAILABLE';

	$contactusername = func_php_cleantext($_POST['contactusername'], 256);
	$postid = func_php_inttext($_POST['postid']);
	$contactserver = false;
	$contactemail = false;
	if (preg_match('/^([^@]+)@(.+)$/', $contactusername, $matches)) {
		$contactemail = $matches[0];
		$contactusername = $matches[1];
		$contactserver = $matches[2];
		$postid = false;
	}

	$query = '';
	if ($contactusername && $contactserver) {
		$ajaxarray['recipientusername'] = $contactemail;
		$ajaxarray['recipientfullname'] = $contactemail;
		$ajaxarray['recipientemail'] = "{$contactusername}@{$contactserver}";
		$ajaxarray['statusimgsrc'] = '/img/status-email12.png';

		$query = sprintf('/* '.__LINE__.' */ select posts.*, accounts.username, accounts.fullname,'
				.' concat(posts.viewpermissions, posts.editpermissions, posts.replypermissions,'
				.' posts.approvepermissions, posts.moderatepermissions, posts.tagpermissions) as permissions,'
				.' (select group_concat(distinct concat_ws("", accounts.username, recipients.emailusername,'
				.'		if (recipients.emailserver is not NULL, "@", ""),'
				.' 		recipients.emailserver) separator " ")'
				.' 	from recipients left join accounts on accounts.id = recipients.recipientaccountid'
				.' 	where recipients.tablename = "posts" and recipients.tableid = posts.id'
				.'	and (recipients.senderaccountid != recipients.recipientaccountid'
				.' 	or recipients.recipientaccountid is NULL)) as recipients,'
				.' (select count(*) from members where members.postid = posts.id'
				.' 	and members.trusted = 1) as trustedmembercount,'
				.' (select count(*) from members where members.postid = posts.id'
				.' 	and members.trusted != 1) as pendingmembercount'
				.' from posts left join accounts on posts.accountid = accounts.id'
				.' left join recipients on posts.id = recipients.tableid'
				.' left join membertree on posts.id = membertree.postid'
				.' where recipients.tablename = "posts"'
				.' and ((posts.emailusername = "%s" and posts.emailserver = "%s" and recipients.recipientaccountid = %d)' /* EMAILUSER, EMAILSERVER, USERID */
				.' or (recipients.emailusername = "%s" and recipients.emailserver = "%s" and posts.accountid = %d))', /* EMAILUSER, EMAILSERVER, USERID */
				$contactusername, $contactserver, $index_activeuserid,
				$contactusername, $contactserver, $index_activeuserid);
	} else if ($contactusername && !$postid) {
		$query = sprintf('select username, fullname from accounts where username="%s" limit 1',
				func_php_escape($contactusername));
		$ajaxarray['selectquery'] = $query;
		$result = func_php_query($query);
		if (mysqli_num_rows($result) != 1) {
			$ajaxarray['error'] = 'ERROR_MESSAGING_INVALID_RECIPIENT';
			return $ajaxarray;
		}
		$r = mysqli_fetch_assoc($result);
		$ajaxarray['recipientusername'] = $r['username'];
		$ajaxarray['recipientfullname'] = $r['fullname'];
		$ajaxarray['recipientemail'] = "{$r['username']}@{$servername}";
		$ajaxarray['statusimgsrc'] = '/img/status-offline12.png'; /* TODO: display actual online status. */
		$query = sprintf('/* '.__LINE__.' */ select distinct posts.*, accounts.username, accounts.fullname,'
				.' concat(posts.viewpermissions, posts.editpermissions, posts.replypermissions,'
				.' posts.approvepermissions, posts.moderatepermissions, posts.tagpermissions) as permissions,'
				.' (select group_concat(distinct accounts.username separator " ") from accounts, recipients'
				.' 	where accounts.id = recipients.recipientaccountid and recipients.tablename = "posts"'
				.' 	and recipients.tableid = posts.id) as recipients,'
				.' (select count(*) from members where members.postid = posts.id'
				.' 	and members.trusted = 1) as trustedmembercount,'
				.' (select count(*) from members where members.postid = posts.id'
				.' 	and members.trusted != 1) as pendingmembercount'
				.' from posts, accounts, recipients, accounts as useracct, accounts as contacct'
				.' where useracct.username="%s"'
				.' and contacct.username="%s" and posts.accountid=accounts.id'
				.' and recipients.tablename="posts" and recipients.tableid=posts.id'
				.' and ((recipients.recipientaccountid=useracct.id and accounts.id=contacct.id)'
				.' or (recipients.recipientaccountid=contacct.id and accounts.id=useracct.id))',
				$index_activeusername, func_php_escape($contactusername));
	} else if ($postid > 0) {
		$query = sprintf('select posts.id, posts.emaildisplayname, posts.emailusername, posts.emailserver,'
				.' posts.parentid, pp.searchcache as parentsearchcache'
				.' from posts left join posts as pp on pp.id = posts.parentid'
				.' where posts.id=%d limit 1', $postid);
		$result = func_php_query($query);
		if (mysqli_num_rows($result) != 1) {
			$ajaxarray['error'] = 'ERROR_MESSAGING_INVALID_POST_ID';
			return $ajaxarray;
		}
		$r = mysqli_fetch_assoc($result);
		$ajaxarray['emailusername'] = $r['emailusername'];
		$ajaxarray['emailserver'] = $r['emailserver'];
		$ajaxarray['statusimgsrc'] = '/img/status-offline12.png';
		if ($r['emailusername'] && $r['emailserver']) {
			$ajaxarray['recipientemail'] = "{$r['emailusername']}@{$r['emailserver']}";
		} else {
			$ajaxarray['recipientemail'] = '';
		}

		/* Fall back to email address if no display name given */
		$recipientfullname = $r['emaildisplayname'];
		if (!$recipientfullname) {
			if ($r['emailusername'] && $r['emailserver']) {
				$recipientfullname = __LINE__.$r['emailusername'].'@'.$r['emailserver'];
			} else if ($r['parentid'] && $r['parentsearchcache']) {
				$parentsearchcache = substr($r['parentsearchcache'], 0, 40);
				$parentid = $r['parentid'];
				$recipientfullname = "Reply to <i>&quot;{$parentsearchcache}</i>&quot;";
			} else {
				$recipientfullname = 'Unknown sender';
			}
		}
		$ajaxarray['recipientfullname'] = $recipientfullname;

		$query = sprintf('/* '.__LINE__.' */ select posts.*, accounts.username, accounts.fullname,'
				.' concat(posts.viewpermissions, posts.editpermissions, posts.replypermissions,'
				.' posts.approvepermissions, posts.moderatepermissions, posts.tagpermissions) as permissions,'
				.' (select group_concat(distinct concat_ws("", accounts.username, recipients.emailusername,'
				.'		if (recipients.emailserver is not NULL, "@", ""),'
				.' 		recipients.emailserver) separator " ")'
				.' 	from recipients left join accounts on accounts.id = recipients.recipientaccountid'
				.' 	where recipients.tablename = "posts" and recipients.tableid = posts.id'
				.'	and (recipients.senderaccountid != recipients.recipientaccountid'
				.' 	or recipients.recipientaccountid is NULL)) as recipients,'
				.' (select count(*) from members where members.postid = posts.id'
				.' 	and members.trusted = 1) as trustedmembercount,'
				.' (select count(*) from members where members.postid = posts.id'
				.' 	and members.trusted != 1) as pendingmembercount'
				.' from posts left join accounts on posts.accountid = accounts.id'
				.' left join membertree on posts.id = membertree.postid, accounts as recipientaccount'
				.' where (posts.id = %d or membertree.parentid = %d)' /* POSTID, POSTID, USERNAME */
				.' and recipientaccount.username = "%s"',
				$postid, $postid, $index_activeusername);
	} else {
		$ajaxarray['error'] = 'ERROR_MESSAGING_UNAVAILABLE_INVALID_USERNAME_OR_POST_ID';
		return $ajaxarray; /* No username or postid? Error! */
	}

	/* Sort messages results. This will become user-configurable later on... */
	$query .= ' order by posts.added asc limit 100;';

	/* First fetch the actual messages, depending on which paramaters have been given. */
	$ajaxarray['getmessagesquery'] = $query;
	//return $ajaxarray;
	$result = func_php_query($query);
	if (mysqli_num_rows($result) > 0) {
		$ajaxarray['error'] = 'INFO_MESSAGING_AVAILABLE';
		//$ajaxarray['message'] = array();
		$i = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			//$r['description'] .= " $query".print_r($_POST, True);;
			$id = $r['id'];
			$ajaxarray['message'][$i] = $r;
			$ajaxarray['message'][$i]['postpopuplist'] = func_php_insertPostPopupList('message',
					$id, false, 'message', false, 'full', $index_activeuserid);
			$ajaxarray['message'][$i]['messagecredits'] = func_php_insertPostCredits('message',
					$id, false, 'full', false, $index_activeuserid);
			$ajaxarray['message'][$i]['messagedescription'] = func_php_markdowntohtml($r['description']);
			$ajaxarray['message'][$i]['messageattachments'] = func_php_fileAttachmentArray('message', $id);

			$i++;
		}
	} else if (mysqli_num_rows($result) === 0) {
		$ajaxarray['error'] = 'INFO_MESSAGING_AVAILABLE';
	}

	$query = sprintf('select messagesendonenterkey, messagecloseonsend from accounts'
			.' where username="%s" limit 1;', $index_activeusername);
	$ajaxarray['selectmessagesettings'] = $query;
	$result = func_php_query($query);
	if (mysqli_num_rows($result) == 1) {
		$r = mysqli_fetch_assoc($result);
		$ajaxarray['messagesendonenterkey'] = $r['messagesendonenterkey'];
		$ajaxarray['messagecloseonsend'] = $r['messagecloseonsend'];
	}

	/* Now set unread messages' viewcount to 1. */
	if ($contactusername && $contactserver) {
		$query = sprintf('/* '.__LINE__.' */ update posts, recipients'
				.' set posts.viewcount = 1'
				.' where posts.viewcount < 1 and recipients.recipientaccountid = %d' /* USERID */
				.' and recipients.tablename = "posts" and recipients.tableid = posts.id'
				.' and posts.emailusername = "%s" and posts.emailserver = "%s";', /* EMLUSERNAME, EMLSERVER */
				$index_activeuserid, func_php_escape($contactusername),
				func_php_escape($contactserver));
	} else if ($contactusername && !$postid) {
		$query = sprintf('/* '.__LINE__.' */ update posts, accounts,'
				.' recipients, accounts as useracct,'
				.' accounts as contacct set posts.viewcount = 1'
				.' where posts.viewcount < 1 and useracct.username = "%s"' /* USERNAME */
				.' and contacct.username = "%s" and posts.accountid = accounts.id' /* USERNAME */
				.' and recipients.tablename = "posts" and recipients.tableid = posts.id'
				.' and ((recipients.recipientaccountid = useracct.id and accounts.id = contacct.id)'
				.' or (recipients.recipientaccountid = contacct.id and accounts.id = useracct.id));',
				func_php_escape($index_activeusername), func_php_escape($contactusername));
	} else if ($postid > 0) {
		$query = sprintf('/* '.__LINE__.' */ update posts'
				.' left join membertree on posts.id = membertree.postid, accounts as recipientaccount'
				.' set posts.viewcount = 1'
				.' where (posts.id = %d or membertree.parentid = %d)' /* POSTID, POSTID */
				.' and recipientaccount.username = "%s" and posts.viewcount < 1;', /* USERNAME */
				$postid, $postid, func_php_escape($index_activeusername));
	}
	$ajaxarray['updateviewcount'] = $query;
	$result = func_php_query($query);

	return $ajaxarray;
}

function index_php_ajax_search_text()
{
	global $mysqli;
	global $index_activeuserid, $index_activeusername, $index_activefullname;
	global $servername, $urlhome;
	$ajaxarray = array();
	$ajaxarray['error'] = 'ERROR_SEARCH_NOT_AVAILABLE';
	$ajaxarray['post'] = print_r($_POST, True);
	/* return $ajaxarray; */

	$rowsperpage = 10; /* Make this configurable later. */

	$searchtext = func_php_cleantext($_POST['searchtext'], 255);
	/* TEMPORARY: Revert back to single-keyword searches while optimisation are under way. */
	$searchkeywordscriteria = sprintf('searchkeywords.keyword like "%s%%"', /* KEYWORD */
			func_php_escape($searchtext));
	$searchtextnumbertest = func_php_inttext($searchtext);
	/* TEMPORARY: This is the tokenised search version for multi-keyword searches.
	$searchtexttokens = explode(' ', mb_strtolower(trim($searchtext)));
	$searchtexttokens = array_unique($searchtexttokens);
	$searchkeywordscriteria = '';
	foreach ($searchtexttokens as $keyword) {
		if ($searchkeywordscriteria)
			$searchkeywordscriteria .= ' and ';
		$searchkeywordscriteria .= sprintf('searchkeywords.keyword like "%s%%"', // KEYWORD
				func_php_escape($keyword));
	}
	*/

	if (strlen($searchtext) > 0) {
		$ajaxarray['searchtext'] = $searchtext;
	} else {
		if (!$searchtext) {
			$ajaxarray['error'] = 'INFO_NO_SEARCH_QUERY';
		} else {
			$ajaxarray['error'] = 'ERROR_SEARCH_INVALID_QUERY';
		}
		return $ajaxarray;
	}

	/* Validate search type */
	$searchtype = func_php_cleantext($_POST['searchtype'], 255);
	switch ($searchtype) {
	case 'post':
	case 'image':
	case 'video':
	case 'audio':
	case 'file':
	case 'profile':
		/* Valid search type given; keep it. */
		break;
	default:
		$searchtype = 'all';
	}
	$ajaxarray['searchtype'] = $searchtype;

	/* Validate search order. */
	$searchorder = func_php_cleantext($_POST['searchorder'], 255);
	switch ($searchorder) {
	case 'popularity':
	case 'date':
	case 'name':
		/* Valid search order; keep it. */
		break;
	default:
		$searchorder = 'relevance';
	}
	$ajaxarray['searchorder'] = $searchorder;

	/* Start building query now. */
	$query = '';

	/* TODO:OPTIMISE - "explain select.." shows this goes through every row of searchindex 6 times! */
	/* Add post search */
	if (($searchtype == 'post') || ($searchtype == 'all')) {
		$searchpostscriteria = $searchkeywordscriteria;
		if ($searchtextnumbertest > 0) {
			/* Add numeric search by ID. */
			$searchpostscriteria .= sprintf(' or posts.id = %d', /* ID */
					$searchtextnumbertest);
		}
		/* World viewable, open viewable, or owner of posts */
		$query .= '/* '.__LINE__.' */'.sprintf('select distinct %d as userid, accounts.username,' /* USERID */
				.' searchindex.tablename, searchindex.tableid,'
				.' concat(substring(posts.searchcache, 1, 65),'
				.' if(length(posts.searchcache) > 65, concat("...<br>...",'
				.' substring(posts.searchcache,'
				.' greatest(least(locate(searchkeywords.keyword, posts.searchcache, 65) - 25,'
				.' char_length(posts.searchcache)-65), 65), 65)), "")) as searchcache,'
				.' approvals.hidden, approvals.trusted, flags.flagactive',
				$index_activeuserid);
		if ($searchorder == 'popularity') {
			$query .= ', posts.viewcount as sortvalue';
		} else if ($searchorder == 'date') {
			$query .= ', posts.added as sortvalue';
		}
		$query .= sprintf(' from searchindex inner join searchkeywords on searchindex.keywordid = searchkeywords.id,'
				.' posts left join accounts on posts.accountid = accounts.id'
				.' left join approvals on approvals.tableid = posts.id and approvals.tablename = "posts"'
				.' left join flags on flags.tableid = posts.id and flags.tablename = "posts"'
				.' where searchindex.tablename = "posts" and posts.id = searchindex.tableid '
				.' and (posts.viewpermissions = 5 or ((posts.accountid = %d ' /* USERID */
				.' or posts.viewpermissions = 4) and %d != 0))' /* USERID */
				.' and ((approvals.trusted = 1) or (posts.parentid = 0))'
				.' and ((flagactive is NULL) or (flagactive = 0)'
				.' or ((posts.accountid = %d) and (posts.accountid != 0)))' /* USERID */
				.' and (%s) group by posts.id', /* CRITERIA */
				$index_activeuserid, $index_activeuserid, $index_activeuserid, $searchpostscriteria);

		/* Viewable by recipients only */
		$query .= sprintf(' UNION select distinct %d as userid, accounts.username,' /* USERID */
				.' searchindex.tablename, searchindex.tableid,'
				.' concat(substring(posts.searchcache, 1, 65),'
				.' if(length(posts.searchcache) > 65, concat("...<br>...",'
				.' substring(posts.searchcache,'
				.' greatest(least(locate(searchkeywords.keyword, posts.searchcache, 65) - 25,'
				.' char_length(posts.searchcache)-65), 65), 65)), "")) as searchcache,'
				.' approvals.hidden, approvals.trusted, flags.flagactive',
				$index_activeuserid);
		if ($searchorder == 'popularity') {
			$query .= ', posts.viewcount as sortvalue';
		} else if ($searchorder == 'date') {
			$query .= ', posts.added as sortvalue';
		}
		$query .= sprintf(' from searchindex inner join searchkeywords on searchindex.keywordid = searchkeywords.id,'
				.' recipients, posts left join accounts on posts.accountid = accounts.id'
				.' left join approvals on approvals.tableid = posts.id and approvals.tablename = "posts"'
				.' left join flags on flags.tableid = posts.id and flags.tablename = "posts"'
				.' where searchindex.tablename = "posts" and posts.id = searchindex.tableid '
				.' and posts.viewpermissions = 1 and recipients.tablename = "posts"'
				.' and recipients.tableid = posts.id'
				.' and (recipients.senderaccountid = %d or recipients.recipientaccountid = %d)' /* USERID, USERID */
				.' and ((approvals.trusted = 1) or (posts.parentid = 0))'
				.' and ((flagactive is NULL) or (flagactive = 0)'
				.' or ((posts.accountid = %d) and (posts.accountid != 0)))' /* USERID */
				.' and (%s) group by posts.id', /* CRITERIA */
				$index_activeuserid, $index_activeuserid, $index_activeuserid, $searchpostscriteria);

		/* Viewable by trusted contacts only */
		$query .= sprintf(' UNION select distinct %d as userid, accounts.username,' /* USERID */
				.' searchindex.tablename, searchindex.tableid,'
				.' concat(substring(posts.searchcache, 1, 65),'
				.' if(length(posts.searchcache) > 65, concat("...<br>...",'
				.' substring(posts.searchcache,'
				.' greatest(least(locate(searchkeywords.keyword, posts.searchcache, 65) - 25,'
				.' char_length(posts.searchcache)-65), 65), 65)), "")) as searchcache,'
				.' approvals.hidden, approvals.trusted, flags.flagactive',
				$index_activeuserid);
		if ($searchorder == 'popularity') {
			$query .= ', posts.viewcount as sortvalue';
		} else if ($searchorder == 'date') {
			$query .= ', posts.added as sortvalue';
		}
		$query .= sprintf(' from searchindex inner join searchkeywords on searchindex.keywordid = searchkeywords.id,'
				.' contacts, posts left join accounts on posts.accountid = accounts.id'
				.' left join approvals on approvals.tableid = posts.id and approvals.tablename = "posts"'
				.' left join flags on flags.tableid = posts.id and flags.tablename = "posts"'
				.' where searchindex.tablename = "posts" and posts.id = searchindex.tableid'
				.' and (posts.viewpermissions = 2 and posts.accountid = contacts.accountid'
				.' and contacts.trusted = 1 and contacts.contactid = %d)' /* USERID */
				.' and ((approvals.trusted = 1) or (posts.parentid = 0))'
				.' and ((flagactive is NULL) or (flagactive = 0)'
				.' or ((posts.accountid = %d) and (posts.accountid != 0)))' /* USERID */
				.' and (%s) group by posts.id', /* CRITERIA */
				$index_activeuserid, $index_activeuserid, $searchpostscriteria);

		/* Viewable by trusted members only */
		$query .= sprintf(' UNION select distinct %d as userid, accounts.username,' /* USERID */
				.' searchindex.tablename, searchindex.tableid,'
				.' concat(substring(posts.searchcache, 1, 65),'
				.' if(length(posts.searchcache) > 65, concat("...<br>...",'
				.' substring(posts.searchcache,'
				.' greatest(least(locate(searchkeywords.keyword, posts.searchcache, 65) - 25,'
				.' char_length(posts.searchcache)-65), 65), 65)), "")) as searchcache,'
				.' approvals.hidden, approvals.trusted, flags.flagactive',
				$index_activeuserid);
		if ($searchorder == 'popularity') {
			$query .= ', posts.viewcount as sortvalue';
		} else if ($searchorder == 'date') {
			$query .= ', posts.added as sortvalue';
		}
		$query .= sprintf(' from searchindex inner join searchkeywords on searchindex.keywordid = searchkeywords.id,'
				.' members, membertree, posts as parentpost, posts left join accounts on posts.accountid = accounts.id'
				.' left join approvals on approvals.tableid = posts.id and approvals.tablename = "posts"'
				.' left join flags on flags.tableid = posts.id and flags.tablename = "posts"'
				.' where searchindex.tablename = "posts" and posts.id = searchindex.tableid'
				.' and (posts.viewpermissions = 3 and posts.id = (membertree.postid = posts.id'
				.' or membertree.parentid = posts.id) and membertree.parentid = members.postid'
				.' and ((members.trusted = 1 and members.accountid = %d)' /* USERID */
				.' or (parentpost.id = membertree.parentid'
				.' and parentpost.accountid = %d and parentpost.accountid != 0)))' /* USERID */
				.' and ((approvals.trusted = 1) or (posts.parentid = 0))'
				.' and ((flagactive is NULL) or (flagactive = 0)'
				.' or ((posts.accountid = %d) and (posts.accountid != 0)))' /* USERID */
				.' and (%s) group by posts.id', /* CRITERIA */
				$index_activeuserid, $index_activeuserid, $index_activeuserid, $searchpostscriteria);
	}

	/* Add image search */
	if (($searchtype == 'all') || ($searchtype == 'image')) {
		if ($query) {
			$query .= ' UNION ';
		}
		$query .= '/* '.__LINE__.' '.$searchtype.' */'.sprintf('select distinct %d as userid, accounts.username,' /* USERID */
				.' searchindex.tablename, posts.id as tableid,'
				.' concat(substring(posts.searchcache, 1, 65), "...<br>(", files.filename, ")") as searchcache,'
				.' approvals.hidden, approvals.trusted, flags.flagactive',
				$index_activeuserid);

		if ($searchorder == 'popularity') {
			$query .= ', files.viewcount as sortvalue';
		} else if ($searchorder == 'date') {
			$query .= ', files.added as sortvalue';
		}

		$query .= sprintf(' from searchindex inner join files on searchindex.tableid = files.id'
				.' inner join searchkeywords on searchindex.keywordid = searchkeywords.id'
				.' inner join posts on posts.id = files.postid'
				.' left join accounts on files.accountid = accounts.id'
				.' left join approvals on approvals.tableid = posts.id and approvals.tablename = "posts"'
				.' left join flags on flags.tableid = posts.id and flags.tablename = "posts"'
				.' where searchindex.tablename = "files" and files.filemime = "image/jpeg"'
				.' and ((approvals.trusted = 1) or (posts.parentid = 0))'
				.' and ((flagactive is NULL) or (flagactive = 0)'
				.' or ((posts.accountid = %d) and (posts.accountid != 0)))' /* USERID */
				.' and (%s)', /* CRITERIA */
				$index_activeuserid, $searchkeywordscriteria);
	}

	/* Add profile search */
	if (($searchtype == 'all') || ($searchtype == 'profile')) {
		if ($query) {
			$query .= ' UNION ';
		}
		$query .= '/* '.__LINE__.' searchtype='.$searchtype.' */'.sprintf('select distinct %d as userid, accounts.username,' /* USERID */
				.' searchindex.tablename, searchindex.tableid,'
				.' concat(accounts.fullname, " <", accounts.username, "@%s>") as searchcache,'
				.' NULL, NULL, flags.flagactive', /* NULL is approvals hidden/trusted. */
				$index_activeuserid, $servername);

		if ($searchorder == 'popularity') {
			$query .= ', accounts.viewcount as sortvalue';
		} else if ($searchorder == 'date') {
			$query .= ', accounts.added as sortvalue';
		}

		$query .= sprintf(' from searchindex inner join accounts on searchindex.tableid = accounts.id'
				.' left join flags on flags.tableid = accounts.id and flags.tablename = "accounts"'
				.' inner join searchkeywords on searchindex.keywordid = searchkeywords.id'
				.' where searchindex.tablename = "accounts"'
				.' and ((flagactive is NULL) or (flagactive = 0)'
				.' or ((accounts.id = %d) and (accounts.id != 0)))' /* USERID */
				.' and (%s)', /* CRITERIA */
				$index_activeuserid, $searchkeywordscriteria);
	}

	if (!$query) {
		$ajaxarray['error'] = 'ERROR_SEARCH_NO_QUERYSTRING';
		return $ajaxarray;
	}

	if (($searchorder == 'popularity') || ($searchorder == 'date')) {
		$query .= ' order by sortvalue desc';
	} else if ($searchorder == 'name') {
		$query .= ' order by searchcache';
	}

	$page = func_php_inttext($_POST['searchpage']);
	if ($page < 0)
		$page = 0;
	$query .= sprintf(' limit %d, %d;', $page*$rowsperpage, $rowsperpage);

	$ajaxarray['query'] = $query;
	$result = func_php_query($query);
	if (mysqli_num_rows($result) > 0) {
		//$ajaxarray['message'] = array();
		$i = 0;
		/* pattern and replacement are used with preg_replace below to emphasize search keywords */
		$pattern = "/\b($searchtext.*?)\b/i";
		$replacement = '<strong>${1}</strong>';
		while ($r = mysqli_fetch_assoc($result)) {
			//$r['description'] .= " $query".print_r($_POST, True);;:
			$ajaxarray['search'][$i] = $r;
			$sortvalue = '';
			if ($searchorder == 'popularity') {
				$sortvalue = "({$r['sortvalue']}) ";
			} else if ($searchorder == 'date') {
				$sortvalue = '('.func_php_fuzzyTimeAgo($r['sortvalue'], true, true).') ';
			}
			/* Emphasize search keywords with preg_replace */
			$ajaxarray['search'][$i]['searchcache'] = $sortvalue.$r['searchcache'];
			$ajaxarray['search'][$i]['searchdisplay'] = preg_replace($pattern, $replacement, $r['searchcache']);
			if (($r['tablename'] == 'posts') || ($r['tablename'] == 'files')) {
				$searchlink = "{$urlhome}/{$r['tableid']}";
			} else if ($r['tablename'] == 'accounts') {
				$searchlink = "{$urlhome}/{$r['username']}";
			}
			/* NEXTINLINE Separate links for direct image/file link.
			} else if ($r['tablename'] == 'files') {
				$searchlink = "{$urlhome}/index.php?fileid={$r['tableid']}&filename={$r['searchcache']}";
			}
			// */
			$ajaxarray['search'][$i]['searchlink'] = $searchlink;
			$i++;
		}
		$query = 'select found_rows();';
		$result = func_php_query($query);
		$r = mysqli_fetch_assoc($result);
		$foundrows = $r['found_rows()'];
		$pagect = ceil($foundrows / $rowsperpage);
		$ajaxarray['searchstatus'] = "Page ".($page+1)." of $pagect | "
				.$foundrows.($i > 1 ? " results" : " result");
		if ($page >= $pagect)
			$page = $pagect - 1;
		$ajaxarray['searchpage'] = $page;
		$ajaxarray['searchpagect'] = $pagect;
		$ajaxarray['error'] = 'INFO_SEARCH_SUCCESSFUL';
	} else {
		$ajaxarray['searchstatus'] = 'No results';
		$ajaxarray['searchpage'] = 0;
		$ajaxarray['searchpagect'] = 0;
		$ajaxarray['error'] = 'INFO_NO_SEARCH_RESULTS';
	}

	return $ajaxarray;
}

function index_php_ajax_profile_search()
{
	global $mysqli;
	global $index_activeuserid, $index_activeusername, $index_activefullname;
	$ajaxarray = array();
	$ajaxarray['error'] = 'DEBUG_PROFILE_SEARCH_INCOMPLETE';
	$ajaxarray['post'] = print_r($_POST, True);

	$querycriteria = '';
	$queryoperator = '';

	/* Restrict by full name. This part needs to be handled differently. */
	$fullname = func_php_cleantext($_POST['profilefullname'], 255);
	if (strlen($fullname) > 1) {
		$fullnametokens = explode(' ', mb_strtolower(trim($fullname)));
		$fullnametokens = array_unique($fullnametokens);
		$i = 0;
		foreach ($fullnametokens as $name) {
			if (strlen($name) > 1) {
				$querycriteria .= sprintf(' inner join searchindex as si%d' /* I */
						.' on accounts.id = si%d.tableid' /* I */
						.' and si%d.tablename = "accounts"' /* I */
						.' inner join searchkeywords as sk%d' /* I */
						.' on si%d.keywordid = sk%d.id' /* I, I */
						.' and sk%d.keyword like "%%%s%%"', /* I, NAME */
						$i, $i, $i, $i, $i, $i, $i, func_php_escape($name));
				$i++;
			}
		}
	}

	/* Other profile search details can be done in table:accounts */
	$profilematch = func_php_cleantext($_POST['profilematch'], 255);
	if ($profilematch == 'any')
		$queryoperator = ' or';
	else
		$queryoperator = ' and';

	/* Restrict by username */
	$username = func_php_cleantext($_POST['profileusername'], 255);
	if (strlen($username) > 1) {
		if (!$querycriteria)
			$querycriteria = ' where';
		else
			$querycriteria .= $queryoperator; /* [ and | or ] */
		$querycriteria .= sprintf(' username like "%%%s%%"',
				func_php_escape($username));
	}

	/* Restrict by country */
	$country = func_php_cleantext($_POST['profilecountry'], 255);
	if (strlen($country) > 1) {
		if (!$querycriteria)
			$querycriteria = " where";
		else
			$querycriteria .= $queryoperator; /* [ and | or ] */
		$querycriteria .= sprintf(' country like "%s%%"', func_php_escape($country));
	}

	/* Restrict by town */
	$town = func_php_cleantext($_POST['profiletown'], 255);
	if (strlen($town) > 1) {
		if (!$querycriteria)
			$querycriteria = ' where';
		else
			$querycriteria .= $queryoperator; /* [ and | or ] */
		$querycriteria .= sprintf(' town like "%s%%"', func_php_escape($town));
	}

	/* Leave early if no profile search criteria given. */
	if (!$querycriteria) {
		$ajaxarray['error'] = 'ERROR_NO_PROFILE_SEARCH_TERMS';
		return $ajaxarray;
	}

	$query = '/* '.__LINE__." */ select distinct accounts.* from accounts {$querycriteria} limit 100;";

	$ajaxarray['query'] = $query;
	$result = func_php_query($query);
	if (mysqli_num_rows($result) > 0) {
		$ajaxarray['error'] = 'INFO_ADVANCED_PROFILE_SEARCH_RESULTS';
		$ajaxarray['count'] = mysqli_num_rows($result);
		//$ajaxarray['message'] = array();
		$i = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			/* $ajaxarray['profile'][$i] = $r; */
			$ajaxarray['profile'][$i]['memberlink'] = func_php_insertMemberLink($r['username'],
					true, true, 0, 0, '', 0, $index_activeuserid);
			$ajaxarray['profile'][$i]['username'] = $r['username'];
			$ajaxarray['profile'][$i]['fullname'] = $r['fullname'];
			$ajaxarray['profile'][$i]['town'] = $r['town'];
			$ajaxarray['profile'][$i]['state'] = $r['state'];
			$ajaxarray['profile'][$i]['country'] = $r['country'];
			$i++;
		}
	} else {
		$ajaxarray['error'] = 'INFO_ADVANCED_PROFILE_SEARCH_NO_RESULTS';
	}

	return $ajaxarray;
}

function index_php_ajax_delete_file()
{
	global $mysqli;
	global $index_activeuserid, $index_activeusername;
	$ajaxarray = array();
	$ajaxarray['error'] = 'DEBUG_DELETE_FILE_INCOMPLETE';

	$ajaxarray['post'] = print_r($_POST, True);
	$fileid = func_php_inttext($_POST['deleteattachmentfileid']);
	$filename = func_php_cleantext($_POST['deleteattachmentfilename'], 4096);
	$query = sprintf('/* '.__LINE__.' */ select * from files'
			.' where id=%d and filename="%s" and accountid=%d limit 1;', /* USERID */
			$fileid, func_php_escape($filename), $index_activeuserid);
	$ajaxarray['selectquery'] = $query;
	$result = func_php_query($query);
	if (mysqli_num_rows($result) != 1) {
		$ajaxarray['error'] = 'ERROR_DELETE_FILE_INVALID_IDENTIFIERS';
		return $ajaxarray;
	}
	$r = mysqli_fetch_assoc($result);
	$fileid = $r['id'];
	$fileindex = $r['fileindex'];
	$filename = $r['filename'];
	$phylum = func_php_cleantext($_POST['deleteattachmentphylum'], 64);
	$postid = func_php_inttext($_POST['deleteattachmentpostid']);

	func_php_deleteFile($r);

	$query = sprintf('/* '.__LINE__.' */ delete from files'
			.' where id=%d and filename="%s" and accountid=%d limit 1;', /* USERID */
			$fileid, func_php_escape($filename), $index_activeuserid);
	$ajaxarray['deletequery'] = $query;
	func_php_query($query);

	$ajaxarray['phylum'] = $phylum;
	$ajaxarray['fileid'] = $fileid;
	$ajaxarray['postid'] = $postid;
	$ajaxarray['error'] = 'INFO_DELETE_ATTACHMENT_SUCCESS';
	return $ajaxarray;
}

function index_php_ajax_share_by_email()
{
	global $mysqli, $urlhome, $servername;
	global $servertitle;
	global $index_activeuserid, $index_activeusername, $index_activefullname;
	$ajaxarray = array();
	$ajaxarray['error'] = 'DEBUG_SHARE_BY_EMAIL_INCOMPLETE';
	$ajaxarray['post'] = print_r($_POST, true);

	$postid = func_php_inttext($_POST['sharebyemailpostid']);
	$emailaddress = func_php_cleantext($_POST['sharebyemailaddress'], 256);
	if (preg_match('/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/', $emailaddress) == null) {
		$ajaxarray['error'] = 'ERROR_SHARE_INVALID_EMAIL';
		return $ajaxarray;
	}

	/* Check if current user has view permissions. */
	$permissions = func_php_getUserPermissions($postid, $index_activeuserid);
	$ajaxarray['permissions'] = print_r($permissions, true);
	if (!$permissions) {
		$ajaxarray['error'] = 'ERROR_SHARE_INVALID_POST';
		return $ajaxarray;
	}

	if (!$permissions['canview']) {
		$ajaxarray['error'] = 'ERROR_SHARE_INVALID_PERMISSIONS';
		return $ajaxarray;
	}

	$query = sprintf('select * from posts where posts.id = %d limit 1;', $postid);
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);

	/* Generate a stripped-down version of the post suitable for plain-text email. */
	$description = $r['description'];
	$description = preg_replace('/\<br\>/', "\r\n", $description); /* Convert <br>s to unix new lines. */
	$description = preg_replace('/&nbsp;/', ' ', $description); /* Remove HTML entity codes. */
	$description = strip_tags($description);

	$to = $emailaddress;
	$subject = 'Shared post from '.$servertitle;
	if ($r['searchcache']) {
		$subject .= ': '.substr($r['searchcache'], 0, 40).'...';
	}
	$message = "You can view the following post in full at:\r\n"
			.$urlhome."/".$postid
			."\r\n"
			."\r\n"
			.$description;
	$message = quoted_printable_encode($message);

	$emailsharefrom = '';
	if ($index_activeuserid) {
		$emailsharefrom = "$index_activefullname <{$index_activeusername}@{$servername}>";
	} else {
		$emailsharefrom = "$servertitle <noreply-{$postid}@{$servername}>";
	}
	$headers = "From: {$emailsharefrom}\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/plain; charset=UTF-8\r\n";
	$headers .= "X-Mailer: PHP/".phpversion()."\r\n";

	if (mail($to, $subject, $message, $headers))
		$ajaxarray['error'] = "INFO_SHARE_BY_EMAIL_SENT";
	else
		$ajaxarray['error'] = "ERROR_SHARE_BY_EMAIL_NOT_SENT";

	return $ajaxarray;
}

function index_php_ajax_scrape_link()
{
	global $mysqli, $urlhome;
	global $index_activeuserid, $index_activeusername;
	global $index_activeuserfilespace, $index_activeuserfileuse;
	$ajaxarray = array();

	if ($index_activeuserid == 0) {
		$ajaxarray['error'] = 'ERROR_SCRAPE_LINK_NOT_SIGNED_IN';
		return $ajaxarray;
	}

	$url = func_php_cleantext($_POST['url'], 4096);
	$urlbase = '';
	preg_match('/^(http[s]?:\/\/[^\/\?\&\#]+)/', $url, $urls);
	$urlbase = $urls[1];
	$ajaxarray['urlbase'] = $urlbase;
	$images = array();

	ini_set('user_agent', 'Mozilla/4.0 (compatible; MirrorIsland Previewer)');
	$html = file_get_contents($url);
	$dom = new domDocument;
	$dom->preserveWhiteSpace = false;
	$dom->loadHTML($html);

	$og_url = '';
	$og_title = '';
	$og_image = '';
	$og_description = '';
	$metatags = $dom->getElementsByTagName('meta');
	foreach ($metatags as $meta) {
		$metaproperty = $meta->getAttribute('property');
		$metaname = $meta->getAttribute('name');
		$metacontent = $meta->getAttribute('content');

		if ($metaproperty == 'og:url') {
			$og_url = $metacontent;
		} else if ($metaproperty == 'og:title') {
			$og_title = $metacontent;
		} else if ($metaproperty == 'og:image') {
			$og_image = $metacontent;
		} else if (($metaproperty == 'og:description')
				|| ($metaname == 'description')) {
			$og_description = $metacontent;
		}
	}

	$title = '';
	if ($og_title) {
		$title = $og_title;
	} else {
		$headtitle = $dom->getElementsByTagName('title');
		foreach ($headtitle as $t) {
			if ($t->nodeValue)
				$title = $t->nodeValue;
		}
	}
	$title = trim($title);
	if (strlen($title) > 88) {
		$title = substr($title, 0, 85).'...';
	}

	if ($og_url) {
		$url = $og_url;
	}

	$iconsrc = '';
	$iconmime = '';
	$iconsize = 0;
	$linktags = $dom->getElementsByTagName('link');
	foreach ($linktags as $link) {
		$linkrel = $link->getAttribute('rel');
		$linkhref = $link->getAttribute('href');
		if ((($linkrel == 'shortcut icon') || ($linkrel == 'icon'))
				&& $linkhref && !$iconsrc) {
			if (preg_match('/^http/', $linkhref)) {
				$iconsrc = $linkhref;
			} else if (preg_match('/^\/.*/', $linkhref)) {
				$iconsrc = $urlbase.$linkhref;
			}
			if ($iconsrc) {
				$ajaxarray['linktags'][$dbgct]['iconsrc'] = $iconsrc;
				$headers = get_headers($imgsrc);
				foreach($headers as $headervalue) {
					if (preg_match('/^Content-Length: ([0-9]+)$/im',$headervalue, $matches))
						$iconsize = $matches[1];
				}
				ini_set('user_agent', 'Mozilla/4.0 (compatible; MirrorIsland Previewer)');
				$imagesizearray = getimagesize($iconsrc);
				$iconmime = $imagesizearray['mime'];
				$ajaxarray['linktags'][$dbgct]['iconmime'] = $iconmime;
				if (!preg_match('/^image.(gif|jpeg|jpg|png)$/', $iconmime))
					$iconsrc = '';
			}
		}
	}

	$ajaxarray['title'] = $title;
	$ajaxarray['href'] = $url;
	$ajaxarray['og_description'] = $og_description;

	$minsize = 120;
	$imagearraylist = array();
	$imagearraylist2 = array();
	if (preg_match('/^http/', $og_image)) {
		$images[] = $og_image;
	}
	$imgtags = $dom->getElementsByTagName('img');
	foreach ($imgtags as $img) {
		$src = $img->getAttribute('src');
		$imgsrc = '';
		if (preg_match('/^http/', $src)) {
			$imgsrc = $src;
		} else if (preg_match('/^\/\//', $src)) {
			$imgsrc = "http:{$src}"; /* Special case - usually from a CDN */
		} else if (preg_match('/^\//', $src)) {
			$imgsrc = "{$urlbase}{$src}";
		} else if (preg_match('/.*/', $src)) {
			$imgsrc = "{$urlbase}/{$src}";
		}

		if ($imgsrc)
			$images[] = $imgsrc;
	}

	$index_activeuserfileuse = index_php_getuserfileuse(FALSE, FALSE);
	$filespace = $index_activeuserfilespace - $index_activeuserfileuse;
	$imageid = 0;
	foreach ($images as $imgsrc) {
		$extradata = false;
		if (!$imgsrc)
			continue; /* Where do blank files come from? */
		ini_set('user_agent', 'Mozilla/4.0 (compatible; MirrorIsland Previewer)');
		$imagesizearray = getimagesize($imgsrc);
		$width = $imagesizearray[0];
		$height = $imagesizearray[1];
		$mime = $imagesizearray['mime'];
		$area = $width * $height;
		if (($width < $minsize) || ($height < $minsize))
			continue; /* Image too small, check next one (if available.) */

		$size = 0;
		$headers = get_headers($imgsrc);
		foreach($headers as $headervalue) {
			if (preg_match('/^Content-Length: ([0-9]+)$/im',$headervalue, $matches))
				$size = $matches[1];
		}

		/* Limit file betwen 1kB and 2MB, and remaining file space for the user */
		if (($size < 1024) || ($size > 2048000) || ($size > $filespace))
			continue;

		/* Skip if matching image already in imagearraylist */
		$imagearray = array('width' => $width, 'height' => $height,
				'area' => $area, 'size' => $size);
		if (in_array($imagearray, $imagearraylist))
			continue;

		$imageid++;
		$imagearraylist[] = $imagearray;

		/* Add addititonal data in the second array. */
		$imagearray['id'] = $imageid;
		$imagearray['src'] = $imgsrc;
		$imagearray['width'] = $width;
		$imagearray['height'] = $height;
		$imagearray['mime'] = $mime;
		$imagearraylist2[] = $imagearray;
	}

	usort($imagearraylist2,
			function($a, $b) { return $b['area'] - $a['area']; });
	$imagearraylist2 = array_slice($imagearraylist2, 0, 9);
	usort($imagearraylist2,
			function($a, $b) { return $a['id'] - $b['id']; });

	if ($iconsrc) {
		$imagearraylist2[] = array('src' => $iconsrc, 'mime' => $iconmime, 'size' => $imagesize);
	}
	$i = 0;
	foreach ($imagearraylist2 as $image) {
		$imgsrc = $image['src'];
		$fileextension = '';
		/* XXX: Generate a thumbnail? */
		if ($image['mime'] == 'image/gif') {
			$fileextension = '.gif';
		} else if ($image['mime'] == 'image/jpeg') {
			$fileextension = '.jpg';
		} else if ($image['mime'] == 'image/png') {
			$fileextension = '.png';
		} else
			continue; /* Stop here. Probably not an image file. */

		$query = sprintf('insert into files(accountid, added) values (%d, NOW());', /* ACCTID */
				$index_activeuserid);
		func_php_query($query);
		$fileid = mysqli_insert_id($mysqli);
		$fileindex = $fileid.$fileextension; /* Also used as filename. */

		/* Temporarily upload all files from remote location.
		 * Unused files will be deleted on post publish or save. */
		$filesrc = fopen($imgsrc, 'r');
		$filedest = fopen('files/'.$fileindex, 'w');
		while ($data = fread($filesrc, 4096)) {
			fwrite($filedest, $data);
		}
		fclose($filedest);
		fclose($filesrc);
		chmod('files/'.$fileindex, 0666);
		$query = sprintf('update files set filename="%s", filemime="%s",' /* FILENAME, FILEMIME */
				.' filewidth=%d, fileheight=%d, ' /* IMGWIDTH, IMGHEIGHT */
				.' filesize=%d, fileindex="%s" ' /* FILESIZE, INDEX */
				.' where id=%d limit 1;', /* FILEID */
				$fileindex, $image['mime'], $image['width'], $image['height'],
				$image['size'], func_php_escape($fileindex), $fileid);
		func_php_query($query);

		/* Do not include icon as scraped image */
		if ($imgsrc == $iconsrc) {
			$ajaxarray['icon']['fileid'] = $fileid;
			$ajaxarray['icon']['filename'] = $fileindex;
		/* Otherwise include other images in the "slideshow" chooser */
		} else {
			$ajaxarray['scrapedimage']['fileid'][$i] = $fileid;
			$ajaxarray['scrapedimage']['filename'][$i] = $fileindex;
			$i++;
		}
	}

	$ajaxarray['error'] = 'INFO_SCRAPE_URL_SUCCESS';
	return $ajaxarray;
}

function index_php_ajax_upload_template_index()
{
	global $mysqli, $urlhome;
	global $index_activeuserid, $index_activeusername;
	global $index_activeuserfilespace, $index_activeuserfileuse;
	global $s3profile, $s3endpoint, $s3region, $s3bucket;

	$ajaxarray = array();

	if ($index_activeuserid == 0) {
		$ajaxarray['error'] = 'ERROR_UPLOAD_TEMPLATE_INDEX_NOT_SIGNED_IN';
		return $ajaxarray;
	}

	$uploadfilename = func_php_cleantext($_FILES['templatesearchfile']['name'], 65535);
	$uploadfiletmpname = func_php_cleantext($_FILES['templatesearchfile']['tmp_name'], 65535);
	$filesize = func_php_inttext($_FILES['templatesearchfile']['size']);

	/* Check the tmp name for errors and JSON validity */
	if ($_FILES['templatesearchfile']['error'] != UPLOAD_ERR_OK
			|| !is_uploaded_file($uploadfiletmpname)) {
		$ajaxarray['error'] = 'ERROR_UPLOAD_TEMPLATE_INDEX_INVALID_FILE';
		return $ajaxarray;
	}

	/* Check for JSON validity. */
	$filetxt = file_get_contents($uploadfiletmpname);
	$json_arr = json_decode($filetxt, TRUE);
	if ($json_arr === null) {
		$ajaxarray['error'] = 'ERROR_UPLOAD_TEMPLATE_INDEX_INVALID_JSON';
		return $ajaxarray;
	}

	/* Check for all JSON columns */
	if (count($json_arr) == 0) {
		$ajaxarray['error'] = 'ERROR_UPLOAD_TEMPLATE_INDEX_ZERO_LENGTH';
		return $ajaxarray;
	}
	if (!array_key_exists('title', $json_arr[0])
			|| !array_key_exists('subtitle', $json_arr[0])
			|| !array_key_exists('imgsrc', $json_arr[0])
			|| !array_key_exists('uri', $json_arr[0])) {
		$ajaxarray['error'] = 'ERROR_UPLOAD_TEMPLATE_INDEX_INVALID_COLUMNS';
		return $ajaxarray;
	}

	$query = sprintf('insert into files(accountid, added, updated)'
			.' values (%d, NOW(), NOW());', /* ACCTID */
			$index_activeuserid);
	func_php_query($query);
	$fileid = mysqli_insert_id($mysqli);

	$fileext = pathinfo($uploadfilename, PATHINFO_EXTENSION);
	$ajaxarray['fileext'] = $fileext;
	$fileindex = "{$fileid}.{$fileext}"; /* Also used as filename. */

	$filesize = filesize($uploadfiletmpname);
	$filemime = mime_content_type($uploadfiletmpname);

	/* Find out if saving to S3 bucket, or locally otherwise */
	$uploadsuccess = false;
	if ($s3profile && $s3endpoint && $s3region && $s3bucket) {
		/* Use cloud storage (S3 bucket) */
		require 'aws/aws-autoloader.php';
		$sdk = new Aws\Sdk([
			'profile' => $s3profile,
			'endpoint' => $s3endpoint,
			'region' => $s3region,
			'version' => 'latest',
			'use_path_style_endpoint' => true,
		]);
		$s3 = $sdk->createS3();

		try {
			$result = $s3->putObject([
				'Bucket' => $s3bucket,
				'Key' => $fileindex,
				'SourceFile' => $uploadfiletmpname,
			]);
			$uploadsuccess = true;

			$query = sprintf('update files'
					.' set s3profile="%s", s3endpoint="%s",' /* s3profile, s3endpoint */
					.' s3region="%s", s3bucket="%s"' /* s3region, s3bucket */
					.' where id=%d limit 1;', /* id */
					$s3profile, $s3endpoint, $s3region, $s3bucket, $fileid);
			func_php_query($query);
		} catch (S3Exception $e) {
			error_log($e->getMessage());
		}
	} else {
		/* Save on web host storage */
		$uploadsuccess = move_uploaded_file($uploadfiletmpname,
				'files/'.$fileindex);
		if ($uploadsuccess) {
			chmod('files/'.$fileindex, 0666);
		}
	}

	if ($uploadsuccess) {
		$query = sprintf('update files set fileindex="%s", filename="%s",'
				.' filesize=%d, filemime="%s" '
				.' where id=%d limit 1;',
				func_php_escape($fileindex), func_php_escape($uploadfilename),
				$filesize, func_php_escape($filemime), $fileid);
		func_php_query($query);

		$ajaxarray['templateindexfile']['fileid'] = $fileid;
		$ajaxarray['templateindexfile']['fileindex'] = $fileindex;
		$ajaxarray['templateindexfile']['filemime'] = $filemime;

		$query = sprintf('insert into templates (accountid, indexfileid, useindex)'
				.' values (%d, %d, TRUE)', /* USERID, FILEID */
				$index_activeuserid, $fileid);
		func_php_query($query);

		$ajaxarray['error'] = 'INFO_UPLOAD_TEMPLATE_SUCCESS';
	} else {
		$query = sprintf('delete from files where id = %d limit 1;',
				$fileid);
		func_php_query($query);

		$ajaxarray['error'] = 'ERROR_UPLOAD_TEMPLATE_FAILED';
	}

	return $ajaxarray;
}

function index_php_ajax_get_template_indexes()
{
	global $mysqli, $urlhome;
	global $index_activeuserid, $index_activeusername;
	global $index_activeuserfilespace, $index_activeuserfileuse;
	$ajaxarray = array();

	if ($index_activeuserid == 0) {
		$ajaxarray['error'] = 'ERROR_GET_TEMPLATE_INDEXES_NOT_SIGNED_IN';
		return $ajaxarray;
	}

	$query = sprintf('/* '.__LINE__.' */ select files.filename,'
			.' templates.indexfileid, templates.useindex'
			.' from templates'
			.' inner join files on files.id = templates.indexfileid'
			.' where templates.accountid = %d', /* USERID */
			$index_activeuserid);
	$result = func_php_query($query);
	$ajaxarray['count'] = mysqli_num_rows($result);

	$arr = array();
	while ($r = mysqli_fetch_assoc($result)) {
		$arr[] = $r;
	}
	$ajaxarray['arr'] = $arr;
	$ajaxarray['error'] = 'INFO_GET_TEMPLATE_INDEXES_FETCHED';

	return $ajaxarray;
}

function index_php_ajax_manage_template_index()
{
	global $mysqli, $urlhome;
	global $index_activeuserid, $index_activeusername;
	$ajaxarray = array();

	$ajaxarray['error'] = 'ERROR_MANAGE_TEMPLATE_INDEX_INCOMPLETE';
	$mode = func_php_cleantext($_POST['managemode'], 64);
	$fileid = func_php_inttext($_POST['fileid']);
	$filename = func_php_cleantext($_POST['filename'], 1024);

	/* Option 1: Save useindex value to database... */
	if ($mode == 'useindex') {
		$useindexflag = func_php_cleantext($_POST['useindexflag'], 64);
		$flag = 'FALSE';
		if ($useindexflag == 'true') {
			$flag = 'TRUE';
		}
		$query = sprintf('update templates set useindex = %s' /* FLAG */
				.' where accountid=%d and indexfileid=%d limit 1', /* USERID, FILEID */
				$useindexflag, $index_activeuserid, $fileid);
		func_php_query($query);
	/* Option 2: Delete the template index file. */
	} else if ($mode == 'deleteindex') {
		$query = sprintf('select templates.* from templates'
				.' inner join files on templates.indexfileid = files.id'
				.' where templates.accountid = %d' /* USERID */
				.' and files.id = %d limit 1;', /* INDEXID */
				$index_activeuserid, $fileid);
		$result = func_php_query($query);

		if (mysqli_num_rows($result) != 1) {
			$ajaxarray['error'] = 'ERROR_TEMPLATE_INDEX_INVALID_FILE_ID';
			return $ajaxarray;
		}

		$r = mysqli_fetch_assoc($result);
		$templatesid = $r['templatesid'];

		$query = sprintf('delete from templates where id = %d limit 1', /* ID */
				$templatesid);
		func_php_query($query);

		$query = sprintf('select * from files'
				.' where files.id = %d limit 1', /* ID */
				$fileid);
		$result = func_php_query($query);
		$r = mysqli_fetch_assoc($result);
		func_php_deleteFile($r);

		$query = sprintf('delete from files where id = %d limit 1', /* ID */
				$fileid);
		func_php_query($query);

		$ajaxarray['fileid'] = $fileid;
		$ajaxarray['error'] = 'INFO_TEMPLATE_INDEX_DELETED';
	}

	return $ajaxarray;
}

function index_php_ajax_send_analytics()
{
	$ajaxarray = array('error' => 'ERROR_ANALYTICS_NOT_SAVED');

	$pageid = func_php_cleantext($_POST['pageid'], 4096);
	$phpsessionid = func_php_cleantext($_POST['phpsessionid'], 255);
	$windowWidth = func_php_inttext($_POST['windowWidth']);
	$documentReferrer = func_php_cleantext($_POST['documentReferrer'], 4096);
	$retval = func_php_setAnalytics($pageid, $phpsessionid, false, false,
			false, $windowWidth, false, -1); /* Authorised (-1) is ignored */
	if ($retval) {
		$ajaxarray['error'] = $retval;
	}

	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_save_online_status()
{
	global $index_activeuserid;

	$ajaxarray = array('error' => 'INFO_ONLINE_STATUS_SAVED');

	$onlinestatus = func_php_inttext($_POST['onlinestatus']);

	$query = sprintf('/* sseserver.php'.__LINE__.' */ update accounts'
			.' set onlinestatus = %d where accounts.id = %d limit 1',
			$onlinestatus, $index_activeuserid);
	func_php_query($query);

	/* TODO - START move this block to backend.php... */
	/* TODO: Can msg_send/msg_receive calls be moved to a function?
	 * TODO: They are also used for sending instant messages. */
	$backend_key = IPCMSGKEY + $index_activeuserid;
	$confirmation_key = (IPCMSGKEY * 2) + $index_activeuserid;
	if (msg_queue_exists($backend_key)
			&& msg_queue_exists($confirmation_key)) {
		$backend_queue = msg_get_queue($backend_key);
		$confirmation_queue = msg_get_queue($confirmation_key);
		$json = json_encode(array('type' => 'online_status',
				'onlinestatus' => $onlinestatus));

		/* Empty the confirmation queue */
		while (msg_receive($confirmation_queue, IPCMSGTYPE, $msgtype, 4096,
				$reply, false, MSG_IPC_NOWAIT)) { /* Do nothing */ }

		$attempts = 5;
		$md5 = md5($json);
		$replymd5 = '';
		do {
			msg_send($backend_queue, IPCMSGTYPE, $json);
			time_nanosleep(0, 100000000); /* 0.1 second delay */

			/* Get a confirmation from the SSE backend */
			$reply = false;
			if (msg_receive($confirmation_queue, IPCMSGTYPE, $msgtype, 4096,
					$reply, false, MSG_IPC_NOWAIT)) {
				$replymd5 = unserialize($reply);
			}
			$attempts--;
		} while (($attempts > 0) && ($replymd5 !== $md5));
	}
	/* TODO - END move this block to backend.php... */

	return $ajaxarray;
}

/**
 *
 */
function index_php_ajax_save_sidepanelvisible()
{
	global $index_activeuserid;

	$ajaxarray = array('error' => 'INFO_SIDEPANELVISIBLE_SAVED');

	$sidepanelvisible = func_php_inttext($_POST['sidepanelvisible']);

	$query = sprintf('/* sseserver.php'.__LINE__.' */ update accounts'
			.' set sidepanelvisible = %d where accounts.id = %d limit 1',
			$sidepanelvisible, $index_activeuserid);
	func_php_query($query);

	return $ajaxarray;
}

$index_ajaxarray = array('error' => false);
$ajax_command = (isset($_POST['ajax_command']) ? func_php_cleantext($_POST['ajax_command'], 64) : '');
if ($ajax_command) {
	$index_ajaxarray = array('error' => 'ERROR_INVALID_AJAX_COMMAND');
	switch ($ajax_command) {
	case 'username_available':
		$index_ajaxarray = index_php_ajax_username_available();
		break;
	case 'save_profile_information':
		if ($index_activeuserid) {
			$index_ajaxarray = index_php_ajax_save_profile_information();
		}
		break;
	case 'save_profile_settings':
		if ($index_activeuserid) {
			$index_ajaxarray = index_php_ajax_save_profile_settings();
		}
		break;
	case 'change_password':
		if ($index_activeuserid) {
			$index_ajaxarray = index_php_ajax_change_password();
		}
		break;
	case 'delete_account':
		if ($index_activeuserid) {
			$index_ajaxarray = index_php_ajax_delete_account();
		}
		break;
	case 'setup_editor':
		/* Start new post */
		$index_ajaxarray = index_php_ajax_setup_editor();
		break;
	case 'init_editor':
		/* Edit existing post or message */
		$index_ajaxarray = index_php_ajax_init_editor();
		break;
	case 'new_post':
	case 'new_postreply':
	case 'new_reply':
	case 'edit_post':
	case 'new_message':
	case 'edit_message':
		/* FORCING EDIT FOR PERMISSION:5 if ($index_activeuserid) */
		{
			$index_ajaxarray = index_php_ajax_save_post($ajax_command);
		}
		break;
	case 'cancel_edit_post':
		$index_ajaxarray = index_php_ajax_cancel_edit_post();
		break;
	case 'delete_post':
		/* ALLOWING DELETE FOR PERMISSION:5 if ($index_activeuserid) */
		{
			$index_ajaxarray = index_php_ajax_delete_post();
		}
		break;
	case 'set_post_domain_name':
		$index_ajaxarray = index_php_ajax_set_post_domain_name();
		break;
	case 'pin_post':
		$index_ajaxarray = index_php_ajax_pin_post();
		break;
	case 'approve_post':
		$index_ajaxarray = index_php_ajax_approve_post();
		break;
	case 'flag_post':
		$index_ajaxarray = index_php_ajax_flag_post();
		break;
	case 'join_group':
		if ($index_activeuserid) {
			$index_ajaxarray = index_php_ajax_join_group();
		}
		break;
	case 'upload_files':
		if ($index_activeuserid) {
			$index_ajaxarray = index_php_ajax_upload_files();
		}
		break;
	case 'get_older_posts':
		$index_ajaxarray = index_php_ajax_get_older_posts();
		break;
	case 'get_newer_posts':
		$index_ajaxarray = index_php_ajax_get_newer_posts();
		break;
	case 'forgot_password':
		$index_ajaxarray = index_php_ajax_forgot_password();
		break;
	case 'sign_in':
		$index_ajaxarray = index_php_ajax_sign_in();
		break;
	case 'sign_up':
		$index_ajaxarray = index_php_ajax_sign_up();
		break;
	case 'sign_out':
		$index_ajaxarray = index_php_ajax_sign_out();
		break;
	case 'add_contact':
		$index_ajaxarray = index_php_ajax_add_contact();
		break;
	case 'remove_contact':
		$index_ajaxarray = index_php_ajax_remove_contact();
		break;
	case 'get_updates':
		$index_ajaxarray = index_php_ajax_get_updates();
		break;
	case 'get_messages':
		$index_ajaxarray = index_php_ajax_get_messages();
		break;
	case 'search_text':
		$index_ajaxarray = index_php_ajax_search_text();
		break;
	case 'profile_search':
		$index_ajaxarray = index_php_ajax_profile_search();
		break;
	case 'delete_file':
		$index_ajaxarray = index_php_ajax_delete_file();
		break;
	case 'share_by_email':
		$index_ajaxarray = index_php_ajax_share_by_email();
		break;
	case 'scrape_link':
		$index_ajaxarray = index_php_ajax_scrape_link();
		break;
	case 'upload_template_index':
		$index_ajaxarray = index_php_ajax_upload_template_index();
		break;
	case 'get_template_indexes':
		$index_ajaxarray = index_php_ajax_get_template_indexes();
		break;
	case 'manage_template_index':
		$index_ajaxarray = index_php_ajax_manage_template_index();
		break;
	case 'send_analytics':
		$index_ajaxarray = index_php_ajax_send_analytics();
		break;
	case 'save_online_status':
		$index_ajaxarray = index_php_ajax_save_online_status();
		break;
	case 'save_sidepanelvisible':
		$index_ajaxarray = index_php_ajax_save_sidepanelvisible();
		break;
	default:
		$index_ajaxarray['error'] = 'ERROR_UNKNOWN_AJAX_COMMAND';
		$index_ajaxarray['command'] = $ajax_command;
		$index_ajaxarray['post'] = print_r($_POST, true);
	}

	echo json_encode($index_ajaxarray, JSON_PRETTY_PRINT);
	$postprocess = $index_ajaxarray['postprocess'] ?? '';
	if ($postprocess === 'indexpostforsearch') {
		/* Send $bgfuncpass as $_POST['p'] */
		$id = $index_ajaxarray['newpostid'] ?? 0;
		$data = array(
			'f' => 'indexpostforsearch',
			'id' => $id, 'deleteflag' => 'false'
		);
		func_php_sendbgfunc($data);
	}
	exit(); /* Terminate script early due to AJAX responses. */
}

function index_php_form_sign_up()
{
	global $mysqli;
	$formarray = array('error' => false);

	$fullname = func_php_cleantext($_POST['signupfullname'], 255);
	$username = func_php_cleantext($_POST['signupusername'], 255);
	$password = func_php_cleantext($_POST['signuppassword'], 255);

	/* Error-check and format input. */
	if ($username || !$password) {
		$formarray['error'] = 'ERROR_SIGNUP_FORM_INCOMPLETE';
		$formarray['signupform'] = $formarray['error'];
	}

	if (preg_match('/^[A-Za-z][A-Za-z0-9\-]*?[0-9]+$/', $username) != 1) {
		$formarray['error'] = 'ERROR_SIGNUP_USERNAME_INVALID';
		$formarray['username'] = $formarray['error'];
	}

	$query = sprintf('select count(*) from accounts where username = "%s" limit 1;',
			func_php_escape($username));
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);
	if ($r['count(*)'] != 0) {
		$formarray['error'] = 'ERROR_SIGNUP_USERNAME_UNAVAILABLE';
		$formarray['username'] = $formarray['error'];
	}

	if ($formarray['error'])
		return;

	/* Only proceed if all fields have been successfully entered. */
	$terms = file_get_contents('./terms.md');
	if ($terms === FALSE) {
		$terms = 'ERROR:UNABLE TO SAVE TERMS';
	}

	$sessionid = '';
	for ($i = 0; $i < 64; $i++) {
		$sessionid .= substr('1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
			mt_rand(0, 61), 1);
	}

	$filespace = mibibyte * 100;
	$filesize = mibibyte * 10; /* Also used for post size */
	$query = sprintf('insert into accounts '
			.' (fullname, username, termsofuse, termsofusedate,'
			.' viewpermissions, editpermissions, replypermissions,'
			.' sessionid, added, filespace, filesize, postsize)'
			.' values ("%s", "%s", "%s", now(), 5, 0, 4,'
			.' "%s", now(), %d, %d, %d);',
			func_php_escape($fullname), func_php_escape($username),
			func_php_escape($terms), func_php_escape($sessionid),
			$filespace, $filesize, $filesize);
	$result = func_php_query($query);

	/* The password belongs in a separate table. */
	$lastinsertid = mysqli_insert_id($mysqli);
	$passwordsalt = "$2y$13$";
	for ($i = 0; $i < 22; $i++) {
		$passwordsalt .= substr('1234567890/abcdefghijklmnopqrstuvwxyz.ABCDEFGHIJKLMNOPQRSTUVWXYZ',
				mt_rand(0, 63), 1);
	}
	$passwordhash = crypt($password, $passwordsalt);
	$query = sprintf('insert into passwords (accountid, password) values (%d, "%s");',
			$lastinsertid, func_php_escape($passwordhash));
	$result = func_php_query($query);

	$formarray['error'] = 'INFO_SIGNED_UP';
	return $formarray;
}

function index_php_form_sign_in()
{
	global $mysqli;
	$formarray = array();

	$username = func_php_cleantext($_POST['signinusername'], 255);
	$password = func_php_cleantext($_POST['signinpassword'], 255);

	$userid = index_php_verifypassword($username, $password);
	if (!$userid) {
		$formarray['error'] = 'ERROR_SIGNIN_INVALID_USERNAME_OR_PASSWORD';
		return $formarray;
	}

	$responsearray = array();
	$sessionid = '';
	for ($i = 0; $i < 64; $i++) {
		$sessionid .= substr('1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
			mt_rand(0, 61), 1);
	}
	$query = sprintf('update accounts set sessionid="%s" where username="%s" limit 1;',
			func_php_escape($sessionid), func_php_escape($username));
	$result = func_php_query($query);
	/* echo "I_SIGN_IN"; */
	$ajaxarray['linenumber'] = __LINE__;
	$index_activesessionid = $sessionid;
	$index_activeusername = $activeusername;
	$index_activeuserid = $activeuserid;
	$windowlocationhref = $_SERVER['HTTP_REFERER'];
	$formarray['error'] = 'INFO_SIGN_IN_SUCCESS';
	$formarray['server'] = print_r($_SERVER, true);

	$cookietimelength = time()+(60*60*24*90); /* 90 days. */
	setcookie('activesessionid', $sessionid, $cookietimelength, '/');
	setcookie('activeusername', $username, $cookietimelength, '/');
	setcookie('activeuserid', $userid, $cookietimelength, '/');
	//header('Location: '.$windowlocationhref);

	return $formarray;
}

/* Use in-line forms for mobile or non-javascript enabled browsers. */
$index_formarray = array('error' => false);
$form_command = (isset($_POST['form_command']) ? func_php_cleantext($_POST['form_command'], 64) : '');
if ($form_command) {
	switch ($form_command) {
	case 'sign_up':
		$index_formarray = index_php_form_sign_up();
		break;
	case 'sign_in':
		$index_formarray = index_php_form_sign_in();
		break;
	}
}

// # session_start();

/*
 * # # # Actual execution starts from here. # # #
 */
/* Retrieve any global data such as form buttons, who is signed in, etc. */

/* Change page layout, title, etc. depending on which page is shown. */
if (($index_get_page == 'advancedpostsearch')
		|| ($index_post_btn == 'advancedpostsearch')) {
	$index_page = 'advancedpostsearch';
	$index_title = 'Advanced Post Search';
	$index_newpostlabel = false;
	$index_showposts = false;
} else if ($index_get_page == 'billing') {
	$index_page = 'billing';
	$index_title = 'Billing Settings';
	$index_newpostlabel = false;
	$index_showposts = false;
} else if (($index_get_page == 'advancedprofilesearch')
		|| ($index_post_btn == 'advancedprofilesearch')) {
	$index_page = 'advancedprofilesearch';
	$index_title = 'Advanced Profile Search';
	$index_newpostlabel = false;
	$index_showposts = false;
} else if ($index_get_page == 'signin') {
	$index_page = 'signin';
	$index_title = 'Sign In';
	$index_newpostlabel = false;
	$index_showposts = false;
} else if ($index_get_page == 'pricing') {
	$index_page = 'pricing';
	$index_title = 'Pricing';
	$index_newpostlabel = false;
	$index_showposts = false;
} else {
	/* Check for pages specified by their numeric IDs. */
	if (preg_match('/^[0-9]+$/', $index_get_page)) {
		$postid = func_php_inttext($index_get_page);
		$showpost = false; /* Changes to true if viewable */
		$index_page = 'page';
		$index_page_id = 0;
		$index_newpostparentid = 0;
		$userpermissions = func_php_getUserPermissions($postid, $index_activeuserid);
		if ($userpermissions) {
			/* Set if view permissions allow. */
			if ($userpermissions['canview']) {
				$showpost = true;
				/* Manage flagged posts, if set */
				if ($userpermissions['flagactive']) {
					$showflaggedpost = false;
					if ($userpermissions['canedit'] || $userpermissions['canmoderate']) {
						/* Owner/moderator always see post */
						$showflaggedpost = true;
					} else {
						/* Only show appropriate posts depending on flag reason */
						switch ($userpermissions['flagreason']) {
						case 'MATURE_CONTENT':
						case 'ADULT_CONTENT':
							if ($index_activeuserconfirmedage >= $userpermissions['flagagerating']) {
								$showflaggedpost = true;
							}
							break;
						case 'COPYRIGHT_OR_TRADEMARK':
						case 'ILLEGAL_OR_VIOLATION':
							break;
						default:
							/* Always hide except from owner. */
						}
					}
					$showpost = $showflaggedpost;
				}

				if ($showpost) {
					$index_page_id = $postid;
					$index_page_userpermissions = $userpermissions;
					$index_newpostparentid = $postid;
					if (!$index_get_viewas) {
						$viewmode = $userpermissions['viewmode'];
						if (($viewmode == 'full') || ($viewmode == 'thumbnail')
								|| ($viewmode == 'list')) {
							$index_viewmode = $viewmode;
							$index_get_viewas = $index_viewmode;
						}
					}
					if (!$index_get_sort) {
						$sortmode = $userpermissions['sortmode'];
						if (($sortmode == 'date') || ($sortmode == 'title')) {
							$index_sortmode = $sortmode;
							$index_get_sort = $index_sortmode;
						}
						$sortreverse = $userpermissions['sortreverse'];
						if ($sortreverse == 1) {
							$index_sortreverse = true;
						}
					}
				}
			}

			/* Clear if not approved from parent, or not owner/moderator. */
			$exitnow = false; /* Used for Thoth-compatible features */
			$permissions = func_php_getPostPermissions($postid);
			$parentuserpermissions = func_php_getUserPermissions($userpermissions['parentid'],
					$index_activeuserid);
			if (((!$permissions['preapproved'] && !$permissions['approvalstrusted'])
					|| ($permissions['approvalshidden'] != 0))
					&& ($permissions['accountid'] != $index_activeuserid)
					&& (!$parentuserpermissions['canmoderate'])) {
				$showpost = false;
				$index_newpostparentid = 0;
			/* Generate a message entry */
			} else if ($index_get_option === 'message') {
				if (!$index_page_id) {
					http_response_code(403);
					exit();
				}

				$query = sprintf('update posts set viewcount = viewcount+1'
						.' where posts.id = %d limit 1;', $postid);
				func_php_query($query);

				$html = func_php_postToMessageHTML($index_page_id, $index_activeuserid);

				$json = json_encode(array('message' => $html));
				echo $json;
				$exitnow = true;
			/* Dump raw text if requested */
			} else if (($index_get_option == 'src') || ($index_get_option == 'htm')
					|| ($index_get_option == 'md')) {
				if (!$index_page_id) {
					http_response_code(403);
					exit();
				}
				$query = sprintf('select description from posts where id=%d limit 1;', /* PAGEID */
						$index_page_id);
				$result = func_php_query($query);
				$r = mysqli_fetch_assoc($result);
				$description = $r['description'];
				if ($index_get_option == 'htm') {
					echo func_php_markdowntohtml($description);
				} else if ($index_get_option == 'src') {
					header('Content-Type: text/plain');
					echo $description;
				} else {
					header('Content-Type: text/plain');
					echo func_php_htmltomarkdown($description);
				}
				$exitnow = true; /* Dump plain text and exit after analytics */
			} else if ($index_get_option == 'analytics') {
				/* Show analytics if valid page */
				if ($index_page_id) {
					$index_page = 'analytics';
					$index_title = 'Post Analytics';
					$index_newpostlabel = false;
					$index_showposts = false;
					if ($userpermissions['canedit'] || $userpermissions['canmoderate']) {
						$index_showanalytics = true;
					}
				}
			}

			/* Save analytics data if not owner, editor, or moderator */
			if (!$userpermissions['canedit'] && !$userpermissions['canmoderate']
					&& $index_page_id && ($index_page !== 'analytics')) {
				$index_analyticspageid = $postid;
				$index_analyticsphpsessionid = session_id();
				$httpuseragent = $_SERVER['HTTP_USER_AGENT'] ?? false;
				$remoteaddr = $_SERVER['REMOTE_ADDR'] ?? false;
				$referrer = $_SERVER['HTTP_REFERER'] ?? false;
				/* Passing $showpost lets us know if anybody tries to
				 * view a post they don't have access to. */
				func_php_setAnalytics($index_analyticspageid,
						$index_analyticsphpsessionid,
						$httpuseragent, $remoteaddr,
						$referrer, false, 'view', $showpost);
			}

			if (!$showpost) {
				$index_page_id = false;
			}

			/* Exit now if set in the block above */
			if ($exitnow) {
				exit();
			}
		}
	}

	if ($index_page == 'page') {
		$index_newpostlabel = 'Reply';
	} else if ($index_activeuserid && ($index_page != 'analytics')) {
		$index_newpostlabel = 'New Post';
	}
	$index_newpostpermissions = '504404';
	if ($index_get_page == 'new') {
		if ($index_activeuserid) {
			$index_page = $index_get_page;
			$index_title = 'Explore new posts';
		} else {
			$index_get_page = '';
		}
	}

	/* Display the next set of posts/page replies if requested. */
	$pagenum = func_php_inttext($index_get_option);
	if ($pagenum > 1) { /* if ($index_get_page && ($pagenum > 1)) */
		$index_page_number = $pagenum;
		$index_page_offset = ($index_page_number * $index_page_count) - $index_page_count;
	}
}

if ($index_get_page) {
	/* Weed out any reserved words. */
	switch ($index_get_page) {
	case 'signin':
	case 'signout':
		/* Reserved word found - do nothing! */
		if (!$index_activeuserid) {
			$index_page = 'signin';
		}
		break;
	case 'home':
		if (!$index_activeuserid) {
			$index_get_page = '';
		}
		break;
	case 'pricing':
		$index_page = 'pricing';
		break;
	case 'billing':
		$index_page = 'billing';
		break;
	default:
		if (!$index_get_page) {
			$index_page = 'landingpage';
			break;
		}

		if ($index_page)
			break; /* Page already specified. */

		/* Check for email address pages. */
		if (preg_match('/^([^@]+)@(.+)$/', $index_get_page, $matches)) {
			$index_emailusername = $matches[1];
			$index_emailusername = str_replace(' ', '+', $index_emailusername);
			$index_emailserver = $matches[2];
			$index_page = 'emails';
			$index_page_id = $index_get_page;
			if ($index_newpostlabel) {
				$index_newpostlabel = 'New Email';
			}
		} else {
			/* Check for profile pages. */
			$query = sprintf('/* '.__LINE__.' */ select accounts.*, files.filename as profileimagefilename'
				.' from accounts left join files on files.id = profileimagefileid'
				.' where username="%s" limit 1',
				func_php_escape($index_get_page));
			$result = func_php_query($query);
			if (mysqli_num_rows($result)) {
				$r = mysqli_fetch_assoc($result);

				$accountowner = false;
				if (strtolower($index_activeusername) === strtolower($r['username'])) {
					$accountowner = true;
				}

				if ($index_get_option == 'settings') {
					$index_page = 'accountsettings';
					$index_title = 'Account Settings';
					$index_newpostlabel = false;
					$index_showposts = false;
					$index_showsettings = $accountowner; /* Sets boolean value */
				} else if ($index_get_option == 'analytics') {
					$index_page = 'accountanalytics';
					$index_title = 'Account Analytics';
					$index_newpostlabel = false;
					$index_showposts = false;
					$index_showanalytics = $accountowner; /* Sets boolean value */
				} else if ($index_get_option == 'terms') {
					$index_page = 'accountterms';
					$index_title = 'Currently Accepted Terms of Use';
					$index_newpostlabel = false;
					$index_showposts = false;
					$index_showsettings = true; /* Terms of Use has no private data */
				} else if ($index_get_option == 'password') {
					$index_page = 'changepassword';
					$index_title = 'Change Password';
					$index_newpostlabel = false;
					$index_showposts = false;
					$index_showsettings = $accountowner; /* Sets boolean value */
				} else if ($index_get_option == 'delete') {
					$index_page = 'deleteaccount';
					$index_title = 'Delete Account';
					$index_newpostlabel = false;
					$index_showposts = false;
					$index_showsettings = $accountowner; /* Sets boolean value */
				} else {
					$index_showprofile = $r;
					$index_page = 'profile';
					$index_page_id = $index_showprofile['username'];
					$index_title = "{$index_showprofile['fullname']}'s profile";
					/* XXX Changing newpost to message user. */
					if (!$accountowner) {
						$index_newpostlabel = 'Send Message';

						$showprofile = true; /* TODO: Manage flagged/private profiles */
						$index_analyticspageid = $index_page_id;
						$index_analyticsphpsessionid = session_id();
						$httpuseragent = $_SERVER['HTTP_USER_AGENT'] ?? false;
						$remoteaddr = $_SERVER['REMOTE_ADDR'] ?? false;
						$referrer = $_SERVER['HTTP_REFERER'] ?? false;
						/* Passing $showpost lets us know if anybody tries to
						 * view a post they don't have access to. */
						func_php_setAnalytics($index_analyticspageid,
								$index_analyticsphpsessionid,
								$httpuseragent, $remoteaddr,
								$referrer, false, 'view', $showprofile);
					}
				}
			/* Show "Profiles not found" page. */
			} else {
				$index_page = 'page';
			}
		}
	}
}

if ($index_page == 'page') {
	if ($index_page_id) {
		$query = sprintf('select posts.id, posts.searchcache, accounts.fullname from posts'
				.' left join accounts on posts.accountid = accounts.id'
				.' where posts.id=%d limit 1', $index_page_id); /* POSTID */
		$result = func_php_query($query);
		$post = mysqli_fetch_assoc($result);
		if ($post['fullname'])
			$index_title .= $post['fullname'].' - ';
		$index_title .= substr($post['searchcache'] ?? '', 0, 140);
		if ($index_get_option !== 'analytics') {
			$permissions = func_php_getUserPermissions($post['id'], $index_activeuserid);
			if (!$permissions['canreply']) {
				$index_newpostlabel = '';
			} else if (!$permissions['preapproved']) {
				$index_newpostlabel .= ' <img src="/img/icon-post-pending.png">';
			}
		}
	} else {
		$index_title = 'Page not found';
		$index_newpostlabel = '';
		$index_showposts = false;
	}
} else if (!$index_page) {
	if (!$index_get_page && !$index_activeuserid) {
		$index_page = 'landingpage';
		$index_newpostlabel = '';
		$index_title = '';
		$index_showposts = false;
		$index_newpostlabel = false;
	} else {
		$index_page = 'home';
		$index_title = 'Home';
	}
}

if ($index_title) {
	$index_title .= ' - ';
}
?>
<!DOCTYPE html>
<html id="htmlmain">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="theme-color" content="#438c21">
<?php
if ($index_page == 'landingpage') {
?>
<meta property="og:title" content="<?=$servertitle?>">
<meta property="og:type" content="website">
<meta property="og:image" content="/img/landingpage.jpg">
<meta property="og:description" content="Create versatile web pages.">
<?php
}
?>
<title><?=$index_title?><?=$servertitle?></title>
<link rel="shortcut icon" href="/mirrorisland.png" type="image/png" />
<link rel="stylesheet" href="/style.css" type="text/css" />
<script>
<!--
/* Global javascript variables (jsglobal). */
var filepreviews = false;
var index_minutetaskstimer = 0;
var index_latestpostdate = [{"jsdate":0, "sqldate":0}];
var index_earliestpostdate = [{"jsdate":0, "sqldate":0}];
var index_divdatesarray = {};
var index_editpostviewmode = '';
var index_editposthostdiv = null;
var index_editpostinlinereply = false;
var index_editpostid = false;
var index_editpostphylum = false;
var index_editpostpagetype = '';
var index_editpostshowparent = false;
var index_editparentid = null;
var index_editmessageid = null;
var index_editrecipientsdiv = null;
var index_editdescription = null;
var index_editpostdescriptionid = null;
var index_editpostdescriptionlink = null;
var index_editpostdescriptionlinkhref = null;
var index_editpostdescriptionimglinkdiv = null;
var index_editpostdescriptionimgarray = [];
var index_editpostdescriptionimgindex = 0;
var index_editpostfilesarray = [];
var index_editpostrecipientsarray = [];
var index_editmessagefilesarray = [];
var index_editmessagerecipientsarray = [];
var index_editlinktext = '';
var index_editorpopupbuttonid = false;
var index_editorpopupdivid = false;
var index_editposthtmlview = false;
var index_originalrecipients = null;
var index_originaldescription = null;
var index_originalhostdivhtml = null;
var index_profileimg = null;
var index_profileimgwd, index_profileimght;
var index_profileimgdivdragdata = null;
var index_profileimgfull;
var index_profileimgzoomjson;
var index_contactlistarray = [];
var index_messagewindowarray = [{"username":false, "postid":0}];
var index_grouplistarray = [];
var index_messagerdragdata = null;
var index_sendingmessage = false;
var index_sendingpost = false;
var index_messagerclickeventlistener = false;
var index_messagerscrollheight = 0;
var index_messagerscrolltop = 0;
var index_searchdisplayed = false;
var index_searchtext = false;
var index_searchtexttimer = 0;
var index_searchtype = 0; /* 0 == all, else keyword(images, videos, profiles, etc...) */
var index_searchbtn = 0;
var index_searchpage = 0;
var index_searchpagect = 0;
var index_messageprevioususername = '';
var index_messagerecipient = '';
var index_messagepostid = 0;
var index_popupiconid = 0;
var index_popupmenuid = 0;
var index_manualpostpermissionsupdate = 0;
var index_manualmessagepermissionsupdate = 0;
var index_editpostfileuploadcount = 0;
var index_messagerfileuploadcount = 0;
var index_editPostFileUploadMode = '';
var index_editpostmaximumfilesize = <?=$index_activeuserfilesize?>;
var index_editpostmaximumpostsize = <?=$index_activeuserpostsize?>;
var index_editpostfilespace = <?=$index_activeuserfilespace?>;
var index_editpostfileuse = <?=$index_activeuserfileuse?>;
var index_newpostsessionid = 0;
var index_setupposteditorev = null;
var index_setupposteditorhostdivid = null;
var index_setupposteditorphylum = null;
var index_setupposteditorparentid = null;
var index_setupposteditorinlinereply = null;
var index_setupposteditorpermissions = null;
var index_setupposteditoragerating = null;
var index_setupposteditorrecipients = null;
var index_setupposteditorpagetype = null;
var index_setupposteditorsourceuri = null;
var index_setupposteditorviewmode = null;
var index_setupposteditorsortmode = null;
var index_setupposteditorsortreverse = false;
var index_templatepopupvisible = false;
var index_templateplaceholder = '';
var index_templateindexloading = 0;
var index_templateindexloaded = false;
var index_templateindexsource = [];
var index_templateindexarray = [];
var index_templatesearchtimer = 0;
var index_templatesearchfound = 0;
var index_templateparentid = 0;
var index_templatepermissions = '000000';
var index_templaterecipients = '';
var index_viewmode = '<?=$index_viewmode?>';
var index_modalprompt = false;
var index_modaldivid = false;
var index_modaldivoriginalparent = false;
var index_linegrapharray = [];
var index_sidepanelvisible = false;
var index_modalpopupid = false;
var index_modalpopuploaded = false;
var index_popupdivid = false;
var index_popupdivinit = false;
var index_lazyloadimgarray = [];
var index_lazyloadtimer = false;
var index_websocket = null;
var index_websockActive = false;
var index_websockuri = '<?=($websockuri ?? null)?>';
var index_websockRetrySec = 0;
var index_websockRetrySecCt = 0;
var index_eventsource = null;

const index_newpostid = 'NEWPOST';

var index_onlineStatus = 0;
/* 0:offline; 10/20/30: Online status type; +1:Trying to connect; +2:Connected.
 * 10:Online,NoConnection; 11:Online,Connecting; 12:Online,Connected;
 * 20:Away,NoConnection; 21:Away,Connecting; 22:Away,Connected;
 * 30:Invisible,NoConnection; 31:Invisible,Connecting; 32:Invisible,Connected */

var d = new Date();
index_earliestpostdate["jsdate"] = d;
d = new Date(1970, 0, 0);
index_latestpostdate["jsdate"] = d;

/**
 * Shorten document.getElementById(...)
 */
function $(id) {
	return document.getElementById(id);
}

/**
 * Change a single character in a string.
 * - str: target string to change.
 * - idx: 0-index offset to change in string.
 * - char: character to replace with.
 */
function index_js_setChar(str, idx, char)
{
	if (idx > str.length - 1)
		return str;

	return str.substring(0, idx) + char + str.substring(idx+1);
}

/**
 * Set colours of form inputs in case of errors.
 */
function index_js_setInputErrorColour(form_node, error_found) {
	if (error_found) {
		form_node.style.background = "#fcc";
		form_node.style.color = "#900";
	} else {
		form_node.style.background = "#fff";
		form_node.style.color = "#345";
	}
}

function index_js_parseResponseText(responseText, popupText)
{
	var responsearray = [];
	try {
		responsearray = JSON.parse(responseText)
	} catch (e) {
		console.log(`index.php:<?=__LINE__?> index_js_parseResponseText ERROR PARSING: ${responseText}`);
		if (popupText) {
			alert(popupText);
		}
		return false;
	}
	return responsearray;
}

/**
 *
 */
function index_js_signOut_AjaxFunction()
{
	if (xhrSignOut.readyState != 4)
		return;

	var responsearray = index_js_parseResponseText(xhrSignOut.responseText, "Could not sign out.\n\nTry again or contact the website administrators.");
	if (!responsearray)
		return;

	if (responsearray['error'] == "INFO_SIGN_OUT_SUCCESS") {
		windowlocationhref = responsearray['windowlocationhref'];
		/* $('signinsuggestions').innerHTML = "Account registered! <img src=\"/img/thinking12.gif\" width=\"12\" height=\"12\">";
		/*/
		document.cookie = "activesessionid=; expires=Thu, 01 Jan 1970, 00:00:00 UTC;";
		document.cookie = "activeusername=; expires=Thu, 01 Jan 1970, 00:00:00 UTC;";
		document.cookie = "activeuserid=; expires=Thu, 01 Jan 1970, 00:00:00 UTC;";
		window.location.href = windowlocationhref;
		// */
	} else {
		$('signinsuggestions').innerHTML = responsearray['error'];
	}
}

/**
 *
 */
function index_js_signOut()
{
	var signoutformdata = new FormData();
	signoutformdata.append("signoutsessionid", "<?=$index_activesessionid?>");
	signoutformdata.append("signoutusername", "<?=$index_activeusername?>");
	signoutformdata.append("ajax_command", "sign_out");

	xhrSignOut = new XMLHttpRequest();
	xhrSignOut.onreadystatechange = index_js_signOut_AjaxFunction;
	xhrSignOut.open("POST", "/index.php", true);
	xhrSignOut.send(signoutformdata);

	return false;
}

/**
 *
 */
function index_js_deleteAttachment_AjaxFunction()
{
	if (xhrDeleteAttachment.readyState != 4)
		return; /* Nothing to do here! */

	var responsearray = index_js_parseResponseText(xhrDeleteAttachment.responseText, false);
	if (!responsearray)
		return;

	if (responsearray['error'] != "INFO_DELETE_ATTACHMENT_SUCCESS") {
		return;
	}

	var phylum = responsearray['phylum'];
	var postid = responsearray['postid'];
	var fileid = responsearray['fileid'];
	var attachmentid = phylum+'_'+postid+'_attachment_'+fileid;
	index_js_deleteElementById(attachmentid);
	index_js_updateMessage();

	var filesarray = null;
	if (editor == 'messager') {
		filesarray = index_editmessagefilesarray;
	} else {
		filesarray = index_editpostfilesarray;
	}
	for (var i = 0, ct = filesarray.length; i < ct; i++) {
		var fileid = filesarray[i]['fileid'];
		if (fileid == responsearray['fileid']) {
			filesarray.splice(i, 1);
			ct--;
		}
	}
}

function index_js_deleteAttachment(phylum, postid, fileid, filename, editor)
{
	var filesarray = null;
	if (editor == 'messager') {
		filesarray = index_editmessagefilesarray;
	} else {
		filesarray = index_editpostfilesarray;
	}
	var returnearly = false;
	for (var i = 0, ct = filesarray.length; i < ct; i++) {
		if (fileid == filesarray[i]['fileid']) {
			/* Remove the file if it hasn't been published yet. */
			if (filesarray[i]['newfile'] == 1) {
				filesarray[i]['deletefile'] = 1; /* Mark for deletion immediately. */
				$(phylum+'_'+postid+'_deleteattachment_'+fileid).innerHTML = "Deleting now...";
			/* Otherwise mark as deleted. */
			} else if (filesarray[i]['deletefile'] == 0) {
				filesarray[i]['deletefile'] = 1; /* Mark for deletion, if editing is finalised. */
				$(phylum+'_'+postid+'_deleteattachment_'+fileid).innerHTML = "<span style='color:#393;'>Deleted</span> Undo";
				returnearly = true;
			/* Or unmark delete. */
			} else {
				filesarray[i]['deletefile'] = 0; /* UNmark for deletion. */
				$(phylum+'_'+postid+'_deleteattachment_'+fileid).innerHTML = "Delete";
				returnearly = true;
			}
			ct--;
		}
	}
	/* Resize popup editor if delete link causes overflow. */
	if (editor == 'messager') {
		index_js_updateMessage();
	}

	if (returnearly) {
		return;
	}

	/* Only proceed if newly uploaded files are about to be deleted. */
	var deleteattachmentformdata = new FormData();
	deleteattachmentformdata.append("deleteattachmentphylum", phylum);
	deleteattachmentformdata.append("deleteattachmentpostid", postid);
	deleteattachmentformdata.append("deleteattachmentfileid", fileid);
	deleteattachmentformdata.append("deleteattachmentfilename", filename);
	deleteattachmentformdata.append("deleteattachmenteditor", editor);
	deleteattachmentformdata.append("ajax_command", "delete_file");

	xhrDeleteAttachment = new XMLHttpRequest();
	xhrDeleteAttachment.onreadystatechange = index_js_deleteAttachment_AjaxFunction;
	xhrDeleteAttachment.open("POST", "/index.php", true);
	xhrDeleteAttachment.send(deleteattachmentformdata);
	// */
}

/**
 * AJAX function within index_js_previewImage.
 */
function index_js_uploadImage_AjaxFunction()
{
	if (xhrUploadImage.readyState != 4)
		return; /* Nothing to do here! */

	var responsearray = index_js_parseResponseText(xhrUploadImage.responseText, false);
	if (!responsearray)
		return;

	if (responsearray['error'] != "INFO_IMAGE_UPLOAD_SUCCESS") {
		console.log("<?=__LINE__?> "+responsearray['error']);
		return;
	}

	/* Update the file space meters */
	if ($('popuplistmeter') != null) {
		$('popuplistmeter').value = responsearray['uploadimagefilespace'];
		$('popuplistmetertext').innerText = responsearray['uploadimagefilespacetext'];
	}

	phylum = responsearray['uploadimagephylum'];
	editor = responsearray['uploadimageposteditor'];
	if ((phylum != 'message') && (phylum != 'post') && (editor != 'messager')) {
		console.log("<?=__LINE__?> ERROR: uploadimagephylum="+phylum+" OR 'editor="+editor);
		return;
	}
	postid = responsearray['uploadimagepostid'];
	editordiv = 0;
	if (editor == "messager") {
		editordiv = $('messagerinputentry');
	} else {
		editordiv = index_editdescription;
	}
	var uploadmode = responsearray['uploadimagefileuploadmode'];

	var i;
	for (i = 0; i < responsearray['uploadedfile']['error'].length; i++) {
		if (responsearray['uploadedfile']['error'] != 0) {
			continue;
		}
		/* TODO: This is also used for profile images. Can this be a function instead? */
		uploadfileid = responsearray['uploadedfile']['fileid'][i];
		uploadedfilename = responsearray['uploadedfile']['name'][i];
		uploadedfileurlname = responsearray['uploadedfile']['urlname'][i];
		uploadfiletempid = responsearray['uploadedfile']['tempid'][i];
		uploadfilemime = responsearray['uploadedfile']['mime'][i];
		uploadfilesize = responsearray['uploadedfile']['size'][i];
		uploadfilewidth = responsearray['uploadedfile']['width'][i];
		uploadfileheight = responsearray['uploadedfile']['height'][i];
		uploadfiletinyimg = responsearray['uploadedfile']['tinyimg'][i];
		/* TODO: (x-sendfile vs. mod_rewrite is a known bug) FIXME01 Check .htaccess: filesrc = "file/"+uploadfileid+"/"+uploadedfileurlname; */

		fileid = uploadfileid; //+'-'+uploadedfilename;
		filesrc = "<?=$urlhome?>/index.php?fileid="+uploadfileid+"&filename="+uploadedfileurlname;
		if (editor == "messager") {
			index_editmessagefilesarray.push({'fileid':uploadfileid, 'filename':uploadedfilename, 'deletefile':0, 'newfile':1});
		} else {
			index_editpostfilesarray.push({'fileid':uploadfileid, 'filename':uploadedfilename, 'deletefile':0, 'newfile':1});
		}

		/* Handle inline images (if possible)... */
		if (uploadfilemime && uploadfilemime) {
			if (uploadfilemime.match('image.(gif|jpeg|jpg|png)')) {
				$(uploadfiletempid).src = filesrc;
				$(uploadfiletempid).className = 'postimg';
				$(uploadfiletempid).setAttribute('width', uploadfilewidth);
				$(uploadfiletempid).setAttribute('height', uploadfileheight);
				$(uploadfiletempid).setAttribute('lazy-src', uploadfiletinyimg);
				$(uploadfiletempid).style.background = 'center/cover url(\''+uploadfiletinyimg+'\');';
				$(uploadfiletempid).id = 'file'+fileid; /* Change this last */
				uploadfileid = 0; /* Clear as flag of successful upload */
				if (editor == "messager") {
					if (index_messagerfileuploadcount > 0)
						index_messagerfileuploadcount--;
					if (index_messagerfileuploadcount == 0) {
						index_js_deleteElementById('messageroverlayuploadprogress');
						$('messagerattach').disabled = false;
						$('messagerattach').style.opacity = 'unset';
						$('messagerattach').src = '/img/attach.png';
						$('messagersubmit').disabled = false;
						$('messagersubmit').style.opacity = 'unset';
					}
				} else {
					if (index_editpostfileuploadcount > 0)
						index_editpostfileuploadcount--;
					if (index_editpostfileuploadcount == 0) {
						$(editor+'fileupload').style.display = 'none';
						$(editor+'submit').disabled = false;
						$(editor+'submit').style.color = '#013';
						$(editor+'submit').style.cursor = 'pointer';
						$(editor+'submit').style.cursor = 'hand';
					}
				}
			}
		}

		/* ...otherwise, attach to bottom of post as a download */
		if (uploadfileid != 0) {
			var newattachmentdiv = document.createElement("div");
			newattachmentdiv.id = phylum+'_'+postid+'_attachment_'+fileid;
			newattachmentdiv.className = "postfile";
			newattachmentdiv.innerHTML = "<span class=\"tooltiphost\"><a href='"+filesrc+"'>"+uploadedfilename+"</a><span class=\"tooltiptext\">Download file</span></span> ("+uploadfilesize+")<span id='"+phylum+"_"+postid+"_attachmentcontrols_"+fileid+"'> <a id='"+phylum+"_"+postid+"_deleteattachment_"+fileid+"' class='postfiledelete' onclick='index_js_deleteAttachment(\""+phylum+"\", \""+postid+"\", \""+uploadfileid+"\", \""+uploadedfilename+"\", \""+editor+"\");'>Delete</a></span>";
			$(phylum+'attachments_'+postid).appendChild(newattachmentdiv);
		}
		if (editor == 'messager') {
			index_js_startMessage();
			/* If popup editor attachment, resize the editor after each attach. */
			index_js_updateMessage();
		}
	}
}

/**
 *
 */
function index_js_previewImage(editor)
{
	var files = $(editor+'file').files;
	var f, i;

	/* First, generate preview images so editing can continue. */
	for (i = 0; f = files[i]; i++) {
		/* NOTE: Checking file size here _could_ be spoofed to allow larger than permitted files to upload.
		 * It isn't a security measure, but a quicker way of letting the user know the file is too big
		 * instead of waiting for an oversized file to upload to the server and then have it refused. */
		if (f.size > index_editpostmaximumfilesize) {
			return false; /* continue; */
		}
		f.tempindex = i;
		if (f.type.match('image.(gif|jpeg|jpg|png)')) {
			if (editor == 'editpost') {
				if (index_editpostfileuploadcount == 0) {
					$(editor+'fileupload').style.display = 'inline';
					$(editor+'submit').disabled = true;
					$(editor+'submit').style.color = '#369';
					$(editor+'submit').style.cursor = 'wait';
				}
				index_editpostfileuploadcount++;
			} else if (editor == 'messager') {
				if (index_messagerfileuploadcount == 0) {
					let div = document.createElement('div');
					div.id = 'messageroverlayuploadprogress';
					div.innerText = "Uploading...";
					$('messageroverlay').appendChild(div);
					$('messagerattach').disabled = true;
					$('messagerattach').style.opacity = 0.3;
					$('messagerattach').src = '/img/attach-uploading.gif';
					$('messagersubmit').disabled = true;
					$('messagersubmit').style.opacity = 0.3;

				}
				index_messagerfileuploadcount++;
			}

			var reader = new FileReader();
			reader.onload = (function(currentFile) { /* TODO: Isolate function. */
				return function(e) {
					var newimg = new Image();
					newimg.id = 'uploadingfile'+currentFile.tempindex;
					newimg.className = 'postimg';
					newimg.src = e.target.result;

					if (editor == "editpost") {
						newimg.setAttribute('class', 'postimg'); /* Sets image zoom */
						var sel = window.getSelection();
						if (sel) {
							var range = sel.getRangeAt(0);
							range.deleteContents();
							range.collapse(true);
							range.insertNode(newimg);
							range.setStartAfter(newimg);
							range.collapse(true);
							sel.removeAllRanges();
							sel.addRange(range);
						}
					} else if (editor == "messager") {
						$('messagerinputentry').appendChild(newimg);
					}
					newimg.onload = function(){ console.log("<?=__LINE__?>: # # # # # # # # # newimg.id="+newimg.id); };
				};
			})(f);
			reader.readAsDataURL(f);
			//index_js_editorExecCommand("insertHTML", "js_previewImage");
		}
	}

	/* Upload to either post editor or popup editor. */
	phylum = '';
	postid = 0;
	if (editor == 'messager') {
		phylum = 'message';
		postid = -1;
	} else {
		phylum = index_editpostphylum;
		postid = index_editpostid;
	}
	fileuploadmode = index_editPostFileUploadMode;

	/* Next, upload each image and replace the previews with the uploaded images. */
	var uploadimageformdata = new FormData();
	for (i = 0; f = files[i]; i++) {
		/* NOTE: Checking file size here _could_ be spoofed to allow larger than permitted files to upload.
		 * It isn't a security measure, but a quicker way of letting the user know the file is too big
		 * instead of waiting for an oversized file to upload to the server and then have it refused. */
		if (f.size > index_editpostmaximumfilesize) {
			return false; /* continue; */
		}
		uploadimageformdata.append("uploadfiletempid[]", 'uploadingfile'+i);
		uploadimageformdata.append("uploadfile[]", f);
	}
	uploadimageformdata.append("uploadimagenewpostsessionid", index_newpostsessionid);
	uploadimageformdata.append("uploadimageusername", "<?=$index_activeusername?>");
	uploadimageformdata.append("uploadimagephylum", phylum);
	uploadimageformdata.append("uploadimagepostid", postid);
	uploadimageformdata.append("uploadimageposteditor", editor);
	uploadimageformdata.append("uploadimagefileuploadmode", fileuploadmode);
	uploadimageformdata.append("ajax_command", "upload_files");

	xhrUploadImage = new XMLHttpRequest();
	xhrUploadImage.onreadystatechange = index_js_uploadImage_AjaxFunction;
	xhrUploadImage.upload.addEventListener('progress', function(e){
		console.log("<?=__LINE__?> PROGRESS editor="+editor+" "+e.loaded+" of "+e.total+" loaded.");
		if (e.loaded < e.total) {
			$(editor+'suggestions').innerHTML = "Uploading: "+Math.floor((e.loaded / e.total) * 100)+"%";
		} else if (e.loaded == e.total) {
			index_js_emptyElementById(editor+'suggestions');
		}
	}, false);
	xhrUploadImage.open("POST", "/index.php", true);
	xhrUploadImage.send(uploadimageformdata);

	return false;
}

/**
 *
 */
function index_js_updateNewPost() {
	/* Update semantic formatting! */
}

/**
 *
 */
function index_js_editorExecCommand(format_command, format_argument)
{
	try {
		index_editdescription.focus();
		document.execCommand(format_command, false, format_argument);
		index_editdescription.focus();
		console.log("<?=__LINE__?> implement index_js_updateToolbar(editor_id);");
	} catch (e) {
		/* No action on error. */
	}
}

function index_js_editorKeyEvent(ev)
{
	let editdescription = ev.target;
	let phylum = "post";
	if (editdescription.id == "messagerinputentry") {
		phylum = "message";
	}

	/* Some browsers report Enter as 10 instead of 13 */
	if ((ev.type == "keydown") && ((ev.keyCode == 10) || (ev.keyCode == 13))) {
		ev.preventDefault();

		/* Prevent line breaks on empty editor */
		if (!editdescription.innerHTML) {
			return false;
		}

		/* Check to send on enter if instant message */
		if (phylum == "message") {
			var SendOnEnterKeyChecked = $('messagersendonenterkey').checked;
			if ((index_editmessageid != null) && SendOnEnterKeyChecked
					&& $('messagerinputentry').innerHTML) { /* Check for non-blank data */
					/* TODO: messager attachment check when description empty */
				$('messagersubmit').click();
				return false;
			}

		/* Save message edits */
		} else if ((index_editpostphylum == 'message') && index_editpostid) {
			ev.preventDefault();
			$('editpostsubmit').click();
			return false;

		/* Otherwise, Ctrl+Enter saves if editing a post */
		} else if ((ev.ctrlKey || ev.metaKey) && index_editpostid) {
			ev.preventDefault();
			$('editpostsubmit').click();
			return false;
		}

		/* Otherwise insert line breaks */
		if (window.getSelection) {
			var sel = window.getSelection();
			var range = sel.getRangeAt(0);
			var brNode = document.createElement('br');
			range.deleteContents();
			range.collapse(true);
			range.insertNode(brNode);
			range = range.cloneRange();
			if (brNode.nextSibling) {
				range.selectNodeContents(brNode.nextSibling);
			}
			range.collapse(true);
			sel.removeAllRanges();
			sel.addRange(range);
		} /* TODO: Impliment document.getSelection and document.selection.createRange for cross-browser compatibility. */

		/* Firefox (and possibly others) won't append line breaks
		 * unless the last child of contentEditable is <br> */
		var lastChildTag = editdescription.lastChild.nodeName.toLowerCase();
		if (lastChildTag != 'br') {
			editdescription.appendChild(document.createElement('br'));
		}

		editdescription.focus();

	} else if ((ev.type == "keydown") && (ev.keyCode == 27)) { /* 27 == Esc */
		if (phylum == "message") {
			ev.preventDefault();
			if ($('messagerinputentry').innerHTML) {
				index_js_emptyElementById('messagerinputentry');
			} else {
				index_js_popupEditorClose();
			}
		}
	} else if ((ev.type == "keyup") && (phylum == "message")) {
		let h = $('messagerinputentry').innerHTML;
		/* Clear contentEditable divs with only <br> tags */
		if (ev.target.innerHTML == "<br>") {
			ev.target.innerHTML = "";
		}

		/* Start new message if not already. */
		if ((index_editmessageid == null)
				&& $('messagerinputentry').innerHTML) {
			index_js_startMessage();

		/* Clear message if blank. */
		} else if ((index_editmessageid != null)
				&& (!$('messagerinputentry').innerHTML)) {
			index_editmessageid = null;
		}
	}
	return false;
}

function index_js_editorPasteEvent(ev)
{
	var plaintext = '';

	ev.preventDefault();
	if (ev.clipboardData || ev.originalEvent.clipboardData) {
		plaintext = (ev || ev.originalEvent).clipboardData.getData('text/plain');
	} else if (window.clipboardData) {
		plaintext = window.clipboardData.getData('Text');
	}

	if (document.queryCommandSupported('insertText')) {
		document.execCommand('insertText', false, plaintext);
	} else {
		document.execCommand('paste', false, plaintext);
	}
}

function index_js_editorDropEvent(ev)
{
	var plaintext = '';

	ev.preventDefault();
	plaintext = ev.dataTransfer.getData('text/plain');

	ev.target.focus();
	if (document.queryCommandSupported('insertText')) {
		document.execCommand('insertText', false, plaintext);
	} else {
		document.execCommand('paste', false, plaintext);
	}
}

/**
 *
 */
function index_js_addPostEditorEvents(editorDivNode)
{
	if (document.addEventListener) {
		editorDivNode.addEventListener('paste', index_js_editorPasteEvent, false);
		editorDivNode.addEventListener('drop', index_js_editorDropEvent, false);
		editorDivNode.addEventListener('keydown', index_js_editorKeyEvent, false);
		editorDivNode.addEventListener('keyup', index_js_editorKeyEvent, false);
	} else if (document.attachEvent) {
		editorDivNode.attachEvent('paste', index_js_editorPasteEvent);
		editorDivNode.attachEvent('drop', index_js_editorDropEvent);
		editorDivNode.attachEvent('keydown', index_js_editorKeyEvent);
		editorDivNode.attachEvent('keyup', index_js_editorKeyEvent);
	}
}

function index_js_removePostEditorEvents(editorDivNode)
{
	if (document.addEventListener) {
		editorDivNode.removeEventListener('paste', index_js_editorPasteEvent);
		editorDivNode.removeEventListener('drop', index_js_editorDropEvent);
		editorDivNode.removeEventListener('keydown', index_js_editorKeyEvent);
		editorDivNode.removeEventListener('keyup', index_js_editorKeyEvent);
	} else if (document.detachEvent) {
		editorDivNode.detachEvent('paste', index_js_editorPasteEvent);
		editorDivNode.detachEvent('drop', index_js_editorDropEvent);
		editorDivNode.detachEvent('keydown', index_js_editorKeyEvent);
		editorDivNode.detachEvent('keyup', index_js_editorKeyEvent);
	}
}

/**
 * Convenience function to add a message to a popup editor.
 * Returns the newly created message DIV that was just inserted.
 * TODO: Add recipient identifier to work with multiple messagers.
 */
function index_js_addMessage(id, phylum, permissions, recipients, previoususername,
		description, messagecredits, messagepopuplist, filesarray)
{
	/* Add the message div. */
	var messagediv = document.createElement("div");
	messagediv.id = "message_"+id;
	messagediv.className = "messageentry";
	messagediv.setAttribute('postpermissions', permissions);
	messagediv.setAttribute('postrecipients', recipients);
	messagediv.setAttribute('postprevioususername', previoususername.toLowerCase());

	/* Put description in a temporary element to extract its HTML later */
	let descriptionhost = document.createElement('div');
	let descriptionspan = document.createElement('span');
	descriptionspan.id = 'messagedescription_'+id;
	descriptionspan.className = 'messagedescription';
	descriptionspan.innerHTML = description;
	descriptionhost.appendChild(descriptionspan);

	var messagedescriptiondiv = document.createElement('div');
	messagedescriptiondiv.id = 'messagedescription_'+id;
	messagedescriptiondiv.className = 'messagecontent';
	messagedescriptiondiv.innerHTML = messagecredits + descriptionhost.innerHTML;
	messagediv.appendChild(messagedescriptiondiv);

	let messagemenudiv = document.createElement('div');
	messagemenudiv.id = 'messagemenudiv_'+id;
	messagemenudiv.innerHTML = messagepopuplist;
	messagediv.appendChild(messagemenudiv);

	/* Move any new attachments from the input field to the new message. */
	if (Array.isArray(filesarray)) {
		filesarray.forEach((fileid) => {
			attachmentid = `${phylum}_${id}_attachment_${fileid}`;
			tempattachmentid = `${phylum}_-1_attachment_${fileid}`;

			/* Change attachment div id from temporary to published. */
			if ($(tempattachmentid)) {
				$(tempattachmentid).id = attachmentid;
			}

			if ($(attachmentid)) {
				messagediv.appendChild($(attachmentid));
				index_js_updateMessage();
			}
		});
	}

	$('messagerbodyscroller').appendChild(messagediv);

	return messagediv;
}

/**
 *
 */
function index_js_sendNewPost_AjaxFunction()
{
	if (xhrSendPost.readyState != 4)
		return; /* Nothing to do here! */

	var responsearray = index_js_parseResponseText(xhrSendPost.responseText, false);
	if (!responsearray) {
		$('editpostsuggestions').innerHTML = "Server error. Try again.";
		return;
	}

	if ((responsearray['error'] != "INFO_NEW_POST_SAVED")
			&& (responsearray['error'] != "INFO_EDIT_POST_SAVED")
			&& (responsearray['error'] != "INFO_NEW_MESSAGE_SAVED")
			&& (responsearray['error'] != "INFO_EDIT_MESSAGE_SAVED")
			&& (responsearray['error'] != "INFO_EMAIL_SENT")) {
		console.log("PHP<?=__LINE__?>: Incomplete new post or message function!");
		$('editpostsuggestions').innerHTML = responsearray['error'];
		index_sendingpost = false; /* TODO: Check if error allows (eg: "try again") */
		return;
	}
	index_js_emptyElementById('editpostsuggestions');

	var phylum = responsearray['phylum'];
	var id = responsearray['newpostid'];
	var posttype = responsearray['posttype'];
	var parentid = responsearray['parentid'];
	var added = responsearray['added'];
	var memberlink = responsearray['memberlink'];
	var fullname = responsearray['fullname'];
	var postcredits = responsearray['postcredits'];
	var postrecipients = responsearray['postrecipients'];
	var postmembers = responsearray['postmembers'];
	var postpopuplist = responsearray['postpopuplist'];
	var description = responsearray['description'];
	var permissions = responsearray['permissions'];
	var recipients = responsearray['recipients'];
	var inlinereply = false;
	var postdiv;
	var addedpostpostcreditsdiv;
	var descriptiondiv = false;
	var ajaxcommand = responsearray['ajax_command'];
	var editlinktext = responsearray['editlinktext'];
	var fullname = responsearray['fullname'];
	var username = responsearray['username'];
	var viewmode = responsearray['viewmode'];

	/* Update the file space meters */
	if ($('popuplistmeter') != null) {
		$('popuplistmeter').value = responsearray['savepostfilespace'];
		$('popuplistmetertext').innerText = responsearray['savepostfilespacetext'];
	}

	if (responsearray['inlinereply'] == 1)
		inlinereply = true;

	/* Re-hide edit post controls */
	if ((ajaxcommand == "new_post") || (ajaxcommand == "new_postreply")
			|| (ajaxcommand == "edit_post") || (ajaxcommand == "edit_message")) {
		index_js_emptyElementById('editpostsuggestions');
		if ((ajaxcommand == "new_post") || (ajaxcommand == "new_postreply")) {
			$('postheader_NEWPOST').classList.remove('postheadersticky');
		} else if ((ajaxcommand != "edit_post") && (ajaxcommand != "edit_message")) {
			$('postheader_'+id).classList.remove('postheadersticky');
		}
		$('editpostcancel').style.display = "none";
		$('editpostcancel').style.position = "absolute";
		$('editpostcontrols').style.display = "none";
		$('post_NEWPOST').appendChild($('editpostcancel'));
		$('post_NEWPOST').appendChild($('editpostcontrols'));
	}

	if ((ajaxcommand == "new_post") || (ajaxcommand == "new_postreply")) {
		postdiv = document.createElement("div");
		postdiv.id = "post_"+id;
		var postblockdiv = document.createElement("div");
		postblockdiv.id = "postblock_"+id;
		if (viewmode == 'list') {
			postblockdiv.className = 'postblocklistentry';
		}
		postdiv.className = "debugpostclass "+responsearray['postreplystyle'];
		postdiv.appendChild(postblockdiv);

		var postheaderdiv = document.createElement('div');
		postheaderdiv.id = phylum+'header_'+id;
		postheaderdiv.className = 'postheader';
		postblockdiv.appendChild(postheaderdiv);

		let postcreditsdivcontent = postcredits;
		let postcreditsdivclassname = 'postcredits';
		if ((viewmode == 'full') || (viewmode == 'thumbnail')) {
			postcreditsdivcontent += postpopuplist;
			postcreditsdivclassname += ' postcreditsflex';
		}
		var postcreditsdiv = document.createElement("div");
		postcreditsdiv.id = phylum+"credits_"+id;
		postcreditsdiv.className = postcreditsdivclassname;
		postcreditsdiv.innerHTML = postcreditsdivcontent;
		postheaderdiv.appendChild(postcreditsdiv);

		if ((viewmode == 'full') || (viewmode == 'thumbnail')) {
			var postrecipientsdiv = document.createElement("div");
			postrecipientsdiv.id = phylum+"recipients_"+id;
			postrecipientsdiv.className = "recipientsdiv";
			postrecipientsdiv.innerHTML = postrecipients;
			postheaderdiv.appendChild(postrecipientsdiv);

			var postmembersdiv = document.createElement("div");
			postmembersdiv.id = phylum+"members_"+id;
			postmembersdiv.className = "postmembers";
			postmembersdiv.innerHTML = postmembers;
			postblockdiv.appendChild(postmembersdiv);

			var postflagdiv = document.createElement('div');
			postflagdiv.id = phylum+'flagdiv_'+id;
			postblockdiv.appendChild(postflagdiv);

			var postpendingdiv = document.createElement('div');
			postpendingdiv.id = phylum+'pending_'+id;
			if ((typeof responsearray['poststatus'] !== 'undefined')
					&& responsearray['poststatus']) {
				postpendingdiv.className = 'poststatuspending';
				postpendingdiv.innerHTML = responsearray['poststatus'];
			}
			postblockdiv.appendChild(postpendingdiv);
		}

		var descriptiondiv = document.createElement('div');
		descriptiondiv.id = phylum+"description_"+id;
		let classname = 'postdescription';
		if (viewmode == 'full') {
			classname += ' postdescriptionpadding';
		} else if (viewmode == 'list') {
			classname += ' postdescriptionlistentry';
		}
		descriptiondiv.className = classname;
		postblockdiv.appendChild(descriptiondiv);
		descriptiondiv.innerHTML = description; // was $(index_editpostdescriptionid).innerHTML;

		if (viewmode == 'full') {
			var postattachmentsdiv = document.createElement("div");
			postattachmentsdiv.id = phylum+"attachments_"+id;
			postattachmentsdiv.className = "attachmentsdiv";
			postblockdiv.appendChild(postattachmentsdiv);

			/* Move any attached files from the new post editor. */
			var fileslength = index_editpostfilesarray.length;
			for (var f = 0; f < fileslength; f++) {
				var fi = index_editpostfilesarray.shift();
				var fileid = fi['fileid'];
				var filename = fi['filename'];
				var deletefile = fi['deletefile'];
				var newfile = fi['newfile'];
				attachmentid = phylum+'_'+id+'_attachment_'+fileid;
				tempattachmentid = phylum+'_NEWPOST_attachment_'+fileid;

				/* Change attachment div id from temporary to published. */
				if ($(tempattachmentid)) {
					$(tempattachmentid).id = attachmentid;
				}

				if ($(attachmentid)) {
					postattachmentsdiv.appendChild($(attachmentid));
				}
			}
		} else if (viewmode == 'list') {
			/* Popup menu is its own element when posts viewed as list entries,
			 * instead of being a child of post credits */
			postblockdiv.insertAdjacentHTML('beforeend', postpopuplist);
		}

		var newpostdiv = index_editdescription.parentNode.parentNode;
		var isreply;
		if (inlinereply) {
			var postreplyblock = document.createElement('div');
			postreplyblock.id = "postreplyblock_"+id;
			postreplyblock.className = "postreplyblock";
			postdiv.appendChild(postreplyblock);

			var postreplyspan = document.createElement('span');
			postreplyspan.className = "fadetext";
			postreplyblock.appendChild(postreplyspan);

			var viewpostlink = document.createElement('a');
			viewpostlink.innerText = "View Page";
			viewpostlink.setAttribute('href', "/"+id);
			postreplyspan.appendChild(viewpostlink);

			var replyeditor = $('newpostreply_'+parentid);
			replyeditor.parentNode.insertBefore(postdiv, replyeditor);
		} else {
			index_js_deleteElementById('emptyhomepage');
			isreply = 'false';
			$('mainflexbox').insertBefore(postdiv, $('mainflexbox').firstChild);

			var postdivlocation = document.createElement('div');
			postdivlocation.id = "postlocation_"+id;
			$('mainflexbox').insertBefore(postdivlocation, postdiv);

			if (index_viewmode == 'full') {
				var addedpostrepliesdiv = document.createElement('div');
				addedpostrepliesdiv.id = phylum+"replyblock_"+id;
				addedpostrepliesdiv.className = "postreplies";
				postdiv.appendChild(addedpostrepliesdiv);

				var newpostreplydiv = document.createElement('div');
				newpostreplydiv.id = "newpostreply_"+id;
				newpostreplydiv.className = "postreplybar";
				addedpostrepliesdiv.appendChild(newpostreplydiv);

				var newreplyspan = document.createElement('span');
				newreplyspan.className = "fadetext";
				newreplyspan.innerHTML = "<a href=\"/"+id+"\">View Page</a> &bull; "
						+"<a onclick=\"index_js_setupPostEditor(this, 'newpostreply_"+id+"',"
						+" 'post', "+id+", true, '504404', 3, '', '', '',"
						+" 'full', '', false);\">Reply</a>";
				newpostreplydiv.appendChild(newreplyspan);
			}
		}

		/* Reset the editor (for new posts only) */
		index_editdescription.contentEditable = "false";
		index_js_removePostEditorEvents(index_editdescription);
		index_js_emptyElementById(index_editdescription.id);

		/* Restore content of new post description divs. */
		if (index_editpostid == index_newpostid) {
			index_js_revertPostEditor();
			index_editposthostdiv.style.paddingTop = '';
		}

		index_editdescription.style.marginTop = "4px";
	} else if (ajaxcommand == "new_message") {
		var messagecredits = responsearray['postcredits'];
		var messagepopuplist = responsearray['postpopuplist'];
		var description = $('messagerinputentry').innerHTML;

		/* Move any new attachments from the input field to the new message. */
		var fileslength = index_editmessagefilesarray.length;
		let filesarray = [];
		for (var f = 0; f < fileslength; f++) {
			var fi = index_editmessagefilesarray.shift();
			var fileid = fi['fileid'];
			var filename = fi['filename']; /* Unused? */
			var deletefile = fi['deletefile']; /* Unused? */
			var newfile = fi['newfile']; /* Unused? */

			filesarray.push(fileid);
		}

		$('messagerbodyscroller').insertAdjacentHTML('beforeend',
				responsearray['messageHTML']);

		/*
		postdiv = index_js_addMessage(id, phylum, permissions, recipients, username,
				description, messagecredits, messagepopuplist, filesarray);
		// */

		$('messagerbodyscroller').scrollTop = $('messagerbodyscroller').scrollHeight - $('messagerbodyscroller').clientHeight;
	} else if ((ajaxcommand == "edit_post") || (ajaxcommand == "edit_message")) {
		postdiv = $(phylum+'_'+id);
		descriptiondiv = $(phylum+'description_'+id);
		descriptiondiv.contentEditable = "false";
		descriptiondiv.innerHTML = description;
		descriptiondiv.blur();
		index_js_removePostEditorEvents(descriptiondiv);

		if (ajaxcommand == "edit_message") {
			let popuplistdiv = $('messagepopuplist_'+id);
			popuplistdiv.style.display = 'block';
		} else {
			recipientsdiv = $(phylum+'recipients_'+id)
			if (recipientsdiv) {
				recipientsdiv.classList.remove('shadowinset');
				recipientsdiv.innerHTML = postrecipients;
			}

			creditsdiv = $(phylum+'credits_'+id);
			if (creditsdiv) {
				creditsdiv.innerHTML = postcredits + postpopuplist;
			}
			membersdiv = $(phylum+'members_'+id);
			if (membersdiv) {
				membersdiv.innerHTML = postmembers;
			}
		}

<?php
if ($index_page == 'page') {
?>
		/* Only update browser title if title post. */
		if ((ajaxcommand == "edit_post") && (id == <?=sprintf("%d", $index_page_id)?>)) {
			document.title = responsearray['title'];
		}
<?php
}
?>
	}

	if ((ajaxcommand == "new_post") || (ajaxcommand == "edit_post")) {
		postdiv.setAttribute('postid', id);
		postdiv.setAttribute('postpermissions', responsearray['permissions']);
		postdiv.setAttribute('postviewmode', responsearray['defaultviewmode']);
		postdiv.setAttribute('postsortmode', responsearray['defaultsortmode']);
		postdiv.setAttribute('postsortreverse', responsearray['defaultsortreverse']);
		postdiv.setAttribute('postagerating', responsearray['agerating']);
		postdiv.setAttribute('postrecipients', responsearray['recipients']);
	}

	/* Handle newly uploaded and deleted files here... */
	/* Remove delete attachment links. (Duplicate code used in Cancel Post function) */
	if ((typeof responsearray['files'] !== 'undefined')
			&& (responsearray['files'].length > 0)) {
		fileslength = responsearray['files'].length;
		for (f = 0; f < fileslength; f++) {
			var fileid = responsearray['files'][f]['fileid'];
			var filename = responsearray['files'][f]['filename'];
			var deletefile = responsearray['files'][f]['deletefile'];

			if (ajaxcommand == 'new_message') {
				index_js_deleteElementById(phylum+'_-1_deleteattachment_'+fileid);
				index_js_deleteElementById(phylum+'_-1_attachmentcontrols_'+fileid);
			} else if (ajaxcommand == 'new_post') {
				index_js_deleteElementById(phylum+'_NEWPOST_deleteattachment_'+fileid);
				index_js_deleteElementById(phylum+'_NEWPOST_attachmentcontrols_'+fileid);
			} else {
				index_js_deleteElementById(phylum+'_'+id+'_deleteattachment_'+fileid);
				index_js_deleteElementById(phylum+'_'+id+'_attachmentcontrols_'+fileid);
			}
			if (deletefile == 1) {
				index_js_deleteElementById(phylum+'_'+id+'_attachment_'+fileid);
			}
		}
	}

	/* Post-posting/editor reset functions go here... */
	if ((ajaxcommand == "new_message")) {
		index_js_emptyElementById('messagerinputentry');
		index_js_emptyElementById('messageattachments_-1'); /* -1 == NEWPOST */
		$('messagerinputentry').contentEditable = "true";
		$('messagerattach').disabled = false;
		$('messagersubmit').disabled = false;
		$('messagerinputentry').classList.remove('messagerinputentrydisabled');
		index_editmessageid = null;
		index_sendingmessage = false;
		index_js_updateMessage();

		if ($('messagercloseonsend').checked) {
			index_js_popupEditorClose();
		}
		$('messagerinputentry').focus();
	}

	if (ajaxcommand != 'new_message') {
		index_editpostid = false;
		index_editpostphylum = false;
		index_editparentid = false;
		index_editpostdescriptionid = false;
		index_sendingpost = false;
	}

	index_editpostfilesarray.length = 0;
	index_editpostrecipientsarray.length = 0;
}

function index_js_editPostAttach()
{
	index_editPostFileUploadMode = 'attach';
	$('editpostfile').click();
	index_editdescription.focus();

	return false; /* Disable submit action */
}

function index_js_editPostHTML()
{
	/* Choose to display either HTML source or rich text. */
	if (index_editposthtmlview) {
		/* Revert from HTML source view. */
		index_editdescription.innerHTML = $('editposthtmleditor').value;
		index_editdescription.style.display = 'block';

		index_editdescription.style.display = 'block';
		index_editdescription.focus();
		$('editposthtmleditor').style.display = 'none';
		index_editposthtmlview = false;
	} else {
		/* Display HTML source. */
		$('editposthtmleditor').value = index_editdescription.innerHTML;

		let ht = index_editdescription.clientHeight+'px';
		$('editposthtmleditor').style.display = 'block';
		$('editposthtmleditor').style.height = ht;
		$('editposthtmleditor').focus();
		index_editdescription.style.display = 'none';
		index_editposthtmlview = true;
	}

	/* Update the <html> toolbar button. */
	if (index_editposthtmlview) {
		$('editposthtmlbutton').style.background = '#eee';
	} else {
		$('editposthtmlbutton').style.background = 'none';
	}

	return false; /* Disable submit action */
}

function index_js_editPostUndo()
{
	index_js_editorExecCommand('undo', 0);
	return false;
}

function index_js_editPostRedo()
{
	index_js_editorExecCommand('redo', 0);
	return false;
}

function index_js_clearSelection()
{
	/* Clear hilighting. */
	var sel = false;
	if (window.getSelection)
		sel = window.getSelection();
	else if (document.selection)
		sel = document.selection;

	if (sel) {
		if (sel.removeAllRanges) {
			var range = document.createRange();
			range.selectNodeContents(index_editdescription);
			range.collapse(false);
			sel.removeAllRanges();
			sel.addRange(range);
		} else if (sel.empty) {
			sel.empty();
		}
	}
}

/* _setLink is needed to prevent accidental link clicks on save */
function index_js_insertScrapedLink_setLink()
{
	index_editpostdescriptionlink.href = index_editpostdescriptionlinkhref;
	index_editpostdescriptionlink.target = "_blank";
	index_editpostdescriptionlink = null;
	index_editpostdescriptionlinkhref = null;
}

function index_js_insertScrapedLink_controls(id)
{
	if (id == 'next') {
		index_editpostdescriptionimgindex++;
		if (index_editpostdescriptionimgindex > index_editpostdescriptionimgarray.length - 1)
			index_editpostdescriptionimgindex = 0;
		$('linkimage').src = index_editpostdescriptionimgarray[index_editpostdescriptionimgindex]['imgsrc'];
	} else if (id == 'prev') {
		index_editpostdescriptionimgindex--;
		if (index_editpostdescriptionimgindex < 0)
			index_editpostdescriptionimgindex = index_editpostdescriptionimgarray.length - 1;
		$('linkimage').src = index_editpostdescriptionimgarray[index_editpostdescriptionimgindex]['imgsrc'];
	} else if (id == 'none') {
		index_js_deleteElementById('linkimagediv');
	} else if (id != 'save') {
		return false;
	}

	index_js_clearSelection();
	if ((id == 'next') || (id == 'prev')) {
		return false;
	}

	if (($('linkimage') != null) && (typeof $('linkimage') !== undefined)) {
		$('linkimagediv').removeAttribute('class');
		$('linkimagediv').removeAttribute('id');
		$('linkimage').id = 'file_'+index_editpostdescriptionimgarray[index_editpostdescriptionimgindex]['fileid'];
		index_js_deleteElementById('newlinkbuttondiv');
		index_js_deleteElementById('newlinkclear');
	}
	index_editdescription.contentEditable = true;
	index_editdescription.focus();
	index_editpostdescriptionimgarray = false;
	index_editpostdescriptionimglinkdiv = false;
	index_editpostdescriptionlink.removeAttribute('class');

	if (document.createRange) {
		var sel = window.getSelection();
		var range = document.createRange();
		range.selectNodeContents(index_editdescription);
		range.collapse(false);
		sel.removeAllRanges();
		sel.addRange(range);
	} /* TODO: Impliment document.getSelection and document.selection.createRange for cross-browser compatibility. */
	index_editdescription.appendChild(document.createElement('br'));

	/* Delay setting href to prevent accidental link clicks */
	if (id == 'none') {
		setTimeout(function(){index_js_insertScrapedLink_setLink();}, 100);
	} else {
		index_js_insertScrapedLink_setLink();
	}

	return false;
}

function index_js_insertScrapedLink_ajaxFunction()
{
	if (xhrScrapeLink.readyState != 4)
		return;

	responsearray = index_js_parseResponseText(xhrScrapeLink.responseText, false);
	if (!responsearray)
		return;

	if (responsearray['error'] != "INFO_SCRAPE_URL_SUCCESS") {
		console.log("<?=__LINE__?> What is this? "+responsearray['error']);
		return false;
	}

	var newdiv = document.createElement('div');
	index_editpostdescriptionimglinkdiv = newdiv;
	newdiv.contentEditable = false;
	newdiv.draggable = true;
	newdiv.className = "postdescriptionscrape";
	index_js_emptyElementById(index_editdescription.id);
	index_editdescription.insertBefore(newdiv, index_editdescription.firstChild);

	/* Wrap all child elements in an a:link */
	var newdivlink = document.createElement('a');
	newdivlink.className = "postdescriptionscrapenolink";
	newdiv.appendChild(newdivlink);
	index_editpostdescriptionlink = newdivlink;
	index_editpostdescriptionlinkhref = responsearray['href'];

	/* Set up title */
	if (responsearray['title']) {
		var urltitle = document.createElement('h3');
		urltitle.innerHTML = responsearray['title'];
		newdivlink.appendChild(urltitle);
	}

	/* Url link */
	var urldiv = document.createElement('div');
	urldiv.className = "postdescriptionscrapeurl";
	if ((typeof responsearray['icon'] !== 'undefined')
			&& (typeof responsearray['icon']['fileid'] !== 'undefined')
			&& (typeof responsearray['icon']['filename'] !== 'undefined')) {
		var iconfileid = responsearray['icon']['fileid'];
		var iconfilename = responsearray['icon']['filename'];
		var iconsrc = "/index.php?fileid="+iconfileid+"&filename="+iconfilename;

		index_editpostfilesarray.push({'fileid':iconfileid, 'filename':iconfilename, 'deletefile':0, 'newfile':1});

		var urldivicon = document.createElement('img');
		urldivicon.id = "file"+iconfileid;
		urldivicon.src = iconsrc;
		urldiv.appendChild(urldivicon);
	}
	urldiv.appendChild(document.createTextNode(responsearray['href']));
	newdivlink.appendChild(urldiv);

	/* Description (if available) appended outside of link */
	if ((typeof responsearray['og_description'] !== 'undefined') && responsearray['og_description']) {
		var descriptiondiv = document.createElement('div');
		descriptiondiv.innerHTML = responsearray['og_description'];
		newdiv.appendChild(descriptiondiv);
	}

	/* Set up url image controls.
	 * If no images display with link, exit and return to editing. */
	if (typeof responsearray['scrapedimage'] === 'undefined') {
		index_js_insertScrapedLink_controls('none');
		return false;
	}

	var i;
	index_editpostdescriptionimgarray = [];
	for (i = 0; i < responsearray['scrapedimage']['fileid'].length; i++) {
		var scrapedfileid = responsearray['scrapedimage']['fileid'][i];
		var scrapedfilename = responsearray['scrapedimage']['filename'][i];

		var imgsrc = "/index.php?fileid="+scrapedfileid+"&filename="+scrapedfilename;
		index_editpostdescriptionimgarray.push({'fileid':scrapedfileid, 'imgsrc':imgsrc});
		index_editpostfilesarray.push({'fileid':scrapedfileid, 'filename':scrapedfilename, 'deletefile':0, 'newfile':1});
	}
	if (i <= 0) {
		index_editpostdescriptionimgarray = false;
	}

	var urlimagebuttonnone = document.createElement('a');
	var urlimagebuttonprev = document.createElement('a');
	var urlimagebuttonnext = document.createElement('a');
	urlimagebuttonnone.innerHTML = "Hide";
	urlimagebuttonprev.innerHTML = "&lt; Prev";
	urlimagebuttonnext.innerHTML = "Next &gt;";
	urlimagebuttonnone.className = "urldivbutton";
	urlimagebuttonprev.className = "urldivbutton";
	urlimagebuttonnext.className = "urldivbutton";
	urlimagebuttonnone.onmousedown = index_js_clearSelection;
	urlimagebuttonprev.onmousedown = index_js_clearSelection;
	urlimagebuttonnext.onmousedown = index_js_clearSelection;
	urlimagebuttonnone.onclick = function(){index_js_insertScrapedLink_controls('none');}
	urlimagebuttonprev.onclick = function(){index_js_insertScrapedLink_controls('prev');}
	urlimagebuttonnext.onclick = function(){index_js_insertScrapedLink_controls('next');}

	var urlimagebuttondiv = document.createElement('div');
	urlimagebuttondiv.id = "newlinkbuttondiv";
	urlimagebuttondiv.appendChild(urlimagebuttonnone);
	urlimagebuttondiv.appendChild(urlimagebuttonprev);
	urlimagebuttondiv.appendChild(urlimagebuttonnext);
	urlimagebuttondiv.style = "position:absolute;top:4px;left:4px;background:rgba(0,0,0,0.3);color:#fff;padding:4px;";

	/* Set up url image */
	var urlimageimg = document.createElement('img');
	urlimageimg.src = index_editpostdescriptionimgarray[0]['imgsrc'];
	urlimageimg.id = "linkimage";
	urlimageimg.className = "postdescriptionscrapeimg";

	var urlimagediv = document.createElement('div');
	urlimagediv.id = "linkimagediv";
	urlimagediv.className = "linkimagediv";
	urlimagediv.appendChild(urlimagebuttondiv);
	urlimagediv.appendChild(urlimageimg);
	newdivlink.insertBefore(urlimagediv, urldiv);
}

function index_js_insertScrapedLink(url)
{
	index_editdescription.innerHTML = "<i>Gathering link information</i>&nbsp;<img src=\"/img/thinking12.gif\">";

	var scrapelinkformdata = new FormData();
	scrapelinkformdata.append("url", url);
	scrapelinkformdata.append("ajax_command", "scrape_link");

	xhrScrapeLink = new XMLHttpRequest();
	xhrScrapeLink.onreadystatechange = index_js_insertScrapedLink_ajaxFunction;
	xhrScrapeLink.open("POST", "/index.php", true);
	xhrScrapeLink.send(scrapelinkformdata);
}

function index_js_editPostPasteLink()
{
	var linkLocation = prompt("Enter a URL:", "https://");
	if (linkLocation == null) {
		return false;
	}

	var sel = window.getSelection();
	if ((sel.isCollapsed) && (index_editdescription.innerHTML != '<br>')) {
		var alink = document.createElement('a');
		var linktext = document.createTextNode(linkLocation);
		var range = sel.getRangeAt(0);
		alink.setAttribute('href', linkLocation);
		alink.appendChild(linktext);
		range.deleteContents();
		range.insertNode(alink);
	} else if ((sel.anchorNode != null) && ((sel.anchorNode.id == "postdescription_"+index_editpostid)
			|| (sel.anchorNode.parentNode.id == "postdescription_"+index_editpostid))) {
	} else {
		return false;
	}

	if ((linkLocation != null) && (linkLocation != "")) {
		/* Only scrape link if blank document AND signed in. */
		if ((<?=$index_activeuserid?> != 0) && (index_editdescription.innerHTML == '<br>')) {
			index_js_insertScrapedLink(linkLocation);
		} else {
			index_js_editorExecCommand('createlink', linkLocation);
		}
		index_editdescription.focus();
	}
	return false;
}

/**
 * Sends new posts, as well as edited posts and messages.
 */
function index_js_sendNewPost(ev)
{
	/* Preflight check before sending post data */
	if (index_editposthtmlview) {
		index_js_editPostHTML();
	}

	/* Checks done; send it now */
	var newpostformdata = new FormData();
	var ajax_command = ev.querySelector('input[name=ajax_command]').value;

	if ((ajax_command == "new_message")) {
		if (index_sendingmessage || (index_editmessageid == null))
			return false; /* Prevent "double-" or "ghost-" posting. */

		index_sendingmessage = true;
		/* newpostformdata.append("messageusername", "<?=$index_activeusername?>"); */
		$('messagerinputentry').contentEditable = false;
		$('messagerattach').disabled = true;
		$('messagersubmit').disabled = true;
		$('messagerinputentry').classList.add('messagerinputentrydisabled');
		newpostformdata.append("editpostid", index_editmessageid);
		newpostformdata.append("newpostdescription", $('messagerinputentry').innerHTML);
		newpostformdata.append("newpostrecipients", $('messagerrecipient').value); /* was index_editmessagerecipientsarray.join(' ')); */
		// newpostformdata.append('newpostagerating', $('messageragerating').value);
		newpostformdata.append("newpostpostid", $('messagerpostid').value);
		newpostformdata.append("messagesendonenterkey", $('messagersendonenterkey').checked);
		newpostformdata.append("messagecloseonsend", $('messagercloseonsend').checked);
	} else if ((ajax_command == "new_post") || (ajax_command) == "new_postreply"
			|| (ajax_command == "edit_post") || (ajax_command == "edit_message")) {
		if (index_sendingpost || !index_editpostid)
			return false; /* Prevent "double-" or "ghost-" posting. */

		index_sendingpost = true;
		$('editpostsuggestions').innerHTML = "<img src=\"/img/thinking12.gif\" width=\"12\" height\"12\">";
		if (index_editpostphylum != 'message') {
			let text = $(index_editpostphylum+'recipientsEntry_'+index_editpostid).value;
			if (text && (!index_editpostrecipientsarray.includes(text))) {
				index_editpostrecipientsarray.push(text);
			}
		}

		newpostformdata.append("newpostpagetype", index_editpostpagetype);
		newpostformdata.append("editpostid", index_editpostid);
		newpostformdata.append("editpostshowparent", index_editpostshowparent);
		newpostformdata.append("newpostpostid", false);
		newpostformdata.append("newpostrecipients", index_editpostrecipientsarray.join(' '));
		newpostformdata.append('newpostagerating', $('editpostagerating').value);
		newpostformdata.append("inlinereply", index_inlinereply);
		newpostformdata.append("newpostdescription", index_editdescription.innerHTML);
	} else {
		console.log("<?=__LINE__?> post/message invalid or unimplemented ajax_command ("+ajax_command+")");
		return false;
	}

	/* Success! Keep going... */

	var permissions = '';
	var viewmode = '';
	var sortmode = '';
	var sortreverse = 'false';
	if ((ajax_command == "new_post") || (ajax_command == "new_postreply")
			|| (ajax_command == "edit_post") || (ajax_command == "edit_message")) {
		permissions = index_js_getEditorPermissions('editpost');
		/* Set a default view mode if not already set */
		if (!document.querySelector('input[name=editpostviewmode]:checked').value) {
			$('editpostviewmodefull').checked = true;
		}
		viewmode = document.querySelector('input[name=editpostviewmode]:checked').value;
		sortmode = document.querySelector('input[name=editpostsortmode]:checked').value;
		if ($('editpostsortreverse').checked) {
			sortreverse = 'true';
		}
	} else if (ajax_command == "new_message") {
		permissions = index_js_getEditorPermissions('messager');
	}
	var viewpermissions = permissions[0];
	var editpermissions = permissions[1];
	var replypermissions = permissions[2];
	var approvepermissions = permissions[3];
	var moderatepermissions = permissions[4];
	var tagpermissions = permissions[5];

	newpostformdata.append("ajax_command", ajax_command);
	newpostformdata.append("editlinktext", index_editlinktext);
	newpostformdata.append("newpostparentid", index_editparentid);
	newpostformdata.append("newpostviewpermissions", viewpermissions);
	newpostformdata.append("newposteditpermissions", editpermissions);
	newpostformdata.append("newpostreplypermissions", replypermissions);
	newpostformdata.append("newpostapprovepermissions", approvepermissions);
	newpostformdata.append("newpostmoderatepermissions", moderatepermissions);
	newpostformdata.append("newposttagpermissions", tagpermissions);
	newpostformdata.append("newpostviewmode", index_editpostviewmode);
	newpostformdata.append("newpostdefaultviewmode", viewmode);
	newpostformdata.append("newpostdefaultsortmode", sortmode);
	newpostformdata.append("newpostdefaultsortreverse", sortreverse);

	/* Add info for file attachments and images, to set permissions on post publish. */
	var filesarray = null;
	if (ajax_command == "new_message") {
		filesarray = index_editmessagefilesarray;
	} else {
		filesarray = index_editpostfilesarray;
	}
	for (var i = 0, ct = filesarray.length; i < ct; i++) {
		newpostformdata.append("newpostfileid[]", filesarray[i]['fileid']);
		newpostformdata.append("newpostfilename[]", filesarray[i]['filename']);
		newpostformdata.append("newpostdeletefile[]", filesarray[i]['deletefile']);
		newpostformdata.append("newpostfilewasuploaded[]", filesarray[i]['newfile']);
	}

	xhrSendPost = new XMLHttpRequest();
	xhrSendPost.onreadystatechange = index_js_sendNewPost_AjaxFunction;
	xhrSendPost.open("POST", "/index.php", true);
	xhrSendPost.send(newpostformdata);

	return false;
}

/**
 *
 */
function index_js_revertPostEditor()
{
	if (index_editpostphylum != 'message') {
		index_js_setEditorRecipients(index_editpostid, index_editpostphylum, false, false);
	}

	if (index_editpostid && (index_editpostid != "NEWPOST")
			&& (index_editpostid != "NEWREPLY")) {
		if (index_editpostphylum != 'message') {
			index_editrecipientsdiv.innerHTML = index_originalrecipients;
		}
		index_editdescription.innerHTML = index_originaldescription;
		$(index_editpostphylum+'popuplist_'+index_editpostid).style.display = "block";
		if ($(index_editpostphylum+'sharemenu_'+index_editpostid)) {
			$(index_editpostphylum+'sharemenu_'+index_editpostid).style.display = "block";
		}
	} else { // if (((index_editpostid == "NEWREPLY") || (index_editpostid == 0)) && (index_editparentid > 0))
		/* Restore "New Message/Reply"-style posts. */
		index_js_emptyElementById(index_editposthostdiv.id);
		index_editposthostdiv.innerHTML = index_originalhostdivhtml;
		index_editposthostdiv.style.paddingTop = '';
	}
}

/**
 * Returns the 6-character permissions string for the given editor.
 * - editor: string of either 'editpost' or 'messager'
 */
function index_js_getEditorPermissions(editor)
{
	var viewpermissions = null, editpermissions = null;
	var replypermissions = null, approvepermissions = null;
	var moderatepermissions = null, tagpermissions = null;

	if (editor == 'editpost') {
		if (document.querySelector('input[name=editpostview]:checked') != null)
			viewpermissions = document.querySelector('input[name=editpostview]:checked').value;
		if (document.querySelector('input[name=editpostedit]:checked') != null)
			editpermissions = document.querySelector('input[name=editpostedit]:checked').value;
		if (document.querySelector('input[name=editpostreply]:checked') != null)
			replypermissions = document.querySelector('input[name=editpostreply]:checked').value;
		if (document.querySelector('input[name=editpostapprove]:checked') != null)
			approvepermissions = document.querySelector('input[name=editpostapprove]:checked').value;
		if (document.querySelector('input[name=editpostmoderate]:checked') != null)
			moderatepermissions = document.querySelector('input[name=editpostmoderate]:checked').value;
		if (document.querySelector('input[name=editposttag]:checked') != null)
			tagpermissions = document.querySelector('input[name=editposttag]:checked').value;
	} else if (editor == 'messager') {
		if (document.querySelector('input[name=messagerview]:checked') != null)
			viewpermissions = document.querySelector('input[name=messagerview]:checked').value;
		if (document.querySelector('input[name=messageredit]:checked') != null)
			editpermissions = document.querySelector('input[name=messageredit]:checked').value;
		if (document.querySelector('input[name=messagerreply]:checked') != null)
			replypermissions = document.querySelector('input[name=messagerreply]:checked').value;
		if (document.querySelector('input[name=messagerapprove]:checked') != null)
			approvepermissions = document.querySelector('input[name=messagerapprove]:checked').value;
		if (document.querySelector('input[name=messagermoderate]:checked') != null)
			moderatepermissions = document.querySelector('input[name=messagermoderate]:checked').value;
		if (document.querySelector('input[name=messagertag]:checked') != null)
			tagpermissions = document.querySelector('input[name=messagertag]:checked').value;
	} else {
		return; /* Invalid editor! */
	}

	if ((viewpermissions < 0) || (viewpermissions > 5) || !viewpermissions) viewpermissions = 0; /* Owner default */
	if ((editpermissions < 0) || (editpermissions > 5) || !editpermissions) editpermissions = 0;
	if ((replypermissions < 0) || (replypermissions > 5) || !replypermissions) replypermissions = 0;
	if ((approvepermissions < 0) || (approvepermissions > 5) || !approvepermissions) approvepermissions = 0;
	if ((moderatepermissions < 0) || (moderatepermissions > 5) || !moderatepermissions) moderatepermissions = 0;
	if ((tagpermissions < 0) || (tagpermissions > 5) || !tagpermissions) tagpermissions = 0;
	permissions = '' + viewpermissions + editpermissions + replypermissions + approvepermissions + moderatepermissions + tagpermissions;
	return permissions;
}

/**
 * Sets the editor permissions for either posts or the popup messager.
 * - editor: string of either 'editpost' or 'messager'.
 * - viewmode: string of either 'full', 'list', or 'thumbnail'.
 * - sortmode: string of either 'date' or 'title'.
 * - sortreverse: boolean.
 */
function index_js_setEditorPermissions(editor, permissions, agerating,
		viewmode, sortmode, sortreverse)
{
	/* Prevent getting in to endless 'update loops' */
	index_manualpostpermissionsupdate = 1;

	/* Default to owner with invalid permissions */
	if (permissions.length != 6) {
		permissions = '000000';
	}
	let viewpermissions = permissions[0];
	let editpermissions = permissions[1];
	let replypermissions = permissions[2];
	let approvepermissions = permissions[3];
	let moderatepermissions = permissions[4];
	let tagpermissions = permissions[5];
	/* Set owner default for any invalid permissions */
	if ((viewpermissions < 0) || (viewpermissions > 5) || !viewpermissions)
		viewpermissions = 0;
	if ((editpermissions < 0) || (editpermissions > 5) || !editpermissions)
		editpermissions = 0;
	if ((replypermissions < 0) || (replypermissions > 5) || !replypermissions)
		replypermissions = 0;
	if ((approvepermissions < 0) || (approvepermissions > 5) || !approvepermissions)
		approvepermissions = 0;
	if ((moderatepermissions < 0) || (moderatepermissions > 5) || !moderatepermissions)
		moderatepermissions = 0;
	if ((tagpermissions < 0) || (tagpermissions > 5) || !tagpermissions)
		tagpermissions = 0;

	permissions = '' + viewpermissions + editpermissions + replypermissions
			+ approvepermissions + moderatepermissions + tagpermissions;

	/* Find out whether to display presets or advanced permissions */
	let advancedenabled = true;
	switch (permissions) {
	case '505505': /* Public, Accept all including guest and emails */
	case '505000': /* Public, Replies need approval */
	case '504404': /* Public, All signed in users can reply */
	case '504000': /* Public, Sign in users can reply but need approval */
	case '500000': /* Public, Replies disabled */
	case '202202': /* Starred Contacts can view and reply */
	case '202000': /* Starred Contacts can view but replies need approval */
	case '200000': /* Starred Contacts can view but not reply */
	case '101101': /* Recipient can view and reply */
	case '101000': /* Recipient can view but replies need approval */
	case '100000': /* Recipient can reply but not reply */
	case '000000': /* Only owner can view */
		advancedenabled = false;
		break;
	}
	$(editor+"advancedpermissions").checked = advancedenabled;

	if (advancedenabled) {
		$(editor+'permissionsiconadvanced').style.display = 'inline';
		$(editor+'permissionspresets').style.display = 'none';
		$(editor+'permissionsadvanced').style.display = 'block';
		$(editor+'advancedpermissionstooltip').innerHTML = 'Click for permission presets.';
	} else {
		$(editor+'permissionsiconadvanced').style.display = 'none';
		$(editor+'permissionspresets').style.display = 'block';
		$(editor+'permissionsadvanced').style.display = 'none';
		$(editor+'advancedpermissionstooltip').innerHTML = 'Click for advanced permissions.';

		/* Set "Replies" permission preset. Note that reply permissions
		 * are ignored if view permission is set to owner (0) */
		if ((permissions[2] == '0') && (permissions[0] != '0')) {
			$(editor+'presetdisablereplies').checked = true;
		} else if ((permissions[3] == '0') && (permissions[0] != '0')) {
			$(editor+'presetrepliesneedapproval').checked = true;
		} else {
			$(editor+'presetacceptreplies').checked = true;
		}
	}

	switch (permissions[0]) {
	case '5':
		$('editpostpermissionsicon').src = '/img/icon-permissions-open.png';
		$('editpostpermissionstitle').innerText = 'Anyone';
		if (permissions[2] == '5')
			$(editor+'allowunsigned').checked = true;
		else
			$(editor+'allowunsigned').checked = false;
		$(editor+'preset5').checked = true;
		break;

	case '4':
		$('editpostpermissionsicon').src = '/img/icon-permissions-all.png';
		$('editpostpermissionstitle').innerText = 'All Users';
		break;

	case '3':
		$('editpostpermissionsicon').src = '/img/icon-permissions-members.png';
		$('editpostpermissionstitle').innerText = 'Group';
		break;

	case '2':
		$('editpostpermissionsicon').src = '/img/icon-permissions-contacts.png';
		$('editpostpermissionstitle').innerText = 'Contacts';
		$(editor+'preset2').checked = true;
		break;

	case '1':
		$('editpostpermissionsicon').src = '/img/icon-permissions-recipient.png';
		$('editpostpermissionstitle').innerText = 'Recipients';
		$(editor+'preset1').checked = true;
		break;

	case '0':
	default:
		$('editpostpermissionsicon').src = '/img/icon-permissions-owner.png';
		$('editpostpermissionstitle').innerText = 'Owner';
		$(editor+'preset0').checked = true;
		break;
	}

	/* Set age rating */
	agerating = Math.ceil(agerating); /* Force int value */
	if ((agerating < 3) || (agerating > 100)) {
		agerating = 3;
	}
	switch (agerating) {
	case 3:
		$('editpostpermissionsageratingtitle').innerText = 'General';
		$('editpostpresetgeneralcontent').checked = true;
		$('editpostcontentratingicon').src = '/img/icon-permissions-generalcontent.png';
		break;

	case 15:
		$('editpostpermissionsageratingtitle').innerText = 'Mature';
		$('editpostpresetmature15').checked = true;
		$('editpostcontentratingicon').src = '/img/icon-permissions-mature15.png';
		break;

	case 18:
		$('editpostpermissionsageratingtitle').innerText = 'Adult';
		$('editpostpresetadult18').checked = true;
		$('editpostcontentratingicon').src = '/img/icon-permissions-adult18.png';
		break;

	default:
		$('editpostpresetgeneralcontent').checked = false;
		$('editpostpresetmature15').checked = false;
		$('editpostpresetadult18').checked = false;
		$('editpostcontentratingicon').src = '/img/icon-permissions-advanced.png';
		$('editpostpermissionsageratingtitle').innerText = 'Ages '+agerating+'+';
	}

	/* Set initial view mode */
	if (editor == 'editpost') {
		switch(viewmode) {
		case 'thumbnail':
			$('editpostviewmodeicon').src = '/img/icon-viewmode-thumbnail.png';
			break;

		case 'list':
			$('editpostviewmodeicon').src = '/img/icon-viewmode-list.png';
			break;

		case 'full':
		default:
			viewmode = 'full';
			$('editpostviewmodeicon').src = '/img/icon-viewmode-full.png';
		}

		/* Set initial sort mode */
		switch(sortmode) {
		case 'title':
			$('editpostsortmodeicon').src = '/img/icon-sortmode-title.png';
			break;

		case 'date':
		default:
			$('editpostsortmodeicon').src = '/img/icon-sortmode-date.png';
			sortmode = 'date';
		}

		$('editpostviewmode'+viewmode).checked = true;
		$('editpostsortmode'+sortmode).checked = true;
		$('editpostsortreverse').checked = sortreverse;
	}

	/* Now change values */
	$(editor+'view'+viewpermissions).checked = true;
	$(editor+'edit'+editpermissions).checked = true;
	$(editor+'reply'+replypermissions).checked = true;
	$(editor+'approve'+approvepermissions).checked = true;
	$(editor+'moderate'+moderatepermissions).checked = true;
	$(editor+'tag'+tagpermissions).checked = true;
	$(editor+'agerating').value = agerating;

	/* Free up updating for responding to user actions */
	index_manualpostpermissionsupdate = 0;
}

/**
 * The following index_js_recipients(*) functions handle the tokenising of
 * recipients during editing.
 */
function index_js_recipientsDeleteButton(ev)
{
	if (ev.target.nodeName != "BUTTON")
		return false;

	var recipient = ev.target.value;
	var i = index_editpostrecipientsarray.indexOf(recipient);
	if (i == -1)
		return false;
	index_editpostrecipientsarray.splice(i, 1);

	var li = ev.target.parentNode;
	$(index_editpostphylum+'recipientsUL_'+index_editpostid).removeChild(li);
	li = null; /* Remove from memory; garbage collector. */
}

function index_js_recipientsAddEntry(postid, phylum, text)
{
	var li = document.createElement('li');
	var liText = document.createTextNode(text);
	var liDelete = document.createElement('button');
	li.className = 'recipientsLI';
	liDelete.innerText = '×';
	liDelete.value = text;
	li.appendChild(liText);
	li.appendChild(liDelete);
	liDelete.addEventListener('click', index_js_recipientsDeleteButton);
	liDelete.className = 'recipientsLIDelete';
	liDelete.title = "Remove recipient";

	$(phylum+'recipientsUL_'+postid).insertBefore(li, $(phylum+'recipientsEntryLI_'+postid));
	$(phylum+'recipientsUL_'+postid).appendChild($(phylum+'recipientsEntryLI_'+postid));
	$(phylum+'recipientsEntry_'+postid).value = '';
	$(phylum+'recipientsEntry_'+postid).focus();
	index_editpostrecipientsarray.push(text);
}

function index_js_recipientsClick(ev)
{
	$(index_editpostphylum+'recipientsEntry_'+index_editpostid).focus();
}

function index_js_recipientsKeyDown(ev)
{
	var phylum = index_editpostphylum;
	var postid = index_editpostid;
	var key = ev.keyCode;
	if (key == 8) {
		/* Check if remove last token. */
		var text = $(phylum+'recipientsEntry_'+postid).value;
		if (text && $(phylum+'recipientsEntryLI_'+postid).previousSibling) {
			var recipient = index_editpostrecipientsarray.pop();
			$(phylum+'recipientsEntry_'+postid).value = recipient;
			var li = $(phylum+'recipientsEntryLI_'+postid).previousSibling;
			$(phylum+'recipientsUL_'+postid).removeChild(li);
			ev.preventDefault();
		}
	} else if ((key == 10) || (key == 13) || (key == 32) || (key == 9)) {
		/* Add token. */
		if (key != 9) {
			ev.preventDefault();
		}
		var text = $(phylum+'recipientsEntry_'+postid).value;
		if (text && (!index_editpostrecipientsarray.includes(text))) {
			index_js_recipientsAddEntry(postid, phylum, text);
		}
	} else if ((key >= 32) && (key <= 90)) {
		/* Already handled. Text input. */
	} else {
		/* Some other key. */
	}
}

function index_js_setEditorRecipients(editpostid, editpostphylum, addEvents, recipientsArray)
{
	var recipientsdiv = $(editpostphylum+'recipients_'+editpostid);
	if (addEvents) {
		var recipientslength = 0;
		if (recipientsArray.length !== undefined) {
			recipientslength = recipientsArray.length;
		}
		var recipientsul = $(editpostphylum+"recipientsUL_"+editpostid);
		var recipientsspan = document.createElement('span');
		recipientsspan.className = "fadetext";
		recipientsspan.innerText = "Recipients ";
		recipientsdiv.insertBefore(recipientsspan, recipientsul);
		index_js_emptyElementById(editpostphylum+'recipientsUL_'+editpostid);

		var li = document.createElement('li');
		li.id = editpostphylum+"recipientsEntryLI_"+editpostid
		li.className = "recipientsEntryLI";

		var input = document.createElement('input');
		input.type = "text";
		input.id = editpostphylum+"recipientsEntry_"+editpostid
		input.className = "recipientsEntry";
		input.placeholder = "Username or email"
		li.appendChild(input);
		$(editpostphylum+'recipientsUL_'+editpostid).appendChild(li);

		for (r = 0; r < recipientslength; r++) {
			var recipient = recipientsArray[r];
			index_js_recipientsAddEntry(editpostid, editpostphylum, recipient);
		}

		recipientsdiv.classList.add("shadowinset");
		$(editpostphylum+"recipientsUL_"+editpostid).addEventListener('click',
				index_js_recipientsClick);
		$(editpostphylum+"recipientsEntry_"+editpostid).addEventListener('keydown',
				index_js_recipientsKeyDown);
	} else {
		$(editpostphylum+"recipients_"+editpostid).classList.remove("shadowinset");
		$(editpostphylum+"recipientsUL_"+editpostid).removeEventListener('click',
				index_js_recipientsClick);
		$(editpostphylum+"recipientsEntry_"+editpostid).removeEventListener('keydown',
				index_js_recipientsKeyDown);
	}
}

/**
 * Prefill description editor with specified URI.
 * Called by index_js_setupPostEditor_AjaxFunction below.
 */
function index_js_setupPostEditor_loadURI(descriptiondiv, templateuri)
{
	fetch(templateuri)
	.then(response => {
		if (!response.ok) {
			throw new Error("HTTP error " + response.status);
		}
		return response.text();
	})
	.then(text => {
		descriptiondiv.innerHTML = text;
	})
	.catch(error => console.warn(error));
}

/**
 * Called by "New Post" buttons or "Reply" links.
 */
function index_js_setupPostEditor_AjaxFunction()
{
	if (xhrSetupPost.readyState != 4)
		return; /* Nothing to do here! */

	var responsearray = index_js_parseResponseText(xhrSetupPost.responseText, "Unable to contact server.\n\nTry again or contact the website administrators.");
	if (!responsearray)
		return;

	if (responsearray['error'] != "INFO_SETUP_POST_PROCEED") {
		console.log("<?=__LINE__?> Error! "+responsearray['error']);
		return;
	}

	/* Update the file space meters */
	if ($('popuplistmeter') != null) {
		$('popuplistmeter').value = responsearray['setupeditorfilespace'];
		$('popuplistmetertext').innerText = responsearray['setupeditorfilespacetext'];
	}
	index_editpostviewmode = responsearray['setupposteditorviewmode'];
	index_editpostsortmode = responsearray['setupposteditorsortmode'];
	index_editpostsortreverse = responsearray['setupposteditorsortreverse'];

	if (index_editpostphylum) {
		index_js_revertPostEditor();
		index_editdescription.contentEditable = false;
		index_js_removePostEditorEvents(index_editdescription);
	}

	ev = index_setupposteditorev;
	hostdivid = index_setupposteditorhostdivid;
	phylum = index_setupposteditorphylum;
	parentid = index_setupposteditorparentid;
	inlinereply = index_setupposteditorinlinereply;
	permissions = index_setupposteditorpermissions;
	agerating = index_setupposteditoragerating;
	pagetype = index_setupposteditorpagetype;
	sourceuri = index_setupposteditorsourceuri;
	viewmode = index_setupposteditorviewmode;
	sortmode = index_setupposteditorsortmode;
	sortreverse = index_setupposteditorsortreverse;
	recipients = false;
	if (index_setupposteditorrecipients) {
		recipients = index_setupposteditorrecipients.split(' ');
	}

	var linktext = ev.innerHTML || ev.textContent;
	if (linktext) {
		index_editlinktext = linktext;
	} else {
		index_editlinktext = 'New Post';
	}

	/* Preserve content of new post description divs. */
	if (index_editpostid == index_newpostid) {
		index_originalhostdivhtml = $(hostdivid).innerHTML;
	}

	if (inlinereply) {
		inlinereply = 1;
	} else {
		inlinereply = 0;
	}
	index_editpostinlinereply = inlinereply;

	/* Preserve entire block in _originaldescription variable in case it needs to be restored */
	index_originalhostdivhtml = $(hostdivid).innerHTML;
	index_js_emptyElementById(hostdivid);

	index_editposthostdiv = $(hostdivid);

	/* Add header to accept recipients editor and cancel button */
	let headerdiv = document.createElement('div');
	headerdiv.id = 'postheader_NEWPOST';
	headerdiv.className = 'postheader postheadersticky';
	headerdiv.style.paddingTop = "24px";
	$(hostdivid).appendChild(headerdiv);

	headerdiv.appendChild($('editpostcancel'));

	/* Set up recipients editor */
	var recipientsdiv = document.createElement('div');
	recipientsdiv.id = 'postrecipients_NEWPOST';
	recipientsdiv.className = 'recipientsdiv';
	index_originalrecipients = recipientsdiv.innerHTML;
	var recipientsul = document.createElement('ul');
	recipientsul.id = 'postrecipientsUL_NEWPOST';
	recipientsul.className = "inlinelist";
	recipientsdiv.appendChild(recipientsul);
	headerdiv.appendChild(recipientsdiv)
	index_editrecipientsdiv = recipientsdiv;
	index_js_setEditorRecipients('NEWPOST', 'post', true, recipients);

	/* Set up description editor */
	var descriptiondiv = document.createElement('div');
	descriptiondiv.id = 'postdescription_NEWPOST';
	descriptiondiv.className = 'postdescription';
	descriptiondiv.contentEditable = 'true';
	descriptiondiv.innerHTML = '<br>';
	$(hostdivid).appendChild(descriptiondiv);
	index_editdescription = descriptiondiv;

	/* If source URI specified, fetch HTML data for description editor. */
	if (sourceuri) {
		descriptiondiv.innerHTML = 'Loading...';
		index_js_setupPostEditor_loadURI(descriptiondiv, sourceuri);
	}

	/* Set up attachment div */
	var attachmentsdiv = document.createElement('div');
	attachmentsdiv.id = 'postattachments_NEWPOST';
	$(hostdivid).appendChild(attachmentsdiv);

	/* Set up the description and controls */
	index_editpostid = index_newpostid; /* 0 = "New post" */
	index_editpostphylum = phylum;
	index_editpostpagetype = pagetype;
	index_editparentid = null;
	index_inlinereply = inlinereply;
	if (parentid > 0) {
		index_editparentid = parentid;
	}

	$(hostdivid).appendChild($('editpostcontrols'));
	$('editpostsubmit').value = "Post";
	index_newpostsessionid = responsearray['newpostsessionid'];
	if (inlinereply) {
		$('editpostajaxcommand').value = "new_postreply";
	} else {
		$('editpostajaxcommand').value = "new_post";
	}
	$('editpostcancel').style.display = "block";
	$('editpostcontrols').style.display = "block";
	/* The following line places editpostcancel just above top right of recipients. */
	index_js_setEditorPermissions('editpost', permissions, 3, 'full', 'date', false);
	index_js_updatePostPermissions('editpost');
	$('editpostagerating').value = agerating;

	index_js_addPostEditorEvents(index_editdescription);
	index_editdescription.onkeyup = index_js_updateNewPost;
	index_editdescription.onclick = index_js_updateNewPost;
	index_editdescription.focus();
	index_js_updateNewPost();
	index_js_setEditorPermissions('editpost', permissions, agerating,
			viewmode, sortmode, sortreverse);
}

/**
 * Most parameters are self-explanitory. Ambiguous ones include:
 * - hostdivid specifies div to modify for editing, usually
 *	'newpostreply_{$postid}' or 'postblock_NEWPOST'.
 * - phylum is either the strings 'message' or 'post'.
 * - recipients is a space-separated string of email addresses or server usernames.
 * - pagetype usually passes on $index_page - the value we're looking for is
 *	either 'page' or 'profile' to show pin menu item.
 * - sourceuri is a web address to copy HTML data from.
 * - viewmode is either 'full', 'thumbnail', 'list', or blank '' to ignore.
 * - sortmode is either 'date', or 'title', or blank '' to ignore.
 * - sortreverse is boolean.
 */
function index_js_setupPostEditor(ev, hostdivid, phylum, parentid, inlinereply,
		permissions, agerating, recipients, pagetype, sourceuri,
		viewmode, sortmode, sortreverse)
{
	if (index_editpostphylum) {
		if (!confirm("Another post is being edited.\n\nDiscard edited post?"))
			return; /* Don't discard already edited post. */

		/* id = 0 && parent = 0 == NEWPOST */
		let cancelled = index_js_cancelEditPost(true);
		index_editdescription.contentEditable = "false";
	}

	/* Transfer variables from this function to the ajax_function */
	index_setupposteditorev = ev;
	index_setupposteditorhostdivid = hostdivid;
	index_setupposteditorphylum = phylum;
	index_setupposteditorparentid = parentid;
	index_setupposteditorinlinereply = inlinereply;
	index_setupposteditorpermissions = permissions;
	index_setupposteditoragerating = agerating;
	index_setupposteditorrecipients = recipients;
	index_setupposteditorpagetype = pagetype;
	index_setupposteditorsourceuri = sourceuri;
	index_setupposteditorviewmode = viewmode;
	index_setupposteditorsortmode = sortmode;
	index_setupposteditorsortreverse = sortreverse;

	var setupposteditorformdata = new FormData();
	setupposteditorformdata.append("ajax_command", "setup_editor");
	setupposteditorformdata.append("parentid", parentid);
	setupposteditorformdata.append('setupposteditorviewmode', viewmode);

	xhrSetupPost = new XMLHttpRequest();
	xhrSetupPost.onreadystatechange = index_js_setupPostEditor_AjaxFunction;
	xhrSetupPost.open("POST", "/index.php", true);
	xhrSetupPost.send(setupposteditorformdata);
}

/**
 * This function, and the following one, are used primarily for the management
 * of attachments and images with the associated post being edited.
 */
function index_js_editPost_AjaxFunction()
{
	if (xhrEditPost.readyState != 4)
		return; /* Nothing to do here! */

	var responsearray = index_js_parseResponseText(xhrEditPost.responseText, "Unable to contact server.\n\nTry again or contact the website administrators.");
	if (!responsearray)
		return;

	if (responsearray['error'] != "INFO_EDIT_POST_PROCEED") {
		console.log("<?=__LINE__?> Error! "+responsearray['error']);
		if (responsearray['error'] == 'ERROR_INIT_EDITOR_INVALID_VIEW_MODE') {
			alert('Can not edit posts displayed as thumbnails or list items.\nChange "View As" to "Full Post" to edit these posts.');
		}
		return;
	}

	/* Update the file space meters */
	if ($('popuplistmeter') != null) {
		$('popuplistmeter').value = responsearray['editpostfilespace'];
		$('popuplistmetertext').innerText = responsearray['editpostfilespacetext'];
	}

	var editpostphylum = responsearray['phylum'];
	var editpostid = responsearray['id'];
	var editpostshowparent = responsearray['showparent'];
	var editpostviewmode = responsearray['editpostviewmode'];
	index_editpostdescriptionid = editpostphylum+'description_'+editpostid;
	index_editdescription = $(index_editpostdescriptionid);

	index_editpostid = editpostid;
	index_editpostphylum = editpostphylum;
	index_editpostshowparent = editpostshowparent;
	index_editparentid = null;
	index_inlinereply = 0; /* Not needed */
	index_editpostviewmode = editpostviewmode;
	index_editrecipientsdiv = $(editpostphylum+'recipients_'+editpostid);
	index_originaldescription = index_editdescription.innerHTML;
	index_originalrecipients = '';
	if (editpostphylum == 'post') {
		index_originalrecipients = index_editrecipientsdiv.innerHTML;
	}

	if (editpostphylum == 'post') {
		$('editpostajaxcommand').value = "edit_post";
		/* Without .firstElementChild() the edit controls would appear AFTER the replies. */
		console.log('<?=__LINE__?> INPROGRESS crashes here - editpostid='+editpostid);
		$('postheader_'+editpostid).insertBefore($('editpostcancel'), $('postheader_'+editpostid).firstChild);
		$('postheader_'+editpostid).classList.add('postheadersticky');
		$('postblock_'+editpostid).appendChild($('editpostcontrols'));
		// was $('post'+editpostid).firstElementChild.appendChild($('editpostcontrols'));
		$('editpostcancel').style.display = "block";
		$('editpostcontrols').style.display = "block";
	} else if (editpostphylum == 'message') {
		$('editpostajaxcommand').value = "edit_message";

		/* Hide edit controls; messages use a simple editing format */
		$('post_NEWPOST').appendChild($('editpostcontrols'));
		$('editpostcontrols').style.display = 'none';

		$('messagemenudiv_'+editpostid).appendChild($('editpostcancel'));
		$('editpostcancel').style.display = "block";
		$('editpostcancel').style.position = "relative";
	} else {
		console.log("<?=__LINE__?> Error! Incorrect phylum! "+responsearray['phylum']);
		return;
	}

	if (editpostshowparent == 'true') {
		editpostshowparent = true;
	} else {
		editpostshowparent = false;
	}

	index_editdescription.innerHTML = responsearray['description']; /* Edit pre-parsed (markdown-ish) version */
	$(editpostphylum+'popuplist_'+index_editpostid).style.display = "none";

	var recipientsarray = false;
	if ((typeof responsearray['recipients'] !== 'undefined')
			&& (responsearray['recipients'].length)) {
		recipientsarray = responsearray['recipients'];
	}

	if (editpostphylum != 'message') {
		index_js_setEditorRecipients(editpostid, editpostphylum, true, recipientsarray);
		$('postsharemenu_'+index_editpostid).style.display = "none";
	}

	index_editdescription.contentEditable = "true";
	index_js_addPostEditorEvents(index_editdescription);
	$('editpostsubmit').value = "Save";
	var editpostpermissions = $(editpostphylum+'_'+editpostid).getAttribute('postpermissions');
	var editpostviewmode = $(editpostphylum+'_'+editpostid).getAttribute('postviewmode');
	var editpostsortmode = $(editpostphylum+'_'+editpostid).getAttribute('postsortmode');
	var editpostsortreverse = $(editpostphylum+'_'+editpostid).getAttribute('postsortreverse');
	var editpostagerating = $(editpostphylum+'_'+editpostid).getAttribute('postagerating');
	var editpostrecipients = $(editpostphylum+'_'+editpostid).getAttribute('postrecipients');
	var editpostsortmode = $(editpostphylum+'_'+editpostid).getAttribute('postsortmode');
	var sortreverse = false;
	if (editpostsortreverse == '1') {
		sortreverse = true;
	}
	index_js_setEditorPermissions('editpost', editpostpermissions, editpostagerating,
			editpostviewmode, editpostsortmode, sortreverse);
	index_js_updatePostPermissions('editpost');
	$('editpostagerating').value = editpostagerating;
	index_editdescription.focus();

	if ((typeof responsearray['files'] !== 'undefined')
			&& (responsearray['files'].length)) {
		fileslength = responsearray['files'].length;
		for (f = 0; f < fileslength; f++) {
			var fileid = responsearray['files'][f]['id'];
			var filename = responsearray['files'][f]['filename'];
			var filemime = responsearray['files'][f]['filemime'];
			var attachmentcontrolsspan = document.createElement('span');
			attachmentcontrolsspan.id = editpostphylum+"_"+editpostid+"_attachmentcontrols_"+fileid;
			attachmentcontrolsspan.innerHTML = " <a id='"+editpostphylum+"_"+editpostid+"_deleteattachment_"+fileid+"' class='postfiledelete' onclick='index_js_deleteAttachment(\""+editpostphylum+"\", \""+editpostid+"\", \""+fileid+"\", \""+filename+"\", \"\");'>Delete</a>";
			$(editpostphylum+'_'+editpostid+'_attachment_'+fileid).appendChild(attachmentcontrolsspan);
			index_editpostfilesarray.push({'fileid':fileid, 'filename':filename, 'deletefile':0, 'newfile':0});
		}
	}
}

function index_js_editPost(phylum, editpostid, showparent, posttype, viewmode)
{
	if ((index_editpostphylum == phylum) && (index_editpostid == editpostid))
		return; /* Prevent multiple edit controls. */

	if (index_editpostphylum) {
		if (!confirm("Another post is being edited.\n\nDiscard edited post?"))
			return; /* Don't discard already edited post. */

		/* id = 0 && parent = 0 == NEWPOST */
		index_js_cancelEditPost(true);
		index_editdescription.contentEditable = "false";
	}

	var initeditorformdata = new FormData();
	initeditorformdata.append("ajax_command", "init_editor");
	initeditorformdata.append("editpostphylum", phylum);
	initeditorformdata.append("editpostid", editpostid);
	initeditorformdata.append("editpostshowparent", showparent);
	initeditorformdata.append("editpostviewmode", viewmode);

	xhrEditPost = new XMLHttpRequest();
	xhrEditPost.onreadystatechange = index_js_editPost_AjaxFunction;
	xhrEditPost.open("POST", "/index.php", true);
	xhrEditPost.send(initeditorformdata);
}

/**
 * Remove any unpublished files here.
 */
function index_js_cancelEditPost_AjaxFunction()
{
	if (xhrCancelEdit.readyState != 4)
		return;

	var responsearray = index_js_parseResponseText(xhrCancelEdit.responseText, false);
	if (!responsearray)
		return;

	if (responsearray['error'] != "INFO_CANCEL_EDIT_POST_SUCCESS") {
		console.log("<?=__LINE__?> Error here! Can NOT cancel edit post!!!");
	}
}

/**
 *
 */
function index_js_cancelEditPost(forceCancel)
{
	if (!index_editpostid)
		return false;

	if (forceCancel)
		console.log("<?=__LINE__?> Forcing cancel.");
	else if (!confirm("Cancel post?"))
		return false; /* Don't discard already edited post. */

	/* id = 0 && parent = 0 == NEWPOST */
	index_js_removePostEditorEvents(index_editdescription);
	index_js_emptyElementById('editpostsuggestions');
	$('editpostcancel').style.display = 'none';
	$('editpostcancel').style.position = "absolute";
	$('editpostcontrols').style.display = 'none';
	$('post_NEWPOST').appendChild($('editpostcancel'));
	$('post_NEWPOST').appendChild($('editpostcontrols'));
	index_js_revertPostEditor();
	if (index_editpostid != index_newpostid) {
		if (index_editpostphylum != 'message') {
			index_editrecipientsdiv.innerHTML = index_originalrecipients;
		}
	} else {
		index_editposthostdiv.style.paddingTop = '';
	}

	if ((index_editpostid != 'NEWPOST') && (index_editpostphylum != 'message')) {
		$('postheader_'+index_editpostid).classList.remove('postheadersticky');
	}

	var cancelpostformdata = new FormData();
	cancelpostformdata.append("ajax_command", "cancel_edit_post");
	cancelpostformdata.append("canceleditpostid", index_editpostid);
	cancelpostformdata.append("canceleditdescriptionid", index_editdescription.id);

	/* Remove delete attachment links. (Duplicate code used in Save Edit Post function) */
	var fileslength = index_editpostfilesarray.length;
	for (var f = 0; f < fileslength; f++) {
		var fi = index_editpostfilesarray.shift();
		var fileid = fi['fileid'];
		var filename = fi['filename'];
		var deletefile = fi['deletefile'];
		var newfile = fi['newfile'];
		cancelpostformdata.append("canceleditpostfileid[]", fileid);
		cancelpostformdata.append("canceleditpostfilename[]", filename);
		index_js_deleteElementById(index_editpostphylum+'_'+index_editpostid+'_deleteattachment_'+fileid);
		index_js_deleteElementById(index_editpostphylum+'_'+index_editpostid+'_attachmentcontrols_'+fileid);
		if (newfile == 1) {
			index_js_deleteElementById(index_editpostphylum+'_'+index_editpostid+'_attachment_'+fileid);
		}
	}

	xhrCancelEdit = new XMLHttpRequest();
	xhrCancelEdit.onreadystatechange = index_js_cancelEditPost_AjaxFunction;
	xhrCancelEdit.open("POST", "/index.php", true);
	xhrCancelEdit.send(cancelpostformdata);

	index_editpostphylum = false;
	index_editpostid = false;
	index_editparentid = false;
	index_editpostdescriptionid = false;
	index_sendingpost = false;
	index_editdescription.contentEditable = false;
	index_editdescription.blur();
	return true;
}

/**
 * Completely remove an element from memory (by ID).
 */
function index_js_deleteElementById(id)
{
	var el = $(id);
	if (el == null) {
		console.log("<?=__LINE__?> Can not delete element id ("+id+") does not exist!");
		return;
	}
	el.parentElement.removeChild(el);
	el = null; /* Remove from memory; garbage collector. */
}

/**
 * Empties an element as fast and cleanly as possible.
 */
function index_js_emptyElementById(id)
{
	const el = $(id);
	if (!el)
		return;

	var elChild = null;
	while (elChild = el.firstChild) {
		el.removeChild(elChild);
		elChild = null; /* Remove from memory; garbage collector. */
	}
}

/**
 *
 */
function index_js_pinPost_AjaxFunction()
{
	if (xhrPinPost.readyState != 4)
		return; /* Nothing to do here. */

	var responsearray = index_js_parseResponseText(xhrPinPost.responseText,
			"Unable to set post pin state.\n\nTry again or contact the website administrators.");
	if (!responsearray)
		return;

	response = responsearray['error'];
	if ((response != "INFO_PIN_POST_SUCCESS") && (response != "INFO_REMOVE_POST_PIN_SUCCESS")
			&& (response != "WARNING_POST_ALREADY_PINNED")) {
		console.log("<?=__LINE__?>: What is this? "+response);
		return;
	}

	var postid = responsearray['postid'];
	var posttype = responsearray['posttype'];
	var pagetype = responsearray['pagetype'];
	if ((response == "INFO_PIN_POST_SUCCESS") || (response == "WARNING_POST_ALREADY_PINNED")) {
		if ($('postpinnedicon_'+postid) == null) {
			var postpinnedimg = document.createElement('img');
			postpinnedimg.id = 'postpinnedicon_'+postid;
			postpinnedimg.src = '/img/icon-post-pinned.png';
			$('postpopupicon_'+postid).parentNode.insertBefore(postpinnedimg,
					$('postpopupicon_'+postid));
		}
		if ((response == "INFO_PIN_POST_SUCCESS") && (posttype == 'post')) {
			$('pinnedpostlocation').appendChild($('post_'+postid));
		}
	} else if (($('postlocation_'+postid) == null) && (posttype == 'post')) {
		index_js_deleteElementById('post_'+postid);
		return;
	} else {
		index_js_deleteElementById('postpinnedicon_'+postid);
		if (posttype == 'post') {
			/* Only post blocks get moved. Other types don't. */
			$('postlocation_'+postid).appendChild($('post_'+postid));
		}
	}

	$('postpinpost_'+postid).innerHTML = responsearray['pinpostlinktext'];
	$('postpinpost_'+postid).onclick = function() {
			index_js_pinPost(postid, responsearray['newpinpostvalue'],
					responsearray['posttype'], responsearray['pagetype']); }
}

/**
 * - postType describes the position of the post on a page, including:
 *   'title', 'post', 'reply' or 'message'. Only 'post' gets moved.
 * - pageType should be either "page" or "profile", since profile posts don't
 *   need to have a parent post reference.
 */
function index_js_pinPost(id, pinState, postType, pageType)
{
	index_popupmenuid = 0;
	var pinpostformdata = new FormData();
	var pinpoststate = 0;
	if (pinState == 1)
		pinpoststate = 1;

	pinpostformdata.append("pinpostid", id);
	pinpostformdata.append("pinpoststate", pinpoststate);
	pinpostformdata.append("pinposttype", postType);
	pinpostformdata.append("pinpagetype", pageType);
	pinpostformdata.append("ajax_command", "pin_post");
	xhrPinPost = new XMLHttpRequest();
	xhrPinPost.onreadystatechange = index_js_pinPost_AjaxFunction;
	xhrPinPost.open("POST", "/index.php", true);
	xhrPinPost.send(pinpostformdata);
	return false;
}

/**
 *
 */
function index_js_deletePost_AjaxFunction()
{
	if (xhrDeletePost.readyState != 4)
		return; /* Nothing to do here. */

	var responsearray = index_js_parseResponseText(xhrDeletePost.responseText, "Unable to delete post.\n\nTry again or contact the website administrators.");
	if (!responsearray)
		return;

	if (responsearray['error'] != "INFO_DELETE_POST_SUCCESS") {
		return;
	}

	/* Update the file space meters */
	if ($('popuplistmeter') != null) {
		$('popuplistmeter').value = responsearray['deletepostfilespace'];
		$('popuplistmetertext').innerText = responsearray['deletepostfilespacetext'];
	}

	var phylum = responsearray['phylum'];
	var id = responsearray['postid'];
	index_js_deleteElementById(phylum+'_'+id);
	if (phylum == "post") {
		index_js_deleteElementById('postlocation_'+id);
	}
}


/**
 *
 */
function index_js_deletePost(phylum, id)
{
	if (!confirm("Delete post?\n\nOnce deleted, this can not be undone"))
		return false;

	index_popupmenuid = 0;
	var deletepostformdata = new FormData();
	deletepostformdata.append("deletepostphylum", phylum);
	deletepostformdata.append("deletepostid", id);
	deletepostformdata.append("ajax_command", "delete_post");
	xhrDeletePost = new XMLHttpRequest();
	xhrDeletePost.onreadystatechange = index_js_deletePost_AjaxFunction;
	xhrDeletePost.open("POST", "/index.php", true);
	xhrDeletePost.send(deletepostformdata);
	return false;
}

/**
 *
 */
function index_js_approvePost_AjaxFunction()
{
	if (xhrApprovePost.readyState != 4)
		return; /* Nothing to do here. */

	var responsearray = index_js_parseResponseText(xhrApprovePost.responseText, "Could not modify approval.\n\nTry again or contact the website administrators.");
	if (!responsearray)
		return;

	if ((responsearray['error'] != "INFO_APPROVE_POST_SUCCESS")
			&& (responsearray['error'] != "INFO_APPROVE_POST_RESET")
			&& (responsearray['error'] != "INFO_APPROVE_POST_HIDDEN")) {
		console.log("<?=__LINE__?>: What is this? "+responsearray['error']);
		return;
	}

	var phylum = responsearray['phylum'];
	var postid = responsearray['postid'];

	/* First, remove all classes (if exist), then apply specific classes. */
	$(phylum+'_'+postid).classList.remove('postapprovalpending');
	//$(phylum+'_'+postid).classList.remove('postapprovaltrusted');
	$(phylum+'_'+postid).classList.remove('postapprovalhidden');
	/* Owner doesn't need pending approval icon. */
	if (!responsearray['approveicon']) {
		index_js_deleteElementById(phylum+'approveicon_'+postid);
	} else {
		if ($(phylum+'approveicon_'+postid) == null) {
			var approveimg = document.createElement('img');
			approveimg.id = phylum+'approveicon_'+postid;
			$(phylum+'popupicon_'+postid).parentNode.insertBefore(approveimg,
					$(phylum+'popupicon_'+postid));
		}
		$(phylum+'approveicon_'+postid).src = responsearray['approveicon'];
	}

	if ($(phylum+'approve_'+postid) != null) {
		$(phylum+'approve_'+postid).innerHTML = responsearray['approvelinktext'];
		$(phylum+'approve_'+postid).onclick = function() {
				index_js_approvePost(phylum, postid, responsearray['newapprovalvalue']); }
	}
	$(phylum+'hide_'+postid).innerHTML = responsearray['hidelinktext'];
	$(phylum+'hide_'+postid).onclick = function() { index_js_approvePost(phylum, postid, responsearray['newhidevalue']); }

	if (responsearray['error'] == "INFO_APPROVE_POST_SUCCESS") {
		// $(phylum+'_'+postid).classList.add('postapprovaltrusted');
		index_js_deleteElementById('postpending_'+postid);
	} else if (responsearray['error'] == "INFO_APPROVE_POST_HIDDEN") {
		$(phylum+'_'+postid).classList.add('postapprovalhidden');
	} else if (responsearray['approveicon']) {
		/* Owner isn't given approve icon, but other users are. */
		$(phylum+'_'+postid).classList.add('postapprovalpending');

		var postpendingdiv = document.createElement('div');
		postpendingdiv.id = phylum+'pending_'+postid;
		postpendingdiv.className = 'poststatuspending';
		postpendingdiv.innerHTML = 'This post is pending approval.';
		$(phylum+'description_'+postid).parentNode.insertBefore(postpendingdiv, $(phylum+'description_'+postid));
	}
}

/**
 * phylum: 'post' or 'message'
 * id: post id.
 * approval: set post as; 1=approve, (-1)=hide, 0=neither.
 */
function index_js_approvePost(phylum, id, approval)
{
	var approvepostformdata = new FormData();
	approvepostformdata.append("phylum", phylum);
	approvepostformdata.append("postid", id);
	approvepostformdata.append("approval", approval);
	approvepostformdata.append("ajax_command", "approve_post");
	xhrApprovePost = new XMLHttpRequest();
	xhrApprovePost.onreadystatechange = index_js_approvePost_AjaxFunction;
	xhrApprovePost.open("POST", "/index.php", true);
	xhrApprovePost.send(approvepostformdata);
	return false;
}

/**
 *
 */
function index_js_joinGroup_AjaxFunction()
{
	if (xhrJoinGroup.readyState != 4)
		return; /* Nothing to do here. */

	var responsearray = index_js_parseResponseText(xhrJoinGroup.responseText, "Could not join group.\n\nTry again or contact the website administrators.");
	if (!responsearray)
		return;

	if ((responsearray['error'] != "INFO_JOIN_GROUP_REQUEST_SENT")
			&& (responsearray['error'] != "INFO_JOIN_GROUP_REQUEST_APPROVED")
			&& (responsearray['error'] != "INFO_LEAVE_GROUP_COMPLETED")
			&& (responsearray['error'] != "INFO_CLOSE_GROUP_COMPLETED")) {
		return;
	}

	var phylum = responsearray['phylum'];
	var postid = responsearray['postid'];
	var userid = responsearray['userid'];

	/* Update popup menu link to set up/close group. */
	if (($(phylum+'joingrouplink_'+postid) != null) && (responsearray['linktext'] != null)) {
		$(phylum+'joingrouplink_'+postid).innerHTML = responsearray['linktext'];
		$(phylum+'joingrouplink_'+postid).onclick = function() { index_js_joinGroup(phylum, postid, userid, responsearray['joinrequest']); }
	}

	/* Update button to join/leave group on the post itself. */
	if (($(phylum+'joingroupbutton_'+postid) != null) && (responsearray['linktext'] != null)) {
		let joinconfirm = !responsearray['joinrequest'];
		$(phylum+'joingroupbutton_'+postid).value = responsearray['linktext'];
		$(phylum+'joingroupbutton_'+postid).onclick = function() { index_js_joinGroup(phylum, postid, userid, responsearray['joinrequest'], joinconfirm); }
	}

	if (responsearray['error'] == "INFO_LEAVE_GROUP_COMPLETED") {
		index_js_deleteElementById('grouplistentry_'+postid);
		index_js_deleteElementById(phylum+'memberentry_'+postid+"_"+userid);
	} else if (responsearray['error'] == "INFO_JOIN_GROUP_REQUEST_SENT") {
		index_js_deleteElementById('grouplistentry_'+postid);
		$('grouplist').insertAdjacentHTML('beforeend', responsearray['grouplistentry']);
		index_js_deleteElementById(phylum+'memberentry_'+postid+'_'+userid);
		var memberlistitem = document.createElement('li');
		memberlistitem.id = phylum+"memberentry_"+postid+"_"+userid;
		memberlistitem.innerHTML = responsearray['groupmembersentry'];
		$(phylum+'membersUL_'+postid).appendChild(memberlistitem);;
	} else if (responsearray['error'] == "INFO_JOIN_GROUP_REQUEST_APPROVED") {
		/* Set up the group (if needed as group owner) */
		var membercount = $(phylum+'_'+postid).getAttribute('postmembercount');
		if (responsearray['userid'] == responsearray['ownerid']) {
			$(phylum+'membercount_'+postid).innerHTML = responsearray['membercounttext'];
			index_js_deleteElementById('grouplistentry_'+postid);
			$('grouplist').insertAdjacentHTML('beforeend', responsearray['grouplistentry']);
			/* Insert owner's "group member" entry */
			index_js_deleteElementById(phylum+'memberentry_'+postid+'_'+userid);
			var memberlistitem = document.createElement('li');
			memberlistitem.id = phylum+"memberentry_"+postid+"_"+userid;
			memberlistitem.innerHTML = responsearray['groupmembersentry'];
			$(phylum+'membersUL_'+postid).appendChild(memberlistitem);
		} else {
			var tempcontainerelement = document.createElement('div');
			tempcontainerelement.innerHTML = responsearray['groupmembersentry'];
			$(tempcontainerelement.childNodes[0].id).innerHTML = tempcontainerelement.childNodes[0].innerHTML;
		}
	} else if (responsearray['error'] == "INFO_CLOSE_GROUP_COMPLETED") {
		/* The following two lines are rough but work. Probably lots of hanging nodes... */
		index_js_emptyElementById(phylum+'membercount_'+postid);
		index_js_emptyElementById(phylum+'memberspending_'+postid);
		index_js_emptyElementById(phylum+'membersUL_'+postid);
		$(phylum+'_'+postid).setAttribute('postmembercount', 0);
		index_js_deleteElementById('grouplistentry_'+postid);
	}

	/* Update member count and (for owner/moderators) pending member count. */
	if (($(phylum+'membercount_'+postid) != null)
			&& (responsearray['trustedmembercounttext'] != null)) {
		$(phylum+'membercount_'+postid).innerHTML = responsearray['trustedmembercounttext'];
	}

	if (($(phylum+'memberspending_'+postid) != null)
			&& (responsearray['memberspendingtext'] != null)) {
		$(phylum+'memberspending_'+postid).innerHTML = responsearray['memberspendingtext'];
	}
}

/**
 * joinrequest values: 0:Leave group or reject request, 1:Request join group, 2:Set post as group (Owner/Editor/Moderator only).
 * confirmtype: 0:no confirmation, 1:owner close group with members.
 */
function index_js_joinGroup(phylum, postid, userid, joinrequest, confirmtype)
{
	if (joinrequest == 1) {
		if ((($(phylum+'_'+postid).getAttribute('postmembercount') == 0)
				&& ($(phylum+'_'+postid).getAttribute('postaccountid') == userid))
				|| (($(phylum+'_'+postid).getAttribute('postmembercount'))
				&& (userid == <?=$index_activeuserid?>))) {
			var joinlinktext = "Joining group...";
			if ($(phylum+'_'+postid).getAttribute('postaccountid') == userid) {
				joinlinktext = "Setting up group";
			}

			/* Add the sidebar group entry.
			 * NOTE: #grouplist(entry) doesn't need phylum. */
			var newgroupentry = document.createElement('div');
			newgroupentry.innerHTML = "<img src='/img/status-pending12.png' alt='pending'> "+joinlinktext+"...";
			newgroupentry.id = "grouplistentry_"+postid;
			$('grouplist').appendChild(newgroupentry);

			/* Add the member entry on the group post members list. */
			var newgroupmemberentry = document.createElement('li');
			newgroupmemberentry.innerHTML = "<span style='white-space:nowrap;'><img src='/img/status-pending12.png' alt='pending'>"+joinlinktext+"...</span>";
			newgroupmemberentry.id = phylum+"memberentry_"+postid+"_"+userid;
			$(phylum+'membersUL_'+postid).appendChild(newgroupmemberentry);

			/* Update popup menu link. Mainly for group owners. */
			if ($(phylum+'joingrouplink_'+postid) != null) {
				$(phylum+'joingrouplink_'+postid).innerHTML = joinlinktext+"...";
				$(phylum+'joingrouplink_'+postid).onclick = null;
			}

			/* Update join/leave group button. */
			if ($(phylum+'joingroupbutton_'+postid) != null) {
				$(phylum+'joingroupbutton_'+postid).value = joinlinktext+"...";
				$(phylum+'joingroupbutton_'+postid).onclick = null;
			}

			/* What the heck is the below line from?
			index_grouplistarray.push */
		} else {
			/* Other conditions, such as group owner verifying people. */
		}
	} else if (joinrequest == 0) {
		let confirmstring = '';
		if (confirmtype == 0) {
			if (<?=$index_activeuserid?> == userid) {
				confirmstring = "Cancel request to join group?";
			} else {
				confirmstring = "Remove this member from the group?";
			}
		} else if (confirmtype == 1) {
			confirmstring = "Leave this group?";
		} else if (confirmtype == 2) {
			confirmstring = "Close group?";
		}

		/* Show the prompt to cancel or proceed */
		if (confirmstring) {
			if (!confirm(confirmstring)) {
				return; /* Don't close group. */
			}
		}
	} else {
		console.log('<?=__LINE__?>: ERROR: Invalid join group request. Exiting...');
		return false;
	}

	var joingroupformdata = new FormData();
	joingroupformdata.append("joingroupphylum", phylum);
	joingroupformdata.append("joingrouppostid", postid);
	joingroupformdata.append("joingroupuserid", userid);
	joingroupformdata.append("joinrequest", joinrequest);
	joingroupformdata.append("ajax_command", "join_group");
	xhrJoinGroup = new XMLHttpRequest();
	xhrJoinGroup.onreadystatechange = index_js_joinGroup_AjaxFunction;
	xhrJoinGroup.open("POST", "/index.php", true);
	xhrJoinGroup.send(joingroupformdata);
	return false;
}

/**
 *
 */
function index_js_flagPost_AjaxFunction()
{
	if (xhrFlagPost.readyState != 4)
		return; /* Nothing to do here. */

	var responsearray = index_js_parseResponseText(xhrFlagPost.responseText,
			"Could not flag post.\n\nTry again or contact the website administrators.");
	if (!responsearray)
		return;

	if ((responsearray['error'] != "INFO_FLAG_POST_SUCCESS")
			&& (responsearray['error'] != "INFO_FLAG_CLEAR_SUCCESS")) {
		if (responsearray['error'] == "ERROR_CLEAR_FLAG_NOT_TIME_PASSED") {
			alert("Could not clear flag.\n\nSuspension time not passed.");
		} else {
			console.log("<?=__LINE__?>: What is this? "+responsearray['error']);
			alert("Could not flag post.\n\nTry again or contact the website administrators.");
		}
		return;
	}

	let phylum = responsearray['phylum'];
	let postid = responsearray['postid'];

	if (responsearray['error'] == "INFO_FLAG_CLEAR_SUCCESS") {
		index_js_deleteElementById(phylum+'approveicon_'+postid);
		index_js_emptyElementById(phylum+'flagdiv_'+postid);
		$(phylum+'flagdiv_'+postid).classList.remove('poststatusflagged');
		$(phylum+'_'+postid).classList.remove('postflagged');
		$(phylum+'_'+postid).classList.add('postapprovalnormal');
		$(phylum+'flag_'+postid).innerHTML = responsearray['flagpostlinktext'];
		$(phylum+'flag_'+postid).onclick = function() {
				index_js_flagPost(phylum, postid,
						responsearray['newflagpostvalue']); }
		return;
	}

	let showflagged = responsearray['showflagged'];
	if (!showflagged) {
		index_js_deleteElementById(phylum+'_'+postid);
		index_js_deleteElementById(phylum+'location_'+postid);
		return;
	}

	/* Fall through if showing flagged post */
	$(phylum+'_'+postid).classList.add('postflagged');
	$(phylum+'flagdiv_'+postid).className = 'poststatusflagged';
	$(phylum+'flagdiv_'+postid).innerHTML = responsearray['flagdivinnerHTML'];
	$(phylum+'flag_'+postid).innerHTML = responsearray['flagpostlinktext'];
	$(phylum+'flag_'+postid).onclick = function() {
			index_js_flagPost(phylum, postid,
					responsearray['newflagpostvalue']); }

	/* Check if approve icon to host flag icon needs to be created */
	if ($(phylum+'approveicon_'+postid) == null) {
		let approveiconimg = document.createElement('img');
		approveiconimg.id = phylum+'approveicon_'+postid;
		$(phylum+'popupicon_'+postid).parentNode.insertBefore(approveiconimg,
				$(phylum+'popupicon_'+postid));
	}
	$(phylum+'approveicon_'+postid).src = '/img/icon-post-flagged.png';
}

/**
 * Function called after modal dialog below returns a specific value.
 */
function index_js_flagPost_sendRequest(phylum, postid, flagrequest, flagreason)
{
	var flagpostformdata = new FormData();
	flagpostformdata.append("phylum", phylum);
	flagpostformdata.append("postid", postid);
	flagpostformdata.append("flagrequest", flagrequest);
	flagpostformdata.append('flagreason', flagreason);
	flagpostformdata.append("ajax_command", "flag_post")

	xhrFlagPost = new XMLHttpRequest();
	xhrFlagPost.onreadystatechange = index_js_flagPost_AjaxFunction;
	xhrFlagPost.open("POST", "/index.php", true);
	xhrFlagPost.send(flagpostformdata);
	return false;
}

function index_js_closeModalPrompt()
{
	$('body').style.overflow = 'auto';
	$('modalprompt').style.display = 'none';
	index_modalprompt = false;
}

/**
 * Functions to set or remove the custom domain for a post.
 * Internal functions check for privileged accounts before saving the domain name.
 */
function index_js_setPostDomainName_ajaxFunction()
{
	if (xhrSetPostDomainName.readyState != 4)
		return; /* Nothing to do here. */

	var responsearray = index_js_parseResponseText(xhrSetPostDomainName.responseText,
			"Unable to set custom domain name for post.\n\nTry again or contact the website administrators.");
	if (!responsearray)
		return;

	if (responsearray['error'] != "INFO_SET_POST_DOMAIN_NAME_SUCCESS") {
		alert("<?=__LINE__?>: Unable to set custom domain: "+responsearray['error']);
		return;
	}

	newdomain = responsearray['domainname'];
	phylum = responsearray['phylum'];
	id = responsearray['id'];
	$(phylum+'creditsdomainname_'+id).innerHTML = responsearray['domainnamelink'];
	$(phylum+'setdomainnamemenuitem_'+id).onclick = function() {
			index_js_setPostDomainName(phylum, id, newdomain); }
}

function index_js_setPostDomainName(phylum, id, domainname)
{
	newdomain = prompt("Enter a valid domain name\nwithout the leading http:// or https://\nLeave blank to remove custom domain",
			domainname);

	if (newdomain === null) {
		return; /* Cancel pressed */
	}

	var setpostdomainnameformdata = new FormData();
	setpostdomainnameformdata.append("setpostdomainnamephylum", phylum);
	setpostdomainnameformdata.append("setpostdomainnameid", id);
	setpostdomainnameformdata.append('domainname', newdomain);
	setpostdomainnameformdata.append("ajax_command", "set_post_domain_name");
	xhrSetPostDomainName = new XMLHttpRequest();
	xhrSetPostDomainName.onreadystatechange = index_js_setPostDomainName_ajaxFunction;
	xhrSetPostDomainName.open("POST", "/index.php", true);
	xhrSetPostDomainName.send(setpostdomainnameformdata);
	return false;
}

/**
 * Following function turn any DIV into a fixed div with a specific position.
 * Mainly designed for popup menus.
 * - Only non-zero values specified in top/right/bottom/left will be used.
 *   This way, we can choose to use top-right or bottom-left corners by
 *   specifying only those coordinates and passing the others as zero.
 */
function index_js_showModalPopup(id, top, right, bottom, left)
{
	/* Clear any pre-existing popup */
	if (index_modalpopupid) {
		$(index_modalpopupid).style.display = 'none';
	}

	index_modalpopupid = id;
	index_modalpopuploaded = true;
	$(id).style.display = 'block';

	if (top)
		$(id).style.top = top+'px';

	if (right)
		$(id).style.right = right+'px';

	if (bottom)
		$(id).style.bottom = bottom+'px';

	if (left)
		$(id).style.left = left+'px';
}

function index_js_showContactListPopup(ev, popupid)
{
	/* If re-showing the same popup, exit early and let
	 * index_js_clickEvent() close the popup instead. */
	if (index_modalpopupid == popupid) {
		ev.blur();
		return false;
	}

	let rect = ev.getBoundingClientRect();
	let right = (rect.right - rect.left) + 20;
	let top = rect.top - 5;
	index_js_showModalPopup(popupid, top, right, 0, 0);

	return false;
}

/**
 * Generic popup functions.
 * The main function (index_js_popupDiv) turns any DIV in to a popup that can
 * be closed clicking anywhere outside the DIV, or by calling _popupDiv again.
 * This function assumes the DIV has `display:none;` to begin with.
 */
function index_js_popupDiv(popupid)
{
	if (!(popupid && document.body.contains($(popupid)))) {
		console.warn('<?=__LINE__?> index_js_popupDiv invalid id: '+popupid+';');
		return; /* Invalid element id, nothing to popup */
	}

	/* Close if being re-called or another popup already displayed */
	if (index_popupdivid) {
		$(index_popupdivid).style.display = 'none';
		if (index_popupdivid === popupid) {
			if (document.activeElement) {
				document.activeElement.blur();
			}
			index_popupdivid = false;
			return;
		}
	}

	$(popupid).style.display = 'block';
	index_popupdivid = popupid;
	index_popupdivinit = true;
}

/**
 * Moves an HTML element with 'position:fixed;' style. It depends on other
 * functions to change popupmenu from 'display:none;' to 'display:block;'.
 * - popupiconid: HTML element ID to attach menu to.
 * - popupmenuid: HTML element ID of menu.
 */
function index_js_movePopupMenu(popupiconid, popupmenuid)
{
	let iconrect = $(popupiconid).getBoundingClientRect();
	let menurect = $(popupmenuid).getBoundingClientRect();

	index_popupiconid = popupiconid;
	index_popupmenuid = popupmenuid;
	$(popupmenuid).style.top = Math.ceil(iconrect.bottom)+'px';
	$(popupmenuid).style.left = Math.ceil((iconrect.left+iconrect.width)-menurect.width)+'px';
}

/**
 * Generic modal functions.
 * The main function (index_js_modalDiv) turns any DIV in to a modal dialog
 * with a close button in the top-right corner.
 */
function index_js_closeModalDiv()
{
	$(index_modaldivid).style.display = 'none';
	index_modaldivoriginalparent.appendChild($(index_modaldivid));
	index_modaldivid = false;
	index_modaldivoriginalparent = false;
	$('modaldivbackground').appendChild($('modaldivclose'));
	$('modaldivbackground').style.display = 'none';
	$('body').style.overflow = 'auto';
}

/**
 * Main function to show any DIV in a modal way.
 * - divid: id of div to center and display.
 * - backbuttonid: id of any optional "cancel" button.
 */
async function index_js_showModalDiv(divid, backbuttonid)
{
	index_modaldivid = divid;
	/* Set up the promise first, then show the modal div */
	var modalclosefunction; /* Used to track click function for removal */
	var returnvalue;
	var promise = new Promise((resolve) => {
		modalclosefunction = function() {
			resolve(0);
		}
		$('modaldivclose').addEventListener('click', modalclosefunction);
		$(backbuttonid).addEventListener('click', modalclosefunction);
	});

	/* Now show the modal div */
	index_modaldivoriginalparent = $(index_modaldivid).parentNode;
	$('modaldivbackground').appendChild($(index_modaldivid));
	$(index_modaldivid).style.display = 'block';
	$(index_modaldivid).appendChild($('modaldivclose'));
	$('modaldivbackground').style.display = 'flex';
	$('body').style.overflow = 'hidden';

	/* Halt while we wait for a response to return */
	await promise.then((result) => {
		returnvalue = result;
		$('modaldivclose').removeEventListener('click',
				modalclosefunction, true);
		$(backbuttonid).removeEventListener('click',
				modalclosefunction, true);
		console.log('billing.php:<?=__LINE__?> calling index_js_closeModalDiv()');
		index_js_closeModalDiv();
	});

	return returnvalue;
}

async function index_js_modalDiv(divid, backbuttonid)
{
	await index_js_showModalDiv(divid, backbuttonid)
	.then ((result) => {
		console.log('index.php:<?=__LINE__?> What are we going to do with result='+result);
	});
}

/**
 * Displays an array of strings as buttons and returns index of string pressed
 * Returns value via Promise resolve:
 * - array index of string clicked, or 0 if cancel/Esc/close button clicked.
 */
async function index_js_modalPrompt(submitButtonLabel, cancelButtonLabel,
		inputType, stringArray)
{
	index_modalprompt = true;
	if (index_popupmenuid) {
		$('modalpromptdialog').focus();
		index_popupmenuid = 0;
	}
	$('modalpromptheader').innerText = stringArray[0];
	index_js_emptyElementById('modalpromptcontent');
	/* Set up the promise first, then show modal dialog */
	var modalcancelfunction; /* Used to track anonymous click function for removal */
	var modalsubmitfunction; /* Also used */
	var chosenoption;
	var promise = new Promise((resolve) => {
		let s;
		for (s = 1; s < stringArray.length; s++) {
			const sval = s;
			if (inputType == 'radio') {
				let label = document.createElement('label');
				label.className = 'modalpromptlabel';
				label.setAttribute('for', 'modalPromptRadio'+sval);

				let inputSpan = document.createElement('span');
				label.appendChild(inputSpan);

				let promptInput = document.createElement('input');
				promptInput.className = 'modalpromptradio';
				promptInput.type = 'radio';
				promptInput.name = 'modalPromptRadio';
				promptInput.value = sval;
				promptInput.id = 'modalPromptRadio'+sval;
				inputSpan.appendChild(promptInput);

				let promptInputSpan = document.createElement('span');
				promptInputSpan.className = 'modalpromptinputspan';
				inputSpan.appendChild(promptInputSpan);

				let textSpan = document.createElement('span');
				textSpan.innerHTML = stringArray[s];
				label.appendChild(textSpan);

				$('modalpromptcontent').appendChild(label);
			}
		}
		if ((inputType == 'radio') || !inputType) {
			$('modalpromptsubmitbutton').innerText = submitButtonLabel;
		}
		$('modalpromptcancelbutton').innerText = cancelButtonLabel;
		modalcancelfunction = function() {
			resolve(0);
		}
		modalsubmitfunction = function() {
			if (document.querySelector('input[name=modalPromptRadio]:checked') == null) {
				$('modalprompthints').innerHTML = '<span style="color:#600;">Choose an option above or click Cancel</span>';
			} else {
				$('modalprompthints').innerHTML = '';
				let checked = document.querySelector('input[name=modalPromptRadio]:checked').value;
				resolve(checked);
			}
		}
		$('modalpromptclosebutton').addEventListener('click', modalcancelfunction);
		$('modalpromptcancelbutton').addEventListener('click', modalcancelfunction);
		$('modalpromptsubmitbutton').addEventListener('click', modalsubmitfunction);
		$('modalprompthints').innerHTML = '';
	});

	/* Now show the modal dialog */
	$('modalprompt').style.display = 'flex';
	$('body').style.overflow = 'hidden';

	/* Halt while we wait for a response to return */
	await promise.then((result) => {
		chosenoption = result;
		$('modalpromptclosebutton').removeEventListener('click',
				modalcancelfunction, true);
		$('modalpromptcancelbutton').removeEventListener('click',
				modalcancelfunction, true);
		$('modalpromptsubmitbutton').removeEventListener('click',
				modalsubmitfunction, true);
		index_js_closeModalPrompt();
	});
	if (chosenoption === undefined)
		return 0;

	return chosenoption;
}

/**
 *
 */
async function index_js_flagPost(phylum, postid, flagrequest)
{
	if (index_popupmenuid) {
		$(index_popupmenuid).style.display = 'none';
	}

	if (flagrequest == 1) {
		await index_js_modalPrompt('Flag', 'Cancel', 'radio',
				['Choose a suitable flag reason.',
				'<b>Mature Content</b><br>Usually rated PG or M15+. Not suitable for younger audiences. Explicit language, war scenes, minor violence.',
				'<b>Adult Content or Not Safe for Work (NSFW)</b><br>Usually rated R18+. Sex or explicit nudity, depictions of drug use, extreme violence and gore.',
				'<b>Copyright or Trademark Infringement</b><br>Unauthorised duplication or use of material not covered by fair use. Plagarism. Selling other people\'s work.',
				'<b>Illegal Content or Violation of Terms</b><br>Criminal activity, drug use, hate-speech, racism, abuse or harassment, impersonating another person, spam, fake news.'
				])
		.then((result) => {
			let flagtype = Math.ceil(result);
			let flagreason = '';
			switch(flagtype) {
			case 1: /* Mature Content */
				flagreason = 'MATURE_CONTENT';
				break;
			case 2: /* Adult/NSFW */
				flagreason = 'ADULT_CONTENT';
				break;
			case 3: /* Copyright or Trademark Infringement */
				flagreason = 'COPYRIGHT_OR_TRADEMARK';
				break;
			case 4: /* Illegal Content */
				flagreason = 'ILLEGAL_OR_VIOLATION';
				break;
			default:
				return; /* Nothing to do here */
			}

			if (flagreason) {
				index_js_flagPost_sendRequest(phylum, postid, flagrequest, flagreason);
			}
		});
	} else if (!flagrequest) {
		/* Send request to clear flag */
		index_js_flagPost_sendRequest(phylum, postid, flagrequest, false);
	}
}

/**
 *
 */
function index_js_shareByEmail_AjaxFunction()
{
	if (xhrEmailPost.readyState != 4)
		return; /* Nothing to do here. */

	var responsearray = index_js_parseResponseText(xhrEmailPost.responseText, "Could not to share by email.\n\nTry again or contact the website administrators.");
	if (!responsearray)
		return;

	if (responsearray['error'] != "INFO_SHARE_BY_EMAIL_SENT") {
		console.log("<?=__LINE__?>: What is this? "+responsearray['error']);
		alert("The email to share post could not be sent.\n\nYou could try again later to see if it sends.");
		return;
	}

	var postid = responsearray['postid'];
}

/**
 * id: Post id to subscribe or unsubscribe to.
 */
function index_js_subscribeByEmail(postid)
{
	alert("<?=__LINE__?> Subscribe! (or unsubscribe...)");
}

/**
 * id: Post id to share.
 */
function index_js_shareByEmail(postid)
{
	var emailaddress = prompt("Enter an email to share this post to:");
	if (emailaddress == null) {
		return false;
	}

	if (emailaddress.match(/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/) == null) {
		alert("Invalid email address.\n\nPost has not been shared.");
		return false;
	}

	var sharebyemaildata = new FormData();
	sharebyemaildata.append("sharebyemailpostid", postid);
	sharebyemaildata.append("sharebyemailaddress", emailaddress);
	sharebyemaildata.append("ajax_command", "share_by_email")

	xhrEmailPost = new XMLHttpRequest();
	xhrEmailPost.onreadystatechange = index_js_shareByEmail_AjaxFunction;
	xhrEmailPost.open("POST", "/index.php", true);
	xhrEmailPost.send(sharebyemaildata);
	return false;
}

/**
 *
 */
function index_js_sendAnalyticData(pageid, phpsessionid)
{
	const pgid = pageid;
	const snid = phpsessionid;

	/* Find out browser window sizes, rather than actual screen size */
	let windowWidth = window.innerWidth;

	let documentReferrer = document.referrer;

	let fd = new FormData();
	fd.append('ajax_command', 'send_analytics');
	fd.append('pageid', pageid);
	fd.append('phpsessionid', phpsessionid);
	fd.append('windowWidth', windowWidth);
	fd.append('documentReferrer', documentReferrer);

	fetch('/index.php', {
		method:'post',
		body:fd
	})
	.then((response) => {
		return response.json();
	})
	.then(result => {
		window.setTimeout(function(){index_js_sendAnalyticData(pgid, snid);}, 15000);
	})
	.catch(error => console.warn(error));
}

/**
 *
 */
function index_js_ajax_checkForNewerPosts(latestpostdate)
{
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if ((xhr.readyState == 4) && (xhr.status == 200)) {
			var responsearray = JSON.parse(xhr.responseText);
			if (responsearray['error'] == "INFO_NEWER_POSTS_FOUND") {
				var i;
				for (i = 0; i < responsearray['posts'].length; i++) {
					var postdiv = document.createElement("div");
					var postdivcredits = document.createElement("div");
					var postdivdescription = document.createElement("div");
					var r = responsearray['posts'][i];
					postdiv.className = "postblock";
					postdivcredits.className = "postcredits";
					postdivcredits.innerHTML = "<a href=\"/"+r['username']+"\">"+r['username']+"</a> "+r['added']+" <a href=\"/post/"+r['id']+"\">"+r['id']+"</a>";
					postdivdescription.innerHTML = r['description'];
					postdiv.appendChild(postdivcredits);
					postdiv.appendChild(postdivdescription);
					$('page').appendChild(postdiv);
				} // */
			} else {
				/* Handle this as an error somehow, eventually */
			}
		}
	}
	xhr.open("POST", "/index.php", true);
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.send("ajax_command=get_newer_posts&earliestpostdate="+encodeURIComponent(index_earliestpostdate)+"&latestpostdate="+encodeURIComponent(index_latestpostdate));
}

/**
 *
 */
function index_js_setupProfileImg(ev)
{
	if ($('profileimg').src.match(/^.*<?=$_SERVER['HTTP_HOST']?>\/img\/defaultprofile.jpg$/)) {
		$('profileimgzoomslider').style.display = "none";
		return;
	}

	/* TODO: This part can be easily simplified! *index_profileimg* is REDUNDANT! */
	if ((index_profileimg == null) && (ev)) {
		index_profileimg = ev;
		index_profileimg.addEventListener('mousedown',
				index_js_startProfileImgDrag, false);
	}

	if (index_profileimg == null) return;

	/* TODO: Add window.attachEvent for IEs before 9. */
	index_profileimgwd = ev.width; //index_profileimg.width;
	index_profileimght = ev.height; //index_profileimg.height;
	if (index_profileimgwd < index_profileimght) {
		index_profileimg.style.width = '256px';
		index_profileimg.style.height = '';
		index_profileimg.style.top = Math.ceil((256 - index_profileimg.height) / 2)+'px';
	} else {
		index_profileimg.style.height = '256px';
		index_profileimg.style.width = '';
		index_profileimg.style.left = Math.ceil((256 - index_profileimg.width) / 2)+'px';
	}

	$('profileimgzoomslider').style.display = "block";
	index_js_setupslider('range-slider', function(value){
		index_js_pimg_zoom(value);
	});
}

/**
 *
 */
function index_js_startProfileImgDrag(ev)
{
	if (index_profileimgdivdragdata) return; /* Move along. Nothing to do here. */

	/* TODO: Add window.attachEvent for IEs before 9. */
	document.body.addEventListener('mousemove', index_profileImageDrag, false);
	document.body.addEventListener('mouseup', index_profileImageStopDrag, false);
	ev = ev || event;
	index_profileimgdivdragdata = {
		x: ev.clientX-index_profileimg.offsetLeft,
		y: ev.clientY-index_profileimg.offsetTop
	};
}

/**
 *
 */
function index_profileImageDrag(ev)
{
	if (!index_profileimgdivdragdata) return; /* Move along. Nothing to do here. */

	ev = ev || event;
	var x = ev.clientX-index_profileimgdivdragdata.x;
	var y = ev.clientY-index_profileimgdivdragdata.y;
	var wd = index_profileimg.width;
	var ht = index_profileimg.height;

	if (x > 0) {
		x = 0;
	} else if ((wd + x) < 256) {
		x = 256 - wd;
	}
	if (y > 0) {
		y = 0;
	} else if ((ht + y) < 256) {
		y = 256 - ht;
	}
	index_profileimg.style.left = x+"px";
	index_profileimg.style.top = y+"px";
	/* Can the next 4 lines be broken off as a function? PHOTOCROPDATA */
	$('profileeditphotowidth').value = Math.floor((256 * (index_profileimgwd / wd)) + 0.0);
	$('profileeditphotoheight').value = Math.floor((256 * (index_profileimght / ht)) + 0.0);
	$('profileeditphotoleft').value = Math.abs(Math.floor((x * (index_profileimgwd / wd)) + 0.0));
	$('profileeditphototop').value = Math.abs(Math.floor((y * (index_profileimght / ht)) + 0.0));
}

/**
 *
 */
function index_profileImageStopDrag(ev)
{
	if (!index_profileimgdivdragdata) return; /* Move along. Nothing to do here. */

	/* TODO: Add window.detachEvent for IEs before 9. */
	document.body.removeEventListener('mousemove',
			index_profileImageDrag ,false);
	document.body.removeEventListener('mouseup',
			index_profileImageStopDrag, false);
	ev = ev || event;
	var x = ev.clientX-index_profileimgdivdragdata.x;
	var y = ev.clientY-index_profileimgdivdragdata.y;
	var wd = index_profileimg.width;
	var ht = index_profileimg.height;
	if (x > 0) {
		x = 0;
	} else if ((wd + x) < 256) {
		x = 256 - wd;
	}
	if (y > 0) {
		y = 0;
	} else if ((ht + y) < 256) {
		y = 256 - ht;
	}
	index_profileimg.style.left = x+"px";
	index_profileimg.style.top = y+"px";
	index_profileimgdivdragdata = null;
}

/**
 *
 */
function index_js_pimg_zoom(amt)
{
	var old_wd = index_profileimg.width;
	var old_ht = index_profileimg.height;
	var new_wd = (256 * (1 - amt)) + (index_profileimgwd * (4 * amt));
	var new_ht = (256 * (1 - amt)) + (index_profileimght * (4 * amt));
	var pimg_ratio = 1;

	if (index_profileimgwd < index_profileimght) {
		index_profileimg.style.width = new_wd+'px';
		index_profileimg.style.height = '';
		new_ht = index_profileimg.height;
		pimg_ratio = index_profileimght / new_ht;
	} else {
		index_profileimg.style.width = '';
		index_profileimg.style.height = new_ht+'px';
		new_wd = index_profileimg.width;
		pimg_ratio = index_profileimgwd / new_wd;
	}
	var new_x = index_profileimg.x + ((old_wd - new_wd) * ((128 - index_profileimg.x) / old_wd));
	var new_y = index_profileimg.y + ((old_ht - new_ht) * ((128 - index_profileimg.y) / old_ht));

	if (new_x > 0) {
		new_x = 0;
	} else if ((new_wd + new_x) < 256) {
		new_x = 256 - new_wd;
	}

	if (new_y > 0) {
		new_y = 0;
	} else if ((new_ht + new_y) < 256) {
		new_y = 256 - new_ht;
	}
	index_profileimg.style.left = new_x+'px';
	index_profileimg.style.top = new_y+'px';
	/* Can the next 4 lines be broken off as a function? PHOTOCROPDATA */
	$('profileeditphotowidth').value = Math.floor((256 * (index_profileimgwd / new_wd)) + 0.0);
	$('profileeditphotoheight').value = Math.floor((256 * (index_profileimght / new_ht)) + 0.0);
	$('profileeditphotoleft').value = Math.abs(Math.floor((new_x * (index_profileimgwd / new_wd)) + 0.0));
	$('profileeditphototop').value = Math.abs(Math.floor((new_y * (index_profileimght / new_ht)) + 0.0));
}

/**
 *
 */
function index_js_setupslider(id, dragfunction)
{
	var range = document.getElementById(id);
	var dragger = range.children[0];
	var mousedown = false;
	var rangewidth, rangeLeft;

	var draggerWidth = dragger.offsetWidth;
	dragger.style.left = '0';
	dragger.style.marginLeft = '0';

	range.addEventListener('mousedown', function slidermousedown(e) {
		rangeWidth = this.offsetWidth;
		rangeLeft = this.offsetLeft;
		mousedown = true;
		updateDragger(e);
		//range.removeEventListener('mousedown', slidermousedown);
		return false;
	});

	range.addEventListener('mousemove', function slidermousemove(e) {
		updateDragger(e);
		//range.removeEventListener('mousemove', slidermousemove);
	});

	range.addEventListener('mouseup', function slidermouseup() {
		mousedown = false;
		//range.removeEventListener('mouseup', slidermouseup);
	});

	function updateDragger(e) {
		if (!mousedown) return; /* Nothing to do here! */

		var amt = (e.pageX - (rangeLeft + draggerWidth + (draggerWidth / 2))) / (rangeWidth - draggerWidth);

		if (amt <= 0) {
			amt = 0;
		} else if (amt >= 1) {
			amt = 1;
		}
		dragger.style.left = ((((rangeLeft + rangeWidth) - draggerWidth) * amt) - 1) + 'px';
		dragfunction(amt);
	}
}

/**
 *
 */
function index_js_previewProfileImg()
{
	var f = $('profileimgfileinput').files[0];

	//*
	if (!f.type.match('image.(gif|jpeg|jpg|png)')) {
		return;
	}

	var reader = new FileReader();
	reader.onload = function(ev) {
		$('profileimg').style.width = '';
		$('profileimg').style.height = '';
		//index_profileimg = $('profileimg');
		$('profileimg').onload = function() {
			index_js_setupProfileImg($('profileimg'));
			index_js_pimg_zoom(0);
		}
		$('profileimg').src = this.result;
	};
	reader.readAsDataURL(f);
}

/**
 * Change the editability state of input fields.
 */
function index_js_editProfileInput(inputid, editable)
{
	var inputnode = $(inputid);

	if (inputnode.nodeName == "DIV") {
		inputnode.contentEditable = editable;
	} else if (inputnode.nodeName == "INPUT") {
		if (editable) {
			inputnode.removeAttribute('readonly');
		} else {
			inputnode.setAttribute('readonly', 'true');
		}
	}

	if (editable) {
		inputnode.classList.add("profiletabledataeditable");
		inputnode.classList.add("shadowinset");
	} else {
		inputnode.classList.remove("profiletabledataeditable");
		inputnode.classList.remove("shadowinset");
	}
}

/**
 *
 */
function index_js_editProfileImg(editable)
{
	if (editable) {
		$('profileimgeditcontrols').style.display = "block";
		$('profileinformationform').style.display = "block";
		$('profileimg').onload = index_js_setupProfileImg($('profileimg'));
	} else {
		$('profileimgzoomslider').style.display = "none";
		$('profileimgeditcontrols').style.display = "none";
		$('profileinformationform').style.display = "none";
		$('profileimg').onload = null;
	}
}

function index_js_editProfileDeleteImg()
{
	$('profileeditimagefileid').value = 0;
	$('profileimgfileinput').value = '';
	$('profileeditimgsrc').value = '/img/defaultprofile.jpg';
	$('profileimg').src = '/img/defaultprofile.jpg';
	index_js_setupProfileImg($('profileimg'));
}

function index_js_editProfile(editable)
{
	/* Preserve original values for restore on cancel. */
	/* This is the same block of code used to send the update profile data, can it be reduced? */
	$('profileeditfullname').value = $('profilefullname').value;
	$('profileeditemail').value = $('profileemail').value;
	$('profileeditwebsite').value = $('profilewebsite').value;
	$('profileeditaddress').value = $('profileaddress').value;
	$('profileeditaddress2').value = $('profileaddress2').value;
	$('profileedittown').value = $('profiletown').value;
	$('profileeditstate').value = $('profilestate').value;
	$('profileeditpostcode').value = $('profilepostcode').value;
	$('profileeditcountry').value = $('profilecountry').value;
	$('profileeditinterests').value = $('profileinterests').value;
	$('profileeditabout').value = $('profileabout').value;
	$('profileeditimagefileid').value = $('profileeditimagefileid').value;

	index_js_editProfileInput('profilefullname', editable);
	index_js_editProfileInput('profileemail', editable);
	index_js_editProfileInput('profilewebsite', editable);
	index_js_editProfileInput('profileaddress', editable);
	index_js_editProfileInput('profileaddress2', editable);
	index_js_editProfileInput('profiletown', editable);
	index_js_editProfileInput('profilestate', editable);
	index_js_editProfileInput('profilepostcode', editable);
	index_js_editProfileInput('profilecountry', editable);
	index_js_editProfileInput('profileinterests', editable);
	index_js_editProfileInput('profileaboutcontenteditable', editable);

	if (editable) {
		$('editprofilebutton').style.display = 'none';
	} else {
		$('editprofilebutton').style.display = 'block';
	}
	index_js_emptyElementById('saveprofileinformationsuggestions');
	$('profilefullname').focus();

	index_js_editProfileImg(editable);
}

/**
 * Restore profile info before edit.
 */
function index_js_editProfileCancel()
{
	$('profileimg').style.width = '';
	$('profileimg').style.height = '';
	$('profileimg').style.top = '';
	$('profileimg').style.left = '';
	$('profilefullname').value = $('profileeditfullname').value;
	$('profileemail').value = $('profileeditemail').value;
	$('profilewebsite').value = $('profileeditwebsite').value;
	$('profileaddress').value = $('profileeditaddress').value;
	$('profileaddress2').value = $('profileeditaddress2').value;
	$('profiletown').value = $('profileedittown').value;
	$('profilestate').value = $('profileeditstate').value;
	$('profilepostcode').value = $('profileeditpostcode').value;
	$('profilecountry').value = $('profileeditcountry').value;
	$('profileinterests').value = $('profileeditinterests').value;
	$('profileimg').src = $('profileeditimgsrc').value;
	$('profileabout').value = $('profileeditabout').value;
	$('profileaboutcontenteditable').innerHTML = $('profileeditabout').value;

	index_js_editProfile(false);
}

/**
 *
 */
function index_js_checkProfileInformation_AjaxFunction()
{
	if (xhrCheckProfileInfo.readyState != 4)
		return;

	var responsearray = index_js_parseResponseText(xhrCheckProfileInfo.responseText, "Unable to save profile.\n\nTry again or contact the website administrators.");
	if (!responsearray)
		return;

	if (responsearray['error'] != "INFO_PROFILE_INFORMATION_SAVED") {
		$('saveprofileinformationsuggestions').innerHTML = responsearray['error'];
		return;
	}

	/* TODO: This is also used for post description images. Can this be a function instead? */
	profileimgfileid = responsearray['profileimgfileid'];
	profileimgfilename = responsearray['profileimgfilename'];
	/* TODO: FIXME01 Check .htaccess: filesrc = "file/"+uploadfileid+"/"+uploadedfilename; */
	if (profileimgfilename && profileimgfileid) {
		filesrc = "/index.php?fileid="+profileimgfileid+"&filename="+profileimgfilename;
		$('profileimg').src = filesrc;
		$('profileimg').style.width = '';
		$('profileimg').style.height = '';
		$('profileimg').style.top = '';
		$('profileimg').style.left = '';
	}

	$('saveprofileinformationsuggestions').innerHTML = "Profile information saved!";
	index_js_editProfile(false);
}

function index_js_checkProfileInformation()
{
	/* Update profile data. */
	$('profileeditfullname').value = $('profilefullname').value;
	$('profileeditemail').value = $('profileemail').value;
	$('profileeditwebsite').value = $('profilewebsite').value;
	$('profileeditaddress').value = $('profileaddress').value;
	$('profileeditaddress2').value = $('profileaddress2').value;
	$('profileedittown').value = $('profiletown').value;
	$('profileeditstate').value = $('profilestate').value;
	$('profileeditpostcode').value = $('profilepostcode').value;
	$('profileeditcountry').value = $('profilecountry').value;
	$('profileeditinterests').value = $('profileinterests').value;
	$('profileeditabout').value = $('profileabout').value;

	var profileinformationformdata = new FormData($('profileinformationform'));
	profileinformationformdata.append("ajax_command", "save_profile_information");

	xhrCheckProfileInfo = new XMLHttpRequest();
	xhrCheckProfileInfo.onreadystatechange = index_js_checkProfileInformation_AjaxFunction;
	xhrCheckProfileInfo.open("POST", "/index.php", true);
	xhrCheckProfileInfo.send(profileinformationformdata); // */
	$('saveprofileinformationsuggestions').innerHTML = "Saving. please wait <img src=\"/img/thinking12.gif\" width=\"12\" height\"12\">";
	return false;
}

function index_js_checkProfileSettingsFunction()
{
	if (xhrCheckProfileSettings.readyState != 4)
		return;

	var responsearray = index_js_parseResponseText(xhrCheckProfileSettings.responseText, "Unable to save settings.\n\nTry again or contact the website administrators.");
	if (!responsearray)
		return;

	if (responsearray['error'] != "INFO_PROFILE_SETTINGS_SAVED") {
		$('saveprofilesettingssuggestions').innerHTML = responsearray['error'];
		return;
	}

	$('saveprofilesettingssuggestions').innerHTML = "Profile settings saved!";
}

function index_js_checkProfileSettings()
{
	/* Check for any form errors before sending */
	var emailnode = $('profilesettingsaccountemail');
	var email = emailnode.value;
	var emailregexp = email.match(/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/);
	var error_found = false;
	var settingssuggestions = '';

	if (emailregexp == null) {
		if (email.length) {
			error_found = true;
			index_js_setInputErrorColour(emailnode, true);
			settingssuggestions += '<div>&bull; Invalid email address.</div>';
		}
	} else {
		index_js_setInputErrorColour(emailnode, false);
	}

	if (error_found) {
		$('saveprofilesettingssuggestions').innerHTML = settingssuggestions;
		return false;
	}

	var profilesettingsformnode = $('profilesettingsform');
	var profilesettingsformdata = new FormData(profilesettingsformnode);
	profilesettingsformdata.append("ajax_command", "save_profile_settings");

	xhrCheckProfileSettings = new XMLHttpRequest();
	xhrCheckProfileSettings.onreadystatechange = index_js_checkProfileSettingsFunction;
	xhrCheckProfileSettings.open("POST", "/index.php", true);
	xhrCheckProfileSettings.send(profilesettingsformdata); // */
	$('saveprofilesettingssuggestions').innerHTML = "Saving. please wait <img src='/img/thinking12.gif' width='12' height='12'>";
	return false;
}

function index_js_confirmTermsOfUse()
{
	if (!confirm("You are about to use a later version of the Terms of Use and Privacy Policy - this can not be undone.\nAre you sure? (reword and not working)"))
		return; /* Don't discard already edited post. */
}

function index_js_checkChangePassword_AjaxFunction()
{
	if (xhrChangePassword.readyState != 4)
		return;

	var responsearray = index_js_parseResponseText(xhrChangePassword.responseText, "Unable to save password.\n\nTry again or contact the website administrators.");
	if (!responsearray)
		return;

	if (responsearray['error'] != "INFO_PASSWORD_CHANGED") {
		$('savepasswordsuggestions').innerHTML = responsearray['error'];
		return;
	}

	$('savepasswordsuggestions').innerHTML = "Password changed!";

	/* Return to home page on password reset links */
	if (responsearray['resetlinkid']) {
		alert('Password changed!');
		window.location.href = responsearray['windowlocationhref'];
	}
}

function index_js_checkChangePassword() /* What's happening here? */
{
	var newpassword = $('newpassword').value;
	var newpasswordcheck = $('newpasswordcheck').value;
	var profilepassword = '';
	if ($('resetlinkid')) {
		profilepassword = $('resetlinkid').value;
	} else {
		profilepassword = $('profilepassword').value;
	}

	if (newpassword != newpasswordcheck) {
		$('savepasswordsuggestions').innerHTML = "New passwords don't match";
		return false;
	} else if (!profilepassword) {
		$('savepasswordsuggestions').innerHTML = "Enter current password";
		return false;
	}

	var profilechangepasswordformdata = new FormData($('profilechangepasswordform'));
	profilechangepasswordformdata.append("ajax_command", "change_password");

	xhrChangePassword = new XMLHttpRequest();
	xhrChangePassword.onreadystatechange = index_js_checkChangePassword_AjaxFunction;
	xhrChangePassword.open("POST", "/index.php", true);
	xhrChangePassword.send(profilechangepasswordformdata); // * /
	$('savepasswordsuggestions').innerHTML = "Saving. please wait <img src=\"/img/thinking12.gif\" width=\"12\" height\"12\">";
	/* */
	return false;
}

function index_js_deleteAccount_AjaxFunction()
{
	if (xhrDeleteAccount.readyState != 4)
		return;

	var responsearray = index_js_parseResponseText(xhrDeleteAccount.responseText, "Could not delete account.\n\nTry again or contact the website administrators.");
	if (!responsearray)
		return;

	$('deleteaccountsuggestions').innerHTML = responsearray['message'];
	if (responsearray['error'] != "INFO_ACCOUNT_DELETED") {
		return;
	}

	alert("Account Deleted! Returning you to the home page.");
	window.location.href = '/';
}

function index_js_deleteAccount()
{
	var profilepassword = $('profilepassword').value;
	if (!profileusername || !profilepassword) {
		$('deleteaccountsuggestions').innerHTML = "Incomplete login details.";
		return false;
	}

	var deleteaccountmessage = "Deleting account... <img src=\"/img/thinking12.gif\" width=\"12\" height\"12\">";

	var deleteaccountformdata = new FormData($('deleteaccountform'));
	deleteaccountformdata.append("ajax_command", "delete_account");

	xhrDeleteAccount = new XMLHttpRequest();
	xhrDeleteAccount.onreadystatechange = index_js_deleteAccount_AjaxFunction;
	xhrDeleteAccount.open("POST", "/index.php", true);
	xhrDeleteAccount.send(deleteaccountformdata); // * /
	$('deleteaccountsuggestions').innerHTML = deleteaccountmessage;

	return false;
}

function index_js_addContact_AjaxFunction()
{
	if (xhrAddContact.readyState != 4)
		return;

	var responsearray = index_js_parseResponseText(xhrAddContact.responseText, false);
	if (!responsearray)
		return;

	if (responsearray['error'] != "INFO_CONTACT_ADDED") {
		console.log("<?=__LINE__?>: What are we going to do with this?! - "+responsearray['error']);
		return;
	}

	index_js_getUpdates(); /* Add or remove the side panel entry in here */
}

/**
 * Get a group page's index currently signed in user's contact list.
 * Return values: -1: page not in group list, 0 or above, index in group list.
 */
function index_js_getGroupIndex(pageid)
{
	for (var i = 0, ct = index_grouplistarray.length; i < ct; i++) {
		if (index_grouplistarray[i]['pageid'] == pageid) {
			return i;
		}
	}

	return -1;
}

/**
 * Get a contact's index currently signed in user's contact list.
 * Return values: -1: username not in contact list, 0 or above, index in contact list.
 */
function index_js_getContactIndex(contactusername)
{
	for (var i = 0, ct = index_contactlistarray.length; i < ct; i++) {
		if (index_contactlistarray[i]['username'].toLowerCase() == contactusername.toLowerCase()) {
			return i;
		}
	}

	return -1;
}

/**
 * Add a person as contact (and optionally as trusted.)
 */
function index_js_addContact(contactusername, trusted)
{
	if (trusted)
		trusted = 1;
	else
		trusted = 0;

	var i = index_js_getContactIndex(contactusername);
	if (i == -1) {
		// console.log('<?=__LINE__?> COMMENTED Adding '+contactusername+' to contact list! (trusted='+trusted+')');
	} else if (trusted == 0) {
		$('memberlinktrust_'+contactusername).innerHTML = 'Star';
		$('memberlinktrust_'+contactusername).onclick = function() { index_js_addContact(contactusername, true); }
		$('otherslist').appendChild($('contactlistentry_'+contactusername));
		index_contactlistarray[i]['trusted'] = 0;
	} else {
		/* Promoting '+contactusername+' from contacts to trusted list! */
		$('memberlinktrust_'+contactusername).innerHTML = 'Unstar';
		$('memberlinktrust_'+contactusername).onclick = function() { index_js_addContact(contactusername, false); }
		$('contactslist').appendChild($('contactlistentry_'+contactusername));
		index_contactlistarray[i]['trusted'] = 1;
	}

	if (i != -1) {
		$('memberlinkaddremove_'+contactusername).innerHTML = 'Remove';
		$('memberlinkaddremove_'+contactusername).onclick = function() { index_js_removeContact(contactusername); }
	}

	var addcontactformdata = new FormData();
	addcontactformdata.append("username", contactusername);
	addcontactformdata.append("trusted", trusted);
	addcontactformdata.append("ajax_command", "add_contact");

	xhrAddContact = new XMLHttpRequest();
	xhrAddContact.onreadystatechange = index_js_addContact_AjaxFunction;
	xhrAddContact.open("POST", "/index.php", true);
	xhrAddContact.send(addcontactformdata);

	return false;
}

function index_js_removeContact_AjaxFunction()
{
	if (xhrRemoveContact.readyState != 4)
		return;

	var responsearray = index_js_parseResponseText(xhrRemoveContact.responseText, false);
	if (!responsearray)
		return;

	if (responsearray['error'] != "INFO_CONTACT_REMOVED") {
		console.log("<?=__LINE__?>: What are we going to do with this?! - "+responsearray['error']);
		return;
	}

	var contactindex = index_js_getContactIndex(responsearray['contactusername']);
	index_contactlistarray.splice(contactindex, 1);
	index_js_deleteElementById('contactlistentry_'+responsearray['contactusername']);
}

/**
 * Remove a contact.
 */
function index_js_removeContact(contactusername)
{
	if (!confirm("Removing "+contactusername+" from contacts?")) {
		return false;
	}

	var removecontactformdata = new FormData();
	removecontactformdata.append("username", contactusername);
	removecontactformdata.append("ajax_command", "remove_contact");

	xhrRemoveContact = new XMLHttpRequest();
	xhrRemoveContact.onreadystatechange = index_js_removeContact_AjaxFunction;
	xhrRemoveContact.open("POST", "/index.php", true);
	xhrRemoveContact.send(removecontactformdata);

	return false;
}

/**
 * Shows and resizes one of the selected popup permissions editors
 * for the post editor.
 * - ev: node of the popup button being hovered on
 * - popupdivid: div ID to show and/or resize.
 * - setpopupids: true sets new global ids, false uses existing ids.
 */
function index_js_showeditorpopup(popupbuttonid, popupdivid, setpopupids)
{
	if (setpopupids) {
		index_editorpopupbuttonid = popupbuttonid;
		index_editorpopupdivid = popupdivid;
	}

	if (!popupdivid || !popupbuttonid)
		return;

	let popupbutton = $(index_editorpopupbuttonid);
	let popupdiv = $(index_editorpopupdivid);

	if ((popupdiv.offsetWidth == 0)
			&& (popupdiv.offsetHeight == 0)) {
		return false; /* Popup not visible */
	}

	let parentrect = popupbutton.getBoundingClientRect();
	popupdiv.style.top = parentrect.bottom+'px';
	popupdiv.style.left = parentrect.left+'px';
	popupdiv.style.width = parentrect.width+'px';

	/* Find the bottom scroll limit, and keep this popup above that. */
	let maxheight = Math.max(document.body.scrollHeight, document.body.offsetHeight,
			document.documentElement.clientHeight, document.documentElement.offsetHeight,
			document.documentElement.scrollHeight);
	let rect = popupdiv.getBoundingClientRect();
	let permissionspopupbottom = rect.top + window.scrollY + rect.height;
	if (permissionspopupbottom > maxheight) {
		popupdiv.style.top = (window.innerHeight - (rect.bottom - rect.top))+'px';
	}

	/* Also, check for off-screen popups to the right */
	let maxwidth = Math.max(document.body.scrollWidth, document.body.offsetWidth,
			document.documentElement.clientWidth, document.documentElement.offsetWidth,
			document.documentElement.scrollWidth);
	rect = popupdiv.getBoundingClientRect();
	let permissionspopupright = rect.left + window.scrollX + rect.width;
	if (permissionspopupright > maxwidth) {
		let newleft = window.innerWidth - (rect.right - rect.left);
		popupdiv.style.left = (newleft - 16)+'px';
	}
}

/**
 * Update permissions icon on editor, and switch between presets/advanced permissions.
 * - editor is string of either 'editpost' or 'messager'
 */
function index_js_updatePostPermissions(editor, id)
{
	if (index_manualpostpermissionsupdate)
		return;

	index_manualpostpermissionsupdate = true;
	let changeAdvancedPermissions = false;
	let checkRepliesPreset = false;
	let permissions = index_js_getEditorPermissions(editor);
	let newReplyPermissions = '' + permissions[0];
	let checkAgeRating = false;

	if (!$(editor+'allowunsigned').checked && (newReplyPermissions == '5')) {
		newReplyPermissions = '4';
	}

	/* Find out what was changed */
	switch (id) {
	case 'editpostpreset5':
	case 'editpostallowunsigned':
		/* Anyone preset and Allow Unsigned can be handled in the same block */
		if (!$(editor+'preset5').checked)
			break; /* Anyone preset not checked */

		if ($(editor+'allowunsigned').checked) {
			permissions = '505505';
			newReplyPermissions = '5';
		} else {
			permissions = '504404';
			newReplyPermissions = '4';
		}

		$('editpostpermissionsicon').src = '/img/icon-permissions-open.png';
		$('editpostpermissionstitle').innerText = 'Anyone';
		changeAdvancedPermissions = true;
		checkRepliesPreset = true;
		break;

	case 'editpostpreset3':
		$('editpostpermissionsicon').src = '/img/icon-permissions-members.png';
		$('editpostpermissionstitle').innerText = 'Group';
		permissions = '303303';
		changeAdvancedPermissions = true;
		newReplyPermissions = '3';
		checkRepliesPreset = true;
		break;

	case 'editpostpreset2':
		$('editpostpermissionsicon').src = '/img/icon-permissions-contacts.png';
		$('editpostpermissionstitle').innerText = 'Contacts';
		permissions = '202202';
		changeAdvancedPermissions = true;
		newReplyPermissions = '2';
		checkRepliesPreset = true;
		break;

	case 'editpostpreset1':
		$('editpostpermissionsicon').src = '/img/icon-permissions-recipient.png';
		$('editpostpermissionstitle').innerText = 'Recipients';
		permissions = '101101';
		changeAdvancedPermissions = true;
		newReplyPermissions = '1';
		checkRepliesPreset = true;
		break;

	case 'editpostpreset0':
		$('editpostpermissionsicon').src = '/img/icon-permissions-owner.png';
		$('editpostpermissionstitle').innerText = 'Owner';
		permissions = '000000';
		changeAdvancedPermissions = true;
		newReplyPermissions = '0';
		checkRepliesPreset = true;
		break;

	case 'editpostpresetacceptreplies':
	case 'editpostpresetrepliesneedapproval':
	case 'editpostpresetdisablereplies':
		checkRepliesPreset = true;
		break;

	case 'editpostview5':
		$('editpostpermissionsicon').src = '/img/icon-permissions-open.png';
		$('editpostpermissionstitle').innerText = 'Anyone';
		break;

	case 'editpostview4':
		$('editpostpermissionsicon').src = '/img/icon-permissions-all.png';
		$('editpostpermissionstitle').innerText = 'All Users';
		break;

	case 'editpostview3':
		$('editpostpermissionsicon').src = '/img/icon-permissions-members.png';
		$('editpostpermissionstitle').innerText = 'Group';
		break;

	case 'editpostview2':
		$('editpostpermissionsicon').src = '/img/icon-permissions-contacts.png';
		$('editpostpermissionstitle').innerText = 'Contacts';
		break;

	case 'editpostview1':
		$('editpostpermissionsicon').src = '/img/icon-permissions-recipient.png';
		$('editpostpermissionstitle').innerText = 'Recipients';
		break;

	case 'editpostview0':
		$('editpostpermissionsicon').src = '/img/icon-permissions-owner.png';
		$('editpostpermissionstitle').innerText = 'Owner';
		break;

	case 'editpostpresetgeneralcontent':
		$('editpostagerating').value = 3;
		checkAgeRating = true;
		break;

	case 'editpostpresetmature15':
		$('editpostagerating').value = 15;
		checkAgeRating = true;
		break;

	case 'editpostpresetadult18':
		$('editpostagerating').value = 18;
		checkAgeRating = true;
		break;

	case 'editpostagerating':
		checkAgeRating = true;
		break;

	case 'editpostviewmodefull':
		$('editpostviewmodeicon').src = '/img/icon-viewmode-full.png';
		break;

	case 'editpostviewmodethumbnail':
		$('editpostviewmodeicon').src = '/img/icon-viewmode-thumbnail.png';
		break;

	case 'editpostviewmodelist':
		$('editpostviewmodeicon').src = '/img/icon-viewmode-list.png';
		break;

	case 'editpostsortmodedate':
		$('editpostsortmodeicon').src = '/img/icon-sortmode-date.png';
		break;

	case 'editpostsortmodetitle':
		$('editpostsortmodeicon').src = '/img/icon-sortmode-title.png';
		break;
	}

	/**
	 * Now respond to item changes.
	 */

	if (checkRepliesPreset) {
		let repliespresetvalue = document.querySelector('input[name=editpostreplies]:checked').value;
		switch (repliespresetvalue) {
		case 'disablereplies':
			$('editpostrepliesicon').src = '/img/icon-post-blocked.png';
			permissions = index_js_setChar(permissions, 2, '0');
			permissions = index_js_setChar(permissions, 3, '0');
			/* Disable tags aswell */
			permissions = index_js_setChar(permissions, 5, '0');
			changeAdvancedPermissions = true;
			break;
		case 'repliesneedapproval':
			$('editpostrepliesicon').src = '/img/icon-post-pending.png';
			permissions = index_js_setChar(permissions, 2, newReplyPermissions);
			permissions = index_js_setChar(permissions, 3, '0');
			/* Disable tags aswell */
			permissions = index_js_setChar(permissions, 5, '0');
			changeAdvancedPermissions = true;
			break;
		case 'acceptreplies':
			$('editpostrepliesicon').src = '/img/icon-post-approved.png';
			/* Reply and Approve permissions are same as View */
			permissions = index_js_setChar(permissions, 2, newReplyPermissions);
			permissions = index_js_setChar(permissions, 3, newReplyPermissions);
			/* Enable tags aswell */
			permissions = index_js_setChar(permissions, 5, newReplyPermissions);
			changeAdvancedPermissions = true;
			break;
		}
	}

	if (checkAgeRating) {
		let agerating = Math.ceil($('editpostagerating').value);
		switch (agerating) {
		case 3:
			$('editpostpermissionsageratingtitle').innerText = 'General';
			$('editpostpresetgeneralcontent').checked = true;
			$('editpostcontentratingicon').src = '/img/icon-permissions-generalcontent.png';
			break;
		case 15:
			$('editpostpermissionsageratingtitle').innerText = 'Mature';
			$('editpostpresetmature15').checked = true;
			$('editpostcontentratingicon').src = '/img/icon-permissions-mature15.png';
			break;
		case 18:
			$('editpostpermissionsageratingtitle').innerText = 'Adult';
			$('editpostpresetadult18').checked = true;
			$('editpostcontentratingicon').src = '/img/icon-permissions-adult18.png';
			break;
		default:
			/* Enforce age rating limits */
			if (agerating < 3) {
				$agerating = 3;
				$('editpostagerating').value = agerating;
			} else if (agerating > 100) {
				$agerating = 100;
				$('editpostagerating').value = agerating;
			}
			$('editpostpresetgeneralcontent').checked = false;
			$('editpostpresetmature15').checked = false;
			$('editpostpresetadult18').checked = false;
			$('editpostcontentratingicon').src = '/img/icon-permissions-advanced.png';
			$('editpostpermissionsageratingtitle').innerText = 'Ages '+agerating+'+';
		}
	}

	if (changeAdvancedPermissions) {
		$(editor+'view'+permissions[0]).checked = true;
		$(editor+'edit'+permissions[1]).checked = true;
		$(editor+'reply'+permissions[2]).checked = true;
		$(editor+'approve'+permissions[3]).checked = true;
		$(editor+'moderate'+permissions[4]).checked = true;
		$(editor+'tag'+permissions[5]).checked = true;
	}

	index_manualpostpermissionsupdate = false; /* Allow respond to user updates */

	console.log('<?=__LINE__?> NEXTINLINE permissions for messager!');
	if (1) /* Force returning early */
		return false;

	/**
	 * Original source code follows here.
	 * Parts are copied from below to above as functionality is needed.
	 */
	var permissionimgsrc = '';
	var permissionstitle = '';
	var ratingimgsrc = '';
	var agerating = 3;
	var permissionmode = 0;
	var contentratingvalue = '';
	var repliespresetvalue = '';
	var viewmodepresetvalue = '';
	var viewmodevalue = '';
	var viewmodeimgsrc = '';
	var sortmodepresetvalue = '';
	var sortmodevalue = '';
	var sortmodeimgsrc = '';
	var sortreversepresetvalue = false;
	var sortreversevalue = false;

	if (editor == 'editpost') {
		/* Set default for required presets. Prevents JS halting on errors. */
		if (document.querySelector('input[name=editpostcontentrating]:checked') == null)
			$('editpostpresetgeneralcontent').checked = true;

		if (document.querySelector('input[name=editpostreplies]:checked') == null)
			$('editpostpresetacceptreplies').checked = true;

		if (document.querySelector('input[name=editpostviewmode]:checked') == null)
			$('editpostviewmodefull').checked = true;

		permissions = index_js_getEditorPermissions('editpost');
		permissionmode = document.querySelector('input[name=editpostpreset]:checked').value;
		contentratingvalue = document.querySelector('input[name=editpostcontentrating]:checked').value;
		repliespresetvalue = document.querySelector('input[name=editpostreplies]:checked').value;
		viewmodepresetvalue = document.querySelector('input[name=editpostviewmode]:checked').value;
		viewmodevalue = document.querySelector('input[name=editpostviewmode]:checked').value;
		sortmodevalue = document.querySelector('input[name=editpostsortmode]:checked').value;
		sortreversevalue = $('editpostsortreverse').checked;
	} else if (editor == 'messager') {
		/* Set default for required presets. Prevents JS halting on errors. */
		if (document.querySelector('input[name=messagercontentrating]:checked') == null)
			$('messagerpresetgeneralcontent').checked = true;

		if (document.querySelector('input[name=messagerreplies]:checked') == null)
			$('messagerpresetacceptreplies').checked = true;

		permissions = index_js_getEditorPermissions('messager');
		permissionmode = document.querySelector('input[name=messagerpreset]:checked').value;
		contentratingvalue = document.querySelector('input[name=messagercontentrating]:checked').value;
		repliespresetvalue = document.querySelector('input[name=messagerreplies]:checked').value;
	} else {
		return; /* Invalid editor! */
	}
	agerating = $(editor+'agerating').value;

	if ($(editor+'advancedpermissions').checked) {
		switch (permissions) {
		case '505505':
		case '504404':
		case '303303':
		case '202202':
		case '101101':
		case '000000':
			$(editor+'permissionsiconadvanced').style.display = 'none';
			break;
		default:
			$(editor+'permissionsiconadvanced').style.display = 'inline';
		}
	} else {
		$(editor+'permissionsiconadvanced').style.display = 'none';
		switch (permissionmode) {
		case '5':
			if ($(editor+'allowunsigned').checked) {
				permissions = '505505';
			} else {
				permissions = '504404';
			}
			break;
		case '4':
			permissions = '404404';
			break;
		case '3':
			permissions = '303303';
			break;
		case '2':
			permissions = '202202';
			break;
		case '1':
			permissions = '101101';
			break;
		default:
			permissions = '000000';
		}

		switch (contentratingvalue) {
		case 'general':
			ratingimgsrc = '/img/icon-permissions-generalcontent.png';
			agerating = 3;
			break;
		case 'mature15':
			ratingimgsrc = '/img/icon-permissions-mature15.png';
			agerating = 15;
			break;
		case 'adult18':
			ratingimgsrc = '/img/icon-permissions-adult18.png';
			agerating = 18;
			break;
		}

		permissionarray = Array.from(permissions);
		switch (repliespresetvalue) {
		case 'acceptreplies':
			repliesimgsrc = '/img/icon-post-approved.png';
			break;
		case 'repliesneedapproval':
			repliesimgsrc = '/img/icon-post-pending.png';
			permissionarray[3] = '0';
			permissions = permissionarray.join('');
			break;
		case 'disablereplies':
			repliesimgsrc = '/img/icon-post-blocked.png';
			permissionarray[2] = '0';
			permissionarray[3] = '0';
			permissions = permissionarray.join('');
			break;
		}

		switch (viewmodepresetvalue) {
		case 'thumbnail':
		case 'list':
			viewmodevalue = viewmodepresetvalue;
			break;
		default:
			viewmodevalue = 'full';
		}

		switch (sortmodepresetvalue) {
		case 'date':
		case 'title':
			sortmodevalue = sortmodepresetvalue;
			break;
		default:
			sortmodevalue = 'date';
		}

		sortreversevalue = sortreversepresetvalue;

		$(editor+'agerating').value = agerating;
		$(editor+'contentratingicon').src = ratingimgsrc;
		$(editor+'repliesicon').src = repliesimgsrc;
	}

	viewpermissions = permissions[0];
	switch (viewpermissions) {
	case '5':
		permissionimgsrc = '/img/icon-permissions-open.png';
		permissionstitle = 'Anyone';
		break;
	case '4':
		permissionimgsrc = '/img/icon-permissions-all.png';
		permissionstitle = 'All Users';
		break;
	case '3':
		permissionimgsrc = '/img/icon-permissions-members.png';
		permissionstitle = 'Members';
		break;
	case '2':
		permissionimgsrc = '/img/icon-permissions-contacts.png';
		permissionstitle = 'Contacts';
		break;
	case '1':
		permissionimgsrc = '/img/icon-permissions-recipient.png';
		permissionstitle = 'Recipients';
		break;
	case '0':
		permissionimgsrc = '/img/icon-permissions-owner.png';
		permissionstitle = 'Owner';
		break;
	}

	switch (viewmodevalue) {
	case 'thumbnail':
		viewmodeimgsrc = '/img/icon-viewmode-thumbnail.png';
		break;
	case 'list':
		viewmodeimgsrc = '/img/icon-viewmode-list.png';
		break;
	default:
		viewmodeimgsrc = '/img/icon-viewmode-full.png';
	}

	switch (sortmodevalue) {
	case 'title':
		sortmodeimgsrc = '/img/icon-sortmode-title.png';
		break;
	default:
		sortmodeimgsrc = '/img/icon-sortmode-date.png';
	}

	$(editor+'permissionsicon').src = permissionimgsrc;
	$(editor+'permissionstitle').innerText = permissionstitle;
	if (editor == 'editpost') {
		$(editor+'viewmodeicon').src = viewmodeimgsrc
		$(editor+'sortmodeicon').src = sortmodeimgsrc
	}
	index_js_setEditorPermissions(editor, permissions, agerating,
			viewmodevalue, sortmodevalue, sortreversevalue);
}

function index_js_switchAdvancedPermissions(editor)
{
	if ($(editor+'advancedpermissions').checked) {
		$(editor+'permissionspresets').style.display = 'none';
		$(editor+'permissionsadvanced').style.display = 'block';
		$(editor+'advancedpermissionstooltip').innerHTML = 'Click for permission presets.';
	} else {
		let permissions = index_js_getEditorPermissions(editor);

		/* Conditionally ask to switch from advanced permissions to presets. */
		switch (permissions) {
		case '505505': /* Public, Accept all including guest and emails */
		case '505000': /* Public, Replies need approval */
		case '504404': /* Public, All signed in users can reply */
		case '504000': /* Public, Sign in users can reply but need approval */
		case '500000': /* Public, Replies disabled */
		case '202202': /* Starred Contacts can view and reply */
		case '202000': /* Starred Contacts can view but replies need approval */
		case '200000': /* Starred Contacts can view but not reply */
		case '101101': /* Recipient can view and reply */
		case '101000': /* Recipient can view but replies need approval */
		case '100000': /* Recipient can reply but not reply */
		case '000000': /* Only owner can view */
			break;
		/* Fall through if not a preset. */
		default:
			if (!confirm("By switching to a preset you will reset this post's permissions.\n\nAre you sure you want to do this?")) {
				$(editor+'advancedpermissions').checked = true;
				return;
			}
		}

		index_manualpostpermissionsupdate = true;
		/* Set "Visibility" permission preset */
		switch (permissions[0]) {
		/* Public */
		case '5':
			$(editor+'preset5').checked = true;
			if (permissions[2] == '5') {
				$(editor+'allowunsigned').checked = true;
			} else {
				$(editor+'allowunsigned').checked = false;
			}
			break;

		/* Starred Contacts */
		case '2':
			$(editor+'preset2').checked = true;
			break;

		/* Recipients */
		case '1':
			$(editor+'preset1').checked = true;
			break;
		default:
			$(editor+'preset0').checked = true;
		}

		/* Set "Replies" permission preset. Note that reply permissions
		 * are ignored if view permission is set to owner (0) */
		if ((permissions[2] == '0') && (permissions[0] != '0')) {
			$(editor+'presetdisablereplies').checked = true;
		} else if ((permissions[3] == '0') && (permissions[0] != '0')) {
			$(editor+'presetrepliesneedapproval').checked = true;
		} else {
			$(editor+'presetacceptreplies').checked = true;
		}

		$(editor+'permissionspresets').style.display = 'block';
		$(editor+'permissionsadvanced').style.display = 'none';
		$(editor+'advancedpermissionstooltip').innerHTML = 'Click for advanced permissions.';
		index_manualpostpermissionsupdate = false;
	}
}

function index_js_popupEditorClose()
{
	index_js_removePostEditorEvents($('messagerinputentry'));
	$('messager').style.display = 'none';
	index_messagewindowarray[0] = {'username':false, 'postid':0}; /* TODO: Make this multi-window */
	index_messagerecipient = '';
	index_messagepostid = 0;
}

function index_js_popupEditorClick(ev)
{
	if ($('messager').contains(ev.target)) {
		return true;
	}

	/* COMMENTED postponed from bug.
	if (index_messagerclickeventlistener) {
		document.removeEventListener('click', index_js_popupEditorClick, false);
		index_messagerclickeventlistener = false;
	}
	*/

	index_js_popupEditorClose();
	return true;
}

function index_js_popupEditorAttach()
{
	$('messagerfile').click();
	$('messagerinputentry').focus();

	return false; /* Disable submit action */
}

/**
 * Ajax function for index_js_message()
 */
function index_js_message_AjaxFunction()
{
	if (xhrSendMessage.readyState != 4)
		return;

	var responsearray = index_js_parseResponseText(xhrSendMessage.responseText, "Could not send message.\n\nTry again or contact the website administrators.");
	if (!responsearray)
		return;

	if (responsearray['error'] != "INFO_MESSAGING_AVAILABLE") {
		console.log("<?=__LINE__?>: messages - What are we going to do with this?! - "+responsearray['error']);
		$('messagerbodyscroller').innerHTML = "Messaging currently unavailable.<br>Try again or contact the website administrators.";
		return;
	}
	var recipientfullname = responsearray['recipientfullname'];
	var recipientusername = responsearray['recipientusername'];
	var recipientemail = responsearray['recipientemail'];

	var messagertitle;
	if (recipientemail) {
		messagertitle = recipientfullname+'<br>'+recipientemail;
	} else {
		messagertitle = recipientfullname;
	}

	if (recipientusername) {
		$('messagerrecipient').value = recipientusername;
		$('messagerpostid').value = 0;
		index_editparentid = 0;
		index_messagerecipient = recipientusername;
	} else {
		$('messagerrecipient').value = recipientemail;
		index_messagerecipient = recipientemail;
		recipientusername = responsearray['message'][0]['id'];
		index_messagepostid = recipientusername;
		/* messagertitle = "<i><?=__LINE__?> Unknown User</i>"; */
		$('messagerpostid').value = recipientusername;
		index_editparentid = recipientusername;
	}
	$('messagertitle').innerHTML = messagertitle;

	var m, mct = 0;

	if ((typeof responsearray['message'] !== 'undefined')
			&& (responsearray['message'].length > 0)) {
		mct = responsearray['message'].length;
	}

	index_js_emptyElementById('messagerbodyscroller');
	index_messageprevioususername = '';
	for (var m = 0; m < mct; m++) {
		var id = responsearray['message'][m]['id'];
		var username = responsearray['message'][m]['username'];
		var fullname = responsearray['message'][m]['fullname'];
		var description = responsearray['message'][m]['description'];
		var messagepopuplist = responsearray['message'][m]['postpopuplist'];
		var messagepermissions = responsearray['message'][m]['permissions'];
		var messagerecipients = responsearray['message'][m]['recipients'];
		var messageattachments = responsearray['message'][m]['messageattachments'];
		let messagecredits = responsearray['message'][m]['messagecredits'];
		let messagedescription = responsearray['message'][m]['messagedescription'];

		let classname = "messageentry";
		if (username == "<?=$index_activeusername?>") {
			classname += " messageentrysent";
		} else if (!username) {
			username = id; /* Use post id as username for identifying guest posts. */

			var emaildisplayname = responsearray['message'][m]['emaildisplayname'];
			var emailusername = responsearray['message'][m]['emailusername'];
			var emailserver = responsearray['message'][m]['emailserver'];
			if (emaildisplayname) {
				fullname = emaildisplayname;
			} else {
				fullname = "<i>User Unknown</i>";
			}
		}

		/* Add the message div. */
		var messagediv = document.createElement("div");
		messagediv.id = "message_"+id;
		messagediv.className = classname;
		messagediv.setAttribute('postpermissions', messagepermissions);
		messagediv.setAttribute('postrecipients', messagerecipients);
		messagediv.setAttribute('postprevioususername', index_messageprevioususername.toLowerCase());

		let messagecreditsdiv = document.createElement('div');
		messagecreditsdiv.className = "messagecredits";
		messagecreditsdiv.innerHTML = messagecredits;
		messagediv.appendChild(messagecreditsdiv);

		let messagemenudiv = document.createElement('div');
		messagemenudiv.id = 'messagemenudiv_'+id;
		messagemenudiv.innerHTML = messagepopuplist;
		messagecreditsdiv.appendChild(messagemenudiv);

		var messagedescriptiondiv = document.createElement('div');
		messagedescriptiondiv.id = 'messagedescription_'+id;
		messagedescriptiondiv.className = 'messagedescription';
		messagedescriptiondiv.innerHTML = messagedescription;
		messagediv.appendChild(messagedescriptiondiv);

		/* Add attachments to the div, if any. */
		if ((typeof messageattachments !== 'undefined')
				&& (messageattachments.length)) {
			mact = messageattachments.length;
			for (var ma = 0; ma < mact; ma++) {
				 var attachmentdiv = document.createElement('div');
				 attachmentdiv.id = messageattachments[ma]['id'];
				 attachmentdiv.className = 'messageattachment';
				 attachmentdiv.innerHTML = messageattachments[ma]['innerHTML'];

				 messagediv.appendChild(attachmentdiv);
			}
		}

		$('messagerbodyscroller').appendChild(messagediv);
		$('messagerbodyscroller').scrollTop = $('messagerbodyscroller').scrollHeight - $('messagerbodyscroller').clientHeight;
	}

	$('messagersendonenterkey').checked = false;
	if (responsearray['messagesendonenterkey'] == "1")
		$('messagersendonenterkey').checked = true;

	$('messagercloseonsend').checked = false;
	if (responsearray['messagecloseonsend'] == "1")
		$('messagercloseonsend').checked = true;

	index_messagerscrollheight = $('messagerbodyscroller').scrollHeight;
	index_messagerscrolltop = index_messagerscrollheight;
	$('messagerinputentry').focus();
	if ($('contactlistentry_'+recipientusername+'_statusimg')) {
		/* TODO: Need update to reflect online status. */
		$('contactlistentry_'+recipientusername+'_statusimg').src = responsearray['statusimgsrc'];
	}
	/* COMMENTED postponed from bug.
	if (!index_messagerclickeventlistener) {
		document.addEventListener('click', index_js_popupEditorClick, false);
		index_messagerclickeventlistener = true;
	} */

	index_js_getUpdates(); /* Update message count, etc. on sidepanel header. */
}

/**
 * Set up a popup window for fast post replying. At least one parameter needs to be set:
 * recipientusername: can be just username or username@server.com.
 * postid: used for fast post replies, or replying to guest posts.
 */
function index_js_message(recipientusername, postid)
{
	/* Reset editor controls, if not already. Causes a bug where the controls disappear.
	 * This also checks if a message in the popup is being edited. */
	if ($('messagerbodyscroller').contains($('editpostcontrols'))) {
		if (!index_js_cancelEditPost(false)) {
			return false;
		}
	}

	index_messagewindowarray[0] = {'username':recipientusername, 'postid':postid};
	/* Set up the popup editor for messaging. */
	var messagerpermissions = "101101"; /* TODO: Get permissions from.... where? editordiv.parentNode.getAttribute('postpermissions'); */
	/*
	if (!recipientusername && (postid > 0))
		messagerpermissions = "555555";
		// */

	/* TODO - Commented until feature complete.
	index_js_setEditorPermissions('messager', messagerpermissions, 3,
			'full', 'date', false);
	index_js_updatePostPermissions('messager');
	$('messagerrecipients').value = recipientusername;
	// */
	$('messagerbodyscroller').innerHTML = '<span style="padding:4px;color:#393;">Loading messages...</span>';
	$('messagertitle').innerHTML = 'Loading ('+recipientusername+')';

	/* Center and display the editor. */
	if ($('messager').style.display != 'grid') {
		$('messager').style.display = 'grid';
		index_js_addPostEditorEvents($('messagerinputentry'));
		var x = ((window.innerWidth - $('rightpanel').offsetWidth) - $('messager').offsetWidth) / 2;
		let headeroffsetheight = $('mirrorisland-header').offsetHeight;
		let windowinnerheight = window.innerHeight;
		let messageroffsetheight = $('messager').offsetHeight;
		var y = headeroffsetheight + (((windowinnerheight - headeroffsetheight) - messageroffsetheight) / 2);
		var width = '350px';
		if (x < 0) {
			x = 0;
		}
		$('messager').style.left = x + 'px';
		$('messager').style.top = y + 'px';
		$('messager').style.height = '380px';
	}

	/* Fetch latest messages from the recipient (if any). */
	index_js_emptyElementById('messagerinputentry');
	index_js_updateMessage();
	$('messagerinputentry').addEventListener('input', index_js_updateMessage, false);
	index_js_makeDraggable('messagertitlebar', 'messager', 'move');
	index_js_makeDraggable('messagerresizenw', 'messager', 'move'); // was 'nw' but changed to 'move' for simplicity.
	index_js_makeDraggable('messagerresizew', 'messager', 'w');
	index_js_makeDraggable('messagerresizee', 'messager', 'e');
	index_js_makeDraggable('messagerresizesw', 'messager', 'sw');
	index_js_makeDraggable('messagerresizes', 'messager', 's');
	index_js_makeDraggable('messagerresizese', 'messager', 'se');

	var messageformdata = new FormData();
	messageformdata.append("ajax_command", "get_messages");
	messageformdata.append("contactusername", recipientusername);
	messageformdata.append("postid", postid);

	xhrSendMessage = new XMLHttpRequest();
	xhrSendMessage.onreadystatechange = index_js_message_AjaxFunction;
	xhrSendMessage.open("POST", "/index.php", true);
	xhrSendMessage.send(messageformdata);

	return false;
}

function index_js_updateMessage()
{
	var bodyheight = $('messager').clientHeight - ($('messagertitle').clientHeight + $('messagerform').clientHeight + $('messagerinput').clientHeight);
	var bottomdistance = $('messagerbodyscroller').scrollHeight - ($('messagerbodyscroller').scrollTop + $('messagerbodyscroller').clientHeight);
	//$('messagerbodyscroller').style.height = bodyheight+'px';
	if (bottomdistance == 0) {
		$('messagerbodyscroller').scrollTop = $('messagerbodyscroller').scrollHeight - $('messagerbodyscroller').clientHeight;
	}
}

function index_js_startMessage()
{
	if (index_editmessageid == "NEWMESSAGE")
		return;

	index_editmessageid = "NEWMESSAGE";
}

function index_js_getUpdates_AjaxFunction()
{
	if (xhrGetUpdates.readyState != 4)
		return;

	var responsearray = index_js_parseResponseText(xhrGetUpdates.responseText, false);
	if (!responsearray)
		return;

	console.log("<?=__LINE__?>: responsearray['error']="+responsearray['error']);
	if (responsearray['error'] != "INFO_UPDATES_AVAILABLE") {
		console.log("<?=__LINE__?>: getUpdates - What are we going to do with this?! - "+responsearray['error']);
		return;
	}

	var unreadmessagecount = responsearray['unreadcount'];
	$('sidepanelunreadmessagecount').innerHTML = unreadmessagecount;
	var onlineusercount = responsearray['onlineusercount'];
	$('sidepanelonlineusercount').innerHTML = onlineusercount;

	/* Get lengths of arrays. */
	var contactlength = 0, unreadlength = 0, messagelength = 0, grouplength = 0;

	if ((typeof responsearray['contact'] !== 'undefined')
			&& (responsearray['contact'].length)) {
		contactlength = responsearray['contact'].length;
	}

	if ((typeof responsearray['unread'] !== 'undefined')
			&& (responsearray['unread'].length)) {
		unreadlength = responsearray['unread'].length;
	}

	if ((typeof responsearray['message'] !== 'undefined')
			&& (responsearray['message'].length)) {
		messagelength = responsearray['message'].length;
	}

	if ((typeof responsearray['group'] !== 'undefined')
			&& (responsearray['group'].length)) {
		grouplength = responsearray['group'].length;
	}

	/* Display any contacts. */
	for (var c = 0; c < contactlength; c++) {
		var contactusername = responsearray['contact'][c]['username'];

		if (index_js_getContactIndex(contactusername) == -1) {
			var arrlen = index_contactlistarray.push({'username':responsearray['contact'][c]['username'],
					'displayname':responsearray['contact'][c]['contactlistentry'], 'trusted':responsearray['contact'][c]['trusted']});
			if (index_contactlistarray[arrlen-1]['trusted'] == 1) {
				$('contactslist').insertAdjacentHTML('beforeend',
						index_contactlistarray[arrlen-1]['displayname']);
			} else {
				$('otherslist').insertAdjacentHTML('beforeend',
						index_contactlistarray[arrlen-1]['displayname']);
			}
		}
	}

	/* Display any groups. */
	for (var g = 0; g < grouplength; g++) {
		var grouppostid = responsearray['group'][g]['id'];

		if (index_js_getGroupIndex(grouppostid) == -1) {
			var arrlen = index_grouplistarray.push({'pageid':responsearray['group'][g]['id'],
					'grouplistentry':responsearray['group'][g]['grouplistentry'], 'trusted':responsearray['group'][g]['trusted']});
			if (index_grouplistarray[arrlen-1]['trusted'] == 1) {
				$('grouplist').insertAdjacentHTML('beforeend', index_grouplistarray[arrlen-1]['grouplistentry']);
			} else {
				$('grouplistpending').insertAdjacentHTML('beforeend', index_grouplistarray[arrlen-1]['grouplistentry']);
			}
		}
	}

	/* Mark any contacts with unread messages. */
	for (u = 0; u < unreadlength; u++) {
		var messageid = responsearray['unread'][u]['id'];
		var messagesender = responsearray['unread'][u]['sender'];

		/* Add non-trusted contact. */
		if (index_js_getContactIndex(messagesender) == -1) {
			var displayname = messagesender; /* was responsearray['unread'][u]['username']; */
			index_contactlistarray.push({'username':messagesender, 'displayname':displayname, 'trusted':0});
			$('messagelist').insertAdjacentHTML('beforeend', responsearray['unread'][u]['contactlistentry']);
		}

		/* Flag contact list entries that have messaged. */
		for (var i = 0, ct = index_contactlistarray.length; i < ct; i++) {
			var unreadcount = Math.floor(responsearray['unread'][u]['unreadcount']);

			if (unreadcount) {
				/* Add messages to any open instant message windows */
				if (index_messagewindowarray[0]['username'] == messagesender) {
					let newMessage = document.createElement('div');
					$('messagerbodyscroller').appendChild(newMessage);
				} else {
					$('contactlistentry_'+messagesender+'_statusimg').src = "/img/status-unread12.png";
				}
				break;
			}
		}
	}
}

function index_js_getUpdates()
{
	var getupdatesformdata = new FormData();
	getupdatesformdata.append("ajax_command", "get_updates");

	xhrGetUpdates = new XMLHttpRequest();
	xhrGetUpdates.onreadystatechange = index_js_getUpdates_AjaxFunction;
	xhrGetUpdates.open("POST", "/index.php", true);
	xhrGetUpdates.send(getupdatesformdata);

	return false;
}

/**
 *
 */
function index_js_messagerScrollDown()
{
	$('messagerbodyscroller').scrollTop = $('messagerbodyscroller').scrollHeight - $('messagerbodyscroller').clientHeight;
	index_js_deleteElementById('messageroverlayscrolldown');
}

/**
 * Show an incoming message, if a popup window from the sender is open.
 */
function index_js_showMessage(id)
{
	let uri = `/${id}/message`;

	fetch(uri)
	.then(response => {
		return response.json();
	})
	.then(data => {
		/* Preserve measurements before adding new node */
		let scrollTop = $('messagerbodyscroller').scrollTop;
		let clientHeight = $('messagerbodyscroller').clientHeight;
		let scrollHeight = $('messagerbodyscroller').scrollHeight;

		$('messagerbodyscroller').insertAdjacentHTML('beforeend', data.message);

		/* Only scroll to view if within 40 pixels */
		if ((scrollTop + clientHeight) > (scrollHeight - 40)) {
			$('messagerbodyscroller').scrollTop = $('messagerbodyscroller').scrollHeight - $('messagerbodyscroller').clientHeight;
		} else {
			let button = document.createElement('button');
			button.id = 'messageroverlayscrolldown';
			button.innerText = '▼ View new messages ▼';
			button.onclick = index_js_messagerScrollDown;
			$('messageroverlay').appendChild(button);
		}
	})
	.catch(error => console.warn(error));

	return false;
}

/**
 * Make an HTML element draggable.
 * - headerid: ID of HTML element to start the drag.
 * - contentid: ID of HTML element to be dragged. Can be the same as headerid.
 * - type: string; action to be done while dragging:
 *       - 'move': moves the content div without resizing it.
 *       - 'e', 'n', 's', 'w': resize content div from an edge.
 *       - 'nw', 'ne', 'sw', 'se': resize content div from a corner.
 */
function index_js_makeDraggable(headerid, contentid, type)
{
	let mouseStartX = 0, mouseStartY = 0;
	let elStartX = 0, elStartY = 0;
	let elStartWd = 0, elStartHt = 0;
	let el = $(contentid);

	$(headerid).onmousedown = dragMouseDown;

	function dragMouseDown(e) {
		e = e || window.event;
		e.preventDefault();

		mouseStartX = e.clientX;
		mouseStartY = e.clientY;
		elStartX = el.offsetLeft;
		elStartY = el.offsetTop;
		elStartWd = el.offsetWidth;
		elStartHt = el.offsetHeight;

		document.onmousemove = dragMove;
		document.onmouseup = dragEnd;
		window.onresize = windowResize;
	}

	function dragMove(e) {
		e = e || window.event;
		e.preventDefault();

		let mouseDiffX = e.clientX - mouseStartX;
		let mouseDiffY = e.clientY - mouseStartY;
		let x = elStartX, y = elStartY;
		let wd = elStartWd, ht = elStartHt;
		let maxright = window.innerWidth;
		if (index_sidepanelvisible) maxright -= 320;

		/* Move left edge WITH mouse */
		if ((type == 'move') || (type == 'nw') || (type == 'w') || (type == 'sw'))
			x += mouseDiffX;

		/* Move top edge WITH mouse */
		if ((type == 'move') || (type == 'nw'))
			y += mouseDiffY;

		/* Left/west horizontal size */
		if ((type == 'nw') || (type == 'w') || (type == 'sw')) {
			wd -= mouseDiffX;

			/* Keep resize within visible bounds */
			if (x < 0)
				wd += x;
		}

		/* Right/east horizontal size */
		if ((type == 'e') || (type == 'se')) {
			wd += mouseDiffX;

			/* Keep resize within visible bounds */
			if (x + wd > maxright)
				wd -= (x + wd) - maxright;
		}

		/* Top/north vertical size (NOTE: 'n' and 'ne' aren't used yet) */
		if ((type == 'nw') || (type == 'n') || (type == 'ne'))
			ht -= mouseDiffY;

		/* Lower/south vertical size */
		if ((type == 'sw') || (type == 's') || (type == 'se'))
			ht += mouseDiffY;

		/* Fix display positions to keep dragged element visible */
		if (x < 0) {
			x = 0;
		} else if (x + el.offsetWidth > maxright) {
			x = maxright - el.offsetWidth;
		}

		if (y < 40)
			y = 40;
		else if (y + el.offsetHeight > window.innerHeight)
			y = window.innerHeight - el.offsetHeight;

		/* Move the element */
		el.style.left = x + 'px';
		el.style.top = y + 'px';
		el.style.width = wd + 'px';
		el.style.height = ht + 'px';
	}

	function dragEnd() {
		document.onmouseup = null;
		document.onmousemove = null;
	}

	function windowResize() {
		const rect = el.getBoundingClientRect();

		if (rect.right > window.innerWidth)
			el.style.left = (rect.left - (rect.right - window.innerWidth)) + "px";

		if (rect.bottom > window.innerHeight)
			el.style.top = (rect.top - (rect.bottom - window.innerHeight)) + "px";
	}
}

function index_js_setupSearch(ev)
{
	$('searchpopup').style.display = 'block';
	$('searchtext').addEventListener('input', index_js_searchTextInput, false);
	$('searchorder').addEventListener('change', index_js_searchTextInput, false);
}

function index_js_searchText_AjaxFunction()
{
	/* TODO: Sanitise input from the server. Blindly accepting the search results is NOT GOOD. */
	if (xhrSearchText.readyState != 4)
		return;

	index_searchtext = false;
	clearTimeout(index_searchtexttimer);
	index_searchtexttimer = 0;
	var responsearray = index_js_parseResponseText(xhrSearchText.responseText, false);
	if (!responsearray) {
		$('searchlist').innerHTML = '<div class="searchlistpost">Search not currently available. Try again later.</div>';
		index_js_emptyElementById('searchstatus');
		return;
	}

	if (responsearray['error'] == "INFO_NO_SEARCH_QUERY") {
		$('searchlist').innerHTML = '<div class="searchlistpost">Enter a search query to start.</div>';
		index_js_emptyElementById('searchstatus');
		return;
	}

	$('searchstatus').innerHTML = responsearray['searchstatus'];
	index_searchpage = responsearray['searchpage'];
	index_searchpagect = responsearray['searchpagect'];

	if (responsearray['error'] == "INFO_NO_SEARCH_RESULTS") {
		$('searchlist').innerHTML = '<div>No search results found.</div>';
		return;
	}

	if (responsearray['error'] != "INFO_SEARCH_SUCCESSFUL") {
		console.log("<?=__LINE__?>: What are we going to do with this?! - "+responsearray['error']);
		return;
	}

	var s, sct = 0;
	index_js_emptyElementById('searchlist');

	if ((typeof responsearray['search'] === 'undefined')
			|| (responsearray['search'].length <= 0)) {
		$('searchlist').insertAdjacentHTML('beforeend', '<div>No search results found.</div>');
		return; /* Nothing more to do. */
	}

	sct = responsearray['search'].length;
	for (var s = 0; s < sct; s++) {
		var id = responsearray['search'][s]['tableid'];
		var searchcache = responsearray['search'][s]['searchcache'];
		var searchdisplay = responsearray['search'][s]['searchdisplay'];
		var searchicon = '/img/icon-unknown.png';
		var searchlink = responsearray['search'][s]['searchlink'];
		switch (responsearray['search'][s]['tablename']) {
		case 'posts':
			searchicon = '/img/largeicon-post.png';
			break;
		case 'accounts':
			searchicon = '/img/largeicon-profile.png';
			break;
		case 'files':
			searchicon = '/img/largeicon-file.png';
			break;
		}
		$('searchlist').insertAdjacentHTML('beforeend', '<div><a href="'+searchlink+'"><img class="searchicon" src="<?=$urlhome?>/'+searchicon+'">'+searchdisplay+'</a></div>');
	}
}

function index_js_searchText(ev)
{
	if (index_searchtext) {
		return false; /* Search in progress... or not for errors. */
	}

	if (index_searchbtn == 1) { /* Next page button */
		index_searchpage++;
		index_searchbtn = 0;
		if (index_searchpage >= index_searchpagect) {
			index_searchpage = index_searchpagect -1;
			return false; /* Don't search; already at end. */
		}
	} else if (index_searchbtn == -1) { /* Prev page button */
		index_searchpage--;
		index_searchbtn = 0;
		if (index_searchpage < 0) {
			index_searchpage = 0;
			return false; /* Don't search; already at start. */
		}
	}

	var searchtextformdata = new FormData($('searchform'));
	searchtextformdata.append("searchpage", index_searchpage);
	searchtextformdata.append("ajax_command", "search_text");

	xhrSearchText = new XMLHttpRequest();
	xhrSearchText.onreadystatechange = index_js_searchText_AjaxFunction;
	xhrSearchText.open("POST", "/index.php", true);
	xhrSearchText.send(searchtextformdata);

	index_searchtext = true;
	index_searchbtn = 0;

	return false;
}

function index_js_searchTextInput(ev)
{
	/* Update timer on keypresses. */
	if (index_searchtexttimer) {
		clearTimeout(index_searchtexttimer);
		index_searchtexttimer = 0;
		$('searchstatus').innerHTML = 'Searching... <img src="/img/thinking12.gif" width="12" height="12">';
	}
	index_searchpage = 0; /* Reset to first page when search text changes. */
	index_searchtexttimer = setTimeout(function(){index_js_searchText();}, 1000);
	return;
}

/**
 * Various function for templates, including uploading a new index file,
 * and searching currently selected index files.
 */

function index_js_manageTemplateIndex_ajaxFunction(result)
{
	if (result.error == 'INFO_TEMPLATE_INDEX_DELETED') {
		let fileid = result['fileid'];
		index_js_deleteElementById('templateindex_'+result.fileid);
	}
}

function index_js_deleteTemplateIndex(ev)
{
	let el = $(ev.target.id);
	let fileid = el.getAttribute('templatefileid');
	let filename = el.getAttribute('templatefilename');
	index_js_manageTemplateIndex(fileid, filename, false, true);
}

function index_js_manageTemplateIndex(fileid, filename, checked, deleteindex)
{
	var deleteflag = 'false';
	var managemode = 'useindex';
	if (deleteindex) {
		if (!confirm("Delete template index? This can not be undone.")) {
			return false;
		}

		var i, idxlen = index_templateindexsource.length;
		for (i = 0; i < idxlen; i++) {
			if (fileid == index_templateindexsource[i]['fileid']) {
				index_templateindexsource[i]['useindex'] = false;
			}
		}
		$('templatedeletelink_'+fileid).innerText = 'Deleting...';
		$('templatedeletelink_'+fileid).onclick = ''; /* Works better than removeAttribute */
		index_js_rebuildTemplateIndexArray();
		index_js_templateSetSearchTimer();

		managemode = 'deleteindex';
		deleteflag = 'true';
	}

	var useindexflag = 'false';
	if (checked) {
		useindexflag = 'true';
	}

	let fd = new FormData();
	fd.append('ajax_command', 'manage_template_index');
	fd.append('managemode', managemode);
	fd.append('fileid', fileid);
	fd.append('filename', filename);
	fd.append('useindexflag', useindexflag);
	fd.append('deleteflag', deleteflag);

	fetch('/index.php', {
		method:'post',
		body:fd
	})
	.then((response) => {
		return response.json();
	})
	.then(index_js_manageTemplateIndex_ajaxFunction)
	.catch(error => console.warn(error));
}

function index_js_uploadTemplateIndex_ajaxFunction()
{
	if (xhrUploadTemplateIndex.readyState != 4)
		return;

	var responsearray = index_js_parseResponseText(xhrUploadTemplateIndex.responseText, false);
	if (!responsearray)
		return;

	if (responsearray['error'] != "INFO_UPLOAD_TEMPLATE_SUCCESS") {
		alert('Invalid template index format.');
		return;
	}
	index_templateindexloaded = false;
	index_js_setupTemplateSearch(false, 0, 0, 0);
}

function index_js_uploadTemplateIndex()
{
	var uploadtemplateindexformdata = new FormData();
	var files = $('templatesearchfile').files;
	uploadtemplateindexformdata.append("templatesearchfile", files[0]);
	uploadtemplateindexformdata.append('ajax_command', 'upload_template_index');

	xhrUploadTemplateIndex = new XMLHttpRequest();
	xhrUploadTemplateIndex.onreadystatechange = index_js_uploadTemplateIndex_ajaxFunction;
	xhrUploadTemplateIndex.open("POST", "/index.php", true);
	xhrUploadTemplateIndex.send(uploadtemplateindexformdata);
}

/**
 * called by index_js_setupTemplateSearch_loadIndex below.
 */
function index_js_rebuildTemplateIndexArray()
{
	index_templateindexarray.length = 0;
	var i, idxlen = index_templateindexsource.length;
	for (i = 0; i < idxlen; i++) {
		if (index_templateindexsource[i]['useindex']) {
			var data = index_templateindexsource[i]['data'];
			index_templateindexarray = index_templateindexarray.concat(data);
		}
	}
}

/**
 * called by index_js_setupTemplateSearch_result below.
 */
function index_js_setupTemplateSearch_loadIndex(filename, fileid, uri, useindex)
{
	index_templateindexloading++;
	fetch(uri)
	.then(response => {
		return response.json();
	})
	.then(data => {
		index_templateindexsource.push({'fileid':fileid, 'filename':filename, 'useindex':useindex, 'data':data});
		index_templateindexloading--;
		if (index_templateindexloading == 0) {
			index_templateindexloaded = true;
			index_js_rebuildTemplateIndexArray();
			index_js_templateSetSearchTimer();
		}
	})
	.catch(error => console.warn(error));
}

function index_js_managePersonalTemplates(checked)
{
	index_js_manageTemplateIndex(0, 0, checked, false);
}

function index_js_templatecheckboxchanged(ev)
{
	/* "hardcoded" HTML checkboxes don't need the "target" for "event"... */
	var evid = ev.id;
	var evchecked = ev.checked;
	/* ...while JS created checkbox elements return NULL, and will
	 * have to be fetched with the "target" class instead. */
	if (!evid) {
		evid = ev.target.id;
		evchecked = ev.target.checked;
	}
	/* Personal templates need to be managed differently. */
	if (evid == 'templatecheckbox_NEWPOST') {
		index_js_managePersonalTemplates(evchecked);
		return false;
	}

	var i, idxlen = index_templateindexsource.length;
	for (i = 0; i < idxlen; i++) {
		var fileid = index_templateindexsource[i]['fileid'];
		var filename = index_templateindexsource[i]['filename'];
		var checkboxid = 'templatecheckbox_'+fileid;
		if (evid == checkboxid) {
			index_templateindexsource[i]['useindex'] = evchecked;
			index_js_manageTemplateIndex(fileid, filename, evchecked, false);
		}
	}

	index_js_rebuildTemplateIndexArray();
	index_js_templateSetSearchTimer();
}

/**
 * called by index_js_setupTemplateSearch below.
 */
function index_js_setupTemplateSearch_result(arr)
{
	index_templateindexsource.length = 0;
	var t = 0;
	var tct = arr.length;
	for (t = 0; t < tct; t++) {
		var filename = arr[t]['filename'];
		var fileid = arr[t]['indexfileid'];
		var useindex = arr[t]['useindex'];

		var uri = '/index.php?fileid='+fileid+'&filename='+filename;
		index_js_setupTemplateSearch_loadIndex(filename, fileid, uri, useindex);
		var divid = 'templateindex_'+fileid;
		if (!$(divid)) {
			var div = document.createElement('div');
			div.id = divid;
			div.className = 'templatesearchsource';

			var checkbox = document.createElement('input');
			checkbox.type = 'checkbox';
			checkbox.id = 'templatecheckbox_'+fileid;
			checkbox.name = 'templatecheckbox_'+fileid;
			if (useindex) {
				checkbox.checked = 'true';
			}
			checkbox.onchange = index_js_templatecheckboxchanged;
			div.appendChild(checkbox);

			var label = document.createElement('label');
			label.innerHTML = '&nbsp;'+filename+' ';
			label.setAttribute('for', 'templatecheckbox_'+fileid);
			div.appendChild(label);

			var downloadlink = document.createElement('a');
			downloadlink.className = 'postfiledownload';
			downloadlink.innerText = 'Download';
			downloadlink.href = uri;
			div.appendChild(downloadlink);

			div.appendChild(document.createTextNode(' '));

			var deletelink = document.createElement('a');
			deletelink.id = 'templatedeletelink_'+fileid;
			deletelink.className = 'postfiledelete';
			deletelink.innerText = 'Delete';
			deletelink.setAttribute('templatefileid', fileid);
			deletelink.setAttribute('templatefilename', filename);
			deletelink.onclick = index_js_deleteTemplateIndex;
			div.appendChild(deletelink);

			$('templatesearchindexes').appendChild(div);
		}
	}
	index_templateindexloaded = true;
}

/**
 * if ev is set - parentid, permissions, and recipients are set,
 * otherwise those other values are ignored if ev is false.
 * This is so this function can be re-called to refresh the template
 * indexes without having to set any post-editing variables.
 */
function index_js_setupTemplateSearch(ev, parentid, permissions, recipients)
{
	if (typeof ev !== 'undefined') {
		index_templateparentid = parentid;
		index_templatepermissions = permissions;
		index_templaterecipients = recipients;
	}

	index_templatepopupvisible = true;
	$('templatesearchpopup').style.display = 'flex';
	$('templatesearchtext').addEventListener('input',
			index_js_templateSetSearchTimer, false);
	index_templateplaceholder = $('templatesearchtext').getAttribute('placeholder');
	$('templatesearchtext').setAttribute('placeholder',
			index_templateplaceholder);

	if (!index_templateindexloaded) {
		let fd = new FormData();
		fd.append('ajax_command', 'get_template_indexes');

		fetch('/index.php', {
			method: 'post', body: fd
		})
		.then((response) => {
			return response.json();
		})
		.then(result => {
			index_js_setupTemplateSearch_result(result.arr);
		})
		.catch(error => console.warn(error));
	}

	return false;
}


/**
 * Takes a single string of space-separated words in needle
 * and searches haystack.
 * Returns hilited haystack string with <b></b> as hilite boundary.
 */
function index_js_hiliteMatchingWords(needles, haystack)
{
	let textKeywords = needles.split(' ', 10);

	let startEndList = [];
	let hsl = haystack.length;
	let lcHaystack = haystack.toLowerCase();

	/* Get all matches for the string. */
	for (var ct = 0; ct < textKeywords.length; ct++) {
		var keyword = textKeywords[ct].toLowerCase();
		var kwl = keyword.length;
		for (var i = 0; i < hsl; i += kwl) {
			let npos = lcHaystack.indexOf(keyword, i);
			if (npos == -1) {
				/* No keyword found. Force exit. */
				i = hsl;
			} else {
				/* Check for pre-existing overlapping zones. */
				let kwstart = npos;
				let kwend = npos + kwl;
				for (var selen = startEndList.length-1; selen > -1; selen--) {
					lstart = startEndList[selen]['start'];
					lend = startEndList[selen]['end'];

					if ((kwstart == lstart) && (kwend == lend)) {
						startEndList.splice(selen, 1); /* Remove existing. */
					} else if ((kwstart >= lstart) && (kwend <= lend)) {
						/* Extend hilite both left and right. */
						kwstart = lstart;
						kwend = lend;
						startEndList.splice(selen, 1); /* Remove existing. */
					} else if ((lstart <= kwstart) && (kwstart <= lend)
							&& (lend < kwend)) {
						kwstart = lstart; /* Extend hilite left only. */
						startEndList.splice(selen, 1); /* Remove existing. */
					} else if ((kwstart < lstart) && (lstart <= kwend)
							&& (kwend <= lend)) {
						kwend = lend; /* Extend hilite right only. */
						startEndList.splice(selen, 1); /* Remove existing. */
					}
				}

				/* Add hilite zone. */
				startEndList.push({'start':kwstart, 'end':kwend});
				i = kwend; /* Continue from end of hilite. */
			}
		}
	}

	/* Arrange hilite sections from last to first. */
	startEndList = startEndList.sort(function(a,b) {
		return a['start'] - b['start'];
	});

	/* Now generate hilited text. */
	var str = haystack;
	for (var ct = startEndList.length-1; ct > -1; ct--) {
		var end = startEndList[ct]['end'];
		str = str.slice(0, end)+'</b>'+str.slice(end);
		var start = startEndList[ct]['start'];
		str = str.slice(0, start)+'<b>'+str.slice(start);
	}
	return str;
}

function index_js_templateSelected(ev, uri)
{
	index_templatepopupvisible = false;
	$('templatesearchpopup').style.display = 'none';
	$('templatesearchtext').value = '';
	$('templatesearchtext').setAttribute('placeholder',
			index_templateplaceholder);

	index_js_setupPostEditor(ev, 'postblock_NEWPOST', 'post', index_templateparentid, false,
			index_templatepermissions, 3, index_templaterecipients, '<?=$index_page?>', uri,
			'<?=$index_viewmode?>', '', false);
}

/**
 *
 */
function index_js_templateSearchText()
{
	index_js_emptyElementById('templatesearchresults');
	let text = $('templatesearchtext').value.trim();
	text = text.toLowerCase();
	text = text.replace(/ +/g, ' '); /* Shrink multiple spaces. */
	if (!text || (text.length < 3)) {
		let entry = 'Enter keyword(s) to search.';
		$('templatesearchresults').innerText = entry;
		return false;
	}

	/* Sort keywords in length, with longest first. */
	var textKeywords = text.split(' ', 10);
	textKeywords = textKeywords.sort(function(a,b) {
		return b.length - a.length;
	});

	/* If no keywords have length >= 3,
	 * crop any keywords less than 3 characters long. */
	for (var maxlen = 0, i = 0; i < textKeywords.length; i++) {
		var len = textKeywords[i].length;
		if (len > maxlen) {
			maxlen = len;
		}
		if (((maxlen < 3) && (len < 3))
				|| (len === 0)) {
			textKeywords.splice(i, 1);
			i--;
		}
	}

	var found = 0;
	var flen = index_templateindexarray.length;
	var tklen = textKeywords.length;
	for (var f = 0; f < flen && found < 30; f++) {
		var title = index_templateindexarray[f]['title'];
		var lctitle = title.toLowerCase();

		var matches = 0;
		for (var i = 0; i < tklen; i++) {
			tkw = textKeywords[i];
			if (lctitle.includes(tkw)) {
				matches++;
			}
		}

		/* Add if all matches. */
		if (matches == tklen) {
			found++;
			let hilitedtext = index_js_hiliteMatchingWords(text, title);
			let subtitle = index_templateindexarray[f]['subtitle'];
			let imgsrc = index_templateindexarray[f]['imgsrc'];
			let uri = index_templateindexarray[f]['uri'];
			let entry = '<div class="templatesearchresult">'
				+'<a class="templatesearchresultlink"'
				+' onclick="index_js_templateSelected(this, \''+uri+'\');">'
				+hilitedtext+' ('+subtitle+')'
				+'<img width="177" height="254" src="'+imgsrc+'">'
				+'</a>'
				+'</div>';
			$('templatesearchresults').insertAdjacentHTML('beforeend', entry);
		}
	}

	index_templatesearchfound = found;
	if (found == 0) {
		let entry = '<div><b>No matches found.</b></div>';
		$('templatesearchresults').insertAdjacentHTML('beforeend', entry);
	}
}

function index_js_templateSetSearchTimer()
{
	if (index_templatesearchfound == 0) {
		let text = $('templatesearchtext').value.trim();
		if (text.length >= 3) {
			index_js_emptyElementById('templatesearchresults');
			let entry = '<div><b>Searching for templates...</b></div>';
			$('templatesearchresults').insertAdjacentHTML('beforeend', entry);
		}
	}
	if (index_templatesearchtimer) {
		clearTimeout(index_templatesearchtimer);
		index_templatesearchtimer = 0;
	}
	index_templatesearchtimer = setTimeout(function(){index_js_templateSearchText();}, 1500);
}

/**
 *
 */
function index_js_advancedProfileSearch_ajaxFunction()
{
	if (xhrProfileSearch.readyState != 4)
		return; /* Nothing to do here! */

	var responsearray = index_js_parseResponseText(xhrProfileSearch.responseText, "Profile search error.\n\nTry again.");
	if (!responsearray)
		return;

	if ((responsearray['error'] != "INFO_ADVANCED_PROFILE_SEARCH_NO_RESULTS") &&
			(responsearray['error'] != "INFO_ADVANCED_PROFILE_SEARCH_RESULTS")) {
		if (responsearray['error'] == "ERROR_NO_PROFILE_SEARCH_TERMS") {
			$('advancedprofilesearchstatus').innerHTML = "Enter at least one field to search...";
			return;
		}
		$('advancedprofilesearchstatus').innerHTML = responsearray['error'];
		return false;
	}

	var profilelength = 0;
	if ((typeof responsearray['profile'] !== 'undefined')
			&& (responsearray['profile'].length > 0)) {
		profilelength = responsearray['profile'].length;
		$('advancedprofilesearchstatus').innerHTML = "Found "+responsearray['count']+" profiles";
	} else {
		$('advancedprofilesearchstatus').innerHTML = "No profiles found.";
	}

	/* Delete all rows but header row (has row index 0) */
	var rowcount = $('profilesearchresultstable').rows.length;
	for (var i = 1; i < rowcount; i++) {
		$('profilesearchresultstable').deleteRow(1);
	}

	/* Add the profile search results. */
	for (var p = 0; p < profilelength; p++) {
		var r = responsearray['profile'][p];
		var profilerow = $('profilesearchresultstable').insertRow();
		var namecell = profilerow.insertCell();
		var usernamecell = profilerow.insertCell();
		var towncitycell = profilerow.insertCell();
		var statecell = profilerow.insertCell();
		var countrycell = profilerow.insertCell();

		namecell.innerHTML = r['memberlink'];
		/* DEV.NOTE: The above line is an alternative to this commented block. Choose whichever one works best.
		var namelink = document.createElement('a');
		var namelinktext = document.createTextNode(r['fullname']);
		namelink.appendChild(namelinktext);
		namelink.href = "/"+r['username'];
		namelink.target = "_blank";
		namelink.className = 'profilefullname';
		var nametooltiphost = document.createElement('span');
		nametooltiphost.style.display = 'block';
		nametooltiphost.className = 'tooltiphost';
		nametooltiphost.appendChild(namelink);
		var nametooltiptext = document.createElement('span');
		nametooltiptext.className = 'tooltiptext';
		nametooltiptext.appendChild(document.createTextNode('Click to view profile in a new window.'));
		nametooltiphost.appendChild(nametooltiptext);
		namecell.appendChild(nametooltiphost);
		// */

		var usernametext = document.createTextNode(r['username']);
		usernamecell.appendChild(usernametext);

		if (r['town'] != null) {
			towncitycell.appendChild(document.createTextNode(r['town']));
		}

		if (r['state'] != null) {
			statecell.appendChild(document.createTextNode(r['state']));
		}

		if (r['country'] != null) {
			countrycell.appendChild(document.createTextNode(r['country']));
		}
	}
}

/**
 *
 */
function index_js_advancedProfileSearch()
{
	var advancedprofilesearchform = $('advancedprofilesearchform');
	var advancedprofilesearchformdata = new FormData(advancedprofilesearchform);

	$('advancedprofilesearchstatus').innerHTML = "Searching...";

	advancedprofilesearchformdata.append("ajax_command", "profile_search");
	xhrProfileSearch = new XMLHttpRequest();
	xhrProfileSearch.onreadystatechange = index_js_advancedProfileSearch_ajaxFunction;
	xhrProfileSearch.open("POST", "/index.php", true);
	xhrProfileSearch.send(advancedprofilesearchformdata);

	return false;
}

/**
 * - showanimations: boolean.
 */
function index_js_showSidePanel(showanimations)
{
	$('rightpanel').style.transition = (showanimations ? '0.5s' : '0s');
	$('showsidepanelbutton').style.transition = (showanimations ? '0.5s' : '0s');

	if (index_sidepanelvisible) {
		$('rightpanel').style.right = '-320px';
		$('showsidepanelbutton').style.right = '100%';
		index_sidepanelvisible = false;
	} else {
		$('rightpanel').style.right = '0';
		$('showsidepanelbutton').style.right = '85%';
		index_sidepanelvisible = true;
	}

	let fd = new FormData();
	fd.append("ajax_command", "save_sidepanelvisible");
	fd.append("sidepanelvisible", (index_sidepanelvisible ? 1 : 0));

	fetch('/index.php', { method: 'post', body: fd })
	.then((response) => { return response.json(); })
	.then(result => { /* console.log(result); */ })
	.catch(error => console.warn(error));
}

/**
 * Following functions set up and handle lazy loading images.
 */
function index_js_lazyloadImagesNow()
{
	let scrollY = window.scrollY;
	let winHeight = window.innerHeight;
	let lowerBound = scrollY + winHeight;
	for (let i = 0; i < index_lazyloadimgarray.length; i++) {
		let img = index_lazyloadimgarray[i];
		if ((img['loaded'] == false) && (img['top'] < lowerBound) && ((img['top'] + img['height']) > scrollY)) {
			img['el'].src = img['src'];
			index_lazyloadimgarray[i]['loaded'] = true;
		}
	}
	index_lazyloadtimer = false;
}

function index_js_setupLazyLoad()
{
	let imglist = document.getElementsByTagName('img');
	for (let i = 0; i < imglist.length; i++) {
		if (!imglist[i].hasAttribute('lazy-src')) {
			continue; /* Skip images without lazy loading attribute */
		}
		let imgEl = imglist[i];

		/* Calculate position by traversing image parent nodes.
		 * This is NOT pixel accurate and is only an estimate */
		let top = 0;
		let topstr = 'calc: '; /* DBG */
		for (let e = imgEl;
				e.nodeName.toLowerCase() != 'body';
				e = e.offsetParent) {
			top += e.offsetTop;
		}

		/* Now calculate and save position */
		let height = imgEl.clientHeight;
		let imgsrc = imgEl.getAttribute('src'); /* If img has both src and lazy-src, they get swapped around at runtime */
		let lazysrc = imgEl.getAttribute('lazy-src'); /* Changed with func_php_markdowntohtml_lazyload() */
		index_lazyloadimgarray.push({'el':imgEl, 'src': lazysrc, 'top':top, 'height':height, 'loaded':false});
		imgEl.style.background = 'center/cover url("'+imgsrc+'")';
	}
	if (index_lazyloadimgarray.length) {
		index_lazyloadtimer = setTimeout(index_js_lazyloadImagesNow, 250);
	}
}

/**
 * XXX jsjunkyard
 * XXX Sections below from old WYSIWYG editor.
 * XXX Keeping for reference or re-use, etc.
 * XXX TODO Remove eventually when obsolete.
 */
/* Change toolbar state button. */
function index_js_toggleToolbarButton(button_id, button_state)
{
	if (button_state) {
		$(button_id).style.background='#ddd';
		$(button_id).style.border='inset 1px #ccc';
	} else {
		$(button_id).style.background='inherit';
		$(button_id).style.border='solid 1px #ccc';
	}
}

function index_js_updateToolbar(editor)
{
	/* Resize the editor so it isn't too small with null content. */
	if ($(editor).innerHTML != "")
		$(editor).style.height = "auto";
	else
		$(editor).style.height = "1em";

	/* Update the toolbar toggle buttons. */
	/* Use index_js_toggleToolbarButton('btnBold', document.queryCommandState('bold')); */
}

/* XXX
 * XXX Above part is obsolete JS from old editor.
 * XXX
 */

/* Javascript that runs on page start. */
/* First, set up file previews. */
if (window.File && window.FileReader) {
	filepreviews = true;
} else {
	/* alert('No previewing possible!'); */
}

function index_js_scrollEvent()
{
	/* Reposition popups */
	if ($('editpostcontrols')
			&& ($('editpostcontrols').style.display == 'block')) {
		index_js_showeditorpopup(0, 0, false);
	}

	/* Reposition popup menus */
	if (index_popupiconid && index_popupmenuid) {
		index_js_movePopupMenu(index_popupiconid, index_popupmenuid)
	}

	/* Lazy load images (if any) */
	if (index_lazyloadtimer) {
		clearTimeout(index_lazyloadtimer);
	}
	if (index_lazyloadimgarray.length) {
		index_lazyloadtimer = setTimeout(index_js_lazyloadImagesNow, 250);
	}
}

function index_js_clickEvent(ev)
{
	var tagname = ev.target.tagName;
	var id = ev.target.id;
	var classname = ev.target.className;

	if (!tagname)
		return false; /* No valid tag? Leave. */

	tagname = tagname.toLowerCase();
	/* Close popups that hide when clicked outside */
	if (index_popupdivid) {
		if (index_popupdivinit) {
			index_popupdivinit = false;
		} else if (!($(index_popupdivid).contains(ev.target)) || !id) {
			$(index_popupdivid).style.display = 'none';
			index_popupdivid = false;
		}
	}

	/* Close popup divs, if one exists */
	if (index_modalpopupid) {
		if (index_modalpopuploaded) {
			index_modalpopuploaded = false; /* Prevents from disappearing early */
		} else {
			$(index_modalpopupid).style.display = 'none';
			index_modalpopupid = false;
		}
	}

	/* Handle closing the search popup */
	if (!($('searchpopup').contains(ev.target)
			|| $('searchtext').contains(ev.target))) {
		$('searchpopup').style.display = 'none';
	}

	/* Handle closing the template search popup */
	if (index_templatepopupvisible
			&& ($('templatesearchpopup') !== null)
			&& !($('templatesearchpopup').contains(ev.target)
			|| $('templatesearchtext').contains(ev.target))) {
		$('templatesearchpopup').style.display = 'none';
		$('templatesearchtext').setAttribute('placeholder',
				index_templateplaceholder);
	}

	/* Handle setting scraped link images. */
	if ((index_editpostdescriptionimgarray) && (index_editpostdescriptionimglinkdiv)) {
		if (!(index_editpostdescriptionimglinkdiv.contains(ev.target))) {
			index_js_insertScrapedLink_controls('save');
		}
	}
}

/**
 * The button on the search list has been changed.
 */
function index_js_searchChangeTab(button_node, restart_search)
{
	$('alllabel').style.color = '#fff';
	$('alllabel').style.background = '#666';
	$('postlabel').style.color = '#fff';
	$('postlabel').style.background = '#666';
	$('imagelabel').style.color = '#fff';
	$('imagelabel').style.background = '#666';
	//$('videolabel').style.color = '#fff';
	//$('videolabel').style.background = '#666';
	//$('audiolabel').style.color = '#fff';
	//$('audiolabel').style.background = '#666';
	//$('filelabel').style.color = '#fff';
	//$('filelabel').style.background = '#666';
	$('profilelabel').style.color = '#fff';
	$('profilelabel').style.background = '#666';
	var label_node = button_node.nextSibling;
	while (label_node.nodeType != 1) {
		label_node = label_node.nextSibling;
	}
	label_node.style.color = '#000';
	label_node.style.background = '#fff';

	var searchtype = button_node.value;
	switch (searchtype) {
	case "post":
		index_searchtype = searchtype;
		/* $("searchhintsadvanced").innerHTML = "<a href=\"advancedpostsearch\" class=\"greenbutton\">Advanced &gt;&gt;</a>"; */
		break;
	case "profile":
		$("searchhintsadvanced").innerHTML = "<a href=\"/advancedprofilesearch\" class=\"greenbutton\">Advanced &gt;&gt;</a>";
		index_searchtype = searchtype;
		break;
	default:
		index_js_emptyElementById("searchhintsadvanced");
		index_searchtype = 0;
	}

	if (restart_search) {
		index_js_searchTextInput(); /* Reset search timer */
	}
}

/**
 * Add global shortcut keys.
 */
function index_js_handleShortcutKeys(ev)
{
	switch(ev.key) {
	case 'Enter':
		if ((ev.ctrlKey || ev.metaKey) && !index_editpostid && $('newpostbutton')) {
			ev.preventDefault();
			$('newpostbutton').click();
			return false;
		}
		break;
	case 'Escape':
		if (index_modalprompt) {
			$('modalpromptclosebutton').click();
		} else if (index_modaldivid) {
			$('modaldivclosebutton').click();
		} else if (index_editpostid) {
			$('editpostcancelbutton').click();
			return false;
		}
		break;
	case ' ':
		if ((ev.ctrlKey || ev.metaKey) && !index_editpostid) {
			ev.preventDefault();
			$('templatesearchtext').focus();
			return false;
		}
		break;
	}
}

/**
 * The following index_js_linegraph* functions are used to draw line graphs.
 *
 * index_js_linegraphDraw fetches its data array using id, with an array structure of:
 * - first entry is an unquoted string of '#RRGGBB' for colour,
 * - next series of entries are axis labels for the hover-tooltip,
 * afterwards, each line to be drawn is composed of:
 * - first entry is an unquoted string of '#RRGGBB' for colour,
 * - second entry is a string text for label,
 * - a list of numbers after those two.
 * The pattern can repeat, with multiple sets of colour/label/number lists
 * appearing through the array.
 */
function index_js_linegraphDraw(id, pointX, pointY)
{
	const colourstring = /^\s*(#[0-9a-fA-F]{6,6})\s*$/;
	var a;

	/* Get array to draw, and obtain maximum values to scale graph to */
	const arr = index_linegrapharray[id];
	let minval = 0;
	let maxval = 0;
	let listLength = 0;
	let currentLength = 0;
	let lastLabelIndex = 0;
	let previousVal = '';
	let graphColour = arr[0];
	for (a = 1; a < arr.length; a++) {
		let aval = arr[a];
		/* Colour string found, save length if needed */
		let armatch = aval.match(colourstring);
		if (armatch) {
			if (!lastLabelIndex) {
				lastLabelIndex = a-1;
			}

			if (currentLength > listLength) {
				listLength = currentLength;
			}
			if ((a + 2) < arr.length) {
				a++; /* Skip colour and label string */
			}
			currentLength = 0;
		/* Process numbers as usual */
		} else {
			currentLength++;
			aval = Math.ceil(aval);
			if (aval < minval) {
				minval = aval;
			}
			if (aval > maxval) {
				maxval = aval;
			}
		}
	}

	let ctx = $(id).getContext('2d');
	let ctxWidth = $(id).width;
	let ctxHeight = $(id).height;

	ctx.save();
	ctx.clearRect(0, 0, ctxWidth, ctxHeight);

	/* Draw graph layout */
	ctx.font = 'normal 16px RobotoRegular';
	ctx.textAlign = 'left';
	ctx.fillStyle = graphColour;
	ctx.fillText(arr[1], 30, ctxHeight-4);
	ctx.textAlign = 'right';
	ctx.fillText(arr[lastLabelIndex], ctxWidth, ctxHeight-4);

	ctx.textAlign = 'right';
	ctx.fillText(maxval, 26, 16);
	ctx.fillText(minval, 26, ctxHeight-20);

	ctx.beginPath();
	ctx.moveTo(29.5, 0);
	ctx.lineTo(29.5, ctxHeight-18.5);
	ctx.lineTo(ctxWidth, ctxHeight-18.5);
	ctx.strokeStyle = graphColour;
	ctx.lineWidth = 1;
	ctx.stroke();
	ctx.closePath();

	/* Draw line (or lines) */
	let currentX = 30;
	let stepSizeX = ((ctxWidth - 30) / (listLength-1));
	let currentColour = '';
	let currentLabel = '';
	let pointValues = [];
	let cursorX = 0;
	let labelIndex = false; /* Axis text */
	for (a = lastLabelIndex+1; a < arr.length; a++) {
		let aval = arr[a];

		/* Get the line colour if found, and start a new line */
		let armatch = aval.match(colourstring);
		if (armatch) {
			/* Draw previous line, if one set */
			if (currentColour) {
				ctx.strokeStyle = currentColour;
				ctx.lineWidth = 1;
				ctx.stroke();
				ctx.closePath();
			}
			currentColour = armatch[1];
			currentLabel = arr[a+1];
			currentX = 30;
			if ((a + 2) < arr.length) {
				a++; /* Skip colour and label string */
			}
		/* Otherwise process numbers as usual */
		} else {
			let y = 0.5 + (ctxHeight - 20) - ((aval * (ctxHeight - 20)) / maxval);
			if ((((currentX - (stepSizeX / 2)) <= pointX) && (pointX <= (currentX + (stepSizeX / 2))))
					&& (pointX && pointY)) {
				pointValues.push({colour:currentColour, label:currentLabel, value:aval});
				cursorX = currentX;
				if (labelIndex === false) {
					labelIndex = (a - lastLabelIndex) - 2;
				}
				if (cursorX === 0) {
					/* Vertical cursor line seems to disappear when x == 0 */
					cursorX = 1;
				}
			}

			if (currentX === 30) {
				ctx.beginPath();
				ctx.moveTo(30, y);
			} else {
				ctx.lineTo(currentX, y);
			}
			currentX += stepSizeX;
		}
	}

	/* Finish drawing current line, if one set */
	if (currentColour) {
		ctx.strokeStyle = currentColour;
		ctx.lineWidth = 1;
		ctx.stroke();
		ctx.closePath();
	}

	/* Mouse hovered, show details */
	if (cursorX) {
		/* Draw the hover line */
		ctx.beginPath();
		ctx.moveTo(cursorX, 0);
		ctx.lineTo(cursorX, ctxHeight-20);
		ctx.strokeStyle = '#f99';
		ctx.stroke();
		ctx.closePath();

		/* Calculate tooltip size and location */
		let tooltipwidth = 130;
		let tooltipx = cursorX - (tooltipwidth + 5); // was 125;
		if (tooltipx < 32) {
			tooltipx = 32;
		}

		/* Draw the tooltip background */
		ctx.beginPath();
		ctx.globalAlpha = 0.8;
		ctx.fillStyle = '#fff';
		ctx.rect(tooltipx, 0, tooltipwidth, 20+(20*pointValues.length));
		ctx.fill();
		ctx.closePath();
		ctx.globalAlpha = 1.0;

		/* Draw tooltip axis text */
		ctx.font = 'normal 16px RobotoRegular';
		ctx.textAlign = 'right';
		ctx.fillStyle = graphColour;
		ctx.fillText(arr[labelIndex], tooltipx+tooltipwidth, 16);

		for (a = 0; a < pointValues.length; a++) {
			let pval = pointValues[a];
			let colour = pval['colour'];
			let label = pval['label'];
			let value = pval['value'];

			/* Draw the tooltip text */
			ctx.font = 'normal 16px RobotoRegular';
			ctx.textAlign = 'left';
			ctx.fillStyle = colour;
			ctx.fillText(label+': '+value, tooltipx+25, 20 + 16 + (a * 20));
		}
	}
}

function index_js_linegraphMouseMove(ev)
{
	let id = ev.target.id;
	let x = ev.layerX;
	let y = ev.layerY;
	index_js_linegraphDraw(id, x, y);
}

function index_js_linegraphMouseOut(ev)
{
	let id = ev.target.id;
	index_js_linegraphDraw(id, 0, 0);
}

function index_js_linegraphInit(id, pointList)
{
	const pointArray = pointList.split(',');
	index_linegrapharray[id] = pointArray;

	index_js_linegraphDraw(id, 0, 0);
	$(id).onmousemove = index_js_linegraphMouseMove;
	$(id).onmouseout = index_js_linegraphMouseOut;
}

function index_js_handleMessageReceived(response)
{
	let id = response.id;
	let messagesender = response.sender;

	if (index_messagewindowarray[0]['username'] == messagesender) {
		index_js_showMessage(id);
	} else {
		index_js_getUpdates();
	}
}

function index_js_handleOnlineStatus(response)
{
	let onlinestatus = Math.floor(response.onlinestatus);

	switch (onlinestatus) {
	case 12:
	case 22:
	case 32:
		index_onlineStatus = onlinestatus;
		break;
	default:
		console.log(`index.php:<?=__LINE__?> [handleOnlineStatus] - No match! onlinestatus=${onlinestatus}`);
	}

	let imgsrc = '';
	if (index_onlineStatus == 12) {
		imgsrc = '/img/status-online12.png';
	} else if (index_onlineStatus == 22) {
		imgsrc = '/img/status-away12.png';
	} else if (index_onlineStatus == 32) {
		imgsrc = '/img/status-invisible12.png'
	}

	if (imgsrc) {
		$('userpanelonlinestatus').src = imgsrc;
	}
}

/**
 * Handle messages from various backends.
 * Can come from server-sent events (SSE) or websock connections.
 */
function index_js_handleBackendData(data)
{
	var response = JSON.parse(data);

	switch (response.type) {
	case 'system':
		console.log('index.php:<?=__LINE__?> TODO: Handle system message from SSE or Websock!');
		break;
	case 'connected':
		break;
	case 'get_updates':
		index_js_getUpdates();
		break;
	case 'message_received':
		index_js_handleMessageReceived(response);
		break;
	case 'online_status':
		index_js_handleOnlineStatus(response);
		break;
	default:
		console.log('<?=__LINE__?> ERROR: Unknown SSE or websock response.type:'+response.type);
	}
}

/**
 * Fallback function to check if online status changes are successful.
 */
function index_js_setOnlineStatus_retry()
{
	switch (index_onlineStatus) {
	case 10:
	case 20:
	case 30:
		/* Retry connection */
		index_js_setOnlineStatus(index_onlineStatus);
		break;
	case 12:
	case 22:
	case 32:
		/* Status change successful */
		break;
	default:
		/* Unknown online status */
	}
}

/**
 *
 */
function index_js_setOnlineStatus_saveData()
{
	let fd = new FormData();
	fd.append("ajax_command", "save_online_status");
	fd.append("onlinestatus", index_onlineStatus);

	fetch('/index.php', { method: 'post', body: fd })
	.then((response) => { return response.json(); })
	.then(result => { /* console.log(result); */ })
	.catch(error => console.warn(error));

	switch (index_onlineStatus) {
	case 10:
	case 20:
	case 30:
		$('userpanelonlinestatus').src = '/img/status-connecting12.gif';
		index_onlineStatus++;
		break;
	}

	setTimeout(function(){ index_js_setOnlineStatus_retry(); }, 1500); /* Check in 1.5sec */
}

/**
 * status: 10=Online, 20=Away, 30=Invisible(Appear offline).
 */
function index_js_setOnlineStatus(status)
{
	switch (status) {
	case 10: /* Online */
	case 20: /* Away */
	case 30: /* Invisible (Appear offline) */
		index_onlineStatus = status;
		//index_js_startWebsocket();
		if (index_eventsource) {
			index_js_setOnlineStatus_saveData(); /* Save to db */
		} else {
			index_js_startSSE();
			/* _js_startSSE above calls _js_setOnlineStatus_saveData()
			 * itself when SSE connection has started. */
		}
		break;
	case 0: /* Offline */
		index_onlineStatus = 0;
		index_js_setOnlineStatus_saveData(); /* Save to db */
		//index_websocket.close();
		//index_websocket = null;
		index_eventsource.close();
		index_eventsource = null;
		$('userpanelonlinestatus').src = '/img/status-offline12.png';
		break;
	default:
		/* Ignore invalid values */
	}

}

/**
 * Sets up server-sent events to get live updates.
 */
function index_js_startSSE()
{
	index_eventsource = new EventSource('/sseserver.php');

	index_eventsource.onopen = function() {
		index_js_setOnlineStatus_saveData();
	};

	index_eventsource.onmessage = function(ev) {
		index_js_handleBackendData(ev.data);
	};

	index_eventsource.onerror = function(ev) {
		if (index_eventsource.readyState == EventSource.CLOSED) {
			console.log(`index.php:<?=__LINE__?> Connection closed!`);
		}
	};
}

/**
 * Set up websockets for live chat and file transferring.
 * (XXX: This is not executed by current deployments, but will be used when
 * security and compatibility issues have been resolved.)
 */
function index_js_startWebsocket()
{
	switch (index_onlineStatus) {
	case 11:
	case 12:
		/* Handle broken "Online" reconnections */
		index_onlineStatus = 11;
		break;
	case 21:
	case 22:
		/* Handle broken "Away" reconnections */
		index_onlineStatus = 21;
		break;
	case 31:
	case 32:
		/* Handle broken "Invisible" reconnections */
		index_onlineStatus = 31;
		break;
	case 10:
	case 20:
	case 30:
		/* New online status change operation */
		index_onlineStatus++;
		break;
	default:
		return false;
	}

	if (!index_websocket) {
		index_websocket = new WebSocket(index_websockuri);
	}
	$('userpanelonlinestatus').src = '/img/status-connecting12.gif';

	index_websocket.onopen = function(ev){
		index_websockActive = true;
		index_websockRetrySec = 0; /* Resets on new connection */
		index_onlineStatus++;

		let imgsrc = '/img/status-online12.png';
		if (index_onlineStatus == 22) {
			imgsrc = '/img/status-away12.png';
		} else if (index_onlineStatus == 32) {
			imgsrc = '/img/status-invisible12.png'
		}
		$('userpanelonlinestatus').src = imgsrc;

		/* Identify this user for the websocket */
		let messageJSON = {
			type: 'wsopen',
			username: "<?=$index_activeusername?>",
			sessionid: "<?=($index_activeuser['sessionid'] ?? '')?>",
		};
		index_websocket.send(JSON.stringify(messageJSON));
	}

	index_websocket.onmessage = function(ev){
		if (!ev.data) {
			console.log('No ev data for websocket!');
		} else {
			var response = JSON.parse(ev.data);
			switch (response.type) {
			case 'system':
				console.log('<?=__LINE__?> INFO: index_websocket Got system message!');
				/* Do something with system messages */
				break;
			case 'connected':
				break;
			case 'get_updates':
				index_js_getUpdates();
				break;
			case 'message_received':
				index_js_handleMessageReceived(response);
				break;
			default:
				console.log('<?=__LINE__?> ERROR: Unknown websock response.type:'+response.type);
			}
		}
	}

	index_websocket.onerror = function(ev){
		console.log('Websocket error:'+ev.data);
	}

	index_websocket.onclose = function(ev){
		index_websockActive = false;
		index_websockRetrySecCt = index_websockRetrySec;
		$('userpanelonlinestatus').src = '/img/status-offline12.png';

		if (!index_onlineStatus) {
			return; /* User has selected offline mode */
		}

		/* Increase retry delay time with subsequent calls */
		if (index_websockRetrySec < 15) {
			index_websockRetrySec += 3;
		}

		index_js_retryWebsocketCountdown();
		index_websocket = null;
	}
}

function index_js_retryWebsocketCountdown()
{
	if (index_websockRetrySecCt > 0) {
		index_websockRetrySecCt--;
		setTimeout(function(){ index_js_retryWebsocketCountdown(); }, 1000);
	} else {
		setTimeout(function(){ index_js_startWebsocket(); }, 1000);
		$('userpanelonlinestatus').src = '/img/status-connecting12.gif';
	}
}

function index_js_bodyonload()
{
	index_js_searchChangeTab($('allsearch'), 0);
	document.onscroll = index_js_scrollEvent;
	document.addEventListener('click', index_js_clickEvent, false);
	document.addEventListener('keydown', index_js_handleShortcutKeys);
	index_js_setupLazyLoad();
<?php
if ($index_activeusername) {
?>
	console.log("<?=__LINE__?>: Running index_js_getUpdates for the first time.");
	index_js_getUpdates();
	index_js_regularEvents();

	/* Restore online status if previously set */
	let onlinestatus = Math.floor(<?=$index_activeuser['onlinestatus']?>);
	switch (onlinestatus) {
	case 12:
	case 22:
	case 32:
		index_js_setOnlineStatus(onlinestatus-2);
	}

	/* Restore sidepanel visibility */
	let sidepanelvisible = <?=$index_activeuser['sidepanelvisible'] ?? 0?>;
	if ((sidepanelvisible == 1) && !index_sidepanelvisible) {
		index_js_showSidePanel(false);
	}
<?php
} else if ($index_showposts) {
?>
	console.log("<?=__LINE__?>: TODO: run index_js_getUpdates when NOT signed in.");
<?php
}
?>

}

/* Set up tasks that run an regular (or random) intervals. */
function index_js_regularEvents()
{
	index_js_getUpdates();
	if ((index_messagerecipient) || (index_messagepostid > 0))
		index_js_message(index_messagerecipient, index_messagepostid);

	/*
	index_js_updateContactList();
	index_js_getMessages();
	// */
	//index_minutetaskstimer = window.setTimeout(index_js_regularEvents, 10000);
}
// -->
<?php
/* Update analytics data with screen width and javascript enabled */
if ($index_analyticspageid && $index_analyticsphpsessionid) {
	echo "index_js_sendAnalyticData('{$index_analyticspageid}', '{$index_analyticsphpsessionid}');";
}
?>
</script>

<?php
if ($index_page == 'billing') {
?>
<script src="https://js.stripe.com/v3/"></script>
<?php
}

if ($headtags ?? '') {
	echo "\n\n$headtags\n\n";
}
?>

</head>

<body id="body" onload="index_js_bodyonload();">

<div id="mirrorisland-header">
	<div class="mirrorisland-headerflex">
		<span class="mirrorisland-headerflexitem">
			<h1>
				<a href="/" class="tooltiphost"><img src="/milogo.png" class="headerimg" id="headerlogo">
				<span class="tooltiptext tooltiptextleftedge">Home</span>
				</a>
			</h1>
<?php
if ($index_activeuserid) {
?>
			<a href="/new" class="tooltiphost"><img class="headerimg" src="/img/viewnew.png">
			<span class="tooltiptext tooltiptextleft">Explore New Posts</span></a>
<?php
}
?>
		</span>
<?php
/* TODO: Add an array-type list of links instead of this hard-coded one. */
if ($helppage) {
?>
		<span class="mirrorisland-headerflexitem"><a href="<?=$helppage?>">Help</a></span>
<?php
}

if (!$index_activeuser || (($index_activeuser['accounttype'] ?? '') === '')) {
?>
		<span class="mirrorisland-headerflexitem"><a href="/pricing">Pricing</a></span>
<?php
}
?>
		<form id="searchform" class="mirrorisland-headerflexitem searchform" action="<?=$urlhome?>/" method="multipart/form-data" onsubmit="return index_js_searchText(this);">
			<span id="searchtextspan"><input id="searchtext" class="shadowinset" name="searchtext" type="text" autocomplete="off" placeholder="Search..." onFocus="index_js_setupSearch(this);"></span>
			<div id="searchpopup" class="searchpopup">
				<div id="searchbuttons">
					<input onChange="index_js_searchChangeTab(this, true);" id="allsearch" name="searchtype" type="radio" value="all" checked>
							<label for="allsearch" id="alllabel">All</label>
					<input onChange="index_js_searchChangeTab(this, true);" id="postsearch" name="searchtype" type="radio" value="post">
							<label for="postsearch" id="postlabel">Posts</label>
					<input onChange="index_js_searchChangeTab(this, true);" id="imagesearch" name="searchtype" type="radio" value="image">
							<label for="imagesearch" id="imagelabel">Images</label>
					<input onChange="index_js_searchChangeTab(this, true)" id="videosearch" name="searchtype" type="radio" value="video">
							<label for="videosearch" id="videolabel">Videos</label>
					<input onChange="index_js_searchChangeTab(this, true)" id="audiosearch" name="searchtype" type="radio" value="audio">
							<label for="audiosearch" id="audiolabel">Audio</label>
					<input onChange="index_js_searchChangeTab(this, true)" id="filesearch" name="searchtype" type="radio" value="file">
							<label for="filesearch" id="filelabel">Files</label>
					<input onChange="index_js_searchChangeTab(this, true)" id="profilesearch" name="searchtype" type="radio" value="profile">
							<label for="profilesearch" id="profilelabel">Profiles</label>
				</div>
				<div id="searchhints">Sort by
					<select size="1" id="searchorder" name="searchorder" class="silverbutton">
						<option value="relevance">Relevance</option>
						<option value="popularity">Popularity</option>
						<option value="date">Date</option>
						<option value="name">Name</option>
					</select>
					<input type="submit" class="silverbutton" name="btn" value="Prev" onclick="index_searchbtn=-1;">
					<input type="submit" class="silverbutton" name="btn" value="Next" onclick="index_searchbtn=1;">
					<span id="searchstatus"></span>
					<span id="searchhintsadvanced"></span>
				</div>
				<div id="searchlist">
					<div class="searchlistpost">Enter a search query to start.</div>
				</div>
			</div>
		</form>
<?php
/* Display author controls. */
if ($index_activeusername) {
	$formaction = ""; /* Set this to return to the same page after signing out. */
	$filelow = ($index_activeuserfilespace / 100) + 20971520; /* 1% + 20MiB */
	$filehigh = ($index_activeuserfilespace / 10) + 41943040; /* 10% + 40MiB */
	$filefree = $index_activeuserfilespace - $index_activeuserfileuse;
?>
		<div id="userpanel" class="mirrorisland-headerflexitem headerusermenudiv">
			<span class="headerusermenubuttonpopupdiv">
				<button onclick="index_js_popupDiv('headerusermenu');">
					<img id="userpanelonlinestatus" src="/img/status-offline12.png">
					<?=$index_activeusername?>
					<img src="/img/popupmenudown.png">
				</button>
				<div id="headerusermenu">
					<ul>
					<li><a onClick="index_js_setOnlineStatus(10);"><img src="/img/status-online12.png">Online</a></li>
					<li><a onClick="index_js_setOnlineStatus(20);"><img src="/img/status-away12.png">Away</a></li>
					<li><a onClick="index_js_setOnlineStatus(30);"><img src="/img/status-invisible12.png">Invisible</a></li>
					<li><a onClick="index_js_setOnlineStatus(0);"><img src="/img/status-offline12.png">Offline</a></li>
					</ul>
					<div class="popuplistseparator"></div>
					<ul>
					<li><a href="<?=$urlhome?>/<?=$index_activeusername?>"><img src="/img/status-owner12.png">View/Edit Profile</a></li>
					<li><a href="<?=$urlhome?>/<?=$index_activeusername?>/settings/"><img src="/img/settings.png" width="12" height="12">Account Settings</a></li>
					<li><a href="<?=$urlhome?>/<?=$index_activeusername?>/analytics/"><img src="/img/analytics.png" width="12" height="12">Analytics</a></li>
					<li><div class="popuplistmetercontainer">
						<div id="popuplistmetertext" class="popuplistmetertext">
							<?=func_php_fuzzyfilesize($filefree)?> available
						</div>
						<meter id="popuplistmeter" class="popuplistmeter" min="0" max="<?=$index_activeuserfilespace?>"
								low="<?=$filelow?>" high="<?=$filehigh?>" optimum="<?=$index_activeuserfilespace?>"
								value="<?=$filefree?>"></meter>
					</div></li>
					</ul>
					<div class="popuplistseparator"></div>
					<form action="<?=$formaction?>" class="popuplistform" onSubmit="return index_js_signOut();"><input class="silverbutton" type="submit" value="Sign Out"></form>
				</div>
			</span>
		</div>
<?php
/* Otherwise display the sign in link. */
} else {
	$page = func_php_cleantext($_GET['page'] ?? '', 64);
	$ref = func_php_cleantext($_GET['ref'] ?? '', 64);
	$signinref = $_SERVER['REQUEST_URI'];

	/* Prevent signin page referring to itself */
	if (($page == 'signin') && $ref) {
		$signinref = $ref;
	}
?>
		<div id="userpanel" class="mirrorisland-headerflexitem">
			<div style="white-space:nowrap;background:none;padding:8px 12px;"><a href="/signin?ref=<?=$signinref?>" style="color:#000;">Sign In</a></div>
		</div>
<?php
}
?>
	</div>
<?php
	/* Generate a tree of titles, if this post is in the current tree */
	if (($index_page == 'page') && $index_page_id) {
		$query = sprintf('select posts.id, posts.parentid, posts.title, membertree.level,'
				.' concat((select group_concat(mtlist.parentid order by mtlist.level desc separator "-")'
				.' 	from posts plist left join membertree mtlist on plist.id = mtlist.postid'
				.' 	where plist.title is not null and plist.title != "" and plist.id = posts.id),'
				.' 	"-", posts.id) as parentlist,'
				.' (select count(*) from membertree cctree where cctree.parentid = posts.id)'
				.'	as childcount,'
				.' (select count(*) from membertree ptree where ptree.parentid = posts.id'
				.' 	and ptree.postid = %d) as currentpath' /* POSTID */
				.' from posts left join membertree on posts.id = membertree.postid'
				.' left join posts as parentpost on posts.parentid = parentpost.id'
				.' where posts.title is not null and posts.title != ""'
				.' and ((parentpost.title is not null and parentpost.title != "") or (membertree.level is NULL))'
				.' and (membertree.parentid = (select basemt.parentid from membertree as basemt'
				.'		left join membertree as rootmt on basemt.parentid = rootmt.postid'
				.' 		where (basemt.postid = %d or basemt.parentid = %d)' /* POSTID, POSTID */
				.' 		and rootmt.id is NULL limit 1)'
				.' 	or posts.id = (select basemt.parentid from membertree as basemt'
				.'		left join membertree as rootmt on basemt.parentid = rootmt.postid'
				.' 		where (basemt.postid = %d or basemt.parentid = %d)' /* POSTID, POSTID */
				.' 		and rootmt.id is NULL limit 1))'
				.' order by parentlist limit 100;',
				$index_page_id, $index_page_id, $index_page_id, $index_page_id, $index_page_id);
		$result = func_php_query($query);

		if (mysqli_num_rows($result) > 1) {
			$r = mysqli_fetch_assoc($result);
?>
	<script>
		$('body').style.paddingTop = '80px';
	</script>
	<div class="headingtreebackground">
		<div class="headingtreespacer">
			<span class="headingtree">
				<button type="button" onclick="index_js_popupDiv('headingtreediv');">
					<img src="/img/treemenu.png"> <?=$r['title']?> <img src="/img/popupmenudown.png">
				</button>
				<div id="headingtreediv">
					<h2><a href="/<?=$r['id']?>">Home - <?=$r['title']?></a></h2>
<?php
			$currentpath = '';
			if ($r['currentpath']) {
				$currentpath = "<a href=\"{$r['id']}\">Home</a>";
			} else {
				$currentpath = "<strong>Home</strong>";
			}

			$treedepth = 0;
			$childcount = 0;
			$childparentid = false;
			while ($r = mysqli_fetch_assoc($result)) {
				while ($treedepth < $r['level']) {
					echo '<ul>';
					$treedepth++;
				}
				while ($treedepth > $r['level']) {
					echo '</ul>';
					$treedepth--;
				}

				if ($r['childcount']) {
					$childcount = 0;
					$childparentid = $r['id'];
				} else {
					$childcount++;
					if ($childcount >= 6) {
						if ($childcount == 6) {
							echo "<a href=\"/{$childparentid}\"><i>More...</i></a>";
						}
						continue;
					}
				}

				$tagstart = "<a href=\"/{$r['id']}\" title=\"{$r['title']}\">";
				$tagend = '</a>';
				if ($r['currentpath']) {
					$currentpath .= " &gt; {$tagstart}{$r['title']}{$tagend}";
					$tagstart = "<li class=\"treeitemactive\"><strong>{$tagstart}";
					$tagend = "{$tagend}</strong></li>";
				} else if ($r['id'] == $index_page_id) {
					$currentpath .= " &gt; <strong>{$r['title']}</strong>";
					$tagstart = '<li class="treeitemactive"><strong>';
					$tagend = '</strong></li>';
				} else {
					$tagstart = "<li>{$tagstart}";
					$tagend = "{$tagend}</li>";
				}
				echo "{$tagstart}{$r['title']}{$tagend}";
			}
			while ($treedepth > 0) {
				echo '</ul>';
				$treedepth--;
			}
?>
				</div>
			</span>
			<span class="treeitempath"><?=$currentpath?></span>
		</div>
	</div>
<?php
		}
	}
?>
</div>
<?php
/* The contact list should only load if signed in. */
if ($index_activesessionid) {
?>
<div id="rightpanel" class="sidepanel rightpanel"
		style="right:<?=$index_activeuser['sidepanelvisible'] == 1 ? '0' : '-320px;'?>">
	<button id="showsidepanelbutton" class="showsidepanelbutton"
			style="right:<?=$index_activeuser['sidepanelvisible'] == 1 ? '85%' : '100%'?>"
			onclick="index_js_showSidePanel(true);">
		<div class="tooltiphost">
			<span class="tooltiptext" style="right:200%;top:40%;bottom:auto;">Show Contact and Messages</span>
			<img src="/img/contactsmenu.png">
			<div style="height:70px; position:relative;">
				<div style="transform:rotate(-90deg);transform-origin:bottom right;position:absolute;bottom:100%;right:-4px;">Contacts</div>
			</div>
		</div>
	</button>
	<div id="sidepanelmain" class="sidepanelmain">
		<div class="sidepaneltitlebar">
			<span id="sidepanelreloadbutton" class="tooltiphost">
				<a onclick="index_js_regularEvents();"><img src="/reload.png"></a>
				<span class="tooltiptext tooltiptextleft">Check for updates</span>
			</span>
			<div>
				<span class="sidepanelbuttonindicator tooltiphost">
					<img alt="Unread Messages" src="/img/sidepanel-message.png"><sup id="sidepanelunreadmessagecount">0</sup>
					<span class="tooltiptext tooltiptextright">New posts and messages</span>
				</span>
				<span class="sidepanelbuttonindicator tooltiphost">
					<img alt="Contacts" src="/img/sidepanel-users.png"><sup id="sidepanelonlineusercount">0</sup>
					<span class="tooltiptext tooltiptextright">Contacts Online</span>
				</span>
			</div>
			<button onclick="index_js_showSidePanel(true);" class="tooltiphost sidepanelclosebutton">×<span class="tooltiptext tooltiptextright">Close Contacts</span></button>
		</div>
		<div class="sidepanelbuttonrow">
			<a class="sidepanelfullwidthbutton" href="/advancedprofilesearch" target="_blank">Find People &gt;&gt;</a>
		</div>
		<div class="sidepanelscroll">
			<div id="contactslist" class="contactlist">
				<div class="userpanelheading">Contacts (Starred)</div>
			</div>
			<div id="otherslist" class="contactlist">
				<div class="userpanelheading">Others</div>
			</div>
			<div id="messagelist" class="contactlist">
				<div class="userpanelheading">Messages</div>
			</div>
			<div id="grouplist" class="contactlist">
				<div class="userpanelheading">Groups</div>
			</div>
			<div id="grouplistpending" class="contactlist">
			</div>
			<div id="notificationslist" class="contactlist">
				<div class="userpanelheading">Notifications</div>
			</div>
		</div>
	</div>
</div>

<?php
} /* END - show contact list */
?>

	<div id="page" class="mainpage">

<?php
/* Start div#titleblock */
$editprofile = false;
if (($index_page == "profile") && is_array($index_showprofile)) {
	$profileimg = "/img/defaultprofile.jpg";
	if (($index_showprofile['profileimagefileid'] != 0) && $index_showprofile['profileimagefilename']) {
		$profileimg = "/index.php?fileid=".$index_showprofile['profileimagefileid']."&filename=".$index_showprofile['profileimagefilename'];
	}

	if (strtolower($index_activeusername) == strtolower($index_showprofile['username'])) {
		$menuicon = "/img/postmenu-edit.png";
		$editprofile = true;
	} else {
		$menuicon = $urlhome."/img/postmenu.png";

		/* Save Analytics data for non-account holders */
		$index_analyticspageid = $index_page_id;
		$index_analyticsphpsessionid = session_id();
		$httpuseragent = $_SERVER['HTTP_USER_AGENT'] ?? false;
		$remoteaddr = $_SERVER['REMOTE_ADDR'] ?? false;
		$referrer = $_SERVER['HTTP_REFERER'] ?? false;
		func_php_setAnalytics($index_analyticspageid, $index_analyticsphpsessionid,
				$httpuseragent, $remoteaddr, $referrer, false, 'view',
				true); /* TODO: set proper 'authorised' value */
	}
?>
		<div id="titleblock" class="debugpostclass profile titlepost debugpostborderclass">
			<div id="profileeditform">
<?php
if ($editprofile) {
?>
				<input id="editprofilebutton" type="button" class="silverbutton" value="Edit Profile"
						style="position:absolute; right:24px; vertical-align:top;"
						onclick="index_js_editProfile(true);">
<?php
}
?>
				<div class="popuplist posteditlist" style="position:absolute;top:0;right:0;">
					<!-- This entire menu is hidden until profile-related features are added (such as share profile by email, download copy of all data, etc.)
					<img src="<?=$menuicon?>">
					<div>
					<ul>
					<li><a onclick="alert('Download a copy (backup) of all visible data from this account!');"><?=__LINE__?> Download</a></li>
					<li><a onclick="alert('Flag as inappropriate!');"><?=__LINE__?> Flag</a></li>
					<li><a onclick="alert('Set Age (rating? rename table column also)');"><?=__LINE__?> Age (rating?)</a></li>
					-->
<?php
if ($editprofile) {
?>
					<!--
					<li><a onclick="index_js_editProfile(true);">Edit</a></li>
					<!--
					<li><a onclick="alert('Copy!');">Copy</a></li>
					<li><a onclick="alert('Move!');"><?=__LINE__?> Move</a></li>
					-->
<?php
}
?>
					<!--
					</ul>
					</div>
					-->
				</div>
			</div>
				<div id="profileinformation" style="overflow:hidden;">
					<div id="profileimgdivblock">
						<div class="profileimgdiv"> <!-- style="background:url('/img/defaultprofile.jpg') no-repeat 50% 50%;"-->
							<img id="profileimg" class="profileimg" src="<?=$profileimg?>" unselectable="on" draggable="false" onDragStart="return false;">
							<div id="profileimgzoomslider" style="position:absolute;bottom:0;left:0;width:256px;height:16px;display:none;">
								<div class="range-slider" id="range-slider">
									<span></span>
								</div>
							</div>
						</div>
						<div id="profileimgeditcontrols">
							<div style="margin:4px 0;">
								<input class="silverbutton" type="button" value="Upload Photo" onClick="$('profileimgfileinput').click();">
								<input class="silverbutton" type="button" value="Delete" onClick="index_js_editProfileDeleteImg();">
							</div>
							<div>Drag and zoom to move the image.</div>
						</div>
					</div>
					<div class="profiletableblock">
						<div style="padding:8px 4px 8px 10px;color:#59a;font-weight:bold;">
							<?=$index_showprofile['username']?>@<?=$servername?>
						</div>
						<div class="profiletable">
							<div class="profiletablerow">
								<div class="profiletablelabel">Name</div>
								<div class="profiletabledata">
									<input id="profilefullname" name="profilefullname" type="text" value="<?=$index_showprofile['fullname']?>" readonly="true">
								</div>
							</div>
							<div class="profiletablerow">
								<div class="profiletablelabel">Email</div>
								<div class="profiletabledata">
									<input id="profileemail" name="profileemail" type="text" value="<?=$index_showprofile['email']?>" readonly="true">
								</div>
							</div>
							<div class="profiletablerow">
								<div class="profiletablelabel" class="inline">Website</div>
								<div class="profiletabledata">
									<input id="profilewebsite" name="profilewebsite" type="text" value="<?=$index_showprofile['website']?>" readonly="true">
								</div>
							</div>
							<div class="profiletablerow profiletablerowbordertop">
								<div class="profiletablelabel">Address</div>
								<div class="profiletabledata">
									<input id="profileaddress" name="profileaddress" type="text" value="<?=$index_showprofile['address']?>" readonly="true">
								</div>
							</div>
							<div class="profiletablerow">
								<div class="profiletablelabel"><span>Address 2</span></div>
								<div class="profiletabledata">
									<input id="profileaddress2" name="profileaddress2" type="text" value="<?=$index_showprofile['address2']?>" readonly="true">
								</div>
							</div>
							<div class="profiletablerow">
								<div class="profiletablelabel">Town/Suburb</div>
								<div class="profiletabledata">
									<input id="profiletown" name="profiletown" type="text" value="<?=$index_showprofile['town']?>" readonly="true">
								</div>
							</div>
							<div class="profiletablerow">
								<div class="profiletablelabel">State</div>
								<div class="profiletabledata">
									<input id="profilestate" name="profilestate" type="text" value="<?=$index_showprofile['state']?>" readonly="true">
								</div>
							</div>
							<div class="profiletablerow">
								<div class="profiletablelabel">Post Code</div>
								<div class="profiletabledata">
									<input id="profilepostcode" name="profilepostcode" type="text" value="<?=$index_showprofile['postcode']?>" readonly="true">
								</div>
							</div>
							<div class="profiletablerow">
								<div class="profiletablelabel">Country</div>
								<div class="profiletabledata">
									<input id="profilecountry" name="profilecountry" type="text" value="<?=$index_showprofile['country']?>" readonly="true">
								</div>
							</div>
						</div>
					</div>
					<div>
						<div class="profiletable">
							<div class="profiletablerow profiletablerowbordertop">
								<div class="profiletablelabel">Interests</div>
								<div class="profiletabledata">
									<input id="profileinterests" name="profileinterests" type="text" value="<?=$index_showprofile['interests']?>" readonly="true">
								</div>
							</div>
							<div class="profiletablerow">
								<div class="profiletablelabel">About</div>
								<div class="profiletabledata">
									<div id="profileaboutcontenteditable" class="spacinginset" onChange="$('profileabout').value = this.innerHTML;"
											onKeyUp="$('profileabout').value = this.innerHTML;"><?=$index_showprofile['about']?></div>
									<input id="profileabout" name="profileabout" type="hidden" value="<?=$index_showprofile['about']?>">
								</div>
							</div>
						</div>
					</div>
				</div>
<?php
	if (strtolower($index_activeusername) == strtolower($index_showprofile['username'])) {
?>
				<form id="profileinformationform" class="editpostcontrolsform" style="display:none;" method="post" enctype="multipart/form-data"
						onSubmit="index_js_checkProfileInformation(); return false;">
					<input type="hidden" id="profileeditphotowidth" name="profilephotowidth" value="">
					<input type="hidden" id="profileeditphotoheight" name="profilephotoheight" value="">
					<input type="hidden" id="profileeditphotoleft" name="profilephotoleft" value="">
					<input type="hidden" id="profileeditphototop" name="profilephototop" value="">
					<input type="hidden" id="profileeditfullname" name="profilefullname" value="<?=$index_showprofile['fullname']?>">
					<input type="hidden" id="profileeditemail" name="profileemail" value="<?=$index_showprofile['email']?>">
					<input type="hidden" id="profileeditwebsite" name="profilewebsite" value="<?=$index_showprofile['website']?>">
					<input type="hidden" id="profileeditaddress" name="profileaddress" value="<?=$index_showprofile['address']?>">
					<input type="hidden" id="profileeditaddress2" name="profileaddress2" value="<?=$index_showprofile['address2']?>">
					<input type="hidden" id="profileedittown" name="profiletown" value="<?=$index_showprofile['town']?>">
					<input type="hidden" id="profileeditstate" name="profilestate" value="<?=$index_showprofile['state']?>">
					<input type="hidden" id="profileeditpostcode" name="profilepostcode" value="<?=$index_showprofile['postcode']?>">
					<input type="hidden" id="profileeditcountry" name="profilecountry" value="<?=$index_showprofile['country']?>">
					<input type="hidden" id="profileeditinterests" name="profileinterests" value="<?=$index_showprofile['interests']?>">
					<input type="hidden" id="profileeditabout" name="profileabout" value="<?=$index_showprofile['about']?>">
					<input type="hidden" id="profileeditimgsrc" name="profileeditimgsrc" value="<?=$profileimg?>">
					<input type="hidden" id="profileeditimagefileid" name="profileeditimagefileid" value="<?=$index_showprofile['profileimagefileid']?>">
					<input id="profileimgfileinput" name="profileimgfileinput" type="file" style="display:none;" onChange="index_js_previewProfileImg();">
					<div id="saveprofileinformationsuggestions"></div>
					<div class="editpostbuttons">
						<input class="silverbutton" name="profileeditcancel" type="button" value="Cancel" onClick="index_js_editProfileCancel();">
						<input class="silverbutton" name="profileeditsubmit" type="submit" value="Save Profile">
					</div>
				</form>
<?php
	}
?>
		</div>
<?php
} else if ($index_page == 'accountsettings') {
	$query = sprintf('/* '.__LINE__.' */ select * from accounts where username="%s" and sessionid="%s" limit 1',
			func_php_escape($activeusername),
			func_php_escape($activesessionid));
	$result = func_php_query($query);
	$profile = mysqli_fetch_assoc($result);

	/* Format any strings for editing. */
	$viewchecked = array(0 => '', 1 => '', 2 => '', 3 => '', 4 => '', 5 => '');
	$viewchecked[$profile['viewpermissions']] = 'checked';

	$editchecked = array(0 => '', 1 => '', 2 => '', 3 => '', 4 => '', 5 => '');
	$editchecked[$profile['editpermissions']] = 'checked';

	$replychecked = array(0 => '', 1 => '', 2 => '', 3 => '', 4 => '', 5 => '');
	$replychecked[$profile['replypermissions']] = 'checked';

	$approvechecked = array(0 => '', 1 => '', 2 => '', 3 => '', 4 => '', 5 => '');
	$approvechecked[$profile['approvepermissions']] = 'checked';

	$moderatechecked = array(0 => '', 1 => '', 2 => '', 3 => '', 4 => '', 5 => '');
	$moderatechecked[$profile['moderatepermissions']] = 'checked';

	$tagchecked = array(0 => '', 1 => '', 2 => '', 3 => '', 4 => '', 5 => '');
	$tagchecked[$profile['tagpermissions']] = 'checked';
?>
		<div class="titlepost debugpostclass debugpostborderclass">
			<h2>Account Settings</h2>
<?php
	if (!$index_showsettings) {
		echo '<br><p class="centered">Only the account holder can access these settings.</p>';
	} else {
		/* Show subscription details, if currently active. */
		if ($index_activeuser['accounttype']) {
?>
			<div style="margin:8px 0;">
				<p class="information" style="padding-left:1em;">Active Subscription: <?=$index_activeuser['accounttype']?>
					<br><a class="boldlink" href="<?=$urlhome?>/billing">Manage</a></p>
			</div>
<?php
		}
?>
		<form action="<?=$urlhome?>/<?=$index_activeusername?>/" id="profilesettingsform" method="post" enctype="multipart/form-data"
				onSubmit="return index_js_checkProfileSettings();">
			<fieldset class="formfield">
			<div class="formdiv">
				<label for="profilesettingsconfirmedage">Confirmed Age:</label>
				<div class="forminput">
					<input id="profilesettingsconfirmedage" name="profilesettingsconfirmedage" class="shadowinset"
							type="number" min="3" max="100" value="<?=$profile['confirmedage']?>">
					<br>
					<small>The confirmed age is used to determine the suitability of content access with this account.<br>
						Only the minimum age needed to access content is required.<br>
						Setting a lower age value can filter out adult or mature content.<br>
						This does not represent your actual age and is not publicly shown.</small>
				</div>
			</div>
			<div class="formdiv">
				<label for="profilesettingsaccountemail">Account Email:</label>
				<div class="forminput">
					<input id="profilesettingsaccountemail" name="profilesettingsaccountemail" class="shadowinset"
							type="text" value="<?=$profile['accountemail']?>">
					<br>
					<small>We use this email address to send account managament information to,<br>
						such as password reset links, or changes to your service.<br>
						This is optional and can be left blank. This is not shown publicly.</small>
				</div>
			</div>
			</fieldset>

			<div class="formbuttonfield">
				<input class="silverbutton" name="btn" type="submit" value="Save Settings">
				<div id="saveprofilesettingssuggestions"></div>
			</div>
			<div style="margin:20px 8px;border-top:solid 1px #9ac;">
				<h3>Other Options</h3>
				<div style="margin:8px 0;">
					<a class="boldlink" href="<?=$urlhome?>/<?=$index_activeusername?>/terms/">View Saved Terms of Use</a>
				</div>
				<div style="margin:8px 0;">
					<a class="boldlink" href="<?=$urlhome?>/<?=$index_activeusername?>/delete/">Delete Account</a>
				</div>
				<div style="margin:8px 0;">
					<a class="boldlink" href="<?=$urlhome?>/<?=$index_activeusername?>/password/">Change Password</a>
				</div>
			</div>
		</form>
<?php
	} /* Only owner can access account settings */
?>
		<br>
		</div>
<?php
} else if ($index_page == 'accountanalytics') {
?>
		<div class="titlepost debugpostclass debugpostborderclass">
			<h2>Account Analytics</h2>
			<div class="postdescriptionpadding">
<?php
	if (!$index_showanalytics) {
?>
				<p>Only the account holders can view analytic data for this account.</p>
<?php
	} else {
?>
				<h3>Statistics for account <a class="boldlink" href="/<?=$index_activeusername?>/"><?=$index_activefullname?></a>.</h3>
				<p>Data combined for all posts owned by this account.</p>
<?php
		/* Show page view count over the year */
		/* Generate an array with days of past year first... */
		$yeartimearray = [];
		$yearviewarray = [];
		$yearpostarray = [];
		$yearreplyarray = [];
		$yearnow = time();
		$yearago = strtotime('-1 year', $yearnow);
		$timepoint = $yearago;
		$maxdays = 366;
		while (($maxdays >= 0) && ($timepoint <= $yearnow)) {
			$dt = date('Y-m-d', $timepoint);
			$labeldt = date('D j M Y', $timepoint);
			$yeartimearray[$dt] = $labeldt;
			$yearviewarray[$dt] = 0;
			$yearpostarray[$dt] = 0;
			$yearreplyarray[$dt] = 0;
			$timepoint += 86400; /* Add total seconds in 24 hours */
			$maxdays--;
		}

		/* ...then populate various arrays with count (ct) values */
		$query = sprintf('select count(*) as ct, date(added) as added from analytics'
				.' where (pageid="%s" or owneraccountid=%d)' /* USERNAME, USERID */
				.' and added >= NOW() - INTERVAL 1 YEAR'
				.' group by date(added) order by added',
				func_php_escape($index_activeusername), $index_activeuserid);
		$result = func_php_query($query);
		$maxct = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			$added = $r['added'];
			if (array_key_exists($added, $yearviewarray)) {
				$ct = (int) $r['ct'];
				$yearviewarray[$added] = $ct;
				if ($ct > $maxct) {
					$maxct = $ct;
				}
			}
		}

		$query = sprintf('select count(*) as ct, date(added) as added from posts'
				.' where posts.accountid=%d' /* USERID */
				.' and added >= NOW() - INTERVAL 1 YEAR'
				.' group by date(added) order by added',
				$index_activeuserid);
		$result = func_php_query($query);
		$maxct = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			$added = $r['added'];
			if (array_key_exists($added, $yearpostarray)) {
				$ct = (int) $r['ct'];
				$yearpostarray[$added] = $ct;
				if ($ct > $maxct) {
					$maxct = $ct;
				}
			}
		}

		$query = sprintf('select count(*) as ct, date(posts.added) as added'
				.' from posts, posts as pp'
				.' where pp.accountid=%d' /* USERID */
				.' and posts.parentid = pp.id and pp.accountid != posts.accountid'
				.' and posts.added >= NOW() - INTERVAL 1 YEAR'
				.' group by added order by added',
				$index_activeuserid);
		$result = func_php_query($query);
		$maxct = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			$added = $r['added'];
			if (array_key_exists($added, $yearreplyarray)) {
				$ct = (int) $r['ct'];
				$yearreplyarray[$added] = $ct;
				if ($ct > $maxct) {
					$maxct = $ct;
				}
			}
		}

		$yeartimelist = implode(',', $yeartimearray);
		$yearviewlist = implode(',', $yearviewarray);
		$yearpostlist = implode(',', $yearpostarray);
		$yearreplylist = implode(',', $yearreplyarray);
		$yearlist = "#8899aa,{$yeartimelist},#33cc33,Posts,{$yearpostlist},#3399cc,Views,{$yearviewlist},#996633,Replies,{$yearreplylist}";
?>
		<div class="yeargraphtitle"><strong>Activity over the Past 12 Months</strong></div>
		<div class="yeargraphmain">
			<canvas id="yeargraphmaincanvas" width="1400" height="150"></canvas>
		</div>
		<script>index_js_linegraphInit('yeargraphmaincanvas', '<?=$yearlist?>');</script>
		<br>
		<div class="analyticsflex">
			<div class="analyticsflexchild">
				<div style="margin-bottom:8px;">
					<strong>Views per Month</strong>
				</div>
<?php
		/* Get months, by year */
		/* But start by generating a blank calendar array */
		$now = time();
		$month = (int) date('n', $now);
		$year = (int) date('Y', $now);
		$year--; /* Jump to this day, previous year */
		$loopct = 100;
		$yearmontharray = [];
		for ($i = 0; $i < 13; $i++) {
			$yearmontharray[$year][$month] = 0;
			$month++;
			if ($month > 12) {
				$month = 1;
				$year++;
			}
		}

		/* Add analytics data to the calendar array */
		$query = sprintf('select month(added) as month, year(added) as year,'
				.' count(*) as ct from analytics'
				.' where (pageid="%s" or owneraccountid=%d)' /* USERNAME, USERID */
				.' and added >= NOW() - INTERVAL 1 YEAR'
				.' group by month(added), year(added)',
				func_php_escape($index_activeusername), $index_activeuserid);
		$result = func_php_query($query);
		$maxct = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			$month = (int) $r['month'];
			$year = (int) $r['year'];
			$ct = (int) $r['ct'];
			if ($ct > $maxct) {
				$maxct = $ct;
			}
			$yearmontharray[$year][$month] = $ct;
		}

		$nowdate = (int) date('j', $yearnow);
		$nowmonth = (int) date('n', $yearnow);
		$nowyear = (int) date('Y', $yearnow);

		foreach ($yearmontharray as $yr => $montharray) {
			foreach ($montharray as $mo => $ct) {
				/* Generate a month array */
				$montharray = [];
				$daysinmonth = cal_days_in_month(CAL_GREGORIAN, $mo, $yr);
				for ($i = 1; $i <= $daysinmonth; $i++) {
					$montharray[$i] = 0;
					/* NULL out any future dates */
					if ((($yr === $nowyear) && ($mo === $nowmonth) && ($i > $nowdate))
							|| (($yr === $nowyear) && ($mo > $nowmonth))
							|| ($yr > $nowyear)) {
						$montharray[$i] = NULL;
					}
				}

				/* Set count values for each day, if any */
				$query = sprintf('select count(*) as ct, day(added) as day from analytics'
						.' where (pageid="%s" or owneraccountid=%d)' /* USERNAME, USERID */
						.' and month(added) = %d and year(added) = %d' /* MONTH, YEAR */
						.' group by day(added)',
						func_php_escape($index_activeusername), $index_activeuserid, $mo, $yr);
				$result = func_php_query($query);
				while ($r = mysqli_fetch_assoc($result)) {
					$day = (int) $r['day'];
					if (array_key_exists($day, $montharray)) {
						$montharray[$day] = (int) $r['ct'];
					}
				}
				$monthlist = implode(',', $montharray);

				$wd = 1;
				if ($maxct) {
					$wd += ceil(($ct / $maxct) * 150);
				}
				$label = date('M Y', mktime(0, 0, 0, $mo, 1, $yr));

				$currentmonth = (int) date('n', $yearnow);
				$currentyear = (int) date('Y', $yearnow);
				if (($mo === $currentmonth) && ($yr === $currentyear)) {
					$label = '<strong>'.$label.'</strong>';
					$barcolour = '6699dd';
				} else {
					$label = '<span class="greytext">'.$label.'</span>';
					$barcolour = 'c0d0e0';
				}
?>
				<div>
					<div style="display:inline-block;width:80px;text-align:right;"><?=$label?></div>
					<img style="margin:0 8px;" src="/img.php?d=120x12-ffffff&s=3366cc,<?=$monthlist?>">
					<img src="\img.php?d=<?=$wd?>x12-<?=$barcolour?>"> <?=$ct?>
				</div>
<?php
			}
		}
?>
				<br>
				<div style="margin-bottom:8px;">
					<strong>Days of the Week</strong>
					<div class="silverpopdownbutton" style="margin-left:1em;width:150px;">
						<div class="silverpopdownbuttonlabel">
							<span class="silverpopdownbuttonlabeltext greytext">Last 12 months</span>
							<img style="opacity:0.2;" src="/img/popupmenudown.png">
						</div>
					</div>
				</div>
<?php
		/* Get day of week (0-based index starting Sunday) */
		$weekdays = [1 => ['day' => 'Sun', 'ct' => 0],
				2 => ['day' => 'Mon', 'ct' => 0],
				3 => ['day' => 'Tue', 'ct' => 0],
				4 => ['day' => 'Wed', 'ct' => 0],
				5 => ['day' => 'Thu', 'ct' => 0],
				6 => ['day' => 'Fri', 'ct' => 0],
				7 => ['day' => 'Sat', 'ct' => 0]
		];

		$query = sprintf('select weekday, count(*) as ct from analytics'
				.' where (pageid="%s" or owneraccountid=%d)' /* USERNAME, USERID */
				.' and added >= NOW() - INTERVAL 1 YEAR'
				.' group by weekday',
				func_php_escape($index_activeusername), $index_activeuserid);
		$result = func_php_query($query);
		$ctmax = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			$weekday = $r['weekday'];
			$ct = $r['ct'];

			$weekdays[$weekday]['ct'] = $ct;
			if ($ct > $ctmax) {
				$ctmax = $ct;
			}
		}

		for ($d = 1; $d <= 7; $d++) {
			/* Create the week array for sparkline data */
			$timepoint = $yearago;
			$weekarray = [];
			$maxweeks = 53;
			/* Move timepoint to match last week day, if needed */
			$datew = date('w', $timepoint) + 1;
			if ($datew !== $d) {
				if ($datew < $d) {
					/* Add 1 week if getting day before previous weekend */
					$datew += 7;
				}
				$timepoint -= (86400 * ($datew - $d)); /* Work this one out! */
			}
			while (($maxweeks >= 0) && ($timepoint <= $yearnow)) {
				$dt = date('Y-m-d', $timepoint);
				$weekarray[$dt] = 0;
				$timepoint += (604800); /* Add total seconds in one week */
				$maxweeks--;
			}

			$query = sprintf('select count(*) as ct, date(added) as added from analytics'
					.' where (pageid="%s" or owneraccountid=%d)' /* USERNAME, USERID */
					.' and weekday=%d' /* WEEKDAY */
					.' and added > NOW() - INTERVAL 1 YEAR'
					.' group by date(added)',
					func_php_escape($index_activeusername), $index_activeuserid, $d);
			$result = func_php_query($query);
			while ($r = mysqli_fetch_assoc($result)) {
				$added = $r['added'];
				if (array_key_exists($added, $weekarray)) {
					$weekarray[$added] = $r['ct'];
				}
			}
			$weeklist = implode(',', $weekarray);

			$day = $weekdays[$d]['day'];
			$ct = $weekdays[$d]['ct'];
			$barwidth = 1;
			if ($ctmax) {
				$barwidth += ceil(($ct * 150) / $ctmax);
			}

			$label = $day;
			$currentday = 1 + (int) date ('w', $yearnow);
			if ($d === $currentday) {
				$label = '<strong>'.$label.'</strong>';
				$barcolour = '6699dd';
			} else {
				$label = '<span class="greytext">'.$label.'</span>';
				$barcolour = 'c0d0e0';
			}
?>
				<div>
					<div style="display:inline-block;width:80px;text-align:right;"><?=$label?></div>
					<img style="margin:0 8px;" src="/img.php?d=120x12-ffffff&s=3366cc,<?=$weeklist?>">
					<img src="\img.php?d=<?=$barwidth?>x12-<?=$barcolour?>"> <?=$ct?>
				</div>
<?php
		}
?>
				<br>
			</div>
			<div class="analyticsflexchild">
				<div style="margin-bottom:8px;">
					<strong>Hours of the Day</strong>
					<div class="silverpopdownbutton" style="margin-left:1em;width:150px;">
						<div class="silverpopdownbuttonlabel">
							<span class="silverpopdownbuttonlabeltext greytext">Last 31 Days</span>
							<img style="opacity:0.2;" src="/img/popupmenudown.png">
						</div>
					</div>
				</div>
<?php
		/* Get Hour of day */
		$hours = [0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0,
				7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0,
				13 => 0, 14 => 0, 15 => 0, 16 => 0, 17 => 0, 18 => 0,
				19 => 0, 20 => 0, 21 => 0, 22 => 0, 23 => 0];

		$query = sprintf('select hour(added) as hour, count(*) as ct from analytics'
				.' where (pageid="%s" or owneraccountid=%d)' /* USERNAME, USERID */
				.' and added > NOW() - INTERVAL 31 DAY'
				.' group by hour',
				func_php_escape($index_activeusername), $index_activeuserid);
		$result = func_php_query($query);
		$ctmax = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			$hour = (int) $r['hour'];
			$ct = (int) $r['ct'];

			$hours[$hour] = $ct;
			if ($ct > $ctmax) {
				$ctmax = $ct;
			}
		}

		for ($h = 0; $h <= 23; $h++) {
			/* Generate hour array for sparkline */
			$hourarray = [];
			$maxdays = 32;
			$timepoint = strtotime('-31 days', $yearnow);
			while (($maxdays > 0) && ($timepoint <= $yearnow)) {
				$dt = date('Y-m-d', $timepoint);
				$hourarray[$dt] = 0;
				$timepoint += 86400; /* Add total seconds in 24 hours */
				$maxdays--;
			}

			$query = sprintf('select count(*) as ct, date(added) as added from analytics'
					.' where (pageid="%s" or owneraccountid=%d)' /* USERNAME, USERID */
					.' and hour(added)=%d'
					.' and added > NOW() - INTERVAL 31 DAY'.' group by date(added)',
					func_php_escape($index_activeusername), $index_activeuserid, $h);
			$result = func_php_query($query);
			while ($r = mysqli_fetch_assoc($result)) {
				$added = $r['added'];
				if (array_key_exists($added, $hourarray)) {
					$hourarray[$added] = (int) $r['ct'];
				}
			}
			$hourlist = implode(',', $hourarray);

			$ct = $hours[$h];
			$barwidth = 1;
			if ($ctmax) {
				$barwidth += ceil(($ct * 200) / $ctmax);
			}

			$label = $h;
			if ($h === 0) {
				$label = '12am';
			} else if ($h === 12) {
				$label = '12pm';
			} else if ($h >= 13) {
				$label = ($h - 12).'pm';
			} else {
				$label = $h.'am';
			}

			$currenthour = (int) date('G', $yearnow);
			if ($h === $currenthour) {
				$label = '<strong>'.$label.'</strong>';
				$barcolour = '6699dd';
			} else {
				$label = '<span class="greytext">'.$label.'</span>';
				$barcolour = 'c0d0e0';
			}
?>
				<div>
					<div style="display:inline-block;width:80px;text-align:right;"><?=$label?></div>
					<img style="margin:0 8px;" src="/img.php?d=120x12-ffffff&s=3366cc,<?=$hourlist?>">
					<img src="\img.php?d=<?=$barwidth?>x12-<?=$barcolour?>"> <?=$ct?>
				</div>
<?php
		}
?>
				<br>
			</div>
		</div>
				<p>Last 100 rows of analytic data for account <a class="boldlink" href="/<?=$index_activeusername?>/"><?=$index_activefullname?></a>.</p>
<?php
		$query = sprintf('select * from analytics where pageid="%s"' /* ACCTID */
				.' order by added desc limit 100;',
				func_php_escape($index_activeusername));
		$result = func_php_query($query);
		if (!mysqli_num_rows($result)) {
			echo '<p>No data is currently available.</p>';
		} else {
?>
				<table class="greytable">
				<tr>
				<th>Added</th>
				<th>Updated</th>
				<th><span class="tooltiphost tooltiphostdotted">Day<span class="tooltiptext">Week day starts<br>with Sunday as 1</span></span></th>
				<th>Country</th>
				<th>Operating System</th>
				<th>Browser</th>
				<th>Width</th>
				<th>Action</th>
				<th>Authorised</th>
				</tr>
				<tr>
				<th colspan="9"> ↳ Referrer</th>
				</tr>
<?php
			$cellstyle = '';
			while ($r = mysqli_fetch_assoc($result)) {
				/* Alternate between light and dark cell bg colours */
				$cellstyle = ($cellstyle ? '' : 'background:#f8f8f8;');
?>
				<tr>
				<td style="<?=$cellstyle?>"><?=$r['added']?></td>
				<td style="<?=$cellstyle?>"><?=$r['updated']?></td>
				<td style="<?=$cellstyle?>"><?=$r['weekday']?></td>
				<td style="<?=$cellstyle?>"><?=$r['country']?></td>
				<td style="<?=$cellstyle?>"><?=$r['operatingsystem']?> <?=$r['operatingsystemversion']?></td>
				<td style="<?=$cellstyle?>"><?=$r['browser']?> <?=$r['browserversion']?></td>
				<td style="<?=$cellstyle?>"><?=$r['windowwidth']?></td>
				<td style="<?=$cellstyle?>"><?=$r['action']?></td>
				<td style="<?=$cellstyle?>"><?=$r['authorised']?></td>
				</tr>
				<tr>
				<td style="<?=$cellstyle?>" colspan="9"> ↳ <?=$r['referrer']?></td>
				</tr>
<?php
			}
?>
				</table>
<?php
		}
?>
				<br>
				<p>Top 100 active posts for this account.</p>
<?php
		$query = sprintf('select count(*) as hitcount, analytics.pageid,'
				.' MIN(analytics.added) as firsthit, MAX(analytics.added) as lasthit,'
				.' posts.searchcache'
				.' from analytics left join posts on posts.id = analytics.pageid'
				.' where analytics.owneraccountid = %d' /* ACCTID */
				.' group by analytics.pageid order by hitcount desc limit 100;',
				$index_activeuserid);
		$result = func_php_query($query);
		if (!mysqli_num_rows($result)) {
			echo '<p>No data is currently available.</p>';
		} else {
?>
				<table class="greytable">
				<tr>
				<th>Hits</th>
				<th>First Hit</th>
				<th>Last Hit</th>
				<th></th>
				<th>Summary</th>
				</tr>
<?php
			$cellstyle = '';
			while ($r = mysqli_fetch_assoc($result)) {
				$firsthit = $r['firsthit'];
				$lasthit = $r['lasthit'];
				$firsthittime = strtotime($r['firsthit']);
				$lasthittime = strtotime($r['lasthit']);
				$searchcache = $r['searchcache'];
				if ($shortdesc) {
					$shortdesc = trim(substr($searchcache, 0, 40));
					if (strlen($searchcache) > 40) {
						$shortdesc .= '...';
					}
				} else {
					$shortdesc = '(Untitled)';
				}

				/* Alternate between light and dark cell bg colours */
				$cellstyle = ($cellstyle ? '' : 'background:#f8f8f8;');
?>
				<tr>
				<td style="<?=$cellstyle?>"><?=$r['hitcount']?></td>
				<td style="<?=$cellstyle?>"><span class="tooltiphost tooltiphostdotted">
						<?=func_php_fuzzyTimeAgo($firsthit, true, true)?>
						<span class="tooltiptext tooltiptextleft">
							<?=func_php_fuzzyTimeAgo($firsthit, false, true)?><br>
							<?=date('l g:i a\<\b\r\>jS F Y', $firsthittime)?>
						</span></span></td>
				<td style="<?=$cellstyle?>"><span class="tooltiphost tooltiphostdotted"><?=func_php_fuzzyTimeAgo($lasthit, true, true)?>
						<span class="tooltiptext tooltiptextleft">
							<?=func_php_fuzzyTimeAgo($lasthit, false, true)?><br>
							<?=date('l g:i a\<\b\r\>jS F Y', $lasthittime)?>
						</span></span></td>
				<td style="<?=$cellstyle?>"><a href="/<?=$r['pageid']?>/analytics" class="greytablelink">Details</a> &nbsp; </td>
				<td style="<?=$cellstyle?>"><a href="/<?=$r['pageid']?>" class="greytablelink" target="_blank">/<?=$r['pageid']?>: <?=$shortdesc?></a></td>
				</tr>
<?php
			}
?>
				</table>
<?php
		} /* END if mysqli_num_rows() */
	}
?>
				<br>
			</div>
		</div>
<?php
} else if ($index_page == 'accountterms') {
?>
		<div class="titlepost debugpostclass debugpostborderclass" style="padding:4px;">
<?php
	$query = sprintf('select termsofuse, termsofusedate from accounts where accounts.id = %d limit 1;', $index_activeuserid);
	$result = func_php_query($query);
	if (mysqli_num_rows($result) == 1) {
		$r = mysqli_fetch_assoc($result);
		$timestamp = strtotime($r['termsofusedate']);
		$termsofusedate = date('jS F Y, l g:i a', $timestamp);
?>
		<p style="color:#696;">The following Terms of Use were saved with this account on<br><strong><?=$termsofusedate?></strong>.</p>
		<pre style="white-space:pre-wrap; font:inherit; max-width:600px; margin-left:auto; margin-right:auto;"><?=$r['termsofuse']?></pre>
<?php
	} else {
		echo 'No terms of use recorded. Using <a href="/terms.php">current terms</a> instead.';
	}
?>
		</div>
<?php
} else if ($index_page == 'changepassword') {
	/* Check for any form errors, and hilite them in the form below. */
	$currentpassworderror = False;
	$newpassworderror = False;

	if (array_key_exists('currentpassword', $index_errorlist))
		$currentpassworderror = True;

	if (array_key_exists('newpassword', $index_errorlist))
		$newpassworderror = True;
?>
		<div class="titlepost debugpostclass debugpostborderclass" style="padding:4px;">
			<h2>Change Your Password</h2>
<?php
		if (!$index_showsettings) {
			echo '<br><p class="centered">Only the account owner can access this page.</p>';
		} else {
?>
			<form action="<?=$urlhome?>/<?=$index_activeusername?>/password/" method="post" id="profilechangepasswordform"
					onSubmit="return index_js_checkChangePassword();">
				<fieldset class="formfield">
<?php
			/* The reset link id can be used as the current password */
			if ($index_passwordresetid) {
?>
					<input type="hidden" id="resetlinkid" name="resetlinkid" value="<?=$index_passwordresetid?>">
<?php
			} else {
?>
				<div class="formdiv">
					<label for="newpassword">Current Password:</label>
					<div class="forminput">
						<input id="profilepassword" class="shadowinset" name="profilepassword" type="password">
					</div>
				</div>
<?php
			}
?>
				<div class="formdiv">
					<label for="newpassword">New Password:</label>
					<div class="forminput">
						<input id="newpassword" class="shadowinset" maxlength="255" name="newpassword" type="password">
						<br>
						<small>The password must be at least 6 characters in length and is case sensitive.</small>
					</div>
				</div>
				<div class="formdiv">
					<label for="newpasswordcheck">Retype new password:</label>
					<div class="forminput">
						<input id="newpasswordcheck" class="shadowinset" maxlength="255" name="newpasswordcheck" type="password">
						<br>
						<small>Enter the new password again to check for any errors.</small>
					</div>
				</div>
				</fieldset>
				<div class="formbuttonfield">
					<input class="silverbutton" name="btn" type="submit" value="Save New Password">
					<span id="savepasswordsuggestions"></span>
				</div>
				<input name="token" type="hidden" value="<?=$_SESSION['token']?>">
			</form>
<?php
		} /* Only owner can access account settings */
?>
		</div>
<?php
} else if ($index_page == 'deleteaccount') {
	/* Check for any form errors, and hilite them in the form below. */
	$currentpassworderror = False;
	$newpassworderror = False;

	if (array_key_exists('currentpassword', $index_errorlist))
		$currentpassworderror = True;

	if (array_key_exists('newpassword', $index_errorlist))
		$newpassworderror = True;
?>
		<div class="titlepost debugpostclass debugpostborderclass" style="padding:4px;">
		<h2>Delete Your Account</h2>
<?php
		if (!$index_showsettings) {
			echo '<br><p class="centered">Only the account owner can access this page.</p>';
		} else {
?>
		<p>Deleting your account is permanent and is done immediately.</p>
		<p>Once started, the process may take a few minutes depending on the amount of information and files linked to this account.</p>
		<p>Before deletion you should save a copy of everything associated with this account.</p>
		<p>To begin the deletion process, enter your login details below and click &quot;<strong>Delete Your Account</strong>&quot;.</p>
		<form action="<?=$urlhome?>/<?=$index_activeusername?>/delete/" method="post" id="deleteaccountform"
				onSubmit="return index_js_deleteAccount();">
			<fieldset class="formfield">
			<legend>Current Login Details</legend>
			<div class="formdiv">
				<label for="profileusername">Current Username</label>
				<div class="forminput">
					<input id="profileusername" class="forminput shadowinset" name="profileusername" type="text">
				</div>
			</div>
			<div class="formdiv">
				<label for="profilepassword">Current Password</label>
				<div class="forminput">
					<input id="profilepassword" class="forminput shadowinset" name="profilepassword" type="password">
				</div>
			</div>
			</fieldset>
			<div class="formbuttonfield">
				<input class="silverbutton" name="btn" type="submit" value="Delete Account Data">
				<span id="deleteaccountsuggestions"></span>
			</div>
			<input name="token" type="hidden" value="<?=$_SESSION['token']?>">
		</form>
<?php
		} /* Only owner can access account settings */
?>
		</div>
<?php
} else if ($index_page == 'advancedpostsearch') {
?>
		<div class="titlepost debugpostclass debugpostborderclass">
		Advanced Post search
		</div>
<?php
} else if ($index_page == 'advancedprofilesearch') {
?>
		<div class="titlepost debugpostclass debugpostborderclass">
			<h2>Advanced Profile Search</h2>
			<form id="advancedprofilesearchform" method="post" onsubmit="return index_js_advancedProfileSearch(this);">
				<table class="profilesearchtable">
				<tr>
					<th>Username</th>
					<td><input type="text" class="shadowinset" name="profileusername"></td>
					<th>Name</th>
					<td><input type="text" class="shadowinset" name="profilefullname"></td>
				</tr>
				<tr>
					<th>Town/Suburb</th>
					<td colspan="5"><input type="text" class="shadowinset" name="profiletown"></td>
				</tr>
				<tr>
					<th>Country</th>
					<td colspan="5"><input type="text" class="shadowinset" name="profilecountry"></td>
				</tr>
				</table>
				<div style="padding:4px;">
					Match
					<input id="profilematchall" type="radio" name="profilematch" value="all" checked><label for="profilematchall">all fields</label>
					| <input id="profilematchany" type="radio" name="profilematch" value="any"><label for="profilematchany">any field</label>
					<input type="submit" class="silverbutton" style="position:absolute;bottom:2px;right:2px;" value="Search Profiles">
				</div>
			</form>
		</div>
		<div class="titlepost debugpostclass debugpostborderclass">
			<div id="advancedprofilesearchresultstoolbar">
				<span id="advancedprofilesearchstatus">Enter search terms to begin...</span>
			</div>
			<div id="advancedprofilesearchresults">
				<table id="profilesearchresultstable" class="greytable">
					<tr>
						<th>Name</th>
						<th>Username</th>
						<th colspan="2">Location</th>
						<th>Country</th>
					</tr>
				</table>
			</div>
		</div>
<?php
} else if ($index_page == 'landingpage') {
?>
		<div class="debugpostclass landingpagepost">
			<div class="landingpagewordartlarge">Versatile web pages.</div>
			<div class="landingpagewordart">Take&nbsp;notes, write&nbsp;lists.</div>
			<div class="landingpagewordart">Share&nbsp;them and&nbsp;collaborate.</div>
			<div class="landingpagewordart">Then turn it in to a blog or web site.</div>
			<p class="landingpagep">On Mirror Island you can make your posts do many things. Build web pages, organise events, be in a group, or let people know where you are or what you are doing.</p>
			<div class="landingpagebuttondiv">
				<a class="landingpagebutton" href="/quickstart.php">View Quickstart</a>
			</div>
		</div>
		<div class="landingpagefullwidth">
			<div class="landingpagediv">
				<img src="/img/glyph-fastedit.svg">
				<h3>Fast From The Start</h3>
				<div>Everything has been designed to get your notes down or message out as fast as possible.</div>
			</div>
			<div class="landingpagediv">
				<img src="/img/glyph-image.svg">
				<h3>Use More Than Words</h3>
				<div>Include images and attachments when making posts to share photos and files. Attachments are also sent by email so your message always stays in one piece.</div>
			</div>
			<div class="landingpagediv">
				<img src="/img/glyph-email.svg">
				<h3>Compatible with Email</h3>
				<div>Send and receive emails directly from your posts so you can keep in contact with anyone, even if they're not members of this website.</div>
			</div>
			<div class="landingpagediv">
				<img src="/img/glyph-html.svg">
				<h3>Markdown Support</h3>
				<div>Write notes and documents in plain text using a simple, easy to use system and publish them in formatted HTML.</div>
			</div>
			<div class="landingpagediv">
				<img src="/img/glyph-audience.svg">
				<h3>Choose Your Audience</h3>
				<div>We put the controls in your hands so you can keep the conversation on track. Post to others in groups, in private, or out in the open - it&rsquo;s up to you.</div>
			</div>
			<div class="landingpagediv">
				<img src="/img/glyph-devices.svg">
				<h3>Access Anywhere</h3>
				<div>All files and posts can be accessed on any device with network access, so you can take your notes, lists, photos and files wherever you go.</div>
			</div>
			<div class="landingpagediv">
				<img src="/img/glyph-shop.svg">
				<h3>Use It for Any Purpose</h3>
				<div>This project is available for any use including personal, commercial and government use.</div>
			</div>
			<div class="landingpagediv">
				<img src="/img/glyph-code.svg">
				<h3>100% Free Software</h3>
				<div>Also called &quot;Open Source&quot;, the source code for this project is publicly available and licensed under the Affero GPL version 3.</div>
			</div>
		</div>
<?php
} else if ($index_page == 'analytics') {
?>
		<div class="titlepost debugpostclass debugpostborderclass">
			<h2>Post Analytics</h2>
			<div class="postdescriptionpadding">
<?php
	if (!$index_showanalytics) {
		echo '<br><p>Data is only available to the owner, editors, or moderators of this post.</p><br>';
	} else {
		$query = sprintf('select searchcache from posts where id = %d limit 1;', /* POSTID */
				$index_page_id);
		$result = func_php_query($query);
		$r = mysqli_fetch_assoc($result);
		$searchcache = $r['searchcache'];
		if ($searchcache) {
			$shortdesc = trim(substr($searchcache, 0, 40));
			if (strlen($searchcache) > 40)
				$shortdesc .= '...';
		}

		$link = "<a href='/{$index_page_id}' class='boldlink'>{$shortdesc}</a>";
?>
				<h3>Statistics for post &quot;<?=$link?>&quot;</h3>
<?php
		/* Show page view count over the year */
		/* Generate an array with days of past year first... */
		$yeartimearray = [];
		$yearviewarray = [];
		$yearreplyarray = [];
		$yearnow = time();
		$yearago = strtotime('-1 year', $yearnow);
		$timepoint = $yearago;
		$maxdays = 366;
		while (($maxdays >= 0) && ($timepoint <= $yearnow)) {
			$dt = date('Y-m-d', $timepoint);
			$labeldt = date('D j M Y', $timepoint);
			$yeartimearray[$dt] = $labeldt;
			$yearviewarray[$dt] = 0;
			$yearreplyarray[$dt] = 0;
			$timepoint += 86400; /* Add total seconds in 24 hours */
			$maxdays--;
		}

		/* ...then populate year array with count (ct) values */
		$query = sprintf('select count(*) as ct, date(added) as added from analytics'
				.' where pageid=%d' /* PAGEID */
				.' and added >= NOW() - INTERVAL 1 YEAR'
				.' group by date(added) order by added',
				$index_page_id);
		$result = func_php_query($query);
		$maxct = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			$added = $r['added'];
			if (array_key_exists($added, $yearviewarray)) {
				$ct = (int) $r['ct'];
				$yearviewarray[$added] = $ct;
				if ($ct > $maxct) {
					$maxct = $ct;
				}
			}
		}

		$query = sprintf('select count(*) as ct, date(posts.added) as added'
				.' from posts, posts as pp'
				.' where pp.id = %d' /* POSTID */
				.' and posts.parentid = pp.id and pp.accountid != posts.accountid'
				.' and posts.added >= NOW() - INTERVAL 1 YEAR'
				.' group by added order by added',
				$index_page_id);
		$result = func_php_query($query);
		$maxct = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			$added = $r['added'];
			if (array_key_exists($added, $yearreplyarray)) {
				$ct = (int) $r['ct'];
				$yearreplyarray[$added] = $ct;
				if ($ct > $maxct) {
					$maxct = $ct;
				}
			}
		}

		$yeartimelist = implode(',', $yeartimearray);
		$yearviewlist = implode(',', $yearviewarray);
		$yearreplylist = implode(',', $yearreplyarray);
		$yearlist = "#8899aa,{$yeartimelist},#3399cc,Views,{$yearviewlist},#996633,Replies,{$yearreplylist}";
?>
		<div class="yeargraphtitle"><strong>Views over the Past 12 Months</strong></div>
		<div class="yeargraphmain">
			<canvas id="yeargraphmaincanvas" width="1400" height="150"></canvas>
		</div>
		<script>index_js_linegraphInit('yeargraphmaincanvas', '<?=$yearlist?>');</script>
		<br>
		<div class="analyticsflex">
			<div class="analyticsflexchild">
				<div style="margin-bottom:8px;">
					<strong>Views per Month</strong>
				</div>
<?php
		/* Get months, by year */
		/* But start by generating a blank calendar array */
		$now = time();
		$month = (int) date('n', $now);
		$year = (int) date('Y', $now);
		$year--; /* Jump to this day, previous year */
		$loopct = 100;
		$yearmontharray = [];
		for ($i = 0; $i < 13; $i++) {
			$yearmontharray[$year][$month] = 0;
			$month++;
			if ($month > 12) {
				$month = 1;
				$year++;
			}
		}

		/* Add analytics data to the calendar array */
		$query = sprintf('select month(added) as month, year(added) as year,'
				.' count(*) as ct from analytics'
				.' where pageid=%d' /* PAGEID */
				.' and added >= NOW() - INTERVAL 1 YEAR'
				.' group by month(added), year(added)',
				$index_page_id);
		$result = func_php_query($query);
		$maxct = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			$month = (int) $r['month'];
			$year = (int) $r['year'];
			$ct = (int) $r['ct'];
			if ($ct > $maxct) {
				$maxct = $ct;
			}
			$yearmontharray[$year][$month] = $ct;
		}

		$nowdate = (int) date('j', $yearnow);
		$nowmonth = (int) date('n', $yearnow);
		$nowyear = (int) date('Y', $yearnow);

		foreach ($yearmontharray as $yr => $montharray) {
			foreach ($montharray as $mo => $ct) {
				/* Generate a month array */
				$montharray = [];
				$daysinmonth = cal_days_in_month(CAL_GREGORIAN, $mo, $yr);
				for ($i = 1; $i <= $daysinmonth; $i++) {
					$montharray[$i] = 0;
					/* NULL out any future dates */
					if ((($yr === $nowyear) && ($mo === $nowmonth) && ($i > $nowdate))
							|| (($yr === $nowyear) && ($mo > $nowmonth))
							|| ($yr > $nowyear)) {
						$montharray[$i] = NULL;
					}
				}

				/* Set count values for each day, if any */
				$query = sprintf('select count(*) as ct, day(added) as day from analytics'
						.' where pageid=%d' /* PAGEID */
						.' and month(added) = %d and year(added) = %d' /* MONTH, YEAR */
						.' group by day(added)',
						$index_page_id, $mo, $yr);
				$result = func_php_query($query);
				while ($r = mysqli_fetch_assoc($result)) {
					$day = (int) $r['day'];
					if (array_key_exists($day, $montharray)) {
						$montharray[$day] = (int) $r['ct'];
					}
				}
				$monthlist = implode(',', $montharray);

				$wd = 1;
				if ($maxct) {
					$wd += ceil(($ct / $maxct) * 150);
				}
				$label = date('M Y', mktime(0, 0, 0, $mo, 1, $yr));

				$currentmonth = (int) date('n', $yearnow);
				$currentyear = (int) date('Y', $yearnow);
				if (($mo === $currentmonth) && ($yr === $currentyear)) {
					$label = '<strong>'.$label.'</strong>';
					$barcolour = '6699dd';
				} else {
					$label = '<span class="greytext">'.$label.'</span>';
					$barcolour = 'c0d0e0';
				}
?>
				<div>
					<div style="display:inline-block;width:80px;text-align:right;"><?=$label?></div>
					<img style="margin:0 8px;" src="/img.php?d=120x12-ffffff&s=3366cc,<?=$monthlist?>">
					<img src="\img.php?d=<?=$wd?>x12-<?=$barcolour?>"> <?=$ct?>
				</div>
<?php
			}
		}
?>
				<br>
				<div style="margin-bottom:8px;">
					<strong>Days of the Week</strong>
					<div class="silverpopdownbutton" style="margin-left:1em;width:150px;">
						<div class="silverpopdownbuttonlabel">
							<span class="silverpopdownbuttonlabeltext greytext">Last 12 months</span>
							<img style="opacity:0.2;" src="/img/popupmenudown.png">
						</div>
					</div>
				</div>
<?php
		/* Get day of week (0-based index starting Sunday) */
		$weekdays = [1 => ['day' => 'Sun', 'ct' => 0],
				2 => ['day' => 'Mon', 'ct' => 0],
				3 => ['day' => 'Tue', 'ct' => 0],
				4 => ['day' => 'Wed', 'ct' => 0],
				5 => ['day' => 'Thu', 'ct' => 0],
				6 => ['day' => 'Fri', 'ct' => 0],
				7 => ['day' => 'Sat', 'ct' => 0]
		];

		$query = sprintf('select weekday, count(*) as ct from analytics'
				.' where pageid=%d' /* PAGEID */
				.' and added >= NOW() - INTERVAL 1 YEAR'
				.' group by weekday',
				$index_page_id);
		$result = func_php_query($query);
		$ctmax = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			$weekday = $r['weekday'];
			$ct = $r['ct'];

			$weekdays[$weekday]['ct'] = $ct;
			if ($ct > $ctmax) {
				$ctmax = $ct;
			}
		}

		for ($d = 1; $d <= 7; $d++) {
			/* Create the week array for sparkline data */
			$timepoint = $yearago;
			$weekarray = [];
			$maxweeks = 53;
			/* Move timepoint to match last week day, if needed */
			$datew = date('w', $timepoint) + 1;
			if ($datew !== $d) {
				if ($datew < $d) {
					/* Add 1 week if getting day before previous weekend */
					$datew += 7;
				}
				$timepoint -= (86400 * ($datew - $d)); /* Work this one out! */
			}
			while (($maxweeks >= 0) && ($timepoint <= $yearnow)) {
				$dt = date('Y-m-d', $timepoint);
				$weekarray[$dt] = 0;
				$timepoint += (604800); /* Add total seconds in one week */
				$maxweeks--;
			}

			$query = sprintf('select count(*) as ct, date(added) as added from analytics'
					.' where pageid=%d' /* PAGEID */
					.' and weekday=%d' /* WEEKDAY */
					.' and added > NOW() - INTERVAL 1 YEAR'
					.' group by date(added)',
					$index_page_id, $d);
			$result = func_php_query($query);
			while ($r = mysqli_fetch_assoc($result)) {
				$added = $r['added'];
				if (array_key_exists($added, $weekarray)) {
					$weekarray[$added] = $r['ct'];
				}
			}
			$weeklist = implode(',', $weekarray);

			$day = $weekdays[$d]['day'];
			$ct = $weekdays[$d]['ct'];
			$barwidth = 1;
			if ($ctmax) {
				$barwidth += ceil(($ct * 150) / $ctmax);
			}

			$label = $day;
			$currentday = 1 + (int) date ('w', $yearnow);
			if ($d === $currentday) {
				$label = '<strong>'.$label.'</strong>';
				$barcolour = '6699dd';
			} else {
				$label = '<span class="greytext">'.$label.'</span>';
				$barcolour = 'c0d0e0';
			}
?>
				<div>
					<div style="display:inline-block;width:80px;text-align:right;"><?=$label?></div>
					<img style="margin:0 8px;" src="/img.php?d=120x12-ffffff&s=3366cc,<?=$weeklist?>">
					<img src="\img.php?d=<?=$barwidth?>x12-<?=$barcolour?>"> <?=$ct?>
				</div>
<?php
		}
?>
				<br>
			</div>
			<div class="analyticsflexchild">
				<div style="margin-bottom:8px;">
					<strong>Hours of the Day</strong>
					<div class="silverpopdownbutton" style="margin-left:1em;width:150px;">
						<div class="silverpopdownbuttonlabel">
							<span class="silverpopdownbuttonlabeltext greytext">Last 31 Days</span>
							<img style="opacity:0.2;" src="/img/popupmenudown.png">
						</div>
					</div>
				</div>
<?php
		/* Get Hour of day */
		$hours = [0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0,
				7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0,
				13 => 0, 14 => 0, 15 => 0, 16 => 0, 17 => 0, 18 => 0,
				19 => 0, 20 => 0, 21 => 0, 22 => 0, 23 => 0];

		$query = sprintf('select hour(added) as hour, count(*) as ct from analytics'
				.' where pageid=%d' /* PAGEID */
				.' and added > NOW() - INTERVAL 31 DAY'
				.' group by hour',
				$index_page_id);
		$result = func_php_query($query);
		$ctmax = 0;
		while ($r = mysqli_fetch_assoc($result)) {
			$hour = (int) $r['hour'];
			$ct = (int) $r['ct'];

			$hours[$hour] = $ct;
			if ($ct > $ctmax) {
				$ctmax = $ct;
			}
		}

		for ($h = 0; $h <= 23; $h++) {
			/* Generate hour array for sparkline */
			$hourarray = [];
			$maxdays = 32;
			$timepoint = strtotime('-31 days', $yearnow);
			while (($maxdays > 0) && ($timepoint <= $yearnow)) {
				$dt = date('Y-m-d', $timepoint);
				$hourarray[$dt] = 0;
				$timepoint += 86400; /* Add total seconds in 24 hours */
				$maxdays--;
			}

			$query = sprintf('select count(*) as ct, date(added) as added from analytics'
					.' where pageid=%d' /* PAGEID */
					.' and hour(added)=%d'
					.' and added > NOW() - INTERVAL 31 DAY'.' group by date(added)',
					$index_page_id, $h);
			$result = func_php_query($query);
			while ($r = mysqli_fetch_assoc($result)) {
				$added = $r['added'];
				if (array_key_exists($added, $hourarray)) {
					$hourarray[$added] = (int) $r['ct'];
				}
			}
			$hourlist = implode(',', $hourarray);

			$ct = $hours[$h];
			$barwidth = 1;
			if ($ctmax) {
				$barwidth += ceil(($ct * 200) / $ctmax);
			}

			$label = $h;
			if ($h === 0) {
				$label = '12am';
			} else if ($h === 12) {
				$label = '12pm';
			} else if ($h >= 13) {
				$label = ($h - 12).'pm';
			} else {
				$label = $h.'am';
			}

			$currenthour = (int) date('G', $yearnow);
			if ($h === $currenthour) {
				$label = '<strong>'.$label.'</strong>';
				$barcolour = '6699dd';
			} else {
				$label = '<span class="greytext">'.$label.'</span>';
				$barcolour = 'c0d0e0';
			}
?>
				<div>
					<div style="display:inline-block;width:80px;text-align:right;"><?=$label?></div>
					<img style="margin:0 8px;" src="/img.php?d=120x12-ffffff&s=3366cc,<?=$hourlist?>">
					<img src="\img.php?d=<?=$barwidth?>x12-<?=$barcolour?>"> <?=$ct?>
				</div>
<?php
		}
?>
				<br>
			</div>
		</div>
				<p>Last 100 rows of analytic data for this page:
<?php
		$query = sprintf('select * from analytics where pageid= %d' /* POSTID */
				.' order by added desc limit 100;',
				$index_page_id);
		$result = func_php_query($query);
		if (!mysqli_num_rows($result)) {
			echo '<p>No data is currently available.</p>';
		} else {
?>
				</p>
				<table class="greytable">
				<tr>
				<th>Added</th>
				<th>Updated</th>
				<th><span class="tooltiphost tooltiphostdotted">Day<span class="tooltiptext">Week day starts<br>with Sunday as 1</span></span></th>
				<th>Country</th>
				<th>Operating System</th>
				<th>Browser</th>
				<th>Width</th>
				<th>Action</th>
				<th>Authorised</th>
				</tr>
				<tr>
				<th colspan="9"> ↳ Referrer</th>
				</tr>
<?php
			$cellstyle = '';
			while ($r = mysqli_fetch_assoc($result)) {
				/* Alternate between light and dark cell bg colours */
				if ($cellstyle) {
					$cellstyle = '';
				} else {
					$cellstyle = 'background:#f8f8f8;';
				}
?>
				<tr>
				<td style="<?=$cellstyle?>"><?=$r['added']?></td>
				<td style="<?=$cellstyle?>"><?=$r['updated']?></td>
				<td style="<?=$cellstyle?>"><?=$r['weekday']?></td>
				<td style="<?=$cellstyle?>"><?=$r['country']?></td>
				<td style="<?=$cellstyle?>"><?=$r['operatingsystem']?> <?=$r['operatingsystemversion']?></td>
				<td style="<?=$cellstyle?>"><?=$r['browser']?> <?=$r['browserversion']?></td>
				<td style="<?=$cellstyle?>"><?=$r['windowwidth']?></td>
				<td style="<?=$cellstyle?>"><?=$r['action']?></td>
				<td style="<?=$cellstyle?>"><?=$r['authorised']?></td>
				</tr>
				<tr>
				<td style="<?=$cellstyle?>" colspan="9"> ↳ <?=$r['referrer']?></td>
				</tr>
<?php
			}
?>
				</table>
				<br>
<?php
		}
	}
?>
			</div>
			<!-- NEXTINLINE
			<div style="padding:4px;">
				<p>Post Analytics for <?=$index_page_id?></p>
				<canvas id="dayofweek" width="800" height="200"></canvas>
			</div>
			NEXTINLINE -->
		</div>
		<!-- NEXTINLINE
		<script>
		let ctx = $('dayofweek').getContext('2d');
		let ctxWidth = $('dayofweek').width;
		let ctxHeight = $('dayofweek').height;
		ctx.save();
		ctx.clearRect(0, 0, ctxWidth, ctxHeight);
		ctx.fillStyle = '#f00';
		ctx.rect(0, 10, 20, 30);
		ctx.fill();
		</script>
		NEXTINLINE -->
<?php
} else if ($index_page == 'signin') {
	if ($index_activeuserid) {
?>
		<div id="post0" class="debugpostclass titlepost debugpostborderclass" permissions="0000000">
			<div class="postdescription postdescriptionpadding" id="postdescription0">
				<h2>Already Signed In</h2>
				<p>To create a new account or sign in as a different user, you will need to sign out first.</p>
				<p>If you need to sign out:
					<ol style="margin-left:1em;">
					<li>Click on the username at the top-right of this screen,</li>
					<li>then click &quot;Sign Out&quot; at the bottom of the popup menu.</li>
					</ol>
				</p>
			</div>
		</div>
<?php
	} else {
		require('signin.php');
	}
} else if ($index_page == 'pricing') {
	require('pricing.php');
} else if ($index_page == 'billing') {
	require('billing.php');
} /* End div#titleblock */


/* TODO: MERGESELECTPOST This is a copy of the code used for the "home"
 * section. It needs to be broken off as a function to simplify things
 * --especially permissions! */
if ($index_page == 'page') {
	if ($index_page_id) {
		$query = sprintf('/* '.__LINE__.' */ select %d as activeuserid, posts.*, parentpost.accountid as parentaccountid,'
				.' parentpost.replypermissions as parentreplypermissions, parentpost.approvepermissions as parentapprovepermissions,'
				.' accounts.id as authorid, accounts.username as authorusername, accounts.fullname as authorfullname,'
				.' concat(posts.viewpermissions, posts.editpermissions, posts.replypermissions, posts.approvepermissions,'
				.' posts.moderatepermissions, posts.tagpermissions) as permissions, posts.agerating,'
				.' (select group_concat(distinct concat_ws("", accounts.username, recipients.emailusername,'
				.'		if (recipients.emailserver is not NULL, "@", ""),'
				.' 		recipients.emailserver) separator " ")'
				.' 	from recipients left join accounts on accounts.id = recipients.recipientaccountid'
				.' 	where recipients.tablename = "posts"'
				.'	and recipients.senderaccountid != recipients.recipientaccountid'
				.' 	and recipients.tableid = posts.id) as recipients,'
				.' (select count(*) from members where members.postid = posts.id) as membercount,'
				.' (select count(*) from members where members.postid = posts.id and trusted = 1) as trustedmembercount,'
				.' ((select count(*) from members left join membertree on members.postid = membertree.parentid'
				.' 	where members.accountid = %d and membertree.postid = posts.id' /* USERID */
				.' 	and members.trusted = 1 and members.accountid != posts.accountid)'
				.' 	+ (select count(*) from members where members.accountid = %d' /* USERID */
				.' 	and members.postid = posts.id and members.trusted = 1 and members.accountid != posts.accountid))'
				.' 	as activeuseristrustedmember,'
				.' ((select count(*) from members left join membertree on members.postid = membertree.parentid'
				.' 	where members.accountid = posts.accountid and membertree.postid = posts.id'
				.' 	and members.trusted = 1 /* and members.accountid != posts.accountid */ )'
				.' 	+ (select count(*) from members where members.accountid = posts.accountid'
				.' 	and members.postid = posts.id and members.trusted = 1 /* and members.accountid != posts.accountid */ ))'
				.' 	as postuseristrustedmember,'
				.' approvals.id as postapprovalid, approvals.trusted as postapprovaltrusted,'
				.' approvals.hidden as postapprovalhidden, approvals.added as postapprovaladded,'
				.' (select count(trusted) from contacts where contacts.accountid = posts.accountid'
				.' 	and contacts.contactid = activeuserid and trusted = 1) as activeuseristrustedcontact,'
				.' (select count(trusted) from contacts where contacts.accountid = parentaccountid'
				.' 	and contacts.contactid = posts.accountid and contacts.trusted = 1) as postuseristrustedcontact,'
				.' (select count(recipients.recipientaccountid) from recipients, membertree where membertree.postid = posts.id'
				.' 	and recipients.tablename = "posts" and recipients.tableid = membertree.parentid'
				.' 	and recipients.recipientaccountid = posts.accountid) as postuserisrecipientofparent,'
				.' (select count(recipientaccountid) from recipients'
				.' 	where recipients.tablename = "posts" and recipients.tableid = posts.id'
				.' 	and recipients.recipientaccountid = activeuserid) as activeuserisrecipient,'
				.' flags.flagcount, flags.flagactive, flags.updated as flagupdated,'
				.' flags.flagagerating, flags.flagreason'
				.' from posts left join accounts on posts.accountid = accounts.id'
				.' left join posts as parentpost on parentpost.id = posts.parentid'
				.' left join flags on flags.tablename = "posts" and flags.tableid = posts.id'
				.' left join approvals on approvals.tablename = "posts" and approvals.tableid = posts.id'
				.' and approvals.accountid = posts.accountid'
				.' where posts.id=%d limit 1',
				$index_activeuserid, $index_activeuserid, $index_activeuserid, $index_page_id);
		$result = func_php_query($query);
		$post = mysqli_fetch_assoc($result);
		$authorid = $post['authorid'];
		$authorusername = $post['authorusername'];
		$parentreplypermissions = $post['parentreplypermissions'];
		$parentapprovepermissions = $post['parentapprovepermissions'];
		$postid = $post['id'];
		$postparentid = $post['parentid'];
		$postdate = $post['added'];
		$postpermissions = $post['permissions'];
		$postviewmode = $post['viewmode'];
		$postsortmode = $post['sortmode'];
		$postsortreverse = $post['sortreverse'];
		$postagerating = $post['agerating'];
		$postmembercount = $post['membercount'];
		$posttrustedmembercount = $post['trustedmembercount'];
		$postuseristrustedmember = $post['postuseristrustedmember'];
		$postuseristrustedcontact = $post['postuseristrustedcontact'];
		$postuserisrecipientofparent = $post['postuserisrecipientofparent'];
		$postapprovaltrusted = $post['postapprovaltrusted'];
		$postapprovalhidden = $post['postapprovalhidden'];
		$postapprovaladded = $post['postapprovaladded'];
		$postrecipients = $post['recipients'];
		$parentaccountid = $post['parentaccountid'];
		$postflagcount = $post['flagcount'];
		$postflagactive = $post['flagactive'];
		$postflagupdated = $post['flagupdated'];
		$postflagagerating = $post['flagagerating'];
		$postflagreason = $post['flagreason'];

		$pendingapproval = false;
		$flagpost = false;
		$flagpostremovebutton = false;
		$additionalpostclasses = '';
		$postflagclass = '';
		$postpendingclass = '';

		/* Display post status if pending, hidden, or flagged. */
		if ($postflagactive) {
			$additionalpostclasses = ' postflagged';
			$postflagclass = 'poststatusflagged';
			$flagpost = true;
			if ($index_page_userpermissions['canedit'] || $index_page_userpermissions['canmoderate']) {
				$flagpostremovebutton = index_php_getClearFlagButton('post', $postid,
						$postflagupdated, $postflagcount);
			}
		} else if (($parentapprovepermissions < $parentreplypermissions) || $postapprovalhidden) {
			if ((($index_activeuserid == $parentaccountid) || ($index_activeuserid == $authorid))
					&& ($parentaccountid != $authorid)
					&& (($postapprovalhidden == 1)
					|| (($parentapprovepermissions == 4) && ($authorid == 0))
					|| (($parentapprovepermissions == 3) && ($postuseristrustedmember == 0))
					|| (($parentapprovepermissions == 2) && ($postuseristrustedcontact == 0))
					|| (($parentapprovepermissions == 1) && ($postuserisrecipientofparent == 0))
					|| (($parentapprovepermissions == 0) && ($parentaccountid != $authorid))
					)) {
				if ($postapprovalhidden == 1) {
					$additionalpostclasses = ' postapprovalhidden';
				} else if ($postapprovaltrusted != 1) {
					$additionalpostclasses = ' postapprovalpending';
					$pendingapproval = true;
					$postpendingclass = 'poststatuspending';
				}
			}
		}

		if (!$flagpost && (!$index_activeuserid || ($authorid != $index_activeuserid))) {
			$query = sprintf('update posts set viewcount = viewcount+1 where posts.id = %d limit 1;', $postid);
			func_php_query($query);
		}
	}

	/* Now display actual page */
	if ($index_page_id) {
?>
		<div id="post_<?=$postid?>" class="debugpostclass titlepost debugpostborderclass<?=$additionalpostclasses?>"
				postaccountid="<?=$authorid?>" postpermissions="<?=$postpermissions?>"
				postviewmode="<?=$postviewmode?>"
				postsortmode="<?=$postsortmode?>" postsortreverse="<?=$postsortreverse?>"
				postagerating="<?=$postagerating?>" postrecipients="<?=$postrecipients?>"
				postmembercount="<?=$postmembercount?>">
			<div id="postblock_<?=$postid?>">
<?php
		if (($flagpost && (($authorid == $index_activeuserid) && ($index_activeuserid > 0))
				|| ($index_activeuserconfirmedage >= $postflagagerating))
				|| !$flagpost) {
?>
				<div id="postheader_<?=$postid?>" class="postheader">
					<div id="postcredits_<?=$postid?>" class="postcredits postcreditsflex">
						<?=func_php_insertPostCredits('post', $postid, true, 'full', true, $index_activeuserid)?>
						<?=func_php_insertPostPopupList('post', $postid, true, 'title', $index_page, 'full', $index_activeuserid)?>
					</div>
					<div id="postrecipients_<?=$postid?>" class="recipientsdiv">
						<?=index_php_insertPostRecipients('post', $postid)?>
					</div>
				</div>
				<div id="postmembers_<?=$postid?>" class="postmembers">
					<?=index_php_insertPostMembers('post', $postid)?>
				</div>
<?php
		}
?>
				<div id="postflagdiv_<?=$postid?>" class="<?=$postflagclass?>">
<?php
		if ($flagpost) {
			$fuzzytime = func_php_fuzzyTimeAgo($postflagupdated, false, false);
			if ($fuzzytime == 'now') {
				$fuzzytime = 'a minute ago';
			}

			$reason = index_php_getflagreasondescription($postflagreason);
			$fulldate = date('l g:i a\<\b\r\>jS F Y', strtotime($postflagupdated));
?>
					This post was flagged <span class="tooltiphost poststatusflaggedunderline"><?=$fuzzytime?><span class="tooltiptext tooltiptextleft tooltiptexttop"><?=$fulldate?></span></span>.<br>Reason: <?=$reason?>
<?php
			if ($flagpostremovebutton) {
?>
					<br><?=$flagpostremovebutton?>
<?php
			}
		}
?>
				</div>
				<div id="postpending_<?=$postid?>" class="<?=$postpendingclass?>">
<?php
		if ($pendingapproval) {
?>
					This post is pending approval.
<?php
		}
?>
				</div>
<?php
		if (($flagpost && (($authorid == $index_activeuserid) && ($index_activeuserid > 0)))
				|| ($index_activeuserconfirmedage >= $postflagagerating)
				|| !$flagpost) { /* Start if flagged... */
?>
				<div class="postdescription postdescriptionpadding" id="postdescription_<?=$postid?>"><?=func_php_markdowntohtml_lazyload($post['description'])?></div>
				<div id="postattachments_<?=$postid?>">
<?php
			if ($attachmentarray = func_php_fileAttachmentArray('post', $postid)) {
				foreach ($attachmentarray as $attachment) {
					echo "<div id=\"{$attachment['id']}\" class=\"postfile\">{$attachment['innerHTML']}</div>";
				}
			}
?>
				</div>
<?php
		} /* End if flagged... */
?>
			</div>
		</div>
<?php
	} else {
?>
		<div id="post0" class="debugpostclass titlepost debugpostborderclass" permissions="0000000">
			<div class="postdescription postdescriptionpadding" id="postdescription0">
				<h2>Page not found</h2>
				<p>The page you were looking for doesn't exist or is currently unavailable.</p>
				<p>Some things you may want to do include:
					<ul>
					<li>Signing in to your account (if you have one).</li>
					<li>Clicking the <strong>back</strong> button on your web browser.</li>
					<li>Visiting the home page of this website.</li>
					<li>Using the search bar above to find the information you are looking for.</li>
					</ul>
				</p>
			</div>
		</div>
<?php
	}
}

/* Replies and the "controls" for them go below here */
if ($index_showposts) {
?>
		<!-- div class="debugpostclass debugpostborderclass">
			<div class="postcredits"><?=__LINE__?> Company Name</div>
			<div class="postdescription"><?=__LINE__?> Advertise here!</div>
		</div -->
<?php
}

/* TODO: MERGESELECTPOST. */
$querycriteria = '';
$querycriteriaand = '';
if ($index_page == 'profile') {
	$querycriteria = sprintf('posts.accountid = (select id from accounts where username = "%s" limit 1)' /* USERNAME */
			.' and posts.viewpermissions != 1 and (posts.parentid = 0'
			.' 	or ((select p.accountid from posts as p where p.id = posts.parentid) <> posts.accountid))'
			.' and pins.pinstate = 1 and pins.parentpostid = 0',
			$index_page_id);
} else if (($index_page == 'page') && $index_page_id) {
	$querycriteria = sprintf('posts.parentid = %d' /* POSTID */
			.' and pins.pinstate = 1 and pins.parentpostid = %d', /* POSTID */
			$index_page_id, $index_page_id);
}

if ($querycriteria) {
	$querycriteriaand = " and $querycriteria";
}

/* Build the final query using post id or profile id criteria and run it. */
/* TODO: Cull values generated in ..._getpostpermissions() and ..._getuserpermissions() instead. */
$index_pinnedposts = false;
if ($index_showposts && $querycriteria) {
	$query = sprintf('/* '.__LINE__. ' */ select %d as activeuserid, posts.*,' /* USERID */
			.' parentpost.accountid as parentaccountid, parentpost.editpermissions as parenteditpermissions,'
			.' parentpost.replypermissions as parentreplypermissions,'
			.' parentpost.approvepermissions as parentapprovepermissions,'
			.' accounts.id as authorid, accounts.username as authorusername, accounts.fullname as authorfullname,'
			.' concat(posts.viewpermissions, posts.editpermissions, posts.replypermissions, posts.approvepermissions,'
			.' posts.moderatepermissions, posts.tagpermissions) as permissions, posts.agerating,'
			.' (select group_concat(distinct accounts.username separator " ") from accounts, recipients'
			.' where accounts.id = recipients.recipientaccountid and recipients.tablename = "posts"'
			.' and recipients.tableid = posts.id) as recipients,'
			.' ((select count(*) from members left join membertree on members.postid = membertree.parentid'
			.' 	where members.accountid = posts.accountid and membertree.postid = posts.id'
			.' 	and members.trusted = 1 /* and members.accountid != posts.accountid */ )'
			.' 	+ (select count(*) from members where members.accountid = posts.accountid'
			.' 	and members.postid = posts.id and members.trusted = 1 /* and members.accountid != posts.accountid */))'
			.' 	as postuseristrustedmember,'
			.' approvals.id as postapprovalid, approvals.trusted as postapprovaltrusted,'
			.' approvals.hidden as postapprovalhidden, approvals.added as postapprovaladded,'
			.' ((select count(*) from members left join membertree on members.postid = membertree.parentid'
			.' 	where members.accountid = %d and membertree.postid = posts.id' /* USERID */
			.' 	and members.trusted = 1 and members.accountid != posts.accountid)'
			.' 	+ (select count(*) from members where members.accountid = %d' /* USERID */
			.' 	and members.postid = posts.id and members.trusted = 1 and members.accountid != posts.accountid))'
			.' 	as activeuseristrustedmember,'
			.' (select count(trusted) from contacts where contacts.accountid = posts.accountid'
			.' 	and contacts.contactid = activeuserid and contacts.trusted = 1) as activeuseristrustedcontact,'
			.' (select count(trusted) from contacts where contacts.accountid = parentaccountid'
			.' 	and contacts.contactid = posts.accountid and contacts.trusted = 1) as postuseristrustedcontact,'
			.' (select count(recipientaccountid) from recipients'
			.' where recipients.tablename = "posts" and recipients.tableid = posts.id'
			.' 	and recipients.recipientaccountid = activeuserid) as activeuserisrecipient,'
			.' (select count(recipients.recipientaccountid) from recipients, membertree'
			.' 	where membertree.postid = posts.id and recipients.tablename = "posts"'
			.' 	and recipients.tableid = membertree.parentid and recipients.recipientaccountid = posts.accountid)'
			.' 	as postuserisrecipientofparent,'
			.' flags.flagcount, flags.flagactive, flags.updated as flagupdated,'
			.' flags.flagagerating, flags.flagreason, pins.pinstate'
			.' from posts left join accounts on posts.accountid = accounts.id'
			.' left join posts as parentpost on parentpost.id = posts.parentid'
			.' left join flags on flags.tablename = "posts" and flags.tableid = posts.id'
			.' left join approvals on approvals.tablename = "posts" and approvals.tableid = posts.id'
			.' 	and approvals.accountid = posts.accountid'
			.' left join pins on pins.postid = posts.id'
			.' where ', $index_activeuserid, $index_activeuserid, $index_activeuserid).$querycriteria;

	/* View Permissions! (ie. If the currently signed in user can view the post.) */
	$query .= ' /* '.__LINE__.':ViewPermissionsFiltering */ and (posts.id in (' /* Open filtering bracket, closes below */

			.sprintf('select posts.id from posts'
			.' where (posts.viewpermissions = 5 or ((posts.accountid = %d' /* USERID */
			.' or posts.viewpermissions = 4) and %d != 0))', /* USERID */
			$index_activeuserid, $index_activeuserid).$querycriteriaand

			.sprintf(' UNION select posts.id from posts, recipients'
			.' where posts.viewpermissions = 1 and recipients.tablename = "posts" and recipients.tableid = posts.id'
			.' and ((recipients.senderaccountid = %d or recipients.recipientaccountid = %d)' /* USERID, USERID */
			.' 	and %d != 0)', /* USERID */
			$index_activeuserid, $index_activeuserid, $index_activeuserid).$querycriteriaand

			.sprintf(' UNION select posts.id from posts, contacts'
			.' where posts.viewpermissions = 2 and posts.accountid = contacts.accountid'
			.' and contacts.trusted = 1 and contacts.contactid = %d', /* USERID */
			$index_activeuserid).$querycriteriaand

			.sprintf(' UNION select posts.id' /* USERID */
			.' from posts, members, membertree, posts as parentpost'
			.' where posts.viewpermissions = 3 and (membertree.postid = posts.id or membertree.parentid = posts.id)'
			.' and membertree.parentid = members.postid'
			.' and ((members.trusted = 1 and members.accountid = %d)' /* USERID */
			.' 	or (parentpost.id = membertree.parentid and parentpost.accountid = %d))', /* USERID */
			$index_activeuserid, $index_activeuserid).$querycriteriaand

			.')';

	/* Filter reply/approve permissions, if we're displaying a post page. */
	if (($index_page == 'page') && $index_page_id) {
		$query .= ' /* '.__LINE__.':PostReplyApprovePermissionsFilter */ and posts.id in ('

				/* Filter reply permissions. */
				.sprintf('select posts.id from posts, posts as parentpost where posts.parentid = parentpost.id'
						.' and (posts.accountid = parentpost.accountid'
						.' or (posts.accountid = %d and %d != 0)' /* USERID, USERID */
						.' or parentpost.replypermissions = 0 and parentpost.accountid = %d' /* USERID */
						.' or parentpost.replypermissions = 5'
						.' or (parentpost.replypermissions = 4 and posts.accountid != 0))', /* USERID */
						$index_activeuserid,  $index_activeuserid, $index_activeuserid).$querycriteriaand

				.sprintf(' UNION select posts.id from posts, posts as parentpost, recipients'
						.' where posts.parentid = parentpost.id'
						.' and parentpost.replypermissions = 1 and recipients.tablename = "posts"'
						.' and recipients.tableid = parentpost.id'
						.' and recipients.recipientaccountid = posts.accountid').$querycriteriaand

				.sprintf(' UNION select posts.id from posts, posts as parentpost, contacts'
						.' where posts.parentid = parentpost.id'
						.' and parentpost.replypermissions = 2 and contacts.accountid = parentpost.accountid'
						.' and contacts.contactid = posts.accountid'
						.' and contacts.trusted = 1').$querycriteriaand

				.sprintf(' UNION select posts.id from membertree inner join posts on membertree.postid = posts.id'
						.' inner join members on membertree.parentid = members.postid'
						.' inner join posts as parentpost on parentpost.id = posts.parentid'
						.' where members.accountid = posts.accountid and parentpost.replypermissions = 3'
						.' and members.trusted = 1').$querycriteriaand

				/* Filter by approve permissions. */

				.sprintf(' UNION select posts.id from membertree inner join posts on membertree.postid = posts.id'
						.' inner join members on membertree.parentid = members.postid'
						.' inner join posts as parentpost on parentpost.id = posts.parentid'
						.' where members.accountid = posts.accountid and parentpost.approvepermissions = 3'
						.' and members.trusted = 1').$querycriteriaand

				.')';
	}
	$query .= ')'; /* Close filtering bracket */

	$query .= sprintf(' order by pins.added desc');

	if ($index_showposts) {
		$index_pinnedposts = func_php_query($query);
	}
}
?>
	<div id="pinnedpostlocation" class="mainflexboxpadded">
<?php
if ($index_pinnedposts) {
	/* TODO: MERGESELECTPOST - This code is also used for the "page" section.
	 * It needs to be broken off as a function to simplify things! */
	while ($post = mysqli_fetch_assoc($index_pinnedposts)) {
		$postid = $post['id'];
		$authorid = $post['accountid'];
		$authorusername = $post['authorusername'];
		$authorfullname = $post['authorfullname'];
		$postparentid = $post['parentid'];
		$parentaccountid = $post['parentaccountid'];
		$parenteditpermissions = $post['parenteditpermissions'];
		$parentreplypermissions = $post['parentreplypermissions'];
		$parentapprovepermissions = $post['parentapprovepermissions'];
		$postdate = $post['added'];
		$postpermissions = $post['permissions'];
		$postviewmode = $post['viewmode'];
		$postsortmode = $post['sortmode'];
		$postsortreverse = $post['sortreverse'];
		$postagerating = $post['agerating'];
		$postuseristrustedmember = $post['postuseristrustedmember'];
		$postuseristrustedcontact = $post['postuseristrustedcontact'];
		$postuserisrecipientofparent = $post['postuserisrecipientofparent'];
		$postapprovalid = $post['postapprovalid'];
		$postapprovaltrusted = $post['postapprovaltrusted'];
		$postapprovalhidden = $post['postapprovalhidden'];
		$postapprovaladded = $post['postapprovaladded'];
		$postrecipients = $post['recipients'];
		$postflagcount = $post['flagcount'];
		$postflagactive = $post['flagactive'];
		$postflagupdated = $post['flagupdated'];
		$postflagagerating = $post['flagagerating'];
		$postflagreason = $post['flagreason'];
		$additionalpostclasses = ''; /* For styling approval pending, hidden posts, etc. */
		$permissions = func_php_getPostPermissions($postid);
		$userpermissions = func_php_getUserPermissions($postid, $index_activeuserid);

		/* TODO: Should we shift the following conditions to SQL? */
		/* Show hidden posts if owner or moderator. */
		/* Skip pending if not approved and not moderator or original author. */

		$skippost = true;
		$flagpost = false;
		$flagpostremovebutton = false;
		$pendingapproval = false;
		$postflagclass = '';
		$postpendingclass = '';

		/* Hide age restricted content. */
		if (($index_activeuserconfirmedage < $postagerating)
				&& ($index_activeuserid != $authorid)) {
			$skippost = true;
		} else if ($postflagactive) {
			$showflaggedpost = false;
			/* Display flagged posts to owner/moderator only. */
			if ($userpermissions['canedit'] || $userpermissions['canmoderate']) {
				/* Owner always sees posts. */
				$showflaggedpost = true;
				$flagpostremovebutton = index_php_getClearFlagButton('post', $postid,
						$postflagupdated, $postflagcount);
			} else {
				/* Only show appropriate posts depending on flag reason */
				switch ($postflagreason) {
				case 'MATURE_CONTENT':
				case 'ADULT_CONTENT':
					if ($index_activeuserconfirmedage >= $postflagagerating) {
						$showflaggedpost = true;
					}
					break;
				case 'COPYRIGHT_OR_TRADEMARK':
				case 'ILLEGAL_OR_VIOLATION':
					break;
				default:
					/* Always hide except from owner. */
				}
			}

			if ($showflaggedpost) {
				$additionalpostclasses = ' postflagged';
				$postflagclass = 'poststatusflagged';
				$flagpost = true;
				$skippost = false;
			}
		} else if (!$permissions['canreply']) {
			if ($authorid == $index_activeuserid) {
				$additionalpostclasses = ' postapprovalhidden';
				$skippost = false;
			}
		/* Display conditionally pending/hidden posts to moderators/owners. */
		} else if (!$permissions['preapproved']
				|| ($permissions['approvalshidden'] != 0)) {
			if ($permissions['approvalstrusted'] != 0) {
				/* Always show approved posts. */
				// $additionalpostclasses = ' postapprovaltrusted';
				$skippost = false;
			} else if ($permissions['approvalshidden'] != 0) {
				/* Show hidden-approved posts to moderators or owners */
				if (($userpermissions['canmoderate'])
						|| ($index_activeuserid && ($index_activeuserid == $authorid))) {
					$additionalpostclasses = ' postapprovalhidden';
					$skippost = false;
				}
			} else if (($userpermissions['canmoderate'])
					|| (($index_activeuserid == $authorid) && ($index_activeuserid != 0))) {
				$additionalpostclasses = ' postapprovalpending';
				$postpendingclass = 'poststatuspending';
				$skippost = false;
				$pendingapproval = true;
			}
		} else {
			$skippost = false;
		}

		if ($skippost) {
			continue;
		}
?>
		<div id="post_<?=$postid?>" class="debugpostclass debugreplyborderclass<?=$additionalpostclasses?>"
				postid="<?=$postid?>" postaccountid="<?=$authorid?>"
				postpermissions="<?=$postpermissions?>" postviewmode="<?=$postviewmode?>"
				postsortmode="<?=$postsortmode?>" postsortreverse="<?=$postsortreverse?>"
				postagerating="<?=$postagerating?>" postrecipient="<?=$authorusername?>"
				postrecipients="<?=$postrecipients?>">
			<div id="postblock_<?=$postid?>">
<?php
		if (($flagpost && $showflaggedpost) || !$flagpost) {

			$showparent = false;
			if ($index_page == 'profile')
				$showparent = true;

			/* With pinned posts, popuplist always has 'full' for $pageview.
			 * No need to check for $index_viewmode. */
?>
				<div id="postheader_<?=$postid?>" class="postheader">
					<div id="postcredits_<?=$postid?>" class="postcredits postcreditsflex">
						<?=func_php_insertPostCredits('post', $postid, $showparent, 'full', true, $index_activeuserid)?>
						<?=func_php_insertPostPopupList('post', $postid, false, 'post', $index_page, 'full', $index_activeuserid)?>
					</div>
					<div id="postrecipients_<?=$postid?>" class="recipientsdiv">
						<?=index_php_insertPostRecipients('post', $postid)?>
					</div>
				</div>
				<div id="postmembers_<?=$postid?>" class="postmembers">
					<?=index_php_insertPostMembers('post', $postid)?>
				</div>
<?php
		}
?>
				<div id="postflagdiv_<?=$postid?>" class="<?=$postflagclass?>">
<?php
		if ($flagpost) {
			$fuzzytime = func_php_fuzzyTimeAgo($postflagupdated, false, false);
			if ($fuzzytime == "now") {
				$fuzzytime = "a minute ago";
			}

			$reason = index_php_getflagreasondescription($postflagreason);
			$fulldate = date('l g:i a\<\b\r\>jS F Y', strtotime($postflagupdated));
?>
				This post was flagged <span class="tooltiphost poststatusflaggedunderline"><?=$fuzzytime?><span class="tooltiptext tooltiptextleft tooltiptexttop"><?=$fulldate?></span></span>.<br>Reason: <?=$reason?>
<?php
			if ($flagpostremovebutton) {
?>
					<br><?=$flagpostremovebutton?>
<?php
			}
		}
?>
				</div>
				<div id="postpending_<?=$postid?>" class="<?=$postpendingclass?>">
<?php

		if ($pendingapproval) {
?>
					This post is pending approval.
<?php
		}
?>
				</div>
<?php
		if (($flagpost && $showflaggedpost) || !$flagpost) { /* Start if flagged... */
?>
				<div class="postdescription postdescriptionpadding" id="postdescription_<?=$postid?>"><?=func_php_markdowntohtml_lazyload($post['description'])?></div>
				<div id="postattachments_<?=$postid?>">
<?php
			if ($attachmentarray = func_php_fileAttachmentArray('post', $postid)) {
				foreach ($attachmentarray as $attachment) {
					echo "<div id=\"{$attachment['id']}\" class=\"postfile\">{$attachment['innerHTML']}</div>";
				}
			}
?>
				</div>
<?php
		} /* End if flagged... */
?>
			</div>
			<!-- Inline post replies start here. -->
			<div id="postreplyblock_<?=$postid?>" class="postreplies">
<?php
		/* Get what reply posts we want... */
		$querycriteria = sprintf('posts.parentid = %d', $postid);
		$querycriteriaand = " and {$querycriteria}";
		$query = sprintf('/* '.__LINE__. ' */ select %d as activeuserid, posts.*,' /* USERID */
				.' parentpost.accountid as parentaccountid,'
				.' parentpost.editpermissions as parenteditpermissions,'
				.' parentpost.replypermissions as parentreplypermissions,'
				.' parentpost.approvepermissions as parentapprovepermissions,'
				.' parentpost.moderatepermissions as parentmoderatepermissions,'
				.' accounts.id as authorid, accounts.username as authorusername,'
				.' accounts.fullname as authorfullname,'
				.' concat(posts.viewpermissions, posts.editpermissions,'
				.' 	posts.replypermissions, posts.approvepermissions, posts.moderatepermissions,'
				.' 	posts.tagpermissions) as permissions, posts.agerating,'
				.' (select group_concat(distinct concat_ws("", accounts.username, recipients.emailusername,'
				.'		if (recipients.emailserver is not NULL, "@", ""),'
				.' 		recipients.emailserver) separator " ")'
				.' 	from recipients left join accounts on accounts.id = recipients.recipientaccountid'
				.' 	where recipients.tablename = "posts"'
				.'	and recipients.senderaccountid != recipients.recipientaccountid'
				.' 	and recipients.tableid = posts.id) as recipients,'
				.' ((select count(*) from members left join membertree on members.postid = membertree.parentid'
				.' 	where members.accountid = posts.accountid and membertree.postid = posts.id'
				.' 	and members.trusted = 1 and members.accountid != posts.accountid)'
				.' 	+ (select count(*) from members where members.accountid = posts.accountid'
				.' 	and members.postid = posts.id and members.trusted = 1'
				.' 	and members.accountid != posts.accountid)) as postuseristrustedmember,'
				.' approvals.id as postapprovalid, approvals.trusted as postapprovaltrusted,'
				.' approvals.hidden as postapprovalhidden, approvals.added as postapprovaladded,'
				.' ((select count(*) from members left join membertree on members.postid = membertree.parentid'
				.' 	where members.accountid = %d and membertree.postid = posts.id' /* USERID */
				.' 	and members.trusted = 1 and members.accountid != posts.accountid)'
				.' 	+ (select count(*) from members where members.accountid = %d' /* USERID */
				.' 	and members.postid = posts.id and members.trusted = 1'
				.' 	and members.accountid != posts.accountid)) as activeuseristrustedmember,'
				.' (select count(trusted) from contacts'
				.' 	where contacts.accountid = posts.accountid and contacts.contactid = activeuserid'
				.' 	and contacts.trusted = 1) as activeuseristrustedcontact,'
				.' (select count(trusted) from contacts'
				.' 	where contacts.accountid = parentaccountid and contacts.contactid = posts.accountid'
				.' 	and contacts.trusted = 1) as postuseristrustedcontact,'
				.' (select count(recipientaccountid) from recipients'
				.' where recipients.tablename = "posts" and recipients.tableid = posts.id'
				.' and recipients.recipientaccountid = activeuserid) as activeuserisrecipient,'
				.' (select count(recipients.recipientaccountid) from recipients, membertree'
				.' 	where membertree.postid = posts.id and recipients.tablename = "posts"'
				.' 	and recipients.tableid = membertree.parentid'
				.' 	and recipients.recipientaccountid = posts.accountid) as postuserisrecipientofparent,'
				.' (select count(*) from posts as postreplies'
				.' 	where postreplies.parentid = posts.id) as postreplycount,'
				.' flags.flagcount, flags.flagactive, flags.updated as flagupdated,'
				.' flags.flagagerating, flags.flagreason'
				.' from posts left join accounts on posts.accountid = accounts.id'
				.' left join posts as parentpost on parentpost.id = posts.parentid'
				.' left join flags on flags.tablename = "posts" and flags.tableid = posts.id'
				.' left join approvals on approvals.tablename = "posts"'
				.' and approvals.tableid = posts.id and approvals.accountid = posts.accountid'
				.' where ', $index_activeuserid, $index_activeuserid, $index_activeuserid).$querycriteria;

		/* View Permissions! (ie. If the currently signed in user can view the post.) */
		$query .= ' /* '.__LINE__.':ViewPermissionsFiltering */ and (posts.id in (' /* Open filtering bracket, closes below */

				.sprintf('select posts.id from posts'
				.' where (posts.accountid = %d or posts.viewpermissions = 5 or (posts.viewpermissions = 4 and %d != 0))', /* USERID, USERID */
				$index_activeuserid, $index_activeuserid).$querycriteriaand

				.sprintf(' UNION select posts.id from posts, recipients'
				.' where posts.viewpermissions = 1 and recipients.tablename = "posts" and recipients.tableid = posts.id'
				.' and (recipients.senderaccountid = %d or recipients.recipientaccountid = %d)', /* USERID, USERID */
				$index_activeuserid, $index_activeuserid).$querycriteriaand

				.sprintf(' UNION select posts.id from posts, contacts'
				.' where posts.viewpermissions = 2 and posts.accountid = contacts.accountid and contacts.trusted = 1 and contacts.contactid = %d', /* USERID */
				$index_activeuserid).$querycriteriaand

				.sprintf(' UNION select posts.id' /* USERID */
				.' from posts, members, membertree, posts as parentpost'
				.' where posts.viewpermissions = 3 and (membertree.postid = posts.id or membertree.parentid = posts.id) and membertree.parentid = members.postid'
				.' and ((members.trusted = 1 and members.accountid = %d) or (parentpost.id = membertree.parentid and parentpost.accountid = %d))', /* USERID, USERID */
				$index_activeuserid, $index_activeuserid).$querycriteriaand

				.')';

		$query .= ' /* '.__LINE__.':PostReplyApprovePermissionsFilter */ and posts.id in ('

				/* Filter reply permissions. */

				.sprintf('select posts.id from posts, posts as parentpost where posts.parentid = parentpost.id'
						.' and (posts.accountid = parentpost.accountid'
						.' or (posts.accountid = %d and %d != 0)' /* USERID, USERID */
						.' or parentpost.replypermissions = 0 and parentpost.accountid = %d' /* USERID */
						.' or parentpost.replypermissions = 5'
						.' or (parentpost.replypermissions = 4 and  posts.accountid != 0))', /* USERID */
						$index_activeuserid, $index_activeuserid, $index_activeuserid).$querycriteriaand

				.sprintf(' UNION select posts.id from posts, posts as parentpost, recipients where posts.parentid = parentpost.id'
						.' and parentpost.replypermissions = 1 and recipients.tablename = "posts" and recipients.tableid = parentpost.id'
						.' and recipients.recipientaccountid = posts.accountid').$querycriteriaand

				.sprintf(' UNION select posts.id from posts, posts as parentpost, contacts where posts.parentid = parentpost.id'
						.' and parentpost.replypermissions = 2 and contacts.accountid = parentpost.accountid and contacts.contactid = posts.accountid'
						.' and contacts.trusted = 1').$querycriteriaand

				.sprintf(' UNION select posts.id from membertree inner join posts on membertree.postid = posts.id'
						.' inner join members on membertree.parentid = members.postid'
						.' inner join posts as parentpost on parentpost.id = posts.parentid'
						.' where members.accountid = posts.accountid and parentpost.replypermissions = 3'
						.' and members.trusted = 1').$querycriteriaand

				/* Filter by approve permissions. */

				.sprintf(' UNION select posts.id from membertree inner join posts on membertree.postid = posts.id'
						.' inner join members on membertree.parentid = members.postid'
						.' inner join posts as parentpost on parentpost.id = posts.parentid'
						.' where members.accountid = posts.accountid and parentpost.approvepermissions = 3'
						.' and members.trusted = 1').$querycriteriaand
				.')';

		$query .= ')'; /* Close filtering bracket */

		/* Sort functionality goes here... eventually. */
		$query .= sprintf(' order by posts.added asc limit 6');

		$postreplies = func_php_query($query);
		$postreplycount = 0;
		$postreplyparentuserpermissions = func_php_getUserPermissions($postid, $index_activeuserid);
		$postreplynumrows = mysqli_num_rows($postreplies);

		/* Set up post reply links */
		$postreplyuserpermissions = func_php_getUserPermissions($postid, $index_activeuserid);
		$newpostreplyrecipient = false;
		if ($authorusername != $index_activeusername) {
			$newpostreplyrecipient = $authorusername;
		}
		$newpostreplypostid = $postid;
		$newpostreplylabel = 'Reply';
		if (!$postreplyuserpermissions['canreply']) {
			$newpostreplylabel = '';
		} else if (!$postreplyuserpermissions['preapproved']) {
			$newpostreplylabel = '<span class="tooltiphost">'
					.'<span class="tooltiptext tooltiptextleft">'
					.'<img src="/img/status-pending12.png">'
					.' Approval required<br>to show replies.</span>'
					.$newpostreplylabel
					.' <img src="/img/status-pending12.png"></span>';
		}

		$newpostreplybyemaillabel = '';
		if ($postreplyuserpermissions['replypermissions'] == 5) {
			$newpostreplybyemaillabel = 'Reply by email';

			if ($postreplyuserpermissions['approvepermissions'] < 5) {
				$newpostreplybyemaillabel = '<span class="tooltiphost">'
						.'<span class="tooltiptext tooltiptextright">'
						.'<img src="/img/status-pending12.png">'
						.' Approval required<br>to show emails.</span>'
						.$newpostreplybyemaillabel
						.' <img src="/img/status-pending12.png"></span>';
			}
		}

		$newpostreplyshowsignin = false;
		$newpostreplyshowjoingroup = false;
		$newpostreplypermissions = '000000';
		if ($newpostreplylabel) {
			switch ($index_newpostpermissions[0]) {
			case '5':
				$newpostreplypermissions = '504404';
				break;
			case '4':
				$newpostreplypermissions = '404404';
				break;
			case '3':
				$newpostreplypermissions = '303303';
				break;
			case '2':
				$newpostreplypermissions = '202202';
				break;
			case '1':
				$newpostreplypermissions = '101101';
				break;
			default:
				$newpostreplypermissions = '000000';
			}
		} else if ($postreplyuserpermissions['replypermissions'] == 4) {
			$newpostreplyshowsignin = true;
		} else if ($postreplyuserpermissions['replypermissions'] == 3) {
			if ($index_activeuserid) {
				$newpostreplyshowjoingroup = true;
			} else {
				$newpostreplyshowsignin = true;
			}
		}

		/* Show upper reply bar if any inline replies */
		if ($postreplynumrows) {
?>
				<div class="postreplybar">
					<span class="fadetext">
						<a href="/<?=$postid?>">View Page</a>
<?php
			if ($newpostreplylabel) {
				echo " &bull; <a href=\"#newpostreply_{$newpostreplypostid}\""
						." onclick=\"$('newpostreplylink_{$newpostreplypostid}').click();\">"
						."$newpostreplylabel</a>";

				/* Only show Reply by email link if permissions allow */
				if ($newpostreplybyemaillabel) {
					echo " &bull; <a href=\"mailto:{$newpostreplypostid}@{$servername}\">"
							."$newpostreplybyemaillabel</a>";
					/*
					echo " &bull; <a onClick=\"prompt('To reply to this post by email,\\n"
							."send a message to:',"
							." '{$newpostreplypostid}'+'@'+'{$servername}')\">"
							."$newpostreplybyemaillabel</a>";
							// */
				}
			}
?>
					</span>
				</div>
<?php
		} /* End upper reply bar */

		/* Now display inline reply ladder */
		while (($postreply = mysqli_fetch_assoc($postreplies)) && ($postreplycount < 5)) {
			$postreplyid = $postreply['id'];
			$postreplyauthorid = $postreply['authorid'];
			$postreplyauthorusername = $postreply['authorusername'];
			$postreplyauthorfullname = $postreply['authorfullname'];
			$postreplyparentid = $postreply['parentid'];
			$postreplyparentaccountid = $postreply['parentaccountid'];
			$postreplyparenteditpermissions = $postreply['parenteditpermissions'];
			$postreplyparentreplypermissions = $postreply['parentreplypermissions'];
			$postreplyparentapprovepermissions = $postreply['parentapprovepermissions'];
			$postreplydate = $postreply['added'];
			$postreplypermissions = $postreply['permissions'];
			$postreplyviewmode = $postreply['viewmode'];
			$postreplysortmode = $postreply['sortmode'];
			$postreplysortreverse = $postreply['sortreverse'];
			$postreplyagerating = $postreply['agerating'];
			$postreplyuseristrustedmember = $postreply['postuseristrustedmember'];
			$postreplyuseristrustedcontact = $postreply['postuseristrustedcontact'];
			$postreplyuserisrecipientofparent = $postreply['postuserisrecipientofparent'];
			$postreplyreplycount = $postreply['postreplycount'];
			$postreplyapprovalid = $postreply['postapprovalid'];
			$postreplyapprovaltrusted = $postreply['postapprovaltrusted'];
			$postreplyapprovalhidden = $postreply['postapprovalhidden'];
			$postreplyapprovaladded = $postreply['postapprovaladded'];
			$postreplyrecipients = $postreply['recipients'];
			$postreplyflagcount = $postreply['flagcount'];
			$postreplyflagactive = $postreply['flagactive'];
			$postreplyflagupdated = $postreply['flagupdated'];
			$postreplyflagagerating = $postreply['flagagerating'];
			$postreplyflagreason = $postreply['flagreason'];
			/* $replyrecipientid = $postreply['recipientid']; */
			$additionalpostclasses = ' postapprovalnormal'; /* For styling approval pending, hidden posts, etc. */
			$postreplycount++;
			$permissions = func_php_getPostPermissions($postreplyid);
			$userpermissions = func_php_getUserPermissions($postreplyid, $index_activeuserid);

			/* TODO: Should we shift the following conditions to SQL? */
			/* Show hidden posts if owner or moderator. */
			/* Skip pending if not approved and not moderator or original author. */

			$skippost = true;
			$flagpost = false;
			$flagpostremovebutton = false;
			$showflaggedpost = $index_showflaggedposts;
			$pendingapproval = false;
			$postflagclass = '';
			$postpendingclass = '';

			/* Hide age restricted content. */
			if (($index_activeuserconfirmedage < $postreplyagerating)
					&& ($index_activeuserid != $postreplyauthorid)) {
				$skippost = true;
			} else if ($postreplyflagactive) {
				/* Display flagged posts to owner/moderator only. */
				if ($userpermissions['canedit'] || $userpermissions['canmoderate']) {
					/* Owner always sees posts. */
					$showflaggedpost = true;
					$flagpostremovebutton = index_php_getClearFlagButton('post', $postreplyid,
							$postreplyflagupdated, $postreplyflagcount);
				} else {
					/* Only show appropriate posts depending on flag reason */
					switch ($postreplyflagreason) {
					case 'MATURE_CONTENT':
					case 'ADULT_CONTENT':
						if ($index_activeuserconfirmedage >= $postreplyflagagerating) {
							$showflaggedpost = true;
						}
						break;
					case 'COPYRIGHT_OR_TRADEMARK':
					case 'ILLEGAL_OR_VIOLATION':
						break;
					default:
						/* Always hide except from owner. */
					}
				}

				if ($showflaggedpost) {
					$additionalpostclasses = ' inlinepostflagged';
					$postflagclass = 'poststatusflagged';
					$flagpost = true;
					$skippost = false;
				}
			} else if (!$permissions['canreply']) {
				if ($authorid == $index_activeuserid) {
					$additionalpostclasses = ' postapprovalhidden';
					$skippost = false;
				}
			/* Display conditionally pending/hidden posts to moderators/owners. */
			} else if (!$permissions['preapproved']
					|| ($permissions['approvalshidden'] != 0)) {
				if ($permissions['approvalstrusted'] != 0) {
					/* Always show approved posts. */
					// $additionalpostclasses = ' postapprovaltrusted';
					$skippost = false;
				} else if ($permissions['approvalshidden'] != 0) {
					/* Show hidden-approved posts to moderators or owners */
					if (($postreplyparentuserpermissions['canmoderate'])
							|| (($index_activeuserid == $authorid) && ($index_activeuserid != 0))) {
						$additionalpostclasses = ' postapprovalhidden';
						$skippost = false;
					}
				} else if (($postreplyparentuserpermissions['canmoderate'])
						|| (($index_activeuserid == $postreplyauthorid) && ($index_activeuserid != 0))) {
					$additionalpostclasses = ' postapprovalpending';
					$postpendingclass = 'poststatuspending';
					$skippost = false;
					$pendingapproval = true;
				}
			} else {
				$skippost = false;
			}

			if ($skippost) {
				continue;
			}
?>
			<div id="post_<?=$postreplyid?>" class="debugpostclass<?=$additionalpostclasses?>"
					postid="<?=$postreplyid?>" postaccountid="<?=$postreplyauthorid?>"
					postpermissions="<?=$postreplypermissions?>" postviewmode="<?=$postreplyviewmode?>"
					postsortmode="<?=$postreplysortmode?>" postsortreverse="<?=$postreplysortreverse?>"
					postagerating="<?=$postreplyagerating?>"
					postrecipient="<?=$postreplyauthorusername?>" postrecipients="<?=$postreplyrecipients?>">
				<div id="postblock_<?=$postreplyid?>">
<?php
			if (($flagpost && $showflaggedpost) || !$flagpost) {
?>
					<div id="postheader_<?=$postreplyid?>" class="postheader">
						<div id="postcredits_<?=$postreplyid?>" class="postcredits postcreditsflex">
							<?=func_php_insertPostCredits('post', $postreplyid, false, 'full', true, $index_activeuserid)?>
							<?=func_php_insertPostPopupList('post', $postreplyid,
									false, 'reply', false, 'full', $index_activeuserid)?>
						</div>
						<div id="postrecipients_<?=$postreplyid?>" class="recipientsdiv">
							<?=index_php_insertPostRecipients('post', $postreplyid)?>
						</div>
					</div>
					<div id="postmembers_<?=$postreplyid?>" class="postmembers">
						<?=index_php_insertPostMembers('post', $postreplyid)?>
					</div>
<?php
			}
?>
					<div id="postflagdiv_<?=$postreplyid?>" class="<?=$postflagclass?>">
<?php
			if ($flagpost) {
				$fuzzytime = func_php_fuzzyTimeAgo($postreplyflagupdated, false, false);
				if ($fuzzytime == 'now') {
					$fuzzytime = 'a minute ago';
				}

				$reason = index_php_getflagreasondescription($postreplyflagreason);
				$fulldate = date('l g:i a\<\b\r\>jS F Y', strtotime($postreplyflagupdated));
?>
						This post was flagged <span class="tooltiphost poststatusflaggedunderline"><?=$fuzzytime?><span class="tooltiptext tooltiptextleft tooltiptexttop"><?=$fulldate?></span></span>.<br>Reason: <?=$reason?>
<?php
				if ($flagpostremovebutton) {
?>
					<br><?=$flagpostremovebutton?>
<?php
				}
			}
?>
					</div>
					<div id="postpending_<?=$postreplyid?>" class="<?=$postpendingclass?>">
<?php
			if ($pendingapproval) {
?>
						This post is pending approval.
<?php
			}
?>
					</div>
<?php
			if (($flagpost && $showflaggedpost) || !$flagpost) { /* Start if flagged... */
?>
					<div class="postdescription postdescriptionpadding" id="postdescription_<?=$postreplyid?>"><?=func_php_markdowntohtml_lazyload($postreply['description'])?></div>
					<div id="postattachments_<?=$postreplyid?>">
<?php
				if ($attachmentarray = func_php_fileAttachmentArray('post', $postreplyid)) {
					foreach ($attachmentarray as $attachment) {
						echo "<div id=\"{$attachment['id']}\" class=\"postfile\">{$attachment['innerHTML']}</div>";
					}
				}
?>
					</div>
<?php
			} /* End if flagged... */
?>
				</div>
				<div class="postreplyblock"><span class="fadetext">
								<a href="/<?=$postreplyid?>">View page</a>
<?php
			/* View nested replies, if any */
			if ($postreplyreplycount > 0) {
				$replyword = $postreplyreplycount;
				if ($postreplyreplycount != 1) {
					$replyword .= ' replies...';
				} else {
					$replyword .= ' reply...';
				}
				echo "&bull; <a href=\"/{$postreplyid}#replies\">{$replyword}</a>";
			}
?>
						</span></div>
					</div>
<?php
		} /* End post reply entry */

		/* Show lower reply bar if either can reply or this post has replies */
		if (($newpostreplylabel) || ($postreplynumrows > $postreplycount)) {
?>
					<div id="newpostreply_<?=$postid?>" class="debugpostclass morerepliesdiv">
							<div class="postreplyblock">
							<span class="fadetext">
<?php
			$spacer = '';
			if ($newpostreplylabel) {
				echo "<a id=\"newpostreplylink_{$newpostreplypostid}\" onclick=\"index_js_setupPostEditor(this,"
						." 'newpostreply_{$newpostreplypostid}', 'post', $newpostreplypostid, true,"
						." '$newpostreplypermissions', 3, '$newpostreplyrecipient', '', '',"
						." 'full', '', false);\">$newpostreplylabel</a>";

				/* Only show Reply by email link if permissions allow */
				if ($newpostreplybyemaillabel) {
					echo " &bull; <a onClick=\"prompt('To reply to this post by email,\\n"
							."send a message to:',"
							." '{$newpostreplypostid}'+'@'+'{$servername}')\">"
							."$newpostreplybyemaillabel</a>";
				}

				$spacer = ' &bull; ';
			} else if ($newpostreplyshowsignin) {
				echo '<span class="fadetext"><a class="ctalink" href="#userpanel">Sign In</a>'
						.' or <a class="ctalink" href="#rightpanel">Sign in</a> to join this group.</span> ';
				$spacer = ' &bull; ';
			} else if ($newpostreplyshowjoingroup) {
				echo '<span class="fadetext">Join this group to reply.</span> ';
				$spacer = ' &bull; ';
			}

			/* Display show more replies link when there are too many to display with this post. */
			if ($postreplynumrows > $postreplycount) {
				echo "{$spacer}<a class=\"fadetext\" href=\"/{$postid}/#replies\">More replies...</a>";
			}
?>
							</span>
						</div>
					</div>
<?php
		} /* End lower reply bar */
?>
				</div>
				<!-- Inline post replies end here. -->
			</div>
<?php
	}
}
?>
		</div>
		<a name="replies"><!-- Used for #replies anchor to skip to, well, replies. --></a>
<?php
if ($index_page === 'invalidresetlink') {
?>
	<div style="background:#fff;padding:48px 8px;margin:8px 8px 8px 0;text-align:center;font-size:18px;">Invalid password reset link.</div>
<?php
}

/* Only show reply bar and reply view controls if posts shown */
if ($index_showposts) {
	$newpostrecipients = '';
	$newpoststyle = '';
	if ($index_newpostlabel) {
		$newpostpermissions = $index_newpostpermissions;
		$newpostrecipients = $index_newpostrecipients;
		$newpostparentid = $index_newpostparentid;

		/* No need for user to recipient self. */
		if ($index_page == 'profile') {
			$newpostrecipients = '';
			$newpostparentid = 0;
			/* No need for user to recipient self. */
			if (strtolower($index_activeusername) == strtolower($index_page_id)) {
				$newpostpermissions = '504404';
			} else {
				$newpostpermissions = '101101';
				$newpostrecipients = $index_page_id;
			}
		} else if ($index_page == 'emails') {
			$newpostpermissions = '101101';
			$newpostrecipients = $index_page_id;
		}
	}

	/* Set up view mode form and buttons */
	$viewasfullpostclass = 'viewasbutton';
	$viewasthumbnailclass = 'viewasbutton';
	$viewaslistclass = 'viewasbutton';
	switch ($index_get_viewas) {
	case 'thumbnail':
		$viewasthumbnailclass .= ' viewasactive';
		break;
	case 'list':
		$viewaslistclass .= ' viewasactive';
		break;
	default:
		$viewasfullpostclass .= ' viewasactive';
	}

	$formaction = '/';
	if ($index_page_id) {
		$formaction .= $index_page_id.'/';
	} else if ($index_get_page == 'new') {
		$formaction .= 'new/';
	} else {
		$formaction .= 'home/';
	}
?>
	<div id="post_NEWPOST" class="newpostblock debugpostclass debugpostborderclass postbordershadow" style="<?=$newpoststyle?>">
		<div id="postblock_NEWPOST">
			<div class="newpostflex">
				<form id="viewsortform" class="viewform" method="get" action="<?=$formaction?><?=$index_page_number?>#replies">
					<input id="viewformview" type="hidden" name="viewas" value="<?=$index_get_viewas?>">
					<span class="tooltiphost">
						<button class="<?=$viewasfullpostclass?>" onclick="$('viewformview').value='full'"><img src="/img/icon-viewmode-full.png"></button>
						<span class="tooltiptext">Show full posts</span>
					</span>
					<span class="tooltiphost">
						<button class="<?=$viewasthumbnailclass?>" onclick="$('viewformview').value='thumbnail'"><img src="/img/icon-viewmode-thumbnail.png"></button>
						<span class="tooltiptext">Show as thumbnails</span>
					</span>
					<span class="tooltiphost">
						<button class="<?=$viewaslistclass?>" onclick="$('viewformview').value='list'"><img src="/img/icon-viewmode-list.png"></button>
						<span class="tooltiptext">Show as list</span>
					</span>
<?php
	/* Replies to posts and emails can be sorted */
	if (($index_page_id) || ($index_page == 'emails')) {
		/* Set current sort mode */
		$sortoption = '';
		switch ($index_get_sort) {
		case 'title':
			$sortoption = $index_get_sort;
			break;
		default:
			$sortoption = '';
		}

		$sortreversechecked = '';
		if ($index_sortreverse) {
			$sortreversechecked = 'checked';
			$sortoption .= 'reversed';
		}
?>
					<span>
						Sort by:
						<select class="sortbyselect" name="sort">
							<option value="date" onclick="$('viewformreverse').checked=false;$('viewsortform').submit()"
									<?=($sortoption === '' ? 'selected' : '')?>>Newest</option>
							<option value="date" onclick="$('viewformreverse').checked=true;$('viewsortform').submit()"
									<?=($sortoption === 'reversed' ? 'selected' : '')?>>Oldest</button>
							<option value="title" onclick="$('viewformreverse').checked=false;$('viewsortform').submit()"
									<?=($sortoption === 'title' ? 'selected' : '')?>>Title A-Z</button>
							<option value="title" onclick="$('viewformreverse').checked=true;$('viewsortform').submit()"
									<?=($sortoption === 'titlereversed' ? 'selected' : '')?>>Title Z-A</button>
						</select>
					</span>
					<input id="viewformreverse" type="checkbox" name="order" value="reverse" style="display:none;" <?=$sortreversechecked?>>
<?php
}
?>
				</form>
<?php
		if ($index_newpostlabel) {
			if (($index_page == 'profile') && ($index_get_page != $index_activeusername)) {
				echo "<a class=\"silverbutton\" onClick=\"index_js_message('$index_get_page', 0);\">$index_newpostlabel</a>";

				if (($index_activeuserid > 0) && ($index_page == 'profile')
						&& (strtolower($index_activeusername) != strtolower($index_get_page))) {
					echo " <a class=\"silverbutton\" onclick=\"index_js_addContact('$index_get_page', false);\">Add Contact</a>";
				}
			} else {
?>
				<div style="flex-grow:1;">
					<div style="float:right;">
						<div id="templatesearchcontainer" class="shadowinset templatesearchcontainer">
							<input id="templatesearchtext" type="text" autocomplete="off" placeholder="Templates... [ ctrl space ]"
									style="border:0;padding:0;margin:0;background:none;flex-grow:1;"
									onFocus="index_js_setupTemplateSearch(this, <?=$newpostparentid?>, '<?=$newpostpermissions?>', '<?=$newpostrecipients?>');">
						</div>
						<div id="templatesearchpopup" class="templatesearchpopup"
								style="flex-direction:column;">
							<div>
								<div class="templatesearchsource">
									<label for="templatecheckbox_0"><input id="templatecheckbox_0" type="checkbox" onchange="index_js_templatecheckboxchanged(this);"> Personal Templates</label>
									&nbsp; <a class="greenlink" onclick="$('templatesearchfile').click();">Upload template file</a>
									<input id="templatesearchfile" type="file" name="templatesearchfile"
											value="Upload Template Index" onChange="index_js_uploadTemplateIndex()" style="display:none;">
								</div>
								<div id="templatesearchindexes"></div>
							</div>
							<div id="templatesearchresults" class="templatesearchresults" style="flex-grow:1;">
								Enter keyword(s) to search.
							</div>
						</div>
						<a id="newpostbutton" class="silverbutton" onClick="index_js_setupPostEditor(this, 'postblock_NEWPOST', 'post', <?=$newpostparentid?>, false,
								'<?=$newpostpermissions?>', 3, '<?=$newpostrecipients?>', '<?=$index_page?>', '', '<?=$index_viewmode?>', '', false);"><div class="silverbuttonflex"><?=$index_newpostlabel?> <span class="silverbuttonkey">ctrl&nbsp;enter</span></div></a>
<?php
				if ($index_page_userpermissions && ($index_page_userpermissions['replypermissions'] === '5')) {
					echo " <a class=\"silverbutton\" onClick=\"prompt("
							."'To reply to this post by email,\\nsend an email message to:',"
							." '{$index_page_id}'+'@'+'{$servername}')\">Reply by email</a>";
				}
?>
					</div>
				</div>
<?php
			}
		} else if ($index_page_id && $index_page_userpermissions
				&& ($index_page != 'analytics')) {
			if ($index_page_userpermissions['replypermissions'] == 4) {
				echo '<div style="flex-grow:1;"><div style="float:right;">'
						.'<a class="ctalink" href="#userpanel" style="margin:0 4px;">Sign In</a>'
						.' or <a class="ctalink" href="#rightpanel" style="margin:0 4px;">Sign in</a> to reply.</div></div>';
			} else if ($index_page_userpermissions['replypermissions'] == 3) {
				if ($index_activeuserid != 0) {
					echo '<span class="ctalink">Join this group to reply.</span> ';
				} else {
					echo '<div style="flex-grow:1;"><div style="float:right;">'
							.'<a class="ctalink" href="#userpanel" style="margin:0 4px;">Sign In</a>'
							.' or <a class="ctalink" href="#rightpanel" style="margin:0 4px;">Sign in</a> to join this group.</div></div>';
				}
			}
		}

		$additionalmessagerinputvar = '';
		$additionalmessagerclasses = '';
		$messagericonpermissionsmembers = 'icon-permissions-members.png';
		$messagericonpermissionscontacts = 'icon-permissions-contacts.png';
		$messagericonpermissionsowner = 'icon-permissions-owner.png';
		$messagericonpostpending = 'icon-post-pending.png';
		if (!$index_activeuserid) {
			$additionalmessagerinputvar = ' disabled';
			$additionalmessagerclasses = ' editpermissionspresetlabeldisabled';
			$messagericonpermissionsmembers = 'icon-permissions-members-disabled.png';
			$messagericonpermissionscontacts = 'icon-permissions-contacts-disabled.png';
			$messagericonpermissionsowner = 'icon-permissions-owner-disabled.png';
			$messagericonpostpending = 'icon-post-pending-disabled.png';
		}
?>
			</div>
		</div>
		<div id="editpostcancel" class="editpostcancel">
			<span class="tooltiphost">
				<input id="editpostcancelbutton" type="image" src="/img/editorstatusbar-cancel.png" class="imagebutton" onclick="return index_js_cancelEditPost(false);">
				<span class="tooltiptext tooltiptextright">Cancel <span class="tooltipkey">esc</span></span>
			</span>
		</div>
		<div id="editpostcontrols">
			<textarea id="editposthtmleditor"></textarea>
			<form class="editpostcontrolsform" method="post" enctype="multipart/form-data" onSubmit="index_js_sendNewPost(this); return false;">
				<div class="editpostpopups">
					<span id="editpostpermissions" class="editpostpermissionsbutton" onMouseOver="index_js_showeditorpopup(this.id, 'editpostpermissionspopup', true);">
						<img id="editpostpermissionsicon" class="editpostpermissionsicon" src="/img/icon-permissions-open.png"><img id="editpostrepliesicon" class="editpostpermissionsicon" src="/img/icon-post-approved.png"><img id="editpostpermissionsiconadvanced" class="editpostpermissionsicon" src="/img/icon-permissions-advanced.png"><span id="editpostpermissionstitle"></span>
						<div id="editpostpermissionspopup" class="permissionspopup">
							<div style="text-align:right;">
								<span class="tooltiphost">
									<input id="editpostadvancedpermissions" class="togglecheckbox" type="checkbox" name="editpostadvancedpermissions" value="advanced"
											onChange="index_js_switchAdvancedPermissions('editpost');">
									<label id="editpostadvancedpermissionslabel" for="editpostadvancedpermissions"><img src="/img/icon-permissions-advanced.png" alt="Advanced permissions"> Advanced permissions</label>
									<span id="editpostadvancedpermissionstooltip" class="tooltiptext tooltiptexttop tooltiptextright">Click for advanced permissions.</span>
								</span>
							</div>
							<div id="editpostpermissionsadvanced" class="editpermissionsadvanced">
								<div class="editpopuppermissions">
									<div class="editpopuppermissionsrow">
										<div class="editpopuppermissionscell">View</div>
										<div class="editpopuppermissionscell editpostpermissionsbar">
											<span>
												<input id="editpostview5" type="radio" name="editpostview" value="5"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostview5"><img src="/img/icon-permissions-open.png" alt="open" title="Anybody can see this"></label>
											</span>
											<span>
												<input id="editpostview4" type="radio" name="editpostview" value="4"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostview4"><img src="/img/icon-permissions-all.png" alt="all" title="Users on this site can see this"></label>
											</span>
											<span>
												<input id="editpostview3" type="radio" name="editpostview" value="3"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostview3"><img src="/img/<?=$messagericonpermissionsmembers?>" alt="members" title="Group members can see this"></label>
											</span>
											<span>
												<input id="editpostview2" type="radio" name="editpostview" value="2"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostview2"><img src="/img/<?=$messagericonpermissionscontacts?>" alt="contacts" title="Starred contacts can see this"></label>
											</span>
											<span>
												<input id="editpostview1" type="radio" name="editpostview" value="1"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostview1"><img src="/img/icon-permissions-recipient.png" alt="recipient" title="The sender and recipient(s) can see this"></label>
											</span>
											<span>
												<input id="editpostview0" type="radio" name="editpostview" value="0"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostview0"><img src="/img/<?=$messagericonpermissionsowner?>" alt="owner" title="Only the owner can see this"></label>
											</span>
										</div>
									</div>
									<div class="editpopuppermissionsrow">
										<div class="editpopuppermissionscell">Edit</div>
										<div class="editpopuppermissionscell editpostpermissionsbar">
											<span>
												<input id="editpostedit5" type="radio" name="editpostedit" value="5"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostedit5"><img src="/img/icon-permissions-open.png" alt="open" title="Anybody can edit this"></label>
											</span>
											<span>
												<input id="editpostedit4" type="radio" name="editpostedit" value="4"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostedit4"><img src="/img/icon-permissions-all.png" alt="all" title="Users on this site can edit this"></label>
											</span>
											<span>
												<input id="editpostedit3" type="radio" name="editpostedit" value="3"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostedit3"><img src="/img/<?=$messagericonpermissionsmembers?>" alt="members" title="Group members can edit this"></label>
											</span>
											<span>
												<input id="editpostedit2" type="radio" name="editpostedit" value="2"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostedit2"><img src="/img/<?=$messagericonpermissionscontacts?>" alt="contacts" title="Starred contacts can edit this"></label>
											</span>
											<span>
												<input id="editpostedit1" type="radio" name="editpostedit" value="1"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostedit1"><img src="/img/icon-permissions-recipient.png" alt="recipient" title="The sender and recipient(s) can edit this"></label>
											</span>
											<span>
												<input id="editpostedit0" type="radio" name="editpostedit" value="0"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostedit0"><img src="/img/<?=$messagericonpermissionsowner?>" alt="owner" title="Only the owner can edit this"></label>
											</span>
										</div>
									</div>
									<div class="editpopuppermissionsrow">
										<div class="editpopuppermissionscell">Reply</div>
										<div class="editpopuppermissionscell editpostpermissionsbar">
											<span>
												<input id="editpostreply5" type="radio" name="editpostreply" value="5"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostreply5"><img src="/img/icon-permissions-open.png" alt="open" title="Anybody can reply"></label>
											</span>
											<span>
												<input id="editpostreply4" type="radio" name="editpostreply" value="4"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostreply4"><img src="/img/icon-permissions-all.png" alt="all" title="Users on this site can reply"></label>
											</span>
											<span>
												<input id="editpostreply3" type="radio" name="editpostreply" value="3"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostreply3"><img src="/img/<?=$messagericonpermissionsmembers?>" alt="members" title="Group members can reply"></label>
											</span>
											<span>
												<input id="editpostreply2" type="radio" name="editpostreply" value="2"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostreply2"><img src="/img/<?=$messagericonpermissionscontacts?>" alt="contacts" title="Starred contacts can reply"></label>
											</span>
											<span>
												<input id="editpostreply1" type="radio" name="editpostreply" value="1"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostreply1"><img src="/img/icon-permissions-recipient.png" alt="recipient" title="The sender and recipient(s) can reply"></label>
											</span>
											<span>
												<input id="editpostreply0" type="radio" name="editpostreply" value="0"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostreply0"><img src="/img/<?=$messagericonpermissionsowner?>" alt="owner" title="Only the owner can reply"></label>
											</span>
										</div>
									</div>
									<div class="editpopuppermissionsrow">
										<div class="editpopuppermissionscell">Approve</div>
										<div class="editpopuppermissionscell editpostpermissionsbar">
											<span>
												<input id="editpostapprove5" type="radio" name="editpostapprove" value="5"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostapprove5"><img src="/img/icon-permissions-open.png" alt="open" title="All replies are pre-approved"></label>
											</span>
											<span>
												<input id="editpostapprove4" type="radio" name="editpostapprove" value="4"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostapprove4"><img src="/img/icon-permissions-all.png" alt="all" title="Users on this site replies pre-approved"></label>
											</span>
											<span>
												<input id="editpostapprove3" type="radio" name="editpostapprove" value="3"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostapprove3"><img src="/img/<?=$messagericonpermissionsmembers?>" alt="members" title="Group members have replies pre-approved"></label>
											</span>
											<span>
												<input id="editpostapprove2" type="radio" name="editpostapprove" value="2"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostapprove2"><img src="/img/<?=$messagericonpermissionscontacts?>" alt="contacts" title="Starred contacts have replies pre-approved"></label>
											</span>
											<span>
												<input id="editpostapprove1" type="radio" name="editpostapprove" value="1"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostapprove1"><img src="/img/icon-permissions-recipient.png" alt="recipient" title="The sender and recipient(s) have replies pre-approved"></label>
											</span>
											<span>
												<input id="editpostapprove0" type="radio" name="editpostapprove" value="0"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostapprove0"><img src="/img/<?=$messagericonpermissionsowner?>" alt="owner" title="Only the owner has replies pre-approved"></label>
											</span>
										</div>
									</div>
									<div class="editpopuppermissionsrow">
										<div class="editpopuppermissionscell">Moderate</div>
										<div class="editpopuppermissionscell editpostpermissionsbar">
											<span>
												<input id="editpostmoderate5" type="radio" name="editpostmoderate" value="5"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostmoderate5"><img src="/img/icon-permissions-open.png" alt="open" title="Anybody can moderate replies to this post"></label>
											</span>
											<span>
												<input id="editpostmoderate4" type="radio" name="editpostmoderate" value="4"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostmoderate4"><img src="/img/icon-permissions-all.png" alt="all" title="Users on this site can moderate replies"></label>
											</span>
											<span>
												<input id="editpostmoderate3" type="radio" name="editpostmoderate" value="3"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostmoderate3"><img src="/img/<?=$messagericonpermissionsmembers?>" alt="members" title="Group members can moderate replies"></label>
											</span>
											<span>
												<input id="editpostmoderate2" type="radio" name="editpostmoderate" value="2"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostmoderate2"><img src="/img/<?=$messagericonpermissionscontacts?>" alt="contacts" title="Starred contacts can moderate replies"></label>
											</span>
											<span>
												<input id="editpostmoderate1" type="radio" name="editpostmoderate" value="1"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostmoderate1"><img src="/img/icon-permissions-recipient.png" alt="recipient" title="The sender and recipient(s) can moderate replies"></label>
											</span>
											<span>
												<input id="editpostmoderate0" type="radio" name="editpostmoderate" value="0"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editpostmoderate0"><img src="/img/<?=$messagericonpermissionsowner?>" alt="owner" title="Only the owner can moderate replies"></label>
											</span>
										</div>
									</div>
									<div class="editpopuppermissionsrow">
										<div class="editpopuppermissionscell">Tags</div>
										<div class="editpopuppermissionscell editpostpermissionsbar">
											<span>
												<input id="editposttag5" type="radio" name="editposttag" value="5"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editposttag5"><img src="/img/icon-permissions-open.png" alt="open" title="Anybody can add tags"></label>
											</span>
											<span>
												<input id="editposttag4" type="radio" name="editposttag" value="4"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editposttag4"><img src="/img/icon-permissions-all.png" alt="all" title="Users on this site can add tags"></label>
											</span>
											<span>
												<input id="editposttag3" type="radio" name="editposttag" value="3"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editposttag3"><img src="/img/<?=$messagericonpermissionsmembers?>" alt="members" title="Group members can add tags"></label>
											</span>
											<span>
												<input id="editposttag2" type="radio" name="editposttag" value="2"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editposttag2"><img src="/img/<?=$messagericonpermissionscontacts?>" alt="contacts" title="Starred contacts can add tags"></label>
											</span>
											<span>
												<input id="editposttag1" type="radio" name="editposttag" value="1"
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editposttag1"><img src="/img/icon-permissions-recipient.png" alt="recipient" title="The sender and recipient(s) can add tags"></label>
											</span>
											<span>
												<input id="editposttag0" type="radio" name="editposttag" value="0"<?=$additionalmessagerinputvar?>
														onChange="index_js_updatePostPermissions('editpost', this.id);">
												<label for="editposttag0"><img src="/img/<?=$messagericonpermissionsowner?>" alt="owner" title="Only the owner can add tags"></label>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div id="editpostpermissionspresets">
								<fieldset>
									<legend>Visibility</legend>
									<div class="editpermissionspreset">
										<input id="editpostpreset5" type="radio" name="editpostpreset" value="5" class="editpermissionspresetinput"
												onChange="index_js_updatePostPermissions('editpost', this.id);" checked>
										<div class="editpermissionspresetlabel">
											<label for="editpostpreset5"><img src="/img/icon-permissions-open.png" alt="open"> Anyone</label>
											<span class="editpermissionspresetunsigned">
												<label for="editpostallowunsigned"><input id="editpostallowunsigned" class="togglecheckbox" type="checkbox" name="editpostallowunsigned" value="allowunsigned" onChange="index_js_updatePostPermissions('editpost', this.id);">&nbsp;Allow guest &amp; email posts</label>
											</span>
										</div>
									</div>
									<div class="editpermissionspreset">
										<input id="editpostpreset2" type="radio" name="editpostpreset" value="2" class="editpermissionspresetinput"
												onChange="index_js_updatePostPermissions('editpost', this.id);"<?=$additionalmessagerinputvar?>>
										<div class="editpermissionspresetlabel<?=$additionalmessagerclasses?>">
											<label for="editpostpreset2"><img src="/img/<?=$messagericonpermissionscontacts?>" alt="contacts"> Starred contacts</label>
										</div>
									</div>
									<div class="editpermissionspreset">
										<input id="editpostpreset1" type="radio" name="editpostpreset" value="1" class="editpermissionspresetinput"
												onChange="index_js_updatePostPermissions('editpost', this.id);">
										<div class="editpermissionspresetlabel">
											<label for="editpostpreset1"><img src="/img/icon-permissions-recipient.png" alt="recipient"> Recipients</label>
										</div>
									</div>
									<div class="editpermissionspreset">
										<input id="editpostpreset0" type="radio" name="editpostpreset" value="0" class="editpermissionspresetinput"
												onChange="index_js_updatePostPermissions('editpost', this.id);"<?=$additionalmessagerinputvar?>>
										<div class="editpermissionspresetlabel<?=$additionalmessagerclasses?>">
											<label for="editpostpreset0"><img src="/img/<?=$messagericonpermissionsowner?>" alt="owner"> Only the owner</label>
										</div>
									</div>
								</fieldset>
								<fieldset>
									<legend>Replies</legend>
									<span class="editpermissionspreset">
										<input id="editpostpresetacceptreplies" type="radio" name="editpostreplies" value="acceptreplies" class="editpermissionspresetinput"
												onChange="index_js_updatePostPermissions('editpost', this.id);" checked>
										<label for="editpostpresetacceptreplies" class="editpermissionspresetlabel"><img src="/img/icon-post-approved.png" alt="Show All Replies"> Show All</label>
									</span>
									<span class="editpermissionspreset">
										<input id="editpostpresetrepliesneedapproval" type="radio" name="editpostreplies" value="repliesneedapproval" class="editpermissionspresetinput"
												onChange="index_js_updatePostPermissions('editpost', this.id);"<?=$additionalmessagerinputvar?>>
										<label for="editpostpresetrepliesneedapproval" class="editpermissionspresetlabel<?=$additionalmessagerclasses?>"><img src="/img/<?=$messagericonpostpending?>" alt="Replies Need Approval"> Need Approval</label>
									</span>
									<span class="editpermissionspreset">
										<input id="editpostpresetdisablereplies" type="radio" name="editpostreplies" value="disablereplies" class="editpermissionspresetinput"
												onChange="index_js_updatePostPermissions('editpost', this.id);">
										<label for="editpostpresetdisablereplies" class="editpermissionspresetlabel"><img src="/img/icon-post-blocked.png" alt="Disable Replies"> Disabled</label>
									</span>
								</fieldset>
							</div>
						</div>
					</span>
					<span id="editpostpermissionsrating" class="editpostpermissionsbutton" onMouseOver="index_js_showeditorpopup(this.id, 'editpostpermissionsageratingpopup', true);">
						<img id="editpostcontentratingicon" class="editpostpermissionsicon" src="/img/icon-permissions-generalcontent.png"><span id="editpostpermissionsageratingtitle">General</span>
						<div id="editpostpermissionsageratingpopup" class="permissionspopup">
							<div id="editpostpermissionspresets">
								<fieldset>
									<legend>Content Suitability</legend>
									<span class="editpermissionspreset">
										<input id="editpostpresetgeneralcontent" type="radio" name="editpostcontentrating" value="general" class="editpermissionspresetinput"
												onChange="index_js_updatePostPermissions('editpost', this.id);" checked>
										<label for="editpostpresetgeneralcontent" class="editpermissionspresetlabel"><img src="/img/icon-permissions-generalcontent.png" alt="General content"> General</label>
									</span>
									<span class="editpermissionspreset">
										<input id="editpostpresetmature15" type="radio" name="editpostcontentrating" value="mature15" class="editpermissionspresetinput"
												onChange="index_js_updatePostPermissions('editpost', this.id);">
										<label for="editpostpresetmature15" class="editpermissionspresetlabel"><img src="/img/icon-permissions-mature15.png" alt="15+ mature content"> Mature</label>
									</span>
									<span class="editpermissionspreset">
										<input id="editpostpresetadult18" type="radio" name="editpostcontentrating" value="adult18" class="editpermissionspresetinput"
												onChange="index_js_updatePostPermissions('editpost', this.id);">
										<label for="editpostpresetadult18" class="editpermissionspresetlabel"><img src="/img/icon-permissions-adult18.png" alt="18+ adult content"> Adult</label>
									</span>
								</fieldset>
								<div>
									<label for="editpostagerating">Age rating:</label>
									<input id="editpostagerating" name="editpostagerating" type="number" min="3" max="25" class="shadowinset" autocomplete="off"
											onChange="index_js_updatePostPermissions('editpost', this.id);">
								</div>
							</div>
						</div>
					</span>
					<span id="editpostpermissionsreplies" class="editpostpermissionsbutton" onMouseOver="index_js_showeditorpopup(this.id, 'editpostpermissionsrepliespopup', true);">
						<img id="editpostviewmodeicon" class="editpostpermissionsicon" src="/img/icon-viewmode-full.png"><img id="editpostsortmodeicon" class="editpostpermissionsicon" src="/img/icon-sortmode-date.png"><span id="editpostpermissionsrepliestitle"></span>
						<div id="editpostpermissionsrepliespopup" class="permissionspopup">
							<div id="editpostpermissionspresets">
								<fieldset>
									<legend>Show Replies As</legend>
									<span class="editpermissionspreset">
										<input id="editpostviewmodefull" type="radio" name="editpostviewmode"
												value="full" class="editpermissionspresetinput"
												onChange="index_js_updatePostPermissions('editpost', this.id);" checked>
										<label for="editpostviewmodefull" class="editpermissionspresetlabel"><img src="/img/icon-viewmode-full.png"
												alt="Show Full Posts"> Full Posts</label>
									</span>
									<span class="editpermissionspreset">
										<input id="editpostviewmodethumbnail" type="radio" name="editpostviewmode"
												value="thumbnail" class="editpermissionspresetinput"
												onChange="index_js_updatePostPermissions('editpost', this.id);">
										<label for="editpostviewmodethumbnail" class="editpermissionspresetlabel"><img src="/img/icon-viewmode-thumbnail.png"
												alt="Show as Thumbnails"> Thumbnails</label>
									</span>
									<span class="editpermissionspreset">
										<input id="editpostviewmodelist" type="radio" name="editpostviewmode"
												value="list" class="editpermissionspresetinput"
												onChange="index_js_updatePostPermissions('editpost', this.id);">
										<label for="editpostviewmodelist" class="editpermissionspresetlabel"><img src="/img/icon-viewmode-list.png"
												alt="Show List Items"> List Items</label>
									</span>
								</fieldset>
								<fieldset>
									<legend>Sort Replies By</legend>
									<span class="editpermissionspreset">
										<input id="editpostsortmodedate" type="radio" name="editpostsortmode"
												value="date" class="editpermissionspresetinput"
												onChange="index_js_updatePostPermissions('editpost', this.id);" checked>
										<label for="editpostsortmodedate" class="editpermissionspresetlabel"><img src="/img/icon-sortmode-date.png"
												alt="Sort by Date"> Date</label>
									</span>
									<span class="editpermissionspreset">
										<input id="editpostsortmodetitle" type="radio" name="editpostsortmode"
												value="title" class="editpermissionspresetinput"
												onChange="index_js_updatePostPermissions('editpost', this.id);">
										<label for="editpostsortmodetitle" class="editpermissionspresetlabel"><img src="/img/icon-sortmode-title.png"
												alt="Sort by Title"> Title</label>
									</span>
									<span class="editpermissionspreset">
										· <label for="editpostsortreverse" class="editpermissionspresetlabel"><input id="editpostsortreverse" type="checkbox" name="editpostsortreverse" value="reverse" class="togglecheckbox" onChange="index_js_updatePostPermissions('editpost', this.id);">&nbsp;Reverse</label>
									</span>
								</fieldset>
							</div>
						</div>
					</span>
				</div>
				<div class="editpostbuttons">
					<img id="editpostfileupload" src="/img/fileupload.gif">
					<span id="editpostsuggestions" class="editpostsuggestions">&nbsp;</span>
					<input id="editpostajaxcommand" type="hidden" name="ajax_command" value="uninitialised">
					<input id="editpostfile" class="editposthidden" type="file" name="files[]"
							value="Upload Photo/File" onChange="index_js_previewImage('editpost')">
<?php
		/* Only show upload features that upload files if signed in. */
		if ($index_activeuserid) {
?>
					<span class="tooltiphost">
						<input id="editposthtmlbutton" type="image" src="/img/editorstatusbar-html.png" class="imagebutton" onclick="return index_js_editPostHTML();">
						<span class="tooltiptext tooltiptextright">Show/hide HTML editor</span>
					</span>
					<span class="tooltiphost">
						<input type="image" src="/img/editorstatusbar-attach.png" class="imagebutton" onclick="return index_js_editPostAttach();">
						<span class="tooltiptext tooltiptextright">Attach an image or file</span>
					</span>
<?php
		}
?>
					<span class="tooltiphost">
						<input type="image" src="/img/editorstatusbar-pastelink.png" class="imagebutton" onclick="return index_js_editPostPasteLink();">
						<span class="tooltiptext tooltiptextright">Paste Link</span>
					</span>
					<span class="tooltiphost">
						<input type="image" src="/img/editorstatusbar-undo.png" class="imagebutton" onclick="return index_js_editPostUndo();">
						<span class="tooltiptext tooltiptextright">Undo</span>
					</span>
					<span class="tooltiphost">
						<input type="image" src="/img/editorstatusbar-redo.png" class="imagebutton" onclick="return index_js_editPostRedo();">
						<span class="tooltiptext tooltiptextright">Redo</span>
					</span>
					<span class="tooltiphost">
						<input id="editpostsubmit" class="silverbutton" type="submit" name="post" value="Post">
						<span class="tooltiptext tooltiptextright"><span class="tooltipkey">ctrl enter</span></span>
					</span>
				</div>
			</form>
		</div><!-- End of #editpostcontrols -->
	</div>
<?php
	}

	$mainflexboxclass = '';
	if ($index_viewmode == 'full') {
		/* $mainflexboxclass = 'mainflexboxpadded'; */
	} else if ($index_viewmode == 'thumbnail') {
		$mainflexboxclass = 'mainflexboxflex';
	} else if ($index_viewmode == 'list') {
		$mainflexboxclass = 'mainflexboxlist';
	}
?>
	<div id="mainflexboxparent">
	<div id="mainflexbox" class="<?=$mainflexboxclass?>">
<?php
/* TODO: MERGESELECTPOST. */
$querycriteria = '';
$pinparentid = 0;
if ($index_page == 'profile') {
	$querycriteria = sprintf('posts.accountid = (select id from accounts where username = "%s" limit 1)' /* USERNAME */
			.' and posts.viewpermissions != 1 and (posts.parentid = 0'
			.' 	or ((select p.accountid from posts as p where p.id = posts.parentid) != posts.accountid))',
			$index_page_id);
} else if (($index_page == 'page') && $index_page_id) {
	$querycriteria = sprintf('posts.parentid = %d', $index_page_id);
	$pinparentid = $index_page_id;
} else if ($index_page == 'emails') {
	$querycriteria = sprintf('posts.emailusername = "%s" and posts.emailserver = "%s"',
			func_php_escape($index_emailusername),
			func_php_escape($index_emailserver));
} else if ($index_showposts) { /* if (!$index_page) */
	$querycriteria = sprintf('posts.parentid = 0 and (posts.viewpermissions = 5'
			.' or (posts.viewpermissions = 4 and %d != 0))', $index_activeuserid); /* USERID */

	/* Home stream filtering */
	if ($index_get_page == 'new') {
		/* Ignore home page filtering */
	} else if ($index_activeuserid != 0) {
		$homepagefilter = '';

		if ($index_activeuserid != 0) {
			if ($homepagefilter)
				$homepagefilter .= ' or ';

			$homepagefilter .= sprintf('accounts.id = %d', $index_activeuserid);
		}

		/* Always show posts from trusted contacts. */
		if ($index_activeuserid != 0) {
			if ($homepagefilter)
				$homepagefilter .= ' or ';

			$homepagefilter .= sprintf('/* '.__LINE__.' */ accounts.id in (select contactaccounts.id from contacts'
					.' inner join accounts as contactaccounts on contacts.contactid = contactaccounts.id'
					.' inner join accounts as activeuseraccount on contacts.accountid = activeuseraccount.id'
					.' where activeuseraccount.username = "%s")', $index_activeusername); /* USERNAME */
		}

		$querycriteria .= " and ($homepagefilter)";
	}
} else {
	$querycriteria = '';
	$querycriteriaand = '';
}

if ($querycriteria) {
	$querycriteriaand = " and $querycriteria";
}

/* Build the final query using post id or profile id criteria. */
/* TODO: Cull values out of this query that are now generated in
 * ..._getpostpermissions() and ..._getuserpermissions() instead. */
$query = sprintf('/* '.__LINE__. ' */ select %d as activeuserid, posts.*,' /* USERID */
		.' parentpost.accountid as parentaccountid, parentpost.editpermissions as parenteditpermissions,'
		.' parentpost.replypermissions as parentreplypermissions,'
		.' parentpost.approvepermissions as parentapprovepermissions,'
		.' accounts.id as authorid, accounts.username as authorusername, accounts.fullname as authorfullname,'
		.' concat(posts.viewpermissions, posts.editpermissions, posts.replypermissions, posts.approvepermissions,'
		.' posts.moderatepermissions, posts.tagpermissions) as permissions, posts.agerating,'
		.' (select group_concat(distinct accounts.username separator " ") from accounts, recipients'
		.' where accounts.id = recipients.recipientaccountid and recipients.tablename = "posts"'
		.' and recipients.tableid = posts.id) as recipients,'
		.' ((select count(*) from members left join membertree on members.postid = membertree.parentid'
		.' 	where members.accountid = posts.accountid and membertree.postid = posts.id'
		.' 	and members.trusted = 1 /* and members.accountid != posts.accountid */ )'
		.' 	+ (select count(*) from members where members.accountid = posts.accountid'
		.' 	and members.postid = posts.id and members.trusted = 1 /* and members.accountid != posts.accountid */))'
		.' 	as postuseristrustedmember,'
		.' approvals.id as postapprovalid, approvals.trusted as postapprovaltrusted,'
		.' approvals.hidden as postapprovalhidden, approvals.added as postapprovaladded,'
		.' ((select count(*) from members left join membertree on members.postid = membertree.parentid'
		.' 	where members.accountid = %d and membertree.postid = posts.id' /* USERID */
		.' 	and members.trusted = 1 and members.accountid != posts.accountid)'
		.' 	+ (select count(*) from members where members.accountid = %d' /* USERID */
		.' 	and members.postid = posts.id and members.trusted = 1 and members.accountid != posts.accountid))'
		.' 	as activeuseristrustedmember,'
		.' (select count(trusted) from contacts where contacts.accountid = posts.accountid'
		.' 	and contacts.contactid = activeuserid and contacts.trusted = 1) as activeuseristrustedcontact,'
		.' (select count(trusted) from contacts where contacts.accountid = parentaccountid'
		.' 	and contacts.contactid = posts.accountid and contacts.trusted = 1) as postuseristrustedcontact,'
		.' (select count(recipientaccountid) from recipients'
		.' where recipients.tablename = "posts" and recipients.tableid = posts.id'
		.' 	and recipients.recipientaccountid = activeuserid) as activeuserisrecipient,'
		.' (select count(recipients.recipientaccountid) from recipients, membertree'
		.' 	where membertree.postid = posts.id and recipients.tablename = "posts"'
		.' 	and recipients.tableid = membertree.parentid and recipients.recipientaccountid = posts.accountid)'
		.' 	as postuserisrecipientofparent,'
		.' flags.flagcount, flags.flagactive, flags.flagreason,'
		.' flags.flagagerating, flags.updated as flagupdated, pins.pinstate'
		.' from posts left join accounts on posts.accountid = accounts.id'
		.' left join posts as parentpost on parentpost.id = posts.parentid'
		.' left join flags on flags.tablename = "posts" and flags.tableid = posts.id'
		.' left join approvals on approvals.tablename = "posts" and approvals.tableid = posts.id'
		.' 	and approvals.accountid = posts.accountid'
		.' left join pins on pins.postid = posts.id and pins.parentpostid = %d' /* PINPARENTID */
		.' where ', $index_activeuserid, $index_activeuserid, $index_activeuserid, $pinparentid).$querycriteria;

/* View Permissions! (ie. If the currently signed in user can view the post.) */
$query .= ' /* '.__LINE__.':ViewPermissionsFiltering */ and (posts.id in (' /* Open filtering bracket, closes below */

		.sprintf('select posts.id from posts'
		.' where (posts.viewpermissions = 5 or ((posts.accountid = %d' /* USERID */
		.' or posts.viewpermissions = 4) and %d != 0))', /* USERID */
		$index_activeuserid, $index_activeuserid).$querycriteriaand

		.sprintf(' UNION select posts.id from posts, recipients'
		.' where posts.viewpermissions = 1 and recipients.tablename = "posts" and recipients.tableid = posts.id'
		.' and ((recipients.senderaccountid = %d or recipients.recipientaccountid = %d)' /* USERID, USERID */
		.' 	and %d != 0)', /* USERID */
		$index_activeuserid, $index_activeuserid, $index_activeuserid).$querycriteriaand

		.sprintf(' UNION select posts.id from posts, contacts'
		.' where posts.viewpermissions = 2 and posts.accountid = contacts.accountid'
		.' and contacts.trusted = 1 and contacts.contactid = %d', /* USERID */
		$index_activeuserid).$querycriteriaand

		.sprintf(' UNION select posts.id' /* USERID */
		.' from posts, members, membertree, posts as parentpost'
		.' where posts.viewpermissions = 3 and (membertree.postid = posts.id or membertree.parentid = posts.id)'
		.' and membertree.parentid = members.postid'
		.' and ((members.trusted = 1 and members.accountid = %d)' /* USERID */
		.' 	or (parentpost.id = membertree.parentid and parentpost.accountid = %d))', /* USERID */
		$index_activeuserid, $index_activeuserid).$querycriteriaand

		.')';

/* Filter reply/approve permissions, if we're displaying a post page. */
if (($index_page == 'page') && $index_page_id) {
	$query .= ' /* '.__LINE__.':PostReplyApprovePermissionsFilter */ and posts.id in ('

			/* Filter reply permissions. */
			.sprintf('select posts.id from posts, posts as parentpost where posts.parentid = parentpost.id'
					.' and (posts.accountid = parentpost.accountid'
					.' or (posts.accountid = %d and %d != 0)' /* USERID, USERID */
					.' or parentpost.replypermissions = 0 and parentpost.accountid = %d' /* USERID */
					.' or parentpost.replypermissions = 5'
					.' or (parentpost.replypermissions = 4 and posts.accountid != 0))', /* USERID */
					$index_activeuserid,  $index_activeuserid, $index_activeuserid).$querycriteriaand

			.sprintf(' UNION select posts.id from posts, posts as parentpost, recipients'
					.' where posts.parentid = parentpost.id'
					.' and parentpost.replypermissions = 1 and recipients.tablename = "posts"'
					.' and recipients.tableid = parentpost.id'
					.' and recipients.recipientaccountid = posts.accountid').$querycriteriaand

			.sprintf(' UNION select posts.id from posts, posts as parentpost, contacts'
					.' where posts.parentid = parentpost.id'
					.' and parentpost.replypermissions = 2 and contacts.accountid = parentpost.accountid'
					.' and contacts.contactid = posts.accountid'
					.' and contacts.trusted = 1').$querycriteriaand

			.sprintf(' UNION select posts.id from membertree inner join posts on membertree.postid = posts.id'
					.' inner join members on membertree.parentid = members.postid'
					.' inner join posts as parentpost on parentpost.id = posts.parentid'
					.' where members.accountid = posts.accountid and parentpost.replypermissions = 3'
					.' and members.trusted = 1').$querycriteriaand

			/* Filter by approve permissions. */

			.sprintf(' UNION select posts.id from membertree inner join posts on membertree.postid = posts.id'
					.' inner join members on membertree.parentid = members.postid'
					.' inner join posts as parentpost on parentpost.id = posts.parentid'
					.' where members.accountid = posts.accountid and parentpost.approvepermissions = 3'
					.' and members.trusted = 1').$querycriteriaand

			.')';
}
$query .= ')'; /* Close filtering bracket */

/* Sort functionality goes here. */
$sortorder = '';
switch ($index_get_sort) {
case 'title':
	if ($index_get_order === 'reverse') {
		$sortorder = 'posts.searchcache desc';
	} else {
		$sortorder = 'posts.searchcache asc';
	}
	break;
default:
	if ($index_get_order === 'reverse') {
		$sortorder = 'posts.added asc';
	} else {
		$sortorder = 'posts.added desc';
	}
}
$query .= sprintf(' order by %s limit %d, %d', $sortorder,
		$index_page_offset, $index_page_count+1);

$index_recentposts = false;
if ($index_showposts) {
	$index_recentposts = func_php_query($query);
}

$postnumrows = 0;
$postcount = 0;
if ($index_recentposts && mysqli_num_rows($index_recentposts)) {
	/* TODO: MERGESELECTPOST - This code is also used for the "page" section.
	 * It needs to be broken off as a function to simplify things! */
	$postnumrows = mysqli_num_rows($index_recentposts);
	$postdirectory = false; /* Used to start and close the directory list UL */
	while (($post = mysqli_fetch_assoc($index_recentposts)) && ($postcount < $index_page_count)) {
		$postid = $post['id'];
		$authorid = $post['accountid'];
		$authorusername = $post['authorusername'];
		$authorfullname = $post['authorfullname'];
		$postparentid = $post['parentid'];
		$parentaccountid = $post['parentaccountid']; /* DELINT */
		$parenteditpermissions = $post['parenteditpermissions']; /* DELINT */
		$parentreplypermissions = $post['parentreplypermissions']; /* DELINT */
		$parentapprovepermissions = $post['parentapprovepermissions']; /* DELINT */
		$postisdirectory = $post['isdirectory'];
		$postdate = $post['added'];
		$postpermissions = $post['permissions'];
		$postagerating = $post['agerating'];
		$postviewmode = $post['viewmode'];
		$postsortmode = $post['sortmode'];
		$postsortreverse = $post['sortreverse'];
		$postuseristrustedmember = $post['postuseristrustedmember'];
		$postuseristrustedcontact = $post['postuseristrustedcontact'];
		$postuserisrecipientofparent = $post['postuserisrecipientofparent'];
		$postapprovalid = $post['postapprovalid'];
		$postapprovaltrusted = $post['postapprovaltrusted'];
		$postapprovalhidden = $post['postapprovalhidden'];
		$postapprovaladded = $post['postapprovaladded'];
		$postrecipients = $post['recipients'];
		$postflagcount = $post['flagcount'];
		$postflagactive = $post['flagactive'];
		$postflagupdated = $post['flagupdated'];
		$postflagreason = $post['flagreason'];
		$postflagagerating = $post['flagagerating'];
		$postpinstate = $post['pinstate'];
		//$recipientid = $post['recipientid'];
		$additionalpostclasses = ''; /* For styling approval pending, hidden posts, etc. */
		$permissions = func_php_getPostPermissions($postid);
		$userpermissions = func_php_getUserPermissions($postid, $index_activeuserid);

		/* TODO: Should we shift the following conditions to SQL? */
		/* Show hidden posts if owner or moderator. */
		/* Skip pending if not approved and not moderator or original author. */
		if (($index_viewmode == 'thumbnail') || ($index_viewmode == 'full')) {
			$additionalpostclasses = 'debugreplyborderclass';
		}


		$skippost = true;
		$flagpost = false;
		$flagpostremovebutton = false;
		$showflaggedpost = $index_showflaggedposts;
		$pendingapproval = false;
		$postflagclass = '';
		$postpendingclass = '';

		/* Hide age restricted content. */
		if (($index_activeuserconfirmedage < $postagerating)
				&& ($index_activeuserid != $authorid)) {
			$skippost = true;
		} else if ($postflagactive) {
			/* Display flagged posts to owner/moderator only. */
			if ($userpermissions['canedit'] || $userpermissions['canmoderate']) {
				/* Owner always sees posts. */
				$showflaggedpost = true;
				$flagpostremovebutton = index_php_getClearFlagButton('post', $postid,
						$postflagupdated, $postflagcount);
			} else {
				/* Only show appropriate posts depending on flag reason */
				switch ($postflagreason) {
				case 'MATURE_CONTENT':
				case 'ADULT_CONTENT':
					if ($index_activeuserconfirmedage >= $postflagagerating) {
						$showflaggedpost = true;
					}
					break;
				case 'COPYRIGHT_OR_TRADEMARK':
				case 'ILLEGAL_OR_VIOLATION':
					break;
				default:
					/* Always hide except from owner. */
				}
			}

			if ($showflaggedpost) {
				$additionalpostclasses .= ' postflagged';
				$postflagclass = 'poststatusflagged';
				$flagpost = true;
				$skippost = false;
			}
		} else if (!$permissions['canreply']) {
			if ($authorid == $index_activeuserid) {
				$additionalpostclasses .= ' postapprovalhidden';
				$skippost = false;
				$showflaggedpost = true;
			}
		/* Display conditionally pending/hidden posts to moderators/owners. */
		} else if (!$permissions['preapproved']
				|| ($permissions['approvalshidden'] != 0)) {
			if ($permissions['approvalstrusted'] != 0) {
				/* Always show approved posts. */
				// $additionalpostclasses .= ' postapprovaltrusted';
				$skippost = false;
			} else if ($permissions['approvalshidden'] != 0) {
				/* Show hidden-approved posts to moderators or owners */
				if ($userpermissions['canmoderate']
						|| (($index_activeuserid == $authorid) && ($index_activeuserid != 0))) {
					$additionalpostclasses .= ' postapprovalhidden';
					$skippost = false;
				}
			} else if ($userpermissions['canmoderate']
					|| (($index_activeuserid == $authorid) && ($index_activeuserid != 0))) {
				$additionalpostclasses .= ' postapprovalpending';
				$postpendingclass = 'poststatuspending';
				$skippost = false;
				$pendingapproval = true;
			}
		} else {
			$skippost = false;
		}

		if ($skippost) {
			continue;
		}
?>
		<div id="postlocation_<?=$postid?>">
<?php
		$postcount++;
		if ($postpinstate == 1) {
			continue; /* Leave post location and skip to next post. */
		}

		/* Add last-minute classes, if needed */
		$postblockclass = '';
		if ($index_viewmode == 'thumbnail') {
			$additionalpostclasses .= ' debugpostthumbnail';
		} else if ($index_viewmode == 'list') {
			$additionalpostclasses .= ' debugpostlist';
			$postblockclass .= 'postblocklistentry';
		}
?>
		<div id="post_<?=$postid?>" class="debugpostclass <?=$additionalpostclasses?>"
				postid="<?=$postid?>" postaccountid="<?=$authorid?>"
				postpermissions="<?=$postpermissions?>" postviewmode="<?=$postviewmode?>"
				postsortmode="<?=$postsortmode?>" postsortreverse="<?=$postsortreverse?>"
				postagerating="<?=$postagerating?>" postrecipient="<?=$authorusername?>"
				postrecipients="<?=$postrecipients?>">
			<div id="postblock_<?=$postid?>" class="<?=$postblockclass?>">
<?php
		if (($flagpost && $showflaggedpost) || !$flagpost) {
			if ($index_viewmode == 'full') {
?>
			<div id="postheader_<?=$postid?>" class="postheader"><!-- Condionally closes below -->
<?php
			}

			$showparent = false;
			if ($index_page == 'profile') {
				$showparent = true;
			}

			$moredetail = true;
			if ($index_viewmode == 'list')
				$moredetail = false;

			$classname = 'postcredits';
			if (($index_viewmode == 'full') || ($index_viewmode == 'thumbnail')) {
				$classname .= ' postcreditsflex';
			}
?>
					<div id="postcredits_<?=$postid?>" class="<?=$classname?>">
						<?=func_php_insertPostCredits('post', $postid, $showparent,
								$index_viewmode, $moredetail, $index_activeuserid)?>
<?php
			/* Full and thumbnail view have popup list in post credits. */
			if (($index_viewmode == 'full') || ($index_viewmode == 'thumbnail')) {
?>
					<?=func_php_insertPostPopupList('post', $postid,
							false, 'post', $index_page, $index_viewmode, $index_activeuserid)?>
<?php
			}
?>
					</div>
<?php

			/* Only show other data with full post. */
			if ($index_viewmode == 'full') {
?>
					<div id="postrecipients_<?=$postid?>" class="recipientsdiv">
						<?=index_php_insertPostRecipients('post', $postid)?>
					</div>
				</div><!-- postheader closing tag. Conditionally opens above. -->
				<div id="postmembers_<?=$postid?>" class="postmembers">
					<?=index_php_insertPostMembers('post', $postid)?>
				</div>
<?php
			}
		}
?>
				<div id="postflagdiv_<?=$postid?>" class="<?=$postflagclass?>">
<?php
		if ($flagpost) {
			$fuzzytime = func_php_fuzzyTimeAgo($postflagupdated, false, false);
			if ($fuzzytime == 'now') {
				$fuzzytime = 'a minute ago';
			}

			$reason = index_php_getflagreasondescription($postflagreason);
			$fulldate = date('l g:i a\<\b\r\>jS F Y', strtotime($postflagupdated));
?>
					This post was flagged <span class="tooltiphost poststatusflaggedunderline"><?=$fuzzytime?><span class="tooltiptext tooltiptextleft tooltiptexttop"><?=$fulldate?></span></span>.<br>Reason: <?=$reason?>
<?php
			if ($flagpostremovebutton) {
?>
					<br><?=$flagpostremovebutton?>
<?php
			}
		}
?>
				</div>
				<div id="postpending_<?=$postid?>" class="<?=$postpendingclass?>">
<?php
		if ($pendingapproval) {
?>
					This post is pending approval.
<?php
		}
?>
				</div>
<?php
		if (($flagpost && $showflaggedpost) || !$flagpost) { /* Start if flagged... */
			$postdescription = '';
			if ($index_viewmode == 'thumbnail') {
				$postimage = $post['image'];
				if (!$postimage) {
					$postimage = '/img/default-thumbnail.jpg';
				}
				$postdescription = "<div class=\"squareimagediv\"><a href=\"/{$postid}\"><img src=\"{$postimage}\"></a></div>";

				$postdescription .= '<div class="postdescriptionpadding">';
				$title = $post['title'];
				if ($title) {
					$postdescription .= "<h1><a href=\"/{$postid}\">{$title}</a></h1>";
				}

				if ($microdataarray = index_php_microdataArray($postid)) {
					foreach ($microdataarray as $microdata) {
						$postdescription .= "<div>{$microdata['str']}</div>";
					}
				} else {
					$shortdesc = $post['shortdesc'];
					$searchcache = $post['searchcache'];
					if ($shortdesc) {
						$postdescription .= substr($shortdesc, 0, 40);
						if (strlen($shortdesc) > 40)
							$postdescription .= '...';
					} else if (!$title) {
						$postdescription .= substr($searchcache, 0, 40);
						if (strlen($searchcache) > 40)
							$postdescription .= '...';
					}
				}
				$postdescription .= '</div>';
			} else if ($index_viewmode == 'list') {
				$postdescription = "&nbsp;&#183;&nbsp;<a href=\"/{$postid}\">";
				$title = $post['title'];
				if ($title) {
					$postdescription .= $title;
				}

				$shortdesc = $post['shortdesc'];
				$searchcache = $post['searchcache'];
				if ($title && $shortdesc) {
					$postdescription .= ' &#183; ';
				}

				$subshortdesc = trim(substr($shortdesc, 0, 80));
				if (!$subshortdesc && !$title)
					$subshortdesc = trim(substr($searchcache, 0, 80));

				if ($subshortdesc) {
					$postdescription .= "<span style=\"color:#888;\">{$subshortdesc}</span>";
				}

				if (!$title && !$subshortdesc) {
					$postdescription .= '(No subject)';
				}
				$postdescription .= '</a>';
			} else {
				/* Show full post */
				$postdescription = func_php_markdowntohtml_lazyload($post['description']);
			}

			$postclasses = 'postdescription';
			if (($index_viewmode == 'full') || ($index_viewmode == 'thumbnail')) {
				$postclasses .= ' postdescriptionwordwrap';
			}
			if ($index_viewmode == 'full') {
				$postclasses .= ' postdescriptionpadding';
			}
			if ($index_viewmode == 'list') {
				$postclasses .= ' postdescriptionlistentry';
			}
?>
				<div class="<?=$postclasses?>" id="postdescription_<?=$postid?>"><?=$postdescription?></div>
<?php
			/* Only show attachments with full post view */
			if ($index_viewmode == 'full') {
?>
				<div id="postattachments_<?=$postid?>">
<?php
				if ($attachmentarray = func_php_fileAttachmentArray('post', $postid)) {
					foreach ($attachmentarray as $attachment) {
						echo "<div id=\"{$attachment['id']}\" class=\"postfile\">{$attachment['innerHTML']}</div>";
					}
				}
?>
				</div>
<?php
			/* In list view, attach edit menu manually to end of post block */
			} else if ($index_viewmode == 'list') {
				echo func_php_insertPostPopupList('post', $postid,
						false, 'post', $index_page, $index_viewmode, $index_activeuserid);
			}
		} /* End if flagged... */
?>
			</div>
			<!-- Inline post replies start here. -->
<?php
		if ($index_viewmode == 'full') {
?>
			<div id="postreplyblock_<?=$postid?>" class="postreplies">
<?php
			/* Get what reply posts we want... */
			$querycriteria = sprintf('posts.parentid = %d', $postid);
			$querycriteriaand = " and $querycriteria";
			$query = sprintf('/* '.__LINE__. ' */ select %d as activeuserid, posts.*,' /* USERID */
					.' parentpost.accountid as parentaccountid,'
					.' parentpost.editpermissions as parenteditpermissions,'
					.' parentpost.replypermissions as parentreplypermissions,'
					.' parentpost.approvepermissions as parentapprovepermissions,'
					.' parentpost.moderatepermissions as parentmoderatepermissions,'
					.' accounts.id as authorid, accounts.username as authorusername,'
					.' accounts.fullname as authorfullname,'
					.' concat(posts.viewpermissions, posts.editpermissions,'
					.' 	posts.replypermissions, posts.approvepermissions, posts.moderatepermissions,'
					.' 	posts.tagpermissions) as permissions, posts.agerating,'
					.' (select group_concat(distinct concat_ws("", accounts.username, recipients.emailusername,'
					.'		if (recipients.emailserver is not NULL, "@", ""),'
					.' 		recipients.emailserver) separator " ")'
					.' 	from recipients left join accounts on accounts.id = recipients.recipientaccountid'
					.' 	where recipients.tablename = "posts"'
					.'	and recipients.senderaccountid != recipients.recipientaccountid'
					.' 	and recipients.tableid = posts.id) as recipients,'
					.' ((select count(*) from members left join membertree on members.postid = membertree.parentid'
					.' 	where members.accountid = posts.accountid and membertree.postid = posts.id'
					.' 	and members.trusted = 1 and members.accountid != posts.accountid)'
					.' 	+ (select count(*) from members where members.accountid = posts.accountid'
					.' 	and members.postid = posts.id and members.trusted = 1'
					.' 	and members.accountid != posts.accountid)) as postuseristrustedmember,'
					.' approvals.id as postapprovalid, approvals.trusted as postapprovaltrusted,'
					.' approvals.hidden as postapprovalhidden, approvals.added as postapprovaladded,'
					.' ((select count(*) from members left join membertree on members.postid = membertree.parentid'
					.' 	where members.accountid = %d and membertree.postid = posts.id' /* USERID */
					.' 	and members.trusted = 1 and members.accountid != posts.accountid)'
					.' 	+ (select count(*) from members where members.accountid = %d' /* USERID */
					.' 	and members.postid = posts.id and members.trusted = 1'
					.' 	and members.accountid != posts.accountid)) as activeuseristrustedmember,'
					.' (select count(trusted) from contacts'
					.' 	where contacts.accountid = posts.accountid and contacts.contactid = activeuserid'
					.' 	and contacts.trusted = 1) as activeuseristrustedcontact,'
					.' (select count(trusted) from contacts'
					.' 	where contacts.accountid = parentaccountid and contacts.contactid = posts.accountid'
					.' 	and contacts.trusted = 1) as postuseristrustedcontact,'
					.' (select count(recipientaccountid) from recipients'
					.' where recipients.tablename = "posts" and recipients.tableid = posts.id'
					.' and recipients.recipientaccountid = activeuserid) as activeuserisrecipient,'
					.' (select count(recipients.recipientaccountid) from recipients, membertree'
					.' 	where membertree.postid = posts.id and recipients.tablename = "posts"'
					.' 	and recipients.tableid = membertree.parentid'
					.' 	and recipients.recipientaccountid = posts.accountid) as postuserisrecipientofparent,'
					.' (select count(*) from posts as postreplies'
					.' 	where postreplies.parentid = posts.id) as postreplycount,'
					.' flags.flagcount, flags.flagactive, flags.updated as flagupdated,'
					.' flags.flagagerating, flags.flagreason'
					.' from posts left join accounts on posts.accountid = accounts.id'
					.' left join posts as parentpost on parentpost.id = posts.parentid'
					.' left join flags on flags.tablename = "posts" and flags.tableid = posts.id'
					.' left join approvals on approvals.tablename = "posts"'
					.' and approvals.tableid = posts.id and approvals.accountid = posts.accountid'
					.' where ', $index_activeuserid, $index_activeuserid, $index_activeuserid).$querycriteria;

			/* View Permissions! (ie. If the currently signed in user can view the post.) */
			$query .= ' /* '.__LINE__.':ViewPermissionsFiltering */ and (posts.id in (' /* Open filtering bracket, closes below */

					.sprintf('select posts.id from posts'
					.' where (posts.accountid = %d or posts.viewpermissions = 5' /* USERID */
					.' 	or (posts.viewpermissions = 4 and %d != 0))', /* USERID */
					$index_activeuserid, $index_activeuserid).$querycriteriaand

					.sprintf(' UNION select posts.id from posts, recipients'
					.' where posts.viewpermissions = 1 and recipients.tablename = "posts"'
					.' and recipients.tableid = posts.id'
					.' and (recipients.senderaccountid = %d' /* USERID */
					.' 	or recipients.recipientaccountid = %d)', /* USERID */
					$index_activeuserid, $index_activeuserid).$querycriteriaand

					.sprintf(' UNION select posts.id from posts, contacts'
					.' where posts.viewpermissions = 2 and posts.accountid = contacts.accountid'
					.' and contacts.trusted = 1 and contacts.contactid = %d', /* USERID */
					$index_activeuserid).$querycriteriaand

					.sprintf(' UNION select posts.id' /* USERID */
					.' from posts, members, membertree, posts as parentpost'
					.' where posts.viewpermissions = 3 and (membertree.postid = posts.id'
					.' 	or membertree.parentid = posts.id) and membertree.parentid = members.postid'
					.' and ((members.trusted = 1 and members.accountid = %d)' /* USERID */
					.' or 	(parentpost.id = membertree.parentid and parentpost.accountid = %d))', /* USERID */
					$index_activeuserid, $index_activeuserid).$querycriteriaand

					.')';

			$query .= ' /* '.__LINE__.':PostReplyApprovePermissionsFilter */ and posts.id in ('

					/* Filter reply permissions. */

					.sprintf('select posts.id from posts, posts as parentpost where posts.parentid = parentpost.id'
							.' and (posts.accountid = parentpost.accountid'
							.' or (posts.accountid = %d and %d != 0)' /* USERID, USERID */
							.' or parentpost.replypermissions = 0 and parentpost.accountid = %d' /* USERID */
							.' or parentpost.replypermissions = 5'
							.' or (parentpost.replypermissions = 4 and  posts.accountid != 0))', /* USERID */
							$index_activeuserid, $index_activeuserid, $index_activeuserid).$querycriteriaand

					.sprintf(' UNION select posts.id from posts, posts as parentpost, recipients'
							.' where posts.parentid = parentpost.id'
							.' and parentpost.replypermissions = 1 and recipients.tablename = "posts"'
							.' and recipients.tableid = parentpost.id'
							.' and recipients.recipientaccountid = posts.accountid').$querycriteriaand

					.sprintf(' UNION select posts.id from posts, posts as parentpost, contacts'
							.' where posts.parentid = parentpost.id'
							.' and parentpost.replypermissions = 2'
							.' and contacts.accountid = parentpost.accountid'
							.' and contacts.contactid = posts.accountid'
							.' and contacts.trusted = 1').$querycriteriaand

					.sprintf(' UNION select posts.id from membertree inner join posts on membertree.postid = posts.id'
							.' inner join members on membertree.parentid = members.postid'
							.' inner join posts as parentpost on parentpost.id = posts.parentid'
							.' where members.accountid = posts.accountid and parentpost.replypermissions = 3'
							.' and members.trusted = 1').$querycriteriaand

					/* Filter by approve permissions. */

					.sprintf(' UNION select posts.id from membertree inner join posts on membertree.postid = posts.id'
							.' inner join members on membertree.parentid = members.postid'
							.' inner join posts as parentpost on parentpost.id = posts.parentid'
							.' where members.accountid = posts.accountid and parentpost.approvepermissions = 3'
							.' and members.trusted = 1').$querycriteriaand
					.')';

			$query .= ')'; /* Close filtering bracket */

			/* Sort functionality goes here... eventually. */
			$query .= sprintf(' order by posts.added desc limit 6');

			$postreplies = func_php_query($query);
			$postreplycount = 0;
			$postreplyparentuserpermissions = func_php_getUserPermissions($postid, $index_activeuserid);
			$postreplynumrows = mysqli_num_rows($postreplies);

			/* Set up post reply links */
			$postreplyuserpermissions = func_php_getUserPermissions($postid, $index_activeuserid);
			$newpostreplyrecipient = false;
			if ($authorusername != $index_activeusername) {
				$newpostreplyrecipient = $authorusername;
			}
			$newpostreplypostid = $postid;
			$newpostreplylabel = 'Reply';
			if (!$postreplyuserpermissions['canreply']) {
				$newpostreplylabel = '';
			} else if (!$postreplyuserpermissions['preapproved']) {
				$newpostreplylabel = '<span class="tooltiphost">'
						.'<span class="tooltiptext tooltiptextleft">'
						.'<img src="/img/status-pending12.png">'
						.' Approval required<br>to show replies.</span>'
						.$newpostreplylabel
						.' <img src="/img/status-pending12.png"></span>';
			}

			$newpostreplybyemaillabel = '';
			if ($postreplyuserpermissions['replypermissions'] == 5) {
				$newpostreplybyemaillabel = 'Reply by email';

				if ($postreplyuserpermissions['approvepermissions'] < 5) {
					$newpostreplybyemaillabel = '<span class="tooltiphost">'
							.'<span class="tooltiptext tooltiptextright">'
							.'<img src="/img/status-pending12.png">'
							.' Approval required<br>to show emails.</span>'
							.$newpostreplybyemaillabel
							.' <img src="/img/status-pending12.png"></span>';
				}
			}

			$newpostreplyshowsignin = false;
			$newpostreplyshowjoingroup = false;
			$newpostreplypermissions = '000000';
			if ($newpostreplylabel) {
				switch ($index_newpostpermissions[0]) {
				case '5':
					$newpostreplypermissions = '504404';
					break;
				case '4':
					$newpostreplypermissions = '404404';
					break;
				case '3':
					$newpostreplypermissions = '303303';
					break;
				case '2':
					$newpostreplypermissions = '202202';
					break;
				case '1':
					$newpostreplypermissions = '101101';
					break;
				default:
					$newpostreplypermissions = '000000';
				}
			} else if ($postreplyuserpermissions['replypermissions'] == 4) {
				$newpostreplyshowsignin = true;
			} else if ($postreplyuserpermissions['replypermissions'] == 3) {
				if ($index_activeuserid) {
					$newpostreplyshowjoingroup = true;
				} else {
					$newpostreplyshowsignin = true;
				}
			}

			/* Show upper reply bar if any inline replies */
			if ($postreplynumrows) {
?>
				<div class="postreplybar">
					<span class="fadetext">
						<a href="/<?=$postid?>">View Page</a>
<?php
				if ($newpostreplylabel) {
					echo " &bull; <a href=\"#newpostreply_{$newpostreplypostid}\""
							." onclick=\"$('newpostreplylink_{$newpostreplypostid}').click();\">"
							."$newpostreplylabel</a>";

					/* Only show Reply by email link if permissions allow */
					if ($newpostreplybyemaillabel) {
						echo " &bull; <a href=\"mailto:{$newpostreplypostid}@{$servername}\">"
								."$newpostreplybyemaillabel</a>";
						/*
						echo " &bull; <a onClick=\"prompt('To reply to this post by email,\\n"
								."send a message to:',"
								." '{$newpostreplypostid}'+'@'+'{$servername}')\">"
								."$newpostreplybyemaillabel</a>";
								// */
					}
				}
?>
					</span>
				</div>
<?php
			} /* End upper reply bar */

			/* Now display inline reply ladder */
			while (($postreply = mysqli_fetch_assoc($postreplies)) && ($postreplycount < 5)) {
				$postreplyid = $postreply['id'];
				$postreplyauthorid = $postreply['authorid'];
				$postreplyauthorusername = $postreply['authorusername'];
				$postreplyauthorfullname = $postreply['authorfullname'];
				$postreplyparentid = $postreply['parentid'];
				$postreplyparentaccountid = $postreply['parentaccountid'];
				$postreplyparenteditpermissions = $postreply['parenteditpermissions'];
				$postreplyparentreplypermissions = $postreply['parentreplypermissions'];
				$postreplyparentapprovepermissions = $postreply['parentapprovepermissions'];
				$postreplydate = $postreply['added'];
				$postreplypermissions = $postreply['permissions'];
				$postreplyviewmode = $postreply['viewmode'];
				$postreplysortmode = $postreply['sortmode'];
				$postreplysortreverse = $postreply['sortreverse'];
				$postreplyagerating = $postreply['agerating'];
				$postreplyuseristrustedmember = $postreply['postuseristrustedmember'];
				$postreplyuseristrustedcontact = $postreply['postuseristrustedcontact'];
				$postreplyuserisrecipientofparent = $postreply['postuserisrecipientofparent'];
				$postreplyreplycount = $postreply['postreplycount'];
				$postreplyapprovalid = $postreply['postapprovalid'];
				$postreplyapprovaltrusted = $postreply['postapprovaltrusted'];
				$postreplyapprovalhidden = $postreply['postapprovalhidden'];
				$postreplyapprovaladded = $postreply['postapprovaladded'];
				$postreplyrecipients = $postreply['recipients'];
				$postreplyflagcount = $postreply['flagcount'];
				$postreplyflagactive = $postreply['flagactive'];
				$postreplyflagupdated = $postreply['flagupdated'];
				$postreplyflagagerating = $postreply['flagagerating'];
				$postreplyflagreason = $postreply['flagreason'];
				/* $replyrecipientid = $postreply['recipientid']; */
				$additionalpostclasses = ' postapprovalnormal'; /* For styling approval pending, hidden posts, etc. */
				$postreplycount++;
				$permissions = func_php_getPostPermissions($postreplyid);
				$userpermissions = func_php_getUserPermissions($postreplyid, $index_activeuserid);

				/* TODO: Should we shift the following conditions to SQL? */
				/* Show hidden posts if owner or moderator. */
				/* Skip pending if not approved and not moderator or original author. */

				$skippost = true;
				$flagpost = false;
				$flagpostremovebutton = false;
				$showflaggedpost = $index_showflaggedposts;
				$pendingapproval = false;
				$postflagclass = '';
				$postpendingclass = '';

				/* Hide age restricted content. */
				if (($index_activeuserconfirmedage < $postreplyagerating)
						&& ($index_activeuserid != $postreplyauthorid)) {
					$skippost = true;
				} else if ($postreplyflagactive) {
					/* Display flagged posts to owner/moderator only. */
					if ($userpermissions['canedit'] || $userpermissions['canmoderate']) {
						/* Owner always sees posts. */
						$showflaggedpost = true;
						$flagpostremovebutton = index_php_getClearFlagButton('post', $postreplyid,
								$postreplyflagupdated, $postreplyflagcount);
					} else {
						/* Only show appropriate posts depending on flag reason */
						switch ($postreplyflagreason) {
						case 'MATURE_CONTENT':
						case 'ADULT_CONTENT':
							if ($index_activeuserconfirmedage >= $postreplyflagagerating) {
								$showflaggedpost = true;
							}
							break;
						case 'COPYRIGHT_OR_TRADEMARK':
						case 'ILLEGAL_OR_VIOLATION':
							break;
						default:
							/* Always hide except from owner. */
						}
					}

					if ($showflaggedpost) {
						$additionalpostclasses = ' inlinepostflagged';
						$postflagclass = 'poststatusflagged';
						$flagpost = true;
						$skippost = false;
					}
				} else if (!$permissions['canreply']) {
					if ($authorid == $index_activeuserid) {
						$additionalpostclasses = ' postapprovalhidden';
						$skippost = false;
					}
				/* Display conditionally pending/hidden posts to moderators/owners. */
				} else if ((!$permissions['preapproved'])
						|| ($permissions['approvalshidden'] != 0)) {
					if ($permissions['approvalstrusted'] != 0) {
						/* Always show approved posts. */
						// $additionalpostclasses = ' postapprovaltrusted';
						$skippost = false;
					} else if ($permissions['approvalshidden'] != 0) {
						/* Show hidden-approved posts to moderators or owners */
						if ($postreplyparentuserpermissions['canmoderate']
								|| (($index_activeuserid == $authorid) && ($index_activeuserid != 0))) {
							$additionalpostclasses = ' postapprovalhidden';
							$skippost = false;
						}
					} else if ($postreplyparentuserpermissions['canmoderate']
							|| (($index_activeuserid == $postreplyauthorid) && ($index_activeuserid != 0))) {
						$additionalpostclasses = ' postapprovalpending';
						$postpendingclass = 'poststatuspending';
						$skippost = false;
						$pendingapproval = true;
					}
				} else {
					$skippost = false;
				}

				if ($skippost) {
					continue;
				}
?>
				<div id="post_<?=$postreplyid?>" class="debugpostclass<?=$additionalpostclasses?>"
						postid="<?=$postreplyid?>" postaccountid="<?=$postreplyauthorid?>"
						postpermissions="<?=$postreplypermissions?>" postviewmode="<?=$postreplyviewmode?>"
						postsortmode="<?=$postreplysortmode?>" postsortreverse="<?=$postreplysortreverse?>"
						postagerating="<?=$postreplyagerating?>"
						postrecipient="<?=$postreplyauthorusername?>" postrecipients="<?=$postreplyrecipients?>">
					<div id="postblock_<?=$postreplyid?>">
<?php
				if (($flagpost && $showflaggedpost) || !$flagpost) {
?>
						<div id="postheader_<?=$postreplyid?>" class="postheader">
							<div id="postcredits_<?=$postreplyid?>" class="postcredits postcreditsflex">
								<?=func_php_insertPostCredits('post', $postreplyid, false, 'full', true, $index_activeuserid)?>
								<?=func_php_insertPostPopupList('post', $postreplyid,
										false, 'reply', false, 'full', $index_activeuserid)?>
							</div>
							<div id="postrecipients_<?=$postreplyid?>" class="recipientsdiv">
								<?=index_php_insertPostRecipients('post', $postreplyid)?>
							</div>
						</div>
						<div id="postmembers_<?=$postreplyid?>" class="postmembers">
							<?=index_php_insertPostMembers('post', $postreplyid)?>
						</div>
<?php
				}
?>
						<div id="postflagdiv_<?=$postreplyid?>" class="<?=$postflagclass?>">
<?php
				if ($flagpost) {
					$fuzzytime = func_php_fuzzyTimeAgo($postreplyflagupdated, false, false);
					if ($fuzzytime == 'now') {
						$fuzzytime = 'a minute ago';
					}

					$reason = index_php_getflagreasondescription($postreplyflagreason);
					$fulldate = date('l g:i a\<\b\r\>jS F Y', strtotime($postreplyflagupdated));
?>
							This post was flagged <span class="tooltiphost poststatusflaggedunderline"><?=$fuzzytime?><span class="tooltiptext tooltiptextleft tooltiptexttop"><?=$fulldate?></span></span>.<br>Reason: <?=$reason?>
<?php
					if ($flagpostremovebutton) {
?>
							<br><?=$flagpostremovebutton?>
<?php
					}
				}
?>
						</div>
						<div id="postpending_<?=$postreplyid?>" class="<?=$postpendingclass?>">
<?php
				if ($pendingapproval) {
?>
							This post is pending approval.
<?php
				}
?>
						</div>
<?php
				if (($flagpost && $showflaggedpost) || !$flagpost) { /* Start if flagged... */
?>
						<div class="postdescription postdescriptionpadding" id="postdescription_<?=$postreplyid?>"><?=func_php_markdowntohtml_lazyload($postreply['description'])?></div>
						<div id="postattachments_<?=$postreplyid?>">
<?php
					if ($attachmentarray = func_php_fileAttachmentArray('post', $postreplyid)) {
						foreach ($attachmentarray as $attachment) {
							echo "<div id=\"{$attachment['id']}\" class=\"postfile\">{$attachment['innerHTML']}</div>";
						}
					}
?>
						</div>
<?php
				} /* End if flagged... */
?>
				</div>
				<div id="postreplyblock_<?=$postreplyid?>" class="postreplyblock"><span class="fadetext">
								<a href="/<?=$postreplyid?>">View page</a>
<?php
				/* View nested replies, if any */
				if ($postreplyreplycount > 0) {
					$replyword = $postreplyreplycount;
					if ($postreplyreplycount != 1) {
						$replyword .= ' replies...';
					} else {
						$replyword .= ' reply...';
					}
					echo "&bull; <a href=\"/{$postreplyid}#replies\">{$replyword}</a>";
				}
?>
					</span></div>
				</div>
<?php
			} /* End post reply entry */

			/* Show lower reply bar if either can reply or this post has replies */
			if (($newpostreplylabel) || ($postreplynumrows > $postreplycount)) {
?>
				<div id="newpostreply_<?=$postid?>" class="debugpostclass morerepliesdiv">
						<div class="postreplyblock">
						<span class="fadetext">
<?php
				$spacer = '';
				if ($newpostreplylabel) {
					echo "<a id=\"newpostreplylink_{$newpostreplypostid}\" onclick=\"index_js_setupPostEditor(this,"
							." 'newpostreply_{$newpostreplypostid}', 'post', $newpostreplypostid, true,"
							." '$newpostreplypermissions', 3, '$newpostreplyrecipient', '', '',"
							." 'full', '', false);\">$newpostreplylabel</a>";

					/* Only show Reply by email link if permissions allow */
					if ($newpostreplybyemaillabel) {
						echo " &bull; <a href=\"mailto:{$newpostreplypostid}@{$servername}\">"
								."$newpostreplybyemaillabel</a>";
						/*
						echo " &bull; <a onClick=\"prompt('To reply to this post by email,\\n"
								."send a message to:',"
								." '{$newpostreplypostid}'+'@'+'{$servername}')\">"
								."$newpostreplybyemaillabel</a>";
								// */
					}
					$spacer = ' &bull; ';
				} else if ($newpostreplyshowsignin) {
					echo '<span class="fadetext"><a class="ctalink" href="#userpanel">Sign In</a>'
							.' or <a class="ctalink" href="#rightpanel">Sign in</a> to join this group.</span> ';
					$spacer = ' &bull; ';
				} else if ($newpostreplyshowjoingroup) {
					echo '<span class="fadetext">Join this group to reply.</span>';
					$spacer = ' &bull; ';
				}

				/* Display show more replies link when there are too many to display with this post. */
				if ($postreplynumrows > $postreplycount) {
					echo "{$spacer}<a class=\"fadetext\" href=\"/{$postid}/#replies\">More replies...</a>";
				}
?>
						</span>
					</div>
				</div>
<?php
			} /* End lower reply bar */
?>
			</div>
			<!-- Inline post replies end here. -->
<?php
		} /* End if ($index_viewmode == 'full') */
?>
		</div></div>
<?php
	}

	if ($index_viewmode == 'list') {
?>
		<div style="border-top:solid 1px #cde;"></div>
<?php
	}
?>
		</div>
<?php
} else if ($index_showposts && !$index_page_id) {
?>
	<div id="emptyhomepage" class="debugpostclass debugreplyborderclass">
		<div class="postdescription postdescriptionwordwrap postdescriptionpadding">
		<h2>Welcome to <?=$servertitle?>!</h2>
		<p>This is your home stream, which shows recent activity from you and other people.</p>
		<p>Some things you can do here include:
			<ul>
			<li>Send and recieve emails.</li>
			<li>Subscribe to RSS feeds.</li>
			<li>Add people through <span style="border:solid 1px #ddd;background:#f0f7ff;padding:2px 4px;"><strong>Contacts</strong>
				<img src="/img/contactsmenu.png" style="vertical-align:-2px;"></span> at the top of this screen.</li>
			<li>Or, you can start writing something yourself by clicking on
				<span style="border:solid 1px #acd;background:#f0f7ff;padding:2px 6px;">New Post</span> above.</li>
			</ul>
		</p>
		</div>
	</div>
<?php
}

/* Display page next/prev buttons when there are too many replies to display with this post. */
$previouspagelink = '';
$nextpagelink = '';
if (($index_page_number > 1) && ($postcount > 0)) {
	$previouspagelink = '/home/';
	if ($index_get_page == 'new')
		$previouspagelink = '/new/';
	else if ($index_page == 'profile')
		$previouspagelink = '/'.$index_showprofile['username'].'/';
	else if (($index_page == 'page') && $index_page_id)
		$previouspagelink = '/'.$index_page_id.'/';

	$previouspagelink .= ($index_page_number - 1).$index_querystring.'#replies';
}

if ($postnumrows > $index_page_count) {
	$nextpagelink = '';
	switch ($index_page) {
	case 'new':
	case 'home':
		$nextpagelink = "/{$index_page}/";
		break;
	case 'profile':
	case 'page':
	case 'emails':
		$nextpagelink = "/{$index_page_id}/";
		break;
	}

	if ($nextpagelink)
		$nextpagelink .= ($index_page_number + 1).$index_querystring.'#replies';
}

if ($previouspagelink || $nextpagelink) {
?>
		<!-- Start prev/next page buttons -->
		<div class="debugpostclass debugpostborderclass postbordershadow">
			<div class="postdescription">
				<div style="display:table;width:100%;">
					<div style="display:table-row;">
						<div style="display:table-cell;width:35%;text-align:left;">
<?php
	/* Display page next/prev buttons when there are too many posts/page replies to display on one page. */
	if ($previouspagelink) {
?>
							<a class="boldlink" href="<?=$previouspagelink?>">&lt; Previous</a>
<?php
	}
?>
						</div>
						<div style="display:table-cell;width:30%;text-align:center;">
							Page <?=$index_page_number?>
						</div>
						<div style="display:table-cell;width:35%;text-align:right;">
<?php
	if ($nextpagelink) {
?>
							<a class="boldlink" href="<?=$nextpagelink?>">Next &gt;</a>
<?php
	}
?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End prev/next page buttons -->
<?php
}
?>
	</div>
	<div id="mainborderbottom"></div>
</div>

<?php
/* Below target label is used by an above goto when skipping the entire
 * page contents to display the landing page instead. */
FOOTERDIV:
?>
<div id="footer">
	<div><?=$copyrightfootertext?></div>
	<div>
		<a href="/"><strong><?=$servertitle?></strong></a>
		<a href="/pricing/">Pricing</a>
		<a href="/about.php">About</a>
		<a href="/contact.php">Contact Us</a>
		<a href="/terms.php">Terms of Use</a>
		<a href="/manifesto.php">Manifesto</a>
		<a href="/quickstart.php">Quickstart Guide</a>
		<a href="/features.php">Feature Tour</a>
		<a href="/advertise.php">Advertise</a>
		<a href="/reportabug.php">Report a Bug</a>
		<a href="/devinfo.php">Developer Information</a>
		<br>
		<a href="https://parsedown.org/">Markdown handled by Parsedown.org</a>
	</div>
</div>

<?php
/* START - Hide instant messager from non-members */
if ($index_activeusername) {
?>
<div id="messager" class="messager">
	<div id="messagerresizenw" class="messagerresizenw"></div>
	<div id="messagertitlebar" class="messagertitlebar">
		<h2 id="messagertitle" class="messagertitle">Loading...</h2>
	</div>
	<div class="messagercontrols"><a onclick="index_js_popupEditorClose();"><span class="tooltiphost"><span class="messagerclose">&#215;</span><span class="tooltiptext tooltiptextright">Close</span></span></a></div>
	<div id="messagerresizew" class="messagerresizew"></div>
	<div id="messagerresizee" class="messagerresizee"></div>
	<div id="messagerresizesw" class="messagerresizesw"></div>
	<div id="messagerresizes" class="messagerresizes"></div>
	<div id="messagerresizese" class="messagerresizese"></div>
	<div class="messagermain">
		<div id="messagerbody" class="messagerbody">
			<div id="messagerbodyscroller" class="messagerbodyscroller"
					onscroll="index_js_scrollEvent();">Loading messages...</div>
		</div>
		<div id="messageroverlay" class="messageroverlay"></div>
		<form id="messagerform" class="messagerform" method="post" enctype="multipart/form-data"
				onSubmit="index_js_sendNewPost(this); return false;">
			<div class="messagerentry">
				<div class="messagerbuttons">
					<span class="messagerformsettings" class="popuplist">
						<img class="imagebutton" src="/img/settings.png"><div>
							<!--
							<input type="radio" id="messagershowonsend" name="messagershowonsend">
							<label for="messagershowonsend">Show on send</label>
							<br>
							<input type="radio" id="messagershowtypingstatus" name="messagershowtypingstatus">
							<label for="messagershowtypingstatus">Show typing status</label>
							<br>
							<input type="radio" id="messagershowmpostbeforesend" name="messagershowmpostbeforesend">
							<label for="messagershowmpostbeforesend">Show post before send</label>
							<br>
							<input type="checkbox" id="messagercondensedview" name="messagercondensedview" onclick="$('messagerinputentry').focus();">
							<label for="messagercondensedview">Condensed View</label>
							<br>
							<input type="checkbox" id="messagerpopuponreceive" name="messagerpopuponreceive" onclick="$('messagerinputentry').focus();">
							<label for="messagerpopuponreceive">Popup on Receive</label>
							<br>
							<input type="checkbox" id="messagersoundonreceive" name="messagersoundonreceive" onclick="$('messagerinputentry').focus();">
							<label for="messagersoundonreceive">Sound on Receive</label>
							<br>
							-->
							<input type="checkbox" id="messagercloseonsend" name="messagercloseonsend" onclick="$('messagerinputentry').focus();">
							<label for="messagercloseonsend">Close on Send</label>
							<br>
							<input type="checkbox" id="messagersendonenterkey" name="messagersendonenterkey" onclick="$('messagerinputentry').focus();">
							<label for="messagersendonenterkey">Send on Enter key</label>
						</div>
					</span>
					<span class="tooltiphost">
						<input id="messagerattach" class="messagerattach imagebutton" type="image" src="/img/attach.png" onclick="return index_js_popupEditorAttach();">
						<span class="tooltiptext tooltiptexttop">Attach an image or file</span>
					</span>
				</div>
				<div id="messagerinput" class="messagerinput">
					<div id="messagerinputmain" class="messagerinputmain">
						<div id="messagerinputentry" class="messagerinputentry" contentEditable="true" placeholder="Type a message..."></div>
						<div id="messageattachments_-1"></div>
					</div>
					<button id="messagersubmit" class="messagersubmit"><img src="/img/msgsend.png"></button>
				</div>
<?php
/* - TODO - Commented until feature complete
				<span id="messagerpermissions" class="editpostpermissionsbutton">
					<img src="/img/popupmenudown.png"><span id="messagerpermissionssuggestion"><img id="messagerpermissionsicon" src="/img/icon-permissions-open.png"><img id="messagercontentratingicon" src="/img/icon-permissions-generalcontent.png"><img id="messagerrepliesicon" src="/img/icon-post-approved.png"><img id="messagerpermissionsiconadvanced" src="/img/settings.png"></span>
					<span id="messagerpermissionstitle"></span>
					<div id="messagerpermissionspopup" class="permissionspopup">
						<div id="messagerpermissionspresets">
							<fieldset>
								<legend>Visibility</legend>
								<div class="editpermissionspreset">
									<input id="messagerpreset5" type="radio" name="messagerpreset" value="5" class="editpermissionspresetinput"
											onChange="index_js_updatePostPermissions('messager');" checked>
									<div class="editpermissionspresetlabel">
										<label for="messagerpreset5"><img src="/img/icon-permissions-open.png" alt="open"> Anyone</label>
										<span class="editpermissionspresetunsigned">
											(<input id="messagerallowunsigned" type="checkbox" name="messagerallowunsigned" value="allowunsigned"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerallowunsigned">Allow guest &amp; email posts</label>)
										</span>
									</div>
								</div>
								<div class="editpermissionspreset">
									<input id="messagerpreset2" type="radio" name="messagerpreset" value="2" class="editpermissionspresetinput"
											onChange="index_js_updatePostPermissions('messager');">
									<div class="editpermissionspresetlabel">
										<label for="messagerpreset2"><img src="/img/icon-permissions-contacts.png" alt="contacts"> Starred contacts</label>
									</div>
								</div>
								<div class="editpermissionspreset">
									<input id="messagerpreset1" type="radio" name="messagerpreset" value="1" class="editpermissionspresetinput"
											onChange="index_js_updatePostPermissions('messager');">
									<div class="editpermissionspresetlabel">
										<label for="messagerpreset1"><img src="/img/icon-permissions-recipient.png" alt="recipient"> Recipients</label>
									</div>
								</div>
								<div class="editpermissionspreset">
									<input id="messagerpreset0" type="radio" name="messagerpreset" value="0" class="editpermissionspresetinput"
											onChange="index_js_updatePostPermissions('messager');">
									<label for="messagerpreset0" class="editpermissionspresetlabel"><img src="/img/icon-permissions-owner.png" alt="owner"> Only the owner</label>
								</div>
							</fieldset>
							<fieldset>
								<legend>Content Suitability</legend>
								<span class="editpermissionspreset">
									<input id="messagerpresetgeneralcontent" type="radio" name="messagercontentrating" value="general" class="editpermissionspresetinput"
											onChange="index_js_updatePostPermissions('messager');" checked>
									<label for="messagerpresetgeneralcontent" class="editpermissionspresetlabel"><img src="/img/icon-permissions-generalcontent.png" alt="General content"> General</label>
								</span>
								<span class="editpermissionspreset">
									<input id="messagerpresetmature15" type="radio" name="messagercontentrating" value="mature15" class="editpermissionspresetinput"
											onChange="index_js_updatePostPermissions('messager');">
									<label for="messagerpresetmature15" class="editpermissionspresetlabel"><img src="/img/icon-permissions-mature15.png" alt="15+ mature content"> Mature</label>
								</span>
								<span class="editpermissionspreset">
									<input id="messagerpresetadult21" type="radio" name="messagercontentrating" value="adult18" class="editpermissionspresetinput"
											onChange="index_js_updatePostPermissions('messager');">
									<label for="messagerpresetadult21" class="editpermissionspresetlabel"><img src="/img/icon-permissions-adult18.png" alt="18+ adult content"> Adult</label>
								</span>
							</fieldset>
							<fieldset>
								<legend>Replies</legend>
								<span class="editpermissionspreset">
									<input id="messagerpresetacceptreplies" type="radio" name="messagerreplies" value="acceptreplies" class="editpermissionspresetinput"
											onChange="index_js_updatePostPermissions('messager');" checked>
									<label for="messagerpresetacceptreplies" class="editpermissionspresetlabel"><img src="/img/icon-post-approved.png" alt="Show All Replies"> Show All</label>
								</span>
								<span class="editpermissionspreset">
									<input id="messagerpresetrepliesneedapproval" type="radio" name="messagerreplies" value="repliesneedapproval" class="editpermissionspresetinput"
											onChange="index_js_updatePostPermissions('messager');">
									<label for="messagerpresetrepliesneedapproval" class="editpermissionspresetlabel"><img src="/img/icon-post-pending.png" alt="Replies Need Approval"> Need Approval</label>
								</span>
								<span class="editpermissionspreset">
									<input id="messagerpresetdisablereplies" type="radio" name="messagerreplies" value="disablereplies" class="editpermissionspresetinput"
											onChange="index_js_updatePostPermissions('messager');">
									<label for="messagerpresetdisablereplies" class="editpermissionspresetlabel"><img src="/img/icon-post-blocked.png" alt="Disable Replies"> Disabled</label>
								</span>
							</fieldset>
						</div>
						<div>
							<span class="tooltiphost">
								<input id="messageradvancedpermissions" type="checkbox" name="messageradvancedpermissions" value="advanced"
										onChange="index_js_switchAdvancedPermissions('messager');">
								<label for="messageradvancedpermissions"><img src="/img/icon-permissions-advanced.png" alt="Advanced permissions"> Advanced permissions</label>
								<span id="messageradvancedpermissionstooltip" class="tooltiptext">Click for advanced permissions.</span>
							</span>
						</div>
						<div id="messagerpermissionsadvanced" class="editpermissionsadvanced">
							<div class="editpopuppermissions">
								<div class="editpopuppermissionsrow">
									<div class="editpopuppermissionscell">View</div>
									<div class="editpopuppermissionscell editpostpermissionsbar">
										<span>
											<input id="messagerview5" type="radio" name="messagerview" value="5"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerview5"><img src="/img/icon-permissions-open.png" alt="open" title="Anybody can see this"></label>
										</span>
										<span>
											<input id="messagerview4" type="radio" name="messagerview" value="4"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerview4"><img src="/img/icon-permissions-all.png" alt="all" title="Users on this site can see this"></label>
										</span>
										<span>
											<input id="messagerview3" type="radio" name="messagerview" value="3"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerview3"><img src="/img/icon-permissions-members.png" alt="members" title="Group members can see this"></label>
										</span>
										<span>
											<input id="messagerview2" type="radio" name="messagerview" value="2"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerview2"><img src="/img/icon-permissions-contacts.png" alt="contacts" tytle="Starred contacts can see this"></label>
										</span>
										<span>
											<input id="messagerview1" type="radio" name="messagerview" value="1"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerview1"><img src="/img/icon-permissions-recipient.png" alt="recipient" title="The sender and recipient(s) can see this"></label>
										</span>
										<span>
											<input id="messagerview0" type="radio" name="messagerview" value="0"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerview0"><img src="/img/icon-permissions-owner.png" alt="owner" title="Only the owner can see this"></label>
										</span>
									</div>
								</div>
								<div class="editpopuppermissionsrow">
									<div class="editpopuppermissionscell">Edit</div>
									<div class="editpopuppermissionscell editpostpermissionsbar">
										<span>
											<input id="messageredit5" type="radio" name="messageredit" value="5"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messageredit5"><img src="/img/icon-permissions-open.png" alt="open" title="Anybody can edit this"></label>
										</span>
										<span>
											<input id="messageredit4" type="radio" name="messageredit" value="4"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messageredit4"><img src="/img/icon-permissions-all.png" alt="all" title="Users on this site can edit this"></label>
										</span>
										<span>
											<input id="messageredit3" type="radio" name="messageredit" value="3"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messageredit3"><img src="/img/icon-permissions-members.png" alt="members" title="Group members can edit this"></label>
										</span>
										<span>
											<input id="messageredit2" type="radio" name="messageredit" value="2"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messageredit2"><img src="/img/icon-permissions-contacts.png" alt="contacts" title="Starred contacts can edit this"></label>
										</span>
										<span>
											<input id="messageredit1" type="radio" name="messageredit" value="1"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messageredit1"><img src="/img/icon-permissions-recipient.png" alt="recipient" title="The sender and recipient(s) can edit this"></label>
										</span>
										<span>
											<input id="messageredit0" type="radio" name="messageredit" value="0"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messageredit0"><img src="/img/icon-permissions-owner.png" alt="owner" title="Only the owner can edit this"></label>
										</span>
									</div>
								</div>
								<div class="editpopuppermissionsrow">
									<div class="editpopuppermissionscell">Reply</div>
									<div class="editpopuppermissionscell editpostpermissionsbar">
										<span>
											<input id="messagerreply5" type="radio" name="messagerreply" value="5"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerreply5"><img src="/img/icon-permissions-open.png" alt="open" title="Anybody can reply"></label>
										</span>
										<span>
											<input id="messagerreply4" type="radio" name="messagerreply" value="4"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerreply4"><img src="/img/icon-permissions-all.png" alt="all" title="Users on this site can reply"></label>
										</span>
										<span>
											<input id="messagerreply3" type="radio" name="messagerreply" value="3"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerreply3"><img src="/img/icon-permissions-members.png" alt="members" title="Group members can reply"></label>
										</span>
										<span>
											<input id="messagerreply2" type="radio" name="messagerreply" value="2"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerreply2"><img src="/img/icon-permissions-contacts.png" alt="contacts" title="Starred contacts can reply"></label>
										</span>
										<span>
											<input id="messagerreply1" type="radio" name="messagerreply" value="1"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerreply1"><img src="/img/icon-permissions-recipient.png" alt="recipient" title="The sender and recipient(s) can reply"></label>
										</span>
										<span>
											<input id="messagerreply0" type="radio" name="messagerreply" value="0"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerreply0"><img src="/img/icon-permissions-owner.png" alt="owner" title="Only the owner can reply"></label>
										</span>
									</div>
								</div>
								<div class="editpopuppermissionsrow">
									<div class="editpopuppermissionscell">Approve</div>
									<div class="editpopuppermissionscell editpostpermissionsbar">
										<span>
											<input id="messagerapprove5" type="radio" name="messagerapprove" value="5"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerapprove5"><img src="/img/icon-permissions-open.png" alt="open" title="All replies are pre-approved"></label>
										</span>
										<span>
											<input id="messagerapprove4" type="radio" name="messagerapprove" value="4"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerapprove4"><img src="/img/icon-permissions-all.png" alt="all" title="Users on this site have replies pre-approved"></label>
										</span>
										<span>
											<input id="messagerapprove3" type="radio" name="messagerapprove" value="3"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerapprove3"><img src="/img/icon-permissions-members.png" alt="members" title="Group members have replies pre-approved"></label>
										</span>
										<span>
											<input id="messagerapprove2" type="radio" name="messagerapprove" value="2"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerapprove2"><img src="/img/icon-permissions-contacts.png" alt="contacts" title="Starred contacts have replies pre-approved"></label>
										</span>
										<span>
											<input id="messagerapprove1" type="radio" name="messagerapprove" value="1"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerapprove1"><img src="/img/icon-permissions-recipient.png" alt="recipient" title="The sender and recipient(s) have replies pre-approved"></label>
										</span>
										<span>
											<input id="messagerapprove0" type="radio" name="messagerapprove" value="0"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagerapprove0"><img src="/img/icon-permissions-owner.png" alt="owner" title="Only the owner has replies pre-approved"></label>
										</span>
									</div>
								</div>
								<div class="editpopuppermissionsrow">
									<div class="editpopuppermissionscell">Moderate</div>
									<div class="editpopuppermissionscell editpostpermissionsbar">
										<span>
											<input id="messagermoderate5" type="radio" name="messagermoderate" value="5"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagermoderate5"><img src="/img/icon-permissions-open.png" alt="open" title="Anybody can moderate replies"></label>
										</span>
										<span>
											<input id="messagermoderate4" type="radio" name="messagermoderate" value="4"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagermoderate4"><img src="/img/icon-permissions-all.png" alt="all" title="Users on this site can moderate replies"></label>
										</span>
										<span>
											<input id="messagermoderate3" type="radio" name="messagermoderate" value="3"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagermoderate3"><img src="/img/icon-permissions-members.png" alt="members" title="Group members can moderate replies"></label>
										</span>
										<span>
											<input id="messagermoderate2" type="radio" name="messagermoderate" value="2"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagermoderate2"><img src="/img/icon-permissions-contacts.png" alt="contacts" title="Starred contacts can moderate replies"></label>
										</span>
										<span>
											<input id="messagermoderate1" type="radio" name="messagermoderate" value="1"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagermoderate1"><img src="/img/icon-permissions-recipient.png" alt="recipient" title="The sender and recipient(s) can moderate replies"></label>
										</span>
										<span>
											<input id="messagermoderate0" type="radio" name="messagermoderate" value="0"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagermoderate0"><img src="/img/icon-permissions-owner.png" alt="owner" title="Only the owner can moderate replies"></label>
										</span>
									</div>
								</div>
								<div class="editpopuppermissionsrow">
									<div class="editpopuppermissionscell">Tags</div>
									<div class="editpopuppermissionscell editpostpermissionsbar">
										<span>
											<input id="messagertag5" type="radio" name="messagertag" value="5"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagertag5"><img src="/img/icon-permissions-open.png" alt="open" title="Anybody can add tags"></label>
										</span>
										<span>
											<input id="messagertag4" type="radio" name="messagertag" value="4"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagertag4"><img src="/img/icon-permissions-all.png" alt="all" title="Users on this site can add tags"></label>
										</span>
										<span>
											<input id="messagertag3" type="radio" name="messagertag" value="3"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagertag3"><img src="/img/icon-permissions-members.png" alt="members" title="Group members can add tags"></label>
										</span>
										<span>
											<input id="messagertag2" type="radio" name="messagertag" value="2"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagertag2"><img src="/img/icon-permissions-contacts.png" alt="contacts" title="Starred contacts can add tags"></label>
										</span>
										<span>
											<input id="messagertag1" type="radio" name="messagertag" value="1"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagertag1"><img src="/img/icon-permissions-recipient.png" alt="recipient" title="The sender and recipient(s) can add tags"></label>
										</span>
										<span>
											<input id="messagertag0" type="radio" name="messagertag" value="0"
													onChange="index_js_updatePostPermissions('messager');">
											<label for="messagertag0"><img src="/img/icon-permissions-owner.png" alt="owner" title="Only the owner can add tags"></label>
										</span>
									</div>
								</div>
							</div>
							<div>
								<label for="messagerrecipients">Recipient:</label><br>
								<input id="messagerrecipients" name="editpostrecipients" type="text" class="shadowinset">
							</div>
							<div>
								<label for="messageragerating">Age rating:</label>
								<input id="messageragerating" name="messageragerating" type="number" min="3" max="25" class="shadowinset"
										onChange="index_js_updatePostPermissions('messager');">
							</div>
						</div>
					</div>
				</span>
// END commented section */
?>
			</div>
			<div id="messagerhidden">
				<input id="messagerfile" name="files[]" type="file" onChange="index_js_previewImage('messager')">
				<input id="messagerrecipient" name="messagerrecipient" type="hidden" value="">
				<input id="messagerpostid" name="messagerpostid" type="hidden" value="0">
				<input name="ajax_command" type="hidden" value="new_message">

				<!-- START - TODO: personal message permission defaults.
				  -- The actual user-configurable code is in PHP below this -->
				<input type="radio" name="messagerview" value="1" checked>
				<input type="radio" name="messageredit" value="0" checked>
				<input type="radio" name="messagerreply" value="1" checked>
				<input type="radio" name="messagerapprove" value="1" checked>
				<input type="radio" name="messagermoderate" value="0" checked>
				<input type="radio" name="messagertag" value="1" checked>
				<!-- END - TODO: personal message permission defaults.
				  -- The actual user-configurable code is in PHP below this -->
			</div>
		</form>
	</div>
</div>
<?php
} /* END hide popup messager for non-members */
?>

<div id="modalprompt" class="modalbackground">
	<div id="modalpromptdialog" class="modalpromptdialog">
		<div id="modalpromptclose" class="modalclosediv">
			<span class="tooltiphost">
				<input id="modalpromptclosebutton" type="image" src="/img/editorstatusbar-cancel.png">
				<span class="tooltiptext tooltiptextright tooltiptexttop">Cancel <span class="tooltipkey">esc</span></span>
			</span>
		</div>
		<div id="modalpromptheader"></div>
		<div id="modalpromptcontent" class="modalpromptcontent"></div>
		<div id="modalpromptbuttons" class="modalpromptbuttons">
			<button id="modalpromptcancelbutton" class="silverbutton">Cancel</button>
			<span id="modalprompthints" class="modalprompthints"></span>
			<button id="modalpromptsubmitbutton" class="silverbutton">Confirm</button>
		</div>
	</div>
</div>

<div id="modaldivbackground" class="modalbackground">
	<!-- Populated with DIVs needing to be modal -->
	<div id="modaldivclose" class="modalclosediv">
		<span class="tooltiphost">
			<input id="modaldivclosebutton" type="image" src="/img/editorstatusbar-cancel.png">
			<span class="tooltiptext tooltiptextright tooltiptexttop">Cancel <span class="tooltipkey">esc</span></span>
		</span>
	</div>
</div>

</body>
</html>
