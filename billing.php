<div class="debugpostclass debugpostborderclass" style="margin-left:8px;">
<div class="postdescription" style="padding:8px;">
<h1>Billing Settings</h1>
<br>
<?php
/* Used to check payments and invoicing */
$validprices = array(10, 15, 20, 25, 30, 35, 40, 45, 50, 60,
		70, 80, 90, 100, 110, 120, 130, 140,
		150, 160, 170, 180, 190, 200);

/* Work out what will be shown on the webfront. */
$billing_page = false;
$planquantity = 0;
$planname = '';
$planprice = '';
$planpricedesc = '';
$planpriceid = '';
$planyearly = false;
$planchangelinkoptions = false;
switch ($index_get_option) {
case 'cancelsubscription':
	$billing_page = $index_get_option;
	break;
default:
	/* Default to payment processing, or error page */
	$planquantity = false;

	if (isset($_GET['qty'])) {
		$planquantity = func_php_inttext($_GET['qty']);
	}

	if ($planquantity) {
		if (in_array($planquantity, $validprices, true)) {
			$planyearly = isset($_GET['yearly']);
			$billing_page = 'payment';
			$planchangelinkoptions = '?qty='.$planquantity;
			if ($planyearly) {
				$planchangelinkoptions .= '&yearly=on';
			}
		}
	}

	if ($planquantity) {
		$planname = ($planquantity * 10).'GB';
		$accounttype = '';
		if ($planyearly) {
			$planname .= ' Yearly';
			$planprice = ($planquantity * 9);
			$planpricedesc = 'AU$'.$planprice.' per year';
			$accounttype = 'GB Yearly';
		} else {
			$planname .= ' Monthly';
			$planprice = $planquantity;
			$planpricedesc = 'AU$'.$planprice.' per month';
			$accounttype = 'GB Monthly';
		}
		$pid = array_search($accounttype, array_column($stripeprices, 'accounttype'));
		$planpriceid = $stripeprices[$pid]['stripepriceid'];
	}
	break;
}

$session = false;
if ($billing_page && $index_activeusername) {
	/* Set up the stripe session */
	require('stripe-php/init.php');
	\Stripe\Stripe::setApiKey($stripesecretapikey); /* set in connectdbinfo.php */

	/* Check if the saved customer ID is still valid. */
	$customerId = $index_activeuser['stripecustomerid'];
	if ($customerId) {
		$customer = \Stripe\Customer::retrieve($customerId, []);
		if (isset($customer->deleted)) {
			$customerId = false;
		}
	}

	/* Create the customer reference for Stripe
	 * if it hasn't been done yet. */
	if (!isset($customerId) || !$customerId) {
		$email = $index_activeusername.'@'.$servername;
		/* Get any customers by email first. */
		$stripecustomerbyemail = \Stripe\Customer::all([
			'email' => $email,
		]);
		if (count($stripecustomerbyemail->data) == 0) {
			/* Create customer if not in stripe */
			$customer = \Stripe\Customer::create([
				'email' => $email,
			]);

			$customerid = $customer->id;
			$index_activeuser['stripecustomerid'] = $customerid;

			$query = sprintf("update accounts"
					." set stripecustomerid='%s'" /* CUSTID */
					." where accounts.id = %d limit 1", /* USERID */
					func_php_escape($customerid), $index_activeuserid);
			func_php_query($query);
		}
	}
} else if ($index_activeusername) {
	/* Invalid plan given, show plan details again. */
}
?>

<style>
/* Generic override styles */
.purchasedetails {
	width:100%;
}

.subscriptionoutput {
	font-weight:bold;
	margin:1em 0;
	text-align:center;
	padding:8px;
}

.greydiv {
	border-top:solid 1px #eee;
	border-bottom:solid 1px #eee;
	background:#f8f8f8;
}
.greendiv {
	border-top:solid 1px #dfd;
	border-bottom:solid 1px #dfd;
	background:#efe;
}

.MyCardElement {
	padding:8px;
}

caption, th, td {
	vertical-align:top;
	padding:0.5em;
}

th, td {
	text-align:left;
}

th {
	font-weight:bold;
}

caption {
	font-size:22px;
	color:#6a9;
	text-align:center;
	/* border-bottom:solid 1px #6a9; */
}

</style>

<script>
/* jsglobal (for billing.php) */
let card, cardElement;
var billing_activePriceId = '<?=$index_activeuser['stripesubscriptionpriceid'] ?? ''?>';
var billing_clientSecret = false;

function $(id)
{
	return document.getElementById(id);
}

<?php
if ($index_activeusername) { /* Use additional Stripe functions. */
?>

/**
 * Functions to change active subscription.
 * - billing_js_changePlan is called first to send the backend request.
 * - billing_js_planChanged is called from _js_changePlan, and shows the
 *	message to the user on success.
 */
function billing_js_planChanged(response)
{
	if (response.status == 'active') {
		$('subscriptionoutput').classList.remove('greendiv');
		$('subscriptionoutput').classList.add('greydiv');
		$('changeplansuggestions').classList.add('greendiv');
		$('changeplansuggestions').innerHTML = 'Subscription was successfully updated.'
				+'<br><a class="boldlink" href="/billing">View current plan</a>';
		$('changeplanpassword').disabled = true;
		$('changeplanbtn').disabled = true;
		$('changeplanbtn').className = 'silverbuttondisabled';
	}

	return response;
}

function billing_js_changePlan()
{
	let password = $('changeplanpassword').value;
	if (!password) {
		$('changeplansuggestions').innerHTML = 'Please enter your password.';
		return;
	}
	$('changeplansuggestions').innerHTML = 'Changing plan <img src="/img/thinking12.gif" width="12" height="12">';

	let fd = new FormData();
	fd.append('command', 'create-subscription');
	fd.append('username', '<?=$index_activeusername?>');
	fd.append('password', password);
	fd.append('customerId', '<?=$index_activeuser['stripecustomerid']?>');
	fd.append('priceId', '<?=$planpriceid?>');
	fd.append('quantity', '<?=$planquantity?>');
	fd.append('yearly', '<?=$planyearly?>');

	fetch('/stripe-backend.php', {
		method:'post',
		body:fd,
	})
	.then((response) => {
		return response.json();
	})
	/* Throw errors here to pass in catch below */
	.then((result) => {
		console.log(result);
		if (result.error) {
			throw result;
		}
		return result;
	})
	.then((changePlanResponse) => {
		return billing_js_planChanged(changePlanResponse);
	})
	.catch((result) => {
		console.log('<?=__LINE__?> error!');
		console.log(result);
		$('changeplansuggestions').innerHTML = result.error.message;
	});
}

/**
 * Functions to cancel subscriptions.
 * - billing_js_cancelSubscription is called first to send the backend request.
 * - billing_js_subscriptionCancelled is called from _js_cancelSubscription,
 *	and shows the message to the user on success. */
function billing_js_subscriptionCancelled(response)
{
	if (response.status == 'canceled') {
			$('cancelsubscriptionsuggestions').innerHTML = 'Subscription was successfully cancelled.';
	}

	return response;
}

function billing_js_cancelSubscription()
{
	let password = $('cancelsubscriptionpassword').value;
	if (!password) {
		$('cancelsubscriptionsuggestions').innerHTML = 'Please enter your password.';
		return;
	}
	$('cancelsubscriptionsuggestions').innerHTML = 'Cancelling subscription <img src="/img/thinking12.gif" width="12" height="12">';

	let fd = new FormData();
	fd.append('command', 'cancel-subscription');
	fd.append('username', '<?=$index_activeusername?>');
	fd.append('password', password);

	return fetch('/stripe-backend.php', {
		method:'post',
		body:fd,
	})
	.then((response) => {
		return response.json();
	})
	/* Throw errors here to pass in catch below */
	.then((result) => {
		console.log(result);
		if (result.error) {
			throw result;
		}
		return result;
	})
	.then((cancelSubscriptionResponse) => {
		return billing_js_subscriptionCancelled(cancelSubscriptionResponse);
	})
	.catch((result) => {
		console.log('<?=__LINE__?> error!');
		console.log(result);
		$('cancelsubscriptionsuggestions').innerHTML = result.error.message;
	});
}
<?php
} /* End Stripe-redirect func. */
?>

</script>

<?php
/* Show message if not signed in or purchasing a plan. */
if (!$index_activeuser && !$billing_page) {
?>
	<p>Billing features are only available to members.</p>
	<p><a class="boldlink" href="/pricing">View Plans</a></p>
<?php
/* Show page content if signed in or purchasing a plan. */
} else {
	if ($index_activeuser) {
		$accounttype = $index_activeuser['accounttype'];

		/* Only show current plan if not purchasing. */
		if ($accounttype && !($billing_page)) {
			$started = $index_activeuser['stripesubscriptioncurrentperiodstart'];

			$lastpayment = date('l jS F Y', strtotime($started));
?>
<div class="raisedwhitediv">
	<table class="plantable">
	<caption>Current Plan</caption>
	<tr>
	<th>Account</th>
	<td><?=$index_activeusername?><span class="greytext">@<?=$servername?></span></td>
	</tr>
	<tr>
	<th>Plan Name</th>
	<td><?=$accounttype?></td>
	</tr>
	<tr>
	<th>Last Payment</th>
	<td><?=$lastpayment?></td>
	</tr>
	</table>

	<p><a class="boldlink" href="/pricing">View or Change Plans</a></p>
	<details id="cancelsubscription" class="animatedSummaryDetails noMarkerDetails">
		<summary class="boldlink shortSummary">Cancel Subscription</summary>
		<div>
			<table class="formtable">
			<tr>
			<th><label for="cancelsubscriptionpassword">Password</label></th>
			<td><input id="cancelsubscriptionpassword" class="forminput shadowinset" name="cancelsubscriptionpassword" type="password"></td>
			</tr>
			<tr>
			<th></th>
			<td>
				<button class="silverbutton" id="cancelsubscriptionbtn" onclick="billing_js_cancelSubscription()">Cancel Subscription</button>
				<div id="cancelsubscriptionsuggestions">&nbsp;</div>
			</td>
			</tr>
			</table>
			<ul>
			<li>Before cancelling your subscription you should save a copy of everything associated with this account in case it gets removed during downgrading.</li>
			<li>Cancelling a subscription is done immediately.</li>
			<li>You will be refunded the full amount billed for this period, whether it is monthly or yearly.</li>
			<li>Any payment methods such as credit cards will be removed to prevent accidental future billing.</li>
			<li>Your account will be downgraded to the free tier. Any files exceeding the allocated storage space will be deleted, starting with oldest files first.</li>
			<li>All posts are kept, although attachments that exceed storage space will be removed.</li>
			<li>Once started, the process may take a few minutes depending on the amount of files linked to this account.</li>
			</ul>
		</div>
	</details>
</div>
<br>
<?php
		}

		/* Only show payment method if set. Stripe is the only method so we can use
		 * it to test if a payment method exists. */
		if ($index_activeuser['stripepaymentmethodid']) {
			$brand = ucwords($index_activeuser['stripepaymentbrand']);
			$last4 = $index_activeuser['stripepaymentlast4'];
			$expiry = $index_activeuser['stripepaymentexpiry'];

?>
<div class="raisedwhitediv">
	<table class="plantable">
	<caption>Payment Method</caption>
	<tr>
	<th>Card Details</th>
	<td><span class="greytext">xxxx xxxx xxxx</span> <?=$last4?>
		<br><?=$brand?> &bull; Expires <?=$expiry?></td>
	</tr>
	</table>
</div>
<?php
		}
	}

	/* No current subscriptions */
	if (($billing_page != 'payment') && !$accounttype) {
?>
<p class="information">You have no current subscriptions.</p>
<p>If you have recently purchased a plan, it may take a few minutes to show here.</p>
<p>Otherwise, you can <a class="boldlink" href="/pricing">click here to Choose A Plan</a>.</p>
<?php
	}

	/* Show payment section */
	if ($billing_page == 'payment') {
?>
<div class="debugpostborderclass raisedwhitediv">
	<table class="purchasedetails">
	<caption>Purchase Details</caption>
<?php
	/* Only show username if signed in */
	if ($index_activeusername) {
?>
	<tr>
	<th>Account</th>
	<td><?=$index_activeusername?><span class="greytext">@<?=$servername?></span></td>
	</tr>
<?php
	}
?>
	<tr>
	<th>Plan</th>
	<td><?=$planname?> <span class="greytext">[<a class="smalllink" href="/pricing/<?=$planchangelinkoptions?>">Change</a>]</span></td>
	</tr>
	<tr>
	<th>Price</th>
	<td><?=$planpricedesc?></td>
	</tr>
	</table>

<?php
		/* START Show signin-register or buy-now button */
		if (!$index_activeusername) {
?>
<!-- Close "Purchase Details" div early --></div>

<p class="centered">To purchase a plan, an account is needed.</p>
<?php

require('signin.php');

		} else {
			/* Show buy-now button */
			if (($index_activeuser['stripesubscriptionpriceid'] == $planpriceid)
					&& ($index_activeuser['stripesubscriptionquantity'] == $planquantity)) {
?>
	<p class="information">You are currently subscribed to this plan.</p>
	<p><a class="boldlink" href="/pricing">View or Change Plans</a></p>
	<p><a class="boldlink" href="/billing">Manage or Cancel Subscription</a></p>

<?php
			} else {
				/* Load js to show price difference. */
				if (($index_activeuser['stripesubscriptionpriceid'])
						|| ($index_activeuser['stripesubscriptionpriceid'] != $planpriceid)) {
?>

<script>

/**
 *
 */
function billing_js_getUpcomingInvoice(newPriceId, newQuantity)
{
	console.log('<?=__LINE__?> getting invoice...');
	let fd = new FormData();
	fd.append('command', 'get-invoice');
	fd.append('username', '<?=$index_activeusername?>');
	fd.append('newPriceId', newPriceId);
	fd.append('newQuantity', newQuantity);
	fetch('/stripe-backend.php', {
		method:'post',
		body:fd
	})
	.then((response) => {
		return response.json();
	})
	.then((result) => {
		console.log('billing.php:<?=__LINE__?> Dumping result for getUpcomingInvoice...');
		console.log(result);
		let price = result.newPrice;
		if (price !== 'none') {
			$('subscriptionoutput').innerHTML = price;
		}
	})
	.catch(error => console.warn(error));

	return false;
}

billing_js_getUpcomingInvoice('<?=$planpriceid?>', '<?=$planquantity?>');
</script>

	<div id="subscriptionoutput" class="subscriptionoutput greendiv">
		Calculating charges <img src="/img/thinking12.gif" width="12" height="12"><br>&nbsp;
	</div>
<script>
/**
 * Get cost of invoice about to be paid for.
 */
function billing_js_createSubscription(priceid, quantity)
{
	console.log('<?=__LINE__?> creating subscription priceid='+priceid+' quantity='+quantity);
	let fd = new FormData();
	fd.append('command', 'create-subscription');
	fd.append('customerId', '<?=$index_activeuser['stripecustomerid']?>');
	fd.append('priceId', priceid);
	fd.append('quantity', quantity);

	fetch('/stripe-backend.php', {
		method:'post',
		body:fd,
	})
	.then((response) => {
		return response.json();
	})
	.then(result => {
		console.log('<?=__LINE__?> dumping result for create-subscription...');
		console.log(result);
		console.log('<?=__LINE__?> result.status = '+(result.status));
		if (result.status === 'incomplete') {
			$('subscriptionoutput').innerHTML = result.displayedCharge;

			const clientSecret = result.clientSecret;
			localStorage.setItem('clientSecret', clientSecret);

			console.log('<?=__LINE__?> clientSecret='+clientSecret);
			const lsClientSecret = localStorage.getItem('clientSecret');
			console.log('<?=__LINE__?> localStorage clientSecret='+lsClientSecret);
		} else if (result.status == 'active') {
			console.log('pricing.php:<?=__LINE__?> active! (but it shouldnt be...)');
		} else if (result.status == 'success') {
			console.log('pricing.php:<?=__LINE__?> success! (What do we do here?...)');
		} else {
			console.warn("<?=__LINE__?> Something went wrong. Try again.");
			console.log(result);
		}
	})
	.catch(error => console.warn(error));

	return false;
}

billing_js_createSubscription('<?=$planpriceid?>', <?=$planquantity?>);
</script>
<?php
				}
?>
	<div style="max-width:30em;margin:1em auto;">
<?php
				/* If already subscribed, show password field to change plans. */
				if ($index_activeuser['accounttype']) {
?>
		<table class="formtable">
		<tr>
		<th><label for="changeplanpassword">Password</label></th>
		<td><input type="password" id="changeplanpassword" class="forminput shadowinset" name="changeplanpassword"></td>
		</tr>
		<tr>
		<th></th>
		<td><button class="silverbutton" id="changeplanbtn" onclick="billing_js_changePlan()">Change Plan</button>
		</td>
		</tr>
		</table>
		<div id="changeplansuggestions" class="subscriptionoutput">&nbsp;</div>
<?php
				/* Otherwise show the credit card payment form. */
				} else {
?>
			<form id="payment-form">
				<div id="card-element" class="MyCardElement shadowinset">
					<!-- Elements will create input elements here -->
				</div>

				<!-- Error messages go in the following div -->
				<div id="card-element-errors" role="alert"></div>
				<div id="cardSubmitField" class="subscriptionoutput">
				<button id="subscribeButton" type="submit" class="silverbutton">Subscribe</button>
			</div>
		</form>
<?php
				}
?>
	</div>
<?php
			} /* End show payment form for plan not subscribed in */
		} /* END Show signin-register or payment form */
?>
	<p>With any plan you can cancel and get the full amount you have paid within 30 days of purchasing, no questions asked.</p>
	<p>After the 30 day period, refunds are handled "pro-rata", where you will be refunded for the remaining time left on the plan.</p>
	<p>Even though we don't ask people why they want a refund, we appreciate and value feedback from anyone!</p>
</div>
<?php
	}
?>

<script>
/**
 * Portions of the following code copied from the Stripe subscription demo.
 */
<?php
	if ($planpriceid && $index_activeusername) { /* Only show stripe functions if signed in and viewing a plan */
?>
var stripe = Stripe('<?=$stripepublicapikey?>'); /* Set in connectdbinfo.php */

function billing_js_displayError(ev)
{
	if (ev.error) {
		$('card-element-errors').textContent = ev.error.message;
	} else {
		$('card-element-errors').textContent = '';
	}
}

/* Set up card element */
if ($('card-element')) {
	var cardElementStyle = {
		base: {
			color: "#32325d",
			fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
			fontSmoothing: "antialiased",
			fontSize: "16px",
			"::placeholder": {
				color: "#aab7c4"
			}
		},
		invalid: {
			color: "#fa755a",
			iconColor: "#fa755a"
		}
	};

	let elements = stripe.elements();
	cardElement = elements.create('card', { style: cardElementStyle });
	cardElement.mount("#card-element");

	cardElement.on('focus', function() {
		$('card-element-errors').classList.add('focused');
	});

	cardElement.on('blur', function() {
		$('card-element-errors').classList.remove('focused');
	});

	cardElement.on('change', function(ev) {
		billing_js_displayError(ev);
	});

	/* Set up the form actions */
	let paymentform = $('payment-form');
	if (paymentform) {
		paymentform.addEventListener('submit', function(ev) {
			ev.preventDefault();

			const clientSecret = localStorage.getItem('clientSecret');
			console.log('<?=__LINE__?> clientSecret='+clientSecret);
			$('cardSubmitField').innerHTML = 'Processing payment <img src="/img/thinking12.gif" width="12" height="12">';

			stripe.confirmCardPayment(clientSecret, {
				payment_method: {
					card: cardElement,
					billing_details: {
						name: '<?=$index_activeusername?>@<?=$servername?>',
					},
				}
			}).then((result) => {
				console.log('<?=__LINE__?> dumping result after payment...');
				console.log(result);
				if (result.error) {
					alert(result.error.message);
				} else {
					/* Payment was successful. Go to next page */
					let amt = result.paymentIntent.amount;
					let amtDollars = amt / 100;
					let amtShown = 'AU$'+(amtDollars.toFixed(2));
					$('subscriptionoutput').classList.remove('greendiv');
					$('subscriptionoutput').classList.add('greydiv');
					$('cardSubmitField').classList.add('greendiv');
					$('cardSubmitField').innerHTML = 'Payment of '+amtShown+' was successful!'
							+'<br><a class="boldlink" href="/billing">View current plan</a>';
				}
			});

			return false;
		});
	}
}

function billing_js_handlePaymentThatRequiresCustomerAction({
		subscription, invoice, priceId, paymentMethodId, isRetry, })
{
	if (subscription && subscription.status === 'active') {
		/* subscription is active, no customer actions required. */
		return { subscription, priceId, paymentMethodId };
	}

	/* If it's a first payment attempt, the payment intent is on the
	 * subscription latest invoice.
	 * If it's a retry, the payment intent will be on the invoice itself. */
	let paymentIntent = invoice ? invoice.payment_intent : subscription.status;

	if (subscription.status === 'requires_action'
			|| (isRetry === true
			&& paymentIntent.status === 'requires_payment_method')) {
		return stripe.confirmCardPayment(subscription.client_secret,
					{payment_method: paymentMethodId,})
		.then((result) => {
			if (result.error) {
				/* Start code flow to handle updating the payment details
				 * Display error message in your UI.
				 * The card was declined (i.e. insufficient funds,
				 * card has expired, etc) */
				throw result;
			} else {
				if (result.paymentIntent.status === 'succeeded') {
					/* There's a risk of the customer closing the window
					 * before callback execution. To handle this case,
					 * set up a webhook endpoint and listen to invoice.paid.
					 * This webhook endpoint returns an Invoice. */
					return {priceId: priceId,
						subscription: subscription,
						invoice: invoice,
						paymentMethodId: paymentMethodId};
				}
			}
		});
	} else {
		/* No customer action needed. */
		return { subscription, priceId, paymentMethodId };
	}
}

function billing_js_handleRequiresPaymentMethod({ subscription, paymentMethodId, priceId, })
{
	if (subscription.status === 'active') {
		/* Subscription is active, no customer actions required. */
		return { subscription, priceId, paymentMethodId };
	} else if (subscription.status === 'requires_payment_method') {
		/* Using localStorage to store the state of the retry here
		 * (feel free to replace with what you prefer)
		 * Store the latest invoice ID and status. */
		localStorage.setItem('latestInvoiceId', subscription.latest_invoice_id);
		localStorage.setItem('latestInvoicePaymentIntentStatus',
			subscription.latest_invoice_payment_intent_status
		);
		throw { error: { message: 'Your card was declined.' } };
	} else {
		return { subscription, priceId, paymentMethodId };
	}
}

/**
 * Shows a success / error message when the payment is complete
 */
function billing_js_onSubscriptionComplete(result)
{
	localStorage.clear();

	window.location.href = '/billing';
}

function billing_js_createSubscription({ customerId, paymentMethodId, priceId })
{
	let fd = new FormData();
	fd.append('command', 'create-subscription');
	fd.append('customerId', customerId);
	fd.append('paymentMethodId', paymentMethodId);
	fd.append('priceId', priceId);
	return (
		fetch('/stripe-backend.php', {
			method:'post',
			body:fd,
		})
		.then((response) => {
			return response.json();
		})
		.then((result) => {
			/* If the card is declined, display an error to the user. */
			if (result.error) {
				/* The card had an error when trying
				 * to attach it to a customer. */
				throw result;
			}
			return result;
		})
		.then((result) => {
			/* Normalize the result to contain the object returned
			 * by Stripe. Add the additional details we need. */
			return {
				/* Use the Stripe 'object' property on the
				 * result to understand what is returned. */
				subscription: result,
				paymentMethodId: paymentMethodId,
				priceId: priceId,
			};
		})
		/* Some payment methods require additional actions.
		 * Eg: 2FA for cards. */
		.then(billing_js_handlePaymentThatRequiresCustomerAction)

		/* If attaching this card to a Customer object succeeds,
		 * but attempts to charge the customer fail. You will
		 * get a requires_payment_method error. */
		.then(billing_js_handleRequiresPaymentMethod)

		/* No more actions required. */
		.then(billing_js_onSubscriptionComplete)

		/* If an error has happened, display the failure to the user here. */
		.catch((error) => {
			/* Utilize the HTML element we created. */
			billing_js_displayError(error);
		})
	);
}

/**
 *
 */
function billing_js_retryInvoiceWithNewPaymentMethod(
		{customerId, paymentMethodId, invoiceId, priceId})
{
	let fd = new FormData();
	fd.append('command', 'retry-invoice');
	fd.append('customerId', customerId);
	fd.append('paymentMethodId', paymentMethodId);
	fd.append('invoiceId', invoiceId);

	return (
		fetch('/stripe-backend.php', {
			method: 'post',
			body: fd,
		})
		.then((response) => {
			return response.json();
		})
		/* If the card is declined, display an error to the user. */
		.then((result) => {
			if (result.error) {
				/* The card had an error when trying
				 * to attach it to a customer. */
				throw result;
			}
			return result;
		})
		/* Normalize the result to contain the object returned
		 * by Stripe. Add the additional details we need. */
		.then((result) => {
			return {
				/* Use the Stripe 'object' property on the
				 * result to understand what is returned. */
				invoice: result,
				paymentMethodId: paymentMethodId,
				priceId: priceId,
				isRetry: true,
			};
		})
		/* Some payment methods require additional actions.
		 * Eg: 2FA for cards. */
		.then(billing_js_handlePaymentThatRequiresCustomerAction)

		/* No more actions required. */
		.then(billing_js_onSubscriptionComplete)

		/* If an error has occurred, display it to the user */
		.catch((error) => {
			billing_js_displayError(error);
		})
	);
}

/**
 * Set up payment method for recurring usage.
 */
function billing_js_createPaymentMethod({ cardElement, isPaymentRetry, invoiceId }) {
	const stripeCustomerId = '<?=$index_activeuser['stripecustomerid']?>';

	let priceId = '<?=$planpriceid?>';
	stripe.createPaymentMethod({
		type: 'card',
		card: cardElement,
	})
	.then((result) => {
		if (result.error) {
			billing_js_displayError(result);
		} else {
			if (isPaymentRetry) {
				/* Update the payment method and retry invoice payment. */
				billing_js_retryInvoiceWithNewPaymentMethod({
					customerId: stripeCustomerId,
					paymentMethodId: result.paymentMethod.id,
					invoiceId: invoiceId,
					priceId: priceId,
				});
			} else {
				// Create the subscription
				billing_js_createSubscription({
					customerId: stripeCustomerId,
					paymentMethodId: result.paymentMethod.id,
					priceId: priceId,
				});
			}
		}
	});
}
<?php
	} /* END Show stripe functions if viewing plan and signed in */
?>
</script>

<?php
} /* End only show if signed in or purchasing a plan. */
?>

</div>
</div>
