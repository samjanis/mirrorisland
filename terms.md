# Terms of Use

Version 0.1 (AU) Alpha
Issued: December 31, 2017
Security Overview: Experimental/Untested

_**Development Notice:** This website uses untested software and may contain incomplete features including: privacy controls that do not prevent unauthorised access, malfunctioning editing features, or unsaved draft posts that can not be removed easily. By using this site you acknowledge that this is experimental software._

**These terms of use are preserved with your account on registration.** When new terms come out you have the option to stay with currently accepted terms (a legal term knows as &quot;grandfathered&quot;) and continue to use the site according to that agreement, or you can forfeit the preserved terms to use the new ones. There is no requirement to accept any new terms of use to continue using the site, only the currently accepted terms of use will apply.

## Overview

Our mission is to **build** a revolutionary online community, provide a platform to **publish** media, content and ideas to the right audience, and **improve** this website according to the needs of its users.

You can use this site for any purpose **including commercial purposes**, **collaborate** with group editing and commenting facilities, and **operate a business** by providing products and services through this site.

Understanding that **you are in complete control** of who can access your content as we will never sell or analyze your data for surveillance or marketing purposes, or give your data to anybody else, and **advertisements are optional** so you can enable or disable them on posts you publish and pages you view.  

There are a few conditions, however. **This site is available &quot;as-is&quot;** and is provided without any warranty. All content and behaviour is the **responsibility** of the person who uploaded it. You mush **abide by the laws and regulations** in your geographical location and jurisdiction. This site encourages a supportive environment for all, and **nobody is to harass or harm anybody**. You **must not tie up resources** such as the network bandwidth or server storage for other people (although this is difficult to do unless done intentionally) You must **respect the privacy of our users** and not use this site for tracking or surveillance purposes. Finally, you must **adhere to these terms of use** and applicable **rules of conduct for various communities** on this site set out by their moderators.

## Table of Contents
1. <a href="#definitions">Definitions</a>
2. <a href="#services">Services</a>
3. <a href="#userconduct">User Conduct</a>
4. <a href="#accounts">Accounts</a>
5. <a href="#copyright">Copyright</a>
6. <a href="#sourcecode">Source Code</a>
7. <a href="#privacy">Privacy</a>
8. <a href="#advertising">Advertising</a>
9. <a href="#payments">Payments</a>
10. <a href="#Incidents">Incidents</a>
11. <a href="#updates">Updates</a>

## <a name="definitions">Definitions</a>

&quot;AGREEMENT&quot; refers to the conditions laid out in this document, also known as &quot;TERMS OF USE&quot;.

&quot;US&quot;, &quot;WE&quot;, and &quot;ITS OPERATORS&quot; refers to the owners of this site.

&quot;THIS WEBSITE&quot; or &quot;THIS SITE&quot; specifically refers to your network communications with the domain this agreement is hosted on.

&quot;CONTENT&quot; refers to all forms of user-submitted data on this site, including but not limited to; information and ideas, videos, music and audio, images, photos, files, and binary data.

&quot;SERVICE&quot; refers to the exchanging and storage of information between us and its users on this website.

&quot;USERS&quot; refers the any person or bot who uses this site, whether they are signed in, not signed in, or not registered.

&quot;ACCOUNT HOLDERS&quot; refers to active users with an account on this site when they are signed in.

&quot;PERMISSIONS&quot; refers to the user-set mechanism on this site that determines which users can or can not access various sections of content.

Throughout this document, the terms &quot;WILL&quot;, &quot;MUST&quot;, &quot;IS&quot;, &quot;SHOULD&quot;, &quot;MAY&quot;, &quot;CAN&quot; and &quot;WILL NOT&quot;, &quot;MUST NOT&quot;, &quot;IS NOT&quot;, &quot;SHOULD NOT&quot;, &quot;CAN NOT&quot; are to be interpreted for their meaning.

## <a name="services">Services</a>

Acceptance to these Terms does not grant us the right to give or sell your content for a profit, nor will you be monitored or identified for marketing or surveillance purposes.

### Limit of Liability

You understand and agree that:

- our website is provided &quot;AS-IS&quot;, and we assume no responsibility for the timeliness, deletion, mis-delivery or failure to store any user data or personalisation settings. Access to our website may be interrupted by a variety of factors including, but not limited to: equipment failure, the need for routine maintenance, and peak demand. Information and services are used entirely at the risk of the visitor or member.

- we may change the layout, features, or visual appearance of this site at any time possibly reducing the availability or functionality of this site.

- links may provided on this website that connect to other websites and resources. Because we have no control over other websites and resources, you acknowledge and agree that we are not responsible for the availability of these external websites and resources, and do not endorse and are not responsible or liable for any content from these websites or resources. You waive any claim resulting from your exposure to material on or through us that is offensive, indecent, or otherwise objectional.

- this site may display content that is inaccurate, contains errors, or is in violation of these terms. Although we have provided the users of this website with moderation tools we make no guarantee of this site's completeness, accuracy or suitability.

### Content On This Site

By submitting or uploading content on this site you grant us the legal right to store and publish the submitted or uploaded content according to the permissions you have provided. This site will analyze the content you submit to a limited extent, including obtaining information for searching, although a user-set mechanism (known as &quot;permissions&quot;) limits who may recieve the results from the analysis of the content. Due to the varying nature of permissions, we can not personally analyze all content that we recieve, nor will we pass that privilege on to other parties, nor will we sell or release the data obtained from the analysation processes to any third party.

Information and content on this site is user-submitted is not to be accepted as professional or legal advice. Since the content can come from creative sources it is up to users to verify the integrity and accuracy of information before utilising it.

### Confidentiality

The confidentiality of information or content transmitted over the internet either to or through this site can not be guaranteed. Although we do not analyze the content published on this site for surveillance or marketing purposes, there are other avenues where third parties may intercept the data when it is transferred to or from this site, including web browser plugins, cached pages, man-in-the-middle attacks, or the monitoring of computer networks. We can not guarantee or verify the privacy, confidentionality or protection of any user and their content.

### Content Access Based On User Identification

The legality of the content you access on this site can vary depending on your geographical location or jurisdiction. By using this site, you affirm you have the required legal rights to access the appropriate content. Since we can not guarantee the geographical location or jurisdiction of any user on this site, we can not limit or restrict the content they have access to based on information they have provided us. Under no circumstances are we liable for any violation. We can not determine if the content you access is legal or not.

### Backup

Although we take reasonable measures to preserve the content on this site and user accounts in their current state, it is your responsibility to keep a backup copy somewhere other than on this site. Other reasons for keeping a backup include; the encryption or wiping of data for security reasons.

### Content Removal

We do not screen content as it is submitted, and only if we have been granted moderative privileges (via permissions) to the content or particular section of this site can we refuse or remove any content that violates these terms of use.

## <a name="userconduct">User Conduct</a>

This site was built with the purpose of distributing ideas, art, media and services. All content on this site is the sole responsibility of the user that submitted it.

Use, reproduction and modification of content on this site is subject to the license it is published under. It is up to the user to ensure that they are legally permitted to access, modify, and interact with the content.

Parts of this site are under the control of its users, and the availability of data they have administration rights to are under their authority. These permissions determine who can or can not have the privilege to access content. Under no circumstances can anybody, including the operators of this site, access content they do not have privileges for.

You can edit and delete all content on this site under ownership of your account at any time, and the content of other users if you have been given permissions to do so. Under no circumstances can any user or the owners of this site ask another user for permissions that allow content to be deleted, edited, or hidden for the purposes of civil or political censorship. It is up to the user of the account to determine if a civil or political dispute is involved.

Users must choose the right target audience when publishing content. Examples of content that must be flagged for suitability may include adult content, war scenes, surgical procedures, or coarse language. Users must publish content in accordance both in their jurisdiction and the audience to their content.

This site can not be used to monitor the activity of others either by the owners of this site or any of its users, whether they are signed in, not signed in, or non-members of this site.

Since we can not confirm who users actually are or examine the content they choose to upload, we can not be involved in user interactions or control their behaviour on this site. Your acknowledge and agree that we are not liable for any disputes that arise between users of this site

### Scraping

Web scraping or &quot;scraping&quot; is the process of processing human-readable HTML pages on this website to extract content using an automated method such as with a bot or web crawler. Users may scrape this site for any reason as long as they:

a. have the permissions for, and legal access to the data,

b. are not going to use the contact details gained within the data for spamming purposes, and

c. will not sell the information itself for a profit, unless they have been provided explicit permissions from the content owner (or owners) to do so.

### Prohibited Activities

To maintain a supportive environment, we probihit certain activities that may be harmful or discouraging to other users, including:

- **Harassment** such as threats, stalking, vandalising other users' pages, and sending spam or junk mail to other users is also considered harassment.

- **Violating Privacy** such as extracting information, content, or data from other users without their explicit permissions, or the intent to obtain personally identifiable information from anybody under the age of 18 to violate any laws for the health, safety, and well-being of minors.

- **False or Fraudulant Activities** where any content claims to be factual information with the intent to decieve, or attempts to impersonate or misrepresent another person, or engage in fraud.

- **Distributing Illegal Material** including malicious computer viruses, worms, trojans, or other code that could harm this website or the computers of our users.

- **Disrupting our services** by using automated methods (such as a bot) that excessively consume network resources, preventing others from using this site, or by intentionally finding vulnerabilities with this site with the intent to cause harm, unless:
		+ your actions do not disrupt our services or the actions of other users,
		+ those actions are not for malicious or destructive intent, and
		+ any vulnerabilities found are reported to the site owner.

When reporting vulnerabilities, you can choose to be credited for your work.

## <a name="accounts">Accounts</a>

### Account Elegibility

Anybody can register for an account, whether it is a single person, a group of people, a robot or other, as long as the account was registered&mdash;or initiated in the case of a robot or other automation method&mdash;by a person who is legally permitted to hold an account on this website, and any of the content accessed or submitted by the account holder (or holders) must reach the appropriate audience.

There is no limit to the number of accounts each user can hold, and there is no limit to the number of users (whether human or bot) that can use a single log in, as long as all members of the account are legally permitted to view, modify, or interact with content the account can access.

Ages an account holder must be, include:

- **at least 13 years** if the user of of website (or a copy of this website) is **in the USA** in accordance with the COPPA (Children's Online Privacy Protection Act).

- **at least 16 years** if the user of this website (or a copy of this website) is **in the EU** in accordance with the GDPR (General Data Protection Reguation).

Other jurisdictions and regulations may vary to the ones listed above. If a user doesn't meet the minimum age requirement for their applicable jurisdiction, they are not permitted to hold an account of their own on this website.

### Account Login Details

When users sign up for an account on this site, they will provide a username and password combination used to access the associated account. Users with login details are responsible for keeping their username and password details safe, and should not be share them with anybody. You agree and understand we can not and will not be held liable for any loss or damage occuring from any user failing to comply with these security obligations.

### Account information

Account information is the personally identifiable information that is not part of any post. There is no requirement to make sure all information is true and accurate. The account information can be as creative as you like as long as it follows all of the conditions set out in these terms of use.

### Editing and Deleting Account Information

You can edit and delete all account information on this site under ownership of the account at any time. Removal of account information is immediate and permanent. Additionally, it is possible to edit other accounts not under your ownership if you have been given permissions to do so.

### Account Termination

Account holders can terminate their account at any time for any reason, such as whether they are dissatisfied with the service, would like to remove all information associated with their account, or do not require the service any more. Only users with the correct username/password information can close an account. We can not and will not terminate accounts using any other methods including, but not limited to, written, fax, phone or email requests.

You can access the account termination page at the bottom of the profile settings page and by following the link &quot;Delete Your Account&quot; link. The deletion process is immediate and irreversable, which can not be undone. Although we take great care in removing all information associated with the account being deleted, some content may remain due to factors including; copies to other accounts, the original account holder transferring ownership, or being one of multiple recipients in a collaborative environment.

## <a name="copyright">Copyright</a>

By publishing information or uploading content to this site, you affirm you have all required rights to publish or upload the content.

Upon submission, you can choose from a variety of licenses with varying degrees of usage, depending on the target audience. There are different licenses for text, images, video, and code. You need to choose the correct one when publishing content, especially depending on how the content will be used with subsequent users.

When posting content that is not original, or from another source other that yourself, attribution such as crediting the original author or artist is required. You must not take credit for work that is not your own in this site.

## <a name="sourcecode">Source Code</a>

The source code of this web site is released under the GNU Affero GPL License version 3.0.

## <a name="privacy">Privacy</a>

When you acces our site, we recieve certain information your browser provides including your IP address, browser version and operating system. This information may not be accurate and is only a speculation of what browser and operating system you could be using. This data is used internally and we do not share this information with anyone.

You have permission controls to determine who has access to the content you submit. We treat the content we do not have access to as confidential, and we can only access it with your explicit consent by you granting us access permissions to the content. We will respect the permissions you have set for your content and will enforce those permissions to protect the content when necessary.

### Cookies

When using account-based services, we will set up and use cookies for the sole purpose of identifying your browser with the account that is signed in. We will not use cookies to track you for advertising  or surveillance purposes. If you do not need to sign in to an account in this site, we strongly recommend that you turn off cookies as they are not needed if not signed in.

### Monitoring

Monitoring users is only restricted to network, disk, memory, and CPU usage on servers and equipment to ensure the service is running optimally. This information does not reveal anything about the content or information being monitored.

### Semantics

When you enter text in the post editor, we may recieve a portion of what you are typing, but not the full body of text. We use this portion of text to try and provide a link of semantic data. We do not save the portion text, nor do we keep record of which semantic tags have been suggested. Semantic autocomplete is anonymous and optional (you can turn it on or off).

## <a name="advertising">Advertising</a>

Advertising is optional for both yourself and others who view your page. You can choose whether you would like to see ads on pages you visit, and to have ads displayed on your posts or pages.

The only way we will draw relevance with the advertisements displayed and the target audience is through search keywords associated with the posts displayed on the page.

We do not keep any record of which search keywords or pages advertisements were displayed with. However, the timestamps and amount of times an advertisement was displayed will be recorded:

a) to display advertisements relatively equally, in proportion to their placement priority, and

b) for advertisers to monitor the performance of their advertising campaigns.

## <a name="payments">Payments</a>

Although we do not charge for general use of the site, we may charge for additional features that are not considered general use, including; advertising, priority support, or customised implimentations of this site. Fees may by applicable during transactions - find &quot;Fees ad Charges&quot; to view applicable amounts.

## <a name="incidents">Incidents</a>

An incident is any action or inaction outside of regular or planned course of action. Actions that are considered incidents include unauthorised access to information or content on this site, and hardware or software failures resulting in undesirable actions.

Throughout an incident response plan we will provide you with information regarding the status and severity of the incident (if applicable). For reliable communication to be possible, you will need to provide an external email address in case Mirror Island itself is compromised.

When this happens we will:

1. Investigate the current situation and if required, notify you of a possible incident.

2. Notify users if an an incident has or has not been discovered.

3. If one has, notify users when an incident response (IR) sequence has been initiated, and

4. Continually inform users of the IR sequence progress.

In the event that unauthorised access to content on this site is imminent and determined to be a critical security threat, we will unconditionally wipe all data on this site as a security measure. This method is only used as a last resort when all other methods are not sufficient. For this reason we strongly recommend backup up data or at least saving data to another location.

## <a name="updates">Updates</a>

We update these terms on a regular basis to keep up with laws and technology. When updates happen, users on this site will be notified what changes have been made but the new terms do not come in to effect for each user until they accept the new terms.

You can be involved too! Community input is essential for this website and its platforms to stay relevent. You can contact the site owner to see how you can be involved with the next release or update to these terms.

EOF
