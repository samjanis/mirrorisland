<html>
<head>
<title>Terms of Use 0.1 Alpha</title>
<style type="text/css">
body { font:normal 16px 'sans-serif'; font-weight:semi-bold; color:#567; background:#f0f0f3; line-height:24px; }
.maindiv { background:#fafafe; max-width:600px; padding:15px 25px; margin:10px auto 10px auto; border:solid 1px #dde; border-radius:8px; }
pre { white-space:pre-wrap; font:inherit; max-width:600px; margin-left:auto; margin-right:auto; }
#footer { color:#79a; padding:10px 20px; text-align:center; margin:30px 0 0 0; border-top:solid 1px #cde; }
#footer div { margin:10px; }
#footer a { background:none; color:#57f; text-decoration:none; margin:auto 4px; }
#footer a:hover { text-decoration:underline; }
</style>
</head>

<body>

<div class="maindiv">
<pre>
<?php
require('terms.md');
?>
</pre>
</div>

<div id="footer">
	<div>
		<a href="/"><strong>Mirror&nbsp;Island</strong></a>
		<a href="/about.php">About</a>
		<a href="/contact.php">Contact Us</a>
		<a href="/terms.php">Terms of Use</a>
		<a href="/manifesto.php">Manifesto</a>
		<a href="/quickstart.php">Quickstart Guide</a>
		<a href="/features.php">Feature Tour</a>
		<a href="/advertise.php">Advertise</a>
		<a href="/reportabug.php">Report a Bug</a>
	</div>
</div>
</body>
</html>
