<?php
require('func.php');

/**
 * TODO - Placeholder code. For use in future projects.
 */
function backend_php_sendWebsockMessage()
{
	$message = array('username' => 'backend.php', 'chatmessage' => 'Test message from backend!');

	$json = json_encode($message);
	define('MSGTYPE', 1);

	$attempts = 5;
	$md5 = md5($json);
	do {
		/* Send the message to websock.php */
		$backend_key = ftok('backend.php', 'A');
		$backend_queue = msg_get_queue($backend_key);
		msg_send($backend_queue, MSGTYPE, $json);

		/* Get a confirmation reply from websock.php */
		$websock_key = ftok('websock.php', 'A');
		$websock_queue = msg_get_queue($websock_key);
		msg_receive($websock_queue, MSGTYPE, $msgtype, 4096, $reply,
				false); /* Use blocking */
		$replymd5 = unserialize($reply);
		$attempts--;
	} while (($attempts > 0) && ($replymd5 !== $md5));
}

/**
 *
 */
function backend_php_savePost($command)
{
	global $mysqli;
	global $index_activeuserid, $index_activeusername, $index_activefullname;
	global $servername, $servertitle, $urlhome;
	global $index_activeuserfileuse, $index_activeuserfilespace;

	$backendarray = array('error' => 'ERROR_EDIT_POST');
	$newpostparentpermissions = false;
	$originalaccountid = 0; /* Set correct senderaccountid with recipients */
	$emailsubject = '';
	$awsloaded = false;

	$id = func_php_inttext($_POST['editpostid'], 255);
	$added = 0;
	$username = '';
	$parentid = func_php_inttext($_POST['newpostparentid']);
	$postid = func_php_inttext($_POST['newpostpostid']); /* Used for identifying replies to guest posts. */
	$viewmode = func_php_cleantext($_POST['newpostviewmode'], 64);
	$inlinereply = func_php_inttext($_POST['inlinereply']);
	$showparent = false;
	$parentpermissions = false;
	if ($parentid != 0) {
		$parentpermissions = index_php_getuserpermissions($parentid, $index_activeuserid);
		if (!$parentpermissions)
			$parentid = 0;
	}

	/* Branch - Editing an existing post or adding a new one?
	 * If editing a new one (handled a few lines down) generate
	 * a blank post and used its ID to save post data to. */
	if ((($command == 'edit_post') || ($command == 'edit_message'))
			&& ($id > 0)) {
		/* First, make sure we're in full view mode */
		if ($viewmode != 'full') {
			$backendarray['error'] = 'ERROR_EDIT_POST_INVALID_VIEW_MODE';
			return $backendarray;
		}

		/* Next check if post exists and the user has permission to edit. */
		$permissions = index_php_getuserpermissions($id, $index_activeuserid);
		if (!$permissions['canedit']) {
			$backendarray['error'] = 'ERROR_EDIT_POST_INVALID_POST_OR_USER_ID';
			return $backendarray;
		}
		$added = $permissions['added'];
		$authorusername = $permissions['username'];
		$originalaccountid = $permissions['accountid'];

		/* Showparent is used later for _insertPostCredits and _insertPopupMenu functions. */
		if (($command == 'edit_post') && (isset($_POST['editpostshowparent']))) {
			$showparent = func_php_cleantext($_POST['editpostshowparent'], 64);
			if ($showparent == 'true') {
				$showparent = true;
			} else {
				$showparent = false;
			}
		}
	} else if (($command == 'new_post') || ($command == 'new_postreply')
			|| ($command == 'new_message')) {
		if ($parentpermissions && !$parentpermissions['canreply']) {
			$backendarray['error'] = 'ERROR_NEW_POST_CAN_NOT_REPLY';
			return $backendarray;
		}

		$preapproved = 0;
		$postreplystyle = '';
		if ((($viewmode == 'full') || ($viewmode == 'thumbnail'))
				&& !$inlinereply) {
			$postreplystyle .= ' debugreplyborderclass';
		}

		if ($viewmode == 'thumbnail') {
			$postreplystyle .= ' debugpostthumbnail';
		} else if ($viewmode == 'list') {
			$postreplystyle .= ' debugpostlist';
		}

		if ($parentid > 0) {
			$postreplystyle .= ' postapprovalnormal';
			if ($parentpermissions['preapproved']) {
				/* Pre-approve post */
				$preapproved = 1;
			} else {
				$postreplystyle .= ' postapprovalpending';
				$backendarray['poststatus'] = 'This post is pending approval.';
			}
		}
		$backendarray['postreplystyle'] = $postreplystyle;

		/* New post or reply - generate a new post id to save to */
		$query = sprintf('/* '.__LINE__.' */ insert into posts (parentid, accountid, added)'
				.' values (%d, %d, NOW());', $parentid, $index_activeuserid);
		$backendarray['insertquery'] = $query;
		func_php_query($query);

		$id = mysqli_insert_id($mysqli);
		$query = sprintf('select posts.added from posts'
				.' where posts.id=%d limit 1;', $id);
		$result = func_php_query($query);
		if (mysqli_num_rows($result) != 1) {
			$backendarray['error'] = 'ERROR_NEW_POST_NOT_SAVED';
			$backendarray['query'] = $query;
			return $backendarray;
		}

		$r = mysqli_fetch_assoc($result);
		$added = $r['added'];
		$authorusername = $index_activeusername;
		$originalaccountid = $index_activeuserid;

		/* Add membertree details if a parentid has been given. */
		if ($parentid > 0) {
			$membertreequery = sprintf('/* '.__LINE__.' */ insert into membertree '
					.'(postid, parentid, level) select %d, %d, 1'
					.' UNION select %d, newbranch.parentid, newbranch.level+1'
					.' from membertree as newbranch'
					.' where newbranch.postid = %d;',
					$id, $parentid, $id, $parentid);
			$backendarray['membertreequery'] = $membertreequery;
			func_php_query($membertreequery);
		}

		/* Add an approval entry, where pending or pre-approved (if applicable.) */
		if ($preapproved != 1) {
			$preapproved = 'NULL';
		}
		$query = sprintf('insert into approvals (tablename, tableid, accountid, trusted, hidden, added)'
				.' values ("posts", %d, %d, %s, NULL, NOW());', /* POSTID, ACCTID, TRUSTED */
				$id, $index_activeuserid, $preapproved);
		func_php_query($query);
	} else {
		/* ERROR! We shouldn't reach here! */
		$backendarray['error'] = 'ERROR_POST_EDITOR_INVALID';
		return $backendarray;
	}

	/* Fetch and correct permissions and ratings */
	$viewpermissions = func_php_inttext($_POST['newpostviewpermissions']);
	$editpermissions = func_php_inttext($_POST['newposteditpermissions']);
	$replypermissions = func_php_inttext($_POST['newpostreplypermissions']);
	$approvepermissions = func_php_inttext($_POST['newpostapprovepermissions']);
	$moderatepermissions = func_php_inttext($_POST['newpostmoderatepermissions']);
	$tagpermissions = func_php_inttext($_POST['newposttagpermissions']);
	$defaultviewmode = func_php_cleantext($_POST['newpostdefaultviewmode'], 64);
	$defaultsortmode = func_php_cleantext($_POST['newpostdefaultsortmode'], 64);
	$defaultsortreverse = func_php_cleantext($_POST['newpostdefaultsortreverse'], 64);
	$recipients = func_php_cleantext($_POST['newpostrecipients'], 4096);
	$agerating = func_php_inttext($_POST['newpostagerating']);
	if (($viewpermissions < 0) || ($viewpermissions > 5)) $viewpermissions = 0;
	if (($editpermissions < 0) || ($editpermissions > 5)) $editpermissions = 0;
	if (($replypermissions < 0) || ($replypermissions > 5)) $replypermissions = 0;
	if (($approvepermissions < 0) || ($approvepermissions > 5)) $approvepermissions = 0;
	if (($moderatepermissions < 0) || ($moderatepermissions > 5)) $moderatepermissions = 0;
	if (($tagpermissions < 0) || ($tagpermissions > 5)) $tagpermissions = 0;
	if ($agerating < 3)
		$agerating = 3;
	else if ($agerating > 100)
		$agerating = 100;

	/* Adjust any permissions for guest accounts */
	if (!$index_activeuserid) {
		if (($viewpermissions != 5) && ($viewpermissions != 4)
				&& ($viewpermissions != 1)) {
			$viewpermissions = 5;
		}

		if (($editpermissions != 5) && ($editpermissions != 4)
				&& ($editpermissions != 1)) {
			$editpermissions = 5;
		}

		if (($replypermissions != 5) && ($replypermissions != 4)
				&& ($replypermissions != 1)) {
			$replypermissions = 5;
		}

		if (($approvepermissions != 5) && ($approvepermissions != 4)
				&& ($approvepermissions != 1)) {
			$approvepermissions = 5;
		}

		if (($moderatepermissions != 5) && ($moderatepermissions != 4)
				&& ($moderatepermissions != 1)) {
			$moderatepermissions = 5;
		}

		if (($tagpermissions != 5) && ($tagpermissions != 4)
				&& ($tagpermissions != 1)) {
			$tagpermissions = 5;
		}
	}

	/* Handle file attachments and images, if given. */
	$i = 0;
	if ($index_activeuserid && isset($_POST['newpostfileid'])) {
		foreach ($_POST['newpostfileid'] as $newpostfileid) { /* Sanitised immediately below */
			$fileid = func_php_inttext($newpostfileid);
			$filename = func_php_cleantext($_POST['newpostfilename'][$i], 256);
			$filewasuploaded = func_php_inttext($_POST['newpostfilewasuploaded'][$i]);
			$deletefile = func_php_inttext($_POST['newpostdeletefile'][$i]);
			$query = sprintf('/* '.__LINE__.' */ select * from files'
					.' where id=%d and filename="%s"' /* FILEID, FILENAME */
					.' and accountid=%d limit 1;', /* ACCTID */
					$fileid, func_php_escape($filename), $index_activeuserid);
			$backendarray['selectcountfromfiles'][$i] = $query;
			$result = func_php_query($query);
			if (mysqli_num_rows($result) == 1) {
				$r = mysqli_fetch_assoc($result);
				$fileid = $r['id'];
				$fileindex = $r['fileindex'] ?? false;
				$backendarray['files'][$i]['fileid'] = $fileid;
				$backendarray['files'][$i]['filename'] = $filename;
				$backendarray['files'][$i]['deletefile'] = $deletefile;
				if ($deletefile == 0) {
					$backendarray['filelinkerror'][$i] = 'INFO_UPDATING_FILE_MATCH';
					$query = sprintf('/* '.__LINE__.' */ update files set newpostsessionid=NULL,'
							.' updated=NOW(), postid=%d,' /* POSTID */
							.' viewpermissions=%d, editpermissions=%d,' /* PERMISSION, PERMISSION */
							.' replypermissions=%d, approvepermissions=%d,' /* PERMISSION, PERMISSION */
							.' moderatepermissions=%d, tagpermissions=%d' /* PERMISSION, PERMISSION */
							.' where files.id = %d limit 1', $id, $viewpermissions, $editpermissions, /* FILEID */
							$replypermissions, $approvepermissions, $moderatepermissions, $tagpermissions, $fileid);
					$backendarray['fileupdatequery'][$i] = $query;
					func_php_query($query);
				} else { /* File was marked for deletion. */
					func_php_deleteFile($r);
					$query = sprintf('delete from files where id=%d;', $fileid); /* FILEID */
					func_php_query($query);
					$backendarray['filedeletequery'][$i] = $query;
				}
			} else {
				$backendarray['filelinkerror'][$i] = 'ERROR_FILE_NO_MATCH';
			}
			$i++;
		}
	}

	/* Delete any images no longer in the description */
	$postfilelist = array();

	$description = func_php_cleantext($_POST['newpostdescription'], 4000000);

	/* Post-process formatting */
	$description = str_replace('&nbsp;', ' ', $description);

	/* Extract metadata and image tags from HTML */
	$domdoc = new DOMDocument();
	$domdoc->loadHTML($description);

	/* Get semantic microdata */
	$spantags = $domdoc->getElementsByTagName('span');
	$insertquery = '';
	foreach ($spantags as $span) {
		$type = 'string';
		$val = false;
		$itemprop = false;
		if ($span->hasAttribute('itemprop')) {
			$itemprop = $span->getAttribute('itemprop');
			$textContent = $span->textContent;
			$val = func_php_cleantext($textContent, 255);
			if (preg_match('!\d+\.*\d*!', $textContent, $nums)) {
				$val = $nums[0];
				$type = 'decimal';
			}
		}
		if ($itemprop && $insertquery) {
			$insertquery .= ', ';
		}
		if ($itemprop && ($type == 'decimal')) {
			if ($itemprop == 'price')
				$val *= 100;

			$insertquery .= sprintf('(%d, %d, "%s", NULL, %d)',
					$id, $index_activeuserid,
					$itemprop, $val);
		} else {
			$insertquery .= sprintf('(%d, %d, "%s", "%s", NULL)',
					$id, $index_activeuserid,
					func_php_escape($itemprop), $val);
		}
	}

	/* Remove existing microdata, then insert any newly fetched data. */
	$query = sprintf('delete from microdata where postid = %d', $id);
	func_php_query($query);

	if ($insertquery) {
		$query = 'insert into microdata'
				.' (postid, accountid, itemprop, strval, intval) values '
				.$insertquery;
		func_php_query($query);
	}

	/* Build a list of images that are in the description... */
	$imgtags = $domdoc->getElementsByTagName('img');
	$postimage = '';
	$home = str_replace([':', '/'], ['\:', '\/'], $urlhome);
	foreach ($imgtags as $tag) {
		$src = $tag->getAttribute('src');
		if (preg_match('/^'.$home.'\/?index\.php\?fileid=([0-9]*)\&filename=(.*)$/', $src, $matches)
			|| preg_match('/^\/?index\.php\?fileid=([0-9]*)\&filename=(.*)$/', $src, $matches)) {
			$postfilelist[] = $matches[1];
		}

		/* First found image is used for thumbnail/OpenGraph image, if any. */
		if (!$postimage) {
			$postimage = $src;
		}
	}

	/* ...and compare with currently saved files, removing any that aren't used anymore. */
	$query = sprintf('select * from files where postid=%d'
			.' and filemime in ("image/gif", "image/jpeg", "image/png");',
			$id);
	$result = func_php_query($query);
	while ($r = mysqli_fetch_assoc($result)) {
		$postfile = $r['id'];
		if (!in_array($postfile, $postfilelist)) {
			func_php_deleteFile($r);
			$query = sprintf('delete from files where id=%d and filename="%s" limit 1;',
					$r['id'], func_php_escape($r['filename']));
			func_php_query($query);
		}
	}

	/* Mark remaining new files as published by setting "updated" column. */
	$query = sprintf('update files set newpostsessionid=NULL, files.updated=NOW()'
			.' where files.postid=%d;', $id); /* POSTID */
	func_php_query($query);

	/* Get used and free disk space */
	$index_activeuserfileuse = index_php_getuserfileuse(FALSE, FALSE);
	$filespace = $index_activeuserfilespace - $index_activeuserfileuse;
	$backendarray['savepostfilespace'] = $filespace;
	$backendarray['savepostfilespacetext'] = index_php_fuzzyfilesize($filespace).' available';

	/* Save first image URI. */
	$query = sprintf('update posts set image="%s"' /* IMAGE */
			.' where posts.id=%d limit 1;', /* POSTID */
			func_php_escape($postimage), $id);
	func_php_query($query);

	/* $phylum is used to distinguish posts from messages.
	 * $posttype identifies how and which posts can be pinned.
	 * $pagetype identifies profile and page posts for pinning */
	$phylum = 'post';
	$posttype = '';
	$pagetype = '';
	if (($command == 'new_message') || ($command == 'edit_message')) {
		$phylum = 'message';
		$posttype = 'message';
	} else if ($command == 'new_post') {
		$posttype = 'post';
		$pagetype = func_php_cleantext($_POST['newpostpagetype'], 64);
	} else if ($command == 'new_postreply') {
		$posttype = 'reply';
	}

	$query = sprintf('select posts.added, accounts.username, accounts.fullname'
			.' from posts inner join accounts on posts.accountid = accounts.id'
			.' where posts.id=%d limit 1;', $id);
	$result = func_php_query($query);
	$r = mysqli_fetch_assoc($result);
	$username = $r['username'];
	$backendarray['username'] = $username;
	$backendarray['fullname'] = $r['fullname'];

	/* Some of these might need to be moved down this function to get updated data. */
	$backendarray['newpostid'] = $id;
	$backendarray['parentid'] = $parentid;
	$backendarray['added'] = $added;

	if (($defaultviewmode != 'full') && ($defaultviewmode != 'thumbnail')
			&& ($defaultviewmode != 'list')) {
		$defaultviewmode = 'full';
	}

	if (($defaultsortmode != 'date') && ($defaultsortmode != 'title')) {
		$defaultsortmode = 'date';
	}

	if ($defaultsortreverse == 'true') {
		$defaultsortreverse = true;
	} else {
		$defaultsortreverse = false;
	}

	/* Fetch title (as well as other text as short description) if given */
	$title = '';
	$shortdesc = '';
	if (preg_match('/^#\s(.+?)(<br>)(.*)?$/', $description, $matches)) {
		$title = func_php_cleantext(func_php_stringtosearchcache($matches[1]), 255);
		if (count($matches) >= 4) {
			$shortdesc = func_php_cleantext(func_php_stringtosearchcache($matches[3]), 1024);
		}
	} else if (preg_match('/^#\s(.+?)(<br>)?/', $description, $matches)) {
		$title = func_php_cleantext(func_php_stringtosearchcache($matches[1]), 255);
	} else {
		$shortdesc = func_php_cleantext(func_php_stringtosearchcache($description), 1024);
	}

	$query = sprintf('/* '.__LINE__.' */ update posts set description="%s",'
			.' viewpermissions=%d, editpermissions=%d, replypermissions=%d,'
			.' approvepermissions=%d, moderatepermissions=%d,'
			.' tagpermissions=%d, viewmode="%s", agerating=%d,'
			.' sortmode="%s", sortreverse=%d,'
			.' title="%s", shortdesc="%s", updated=NOW()'
			.' where posts.id = %d limit 1', func_php_escape($description),
			$viewpermissions, $editpermissions, $replypermissions,
			$approvepermissions, $moderatepermissions, $tagpermissions,
			func_php_escape($defaultviewmode), $agerating,
			func_php_escape($defaultsortmode), $defaultsortreverse,
			func_php_escape($title), func_php_escape($shortdesc), $id);
	$result = func_php_query($query);
	$backendarray['postprocess'] = 'indexpostforsearch';
	$backendarray['postpopuplist'] = index_php_insertPostPopupList($phylum, $id,
			$showparent, $posttype, $pagetype, $viewmode);
	$backendarray['memberlink'] = index_php_insertMemberLink($username, true, false, 0, 0, $phylum, $id);

	$phylum = '';
	$editordescription = '';
	if (($command == 'new_post') || ($command == 'new_postreply')) {
		$phylum = 'post';
		$backendarray['error'] = 'INFO_NEW_POST_SAVED';
	} else if ($command == 'edit_post') {
		$phylum = 'post';
		$backendarray['error'] = 'INFO_EDIT_POST_SAVED';
		$markdown = func_php_markdowntohtml($description);
		$editordescription = $markdown;
		$searchcache = func_php_stringtosearchcache($markdown);
		$backendarray['title'] = substr($searchcache, 0, 140) .' - '.$servertitle;
	} else if ($command == 'new_message') {
		/* Save settings that get sent with the popup editor. */
		$messagesendonenterkey = 0;
		$messagecloseonsend = 0;

		if (func_php_cleantext($_POST['messagesendonenterkey'], 255) == 'true')
			$messagesendonenterkey = 1;

		if (func_php_cleantext($_POST['messagecloseonsend'], 255) == 'true')
			$messagecloseonsend = 1;

		$query = sprintf('update accounts set messagesendonenterkey=%d,'
				.' messagecloseonsend=%d where id=%d limit 1',
				$messagesendonenterkey, $messagecloseonsend, $index_activeuserid);
		$result = func_php_query($query);

		$phylum = 'message';
		$backendarray['error'] = 'INFO_NEW_MESSAGE_SAVED';
	} else if ($command == 'edit_message') {
		$phylum = 'message';
		$backendarray['error'] = 'INFO_EDIT_MESSAGE_SAVED';
	} else {
		$backendarray['error'] = 'ERROR_INCOMPLETE_POST_FUNC';
	}
	$permissions = $viewpermissions.$editpermissions.$replypermissions.$approvepermissions.$moderatepermissions.$tagpermissions;
	$backendarray['phylum'] = $phylum;
	$backendarray['posttype'] = $posttype;
	$backendarray['permissions'] = $permissions;
	$backendarray['defaultviewmode'] = $defaultviewmode;
	$backendarray['defaultsortmode'] = $defaultsortmode;
	$backendarray['defaultsortreverse'] = ($defaultsortreverse ? 1 : 0);
	$backendarray['agerating'] = $agerating;
	$backendarray['command'] = $command;
	$backendarray['inlinereply'] = $inlinereply;
	$backendarray['viewmode'] = $viewmode;

	/* Generate description data to display in postdescription div here. */
	$postdescription = '';
	if ($viewmode == 'thumbnail') {
		if (!$postimage) {
			$postimage = '/img/default-thumbnail.jpg';
		}
		$postdescription = "<div class=\"squareimagediv\"><a href=\"/{$id}\"><img src=\"{$postimage}\"></a></div>";

		$postdescription .= '<div class="postdescriptionpadding">';
		if ($title) {
			$postdescription .= "<h1><a href=\"/{$id}\">{$title}</a></h1>";
		}

		if ($microdataarray = index_php_microdataArray($id)) {
			foreach ($microdataarray as $microdata) {
				$postdescription .= "<div>{$microdata['str']}</div>";
			}
		} else {
			$postdescription .= trim(substr($shortdesc, 0, 40));
			if (strlen($shortdesc) > 40)
				$postdescription .= '...';
		}
		$postdescription .= '</div>';
	} else if ($viewmode == 'list') {
		$postdescription = "&nbsp;&#183;&nbsp;<a href=\"/{$postid}\">";
		if ($title) {
			$postdescription .= $title;
		}

		if ($title && $shortdesc) {
			$postdescription .= ' &#183; ';
		}

		$subshortdesc = trim(substr($shortdesc, 0, 80));
		if ($subshortdesc) {
			$postdescription .= "<span style=\"color:#888;\">{$subshortdesc}</span>";
		}

		if (!$title && !$subshortdesc) {
			$postdescription .= '(No subject)';
		}
		$postdescription .= '</a>';
	} else {
		/* Show full post */
		$postdescription = func_php_markdowntohtml($description);
	}
	$backendarray['description'] = $postdescription;

	/* Separate local and remote users, verifying local ones */
	$emailrecipientarray = array();
	$localrecipientarray = array();
	$recipientarray = array();
	$emailrecipients = ''; /* Comma-separated string used later for sending emails */
	if ($recipients && ($command !== 'edit_message')) {
		$recipients = func_php_escape(preg_replace('/\s+/', ' ', $recipients));
		$recipients = strtolower($recipients);
		$recipientarray = explode(' ', $recipients);
		$recipientarray = array_unique($recipientarray);
		foreach ($recipientarray as $key => $recipient) {
			$username = false;
			$server = false;
			/* Separate local and remote email addresses */
			if (preg_match('/^([^@]+)@(.+)$/', $recipient, $matches)) {
				$username = $matches[1];
				$server = $matches[2];
				if ($server == $servername) {
					/* Local recipient */
					$query = sprintf('select count(*) from accounts'
							.' where username="%s" limit 1', /* USERNAME */
							func_php_escape($username));
					$result = func_php_query($query);
					$r = mysqli_fetch_assoc($result);
					if ($r['count(*)'] == 1) {
						$recipient = $username;
						$server = false;
					}
				}
				if ($server) {
					/* Add email recipients if not already exist. The insert query checks
					 * if a row already exists and doesn't insert if one is found. */
					$query = sprintf('insert into recipients'
							.' (tablename, tableid, senderaccountid, recipientaccountid,'
							.' emailusername, emailserver, added)'
							.' select "posts", %d, %d, NULL,' /* POSTID, ACCTID */
							.' "%s", "%s", NOW() from recipients' /* EMAILUSER, EMAILSERVER */
							.' where recipients.tablename = "posts" and recipients.tableid = %d' /* POSTID */
							.' and recipients.senderaccountid = %d' /* ACCTID */
							.' and emailusername = "%s" and emailserver = "%s"' /* EMAILUSER, EMAILSERVER */
							.' having count(*) = 0 limit 1;',
							$id, $originalaccountid, func_php_escape($username), func_php_escape($server),
							$id, $originalaccountid, func_php_escape($username), func_php_escape($server));
					func_php_query($query);

					if ($emailrecipients)
						$emailrecipients .= ', ';
					$emailrecipients .= $recipient;
					$emailrecipientarray[] = $recipient;
				}
			}

			/* Check for valid local recipient */
			if ($recipient && !$server) {
				$recipientid = 0;
				$query = sprintf('select id, username from accounts where username = "%s" limit 1;',
						func_php_escape($recipient));
				$result = func_php_query($query);
				if (mysqli_num_rows($result)) {
					$r = mysqli_fetch_assoc($result);
					$recipientid = $r['id'];
					$recipientusername = $r['username'];
					if ($recipientid == $originalaccountid) {
						$recipientid = 0;
					}
				}

				if ($recipientid) {
					/* Add local recipients only if it doesn't already exist for that post.
					 * The insert query checks for an already exsting row for the specified post
					 * and doesn't insert if one is found. */
					$query = sprintf('insert into recipients'
							.' (tablename, tableid, senderaccountid, recipientaccountid, added)'
							.' select "posts", %d, %d, %d, NOW() from recipients' /* POSTID, ACCTID, RECPID */
							.' where recipients.tablename = "posts" and recipients.tableid = %d' /* POSTID */
							.' and recipients.senderaccountid = %d' /* ACCTID */
							.' and recipients.recipientaccountid = %d' /* RECPID */
							.' having count(*) = 0 limit 1;',
							$id, $originalaccountid, $recipientid,
							$id, $originalaccountid, $recipientid);
					func_php_query($query);
					$localrecipientarray[] = $recipientusername;
				} else {
					unset($recipientarray[$key]);
				}
			}
		}
		$recipients = implode(' ', $recipientarray);
	}

	if ($command !== 'edit_message') {
		$query = sprintf('/* '.__LINE__.' */ select recipients.*, accounts.username'
				.' from recipients left join accounts on recipients.recipientaccountid = accounts.id'
				.' where recipients.tablename = "posts" and recipients.tableid = %d', /* POSTID */
				$id);
		$result = func_php_query($query);

		/* Remove any already existing recipients that aren't specified by the user in this edit.
		 * Already existing recipients that aren't removed between edits won't be removed from the post but will be
		 * removed from the new recipient array to be added. */
		$idremovearray = array();
		while ($r = mysqli_fetch_assoc($result)) {
			$username = $r['username'];
			$emailusername = $r['emailusername'];
			$emailserver = $r['emailserver'];
			$email = "{$emailusername}@{$emailserver}";

			if (!in_array($username, $localrecipientarray) && !in_array($email, $emailrecipientarray)) {
				/* Remove already existing recipients that aren't specified in this edit */
				$idremovearray[] = $r['id'];
			} else {
				/* Clear preserved existing recipients, so they aren't added as recipients again */
				$key = array_search($username, $localrecipientarray);
				if ($key) {
					unset($localrecipientarray[$key]);
				}

				$key = array_search($email, $emailrecipientarray);
				if ($key) {
					unset($emailrecipientarray[$key]);
				}
			}
		}

		if (count($idremovearray)) {
			$idlist = implode(' ,', $idremovearray);
			$query = sprintf('delete from recipients where recipients.tablename = "posts"'
					.' and recipients.tableid = %d and recipients.senderaccountid = %d' /* POSTID, ACCTID */
					.' and recipients.id in (%s);', /* IDLIST */
					$id, $originalaccountid, $idlist);
			$result = func_php_query($query);
		}

		/* Get current recipients after (conditionally) removing/adding recipients. */
		$recipients = '';
		if (count($localrecipientarray) || count($emailrecipientarray)) {
			$query = sprintf('/* '.__LINE__.' */ select (select group_concat(distinct concat_ws("",'
					.' 	accounts.username, recipients.emailusername,'
					.'	if (recipients.emailserver is not NULL, "@", ""), recipients.emailserver)'
					.' separator " ")'
					.' from recipients left join accounts on accounts.id = recipients.recipientaccountid'
					.' where recipients.tablename = "posts" and recipients.tableid = %d)'
					.' as recipients;', $id); /* POSTID */
			$result = func_php_query($query);
			$r = mysqli_fetch_assoc($result);
			$recipients = $r['recipients'];
		}
	}

	$moredetail = true;
	if ($viewmode == 'list')
		$moredetail = false;

	$backendarray['recipients'] = $recipients;
	$backendarray['postcredits'] = index_php_insertPostCredits($phylum, $id, $showparent, $viewmode, $moredetail);
	$backendarray['postmembers'] = index_php_insertPostMembers($phylum, $id);
	$backendarray['postrecipients'] = index_php_insertPostRecipients($phylum, $id);

	$backendarray['editlinktext'] = func_php_cleantext($_POST['editlinktext'], 255);

	/* Send emails where addresses given */
	if ($emailrecipients) {
		$emailfrom = "$index_activefullname <{$index_activeusername}@{$servername}>";
		$subject = '';
		if (($command == 'edit_post') || ($command == 'edit_message')) {
			$subject = '[EDITED] ';
		}
		index_php_mail($id, $emailfrom, $emailrecipients, $subject);
	}

	return $backendarray;
}


/**
 *
 */

$backend_array = array();
$command = (isset($_POST['command']) ? func_php_cleantext($_POST['command'], 64) : '');
switch ($command) {
case 'new_message':
case 'edit_message':
	$backend_array = backend_php_savePost($command);
	break;

case 'cancel-subscription':
	$backend_array = backend_php_cancel_subscription();
	break;

default:
	//backend_php_sendWebsockMessage();
	$backend_array['error'] = "No function";
	$backend_array['command'] = $command;
}

echo json_encode($backend_array, JSON_PRETTY_PRINT);
?>
