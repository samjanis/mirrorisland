<?php
/**
 * bgfunc.php - A collection of background functions.
 * Most of these are run as a post-process, usually for functions that would
 * cause a script to halt for a long time (eg. to index keywords in a post)
 */

require('func.php');

/**
 * Add (or update) a post's search index entries.
 * $id - Post ID to be indexed.
 * $removekeys - Remove unused keywords.
 * TODO:OPTIMISE - This has duplicate code shared wth other index_php_index* functions.
 */
function bgfunc_php_indexpostforsearch($id, $removekeywords)
{
	global $mysqli;

	$query = sprintf('select description from posts where posts.id = %d' /* POSTID */
			.' and (posts.indexed != posts.updated or posts.indexed is NULL) limit 1;',
			$id);
	$result = func_php_query($query);
	if (mysqli_num_rows($result) == 0) {
		return $query; /* No valid post, leave this function... */
	}

	$query = sprintf('update posts set posts.indexed = posts.updated where posts.id=%d limit 1;', $id);
	func_php_query($query);

	$r = mysqli_fetch_assoc($result);
	$description = $r['description'];
	$keywordarray = false;
	$keywordlist = '';
	$searchcache = '';

	if ($description) {
		/* Generate a stripped-down copy of description for searchcache. */
		$searchcache = func_php_stringtosearchcache(func_php_markdowntohtml($description));

		/* Tokenize and remove some stop words. */
		$keywordarray = func_php_stringtokeywordarray($searchcache, true);

		/* Generate a comma-separated single-quoted string of keywords for use in sql below. */
		foreach ($keywordarray as $keyword) {
			if ($keywordlist)
				$keywordlist .= ', ';
			$keywordlist .= '"'.func_php_escape($keyword).'"';
		}

	}

	$query = sprintf('update posts set searchcache="%s" where id=%d limit 1;',
			func_php_escape($searchcache), $id);
	func_php_query($query);

	if ($removekeywords) {
		/* Remove any keywords not use by any other searchindex entry. */
		$query = sprintf('delete from searchkeywords where id in (select keywordid from'
				.' (select searchindex.*, searchkeywords.keyword, count(*) as keywordcount'
				.' from searchindex'
				.' inner join searchkeywords on searchindex.keywordid = searchkeywords.id'
				.' left join searchindex as si on si.keywordid = searchindex.keywordid'
				.' where searchindex.tablename="posts" and searchindex.tableid=%d' /* POSTID */
				.' group by keywordid) as searchkeywordstodelete'
				.' where keywordcount <= 1', $id);

		if ($keywordlist) {
			$query .= sprintf(' and keyword not in (%s)', $keywordlist);
		}

		$query .= ');';
		func_php_query($query);

		/* Remove searchindex entries not in keyword list. */
		$query = '/* '.__LINE__.' */ '.sprintf('delete from searchindex where id in'
				.'(select id from (select searchindex.id from searchindex'
				.' inner join searchkeywords on searchindex.keywordid = searchkeywords.id'
				.' where searchindex.tablename="posts" and tableid=%d', $id);

		if ($keywordlist) {
			$query .= sprintf(' and keyword not in (%s)', $keywordlist);
		}

		$query .= ') as searchindexentriestodelete);';
		func_php_query($query);
	}

	if (!$keywordarray)
		return; /* Nothing new to insert, leave now. */

	/* Only add keywords that don't exist yet. TODO:OPTIMISE */
	foreach ($keywordarray as $keyword) {
		$query = sprintf('select * from searchkeywords where keyword="%s" limit 1;',
				func_php_escape($keyword));
		$result = func_php_query($query);

		/* Fetch an existing keyword id or save a new one. */
		$keywordid = 0;
		if (mysqli_num_rows($result) == 1) {
			$r = mysqli_fetch_assoc($result);
			$keywordid = $r['id'];
		} else {
			$query = sprintf('insert into searchkeywords (keyword) values ("%s");', /* KEYWORD */
					func_php_escape($keyword));
			$result = func_php_query($query);
			$keywordid = mysqli_insert_id($mysqli);
		}

		/* See if an existing searchindex entry exists for posts. */
		$query = sprintf('insert into searchindex (tablename, tableid, keywordid)'
				.' select * from (select "posts", %d as tid, %d as kwid) as newentry' /* POSTID, KEYWORDID */
				.' where not exists (select tablename, tableid, keywordid from searchindex'
				.' where tablename="posts" and tableid=%d and keywordid=%d limit 1)' /* POSTID, KEYWORDID */
				.' limit 1;', $id, $keywordid, $id, $keywordid);
		func_php_query($query);
	}

	return;
}

/* Only continue if passcode given */
$pass = func_php_cleantext($_POST['p'] ?? '', 256);
if ($pass !== $bgfuncpass) {
	exit();
}

$func = func_php_cleantext($_POST['f'] ?? '', 64);
switch ($func) {
case 'indexpostforsearch':
	$postid = func_php_inttext($_POST['id'] ?? 0);
	$deleteswitch = strtolower(func_php_cleantext($_POST['deletefile'] ?? 'false', 64));
	if ($deleteswitch !== 'true') {
		$deleteswitch = 'false';
	}
	if ($postid) {
		bgfunc_php_indexpostforsearch($postid, $deleteswitch);
	}
}
?>
