<!DOCTYPE html>
<html>
<head>
<title>Mirror Island Quickstart</title>
<style type="text/css">
body { font:normal 16px 'sans-serif'; font-weight:semi-bold; color:#567; background:#f0f0f3; padding:0px; margin:0px; line-height:24px; }
h1, h2, h3 { font-weight:bold; font-family:'sans-serif'; color:#345; }
h1 { font-size:18px; text-align:center; text-transform:uppercase; }
h2 { font-size:18px; text-align:left; font-weight:bold; border-bottom:solid 1px #5a9; letter-spacing:2px; color:#5a9; padding:0 0 4px 0; }
h3 { font-size:16px; text-align:left; letter-spacing:1px; color:#5a9; margin-top:20px; }
p { margin:16px 0 0 0; }
i, em { font-family:'serif'; }
ul { padding-left:20px; }
li { margin:10px 0; }
.maindiv { background:#fafafe; max-width:600px; padding:15px 25px; margin:10px auto 10px auto; border:solid 1px #dde; border-radius:8px; }
.centerimg { background:#fff; border:solid 1px #cde; border-width:0 1px 0 1px; display:block; padding:20px; margin:20px auto 20px auto; max-width:100%;}
#footer { color:#79a; padding:10px 20px; text-align:center; margin:30px 0 0 0; border-top:solid 1px #cde; } 
#footer div { margin:10px; }
#footer a { background:none; color:#57f; text-decoration:none; margin:auto 4px; }
#footer a:hover { text-decoration:underline; } 
</style>
</head>

<body>

<div class="maindiv">

<center>
<h1>Mirror Island Quick Start Guide</h1>
<a href="/"><img src="/milogo.png"></a>
<p>June 27, 2018</p>
</center> 

<p>Mirror Island is a new kind of network designed to help you get your ideas and messages out to the world as quick as possible.</p>
<p>The best way to see how things work is by making your first post.</p>
<p>It doesn&rsquo;t really matter what you put in it for now since you can edit any post you have made, or you can delete them altogether - so have fun!</p>

<h2>Sign Up And Sign In (Optional)</h2>
<p>It is recommended to sign up for an account and sign in before continuing.</p>
<p>As a registered user it will be easier to know which posts you have made and it enables additional features mentioned later in this quick start guide.</p>
<p>The sign up form is located on the right side of every page and looks like this:</p>
<img class="centerimg" src="/img/quickstart-00-signup.png">
<p>It literally takes a few seconds, and signing in is fast too!</p>
<p style="text-align:center;"><a style="text-decoration:none; font:bold 18px 'sans-serif'; color:#36f; margin-top:10px; " href="/#rightpanel" target="_blank">Click here to go to the sign up page...</a></p>
<p>Once you have signed in you can return here and continue on to the next section.</p>
<h3>Already have an account?</h3>
<img class="centerimg" src="/img/quickstart-00-signin.png">
<p>You can sign in at any time at the top-right of any page.</p>

<h2>Start With A Post</h2>
<p>Near the top of the page click on <strong>New Post</strong> and the button will transform in to a new, blank document.</p>
<img class="centerimg" src="/img/quickstart-01-newpost.png">
<p>You can now start to write your first post!</p>

<p>The area highlighted in red is the <em>editor window</em> where you can type and add images. Surrounding the editor window are the <em>editor controls</em>.<p>
<img class="centerimg" src="/img/quickstart-02-editpost.png">
<ul>
<li><strong>Undo</strong> and <strong>Redo</strong>: Undo (or redo) changes before saving a post.</li>
<li><strong>Paste Link</strong>: Turn the selected text in to a link, or if no selection has been made a copy of the address itself will be inserted and linked.</li>
<li><strong>Attach File or Image</strong>: Images will be displayed in the editor window. Other files will be listed below the editor window as attachments.<br>Currently, only files up to 4MB in size are accepted.</li>
<li><strong>Save Post</strong>: Saves the current post and closes the editor.</li>
<li><strong>Cancel</strong>: Undoes all changes and closes the editor.</li>
<li><strong>Permissions dialogue</strong>: Lets you control who can see the post.</li>
</ul>

<h3>A Quick Note On Permissions</h3>
<p>You can specify permissions when you hover the mouse pointer over the icons on the bottom left.</p>
<img class="centerimg" src="/img/quickstart-03-permissions.png">
<ul>
<li><strong>Visibility</strong>: Who can see the post. This includes;
	<ul>
	<li><strong>Anyone</strong>: Everybody can see.</li>
	<li><strong>Allow anonymous &amp; email posts</strong>: With <em>Anyone</em> selected, users don&rsquo;t have to be signed in to reply, or they can reply to this post by email.</li>
	<li><strong>Trusted contacts</strong>: People you mark &quot;<em>Trusted</em>&quot; on your contact list.</li>
	<li><strong>Recipients</strong>: People entered in the recipient field can see this.</li>
	<li><strong>Only the owner</strong>: Only the owner can see this post. (This can be useful to save drafts for publishing later.)</li>
	</ul>
</li>
<li><strong>Replies</strong>: Permissions also apply to replies. They include;
	<ul>
	<li><strong>Accept All</strong>: All replies are displayed.</li>
	<li><strong>Need Approval</strong>: Replies from other users are saved but they can only be visible when you approve them.</li>
	<li><strong>Disabled</strong>: No replies are accepted.</li>
	</ul>
</li>
</ul>

<p>Checking <strong>Advanced permissions</strong> will allow you to fine tune permissions (although that is an in-depth topic for another day.)<p>
<img class="centerimg" src="/img/quickstart-04-permissions-advanced.png">
<p>If you ever find yourself in advanced mode, click on <strong>Advanced&nbsp;permissions</strong> at the top to uncheck it and return to the quicker to use presets.</p>
<p>A prompt may show up letting you know presets will reset permissions - simply click <em>OK</em> to return to presets and continue editing.<p>

<h3>Saving Your Post</h3>
<p>Once you have finished typing in your post or uploading images or attachments, click on <em>Post</em> to save your post.</p>

<h2>Details On Each Post</h2>
<p>On each post only basic details are shown for faster browsing. An example post is shown below:</p>
<img class="centerimg" src="/img/quickstart-05-post-overview.png">
<p>By hovering the mouse over these details you can find out more information about the post including who and when it was posted, and its permissions.</p> 
<ul>
<li><strong>Permissions Icon</strong>: An icon displaying who can see this post. The icon used here is the same one selected in the permissions box during editing.</li>
<li><strong>Post Link</strong>: A link to the post&rsquo;s page showing a shortened form of when the post was made. In the above example <em>3d&nbsp;20h</em> means <em>3 days and 20 hours ago</em>.</li>
<li><strong>Member Link</strong>: A multipurpose link that can display information about the author of the post and how to contact them.</li>
<li><strong>Post Menu</strong>: A popup menu with the options to share, edit, or delete the post. Only posts you can edit will have a &quot;red pen&quot; icon displayed.</li>
<li><strong>Attachments</strong>: Links to other files attached to this post. Click the attachment&rsquo;s link to download (or use the right-click menu if needed.)</li>
<li><strong>Reply Button</strong>: Starts a new post in reply to the above post. This is only shown if the post accepts replies. If a reply neeeds approval, a small clock icon will be displayed in the Reply button.<br><img src="/img/quickstart-05a-reply-pending.png"></li>
</ul>

<h3>The Permissions Icon Popup</h3>
<img class="centerimg" src="/img/quickstart-06-tooltip-permissions.png">
<p>A description of a post&rsquo;s permissions can be found in the popup for the permission icon.</p>

<h3>The Post Link Popup</h3>
<img class="centerimg" src="/img/quickstart-07-tooltip-date.png">
<p>After the permission icon is the post link which shows in detail when the post was made. Also, it is the link to the post&rsquo;s own page.</p> 

<h3>The Member Link Popup</h3>
<img class="centerimg" src="/img/quickstart-08-tooltip-memberlink.png">
<p>After the post link is the name of the author displayed as a member link. The popup for this shows a few options on how to contact them.</p>
<p>At the bottom of the popup is the member&rsquo;s handle and looks like an email address; this is what goes in the <em>Recipients</em> field in the permissions popup.</p>
<p>The other options in the member link popup include:</p>
<ul>
<li><strong>View Profile</strong>: A link to the profile page of the member.</li>
<li><strong>Message</strong>: Opens a popup window to send a quick post (message) to the member.</li>
<li><strong>Add Contact</strong>: If you have signed in you can add this person to an address book on the side of the window.</li>
<li><strong>Trust</strong>: Add this member as a trusted contact to your address book. You will need to be signed in to also see this feature.</li>
</ul>

<h3>The Post Menu</h3>
<img class="centerimg" src="/img/quickstart-09-postmenu.png">
<p>The menu for each post can be accessed through the menu icon in the top right of the post. With all posts you can see, you are able to:</p>
<ul>
<li><strong>Flag Post</strong>: If the post contains inapproprate or offensive material, the post can be removed from view.</li>
<li><strong>Share...</strong>: Send a copy of the post by email or through a social network.</li>
</ul>
<p>In addition to the above menu item, posts you can edit will have a &quot;red pen&quot; in its post menu icon. That way, you can quickly tell the difference between posts you can edit (<em>Editable&nbsp;menu</em>) and those you can&rsquo;t (<em>Normal&nbsp;menu</em>).</p>
<p>The additional options available are:</p>
<ul>
<li><strong>Edit</strong>: The post will become editable with the edit controls reappearing around the post.</li>
<li><strong>Delete</strong>: Delete the post and any photos or attachments saved with it. Deleting is immediate and permanent, so choose wisely when doing this!</li>
</ul>

<h3>Wrapping Up This Quickstart Guide</h3>
<p>This concludes the quick start guide for now.</p>
<p>Now that you know enough to start writing posts, sharing photos and uploading attachments, you can also explore the other features Mirror Island has to offer! These include:</p>
<ul>
<li>Using the search bar located at the top</li>
<li>Making image galleries by uploading multiple images in the one post</li>
<li>Using groups</li>
<li>Managing your contact list</li>
<li>Sending and recieving emails</li>
</ul>
<p>And other upcoming features that are in development!</p>

<div id="footer">
	<div>
		<a href="/"><strong>Mirror&nbsp;Island</strong></a>
		<a href="/about.php">About</a>
		<a href="/contact.php">Contact Us</a>
		<a href="/terms.php">Terms of Use</a>
		<a href="/manifesto.php">Manifesto</a>
		<a href="/quickstart.php">Quickstart Guide</a>
		<a href="/features.php">Feature Tour</a>
		<a href="/advertise.php">Advertise</a>
		<a href="/reportabug.php">Report a Bug</a>
	</div>
</div>

</div>

</body>
</html>
