#!/usr/bin/php -q
<?php
require('func.php');

/* phpglobal: Saved variables */
$toarray = '';
$fromarray = '';
$subject = '';
$body = '';
$bodycontenttype = '';
$boundary = [];
$boundarydepth = 0;
$readingheaders = true;
$multipartemail = false;
$readingdata = false; /* saving data (possibly multipart data) */
$partheaders = [];
$partdata = '';
$attachment = []; /* Array of attachments to copy to recipients */
$attachmentcount = 0;

/**
 * Handle "Content-type:" headers.
 * Returns processed data in an array on success, false on error.
 */
function emailhandler_php_handleContentTypeHeader($value)
{
	$retval = [];
	if (preg_match('/^([^;\s]+)\s*;?\s*(.*)?/', $value, $matches)) {
		$contenttype = $matches[1];
		$contenttypedata = $matches[2];
		if (($contenttype === 'multipart/mixed') || ($contenttype === 'multipart/alternative')) {
			if (preg_match('/^boundary="([^"]*)/', $contenttypedata, $matches)) {
				$retval['content-type'] = $contenttype;
				$retval['boundarystart'] = '--'.$matches[1];
				$retval['boundaryend'] = '--'.$matches[1].'--';

				return $retval;
			}
		} else if ($contenttype) {
			$retval['content-type'] = $contenttype;

			return $retval;
		}
	}

	return false;
}

/**
 * Process the "From:" header entry and return an array of data.
 */
function emailhandler_php_handleFromHeader($from)
{
	$displayname = '';
	$emailusername = '';
	$emailserver = '';
	$emailinbox = '';

	$from = func_php_cleantext($from, 1024);
	if (preg_match('/^"?([^<"]*)"?\s*?<([^@]*)@([^>]*)>$/', $from, $matches)) {
		/* Handle emails WITH display name. */
		$displayname = $matches[1];
		$emailusername = strtolower($matches[2]);
		$emailserver = strtolower($matches[3]);
	} else if (preg_match('/^([^<@"]*)@([^>]*)$/', $from, $matches)) {
		/* Handle emails without display names. */
		$emailusername = strtolower($matches[1]);
		$emailserver = strtolower($matches[2]);
	} else {
		return false;
	}

	/* Generate a display name if one not given */
	if (!$displayname) {
		$displayname = "{$emailusername}@{$emailserver}";
	}

	$fromarray['displayname'] = $displayname;
	$fromarray['emailusername'] = $emailusername;
	$fromarray['emailserver'] = $emailserver;

	return $fromarray;
}

/**
 * Process the "To:" header entry and return an array of recipients.
 * This returns an array of verified recipients.
 */
function emailhandler_php_handleToHeader($to)
{
	global $servername;

	$tolist = explode(',', $to);
	if (!$tolist || !count($tolist)) {
		return FALSE;
	}

	$toarray = [];
	$i = 0;
	foreach ($tolist as $to) {
		$displayname = '';
		$emailusername = '';
		$emailserver = '';
		$emailinbox = '';

		$to = func_php_cleantext($to, 1024);
		if (preg_match('/^"?([^<"]*)"?\s*?<([^@]*)@([^>]*)>$/', $to, $matches)) {
			/* Handle emails WITH display name. */
			$displayname = $matches[1];
			$emailusername = strtolower($matches[2]);
			$emailserver = strtolower($matches[3]);
		} else if (preg_match('/^([^<@"]*)@([^>]*)$/', $to, $matches)) {
			/* Handle emails without display names. */
			$emailusername = strtolower($matches[1]);
			$emailserver = strtolower($matches[2]);
		} else {
			continue;
		}

		/* Check if an inbox name has been given. */
		if (preg_match("/^([^\.]*)\.{1}([^\.]*)$/", $emailusername, $matches)) {
			$emailusername = $matches[1];
			$emailinbox = $matches[2];
		}

		/* Validate the email recipient here before adding it to the array.
		 * $servername is set in connectdbinfo.php */

		if ($emailserver !== $servername)
			continue; /* Jump to beginning of loop */

		$postid = 0;
		$userid = 0;
		/* Check for pages specified by their numeric IDs. */
		if (preg_match('/^[0-9]+$/', $emailusername)) {
			$id = func_php_inttext($emailusername);
			$emailusername = '';
			$query = sprintf("select id, accountid from posts"
					." where posts.id = %d" // POSTID
					." and replypermissions = 5 limit 1", $id);
			$result = func_php_query($query);

			if (mysqli_num_rows($result) == 1) {
				$r = mysqli_fetch_assoc($result);
				$postid = $r['id'];
				$userid = $r['accountid'];
			}
		} else {
			$query = sprintf("select id from accounts"
					." where username='%s' limit 1;", // USERNAME
					func_php_escape($emailusername));
			$result = func_php_query($query);

			if (mysqli_num_rows($result) == 1) {
				$r = mysqli_fetch_assoc($result);
				$userid = $r['id'];
			}
		}

		if (!$userid && !$postid)
			continue; /* Jump to beginning of loop */

		/* Generate a display name if one not given and not post email */
		if (!$displayname && !$postid) {
			$displayname = "{$emailusername}@{$emailserver}";
		}

		$toarray[$i]['displayname'] = $displayname;
		$toarray[$i]['emailusername'] = $emailusername;
		$toarray[$i]['emailserver'] = $emailserver;
		$toarray[$i]['emailinbox'] = $emailinbox;
		$toarray[$i]['postid'] = $postid;
		$toarray[$i]['userid'] = $userid;
		$i++;
	}

	return $toarray;
}

/**
 * Save attachments.
 * - $filedata is raw data from the email.
 *   Base64 and urlencoded data is expected to be decoded before entering here.
 * Returns path for the temporary file that can be copied to each recipient.
 */
function emailhandler_php_saveAttachment($filedata, $filename, $mimetype)
{
	global $attachment, $attachmentcount;

	$tmpfilename = tempnam(sys_get_temp_dir(), 'emlfile');
	$handle = fopen($tmpfilename, "w");
	$filesize = fwrite($handle, $filedata);
	fclose($handle);

	$attachment[$attachmentcount]['tmpfilename'] = $tmpfilename;
	$attachment[$attachmentcount]['filename'] = $filename;
	$attachment[$attachmentcount]['filesize'] = $filesize;
	$attachment[$attachmentcount]['mimetype'] = $mimetype;
	$attachmentcount++;
}

/**
 *
 */
function emailhandler_php_handlePartData($partheaders, $partdata)
{
	global $boundary, $boundarydepth;
	global $body, $bodycontenttype;

	if (!$partdata)
		return false;

	$boundarycontenttype = '';
	if (isset($boundary[$boundarydepth]['content-type'])) {
		$boundarycontenttype = $boundary[$boundarydepth]['content-type'];
	}

	$filename = false;
	if (isset($partheaders['filename'])) {
		$filename = $partheaders['filename'];
	}

	$contenttype = '';
	if (isset($partheaders['content-type'])) {
		$contenttype = $partheaders['content-type'];
	}

	$contenttransferencoding = '';
	if (isset($partheaders['content-transfer-encoding'])) {
		$contenttransferencoding = $partheaders['content-transfer-encoding'];
	}

	if ($contenttransferencoding === 'base64') {
		$partdata = base64_decode($partdata);
	} else if ($contenttransferencoding === 'quoted-printable') {
		$partdata = quoted_printable_decode($partdata);
	}

	if ((($contenttype === 'text/html') || ($contenttype === 'text/plain'))
			&& (($boundarycontenttype === 'multipart/mixed')
					|| ($boundarycontenttype === 'multipart/alternative')
					|| (!$boundarycontenttype))) {
		if ($contenttype === 'text/html') {
			emailhandler_php_saveAttachment($partdata, 'emailmessage.htm', $contenttype);
		} else {
			if ($bodycontenttype === 'text/html') {
				emailhandler_php_saveAttachment($body, 'emailmessage.htm', $bodycontenttype);
			}

			$body = $partdata;
			$bodycontenttype = $contenttype;
		}
	} else if ($filename) {
		emailhandler_php_saveAttachment($partdata, $filename, $contenttype);
	}
}

$email = '';
$fo = fopen('php://stdin', 'r');
while (!feof($fo)) {
	$email .= fread($fo, 1024);
}
fclose($fo);

if (!$email)
	exit();

/* Line reading loop */
$lines = explode("\n", $email);
$linecount = count($lines);
$currentline = 0;
for ($currentline = 0; $currentline < $linecount; $currentline++) {
	$line = func_php_cleantext($lines[$currentline], 1024);
	if ($readingheaders && preg_match('/;$/', $line)) {
		/* Keep appending header lines ending with a semicolon (;)
		 * These are ususally meant to be processed together */
		while (preg_match('/;$/', $line)) {
			$currentline++;
			$line .= ' '.func_php_cleantext($lines[$currentline], 1024);
		}
	}

	/* Look for any headers, before reading data */
	if ($readingheaders && preg_match('/^(.+):\s*(.+)$/', $line, $matches)) {
		$key = mb_strtolower($matches[1]);
		$value = $matches[2];

		switch ($key) {
		case 'from':
			if ($boundarydepth === 0)
				$fromarray = emailhandler_php_handleFromHeader($value);
			break;
		case 'to':
			if ($boundarydepth === 0)
				$toarray = emailhandler_php_handleToHeader($value);
			break;
		case 'subject':
			if ($boundarydepth === 0)
				$subject = $value;
			break;
		case 'content-type':
			$retval = emailhandler_php_handleContentTypeHeader($value);
			if (isset($retval['boundarystart']) && isset($retval['boundaryend'])) {
				if (isset($boundary[0]['boundarystart'])) {
					$boundarydepth++;
				}
				$boundary[$boundarydepth] = $retval;
			} else if (isset($retval['content-type'])) {
				$partheaders[$key] = $retval['content-type'];
			}
			break;
		case 'content-transfer-encoding':
			$partheaders[$key] = $value;
			break;
		case 'content-disposition':
			if (preg_match('/attachment;\s*?filename=(.+)/', $value, $matches)) {
				$filename = $matches[1];
				if (preg_match('/^"(.*)"$/', $filename, $matches)) {
					$filename = $matches[1];
				}
				$partheaders[$key] = 'attachment';
				$partheaders['filename'] = $filename;
			}
		}
	}

	/* Look for multipart boundaries, and end the end of header newline */
	if (isset($boundary[$boundarydepth]['boundarystart'])
			&& ($line === $boundary[$boundarydepth]['boundarystart'])) {
		if ($partdata) {
			emailhandler_php_handlePartData($partheaders, $partdata);
		}

		$readingheaders = true;
		$readingdata = false;
		$partdata = '';
		$partheaders = [];
		continue;
	} else if (isset($boundary[$boundarydepth]['boundaryend'])
			&& $line === $boundary[$boundarydepth]['boundaryend']) {
		if ($partdata) {
			emailhandler_php_handlePartData($partheaders, $partdata);
		}
		$boundarydepth--;

		/* Discard unused boundary information, in case it gets unintentionally "recycled" */
		array_pop($boundary);
		$readingheaders = false;
		$readingdata = false;
		$partdata = '';
		$partheaders = [];
		continue;
	} else if ($readingheaders && ($line === '')) {
		if (count($toarray) == 0) {
			exit(); /* No recipients given at all. Exit */
		}
		$readingheaders = false;
		$readingdata = true;
		continue;
	}

	/* Save data, which is usually multipart but can be plain text email */
	if ($readingdata) {
		if (($line === '') && ($partdata)) {
			$partdata .= "\r\n";
		} else {
			$partdata .= $line;
			if (array_key_exists('content-transfer-encoding', $partheaders)
					&& ($partheaders['content-transfer-encoding'] !== 'base64')) {
				$partdata .= "\r\n";
			}
		}
	}
}

/* Attach email source file */
$gzdata = gzencode($email, 9);
emailhandler_php_saveAttachment($gzdata, 'emailsource.eml.gz', 'application/gzip');

/* Check if processing plain text email */
if (!$body && $partdata) {
	if (isset($partheaders['content-type'])) {
		if (($partheaders['content-type'] === 'text/plain')
				|| ($partheaders['content-type'] === 'text/html')) {
			emailhandler_php_handlePartData($partheaders, $partdata);
		}
	}
}

foreach ($toarray as $to) {
	/* Set certain permissions, depending if this is a post reply
	 * or an email to a specified user. */
	$parentid = 0;
	$viewpermissions = 0;
	$replypermissions = 0;
	$approvepermissions = 0;
	if ($to['postid'] !== 0) {
		$parentid = $to['postid'];
		$viewpermissions = 5;
		$replypermissions = 5;
		$approvepermissions = 5;
	} else {
		$viewpermissions = 1;
		$replypermissions = 1;
		$approvepermissions = 1;
	}
	$query = sprintf("insert into posts(parentid, accountid, emaildisplayname,"
			." emailusername, emailserver, emailinbox,"
			." viewpermissions, editpermissions, replypermissions,"
			." approvepermissions, moderatepermissions,"
			." tagpermissions, added)"
			." values (%d, 0, '%s'," // PARENTID, DISPLAYNAME
			." '%s', '%s', '%s'," // USERNAME, SERVER, INBOXNAME
			." %d, 1, %d, %d, " // VIEW, REPLY, APPROVE
			." 1, 1, NOW());",
			$parentid, func_php_escape($fromarray['displayname']),
			func_php_escape($fromarray['emailusername']),
			func_php_escape($fromarray['emailserver']),
			func_php_escape($to['emailinbox']),
			$viewpermissions, $replypermissions, $approvepermissions);
	func_php_query($query);

	$postid = mysqli_insert_id($mysqli);

	$inlineimages = '';
	$awsloaded = false;
	$s3available = ($s3profile && $s3endpoint && $s3region && $s3bucket);
	foreach ($attachment as $at) {
		/* TODO: Can we change the following lines to a function?
		 * They're reused in parts of index.php to upload files. */
		$query = sprintf('insert into files(accountid, postid, filename, filesize, filemime,'
				.' viewpermissions, editpermissions, replypermissions,'
				.' approvepermissions, moderatepermissions, tagpermissions, added)'
				.' values (%d, %d, "%s", %d, "%s",' // USERID, POSTID, FILENAME, FILESIZE, MIME
				.' %d, 1, %d, %d, 1, 1, NOW());', // VIEW, REPLY, APPROVE
				$to['userid'], $postid, func_php_escape($at['filename']),
				$at['filesize'], func_php_escape($at['mimetype']),
				$viewpermissions, $replypermissions, $approvepermissions);
		func_php_query($query);
		$fileid = mysqli_insert_id($mysqli);

		$fileext = '';
		if (preg_match('/(\.[^.]*?)$/', $at['filename'], $matches)) {
			$fileext = $matches[1];
		}
		$fileindex = $fileid.$fileext;

		/* Find out if saving to S3 bucket, or locally otherwise */
		$uploadsuccess = false;
		if ($s3available) {
			/* Use cloud storage (S3 bucket) */
			if (!$awsloaded) {
				require 'aws/aws-autoloader.php';
				$awsloaded = true;
			}

			$sdk = new Aws\Sdk([
				'profile' => $s3profile,
				'endpoint' => $s3endpoint,
				'region' => $s3region,
				'version' => 'latest',
				'use_path_style_endpoint' => true,
			]);
			$s3 = $sdk->createS3();

			try {
				$result = $s3->putObject([
					'Bucket' => $s3bucket,
					'Key' => $fileindex,
					'SourceFile' => $at['tmpfilename'],
				]);
				$uploadsuccess = true;
				$query = sprintf('update files'
						.' set s3profile="%s", s3endpoint="%s",' /* s3profile, s3endpoint */
						.' s3region="%s", s3bucket="%s"' /* s3region, s3bucket */
						.' where id=%d limit 1;', /* id */
						$s3profile, $s3endpoint, $s3region, $s3bucket, $fileid);
				func_php_query($query);
			} catch (S3Exception $e) {
				error_log($e->getMessage());
			}
		} else {
			/* Save on web host storage */
			$uploadsuccess = copy($at['tmpfilename'], 'files/'.$fileindex);
			if ($uploadsuccess) {
				chmod('files/'.$fileindex, 0666);
			}
		}
		if ($uploadsuccess) {
			$query = sprintf('update files set fileindex="%s"' // FILEINDEX
					.' where id=%d limit 1;', // FILEID
					func_php_escape($fileindex), $fileid);
			func_php_query($query);

			/* Display images within the post, rather than as attachments */
			if (preg_match('/^image.(gif|jpeg|jpg|png)$/', $at['mimetype'])) {
				$inlineimages .= sprintf("<img id=\"file%d\"" // FILEID
						." src=\"/index.php?fileid=%d" // FILEID
						."&amp;filename=%s\" class=\"postimg\">", // FILENAME
						$fileid, $fileid, rawurlencode($filename));
			}
		} else {
			$query = sprintf('delete from files where id = %d limit 1;',
					$fileid);
			func_php_query($query);
		}
	}

	/* Format some strings for storage. */
	$title = func_php_plaintexttohtml($subject);
	$body = trim($body);
	$description = func_php_plaintexttohtml("# {$subject}\n\n{$body}");
	if ($inlineimages)
		$description .= "<br><br>{$inlineimages}";
	$searchcache = func_php_stringtosearchcache("{$subject} {$body}");

	$query = sprintf("update posts set title='%s'," /* TITLE */
			." description='%s', searchcache='%s'" /* DESCRIPTION, SEARCHCACHE */
			." where posts.id=%d limit 1;", /* POSTID */
			func_php_escape($title), func_php_escape($description),
			func_php_escape($searchcache), $postid);
	func_php_query($query);

	if ($to['userid']) {
		$query = sprintf("insert into recipients"
			." (tablename, tableid, senderaccountid, recipientaccountid, added)"
			." values ('posts', %d, 0, %d, NOW());", /* POSTID, USERID */
			$postid, $to['userid']);
		func_php_query($query);

		/* Send the message to any open sseserver.php
		 * instances specified by userID */
		/* TODO: Can msg_send/msg_receive calls be moved to a function?
		 * TODO: They are also used to change online/offline statuses. */
		$recipientid = $to['userid'];
		$senderemail = "{$emailusername}@{$emailserver}";
		$backend_key = IPCMSGKEY + $recipientid;
		$confirmation_key = (IPCMSGKEY * 2) + $recipientid;
		if (msg_queue_exists($backend_key)
				&& msg_queue_exists($confirmation_key)) {
			$backend_queue = msg_get_queue($backend_key);
			$confirmation_queue = msg_get_queue($confirmation_key);
			$json = json_encode(array('type' => 'message_received', 'postid' => $id,
					'sender' => $senderemail, 'recipient' => $recipientid));

			/* Empty the confirmation queue */
			while (msg_receive($confirmation_queue, IPCMSGTYPE, $msgtype, 4096,
					$reply, false, MSG_IPC_NOWAIT)) { /* Do nothing */ }

			$attempts = 5;
			$md5 = md5($json);
			$replymd5 = '';
			do {
				msg_send($backend_queue, IPCMSGTYPE, $json);
				time_nanosleep(0, 100000000); /* 0.1 second delay */

				/* Get a confirmation from the SSE backend */
				$reply = false;
				if (msg_receive($confirmation_queue, IPCMSGTYPE, $msgtype, 4096,
						$reply, false, MSG_IPC_NOWAIT)) {
					$replymd5 = unserialize($reply);
				}
				$attempts--;
			} while (($attempts > 0) && ($replymd5 !== $md5));
		}
		/* TODO - END move this block to backend.php... */
	}
}
?>
