<html>
<head>
<title>Mirror Island Quickstart</title>
<style type="text/css">
body { font:normal 16px 'sans-serif'; font-weight:semi-bold; color:#567; background:#f0f0f3; padding:0px; margin:0px; line-height:24px; }
h1, h2, h3 { font-weight:bold; font-family:'sans-serif'; color:#345; }
h1 { font-size:18px; text-align:center; text-transform:uppercase; }
h2 { font-size:16px; font-weight:bold; text-align:left; text-transform:uppercase; }
h3 { font-size:16px; text-align:left; letter-spacing:1px; }
p { margin:16px 0 0 0; }
i, em { font-family:'serif'; }
ul { padding-left:20px; }
li { margin:10px 0; }
.maindiv { background:#fafafe; max-width:600px; padding:15px 25px; margin:10px auto 10px auto; border:solid 1px #dde; border-radius:8px; }
</style>
</head>

<body>

<div class="maindiv">

<center>
<h1>Mirror Island Quick Start Guide</h1>
<img src="milogo.png">
<p>December 31, 2017</p> 
</center> 

<h2>Start With A Post</h2>
<p>Start by making your first post. Near the top of the page click on <strong>New Post</strong> and the button will transform in to a new, blank document.</p>
<div style="background:#fff;border:solid 1px #dde; border-width:0 1px 0 1px; padding:20px 0;margin:20px; text-align:center;">
	<img src="/quickstart-01-newpost.png">
</div>

<p>The area highlighted in red is the <em>editor window</em> where you can type and add images. Surrounding the editor window are the controls.<p>
<div style="background:#fff;border:solid 1px #dde; border-width:0 1px 0 1px; padding:20px 0;margin:20px; text-align:center;">
	<img src="/quickstart-02-editpost.png">
</div>
<ul>
<li><strong>Undo</strong> and <strong>Redo</strong>: Undo (or redo) changes before saving a post.</li>
<li><strong>Paste Link</strong>: Turn the selected text in to a link, or if no selection has been made a copy of the address itself will be inserted and linked.</li>
<li><strong>Attach File or Image</strong>: Images will be displayed in the editor window. Other files will be listed below the editor window as attachments.</li>
<li><strong>Save Post</strong>: Saves the current post and closes the editor.</li>
<li><strong>Cancel</strong>: Undoes all changes and closes the editor.</li>
<li><strong>Permissions dialogue</strong>: Lets you control who can see the post.</li>
</ul>

<h3>A Quick Note On Permissions</h3>
<p>You can specify permissions when you hover the mouse pointer over the icons on the bottom left.</p>
<div style="background:#fff;border:solid 1px #dde; border-width:0 1px 0 1px; padding:20px 0;margin:20px; text-align:center;">
	<img src="quickstart-03-permissions.png">
</div>
<ul>
<li><strong>Visibility</strong>: Who can see the post. This includes;
	<ul>
	<li><strong>Anyone</strong>: Everybody can see.</li>
	<li><strong>Allow anonymous posts</strong>: When <em>Anyone</em> is selected, users don&rsquo;t have to be signed in to reply.</li>
	<li><strong>Trusted friends</strong>: People you have marked &quot;<em>Trusted</em>&quot; on your cantact list.</li>
	<li><strong>Recipients</strong>: People entered in the recipient field can see.</li>
	<li><strong>Only the owner</strong>: Only the owner can see this post.</li>
	</ul>
</li>
<li><strong>Replies</strong>: Permissions also apply to replies. They include;
	<ul>
	<li><strong>Accept All</strong>: All replies are displayed.</li>
	<li><strong>Need Approval</strong>: All replies are saved but only approved ones are displayed.</li>
	<li><strong>Disabled</strong>: No replies are accepted.</li>
	</ul>
</li>
</ul>

<p>Checking <strong>Advanced permissions</strong> will allow you to fine tune permissions (although that is an in-depth topic for another day.)<p>
<div style="background:#fff;border:solid 1px #dde; border-width:0 1px 0 1px; padding:20px 0;margin:20px; text-align:center;">
	<img src="quickstart-04-permissions-advanced.png">
</div>
<p>If you ever yourself in advanced mode, click on <strong>Advanced&nbsp;permissions</strong> to uncheck it and return to the quicker to use presets. A prompt may show up letting you know presets will reset permissions - simply click <em>OK</em> and continue editing.<p>

<h3>Saving Your Post</h3>
<p>Once you have finished typing in your post or uploading images or attachments, click on <em>Post</em> to save your post.</p>

<h2>Details On Each Post</h2>
<p>On each post you can find out more details including when it was posted, who posted it, and what permissions have been set on it.


<p>Permissions determines who can view, modify, add posts.</p>

<h3>Types of actions</h3>

<p>View: Who can see the post</p>

<p>Edit: Who can edit the post</p>

<p>Reply: Who can reply to the post</p>

<p>Approve: Who's replies can be displayed without needing approval from the post owner or moderator(s).</p>

<p>Observe: Who can see replies to the post.</p>

<p>Moderate: Who can edit and approve replies, as well as see and moderate banned replies to the post.</p>

<p>Tag: Who can add additional tags other than the editor/owner.</p>

<p>Edit: Who can see the additional tags.</p>

<h2>Audience</h2>

<p>This determines who the permissions apply to. Permissions cascade, however, so permissions that apply to a lower level also allow people higher in the permissions hierarchy.</p>

<h3>Target Audiences</h3> 

<p>Owner: Associated permissions apply to the owner only.</p>

<p>Recipient: Associated permissions apply to designated recpient.</p>

<p>Owner: Associated permissions apply to the owner only.</p>

</div>

</body>
</html>
