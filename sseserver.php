<?php
require('func.php');

date_default_timezone_set('Australia/Brisbane');
header('X-Accel-Buffering: no');
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

/* Verify user cookies */
$activesessionid = func_php_cleantext($_COOKIE['activesessionid'] ?? '', 64);
$activeusername = func_php_cleantext($_COOKIE['activeusername'] ?? '', 255);

$query = sprintf('select id, username from accounts'
		.' where sessionid="%s" and username="%s" limit 1',
		$activesessionid, $activeusername);
$result = func_php_query($query);
if (mysqli_num_rows($result) !== 1) {
	exit(); /* No valid user? Exit now */
}

$r = mysqli_fetch_assoc($result);
$sseserver_activeuserid = $r['id'];
$sseserver_activeusername = $r['username'];

$sseserver_backendkey = IPCMSGKEY + $sseserver_activeuserid;
$sseserver_backendqueue = msg_get_queue($sseserver_backendkey);

$sseserver_confirmationkey = (IPCMSGKEY * 2) + $sseserver_activeuserid;
$sseserver_confirmationqueue = msg_get_queue($sseserver_confirmationkey);

/* Generate and save a unique ID for changing online status, etc. */
$sseconnectionid = '';
for ($i = 0; $i < 64; $i++) {
	$sseconnectionid .= substr('1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
		mt_rand(0, 61), 1);
}
$query = sprintf('update accounts set sseconnectionid="%s" where id=%d limit 1;',
		func_php_escape($sseconnectionid), $sseserver_activeuserid);
$result = func_php_query($query);

/* Send data to open the connection */
echo 'data: {"type": "connected", "sseconnectionid": "'.$sseconnectionid.'"}'."\n\n";

$pingct = 0;
$isrunning = true;
while ($isrunning) {

	$pingct++;
	$previousreply = '';

	/* Recieve any messages from backend.php */
	while (msg_receive($sseserver_backendqueue, IPCMSGTYPE, $msgtype, 4096, $msg, false, MSG_IPC_NOWAIT)) {
		$m2 = unserialize($msg);

		/* Reply back to index.php with the md5
		 * to let it know we got the message */
		$reply = md5($m2);
		msg_send($sseserver_confirmationqueue, IPCMSGTYPE, $reply);

		/* Generate the SSE data to send depending on its type */
		$echodata = '';
		$m = json_decode($m2);
		switch ($m->type) {
		case 'message_received':
			$echodata = 'data: {"type": "message_received",'
				.'"id": "'.$m->postid.'",'
				.'"sender": "'.$m->sender.'"}';
			break;

		case 'online_status':
			$onlinestatus = 0;
			switch ($m->onlinestatus) {
			case 10:
			case 20:
			case 30:
				/* First-attempt connections: user changed online status */
				$onlinestatus = $m->onlinestatus + 2;

				$query = sprintf('update accounts set onlinestatus=%d where id=%d limit 1;',
						$onlinestatus, $sseserver_activeuserid);
				$result = func_php_query($query);
				break;
			case 11:
			case 21:
			case 31:
				/* Second-attempt connections: client app is retrying
				 * since it didn't receive status change message.
				 * No need to save to DB, just retry sending status change... */
				 $onlinestatus++;
				 break;
			case 0:
				$onlinestatus = 0;
				$isrunning = false;
			}

			$echodata = 'data: {"type": "online_status",'
				.'"onlinestatus": "'.$onlinestatus.'"}';
			break;
		}

		/* Echo data to recipient if not already done */
		if ($previousreply !== $reply) {
			$previousreply = $reply;

			echo "{$echodata}\n\n";

			$pingct = 0; /* No need to ping, already sending data */
		}
	}

	/* Ping every 15 seconds if no other data has been sent */
	if ($pingct > (15 * 10)) {
		$date = date(DATE_ISO8601);
		$pingct = 0;
		echo "event: ping\n"
			.'data: {"time": "'.$date.'"}'."\n\n";
	}

	/* Flush output buffer */
	while (ob_get_level() > 0) {
		ob_end_flush();
	}
	flush();

	if (connection_aborted()) {
		break;
	}

	//sleep(1); /* 1 second */
	time_nanosleep(0, 100000000); /* 0.1 second delay */
}

/* Free up resources */
if (msg_queue_exists($sseserver_backendkey))
	msg_remove_queue($sseserver_backendqueue);

if (msg_queue_exists($sseserver_confirmationkey))
	msg_remove_queue($sseserver_confirmationqueue);

/* Set status offline */
$query = sprintf('/* sseserver.php'.__LINE__.' */ update accounts set onlinestatus = 0'
		.' where id = %d limit 1', $sseserver_activeuserid);
func_php_query($query);
?>
