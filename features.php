<!DOCTYPE html>
<html>
<head>
<title>Mirror Island Feature Tour</title>
<style type="text/css">
body { font:normal 16px Arial,Helvetica,sans-serif; font-weight:semi-bold; color:#567; background:#f0f0f3; padding:0px; margin:0px; line-height:24px; }
h1, h2, h3 { font-weight:bold; font-family:'sans-serif'; color:#345; }
h1 { font-size:18px; text-align:center; text-transform:uppercase; }
h2 { font-size:18px; text-align:left; font-weight:bold; border-bottom:solid 1px #5a9; letter-spacing:2px; color:#5a9; padding:0 0 4px 0; }
h3 { font-size:16px; text-align:left; letter-spacing:1px; color:#5a9; margin-top:20px; }
p { margin:16px 0 0 0; }
i, em { font-family:'serif'; }
ul { padding-left:20px; }
li { margin:10px 0; }
.maindiv { background:#fafafe; max-width:600px; padding:15px 25px; margin:10px auto 10px auto; border:solid 1px #dde; border-radius:8px; }
.centerimg { background:#fff; border:solid 1px #cde; border-width:0 1px 0 1px; display:block; padding:20px; margin:20px auto 20px auto; }
#footer { color:#79a; padding:10px 20px; text-align:center; margin:30px 0 0 0; border-top:solid 1px #cde; } 
#footer div { margin:10px; }
#footer a { background:none; color:#57f; text-decoration:none; margin:auto 4px; }
#footer a:hover { text-decoration:underline; } 
</style>
</head>

<body>

<div class="maindiv">

<center>
<h1>Mirror Island Feature Tour</h1>
<a href="/"><img src="/milogo.png"></a>
<p>June 27, 2018</p>
</center> 

<p>Mirror Island is a new kind of network designed to help you get your ideas and messages out to the world as quick as possible.</p>

<p>The feature list is building, and currently you can:</p>

<h2>Attach Files Like An Email</h2>

<p>Uploading isn't limited to just videos, photos and music, you can attach any file like an email for anybody to download. Currently, only attachments up to 4MB for each file are accepted, although this limit will change in the future.<//p>

<h2>Edit Anything You Have Posted</h2>

<p>You always have complete control, so you can edit and delete anything you have put on here. Even instant messages you have sent a long time ago can be corrected or removed!</p>

<h2>Edit In A Group</h2>

<p>You can specify who else can edit posts and moderate replies to the post.</p>

<div id="footer">
	<div>
		<a href="/"><strong>Mirror&nbsp;Island</strong></a>
		<a href="/about.php">About</a>
		<a href="/contact.php">Contact Us</a>
		<a href="/terms.php">Terms of Use</a>
		<a href="/manifesto.php">Manifesto</a>
		<a href="/quickstart.php">Quickstart Guide</a>
		<a href="/features.php">Feature Tour</a>
		<a href="/advertise.php">Advertise</a>
		<a href="/reportabug.php">Report a Bug</a>
	</div>
</div>

</div>

</body>
</html>
